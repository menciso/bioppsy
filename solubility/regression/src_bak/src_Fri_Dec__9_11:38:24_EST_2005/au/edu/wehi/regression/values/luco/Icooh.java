/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;
import au.edu.wehi.regression.values.SmartsMatchCount;
import joelib2.molecule.*;
import joelib2.smarts.BasicSMARTSPatternMatcher;
import joelib2.util.iterator.BondIterator;

/**
 * Icooh, -1.0 if molecule contains an acidic COOH group,
 * 0.0 otherwise.
 * 
 * @author eduthie
 */
public class Icooh extends MoleculeValueDesc
{
    /**
     * Return -1 if the given Molecule contains an acidic COOH group,
     * 0.0 otherwise.
     */
    public double value(Molecule mol)
    {
        String pattern = "[CX3,$(*=[OX1]),$(*[OX2H1])]";
        BasicSMARTSPatternMatcher pm = new BasicSMARTSPatternMatcher();
        if( !pm.init(pattern) )
        {
            Logger.getLogger(getClass()).error("Invalid SMARTS pattern: " + pattern);
            return Double.NaN;
        }
        pm.match(mol);
        if( pm.getMatchesSize() > 0 )
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
}
