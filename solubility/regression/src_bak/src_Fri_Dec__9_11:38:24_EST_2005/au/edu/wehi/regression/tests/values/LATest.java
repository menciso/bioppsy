/*
 * Created on: Jun 17, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values;

import joelib2.io.BasicIOTypeHolder;
import joelib2.io.IOType;
import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.LA;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author eduthie
 */
public class LATest extends TestCase
{
    private static Logger logger = Logger.getLogger(LATest.class);
    private MoleculeVector molecules;

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(LATest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        String name = "laTest.mol2";
        InputStream in = getClass().getResourceAsStream(name);
        IOType ioType = BasicIOTypeHolder.instance().getIOType("mol2");
        molecules = new BasicMoleculeVector(in,ioType,ioType);
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for LATest.
     * @param arg0
     */
    public LATest(String arg0)
    {
        super(arg0);
    }

    public void testLA()
    {
        
        double[] expected = 
        {
                0.864,
                0.318,
                1.346,
                0.123,
                1.03,
                1.794,
                1.721,
                0.9690000000000001,
                0.6799999999999999,
                1.041,
                2.127,
                3.06,
                1.9729999999999999,
                0.569,
                0.741,
                0.632,
                1.425,
                2.316,
                0.739,
                1.187,
                2.048
        };
        
        try
        {
            LA la = new LA();
            for( int i = molecules.getSize() - 1; i >= 0; --i )
            {
                Molecule mol = molecules.getMol(i);
                double value = la.value(mol);
                assertEquals(expected[20-i],value);
                //logger.info("VALUE FOR MOLECULE " + (20-i) + " : " + value);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
