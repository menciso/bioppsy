/*
 * Created on: Jul 22, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import java.io.*;
import java.util.Properties;

import joelib2.io.BasicIOType;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.values.*;
import au.edu.wehi.regression.values.luco.*;
import au.edu.wehi.regression.gui.GUI;
/**
 * Provides a command-line user interface
 * 
 * @author eduthie
 */
public class CommandLine
{
    private static Logger logger = Logger.getLogger(CommandLine.class);
    
    private static final int MAX_ARGS = 2;
    private static final int MIN_ARGS = 2;
    private static final String PROGRAM_NAME = "regression.jar";
    private static final String WHITESPACE = " ";
    private static final String OUTPUT_SEPARATOR = " ";
    
    private String[] acceptableValues;
    private String[] valuesNames;
    private MoleculeValueDesc[] descriptors;
    private String inputFilename;
    private String[][] results;

    public static void main(String[] args)
    {
        PropertyConfigurator.configure(
                Constants.class.getResource("regression.log4j"));
        
        // load gui if there are no args
        if( args.length == 0 )
        {
            GUI gui = new GUI();
            return;
        }
       
        CommandLine cl = new CommandLine();
        if( !cl.loadProperties() )
        {
            logger.error("Failed to load vital constants");
            System.exit(0);
        }
        if( !cl.processArgs(args) )
        {
            cl.printUsage();
            System.exit(0);
        }
        if( !cl.prepareDescriptors() )
        {
            System.exit(0);
        }
        if( !cl.performCalculation() )
        {
            logger.error("Failed to perform calculation");
            System.exit(0);
        }
        if( !cl.formatResults() )
        {
            logger.error("Failed to format results");
            System.exit(0);
        }
        cl.printResults(System.out);
        System.exit(1);
    }
    
    /**
     * Loads constants from the properties file
     */
    public boolean loadProperties()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + Constants.PROPERTIES);
                return false;
            }
            
            // get database inputFilename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            String valuesString = typesProp.getProperty("ValuesClassNames");
            if( valuesString == null )
            {
                logger.error("Failed to retrieve ValuesClassNames");
                return false;
            }
            acceptableValues = valuesString.trim().split(Constants.INPUT_SEPARATOR);
            return true;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }        
    }
    
    /**
     * Validates the command line arguments
     */
    public boolean processArgs(String[] args)
    {
        if( args.length > MAX_ARGS )
        {
            logger.error("Number of arguments exceeds " + MAX_ARGS);
            return false;
        }
        if( args.length < MIN_ARGS )
        {
            logger.error("Number of arguments is less than " + MIN_ARGS);
            return false;
        }
        
        valuesNames = args[0].split(Constants.INPUT_SEPARATOR);
        for( int i=0; i < valuesNames.length; ++i )
        {
            boolean have = false;
            for(int j=0; j < acceptableValues.length; ++j )
            {
                if( acceptableValues[j].equals(valuesNames[i]) )
                {
                    have = true;
                    break;
                }
            }
            if( !have )
            {
                logger.error("The value '" + valuesNames[i] + "' is invalid");
                return false;
            }
        }
        
        inputFilename = args[1];
        File file = new File(inputFilename);
        if( !file.canRead() )
        {
            logger.error("Unable to read the file: " + inputFilename);
            return false;
        }
        
        return true;
    }
    
    /**
     * Prints comand line usage information to System.out
     */
    public void printUsage()
    {
        System.out.println("");
        System.out.println("USAGE INFORMATION:");
        System.out.println("");
        System.out.println("java -jar " + PROGRAM_NAME + 
                " Comma,Separated,List,Of,Values,To,Calculate" +
                " FilenameContainingMolecules ");
        System.out.println("");
        String line = "Avaliable Values: ";
        for(int i=0; i < acceptableValues.length; ++i )
        {
            if( i > 0 )
            {
                line += ",";
            }
            line += acceptableValues[i];
        }
        System.out.println(line);
    }
    
    /**
     * Loads all the descriptors chosen by the user. Returns true
     * if all descriptors have been created successfully and stored
     * in the descriptors array, false otherwise.
     */
    public boolean prepareDescriptors()
    {
        descriptors = new MoleculeValueDesc[valuesNames.length];
        Class theClass = null;
        
        for( int i=0; i < valuesNames.length; ++i )
        {
            try
            {
                theClass = Class.forName(Constants.VALUES_PREFIX + "." + valuesNames[i]);
            }
            catch( ClassNotFoundException cnfe )
            {
                try
                {
                    // second time try the LUCO_PREFIX
                    theClass = Class.forName(Constants.LUCO_PREFIX + "." + valuesNames[i]);
                }
                catch( ClassNotFoundException cnfe2 )
                {
                    logger.error("Unable to locate the value: " + valuesNames[i]);
                    logger.error("Please remove from the list to perform calculation");
                    return false; 
                }
            }
            try
            {
                descriptors[i] = (MoleculeValueDesc) theClass.newInstance();
            }
            catch( IllegalAccessException iae )
            {
                logger.error("Illegal Access to class: " + theClass.getName());
                logger.error(iae.getMessage());
                logger.error("Please remove the value: " + valuesNames[i] + 
                        " from the list to perform calculation");
                return false;
            }
            catch( InstantiationException ie )
            {
                logger.error("Unable to Instantiate class: " + theClass.getName());
                logger.error(ie.getMessage());
                logger.error("Please remove the value: " + valuesNames[i] + 
                    " from the list to perform calculation");
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Calculates the values and stores them as Strings
     * in the results matrix
     */
    public boolean performCalculation()
    {       
        try
        {
            RegressionMoleculeVector rmv = getRMV(inputFilename);
            if( rmv == null )
            {
                logger.error("Failed to read input file: " + inputFilename);
                return false;
            }
            
            // initialise the output matrix
            results = new String[rmv.getSize()+1][descriptors.length];
            
            int index = 0;
            
            // add the headings
            for( int i=0; i < valuesNames.length; ++i )
            {
                results[index][i] = valuesNames[i];
            }
            
            // do the calculations
            for( int i=0; i < rmv.getSize(); ++i )
            {
                ++index;
                Molecule mol = rmv.getMol(i);
                for( int j=0; j < descriptors.length; ++j )
                {
                    double value = descriptors[j].value(mol);
                    results[index][j] = round(value,Constants.DP_OUTPUT);
                }
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        
        return true;
    }
    
    public RegressionMoleculeVector getRMV(String filename)
        throws IOException
    {
        if( !BasicIOTypeHolder.instance().canReadExtension(filename) )
        {
            logger.error("Invalid file extension: " + filename);
            return null;
        }
        BasicIOType type = BasicReader.checkGetInputType(filename);
        if( type == null )
        {
            logger.error("Invalid file extension: " + filename);
            return null;
        }
        InputStream in = new FileInputStream(filename);
        if( in == null )
        {
            logger.error("Unable to read file: " + filename);
            return null;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        return rmv;
    }
    
    /**
     * Rounds the given double to the given number of decimal places and
     * returns its string representation
     */
    public String round(double value, int dp)
    {
        String valueString = new Double(value).toString();
        int dpIndex = valueString.indexOf('.');
        if( (dpIndex+dp+1) >= valueString.length() )
        {
            return valueString;
        }
        else
        {
            return valueString.substring(0,dpIndex+dp+1);
        }
    }
    
    /**
     * Formats the results table
     */
    public boolean formatResults()
    {
        // for each column
        for( int j=0; j < results[0].length; ++j )
        {
            int maxLength = results[0][j].length();
            
            // for each row
            for( int i=0; i < results.length; ++i )
            {
                if( results[i][j].length() > maxLength )
                {
                    maxLength = results[i][j].length();
                }
            }
            // add additional spaces to end of strings
            for( int i=0; i < results.length; ++i )
            {
                for( int diff = results[i][j].length();
                    diff < maxLength; ++diff )
                {
                    results[i][j] += WHITESPACE;
                }
            }
        }
        
        return true;
    }
    
    /**
     * Displays the results to the given PrintStream
     */
    public void printResults(PrintStream out)
    {
        String line;
        for( int i=0; i < results.length; ++i )
        {
            line = "";
            for( int j=0; j < results[i].length; ++j )
            {
                if( j > 0 )
                {
                    line += OUTPUT_SEPARATOR;
                }
                line += results[i][j];
            }
            out.println(line);
        }
    }
}
