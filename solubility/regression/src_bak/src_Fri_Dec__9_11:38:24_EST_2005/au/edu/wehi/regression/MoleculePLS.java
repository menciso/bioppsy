/*
 * Created on: May 3, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import java.util.Vector;
import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import Jama.Matrix;
import au.edu.wehi.regression.util.XMatrix;
import au.edu.wehi.regression.rows.MoleculeRowDesc;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * The MoleculePLS is a PLS model that uses Molecules as data
 * sources rather than just the data Matricies.
 * 
 * @author eduthie
 */
public class MoleculePLS extends PLS implements MoleculeModel, Named
{
    private String name;
    private MoleculeRowDesc xDesc;
    
    public MoleculePLS()
    {
        super();
    }
    
    public MoleculePLS(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return name;
    }
    
    /**
     * Trains the PLS from the given data. The given RegressionMoleculeVector
     * must have been initialised with the molecules from the training set.
     * Additionally the independent and dependent describers of the 
     * RegressionMoleculeVector must be set to determine what descriptors
     * are calculated from the molecules and what properties are being 
     * predicted.
     * 
     * @param rmv - training data
     */
    public boolean train(RegressionMoleculeVector rmv)
    {
        Matrix X = new Matrix(rmv.getXMatrix());
        Vector<Integer> allZero =  (new XMatrix(X.getArray())).anyZeroColumns();
        if( allZero.size() > 0)
        {
            Logger.getLogger(MoleculePLS.class).error("One or more descriptors are all zero for the entire " +
                    " training set. Please remove them:");
            for( int i=0; i < allZero.size(); ++i)
            {
                Logger.getLogger(getClass()).error(" descriptor index: " + allZero.elementAt(i));
            }
            return false;
        }
        Matrix Y = new Matrix(rmv.getYMatrix());
        
        xDesc = rmv.getIndependentDescriber();
        
        return train(X.getArray(),Y.getArray());
    }
    
    /**
     * Returns the predicted value for the given Molecule. Returns Double.NaN
     * on error.
     */
    public double value(Molecule mol)
    {
        double[] row = xDesc.getRow(mol);
        double[][] results = predict(row);
        if( (results.length <= 0) || (results[0].length <= 0) )
        {
            Logger.getLogger(getClass()).error("No results obtained");
            return Double.NaN;
        }
        return results[0][0];
    }
}
