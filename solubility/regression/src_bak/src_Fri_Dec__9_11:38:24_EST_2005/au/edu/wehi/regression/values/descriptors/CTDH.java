/*
 * Created on: Dec 8, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.descriptors;

import joelib2.molecule.Atom;
import joelib2.util.iterator.AtomIterator;
import joelib2.molecule.Molecule;
import joelib2.feature.types.atomlabel.AtomIsCarbon;
import joelib2.feature.types.atomlabel.AtomIsHydrogen;
import joelib2.feature.types.atomlabel.AtomImplicitHydrogenCount;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * CTDH, the count of donatable hydrogens. Counts the
 * number of hydrogen atoms that are not connected to
 * Carbon.
 * 
 * @author eduthie
 */
public class CTDH extends MoleculeValueDesc
{

    /* (non-Javadoc)
     * @see au.edu.wehi.regression.values.MoleculeValueDesc#value(joelib2.molecule.Molecule)
     */
    public double value(Molecule mol)
    {
        int count = 0;
        Atom atom;
 
        AtomIterator ai = mol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            if( !AtomIsCarbon.isCarbon(atom) && !AtomIsHydrogen.isHydrogen(atom) )
            {
                count += AtomImplicitHydrogenCount.getIntValue(atom);
            }
        }

        return (double) count;
    }

}
