/*
 * Created on: Sep 16, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.qsar.DescriptorValue;
import org.openscience.cdk.qsar.RotatableBondsCountDescriptor;
import org.openscience.cdk.qsar.result.IntegerResult;
import au.edu.wehi.regression.util.CDKConverter;

/**
 * Number of rotatable bonds in the molecule
 * 
 * @author eduthie
 */
public class RotatableBonds extends MoleculeValueDesc
{
    public String getDescription()
    {
        return "Number of rotatable bonds in the molecule";
    }

    /**
     * Returns number of rotatable bonds in the molecule
     */
    public double value(Molecule mol)
    {
        try
        {
            org.openscience.cdk.Molecule cdkMol = 
                CDKConverter.convertToCDK(mol);
            RotatableBondsCountDescriptor rotBonds = new RotatableBondsCountDescriptor();
            Object[] parameters = {true}; // include terminal bonds
            rotBonds.setParameters(parameters);
            DescriptorValue value = rotBonds.calculate(cdkMol);
            IntegerResult result = (IntegerResult) value.getValue();
            return result.intValue();
        }
        catch( CDKException cdke)
        {
            Logger.getLogger(getClass()).error(cdke.getMessage());
            return Double.NaN;
        }
    }

}
