/*
 * Created on: May 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values.luco;

import java.io.IOException;
import java.io.InputStream;

import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.IOType;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.luco.E;

/**
 * @author eduthie
 */
public class ETest extends TestCase
{
    private static Logger logger = Logger.getLogger(ETest.class);
    private Molecule hexane;
    private MoleculeVector molecules;

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(ETest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            String name = "2,3,4,5-tetramethylhexane.mol2";
            InputStream in = getClass().getResourceAsStream(name);
            hexane = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(hexane) )
            {
                fail("Failed to read: " + name);
            }
            /*
            name = "chitest.mol2";
            in = getClass().getResourceAsStream(name);
            IOType ioType = BasicIOTypeHolder.instance().getIOType("mol2");
            molecules = new BasicMoleculeVector(in,ioType,ioType);
            */
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for ETest.
     * @param arg0
     */
    public ETest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        E e = new E();
        double value = e.value(hexane);
        assertEquals(2.8,value);
        
        /*
        for( int i=0; i < molecules.getSize(); ++i )
        {
            Molecule mol = molecules.getMol(i);
            value = e.value(mol);
            logger.info("MOL: " + i + " E: " + value);
        }
        */
    }

}
