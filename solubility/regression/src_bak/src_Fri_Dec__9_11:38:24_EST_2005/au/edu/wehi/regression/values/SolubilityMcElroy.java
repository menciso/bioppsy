/*
 * Created on: Dec 8, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import joelib2.molecule.Bond;
import joelib2.util.iterator.BondIterator;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.descriptors.*;

/**
 * Returns logS using the McElroy model:
 * Prediction of Aqueous Solubility of Heteroatom-Containing
 * Organic Compounds from Molecular Structure
 * Nathan R. McElroy and Peter C. Jurs
 * 
 * @author eduthie
 */
public class SolubilityMcElroy extends MoleculeValueDesc
{
    public String getDescription()
    {
        return
            "Returns logS using the McElroy model:<br>" +
            "Prediction of Aqueous Solubility of Heteroatom-Containing<br>" +
            "Organic Compounds from Molecular Structure<br>" +
            "Nathan R. McElroy and Peter C. Jurs<br><br>" +
            "REQUIRES 3D data";
    }

    /**
     * Returns logS using the McElroy model
     */
    public double value(Molecule mol)
    {
        double nsb,ndb,mde,mde24,mde34,eave2,geom1,geom3,grav6,rncg,ctdh;
        
        nsb = (double) numberOfSingleBonds(mol);
        ndb = (double) numberOfDoubleBonds(mol);
        mde = new MDE(1,4).value(mol);
        mde24 = new MDE(2,4).value(mol);
        mde34 = new MDE(3,4).value(mol);
        eave2 = new EAVE2().value(mol);
        geom1 = new GEOM(1).value(mol);
        geom3 = new GEOM(3).value(mol);
        grav6 = new GRAV6().value(mol);
        rncg = new RNCG().value(mol);
        ctdh = new CTDH().value(mol);
        
        /*
        System.out.println("nsb: " + nsb);
        System.out.println("ndb: " + nsb);
        System.out.println("mde: " + mde);
        System.out.println("mde24: " + mde24);
        System.out.println("mde34: " + mde34);
        System.out.println("eave2: " + eave2);
        System.out.println("geom1: " + geom1);
        System.out.println("geom3: " + geom3);
        System.out.println("grav6: " + grav6);
        System.out.println("rncg: " + rncg);
        System.out.println("ctdh: " + ctdh);
        */
        
        return
            2.58 +
            -0.133*nsb +
            0.356*ndb +
            0.0646*mde +
            0.0383*mde24 +
            -0.0337*mde34 +
            -0.186*eave2 +
            0.0847*geom1 +
            1.10*geom3 +
            -0.306*grav6 +
            3.00*rncg +
            0.787*ctdh;
    }
    
    /**
     * Returns the number of single bonds in the molecule
     */
    public int numberOfSingleBonds(Molecule mol)
    {
        int nsb = 0;
        BondIterator bi = mol.bondIterator();
        while(bi.hasNext())
        {
            Bond bond = bi.nextBond();
            if( (bond.getBondOrder() == 1) && (!bond.isBondOrderAromatic()))
            {
                ++nsb;
            }
        }
        return nsb;
    }
    
    /**
     * Returns the number of double bonds in the molecule
     */
    public int numberOfDoubleBonds(Molecule mol)
    {
        int ndb = 0;
        BondIterator bi = mol.bondIterator();
        while(bi.hasNext())
        {
            Bond bond = bi.nextBond();
            if( (bond.getBondOrder() == 2) && (!bond.isBondOrderAromatic()))
            {
                ++ndb;
            }
        }
        return ndb;
    }

}
