/*
 * Created on: Sep 12, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import org.openscience.cdk.qsar.*;
import org.openscience.cdk.qsar.result.*;
import au.edu.wehi.regression.util.CDKConverter;
import org.openscience.cdk.exception.*;

/**
 * XLogP as provided by the CDK package
 * 
 * @author eduthie
 */
public class XLogP extends MoleculeValueDesc
{
    /**
     * Returns a text description of the descriptor for the user
     */
    public String getDescription()
    {
        return "XLogP as provided by the CDK package";
    }
    
    /**
     * Returns XLogP as provided by the CDK package
     */
    public double value(Molecule mol)
    {
        try
        {
            org.openscience.cdk.Molecule cdkMol = 
                CDKConverter.convertToCDK(mol);
            XLogPDescriptor xlogp = new XLogPDescriptor();
            DescriptorValue value = xlogp.calculate(cdkMol);
            DoubleResult result = (DoubleResult) value.getValue();
            return result.doubleValue();
        }
        catch( CDKException cdke)
        {
            Logger.getLogger(getClass()).error(cdke.getMessage());
            return Double.NaN;
        }
    }

}
