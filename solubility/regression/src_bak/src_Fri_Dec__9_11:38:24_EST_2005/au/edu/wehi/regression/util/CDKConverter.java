/*
 * Created on: Sep 13, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import org.apache.log4j.Logger;
import joelib2.molecule.*;
import joelib2.util.iterator.AtomIterator;
import joelib2.util.iterator.BondIterator;
import org.openscience.cdk.AtomContainer;
import javax.vecmath.Point3d;
import joelib2.data.*;

/**
 * Static methods for converting from JOELib representations
 * to CDK representations.
 * 
 * @author eduthie
 */
public class CDKConverter
{
    private static Logger logger = Logger.getLogger(CDKConverter.class);
    
    public static org.openscience.cdk.Molecule convertToCDK(Molecule mol)
    {
        AtomContainer atomContainer = new AtomContainer();
        
        AtomIterator ai = mol.atomIterator();
        while( ai.hasNext() )
        {
            Atom atom = ai.nextAtom();
            atomContainer.addAtom(convertToCDK(atom));
        }
        
        BondIterator bi = mol.bondIterator();
        while( bi.hasNext() )
        {
            Bond bond = bi.nextBond();
            atomContainer.addBond(convertToCDK(bond,atomContainer));
        }
        
        org.openscience.cdk.Molecule cdkMol = new 
            org.openscience.cdk.Molecule(atomContainer);
        
        return cdkMol;
    }
    
    public static org.openscience.cdk.Atom convertToCDK(Atom atom)
    {
        /* my old stuff
        int atomicNumber = atom.getAtomicNumber();
        Point3d point3d = new Point3d(atom.get3Dx(),atom.get3Dy(),atom.get3Dz());
        String type = atom.getType();
        BasicAtomTypeConversionHolder converter = 
            BasicAtomTypeConversionHolder.instance();
        converter.setFromType("INT");
        converter.setToType("XYZ"); // close enough to CDK conventions
        String convertedType = converter.translate(type);
        org.openscience.cdk.Atom cdkAtom = 
            new org.openscience.cdk.Atom(convertedType);
        cdkAtom.setAtomicNumber(atomicNumber);
        cdkAtom.setFractionalPoint3d(point3d);
        return cdkAtom;
        */
        org.openscience.cdk.Atom convertedAtom = 
            new org.openscience.cdk.Atom("C");
        try 
        {
            // try to give the atom the correct symbol
            org.openscience.cdk.config.IsotopeFactory ef =
                org.openscience.cdk.config.IsotopeFactory.getInstance();
            org.openscience.cdk.Element e = ef.getElement(atom.getAtomicNumber());
            convertedAtom = new org.openscience.cdk.Atom(e.getSymbol());
            // try to give the atom its coordinates
            convertedAtom.setX3d(atom.get3Dx());
            convertedAtom.setY3d(atom.get3Dy());
            convertedAtom.setZ3d(atom.get3Dz());
            // try to give the atom its atomic number
            convertedAtom.setAtomicNumber(atom.getAtomicNumber());
        } 
        catch (java.lang.Exception e) 
        {
            logger.error("Atom conversion failed");
            return null;
        }
        return convertedAtom;
    }
    
    public static org.openscience.cdk.Bond convertToCDK(Bond bond, 
            AtomContainer atomContainer)
    {
        Atom atom1 = bond.getBegin();
        Atom atom2 = bond.getEnd();
        org.openscience.cdk.Atom cdkAtom1 = atomContainer.getAtomAt(atom1.getIndex()-1);
        org.openscience.cdk.Atom cdkAtom2 = atomContainer.getAtomAt(atom2.getIndex()-1);
        org.openscience.cdk.Bond cdkBond = 
            new org.openscience.cdk.Bond(cdkAtom1,cdkAtom2,bond.getBondOrder());
        // IS THE BOND ORDER THE SAME?????????
        return cdkBond;
    }
}
