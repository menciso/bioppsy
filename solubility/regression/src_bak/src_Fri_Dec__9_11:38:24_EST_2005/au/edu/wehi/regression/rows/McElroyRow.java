/*
 * Created on: Dec 9, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.rows;

import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.descriptors.*;

/**
 * Returns the descriptors of the McElroy model:
 * Prediction of Aqueous Solubility of Heteroatom-Containing
 * Organic Compounds from Molecular Structure
 * Nathan R. McElroy and Peter C. Jurs
 * 
 * @author eduthie
 */
public class McElroyRow extends MoleculeRowDesc
{
    public McElroyRow()
    {
        super();
        addFeature(new NSB());
        addFeature(new NDB());
        addFeature(new MDE(1,4));
        addFeature(new MDE(2,4));
        addFeature(new MDE(3,4));
        addFeature(new EAVE2());
        addFeature(new GEOM(1));
        addFeature(new GEOM(3));
        addFeature(new GRAV6());
        addFeature(new RNCG());
        addFeature(new CTDH());
    }

}
