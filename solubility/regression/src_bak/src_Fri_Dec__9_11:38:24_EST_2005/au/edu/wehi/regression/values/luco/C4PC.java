/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.Molecule;
import au.edu.wehi.regression.util.Subgraph;
import au.edu.wehi.regression.values.MoleculeValueDesc;

import org.apache.log4j.Logger;

/**
 * The simple and valence (n=v) connectivity index defined as:<br><br>
 * C4PCn = X4PCn / X3Cn + 1<br>
 * where Xmtn is the Chi index of type t with m edges.<br>
 * <br>
 * Reference:<br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * Juan M Luco<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404
 * 
 * @author eduthie
 */
public class C4PC extends MoleculeValueDesc
{
    private boolean valence;
    
    /**
     * Constructs a C4PC with valence = true by default
     */
    public C4PC()
    {
        this.valence = true;
    }
    
    /**
     * If valence = true, the valence of an atom is calculated as
     * Z-h where Z is the number of valence electrons and h is the number
     * of attached Hydrogens. Otherwise an atom's valence is the number of
     * bonds connected to the atom.
     */
    public C4PC(boolean valence)
    {
        this.valence = valence;
    }
    
    /**
    * Returns the simple and valence (n=v) connectivity index defined as:<br><br>
    * C4PCn = X4PCn / X3Cn + 1<br>
    * where Xmtn is the Chi index of type t with m edges.
    */
    public double value(Molecule mol)
    {
        Chi chi4 = new Chi(4,Subgraph.PATH_CLUSTER,valence);
        Chi chi3 = new Chi(3,Subgraph.CLUSTER,valence);
        double denominator = chi3.value(mol) + 1;
        if( denominator == 0.0 )
        {
            Logger.getLogger(getClass()).error("Zero denominator in C4PC");
            return Double.NaN;
        }
        else
        {
            return chi4.value(mol) / denominator;
        }
        
    }

}
