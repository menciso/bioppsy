/*
 * Created on: Nov 7, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.*;
import joelib2.util.iterator.AtomIterator;
import org.apache.log4j.Logger;

/**
 * Returns logS from the Delaney model:<br>
 * <br>
 * logS = 0.16 - 0.63clogP - 0.0062MWT + 0.066RB - 0.74AP
 * <br>
 * <br>
 * ESOL: Estimating Aqueous Solubility Directly from Molecular Structure<br>
 * John S. Delaney<br>
 * J. Chem. Inf. Comput. Sci, October 2003<br>
 */
public class SolubilityDelaney extends MoleculeValueDesc
{
    
    public String getDescription()
    {
        return
        " Returns logS from the Delaney model:<br>" +
        " <br>" +
        " logS = 0.16 - 0.63clogP - 0.0062MWT + 0.066RB - 0.74AP" +
        " <br>" +
        " <br>" +
        " ESOL: Estimating Aqueous Solubility Directly from Molecular Structure<br>" +
        " John S. Delaney<br>" +
        " J. Chem. Inf. Comput. Sci, October 2003<br>";
    }

    /**
     * Returns:<br>
     * <br>
     * logS = 0.16 - 0.63clogP - 0.0062MWT + 0.066RB - 0.74AP

     */
    public double value(Molecule mol)
    {
        double xlogp = new XLogP().value(mol);
        double mwt = new MWT().value(mol);
        double rb = new RotatableBonds().value(mol);
        
        int arAtoms = 0;
        int totalAtoms = 0;
        AtomIterator ai = mol.atomIterator();
        while( ai.hasNext() )
        {
            ++totalAtoms;
            Atom atom = ai.nextAtom();
            if( atom.hasAromaticBondOrder() )
            {
                ++arAtoms;
            }
        }
        double ap = arAtoms/totalAtoms;
        
        return 0.16 - 0.63*xlogp - 0.0062*mwt + 0.066*rb + 0.74*ap;
    }

}
