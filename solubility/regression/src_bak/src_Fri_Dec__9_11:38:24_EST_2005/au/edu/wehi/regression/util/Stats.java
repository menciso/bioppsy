/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import java.util.Vector;
import org.apache.log4j.Logger;

/**
 * static statistics utility functions.
 * 
 * @author eduthie
 */
public class Stats
{
    private static Logger logger = Logger.getLogger(Stats.class);
    
    /**
     * Returns the mean of the array of values.
     */
    public static double mean(double[] x)
    {
        double mean = 0.0;
        for( int i=0; i < x.length; ++i )
        {
            mean += x[i];
        }
        return mean/x.length;
    }
    
    /**************************************************
     * Returns the variance of the given array
     * of doubles.<br>
     * v = ( n*sum(x^2) + sum(x)^2 ) / n(n-1)
     **************************************************/
    public static double variance(double[] values)
    {
        double value;
        double sumX = 0.0;
        double sumX2 = 0.0;
        double n = (double) values.length;

        if( n == 1 )
        {
            return 0.0;
        }
        
        for( int i=0; i < values.length; ++i )
        {
            value = values[i];
            sumX += value;
            sumX2 += Math.pow(value,2);
        }
        
        return ( (n*sumX2) - Math.pow(sumX,2) ) / (n*(n-1.0));
    }

    /****************************************************
     * Returns the standard deviation of the given array
     * of doubles.<br>
     * s = sqrt( ( n*sum(x^2) + sum(x)^2 ) / n(n-1) )
     ***************************************************/
    public static double standardDeviation(double[] values)
    {
        return Math.pow(variance(values),0.5);
    }
    
    /**
     * Returns the R squared value<br>
     * RSQ = 1 - SSError/SSTotal
     */
    public static double rSquared(double[] actual, double[] predicted)
    {
        if( actual.length != predicted.length )
        {
            logger.error("Different number of actual and predicted values");
            return Double.NaN;
        }
        
        return 1.0 - (SSError(actual,predicted)/SSTotal(actual));
    }
    
    /**
     * Returns SSTotal = sum( (Yi-Y#)^2 )<br>
     * 
     * Yi - i'th value
     * Y# - mean of all values
     */
    public static double SSTotal(double[] values)
    {   
        double total = 0.0;
        double mean = mean(values);
        for( int i=0; i < values.length; ++i )
        {
            total += Math.pow(values[i]-mean,2.0);
        }
        return total;
    }
    
    /**
     * Returns SSError = sum( (Yi-Yi')^2 )<br>
     * Yi - i'th actual value<br>
     * Yi' - i'th predicted value
     */
    public static double SSError(double[] actual, double[] predicted)
    {
        if( actual.length != predicted.length )
        {
            logger.error("Different number of actual and predicted values");
            return Double.NaN;
        }
        
        double error = 0.0;
        for( int i=0; i < actual.length; ++i )
        {
            error += Math.pow(actual[i]-predicted[i],2.0);
        }
        return error;
    }
    
    /**
     * Returns the standard error = sqrt(SSError/(n-2))
     */
    public static double standardError(double[] actual, double[] predicted)
    {
        if( actual.length <= 2 )
        {
            logger.error("standard error called for two or less values");
            return Double.NaN;
        }
        return Math.pow(SSError(actual,predicted)/(actual.length-2),0.5);
    }
}
