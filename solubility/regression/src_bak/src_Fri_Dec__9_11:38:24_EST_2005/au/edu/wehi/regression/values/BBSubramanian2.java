/*
 * Created on: Sep 15, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import java.io.IOException;

/**
 * "Returns model (2) from the Subramanian paper
 * for prediction of logBB:
 * 
 * -0.0213 - 0.0143*TPSA + 0.241*XLogP
 * 
 * Computational models to predict blood-brain
 * barrier permeation and CNS activity
 * Govindan Subramanian and Douglas B. Kitchen
 * Journal of Computer-AIded Molecular Design
 * 1-20, 2003
 * 
 * @author eduthie
 */
public class BBSubramanian2 extends MoleculeValueDesc
{
    
    public String getDescription()
    {
        return "Returns model (2) from the Subramanian paper<br>" +
               "for prediction of logBB:<br><br>" +
               "-0.0213 - 0.0143*TPSA + 0.241*XLogP<br><br>" +
               "Computational models to predict blood-brain<br>" +
               "barrier permeation and CNS activity<br>" +
               "Govindan Subramanian and Douglas B. Kitchen<br>" +
               "Journal of Computer-Aided Molecular Design<br>" +
               "1-20, 2003";
    }

    /**
     * Return logBB as:
     * -0.0213 - 0.0143*TPSA + 0.241*XLogP
     */
    public double value(Molecule mol)
    {
        try
        {
            double tpsa = new TPSA().value(mol);
            double xlogp = new XLogP().value(mol);
            return -0.0213 - 0.0143*tpsa + 0.241*xlogp;
        }
        catch( IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
            return Double.NaN;
        }
        
    }

}
