/*
 * Created on: May 30, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import joelib2.molecule.Molecule;

/**
 * A Model of Molecule attributes.
 * 
 * @author eduthie
 */
public interface MoleculeModel
{
    /**
     * Trains the Model. If this method returns true the model
     * is then ready to be used via the value() method.
     */
    public boolean train(RegressionMoleculeVector rmv);
    /**
     * Returns the predicted value for the given Molecule. train()
     * must have returned true before this method is called.
     */
    public double value(Molecule mol);
}
