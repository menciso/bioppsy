/*
 * Created on: Sep 15, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 */
public class MolecularWeight extends MoleculeValueDesc
{
    public double value(Molecule mol)
    {
        return new joelib2.feature.types.MolecularWeight().getMolecularWeight(mol);
    }

}
