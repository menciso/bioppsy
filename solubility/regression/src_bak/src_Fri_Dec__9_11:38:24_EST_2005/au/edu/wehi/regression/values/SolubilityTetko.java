/*
 * Created on: Jul 28, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;

import au.edu.wehi.regression.Constants;
import au.edu.wehi.regression.Database;
import au.edu.wehi.regression.MLR;
import au.edu.wehi.regression.luco.LucoPLS;

import com.db4o.ObjectSet;

/**
 * Returns logS using MLR and the Tetko model:
 * Estimation of Aqueous Solubility of Chemical Compounds
 * Using E-State Indicies 
 * Igor V. Tetko et al.
 * J. Chem. Inf. Comput. Sci. 2001, 41, 1488-1493
 * 
 * @author eduthie
 */
public class SolubilityTetko extends MoleculeValueDesc
{
    private MLR mlr;
    
    public String getDescription()
    {
        return "Returns logS using the Tetko model and MLR for the regression<br>" +
               "Estimation of Aqueous Solubility of Chemical Compounds<br>" +
               "Using E-State Indicies<br>" +
               "Igor V. Tetko et al.<br>" +
               "J. Chem. Inf. Comput. Sci. 2001, 41, 1488-1493";
    }

    public SolubilityTetko()
    {
        super();
        int expectedSize = 1;
        Database database = Database.instance();
        if( database == null )
        {
            return;
        }
        mlr = new MLR(Constants.TETKO_MLR);
        ObjectSet objectSet = database.get(mlr);
        if( !(objectSet.size()==expectedSize) )
        {
            Logger.getLogger(getClass()).error("Unexpected number of TETKO_MLR objects returned");
            Logger.getLogger(getClass()).error("Recieved: " + objectSet.size() + " Expected: " + expectedSize);
            mlr = null;
        }
        mlr = (MLR) objectSet.next();
    }

    /**
     * Returns the predicted logS value
     */
    public double value(Molecule mol)
    {
        if( mlr == null )
        {
            return Double.NaN;
        }
        else
        {
            return mlr.value(mol);
        }
    }

}
