/*
 * CREATED: Feb 3, 2005
 * FILENAME: RegressionMoleculeVector.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression;

import java.io.*;
import au.edu.wehi.regression.rows.MoleculeRowDesc;
import joelib2.io.IOType;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.*;
import org.apache.log4j.Logger;
import joelib2.molecule.types.*;

/**
 * A RegressionMoleculeVector is simply a set of Molecule objects
 * which can be loaded from a file.
 * The methods getXMatrix() and getYMatrix() can be used to form
 * and use models based on this set.
 * Before using a RegressionMoleculeVector the MolecularDescribers
 * must be set using setIndependentDescriber() and 
 * setDependentDescriber().
 * 
 * @author eduthie
 */
public class RegressionMoleculeVector extends BasicMoleculeVector
{
    static Logger logger = Logger.getLogger(RegressionMoleculeVector.class);
    
    static final long serialVersionUID = 1;
    
    public static final String LOG_BB = "logBB";
    
    private MoleculeRowDesc xDesc, yDesc;
    private double[][] xM = null;
    private double[][] yM = null;

    public RegressionMoleculeVector()
    {
        super();
    }

    /**
     * @param ifs
     * @throws java.io.IOException
     */
    public RegressionMoleculeVector(InputStream ifs) 
        throws IOException
    {
        super(ifs);
    }

    /**
     * @param ifs
     * @param nToRead
     * @throws java.io.IOException
     */
    public RegressionMoleculeVector(InputStream ifs, int nToRead)
        throws IOException
    {
        super(ifs, nToRead);
    }

    /**
     * @param ifs
     * @param inType
     * @param outType
     * @throws java.io.IOException
     */
    public RegressionMoleculeVector(InputStream ifs, IOType inType, IOType outType)
        throws IOException
    {
        super(ifs, inType, outType);
    }

    /**
     * @param ifs
     * @param inType
     * @param outType
     * @param nToRead
     * @throws java.io.IOException
     * @throws joelib2.io.MoleculeIOException
     */
    public RegressionMoleculeVector(InputStream ifs, IOType inType, IOType outType, int nToRead) 
        throws IOException, MoleculeIOException
    {
        super(ifs, inType, outType, nToRead);
    }
    
    /*
     * Returns the X matrix of independent variables for this set of molecules.
     * Returns a double[r][c] where r is the number of molecules in this
     * set and c is the number of values returned by the independent
     * MoleculeRowDesc set in setIndependentDescriber().
     */
    public double[][] getXMatrix()
    {   
        if( xM == null )
        {
            if( !readXYMatricies() )
            {
                return null;
            }
        }
        return xM;
    }

    /*
     * Returns the Y matrix of independent variables for this set of molecules.
     * Returns a double[r][c] where r is the number of molecules in this
     * set and c is the number of values returned by the dependent 
     * MolecularDescriber set in setDependentDescriber().
     */
    public double[][] getYMatrix()
    {
        if( yM == null )
        {
            if( !readXYMatricies() )
            {
                return null;
            }
        }
        return yM;
    }
    
    /*
     * Iterates through this Vector of Molecule objects and extracts the
     * X and Y matricies into internal data arrays. Returns true on
     * success, false on failure.
     */
    private boolean readXYMatricies()
    {
        int i;
        int size = getSize();
        Molecule mol;
        double[] row;
        
        if( (xDesc == null) || (yDesc == null) )
        {
            logger.error("MoleculeRowDesc objects must be set before retrieving " +
                " X and Y matricies");
            return false;
        }
        
        xM = new double[size][];
        yM = new double[size][];
            
        for( i=0; i < size; ++i )
        {
            mol = getMol(i);
            row = xDesc.getRow(mol);
            xM[i] = row;
            row = yDesc.getRow(mol);
            yM[i] = row;
        }
        
        return true;
    }
    
    /**
     * Sets the MolecularDescriber used for each molecule to calculate
     * each row in the X matrix.
     */
    public void setIndependentDescriber(MoleculeRowDesc independent)
    {
        xDesc = independent;
    }
    
    public MoleculeRowDesc getIndependentDescriber()
    {
        return xDesc;
    }
    
    /**
     * Sets the MolecularDescriber used for each molecule to calculate
     * each row in the Y matrix.
     */
    public void setDependentDescriber(MoleculeRowDesc dependent)
    {
        yDesc = dependent;
    }
    
    public MoleculeRowDesc getDependentDescriber()
    {
        return yDesc;
    }
    
    /**
     * Loads values of logBB from the given InputStream to this set of molecules.
     * Each line in the given file should give the logBB value of a molecule.
     * The lines in the file correspond to the molecules in this list and must
     * be input in the same order.
     * 
     * @return true on success, false on failure.
     */
    public boolean loadBBData(InputStream in)
    {
        try
        {
            if( in == null )
            {
                logger.error("null InputStream passed to loadBBData");
                return false;
            }
            
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            
            String line = null;
            
            int i = 0;
            
            while( (line = br.readLine()) != null )
            {
                try
                {
                    Double value = new Double(line.trim());
                    
                    if( i < getSize() )
                    {
                        Molecule mol = getMol(i);
                        if( value.isNaN() )
                        {
                            logger.error("Invalid logBB value: " + line + " line: " + (i+1));
                        }
                        else
                        {
                            mol.addData(new BasicPairData(LOG_BB,value));
                        }
                    }
                    else
                    {
                        logger.error("Too many values in logBB input: " + i);
                        break;
                    }
                }
                catch(NumberFormatException nfe)
                {
                    logger.error("Invalid logBB value: " + line + " line: " + (i+1));
                    logger.error(nfe.getMessage());
                }
                
                ++i;
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        finally
        {
            try
            {
                in.close();
            }
            catch( IOException ioe )
            {
                logger.error(ioe.getMessage());
            }
        }
        
        return true;
    }
}
