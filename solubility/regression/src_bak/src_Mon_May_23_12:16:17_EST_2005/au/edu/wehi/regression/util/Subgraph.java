/*
 * Created on: May 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import java.util.*;

import org.apache.log4j.Logger;

/**
 * @author eduthie
 */
public class Subgraph extends TreeSet<Integer>
{
    public String toString()
    {
        String string = null;
        Iterator<Integer> i = iterator();
        while(i.hasNext())
        {
            if( string == null )
            {
                string = "";
            }
            else
            {
                string += ",";
            }
            string += i.next();
        }
        return string;
    }
}
