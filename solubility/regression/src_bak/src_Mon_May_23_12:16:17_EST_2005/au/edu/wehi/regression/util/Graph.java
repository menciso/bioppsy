/*
 * Created on: May 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import org.apache.log4j.Logger;
import java.util.*;
import joelib2.molecule.*;
import joelib2.util.iterator.*;

/**
 * The Graph class contains various static algorithms that
 * provide operations on the molecule graph.
 * 
 * @author eduthie
 */
public class Graph
{
    private static Logger logger = Logger.getLogger(Graph.class);

    public static Set<Subgraph> subgraphs(Molecule molecule, int m)
    {
        Molecule mol = (Molecule) molecule.clone();
        mol.deleteHydrogens();
        
        HashSet<Subgraph> lastSubgraphs = new HashSet<Subgraph>();
        HashSet<Subgraph> currentSubgraphs = null;
        
        if( m < 0 ) // done
        {
            return lastSubgraphs;
        }
        
        // we need to populate the lastSubgraphs List with subgraphs containing
        // all the single nodes in the molecule
        for( int i=1; i <= mol.getAtomsSize(); ++i )
        {
            Subgraph subgraph = new Subgraph();
            subgraph.add(new Integer(i));
            lastSubgraphs.add(subgraph);
        }
        
        if( m == 0 ) // done
        {
            return lastSubgraphs;
        }
        
        for( int k=1; k <= m; ++k )
        {
            currentSubgraphs = new HashSet<Subgraph>();
                
            // Here we start with a baseSubgraph that is one of the subgraphs from
            // the last iteration. Then we visit all the nodes of the base. For
            // each of these nodes we visit all adjacent nodes. If these adjacent 
            // nodes are not in the baseSubgraph, we add them to the baseSubgraph
            // to create a newSubgraph. If we don't already have this subgraph,
            // we add it.
            Iterator<Subgraph> lastSubgraphsI = lastSubgraphs.iterator();
            while( lastSubgraphsI.hasNext() )
            {
                Subgraph baseSubgraph = lastSubgraphsI.next();
                Iterator<Integer> atomIterator = baseSubgraph.iterator();
                while( atomIterator.hasNext() ) // visit all atoms in baseSubgraph
                {
                    Integer atomIndex = atomIterator.next();
                    Atom atom = mol.getAtom(atomIndex.intValue());
                    if( atom == null )
                    {
                        logger.error("Failed to retrieve atom: " + atomIndex.intValue());
                        return null;
                    }
                    else
                    {
                        BondIterator bi = atom.bondIterator();
                        while( bi.hasNext() ) // visit all adjacent atoms
                        {
                            Bond bond = bi.nextBond();
                            Atom adjacent = bond.getEnd();
                            if( adjacent.getIndex() == atom.getIndex() )
                            {
                                adjacent = bond.getBegin();
                            }
                            Integer adjacentIndex = new Integer(adjacent.getIndex());
                            
                            // if the adjacent node is not currently in the base subgraph, we
                            // create a newSubgraph and add it ht the currentSubgraphs
                            Subgraph newSubgraph = (Subgraph) baseSubgraph.clone();
                            if( newSubgraph.add(adjacentIndex) )
                            {
                                currentSubgraphs.add(newSubgraph);
                            }
                        }
                    }
                }
            }
            
            lastSubgraphs = currentSubgraphs;
        }
        
        return currentSubgraphs;
    }
}
