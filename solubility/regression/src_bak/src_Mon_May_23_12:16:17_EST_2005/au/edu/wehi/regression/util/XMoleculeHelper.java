/*
 * Created on: May 9, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import org.apache.log4j.Logger;
import joelib2.molecule.*;
import java.util.*;

/**
 * Performs calculations on Molecule objects via static methods. Called X to differentiate
 * from the JOELib2 MoleculeHelper class.<br><br>
 * 
 * <h3>References</h3>
 * <ol>
 *     <li>
 *         Charge Indexes. New Topological Descriptors.
 *         J.Galvez et co.
 *         J. Chem. Inf. Comput. Sci. 1994, 34, 520-525
 *     </li>
 * </ol>
 * 
 * @author eduthie
 */
public class XMoleculeHelper
{
    private static Logger logger = Logger.getLogger(XMoleculeHelper.class);

    /**
     * Calulates the topological distance matrix for the given molecule. Each i,j
     * value in the returned matrix represents the shortest distance between atom 
     * i and atom j in terms of number of edges in the molecular graph. Hydrogen
     * atoms are not counted. Warning, the hydrogens in the given molecule are
     * removed by this method.
     *  
     * @param mol - Molecule to calculate.
     * @return - a matrix of double[][].
     */
    public static double[][] distanceMatrix(Molecule mol)
    {
        Dijkstra dijkstra = new Dijkstra(mol);
        double[][] matrix = new double[mol.getAtomsSize()][];
        for( int i=0; i < mol.getAtomsSize(); ++i )
        {
            if(dijkstra.calculate(i+1))
            {
                matrix[i] = dijkstra.getDistances();
            }
            else
            {
                logger.error("Failed to calculate spanning tree for atom: " + i);
                matrix[i] = new double[mol.getAtomsSize()];
            }
        }
        return matrix;
    }
    
    /**
     * Returns the adjacency matrix for the given molecule. An element
     * x at row i and column j of the adjacency matrix is 1.0 of molecule
     * i is attached to molecule j, and 0.0 otherwise.
     * 
     * @param mol
     * @return
     */
    public static double[][] adjacencyMatrix(double[][] distanceMatrix)
    {
        if( distanceMatrix.length <= 0 )
        {
            return new double[0][0];
        }
        double[][] adj = new double[distanceMatrix.length][distanceMatrix[0].length];
        
        for( int i=0; i < distanceMatrix.length; ++i )
        {
            for( int j=0; j < distanceMatrix[i].length; ++j )
            {
                if( distanceMatrix[i][j] == 1.0 )
                {
                    adj[i][j] = 1.0;
                }
                else
                {
                    adj[i][j] = 0.0;
                }
            }
        }
        
        return adj;
    }
    
    /**
     * Returns the longest chain of non-Hydrogen atoms in the molecule.
     * 
     * @return ordered  array of indicies (joelib2 atom idx which start from 1)
     * of atoms in the longest chain. Returns null on error.
     */
    public static int[] getLongestChain(Molecule mol)
    {
        // remove hydrogens first so we have the correct number of atoms
        mol.deleteHydrogens();
        Dijkstra[] dijkstras = new Dijkstra[mol.getAtomsSize()];
        int longestBegin = -1;
        int longestEnd = -1;
        double maxDist = 0.0;
        
        for( int i=0; i < mol.getAtomsSize(); ++i)
        {
            dijkstras[i] = new Dijkstra(mol);
            if( !dijkstras[i].calculate(i+1) )
            {
                logger.error("Failed to calculate dijkstra from atom: " + i);
                return null;
            }
            for( int j=i+1; j < mol.getAtomsSize(); ++j )
            {
                if( dijkstras[i].getDistances()[j] > maxDist )
                {
                    longestBegin = i;
                    longestEnd = j;
                    maxDist = dijkstras[i].getDistances()[j];
                }
            }
        }
        
        return dijkstras[longestBegin].getShortestPath(longestEnd+1);
    }
  
    
}
