/*
 * Created on: Jun 20, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import java.io.IOException;
import au.edu.wehi.regression.values.LA;
import joelib2.feature.types.MolecularWeight;

/**
 * logBB calculated directly from MLR with three parameters;
 * TPSA , MW, and MW.
 * <br><br>
 * Reference:<br>
 * Development of Quantitive Structure-Property Relationship Models for Early 
 * ADME Evaluation in Drug Discovery. 2. Blood-Brain Barrier Penetration<br>
 * Ruifeng Liu, Hongmao Sun, and Sung-Sau So<br>
 * J. Chem. Inf. Comput. Sci., Vol. 41, No. 6, 2001
 * 
 * @author eduthie
 */
public class BBLiu4 extends MoleculeValueDesc
{
    /**
     * Returns logBB calculated as:<br>
     * 0.181 - 0.00876*MW + 0.296*LA - 0.00412*TPSA
     */
    public double value(Molecule mol)
    {
        try
        {
            LA la = new LA();
            TPSA tpsa = new TPSA();
            double mwValue = MolecularWeight.getMolecularWeight(mol);
            double laValue = la.value(mol);
            double tpsaValue = tpsa.value(mol);
            return 0.181 - (0.00876*mwValue) + (0.296*laValue) -
                (0.00412*tpsaValue);
        }
        catch(IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
            return Double.NaN;
        }
    }

}
