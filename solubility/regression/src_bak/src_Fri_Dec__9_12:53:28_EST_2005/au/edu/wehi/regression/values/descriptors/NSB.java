/*
 * Created on: Dec 9, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.descriptors;

import joelib2.molecule.Bond;
import joelib2.molecule.Molecule;
import joelib2.util.iterator.BondIterator;

import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * Returns the number of single bonds in the Molecule
 * 
 * @author eduthie
 */
public class NSB extends MoleculeValueDesc
{

    /**
     * Returns the number of single bonds in the Molecule
     */
    public double value(Molecule mol)
    {
        int ndb = 0;
        BondIterator bi = mol.bondIterator();
        while(bi.hasNext())
        {
            Bond bond = bi.nextBond();
            if( (bond.getBondOrder() == 1) && (!bond.isBondOrderAromatic()))
            {
                ++ndb;
            }
        }
        return ndb;
    }

}
