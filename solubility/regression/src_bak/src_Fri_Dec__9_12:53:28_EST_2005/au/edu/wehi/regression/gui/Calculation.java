/*
 * Created on: Jul 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import net.infonode.tabbedpanel.titledtab.*;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;
import au.edu.wehi.regression.*;
import java.io.*;
import java.util.Properties;
import joelib2.io.*;
import joelib2.molecule.*;
import java.util.Vector;
import javax.swing.table.*;
import java.lang.reflect.InvocationTargetException;
import com.db4o.*;

/**
 * @author eduthie
 */
public class Calculation extends TitledTab implements Runnable
{   
    private Logger logger;
    
    private File file;
    private String[] values;
    private JPanel content;
    private JLabel closeButton;
    private MoleculeValueDesc[] descriptors;
    private String[][] results;
    private Thread thread;
    private CalculationView parent;
    private String[] choices = null;
    private JTable table;
    
    private JPopupMenu popup;
    private PopupMouseListener popupMouseListener;
    
    public Calculation(File file, String[] values, CalculationView parent)
    {
        super(file.getName(), null, new JPanel(),null);
        this.parent = parent;
        logger = Logger.getLogger(getClass());
        content = (JPanel) getContentComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        this.file = file;
        this.values = values;
        closeButton = new JLabel("X");
        closeButton.setForeground(Color.RED);
        closeButton.addMouseListener(new CloseButtonMouseListener());
        setTitleComponent(closeButton);
    }
    
    public void createPopupMenu()
    {
        try
        {
            SwingUtilities.invokeAndWait(new CreatePopupMenu());
        }
        catch( InterruptedException ie )
        {
            logger.error(ie.getMessage());
        }
        catch( InvocationTargetException ite )
        {
            logger.error(ite.getMessage());
        }
    }
    
    public class CreatePopupMenu implements Runnable
    {
        public void run()
        {
            popup = new JPopupMenu();
            JMenuItem menuItem = new JMenuItem("Export to file");
            menuItem.addActionListener(new ExportActionListener());
            popup.add(menuItem);
            menuItem = new JMenuItem("Export to another application");
            menuItem.addActionListener(new ApplicationActionListener());
            popup.add(menuItem);
            popupMouseListener = new PopupMouseListener();
            addMouseListener(popupMouseListener);
            content.addMouseListener(popupMouseListener);
            if( table != null )
            {
                table.addMouseListener(popupMouseListener);
            }
            content.validate();
        }
    }
    
    public class PopupMouseListener extends MouseAdapter
    {
        public void mousePressed(MouseEvent e)
        {
            if (e.isPopupTrigger()) 
            {
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }
        
        public void mouseReleased(MouseEvent e)
        {
            if(popup.isVisible())
            {
                popup.setVisible(false);
            }
        }
    }
    
    public class ExportActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            exportToFile();
        }
    }
    
    public void exportToFile()
    {
        try
        {
            if( table != null )
            {
                int[] columns = table.getSelectedColumns();
                if( (columns == null) || (columns.length <= 0 ) )
                {
                    String message = "Please select data from the table to export";
                    logger.info(message);
                    JOptionPane.showMessageDialog(null,message,"No data selected",
                            JOptionPane.INFORMATION_MESSAGE);
                }
                else
                {
                    logger.info("NUMBER OF COLUMNS SELECTED: " + columns.length );
                    FileDialog fd = new FileDialog(new JFrame(),"Save File",FileDialog.SAVE);
                    fd.setVisible(true);
                    if( fd.getFile() != null )
                    {
                        File file = new File(fd.getFile());
                        if( file != null )
                        {
                            logger.info("WRITING DATA TO FILE");
                            PrintStream pw = new PrintStream(file);
                            
                            String[][] data = getSelectedData();
                            logger.info("NUMBER OF ROWS: " + data.length);
                            for( int i=0; i < data.length; ++i )
                            {
                                String line = "";
                                for( int j=0; j < data[i].length; ++j )
                                {
                                    if( j != 0 )
                                    {
                                        line += Constants.OUTPUT_SEPARATOR;
                                    }
                                    line += data[i][j];
                                }
                                logger.info("WRITING LINE: " + line);
                                pw.println(line);
                            }
                            
                            pw.close();
                            JOptionPane.showMessageDialog(this,"Data Saved to: " + file.getName(),
                                    "Message",JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                }
            }
        }
        catch( IOException ioe )
        {
            String message = "Write failure to file";
            logger.error(ioe.getMessage());
            JOptionPane.showMessageDialog(this,message,"IO Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public class PlotActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            plot();
        }
    }
    
    public void plot()
    {
        if( table != null )
        {
            int[] columns = table.getSelectedColumns();
            if( (columns == null) || (columns.length <= 0 ) )
            {
                String message = "Please select data from the table to chart";
                logger.info(message);
                JOptionPane.showMessageDialog(null,message,"No data selected",
                        JOptionPane.INFORMATION_MESSAGE);
            }
            else
            {
                String[][] data = getSelectedData();
                if( (data != null) && (data.length > 0) )
                {
                    new Chart(file.getName(),data,parent);
                }
            }
        }
    }
    
    public class ApplicationActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            application();
        }
    }
    
    public void application()
    {
        File file = null;
        
        try
        {
            if( table != null )
            {
                int[] columns = table.getSelectedColumns();
                if( (columns == null) || (columns.length <= 0 ) )
                {
                    String message = "Please select data from the table to export";
                    logger.info(message);
                    JOptionPane.showMessageDialog(null,message,"No data selected",
                            JOptionPane.INFORMATION_MESSAGE);
                }
                else
                {
                    logger.info("NUMBER OF COLUMNS SELECTED: " + columns.length );
                    file = new File(Constants.TEMP_FILENAME);
                    if( file != null )
                    {
                        logger.info("WRITING DATA TO FILE");
                        PrintStream pw = new PrintStream(file);
                        
                        String[][] data = getSelectedData();
                        logger.info("NUMBER OF ROWS: " + data.length);
                        for( int i=0; i < data.length; ++i )
                        {
                            String line = "";
                            for( int j=0; j < data[i].length; ++j )
                            {
                                if( j != 0 )
                                {
                                    line += Constants.OUTPUT_SEPARATOR;
                                }
                                line += data[i][j];
                            }
                            logger.info("WRITING LINE: " + line);
                            pw.println(line);
                        }
                        
                        pw.close();
                        logger.info("Data saved to file: " + file.getName());
                        
                        Command command = new Command();
                        ObjectSet objectSet = Database.instance().get(command);
                        if( (objectSet == null) || (objectSet.size() <= 0) )
                        {
                            command.setCommand(Constants.DEFAULT_COMMAND);
                        }
                        else
                        {
                            command = (Command) objectSet.next();
                        }
                        
                        String returnCommand = 
                            JOptionPane.showInputDialog(this,
                                "<html><p>Modify the output commmand (if required)." +
                                "<br><br> Use the key '" + Constants.FILENAME_KEY + "' to " +
                                "<br>indicate the name of the file.</p><html>",
                                command.getCommand());
                        if( !returnCommand.equals(command.getCommand()) )
                        {
                            command.setCommand(returnCommand);
                            Database.instance().set(command);
                        }
                        
                        String actualCommand = returnCommand.replaceAll(
                                Constants.FILENAME_KEY,
                                Constants.TEMP_FILENAME);
                        
                        Process p = Runtime.getRuntime().exec(actualCommand);
                    }
                    
                    
                }
            }
        }
        catch( IOException ioe )
        {
            if( file != null )
            {
                file.delete();
            }
            String message = "Write failure to file";
            logger.error(ioe.getMessage());
            JOptionPane.showMessageDialog(this,message,"IO Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public class Command
    {
        String command;
        
        public void setCommand(String command)
        {
            this.command = command;
        }
        
        public String getCommand()
        {
            return command;
        }
    }
    
    public boolean askUserToProceed()
    {
        if( !file.canRead() )
        {
            preCalcError("Unable to read file: " + file.getName());
            return false;
        }
        String fileInfoString =
            "<html>File: " + file.getName() + "<br>" +
            "Size: " + file.length() + " bytes<br><br>";
        
        if( getChoices() )
        {
            if( (choices != null) && (choices.length > 0) )
            {
                Object result = JOptionPane.showInputDialog(
                        this,
                        fileInfoString +
                        "Choose a property to include in display:",
                        "Input",
                        JOptionPane.INFORMATION_MESSAGE,
                        null,
                        choices,
                        choices[0]);
                if( result == null )
                {
                    return false;
                }
                // add the chosen property to the start of the 
                // values array
                String[] newValues = new String[values.length+1];
                newValues[0] = (String) result;
                System.arraycopy(values,0,newValues,1,values.length);
                values = newValues;
            }
            else
            {
                int result = JOptionPane.showConfirmDialog(
                        this,
                        fileInfoString +
                        " Perform Calculation?",
                        "Input",
                        JOptionPane.YES_NO_OPTION);
                if( result == JOptionPane.YES_NO_CANCEL_OPTION )
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * Sets the choices to all availiable ones based on the first 
     * molecule in the given file. Returns false if there are
     * no choices available.
     */
    public boolean getChoices()
    {
        Vector<String> finalChoices = new Vector<String>();
        
        // get the first molecule in the file to test 
        // the choices on
        Molecule molecule = getFirstMolecule(file);
        if( molecule == null )
        {
            preCalcError("Unable to load first molecule of file: " + file.getName());
            return false;
        }
        
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + Constants.PROPERTIES);
                return false;
            }
            
            // get database inputFilename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            String valuesString = typesProp.getProperty("PropertiesKeywords");
            if( valuesString == null )
            {
                logger.error("Failed to read properties: PropertiesKeywords");
                return false;
            }
            choices = valuesString.split(Constants.INPUT_SEPARATOR);
            
            // check all the choices to make sure they are availiable for
            // the first molecule in the file
            for( int i=0; i < choices.length; ++i )
            {
                choices[i] = choices[i].trim();
                MoleculeValueDesc desc = ValuesView.getDescriptorByName(choices[i]);
                if( desc != null )
                {
                    if( !(new Double(desc.value(molecule)).isNaN()) )
                    {
                        // choices is avaialible, add it to final list
                        finalChoices.add(choices[i]);
                    }
                }
            }
            
            // set the choices array to the final list
            choices = new String[finalChoices.size()];
            for(int i=0; i < finalChoices.size(); ++i )
            {
                choices[i] = finalChoices.elementAt(i);
            }
        }
        catch( IOException ioe )
        {
            preCalcError(ioe.getMessage());
            return false;
        }

        return true;
    }
    
    /**
     * Returns the first molecule in the given file, null on error
     */
    public Molecule getFirstMolecule(File theFile)
    {
        String filename = theFile.getAbsolutePath();
        
        try
        {
            if( !BasicIOTypeHolder.instance().canReadExtension(filename) )
            {
                logger.error("Invalid file extension: " + filename);
                return null;
            }
            BasicIOType type = BasicReader.checkGetInputType(filename);
            if( type == null )
            {
                logger.error("Invalid file extension: " + filename);
                return null;
            }
            InputStream in = new FileInputStream(filename);
            if( in == null )
            {
                logger.error("Unable to read file: " + filename);
                return null;
            }
            BasicReader basicReader = new BasicReader(in,type);
            BasicConformerMolecule molecule = new BasicConformerMolecule();
            if( !basicReader.readNext(molecule) )
            {
                return null;
            }
            return molecule;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return null;
        }
        catch( MoleculeIOException mioe )
        {
            logger.error(mioe.getMessage());
            return null;
        }
    }
    
    public void preCalcError(String message)
    {
        logger.error(message);
        JOptionPane.showMessageDialog(this,message,"Error",JOptionPane.ERROR_MESSAGE);
    }
    
    public class CloseButtonMouseListener extends MouseAdapter
    {
        public void mousePressed(MouseEvent e)
        {
            closeButton.setForeground(Color.BLUE);
        }
        
        public void mouseClicked(MouseEvent e)
        {
            close();
        }
    }
    
    public void runCalc()
    {
        thread = new Thread(this,file.getName());
        thread.start();
    }
    
    public void run()
    {
        try
        {
            displayProgress();
            if( !prepareDescriptors() )
            {
                errorMessage("Failed to prepare descriptors");
                return;
            }
            if( !performCalculation() )
            {
                errorMessage("Failed to calculate values. ");
                return;
            }
            displayTable();
            createPopupMenu();
        }
        catch(Exception e)
        {
            errorMessage("Unknown Error: " + e.getMessage());
            e.printStackTrace();
            return;
        }
    }
    
    public void errorMessage(String message)
    {
        try
        {
            SwingUtilities.invokeAndWait(new ErrorMessage(message));
        }
        catch( InterruptedException ie )
        {
            logger.error(ie.getMessage());
        }
        catch( InvocationTargetException ite )
        {
            logger.error(ite.getMessage());
        }
    }
    
    public class ErrorMessage implements Runnable
    {
        private String message;
        
        public ErrorMessage(String message)
        {
            this.message = message;
        }
        
        public void run()
        {
            content.removeAll();
            String text = message + 
                    " See console output for more information";
            JProgressBar progressBar = new JProgressBar();
            progressBar.setForeground(Color.RED);
            progressBar.setString(text);
            progressBar.setStringPainted(true);
            progressBar.setIndeterminate(false);
            progressBar.setAlignmentX(Component.LEFT_ALIGNMENT);
            content.add(progressBar);
            content.validate();
        }
    }
    
    /**
     * Loads all the descriptors chosen by the user. Returns true
     * if all descriptors have been created successfully and stored
     * in the descriptors array, false otherwise.
     */
    public boolean prepareDescriptors()
    {
        descriptors = new MoleculeValueDesc[values.length];
        Class theClass = null;
        
        for( int i=0; i < values.length; ++i )
        {
            try
            {
                theClass = Class.forName(Constants.VALUES_PREFIX + "." + values[i]);
            }
            catch( ClassNotFoundException cnfe )
            {
                try
                {
                    // second time try the LUCO_PREFIX
                    theClass = Class.forName(Constants.LUCO_PREFIX + "." + values[i]);
                }
                catch( ClassNotFoundException cnfe2 )
                {
                    logger.error("Unable to locate the value: " + values[i]);
                    logger.error("Please remove from the list to perform calculation");
                    return false; 
                }
            }
            try
            {
                descriptors[i] = (MoleculeValueDesc) theClass.newInstance();
            }
            catch( IllegalAccessException iae )
            {
                logger.error("Illegal Access to class: " + theClass.getName());
                logger.error(iae.getMessage());
                logger.error("Please remove the value: " + values[i] + 
                        " from the list to perform calculation");
                return false;
            }
            catch( InstantiationException ie )
            {
                logger.error("Unable to Instantiate class: " + theClass.getName());
                logger.error(ie.getMessage());
                logger.error("Please remove the value: " + values[i] + 
                    " from the list to perform calculation");
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Calculates the values and stores them as Strings
     * in the results matrix
     */
    public boolean performCalculation()
    {       
        try
        {
            RegressionMoleculeVector rmv = getRMV(file.getAbsolutePath());
            if( rmv == null )
            {
                logger.error("Failed to read input file: " + file.getAbsolutePath());
                return false;
            }
            
            // initialise the output matrix
            results = new String[rmv.getSize()+1][descriptors.length];
            
            int index = 0;
            
            // add the headings
            for( int i=0; i < values.length; ++i )
            {
                results[index][i] = values[i];
            }
            
            // do the calculations
            for( int i=0; i < rmv.getSize(); ++i )
            {
                ++index;
                Molecule mol = rmv.getMol(i);
                for( int j=0; j < descriptors.length; ++j )
                {
                    double value = descriptors[j].value(mol);
                    results[index][j] = round(value,Constants.DP_OUTPUT);
                }
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        
        return true;
    }
    
    public RegressionMoleculeVector getRMV(String filename)
        throws IOException
    {
        if( !BasicIOTypeHolder.instance().canReadExtension(filename) )
        {
            logger.error("Invalid file extension: " + filename);
            return null;
        }
        BasicIOType type = BasicReader.checkGetInputType(filename);
        if( type == null )
        {
            logger.error("Invalid file extension: " + filename);
            return null;
        }
        InputStream in = new FileInputStream(filename);
        if( in == null )
        {
            logger.error("Unable to read file: " + filename);
            return null;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        return rmv;
    }
    
    /**
     * Rounds the given double to the given number of decimal places and
     * returns its string representation
     */
    public String round(double value, int dp)
    {
        String valueString = new Double(value).toString();
        int dpIndex = valueString.indexOf('.');
        if( (dpIndex+dp+1) >= valueString.length() )
        {
            return valueString;
        }
        else
        {
            return valueString.substring(0,dpIndex+dp+1);
        }
    }
    
    public void displayTable()
    {
        try
        {
            SwingUtilities.invokeAndWait(new DisplayTable());
        }
        catch( InterruptedException ie )
        {
            logger.error(ie.getMessage());
        }
        catch( InvocationTargetException ite )
        {
            logger.error(ite.getMessage());
        }
    }
    
    public class DisplayTable implements Runnable
    {
        public void run()
        {
            String[] columnNames = results[0];
            String[][] rowData = new String[results.length-1][results[0].length];
            for( int i=1; i < results.length; ++i )
            {
                rowData[i-1] = results[i];
            }
            table = new JTable(rowData,columnNames);
            table.setColumnSelectionAllowed(true);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            JScrollPane scrollPane = new JScrollPane(table);
            scrollPane.setHorizontalScrollBarPolicy(
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            content.removeAll();
            scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
            content.add(scrollPane);
            content.validate();
        }
    }
    
    public void displayProgress()
    {
        try
        {
            SwingUtilities.invokeAndWait(new DisplayProgress());
        }
        catch( InterruptedException ie )
        {
            logger.error(ie.getMessage());
        }
        catch( InvocationTargetException ite )
        {
            logger.error(ite.getMessage());
        }
    }
    
    public class DisplayProgress implements Runnable
    {
        public void run()
        {
            content.removeAll();
            JProgressBar progressBar = new JProgressBar();
            progressBar.setString("Calculation in Progress");
            progressBar.setStringPainted(true);
            progressBar.setIndeterminate(true);
            progressBar.setAlignmentX(Component.LEFT_ALIGNMENT);
            content.add(progressBar);
            content.validate();
        }
    }
    
    
    /**
     * Terminates the thread if it is running and removes the tab
     * from the CalculationView
     */
    public void close()
    {
        if( thread != null )
        {
            thread.stop(); // naughty
        }
        
        parent.removeTab(this);
    }
    
    /**
     * Retrieves the current selected data. The first row is the column
     * names
     */
    public String[][] getSelectedData()
    {
        TableModel model = table.getModel();
        int[] rows = table.getSelectedRows();
        int[] columns = table.getSelectedColumns();
        String[][] data = new String[rows.length+1][columns.length];
        
        // set column names as first row
        for( int j=0; j < columns.length; ++j )
        {
            data[0][j] = table.getColumnName(columns[j]);
        }
        
        int increment = 1;
        
        // set the data as the rest
        for( int i=0; i < rows.length; ++i)
        {
            for( int j=0; j < columns.length; ++j)
            {
                data[i+increment][j] = (String) model.getValueAt(rows[i],columns[j]);
            }
        }
        
        return data;
    }

}
