/*
 * Created on: Dec 8, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.descriptors;

import joelib2.feature.types.ElectrotopologicalState;
import joelib2.feature.result.AtomDynamicResult;
import joelib2.feature.FeatureException;
import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * Returns EAVE2, the mean of the Electrotopological State indicies
 * for each atom in the molecule (E-State).
 * The E-State index is defined in:<br>
 * Lowell H Hall 
 * J. Chem. Inf. Comput. Sci. 1995, 35, 1039-1045
 * 
 * @author eduthie
 */
public class EAVE2 extends MoleculeValueDesc
{

    /**
     * Returns the mean of the Electrotopological State indicies
     * for each atom in the molecule (E-State).
     */
    public double value(Molecule mol)
    {
        try
        {
        double ave = 0; // average eState
        
        AtomDynamicResult result = (AtomDynamicResult) new ElectrotopologicalState().calculate(mol);

        // Iterate through all resulting atoms
        // note that atomic indecies start at 1
        for( int i=1; i <= result.getSize(); ++i )
        {
            ave += result.getDoubleValue(i);
        }

        return ave/result.getSize();
        }
        catch( FeatureException fe )
        {
            Logger.getLogger(getClass()).error(fe.getMessage());
            return Double.NaN;
        }
    }

}
