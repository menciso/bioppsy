/*
 * Created on: Sep 16, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.qsar.DescriptorValue;
import org.openscience.cdk.qsar.CPSADescriptor;
import org.openscience.cdk.qsar.result.DoubleArrayResult;

import au.edu.wehi.regression.util.CDKConverter;

/**
 * The Stanton WNSA-3 descriptor
 * 
 * Defined as:
 * 
 * PNSA3 * total molecular surface area / 1000
 * 
 * PNSA3 = sum of ( solvent accessible surface area *
 *                  partial charge )
 * where the sum is over all negatively charged atoms
 * 
 * @author eduthie
 */
public class WNSA3 extends MoleculeValueDesc
{
    public String getDescription()
    {
        return "The Stanton WNSA-3 descriptor<br><br>" +
               "Defined as:<br><br>" +
               "PNSA3 * total molecular surface area / 1000 <br><br>" +
               "PNSA3 = sum of ( solvent accessible surface area * " +
               "partial charge )<br>" +
               "where the sum is over all negatively charged atoms<br><br>" +
               "Requires 3D data";
    }
    

    /**
     * Returns the WNSA3
     */
    public double value(Molecule mol)
    {
        try
        {
            org.openscience.cdk.Molecule cdkMol = 
                CDKConverter.convertToCDK(mol);
            CPSADescriptor cpsa = new CPSADescriptor();
            DescriptorValue value = cpsa.calculate(cdkMol);
            DoubleArrayResult result = (DoubleArrayResult) value.getValue();
            return result.get(20);
        }
        catch( CDKException cdke)
        {
            Logger.getLogger(getClass()).error(cdke.getMessage());
            return Double.NaN;
        }
    }

}
