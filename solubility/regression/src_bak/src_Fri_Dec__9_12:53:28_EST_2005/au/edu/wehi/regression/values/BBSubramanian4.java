/*
 * Created on: Sep 16, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import org.openscience.cdk.qsar.CPSADescriptor;
import org.openscience.cdk.qsar.DescriptorValue;
import org.openscience.cdk.qsar.result.DoubleArrayResult;
import org.openscience.cdk.exception.CDKException;

import au.edu.wehi.regression.util.CDKConverter;

/**
 * Returns model (4) from the Subramanian paper
 * for prediction of logBB
 * 
 * logBB = 0.182 - 0.0410*S_dO - 0.0896*rotBonds
 * - 0.0501*rpcs + 3.707*fpsa - 0.0645*S_sOH
 * - 0.276*S_dsN.value(mol) + 0.000985*thsa
 * 
 * Computational models to predict blood-brain
 * barrier permeation and CNS activity
 * Govindan Subramanian and Douglas B. Kitchen
 * Journal of Computer-Aided Molecular Design
 * 1-20, 2003
 * 
 * @author eduthie
 */
public class BBSubramanian4 extends MoleculeValueDesc
{
    
    public String getDescription()
    {
        return 
            "Returns model (2) from the Subramanian paper<br>" +
            "for prediction of logBB. Requires 3D data<br><br>" +
            "logBB = 0.182 - 0.0410*S_dO - 0.0896*rotBonds<br>" +
            "- 0.0501*rpcs + 3.707*fpsa - 0.0645*S_sOH<br>" +
            "- 0.276*S_dsN.value(mol) + 0.000985*thsa<br><br>" +
            "Computational models to predict blood-brain<br>" +
            "barrier permeation and CNS activity<br>" +
            "Govindan Subramanian and Douglas B. Kitchen<br>" +
            "Journal of Computer-AIded Molecular Design<br>" +
            "1-20, 2003";
    }

    /**
     * Returns Subramanian model number 4
     */
    public double value(Molecule mol)
    {
        try
        {
            SmartsMatchCount S_dO = new SmartsMatchCount();
            S_dO.addPattern("[OX1H0]");
            SmartsMatchCount S_sOH = new SmartsMatchCount();
            S_sOH.addPattern("[OX2H1]");
            double rotBonds = new RotatableBonds().value(mol);
            SmartsMatchCount S_dsN = new SmartsMatchCount();
            S_dsN.addPattern("[NX2H0]");
            
            org.openscience.cdk.Molecule cdkMol = 
                CDKConverter.convertToCDK(mol);
            CPSADescriptor cpsa = new CPSADescriptor();
            DescriptorValue value = cpsa.calculate(cdkMol);
            DoubleArrayResult result = (DoubleArrayResult) value.getValue();
            
            double rpcs = result.get(23);
            double fpsa = result.get(11);
            double thsa = result.get(25); // could be tpsa 26 ?
            
            return
            0.182 - 0.0410*S_dO.value(mol) - 0.0896*rotBonds
            - 0.0501*rpcs + 3.707*fpsa - 0.0645*S_sOH.value(mol)
            - 0.276*S_dsN.value(mol) + 0.000985*thsa;
        }
        catch( CDKException cdke )
        {
            Logger.getLogger(getClass()).error(cdke.getMessage());
            return Double.NaN;
        }
    }

}
