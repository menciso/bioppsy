/*
 * Created on: Sep 15, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.qsar.DescriptorValue;
import org.openscience.cdk.qsar.MomentOfInertiaDescriptor;
import org.openscience.cdk.qsar.result.DoubleArrayResult;

import au.edu.wehi.regression.util.CDKConverter;

import java.io.IOException;

/**
 * Radius of Gyration is defined as the value k such that:
 * I = mk^2 where m is the mass and I is the moment of
 * inertia. Requires 3D data.
 * 
 * @author eduthie
 */
public class RadiusOfGyration extends MoleculeValueDesc
{
    public String getDescription()
    {
        return "Radius of Gyration is the k such that<br>" +
               "I = m*(k^2) where I is the moment of inertia<br>" +
               "and m is the mass<br>" +
               "Requires 3D data";
    }
    
    /**
     * Returns the radius of gyration k
     */
    public double value(Molecule mol)
    {
        try
        {
            org.openscience.cdk.Molecule cdkMol = 
                CDKConverter.convertToCDK(mol);
            MomentOfInertiaDescriptor moi = new MomentOfInertiaDescriptor();
            DescriptorValue value = moi.calculate(cdkMol);
            DoubleArrayResult result = (DoubleArrayResult) value.getValue();
            return result.get(6);
        }
        catch( CDKException cdke)
        {
            Logger.getLogger(getClass()).error(cdke.getMessage());
            return Double.NaN;
        }
    }

}
