/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import java.io.*;
import joelib2.io.BasicIOType;
import joelib2.io.BasicIOTypeHolder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.luco.LucoPLS;
import au.edu.wehi.regression.rows.HouRow;
import au.edu.wehi.regression.rows.SolubilityRow;
import au.edu.wehi.regression.rows.TetkoRow;
import au.edu.wehi.regression.rows.McElroyRow;
import com.db4o.*;
import java.util.Properties;

/**
 * Trains all the models and adds them to the database to allow them to be
 * used.
 * 
 * @author eduthie
 */
public class CreateDatabase
{
    private String dbFilename;
    private String solTrainFilename = "/au/edu/wehi/regression/solubilityTraining.sdf";
    private String fileType = "SDF";
    
    public static Logger logger = Logger.getLogger(CreateDatabase.class);
    
    public CreateDatabase()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + Constants.PROPERTIES);
                System.exit(0);
            }
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            dbFilename = typesProp.getProperty(Constants.DATABASE_FILE_KEY);
            if( dbFilename == null )
            {
                logger.error("Unable to get property: " + Constants.DATABASE_FILE_KEY +
                        " from file: " + Constants.PROPERTIES);
                System.exit(0);
            }
            solTrainFilename = typesProp.getProperty("SolubilityTrainingFilename");
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            System.exit(0);
        }
    }
    
    public static void main(String[] args)
    {
        PropertyConfigurator.configure(
               CreateDatabase.class.getResource("regression.log4j"));
        CreateDatabase cd = new CreateDatabase();
        cd.createDatabase();
    }
    
    public void createDatabase()
    {   
        logger.info("Creating Database in file: " + dbFilename);
        ObjectContainer db = Db4o.openFile(dbFilename);
        if( db == null )
        {
            logger.error("Unable to open database: " + dbFilename);
        }
        try
        {
            deleteDatabase(db);
            configure();
            if( !createHou(db) )
            {
                logger.error("FAILED TO CREATE HOU");
            }
            /*
            if( !createTetko(db) )
            {
                logger.error("FAILED TO CREATE TETKO");
            }
            */
            if( !createLuco(db) )
            {
                logger.error("FAILED TO CREATE LUCO");
            }
            /*
            if( !createMcElroy(db) )
            {
                logger.error("FAILED TO CREATE MCELROY");
            }
            */
            logger.info("FINISHED CREATING DATABASE");
              
        }
        catch(IOException ioe)
        {
            logger.error(ioe.getMessage());
        }
        finally
        {
            logger.info("ABOUT TO CLOSE CONNECTION WITH DATABASE");
            db.close();
            logger.info("HAVE CLOSED CONNECTION WITH DATABASE");
        }
    }
    
    public void deleteDatabase(ObjectContainer db)
    {
        logger.info("ABOUT TO DELETE OLD DATABASE");
        MLR mlr = new MLR();
        ObjectSet os = db.get(mlr);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
        MoleculePLS pls = new MoleculePLS();
        os = db.get(pls);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
        Test test = new Test();
        os = db.get(test);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
        db.commit();
        logger.info("OLD DATABASE DELETED");
    }
    
    public void configure()
    {
        Db4o.configure().objectClass("au.edu.wehi.regression.Test").cascadeOnDelete(true);
        Db4o.configure().objectClass("au.edu.wehi.regression.MLR").cascadeOnDelete(true);
        Db4o.configure().objectClass("au.edu.wehi.regression.MoleculePLS").cascadeOnDelete(true);
    }
    
    public boolean createHou(ObjectContainer db)
        throws IOException
    {
        MLR mlr = new MLR(Constants.HOU_MLR);
        InputStream in = getClass().getResourceAsStream(solTrainFilename);
        if( in == null )
        {
            logger.error("Unable to find resource: " + solTrainFilename);
            return false;
        }
        BasicIOType type = BasicIOTypeHolder.instance().getIOType(fileType);
        if( type == null )
        {
            logger.error("Unable to get type: " + fileType);
            return false;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        HouRow houRow = new HouRow();
        houRow.init();
        rmv.setIndependentDescriber(houRow);
        rmv.setDependentDescriber(new SolubilityRow());
        if( mlr.train(rmv) )
        {
            logger.info("Hou MLR trained");
            logger.info("HOU REGRESSION RSQ: " + mlr.getRsq());
            logger.info("HOU REGRESSION SDDEV: " + mlr.getSdDev());
            db.set(mlr);
            db.commit();
        }
        else
        {
            logger.error("Unable to train MLR");
        }
        /*        
        MoleculePLS pls = new MoleculePLS(Constants.HOU_PLS);
        if( pls.train(rmv) )
        {
            logger.info("Hou PLS trained");
            db.set(pls);
            logger.info("ABOUT TO COMMIT HOU PLS");
            db.commit();
            logger.info("FINISHED COMMITING HOU PLS");
        }
        else
        {
            logger.error("Unable to train Hou PLS");
        }
        */
        
        db.set(new Test());
        db.commit();
        
        return true;
    }
    
    public boolean createTetko(ObjectContainer db)
        throws IOException
    {
        MLR mlr = new MLR(Constants.TETKO_MLR);
        InputStream in = getClass().getResourceAsStream(solTrainFilename);
        if( in == null )
        {
            logger.error("Unable to find resource: " + solTrainFilename);
            return false;
        }
        BasicIOType type = BasicIOTypeHolder.instance().getIOType(fileType);
        if( type == null )
        {
            logger.error("Unable to get type: " + fileType);
            return false;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        TetkoRow tetkoRow = new TetkoRow();
        tetkoRow.init();
        rmv.setIndependentDescriber(tetkoRow);
        rmv.setDependentDescriber(new SolubilityRow());
        if( mlr.train(rmv) )
        {
            logger.info("Tetko MLR trained");
            logger.info("TETKO REGRESSION RSQ: " + mlr.getRsq());
            logger.info("TETKO REGRESSION SDDEV: " + mlr.getSdDev());
            db.set(mlr);
            db.commit();
        }
        else
        {
            logger.error("Unable to train MLR");
        }
        
        db.set(new Test());
        db.commit();
        
        return true;
    }
    
    public boolean createLuco(ObjectContainer db)
        throws IOException
    {   
        LucoPLS pls = new LucoPLS(Constants.LUCO_PLS);
        if( pls.train() )
        {
            logger.info("LUCO RSQ: " + pls.rSquared());
            logger.info("LUCO STANDARD ERROR: " + pls.standardError());
            db.set(pls);
            logger.info("ABOUT TO COMMIT LUCO PLS");
            db.commit();
            logger.info("COMMITED LUCO PLS");
        }
        else
        {
            logger.error("Unable to train Luco PLS");
        }
        
        return true;
    }
    
    public boolean createMcElroy(ObjectContainer db)
        throws IOException
    {
        MLR mlr = new MLR(Constants.MCELROY_MLR);
        InputStream in = getClass().getResourceAsStream(solTrainFilename);
        if( in == null )
        {
            logger.error("Unable to find resource: " + solTrainFilename);
            return false;
        }
        BasicIOType type = BasicIOTypeHolder.instance().getIOType(fileType);
        if( type == null )
        {
            logger.error("Unable to get type: " + fileType);
            return false;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        McElroyRow mcElroyRow = new McElroyRow();
        rmv.setIndependentDescriber(mcElroyRow);
        rmv.setDependentDescriber(new SolubilityRow());
        if( mlr.train(rmv) )
        {
            logger.info("McElroy MLR trained");
            logger.info("MCELROY REGRESSION RSQ: " + mlr.getRsq());
            logger.info("MCELROY REGRESSION SDDEV: " + mlr.getSdDev());
            db.set(mlr);
            db.commit();
        }
        else
        {
            logger.error("Unable to train MLR");
        }
        
        db.set(new Test());
        db.commit();
        
        return true;
    }
}
