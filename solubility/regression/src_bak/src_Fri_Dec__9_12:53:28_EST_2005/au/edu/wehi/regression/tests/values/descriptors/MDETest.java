/*
 * Created on: Dec 8, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values.descriptors;

import java.io.IOException;
import java.io.InputStream;

import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.BasicConformerMolecule;
import joelib2.molecule.Molecule;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.descriptors.MDE;

/**
 * @author eduthie
 */
public class MDETest extends TestCase
{
    private Molecule hexane;

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(MDETest.class);
    }
    
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            String name = "2,3,4,5-tetramethylhexane.mol2";
            InputStream in = getClass().getResourceAsStream(name);
            hexane = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(hexane) )
            {
                fail("Failed to read: " + name);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    public void testValue()
    {
        MDE mde = new MDE(1,4);
        double result = mde.value(hexane);
        assertFalse(Double.isNaN(result));
        mde = new MDE(2,4);
        result = mde.value(hexane);
        assertFalse(Double.isNaN(result));
        mde = new MDE(3,4);
        result = mde.value(hexane);
        assertFalse(Double.isNaN(result));
    }

}
