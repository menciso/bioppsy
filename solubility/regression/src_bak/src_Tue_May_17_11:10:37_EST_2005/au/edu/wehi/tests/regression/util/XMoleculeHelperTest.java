/*
 * Created on: May 10, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.tests.regression.util;

import java.io.InputStream;

import joelib2.io.*;
import joelib2.molecule.BasicConformerMolecule;
import joelib2.molecule.Molecule;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.tests.regression.AllTests;
import au.edu.wehi.regression.util.XMoleculeHelper;
import Jama.Matrix;
import java.util.*;

import java.io.IOException;

/**
 * @author eduthie
 */
public class XMoleculeHelperTest extends TestCase
{
    private static Logger logger = Logger.getLogger(XMoleculeHelperTest.class);
    
    private Molecule mol;
    private Molecule ethanol;
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(XMoleculeHelperTest.class);
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    protected void setUp()
    {
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            InputStream in = getClass().getResourceAsStream("2,4,4-trimethylhexane.mol2");
            mol = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(mol) )
            {
                fail("Failed to read mol");
            }
            in = getClass().getResourceAsStream("ethanol.mol2");
            ethanol = new BasicConformerMolecule();
            reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(ethanol) )
            {
                fail("Failed to read ethanol");
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /**
     * Constructor for XMoleculeHelperTest.
     * @param arg0
     */
    public XMoleculeHelperTest(String arg0)
    {
        super(arg0);
    }

    public void testDistanceMatrix()
    {
        double[][] distanceMatrix = XMoleculeHelper.distanceMatrix(mol);
        Matrix M = new Matrix(distanceMatrix);
        //M.print(1,1);
        double[][] expected =
        {
                {0.0,1.0,2.0,2.0,3.0,2.0,3.0,4.0,4.0},
                {1.0,0.0,1.0,1.0,2.0,1.0,2.0,3.0,3.0},
                {2.0,1.0,0.0,2.0,3.0,2.0,3.0,4.0,4.0},
                {2.0,1.0,2.0,0.0,1.0,2.0,3.0,4.0,4.0},
                {3.0,2.0,3.0,1.0,0.0,3.0,4.0,5.0,5.0},
                {2.0,1.0,2.0,2.0,3.0,0.0,1.0,2.0,2.0},
                {3.0,2.0,3.0,3.0,4.0,1.0,0.0,1.0,1.0},
                {4.0,3.0,4.0,4.0,5.0,2.0,1.0,0.0,2.0},
                {4.0,3.0,4.0,4.0,5.0,2.0,1.0,2.0,0.0}
        };
        
        for( int i=0; i < distanceMatrix.length; ++i )
        {
            for( int j=0; j < distanceMatrix[i].length; ++j )
            {
                if( expected[i][j] != distanceMatrix[i][j])
                {
                    logger.info("i: " + i + " j : " + j);
                }
                assertEquals(expected[i][j],distanceMatrix[i][j]);
            }
        }
    }
    
    public void testAdjacencyMatrix()
    {
        double[][] distanceMatrix = XMoleculeHelper.distanceMatrix(mol);
        double[][] M = XMoleculeHelper.adjacencyMatrix(distanceMatrix);
        double[][] expected =
        {
                {0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},
                {1.0,0.0,1.0,1.0,0.0,1.0,0.0,0.0,0.0},
                {0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},
                {0.0,1.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0},
                {0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0},
                {0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0},
                {0.0,0.0,0.0,0.0,0.0,1.0,0.0,1.0,1.0},
                {0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0},
                {0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0}
        };
        for( int i=0; i < M.length; ++i )
        {
            for( int j=0; j < M[i].length; ++j )
            {
                if( M[i][j] != expected[i][j])
                {
                    fail("Error in adjacency matrix i: " + i + " j: " + j);
                }
            }
        }
    }
}
