/*
 * CREATED: Feb 7, 2005
 * FILENAME: RegressionMoleculeVectorTest.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.tests.regression;

import java.io.InputStream;
import joelib2.io.*;
import junit.framework.TestCase;
import au.edu.wehi.regression.*;
import au.edu.wehi.regression.rows.*;
import java.io.*;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import joelib2.molecule.*;
import joelib2.molecule.types.*;

/**
 * @author eduthie
 *
 */
public class RegressionMoleculeVectorTest extends TestCase
{
    
    public static Logger logger = Logger.getLogger(RegressionMoleculeVectorTest.class);

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(RegressionMoleculeVectorTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for RegressionMoleculeVectorTest.
     * @param arg0
     */
    public RegressionMoleculeVectorTest(String arg0)
    {
        super(arg0);
    }

    public void testAll()
    {
        try
        {
            /*
             * Set up the independent variable MoleculeRowDesc
             */
	        MoleculeRowDesc mrd = new MoleculeRowDesc();
	        InputStream in = getClass().getResourceAsStream(
	        "test.smarts");
		    if( in.available() == 0 )
		    {
		        fail("Did not read tests.smarts correctly");
		    }
		    if( !mrd.addSmarts(in) )
		    {
		        fail("Failed to add smarts from file: " + "test.smarts");
		    }
		    
		    /*
		     * Set up the dependent variable MoleculeRowDesc
		     */
	        SolubilityRow sr = new SolubilityRow();
	        
	        /*
	         * Create the RegressionMoleculeVector and get the X and 
	         * Y matricies
	         */
            InputStream molStream  = getClass().getResourceAsStream(
                "test.sdf");
            BasicIOType type = BasicIOTypeHolder.instance().getIOType("SDF");
            RegressionMoleculeVector rmv = new RegressionMoleculeVector(
                molStream, type, type);
            rmv.setDependentDescriber(sr);
            rmv.setIndependentDescriber(mrd);
            
            double[][] xM = rmv.getXMatrix();
            double[][] yM = rmv.getYMatrix();
            
            ResourceBundle rb = ResourceBundle.getBundle("test");
            int n = ( new Integer(rb.getString("test.sdf.n")) ).intValue();
            int p = ( new Integer(rb.getString("test.smarts.n")) ).intValue();

            assertEquals(n,xM.length);
            assertEquals(n,yM.length);
            
            if( n > 0 )
            {
                assertEquals(p,xM[0].length);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }

    }
    
    public void testLoadBBData()
    {
        try
        {
            InputStream molStream  = getClass().getResourceAsStream(
            "test.sdf");
            BasicIOType type = BasicIOTypeHolder.instance().getIOType("SDF");
            RegressionMoleculeVector rmv = new RegressionMoleculeVector(
                molStream, type, type);
            InputStream in = getClass().getResourceAsStream("test.logBB");
            if( !rmv.loadBBData(in) )
            {
                fail("Failed to load logBB data");
            }
            Molecule mol = rmv.getMol(0);
            PairData data = mol.getData(RegressionMoleculeVector.LOG_BB);
            Double value = (Double) data.getKeyValue();
            double expected = 1.34;
            if( value.doubleValue() != expected )
            {
                fail("Incorrect logBB value read in: " + value.doubleValue() + " expected: " +
                        expected);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
