/*
 * Created on: May 10, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.tests.regression.values;

import java.io.IOException;
import java.io.InputStream;
import joelib2.molecule.*;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.BasicConformerMolecule;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.values.Gk;
import au.edu.wehi.regression.util.*;
import Jama.Matrix;
import au.edu.wehi.tests.regression.AllTests;
import joelib2.feature.types.atomlabel.*;

/**
 * @author eduthie
 */
public class GkTest extends TestCase
{
    private Molecule current;
    private static Logger logger = Logger.getLogger(GkTest.class);

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(GkTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            InputStream in = getClass().getResourceAsStream("current.mol2");
            current = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(current) )
            {
                fail("Failed to read butane");
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for GkTest.
     * @param arg0
     */
    public GkTest(String arg0)
    {
        super(arg0);
    }
    
    public void testValue()
    {
        for( int i=0; i < 4; ++i )
        {
            Gk gk = new Gk(i);
            double value = gk.value(current);
            String message = "Error calculating g" + i;
            switch(i)
            {
                case 0 : if(value!=0.0) {fail(message);} break;
                case 1 : if(value!=2.5) {fail(message);} break;
                case 2 : if(value!=0.8888888888888893) {fail(message);} break;
                case 3 : if(value!=0.5) {fail(message);} break;
            }
            gk = new Gk(i,true);
            value = gk.value(current);
        }
    }
    
    public void testENPauling()
    {
        Atom atom = new BasicConformerAtom();
        atom.setAtomicNumber(9);
        int value = (int) AbstractENPauling.calculate(atom);
        assertEquals(4,value);
    }
    
    public void testAddValence()
    {
        Gk g3 = new Gk(3);
        double[][] adj = 
            XMoleculeHelper.adjacencyMatrix(XMoleculeHelper.distanceMatrix(current));
        if( !g3.addValence(current,adj) )
        {
            fail("Unable to add electronegativities to adj matrix");
        }
    }
}
