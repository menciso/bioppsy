/*
 * CREATED: Feb 4, 2005
 * FILENAME: AllTests.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.tests.regression;

import junit.framework.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.tests.regression.rows.*;
import au.edu.wehi.tests.regression.values.*;
import au.edu.wehi.tests.regression.util.*;

/***************************************************************
 * TestSuite that runs all the sample tests
 ***************************************************************/
public class AllTests {

    static Logger logger = Logger.getLogger(AllTests.class);
    
	public static void main (String[] args) {

        PropertyConfigurator.configure(
            AllTests.class.getResource("test.log4j"));

		junit.textui.TestRunner.run(suite());
	}
	public static Test suite ( ) 
	{
		TestSuite suite= new TestSuite("mdm unit tests");
	    suite.addTest(new TestSuite(MoleculeRowDescTest.class));
	    suite.addTest(new TestSuite(SmartsMatchCountTest.class));
	    suite.addTest(new TestSuite(RegressionMoleculeVectorTest.class));
        suite.addTest(new TestSuite(TetkoRowTest.class));
        suite.addTest(new TestSuite(FeatureValueTest.class));
        suite.addTest(new TestSuite(XArraysTest.class));
        suite.addTest(new TestSuite(HouRowTest.class));
        suite.addTest(new TestSuite(HydrophobicCarbonTest.class));
        suite.addTest(new TestSuite(MLRTest.class));
        suite.addTest(new TestSuite(XMatrixTest.class));
        suite.addTest(new TestSuite(PLSTest.class));
        suite.addTest(new TestSuite(XMoleculeHelperTest.class));
        suite.addTest(new TestSuite(GkTest.class));
        suite.addTest(new TestSuite(DijkstraTest.class));
        suite.addTest(new TestSuite(LTest.class));
	    return suite;
	}
}
