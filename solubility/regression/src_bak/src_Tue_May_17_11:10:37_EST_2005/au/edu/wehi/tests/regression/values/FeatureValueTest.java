/*
 * CREATED: Feb 8, 2005
 * FILENAME: FeatureValueTest.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.tests.regression.values;

import junit.framework.TestCase;
import joelib2.feature.*;
import joelib2.io.*;
import joelib2.molecule.BasicConformerMolecule;
import joelib2.molecule.Molecule;
import java.io.*;
import java.util.Enumeration;

import au.edu.wehi.regression.values.FeatureValue;

/**
 * @author eduthie
 *
 */
public class FeatureValueTest extends TestCase
{

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(FeatureValueTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for FeatureValueTest.
     * @param arg0
     */
    public FeatureValueTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        double value;
        Molecule mol = null;
        
        try
        {
            FeatureValue mw = new FeatureValue("joelib2.feature.types.MolecularWeight");
            InputStream in = getClass().getResourceAsStream("../test.mol2");
            mol = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(mol) )
            {
                fail("Failed to read mol");
            }
            value = mw.value(mol);
            if( value == 0.0 )
            {
                fail("Molecular Weight is 0.0");
            }
            
            FeatureValue count = new FeatureValue("joelib2.feature.types.count.SOGroups");
            value = count.value(mol);
            if( value != 0.0 )
            {
                fail("Unexpected SO Group found in molecule");
            }
        }
        catch( FeatureException fe )
        {
            fail(fe.getMessage());
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
        
        try
        {
            String badFeature = "joelib2.feature.types.CharacteristicPolynomial";
            FeatureValue badFV = new FeatureValue(badFeature);
            value = badFV.value(mol);
            if( !(new Double(value)).isNaN() )
            {
                fail("Succeded with dab feature: " + badFeature +
                    " recieved value: " + value);
            }
        }
        catch( FeatureException fe )
        {
            // expected
        }
        
    }

    public void testFeatureValue()
    {
        Enumeration e = FeatureHelper.instance().getFeatureNames();
        while( e.hasMoreElements() )
        {
            String name = (String) e.nextElement();
            try
            {
                FeatureValue fv = new FeatureValue(name);
            }
            catch( FeatureException fe )
            {
                fail(fe.getMessage());
            }
        }
    }

}
