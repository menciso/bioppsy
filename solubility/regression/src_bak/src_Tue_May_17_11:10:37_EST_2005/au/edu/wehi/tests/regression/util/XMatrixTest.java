/*
 * Created on: Apr 29, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.tests.regression.util;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.util.XMatrix;
import au.edu.wehi.regression.util.Stats;
import au.edu.wehi.tests.regression.AllTests;
import Jama.Matrix;

/**
 * @author eduthie
 */
public class XMatrixTest extends TestCase
{
    private static Logger logger = Logger.getLogger(XMatrixTest.class);
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(XMatrixTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for XMatrixTest.
     * @param arg0
     */
    public XMatrixTest(String arg0)
    {
        super(arg0);
    }
    
    public void testNormalise()
    {
        double tolerance = 1E-10;
        
        int rows = 100;
        int columns = 100;
        double[][] M = new double[rows][columns];
        
        for( int i=0; i < M.length; ++i )
        {
            double[] row = M[i];
            for( int j=0; j < row.length; ++j )
            {
                row[j] = Math.random();
            }
        }
        
        XMatrix.normalise(M);
        
        for( int i=0; i < M[0].length; ++i )
        {
            double[] column = new double[M.length];
            for( int j=0; j < M.length; ++j )
            {
                column[j] = M[j][i];
            }
            double mean = Stats.mean(column);
            if( (mean - 0.0) > tolerance )
            {
                fail("MEAN IS NOT ZERO FOR NORMALISED MATRIX");
            }
            double sdDev = Stats.standardDeviation(column);
            if( (sdDev - 1.0) > tolerance )
            {
                fail("SDDEV IS NOT 1 FOR NORMALISED MATRIX");
            }
        }
    }

    public void testTimes()
    {
        double[][] A =
        {
                {1.0,2.0,3.0},
                {2.0,0.0,1.0},
                {1.0,1.0,0.0}
        };
        
        double[][] B =
        {
                {2.0,1.0,0.0},
                {3.0,1.0,2.0},
                {0.0,2.0,1.0}
        };
        
        double[][] result = XMatrix.times(A,B);
    }
}
