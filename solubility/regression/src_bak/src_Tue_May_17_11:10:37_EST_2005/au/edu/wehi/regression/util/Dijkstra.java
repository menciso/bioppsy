/*
 * Created on: May 13, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import org.apache.log4j.Logger;
import joelib2.molecule.*;
import java.util.*;

/**
 * Implements Dikstra's algorithm for finding the shortest distance
 * from a given node to all other nodes. Initialise by passing a Molecule
 * to the constructor. Be warned that all the hydrogen atoms in the Molecule
 * will be removed. Then use the calculate(int) method to calculate the
 * shortest distances from a given atom to all the other atoms in the
 * molecule. The getDistances() method can be used to retreive these 
 * calculated distances once calculate(int) has returned successfully.
 * 
 * @author eduthie
 */
public class Dijkstra
{
    private Logger logger;
    
    private Molecule mol;
    private int rootAtom;
    // indicies of molecules to which the shortest path from the root
    // vertex has been calculated
    private Vector<Integer> permanent;
    // indicies of molecules that the shortest path to has not yet been
    // determined
    private Vector<Integer> temporary;
    // each element i in the following array represents the best current
    // known shortest path to the molecule with index i+1
    private double[] distances;
    // each element i in the following array represents the index of the
    // preceeding vertex in the best current known shortest path to
    // the molecule with index i+1
    private int[] preceeding;
    
    /**
     * Initialises the algorithm with the given molecule. Note that
     * any hydrogens in the given molecule will be removed.
     */
    public Dijkstra(Molecule mol)
    {
        this.mol = mol;
        this.mol.beginModify();
        this.mol.deleteHydrogens();
        this.mol.endModify();
        this.logger = Logger.getLogger(getClass());
    }
    
    /**
     * Calculates the shortest distances from the atom at the given index
     * (joelib2 index starting from 1) to all the other atoms in the
     * molecule.
     * 
     * @param root - index of atom to start from
     * @return - true on success, false on failure
     */
    public boolean calculate(int root)
    {
        this.rootAtom = root;
        
        if( root <= 0 )
        {
            logger.error("Invalid index for root molecule: " + root);
            return false;
        }
        initialise(root);
        
        while( temporary.size() > 0 )
        {
            // sort the vector of molecule that the shortest
            // path to has not been confirmed
            Object[] temporaryArray = temporary.toArray();
            Arrays.sort(temporaryArray, new DistanceComparator());
            temporary = new Vector<Integer>();
            for( int i=0; i < temporaryArray.length; ++i )
            {
                temporary.add((Integer) temporaryArray[i]);
            }
            
            // molecule with next smallest distance u
            // is removed from temporary and added to permanent
            Integer u = temporary.get(0);
            temporary.remove(0);
            permanent.add(u);
            
            // relax the other verticies in temporary in relation
            // to u
            relax(u);
        }
        
        return true;
    }
    
    private void initialise(int root)
    {
        permanent = new Vector<Integer>();
        temporary = new Vector<Integer>();
        for( int i=1; i <= mol.getAtomsSize(); ++i )
        {
            temporary.add(new Integer(i));
        }
        distances = new double[mol.getAtomsSize()];
        for( int i=0; i < distances.length; ++i )
        {
            distances[i] = Double.NaN; // represents infinity
        }
        preceeding = new int[mol.getAtomsSize()];
        for( int i=0; i < preceeding.length; ++i )
        {
            preceeding[i] = -1; // represents no preceeding vertex
        }
        
        // set the distance to the root molecule to indicate
        /// we are starting there
        distances[root-1] = 0.0;
    }
    
    /**
     * The DistanceComparitor allows Integers representing the index
     * of an atom in the molecule to be compared by their distances
     * in the 'distances' list of current shortest distances from the 
     * root.
     * 
     * @author eduthie
     */
    public class DistanceComparator implements Comparator
    {
        /**
         * If a is closer to the root than b in the 'distances' array,
         * -1 is returned. If they are the same distance away, 0 is 
         * returned. If b is closer 1 is returned.
         */
        public int compare(Object a, Object b)
        {
            int aInt = ((Integer) a).intValue();
            int bInt = ((Integer) b).intValue();
            double aDist = distances[aInt-1];
            double bDist = distances[bInt-1];
            
            if( new Double(aDist).isNaN() )
            {
                return 1;
            }
            if( new Double(bDist).isNaN() )
            {
                return -1;
            }
            
            if( aDist < bDist )
            {
                return -1;
            }
            else if( aDist == bDist )
            {
                return 0;
            }
            else if( aDist < bDist )
            {
                return 1;
            }
            else
            {
                logger.error("Unreachable error in compare()");
                return -1;
            }
        }
    }
    
    /**
     * Checks all the neighbours of u to see if they can be reached from
     * the root atom in a shorter distance by going through u
     * than via their current shortest path. If it is shorter through u
     * their shortest path is changed to go via u.
     */
    private void relax(Integer u)
    {
        double uDist = distances[u.intValue()-1];
        double distViaU = uDist + 1.0;
        
        // visit all atoms adjoining u
        Atom atom = mol.getAtom(u.intValue());
        List bonds = atom.getBonds();
        for( int i=0; i < bonds.size(); ++i )
        {
            Bond bond = (Bond) bonds.get(i);
            // we have to determine if the adjoining atom
            // is at the beginning or the end of the bond
            Atom current = bond.getBegin();
            if( current.getIndex() == u.intValue() )
            {
                current = bond.getEnd();
            }
            int currentIndex = current.getIndex();
logger.info("U: " + uDist + " neighbour: " + currentIndex);
            double currentDist = distances[currentIndex-1];
            // if we can get to the current atom in a shorter
            // distance that the known shortest path to the
            // atom, set the new shortest distance and set
            // u as the preceeding molecule in the path
            if( (new Double(currentDist)).isNaN() ||
                (distViaU < currentDist) )
            {
                distances[currentIndex-1] = distViaU;
                preceeding[currentIndex-1] = u.intValue();
            }
        }
    }
    
    /**
     * Returns the shortest distances to the root atom in terms of
     * number of connections for all the atoms in the molecule.
     * 
     * @return double[] distances - where distances[i] is the
     * shortest distance for atom index (i+1) from the root node. 
     */
    public double[] getDistances()
    {
        return distances;
    }
    
    /**
     * Returns an array where each element i in the array is the index of
     * the atom preceeding atom i in the shortest path from the root to
     * atom i.
     */
    public int[] getPreceeding()
    {
        return preceeding;
    }
    
    /**
     * Returns the shortest path of atoms from the root atom to the atom at
     * the given index j.
     * 
     * @return ordered  array of indicies (joelib2 atom idx which start from 1)
     * of atoms in the shortest path to j. The first atom will be the root
     * atom and the last will be j.
     */
    public int[] getShortestPath(int j)
    {
        Vector<Integer> path = new Vector<Integer>();
        boolean backToRoot = false;
        int lastIndex = j;
        while( !backToRoot )
        {
            lastIndex = preceeding[lastIndex-1];
            path.add(new Integer(lastIndex));
            if( lastIndex == rootAtom )
            {
                backToRoot = true;
            }
        }
        int[] pathArray = new int[path.size()];
        for( int i = (path.size() - 1); i >= 0; --i )
        {
            pathArray[path.size()-i-1] = path.elementAt(i).intValue();
        }
        return pathArray;
    }
}
