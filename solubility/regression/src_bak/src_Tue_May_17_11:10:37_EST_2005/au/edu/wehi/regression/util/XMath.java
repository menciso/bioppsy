/*
 * CREATED: Feb 14, 2005
 * FILENAME: XMath.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.util;

/**
 * @author eduthie
 * 
 * Static mathematical utility methods.
 */
public class XMath
{
    /*
     * Returns -1 or 1 in a psedo random fashion.
     */
    public static int randomSign()
    {
        if( Math.random() > 0.5 )
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    
    /*
     * Returns log(x) if x > 0 or |x| if
     * x <= 0
     */
    public static double logPosModNeg(double x)
    {
        if( x > 0)
        {
            return Math.log(x);
        }
        else
        {
            return Math.abs(x);
        }
    }
    
    public static double cutoff(double x, double cutoff)
    {
        if( Math.abs(x) > cutoff )
        {
            if( x > 0 )
            {
                return cutoff;
            }
            else
            {
                return -cutoff;
            }
        }
        else
        {
            return x;
        }
    }
    
    public static double cutoffAbs(double x, double cutoff)
    {
        if( Math.abs(x) > cutoff )
        {
            return cutoff;
        }
        else
        {
            return Math.abs(x);
        }
    }
    
    public static double stairs(double x)
    {
        if( x < 0.2 )
        {
            return 0.0;
        }
        if( x < 0.75 )
        {
            return 1.0;
        }
        return 0.0;
    }
}
