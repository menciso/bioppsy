/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import java.io.*;

import joelib2.io.BasicIOType;
import joelib2.io.BasicIOTypeHolder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.rows.HouRow;
import au.edu.wehi.regression.rows.SolubilityRow;
import com.db4o.*;
import java.io.*;
import java.util.Properties;

/**
 * Trains all the models and adds them to the database to allow them to be
 * used.
 * 
 * @author eduthie
 */
public class CreateDatabase
{
    public static String PROPERTIES = "regression.properties";
    public static String DATABASE_FILE_KEY = "DatabaseFilename";
    public static String HOU_MLR = "HOU_MLR_MAIN"; 
    public static String HOU_PLS = "HOU_PLS_MAIN";
    private String filename;
    private String trainingFile = "/au/edu/wehi/tests/regression/training.sdf";
    private String fileType = "SDF";
    
    public static Logger logger = Logger.getLogger(CreateDatabase.class);
    
    public CreateDatabase()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + PROPERTIES);
                System.exit(0);
            }
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            filename = typesProp.getProperty(DATABASE_FILE_KEY);
            if( filename == null )
            {
                logger.error("Unable to get property: " + DATABASE_FILE_KEY +
                        " from file: " + PROPERTIES);
                System.exit(0);
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            System.exit(0);
        }
    }
    
    public static void main(String[] args)
    {
        PropertyConfigurator.configure(
               CreateDatabase.class.getResource("regression.log4j"));
        CreateDatabase cd = new CreateDatabase();
        cd.createDatabase();
    }
    
    public void createDatabase()
    {   
        logger.info("Creating Database in file: " + filename);
        ObjectContainer db = Db4o.openFile(filename);
        if( db == null )
        {
            logger.error("Unable to open database: " + filename);
        }
        try
        {
            deleteDatabase(db);
            configure();
            createHou(db);
            logger.info("FINISHED CREATING DATABASE");
        }
        catch(IOException ioe)
        {
            logger.error(ioe.getMessage());
        }
        finally
        {
            db.close();
        }
    }
    
    public void deleteDatabase(ObjectContainer db)
    {
        MLR mlr = new MLR();
        ObjectSet os = db.get(mlr);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
        MoleculePLS pls = new MoleculePLS();
        os = db.get(pls);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
        Test test = new Test();
        os = db.get(test);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
    }
    
    public void configure()
    {
        Db4o.configure().objectClass("au.edu.wehi.regression.Test").cascadeOnDelete(true);
        Db4o.configure().objectClass("au.edu.wehi.regression.MLR").cascadeOnDelete(true);
        Db4o.configure().objectClass("au.edu.wehi.regression.MoleculePLS").cascadeOnDelete(true);
    }
    
    public boolean createHou(ObjectContainer db)
        throws IOException
    {
        MLR mlr = new MLR(HOU_MLR);
        InputStream in = getClass().getResourceAsStream(trainingFile);
        if( in == null )
        {
            logger.error("Unable to find resource: " + trainingFile);
            return false;
        }
        BasicIOType type = BasicIOTypeHolder.instance().getIOType(fileType);
        if( type == null )
        {
            logger.error("Unable to get type: " + fileType);
            return false;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        HouRow houRow = new HouRow();
        houRow.init();
        rmv.setIndependentDescriber(houRow);
        rmv.setDependentDescriber(new SolubilityRow());
        if( mlr.train(rmv) )
        {
            logger.info("MLR trained");
            logger.info("HOU REGRESSION RSQ: " + mlr.getRsq());
            logger.info("HOU REGRESSION SDDEV: " + mlr.getSdDev());
            db.set(mlr);
        }
        else
        {
            logger.error("Unable to train MLR");
        }
                
        MoleculePLS pls = new MoleculePLS(HOU_PLS);
        if( pls.train(rmv) )
        {
            logger.info("PLS trained");
            db.set(pls);
        }
        else
        {
            logger.error("Unable to train PLS");
        }
        
        db.set(new Test());
        db.commit();
        
        return true;
    }

}
