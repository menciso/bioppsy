/*
 * Created on: May 31, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import org.apache.log4j.Logger;

/**
 * @author eduthie
 */
public interface Named
{
    public String getName();
}
