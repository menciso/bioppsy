/*
 * Created on: Jul 28, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;

import au.edu.wehi.regression.Constants;
import au.edu.wehi.regression.Database;
import au.edu.wehi.regression.MLR;
import au.edu.wehi.regression.luco.LucoPLS;

import com.db4o.ObjectSet;

/**
 * Calculates the logS value using the Hou model and MLR for
 * the regression. The Hou model is taken from:<br><br>
 * 
 * ADME Evalucation in Drug Discovery.
 * 4. Prediction of Aqueous Solubility Based on Atom Contribution Approach<br>
 * T.J. Hou, K. Xia, W. Zhang, and X.J. Xu<br>
 * J. Chem. Inf. Sci, August 24, 2003
 * 
 * @author eduthie
 */
public class SolubilityHouMLR extends MoleculeValueDesc
{
    private MLR mlr;
    
    public String getDescription()
    {
        return "Calculates the logS value using the Hou model" +
                " and MLR for the regression.<br>" +
                "Only 2D molecular information is required.<br>" +
                "The Hou model is taken from:<br><br>" +
               "ADME Evalucation in Drug Discovery.<br>" +
               "4. Prediction of Aqueous Solubility<br>" +
               "Based on Atom Contribution Approach<br>" +
               "T.J. Hou, K. Xia, W. Zhang, and X.J. Xu<br>" +
               "J. Chem. Inf. Sci, August 24, 2003";
    }

    public SolubilityHouMLR()
    {
        super();
        int expectedSize = 1;
        Database database = Database.instance();
        if( database == null )
        {
            return;
        }
        mlr = new MLR(Constants.HOU_MLR);
        ObjectSet objectSet = database.get(mlr);
        if( !(objectSet.size()==expectedSize) )
        {
            Logger.getLogger(getClass()).error("Unexpected number of HOU_MLR objects returned");
            Logger.getLogger(getClass()).error("Recieved: " + objectSet.size() + " Expected: " + expectedSize);
            mlr = null;
        }
        mlr = (MLR) objectSet.next();
    }

    /**
     * Returns the predicted logS value
     */
    public double value(Molecule mol)
    {
        if( mlr == null )
        {
            return Double.NaN;
        }
        else
        {
            return mlr.value(mol);
        }
    }

}
