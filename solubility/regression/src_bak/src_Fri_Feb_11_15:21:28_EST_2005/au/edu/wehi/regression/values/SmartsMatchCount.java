/*
 * CREATED: Feb 4, 2005
 * FILENAME: SmartsMatchCount.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import joelib2.smarts.BasicSMARTSPatternMatcher;
import org.apache.log4j.Logger;
import java.util.Vector;

/**
 * @author eduthie
 *
 * A SmartsMatchCount contains zero or many SMARTS patterns
 * which can be added with the addPattern(String) method.
 * When the value(Molecule) method is called, each SMARTS
 * pattern is run against the given molecule and the number
 * of total matches are returned.
 */
public class SmartsMatchCount extends MoleculeValueDesc
{
    Logger logger = Logger.getLogger(SmartsMatchCount.class);
    
    Vector<BasicSMARTSPatternMatcher> smarts;
    
    public SmartsMatchCount()
    {
        smarts = new Vector<BasicSMARTSPatternMatcher>();
    }
    
    /*
     * Add a new SMARTS pattern, returns false if it is an
     * invalid pattern.
     */
    public boolean addPattern(String pattern)
    {
        BasicSMARTSPatternMatcher pm = new BasicSMARTSPatternMatcher();
        if( !pm.init(pattern) )
        {
            String message = "Invalid SMARTS pattern: " + pattern;
            logger.error(message);
            return false;
        }
        else
        {
            smarts.add(pm);
            return true;
        }
    }
    
    /* 
     * Returns the total number of matches all the internal SMARTS
     * patterns have with the given Molecule.
     */
    public double value(Molecule mol)
    {
        int count = 0;
        BasicSMARTSPatternMatcher pm;
        int i;
        
        for( i=0; i < smarts.size(); ++i )
        {
            pm = smarts.elementAt(i);
            pm.match(mol);
            count += pm.getMatchesSize();
        }
        
        return (double) count;
    }

}
