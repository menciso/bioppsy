/*
 * CREATED: Feb 7, 2005
 * FILENAME: TetkoANN.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression;

import org.joone.engine.*;
import org.joone.io.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.RegressionMoleculeVector;
import java.io.*;
import joelib2.io.*;
import au.edu.wehi.regression.rows.*;
import org.joone.net.*;

/**
 * @author eduthie
 *
 */
public class TetkoANN extends ANN 
    implements NeuralNetListener, NeuralValidationListener
{
    Logger logger = Logger.getLogger(getClass());
    
    NeuralNet nnet;
    MemoryOutputSynapse mos;
    boolean tested = false;
    
    public TetkoANN()
    {
        super();
        PropertyConfigurator.configure(getClass().getResource("regression.log4j"));
    }

    public static void main(String[] args)
    {
        TetkoANN ta = new TetkoANN();
        ta.train();
    }
    
    public RegressionMoleculeVector getRMV(String filename, String fileType)
    {
        try
        {
            InputStream trainingStream = getClass().
                getResourceAsStream(filename);
            BasicIOType sdfType = BasicIOTypeHolder.instance().getIOType(fileType);
            RegressionMoleculeVector rmv = new RegressionMoleculeVector(
                trainingStream, sdfType, sdfType);
            TetkoRow tetkoRow = new TetkoRow();
            if( !tetkoRow.init() )
            {
                logger.error("Failed to init TetkoRow");
                return null;
            }
            rmv.setIndependentDescriber(tetkoRow);
            SolubilityRow solRow = new SolubilityRow();
            rmv.setDependentDescriber(solRow);
            return rmv;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return null;
        }   
    }
    
    public void train()
    {
        String filename = "training.sdf";
        String fileType = "SDF";
        RegressionMoleculeVector rmv = getRMV(filename,fileType);
        if( rmv == null )
        {
            logger.error("Could not load file: " + filename);
            return;
        }
        double[][] inputArray = rmv.getXMatrix();
        double[][] outputArray = rmv.getYMatrix();
        
        constructANN(inputArray,outputArray);
    }
    
    public void test()
    {
        if( !tested )
        {
            tested = true;
        }
        else
        {
            displayResults();
        }
        
        String filename = "training.sdf";
        String fileType = "SDF";
        RegressionMoleculeVector rmv = getRMV(filename,fileType);
        if( rmv == null )
        {
            logger.error("Unable to read file: " + filename);
            return;
        }
        double[][] inputArray = rmv.getXMatrix();
        testANN(inputArray);
    }
}
