/*
 * CREATED: Feb 11, 2005
 * FILENAME: ANN.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression;


import org.joone.engine.*;
import org.joone.engine.learning.*;
import org.joone.io.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.joone.util.LearningSwitch;
import org.joone.net.*;

/**
 * @author eduthie
 *
 */
public class ANN implements NeuralNetListener, NeuralValidationListener
{
    Logger logger = Logger.getLogger(getClass());
    
    NeuralNet nnet;
    MemoryOutputSynapse mos;
    boolean tested = false;
    int n = 1000;
    int p = 100;
    
    public ANN()
    {
        super();
        PropertyConfigurator.configure(getClass().getResource("regression.log4j"));
    }

    public static void main(String[] args)
    {
        ANN ann = new ANN();
        ann.train();
    }
    
    public void train()
    {        
        double[][] inputArray = new double[n][p];
        double[][] outputArray = new double[n][1];
        
        for( int i=0; i < n; ++i )
        {
            for( int j=0; j < p; ++j )
            {
                inputArray[i][j] = Math.random()*10.0;
            }
        }
        
        // y = sum( xi^2 )
        for( int i=0; i < n; ++i )
        {
            double sumSq = 0.0;
            for( int j=0; j < p; ++j )
            {
                sumSq += Math.pow(inputArray[i][j],2);
            }
            outputArray[i][0] = sumSq;
        }
        
        constructANN(inputArray,outputArray);
    }
    
    public void test()
    {
        if( !tested )
        {
            tested = true;
        }
        else
        {
            displayResults();
        }
        
        double[][] inputArray = new double[1][p];
        
        double sumSq = 0.0;
        
        for( int i=0; i < p; ++i)
        {
            inputArray[0][i] = Math.random()*10.0;
            sumSq += inputArray[0][i];
        }
        
        logger.info("SUM OF SQUARES: " + sumSq);
        
        testANN(inputArray);
    }
    
    public boolean constructANN(double[][] inputArray, double[][] outputArray)
    {
        
        LinearLayer input = new LinearLayer();
        TanhLayer hidden = new TanhLayer();
        //TanhLayer hidden2 = new TanhLayer();
        LinearLayer output = new LinearLayer();
        input.setRows(inputArray[0].length);
        hidden.setRows(4);
        //hidden2.setRows(4);
        output.setRows(1);
        
        FullSynapse synapse_IH = new FullSynapse();
        //FullSynapse synapse_HH = new FullSynapse();
        FullSynapse synapse_HO = new FullSynapse();
        input.addOutputSynapse(synapse_IH);
        hidden.addInputSynapse(synapse_IH);
        hidden.addOutputSynapse(synapse_HO);
        //hidden2.addInputSynapse(synapse_HH);
        //hidden2.addOutputSynapse(synapse_HO);
        output.addInputSynapse(synapse_HO);
        
        input.addInputSynapse
        (
            createInputSynapse(inputArray,1,inputArray.length)
        );
        
        TeachingSynapse trainer = new TeachingSynapse();
        MemoryInputSynapse desired =
            createInputSynapse(outputArray,1,outputArray.length);
        trainer.setDesired(desired);
        output.addOutputSynapse(trainer);
        
        nnet = new NeuralNet();
        nnet.addLayer(input, NeuralNet.INPUT_LAYER);
        nnet.addLayer(hidden, NeuralNet.HIDDEN_LAYER);
        //nnet.addLayer(hidden2, NeuralNet.HIDDEN_LAYER);
        nnet.addLayer(output, NeuralNet.OUTPUT_LAYER);
        nnet.setTeacher(trainer);
        
        Monitor monitor = nnet.getMonitor();
        monitor.setLearningRate(0.7);
        //monitor.setMomentum(0.9);
        monitor.setTrainingPatterns(inputArray.length);
        monitor.setTotCicles(500);
        monitor.setLearning(true);
        monitor.addNeuralNetListener(this);
        
        nnet.start();
        nnet.getMonitor().Go();
        
        return true;
    }
    
    public void netStopped(NeuralNetEvent nne)
    {
        logger.info("NET STOPPED");
        test();
    }
    
    public void netStoppedError(NeuralNetEvent nne, String error)
    {
        logger.info("NET STOPPED ERROR: " + error);
    }
    
    public void errorChanged(NeuralNetEvent nne)
    {
        //logger.info("ERROR: " + nnet.getMonitor().getGlobalError());
        //logger.info("ERROR CHANGED");
    }
    
    public void cicleTerminated(NeuralNetEvent nne)
    {   
        Monitor mon = nnet.getMonitor();
        long c = mon.getTotCicles() - mon.getCurrentCicle();
        if ((c % 1000) == 0)
        {
            logger.info("ERROR: " + mon.getGlobalError());
        }
    }
    
    public void netValidated(NeuralValidationEvent event) 
    { 
        // Shows the RMSE at the end of the cycle 
        //NeuralNet NN = (NeuralNet) event.getSource();
        //System.out.println(" Validation Error: "+ NN.getMonitor().getGlobalError());
    }
    
    public void netStarted(NeuralNetEvent nne)
    {
        logger.info("NET STARTED");
    }
    
    public boolean testANN( double[][] inputArray )
    {   
        Layer input = nnet.getInputLayer();
        input.removeAllInputs();
        input.addInputSynapse
        (
            createInputSynapse(inputArray,1,inputArray.length)
        );
        
        Layer output = nnet.getOutputLayer();
        output.removeAllOutputs();
        mos = new MemoryOutputSynapse();
        output.addOutputSynapse(mos);
        
        Monitor monitor = nnet.getMonitor();
        monitor.setTotCicles(1);
        monitor.setTrainingPatterns(inputArray.length);
        monitor.setLearning(false);
        nnet.start();
        monitor.Go();
        
        return true;
    }
    
    public boolean displayResults()
    {
        double[] pattern;
        
        while( (pattern = mos.getNextPattern()) != null )
        {
            System.out.println("CALCULATED: " + pattern[0]);
        }
        
        return true;
    }
    
    public MemoryInputSynapse createInputSynapse(
        double[][] inputArray, int firstRow, int lastRow)
    {
        MemoryInputSynapse inputStream = new MemoryInputSynapse();
        inputStream.setInputArray(inputArray);
        inputStream.setAdvancedColumnSelector(1 + "-" + inputArray[0].length);
        inputStream.setFirstRow(firstRow);
        inputStream.setLastRow(lastRow);
        return inputStream;
    }
    
    public LearningSwitch createSwitch( 
        StreamInputSynapse train, StreamInputSynapse validate)
    {
        LearningSwitch lsw = new LearningSwitch();
        lsw.addTrainingSet(train);
        lsw.addValidationSet(validate);
        return lsw;
    }
    
    public void displayMatrix(double[][] M)
    {
        for( int i=0; i < M.length; ++i)
        {
            System.out.println("");
            
            for( int j=0; j < M[i].length; ++j)
            {
                System.out.print(M[i][j] + " ");
            }
        }
    }
    
    public void tanhMatrix(double[][] M)
    {
        for( int i=0; i < M.length; ++i )
        {
            for( int j=0; j < M[i].length; ++j )
            {
                M[i][j] = Math.tanh(M[i][j]);
            }
        }
    }
    
    public double atanh(double x)
    {
        double value = Math.log(1+x) - Math.log(1-x);
        return value/2.0;
    }
}

