/*
 * Created on: May 24, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.*;
import java.util.*;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.util.*;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * The connectivity index as defined in:
 * <br><br>
 * Molecular Connectivity in Chemistry and Drug Research<br>
 * (Medicinal chemistry, a series of monographs)<br>
 * Lemont B. Kier; Lowell H. Hall<br>
 * Academic Press, Inc. 1976<br>
 * ISBN 0-12-206560-0
 * 
 * @author eduthie
 */
public class Chi extends MoleculeValueDesc
{
    private Logger logger;
    private int m;
    private int type;
    private boolean valence;
    
    /**
     * Initialises the connectivity index.
     * 
     * @param m - Number of edges in subgraphs.
     * @param type - Subgraph type, e.g. Subgraph.PATH, see 
     * au.edu.wehi.regression.util.Subgraph
     * @param valence - If true valence of a node is calculated
     * as Z-h where Z is the number of valence electrons and h
     * is the number of Hydrogens. If false valence is calculated
     * as number of adjoining bonds. 
     */
    public Chi(int m, int type, boolean valence)
    {
        this.logger = Logger.getLogger(getClass());
        this.m = m;
        this.type = type;
        this.valence = valence;
    }
    
    /**
     * Calculates the connectivity index. Firstly all the subgraphs
     * of the molecule that have m edges and are of type t are found.
     * The index is the sum of S values for each of these subgraphs.
     * The S value for a subgraph is the inverse square root of the
     * product of the valences of each of the subgraph's nodes.<br>
     * <br>
     * Chi = sum(over all subgraphs i)(Si)<br>
     * Si = (product(over all vecticies j)(valence of vertex))^(-1/2)
     */
    public double value(Molecule molecule)
    {
        double chi = 0.0;
        
        Molecule mol = (Molecule) molecule.clone();
        mol.deleteHydrogens();
        
        Set<Subgraph> subgraphs = GraphHelper.subgraphs(mol,m);
        Iterator<Subgraph> subgraphI = subgraphs.iterator();
        
        while( subgraphI.hasNext() )
        {   
            Subgraph subgraph = subgraphI.next();
            
            if( subgraph.isOfType(type) )
            {
                Set<Integer> node = subgraph.getNodes();
                Iterator<Integer> nodeI = node.iterator();
                
                double subgraphValue = 1.0;
                
                while( nodeI.hasNext() )
                {
                    Atom atom = mol.getAtom(nodeI.next().intValue());
                    if( valence )
                    {
                        subgraphValue *= XAtomHelper.getValenceZmH(atom);
                    }
                    else
                    {
                        subgraphValue *= XAtomHelper.getGraphValence(atom,true);
                    }
                }
                
                chi += Math.pow(subgraphValue,-0.5);
            }
        }
        
        return chi;
    }
}
