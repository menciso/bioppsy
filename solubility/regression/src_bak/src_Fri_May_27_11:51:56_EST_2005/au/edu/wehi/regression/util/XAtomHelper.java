/*
 * Created on: May 25, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import org.apache.log4j.Logger;
import joelib2.molecule.*;
import joelib2.data.BasicElementHolder;
import joelib2.feature.types.atomlabel.AbstractImplicitHydrogenCount;
import joelib2.feature.types.atomlabel.AbstractExplicitHydrogenCount;
import joelib2.util.iterator.BondIterator;

/**
 * Static functions for operations on Atoms
 * 
 * @author eduthie
 */
public class XAtomHelper
{
    public static Logger logger = Logger.getLogger(XAtomHelper.class);
    
    /**
     * Returns the valence of the given Atom by using the formula
     * d = Z -h where Z is the number of valence electrons available 
     * in the outer shells of the atom and h is the number of hydrogens
     * implicitly attached to the atom.
     */
    public static int getValenceZmH(Atom atom)
    {
        int h = AbstractImplicitHydrogenCount.calculate(atom);
        int Z = getValenceElectrons(atom);
        return Z - h;
    }
    
    /**
     * Returns the valence of the atom as the sum of the number of atoms bonded
     * to the atom that are not Hydrogen. If multipleBonds is true double and
     * triple bonds contribute 2 and 3 respectively towards the valence. Otherwise
     * if multipleBonds is false they only contribute 1.
     * <br><br>
     * Note to self:
     * Use multipleBonds = true for calculating valence for use in sum instead
     * of d = Z - h. Use multipleBonds = false when classifying subgraphs.
     *  
     * @param atom
     * @param multipleBonds - whether to count the multiplicity of multiple bonds
     * @return - number of non-Hydrogen atoms connected to the given atom.
     */
    public static int getGraphValence(Atom atom, boolean multipleBonds)
    {
        int valence = 0;
        
        BondIterator bi = atom.bondIterator();
        while(bi.hasNext())
        {
            Bond bond = bi.nextBond();
            Atom adjacent = bond.getEnd();
            if( adjacent.getIndex() == atom.getIndex() )
            {
                adjacent = bond.getBegin();
            }
            if( adjacent.getAtomicNumber() != 1 )
            {
                // not Hydrogen
                if( multipleBonds )
                {
                    valence += bond.getBondOrder();
                }
                else
                {
                    ++valence;
                }
            }
        }
        
        return valence;
    }
     
    /**
     * Returns the number of valence electrons in the outer shells of the given atom as
     * the total number (determined from the atomic number) of electrons minus the
     * formal charge on the atom.
     */
    public static int getValenceElectrons(Atom atom)
    {
        return 
            BasicElementHolder.instance().getExteriorElectrons(atom.getAtomicNumber()) -
            atom.getFormalCharge();
    }
}
