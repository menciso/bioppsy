/*
 * Created on: Apr 29, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import org.apache.log4j.Logger;
import Jama.Matrix;
import au.edu.wehi.regression.util.*;
import java.util.Vector;

/**
 * <h1>PLS - Partial Least Squares Regression</h1>
 * 
 * Implementation of the partial least-squares regression
 * algorithm. Simply pass the training data to the train()
 * method and if true is returned, pass the data to predict to
 * the predict() method.<br><br>
 * 
 * Reference:<br>
 * Partial Least-Squares Regression: A Tutorial<br>
 * Paul Geladi and Bruce R. Kowalski<br>
 * Analytica Chimica Acta, 185 (1986) 1-17
 * 
 * @author eduthie
 */
public class PLS
{
    private static Logger logger = Logger.getLogger(PLS.class);
    
    Matrix E, F;
    Matrix Fold;
    Matrix wt,t,qt,u,pt,bh;
    Matrix told,ptold;
    Vector<Matrix> Pv,Qv,Wv;
    Vector<Double> Bv;
    Vector<double[]> Tv,Uv;
    double b;
    Matrix B,Q;
    
    int h,p,q,n,k,iteration;
    
    double tolerance;
    
    double[][] xMatrixStats;
    double[][] yMatrixStats;
    
    public PLS()
    {
        tolerance = 1E-10;
    }
    
    /**
     * Returns the rounding error that is used in this algorithm.
     */
    public double getTolerance()
    {
        return tolerance;
    }
    
    /**
     * Trains the PLS model given a set of training data. 
     * Each row in the matricies provided represents a single
     * observation in the training set. The columns of the X
     * matrix are the values of the independent variables for
     * this observation. The columns of the Y matrix are the
     * corresponding values of the dependent variables for the
     * observation.<br><br>
     * 
     * For the given parameters, each array in the array of
     * arrays represents a row of a matrix.<br><br>
     * 
     * n - number of observations<br>
     * p - number of independent variables<br>
     * q - number of dependent variables<br>
     * <br><br>
     * 
     * @param X - n by p matrix. 
     * @param Y - n by q matrix.
     * @return - true on success, false on failure
     */
    public boolean train(double[][] X, double[][] Y)
    {
        init(X,Y);
        
        boolean fStillReducing = true;
        while( fStillReducing )
        {
            ++h;
            boolean tStillReducing = true;
            iteration = 0;
            while( tStillReducing )
            {
                // calculate wt
                wt = u.transpose().times(E).times(1/u.transpose().times(u).get(0,0));
                // normalise wt
                wt = wt.times(1/wt.normF());
                // calculate t
                t = E.times(wt.transpose()).times(1/wt.times(wt.transpose()).get(0,0));
                // calculate qt
                qt = t.transpose().times(F).times(1/t.transpose().times(t).get(0,0));
                // normalise qt
                qt = qt.times(1/qt.normF());
                // calculate u
                u = F.times(qt.transpose()).times(1/qt.transpose().times(qt).get(0,0));
                // check for convergence of t
                if( iteration == 0 )
                {
                    tStillReducing = true;
                }
                else
                {
                    tStillReducing = !hasConverged(t,told,tolerance);
                }
                told = t;
                ++iteration;
            }
            logger.debug("T has converged");
            
            // calculate pt
            pt = t.transpose().times(E).times(1/t.transpose().times(t).get(0,0));
            // renormalise pt,t,wt
            ptold = pt;
            pt = ptold.times(1/ptold.normF());
            t = t.times(ptold.normF());
            wt = wt.times(ptold.normF());
            // calculate b
            b = u.transpose().times(t).times(1/t.transpose().times(t).get(0,0)).get(0,0);
            // calculate E and F
            E = E.minus(t.times(pt));
            Fold = F;
            F = F.minus(t.times(qt).times(b));
            // save pt,wt,qt,b
            Pv.add(pt);
            Wv.add(wt);
            Qv.add(qt);
            Bv.add(new Double(b));
            Tv.add(t.transpose().getArray()[0]);
            Uv.add(u.transpose().getArray()[0]);
            
            // check for convergence of F
            if( h >= p )
            {
                fStillReducing = false;
            }
            else
            {
                fStillReducing = !hasConverged(F,Fold,tolerance);
            }
        }
        logger.debug("F has converged");
        
        k = Bv.size();
        B = new Matrix(k,k,0.0);
        for( int i=0; i < k; ++i )
        {
            B.set(i,i,Bv.elementAt(i).doubleValue());
        }
        Q = new Matrix(k,q);
        for( int i=0; i < Qv.size(); ++i)
        {
            Q.setMatrix(i,i,0,q-1,Qv.get(i));
        }
        
        return true;
    }
    
    
    /**
     * Initialises the attributes.
     * 
     * @param X - n by p matrix
     * @param Y - n by q matrix
     */
    private void init(double[][] X, double[][] Y)
    {        
        E = new Matrix(X);
        F = new Matrix(Y);
        xMatrixStats = XMatrix.normalise(E.getArray());
        yMatrixStats = XMatrix.normalise(F.getArray());
        
        h = 0;
        n = E.getRowDimension();
        logger.debug("n: " + n);
        p = E.getColumnDimension();
        logger.debug("p: " + p);
        q = F.getColumnDimension();
        logger.debug("q: " + q);
        
        u = F.getMatrix(0,n-1,0,0);
        //u = new Matrix(n,1,1.0);
        logger.debug("u rows: " + u.getRowDimension());
        logger.debug("u columns: " + u.getColumnDimension());
        told = new Matrix(n,1,0.0);
        
        Pv = new Vector<Matrix>();
        Qv = new Vector<Matrix>();
        Wv = new Vector<Matrix>();
        Bv = new Vector<Double>();
        Tv = new Vector<double[]>();
        Uv = new Vector<double[]>();
    }
    
    /**
     * Returns true if the difference between the two given matricies is
     * less than or equal to the specified tolerance. Returns false otherwise.
     * The given matricies must have the same dimensions.<br><br>
     * 
     * @param X - a by b matrix.
     * @param Xold - a by b matrix
     * @param double - tolerance
     * @return - true if ||X - Xold || <= tolerance
     */
    public boolean hasConverged(Matrix X, Matrix Xold, double tolerance)
    {   
        if( (X.getRowDimension() != Xold.getRowDimension()) || 
             (X.getColumnDimension() != Xold.getColumnDimension()))
        {
            logger.error("Matricies of unequal size passed to hasConverged()");
            return true;
        }
        Matrix D = X.minus(Xold);
        if( D.normF() > tolerance )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    /**
     * Predicts the dependent variables from the given row of
     * values of the independent variables. train() must have
     * returned successfully before calling this method.
     * <br><br>
     * 
     * @param x - Row of p values of independent variables. p
     * must equal the number of independent variables passed
     * to train().
     * @return - Matrix of dependent variables. Will only contain
     * one row (each array in array of arrays represents a row) of
     * q values. Returns predicted values of dependent variables
     * based on input. Returns null on error.
     */
    public double[][] predict(double[] x)
    {
        double[][] data = {x};
        return predict(data);
    }
    
    /**
     * Predicts the dependent variables from the given matrix of
     * values of the independent variables. Each row in the given matrix X
     * contains the independent variables for one observation.
     * A matrix, each row containing the predicted values for one observation,
     * is returned. train() must have returned successfully before calling this 
     * method.<br><br>
     * 
     * (each array in array of arrays represents a row in a matrix)<br><br>
     * 
     * @param X - n by p matrix. Each row is the values of independent variables
     * for one observation.  p must equal the number of independent variables passed
     * to train().
     * @return - Matrix of dependent variables. Will only contain
     * Each row is the predicted values of the dependent variables
     * based on the given input. Returns null on error.
     */
    public double[][] predict(double[][] X)
    {   
        Matrix Es = new Matrix(X);
        XMatrix.normalise(Es.getArray(),xMatrixStats);
        int ns = X.length;
        Matrix Ts = new Matrix(ns,k);
        Matrix ts;
        
        for( int i=0; i < k; ++i )
        {
            ts = Es.times(Wv.elementAt(i).transpose());
            Es = Es.minus(ts.times(Pv.elementAt(i)));
            Ts.setMatrix(0,ns-1,i,i,ts);
        }
        
        Matrix Ys = Ts.times(B.times(Q));
        
        XMatrix.reverseNormalise(Ys.getArray(),yMatrixStats);
        return Ys.getArray();
    }
    
    public Vector<double[]> getT()
    {
        return Tv;
    }
    
    public Vector<double[]> getU()
    {
        return Uv;
    }
    
    public Vector<Matrix> getW()
    {
        return Wv;
    }
    
    public Vector<Matrix> getP()
    {
        return Pv;
    }
    
    public Vector<Matrix> getQ()
    {
        return Qv;
    }
}
