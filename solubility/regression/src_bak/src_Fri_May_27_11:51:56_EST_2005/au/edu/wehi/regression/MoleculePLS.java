/*
 * Created on: May 3, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import java.util.Vector;

import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;

import Jama.Matrix;
import au.edu.wehi.regression.util.XMatrix;

/**
 * @author eduthie
 */
public class MoleculePLS extends PLS
{
    private String name;
    
    public MoleculePLS()
    {
        super();
    }
    
    public MoleculePLS(String name)
    {
        this.name = name;
    }
    
    /**
     * Trains the PLS from the given data. The given RegressionMoleculeVector
     * must have been initialised with the molecules from the training set.
     * Additionally the independent and dependent describers of the 
     * RegressionMoleculeVector must be set to determine what descriptors
     * are calculated from the molecules and what properties are being 
     * predicted.
     * 
     * @param rmv - training data
     */
    public boolean train(RegressionMoleculeVector rmv)
    {
        Matrix X = new Matrix(rmv.getXMatrix());
        Vector<Integer> allZero =  (new XMatrix(X.getArray())).anyZeroColumns();
        if( allZero.size() > 0)
        {
            Logger.getLogger(MoleculePLS.class).error("One or more descriptors are all zero for the entire " +
                    " training set. Please remove them:");
            for( int i=0; i < allZero.size(); ++i)
            {
                Logger.getLogger(MLR.class).error(" descriptor index: " + allZero.elementAt(i));
            }
            return false;
        }
        Matrix Y = new Matrix(rmv.getYMatrix());
        
        return train(X.getArray(),Y.getArray());
    }
}
