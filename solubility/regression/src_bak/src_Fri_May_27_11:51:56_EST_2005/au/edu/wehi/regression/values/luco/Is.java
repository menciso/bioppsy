/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.Molecule;
import joelib2.smarts.BasicSMARTSPatternMatcher;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * Icooh, 1.0 if molecule contains a sulphur atom,
 * 0.0 otherwise.
 * 
 * @author eduthie
 */
public class Is extends MoleculeValueDesc
{
    private Logger logger;
    
    public Is()
    {
        logger = Logger.getLogger(getClass());
    }
    
    /**
     * Return 1 if the given Molecule contains a sulpher atom
     * 0.0 otherwise.
     */
    public double value(Molecule mol)
    {
        String pattern = "[S]";
        BasicSMARTSPatternMatcher pm = new BasicSMARTSPatternMatcher();
        if( !pm.init(pattern) )
        {
            logger.error("Invalid SMARTS pattern: " + pattern);
            return Double.NaN;
        }
        pm.match(mol);
        if( pm.getMatchesSize() > 0 )
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
