/*
 * CREATED: Feb 21, 2005
 * FILENAME: TetkoANN.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression;

import java.io.*;
import org.apache.log4j.PropertyConfigurator;
import joelib2.io.*;
import au.edu.wehi.regression.rows.*;

/**
 * Implements the Tetko ANN model.
 * 
 * @author eduthie
 */
public class TetkoANN extends ANN
{
    public static void main(String[] args)
    {
        PropertyConfigurator.configure(ANN.class.getResource("regression.log4j"));
        TetkoANN ann = new TetkoANN();
        ann.begin();
    }
    
    public RegressionMoleculeVector getRMV(String filename, String fileType)
    {
        try
        {
            InputStream trainingStream = getClass().
                getResourceAsStream(filename);
            BasicIOType sdfType = BasicIOTypeHolder.instance().getIOType(fileType);
            RegressionMoleculeVector rmv = new RegressionMoleculeVector(
                trainingStream, sdfType, sdfType);
            TetkoRow tetkoRow = new TetkoRow();
            if( !tetkoRow.init() )
            {
                logger.error("Failed to init TetkoRow");
                return null;
            }
            rmv.setIndependentDescriber(tetkoRow);
            SolubilityRow solRow = new SolubilityRow();
            rmv.setDependentDescriber(solRow);
            return rmv;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return null;
        }   
    }
    
    public boolean setTrainingMatricies()
    {
        String filename = "training.sdf";
        String fileType = "SDF";
        RegressionMoleculeVector rmv = getRMV(filename,fileType);
        if( rmv == null )
        {
            logger.error("Could not load file: " + filename);
            return false;
        }
        setIndependentM(rmv.getXMatrix());
        setDependentM(rmv.getYMatrix());
        
        return true;
    }
    
    public boolean setTestMatricies()
    {
        return true;
    }
}
