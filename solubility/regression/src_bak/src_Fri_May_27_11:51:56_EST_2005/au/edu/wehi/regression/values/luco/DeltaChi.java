/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;

import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * DeltaChi is the difference between the plain and valence
 * Chi indicies:<br>
 * DeltaChi(mol) = X(mol) - Xv(mol)<br>
 * <br>
 * Reference:<br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * Juan M Luco<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404
 * 
 * @author eduthie
 */
public class DeltaChi extends MoleculeValueDesc
{
    private Logger logger;
    private int m;
    private int type;
    private boolean valence;
    
    /**
     * Initialises the connectivity index.
     * 
     * @param m - Number of edges in subgraphs.
     * @param type - Subgraph type, e.g. Subgraph.PATH, see 
     * au.edu.wehi.regression.util.Subgraph
     * @param valence - If true valence of a node is calculated
     * as Z-h where Z is the number of valence electrons and h
     * is the number of Hydrogens. If false valence is calculated
     * as number of adjoining bonds. 
     */
    public DeltaChi(int m, boolean valence)
    {
        this.logger = Logger.getLogger(getClass());
        this.m = m;
        this.type = type;
        this.valence = valence;
    }

    /**
     * Returns Chi(mol) - ChiV(mol)
     */
    public double value(Molecule mol)
    {
        Chi chi = new Chi(m,type,false);
        Chi chiV = new Chi(m,type,true);
        return chi.value(mol) - chiV.value(mol);
    }

}
