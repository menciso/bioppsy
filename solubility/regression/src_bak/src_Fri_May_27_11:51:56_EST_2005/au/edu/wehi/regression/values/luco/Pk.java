/*
 * Created on: May 17, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.*;
import joelib2.util.iterator.BondIterator;
import org.apache.log4j.Logger;

import au.edu.wehi.regression.values.MoleculeValueDesc;

import java.util.Vector;

/**
 * Calculates Pk, the number of pairs of ramifications separated
 * by k verticies. A ramification is a vertex with valence 3 or 4.
 * <br><br>
 * Reference:<br>
 * Prediction and Interpretation of Some Pharmacological Properties
 * of Cephalosporins Using Molecular Connectivity<br>
 * R. Garcia Domenech, J Galvez ALvarez, R Moliner Llusar and F.
 * Garcia March<br>
 * Drug Invest 3 (5), 344-350, 1991
 * 
 * @author eduthie
 */
public class Pk extends MoleculeValueDesc
{
    private int k;
    private Logger logger;
    
    public Pk(int k)
    {
        this.k = k;
        logger = Logger.getLogger(getClass());
    }
    
    /**
     * Calculates the Pk value for the given molecule with the k value that
     * was passed to the constructor. Pk is the number of verticies pairs with
     * valence 3 or 4 separated by k edges. All hydrogens are removed.
     */
    public double value(Molecule molecule)
    {
        Molecule mol = (Molecule) molecule.clone();
        mol.deleteHydrogens();

        int num = 0;
        
        for( int i=1; i <= mol.getAtomsSize(); ++i)
        {
            Atom atom = mol.getAtom(i);
            if( isRamification(atom) )
            {
                // we have a ramification
                Vector<Integer> haveAlreadyVisited = new Vector<Integer>();
                haveAlreadyVisited.add(new Integer(atom.getIndex()));
                num += searchForAdjacentRamifications(atom,1,k,haveAlreadyVisited);     
            }
        }
        
        // we return half the number found since each pair is
        // counted twice
        return num/2;
    }
    
    /**
     * Searches neighbouring atoms of the given atom. If they are not in
     * haveAlreadyVisited and if the current level is equal to the targetLevel
     * they are added to the total ramifications returned by this function.
     * If the current level is greater than the target level 0 is returned.
     * When an atom is visited it is added to haveAlreadyVisited.
     * The neighbours of the adjacent atoms to each neighbouring atom are
     * visited recursively using this function with the level incremented,
     * the total ramifications found at the correct level are returned.
     * 
     * @param atom - atom to start search from
     * @param level - current level in recursion
     * @param targetLevel - level at which ramifications count
     * @param haveAlreadyVisited - atoms already checked
     * @return - number of ramifications at target level from starting atom
     */
    public int searchForAdjacentRamifications(Atom atom, int level, int targetLevel,
            Vector<Integer> haveAlreadyVisited)
    {
        if( level > targetLevel )
        {
            return 0;
        }
        
        int num = 0;
        
        BondIterator bi = atom.bondIterator();
        while( bi.hasNext() )
        {
            Bond bond = bi.nextBond();
            Atom adjacent = bond.getEnd();
            if( adjacent.getIndex() == atom.getIndex() )
            {
                adjacent = bond.getBegin();
            }
            if( !haveAlreadyVisited.contains(new Integer(adjacent.getIndex())) )
            {
                if(level == targetLevel)
                {
                    if( isRamification(adjacent) )
                    {
                        ++num;
                    }
                }
                haveAlreadyVisited.add(new Integer(adjacent.getIndex()));
                num += searchForAdjacentRamifications(adjacent,level+1,targetLevel,
                        haveAlreadyVisited);
            }
        }
        
        return num;
    }
    
    /**
     * Returns true if the given atom is a ramification (has a valence
     * of 3 or 4) or returns false otherwise.
     */
    public boolean isRamification(Atom atom)
    {
        if( (atom.getValence() == 3) ||
                (atom.getValence() == 4) )
        {
            return true;     
        }
        else
        {
            return false;
        }
    }
    
}
