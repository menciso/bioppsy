/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import java.io.*;
import joelib2.io.BasicIOType;
import joelib2.io.BasicIOTypeHolder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.rows.HouRow;
import au.edu.wehi.regression.rows.SolubilityRow;
import au.edu.wehi.regression.rows.LucoRow;
import au.edu.wehi.regression.rows.BBRow;
import com.db4o.*;
import java.io.*;
import java.util.Properties;

/**
 * Trains all the models and adds them to the database to allow them to be
 * used.
 * 
 * @author eduthie
 */
public class CreateDatabase
{
    public static final String PROPERTIES = "regression.properties";
    public static final String DATABASE_FILE_KEY = "DatabaseFilename";
    public static final String SOL_TRAIN_KEY = "SolubilityTrainingFilename";
    public static final String BB_TRAIN_KEY = "BBTrainingFilename";
    public static final String HOU_MLR = "HOU_MLR_MAIN"; 
    public static final String HOU_PLS = "HOU_PLS_MAIN";
    public static final String LUCO_PLS = "LUCO_PLS";
    private String dbFilename;
    private String solTrainFilename = "/au/edu/wehi/regression/solubilityTraining.sdf";
    private String bbTrainFilename = "/au/edu/wehi/regression/bbTraining.mol2";
    private String fileType = "SDF";
    
    public static Logger logger = Logger.getLogger(CreateDatabase.class);
    
    public CreateDatabase()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + PROPERTIES);
                System.exit(0);
            }
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            dbFilename = typesProp.getProperty(DATABASE_FILE_KEY);
            if( dbFilename == null )
            {
                logger.error("Unable to get property: " + DATABASE_FILE_KEY +
                        " from file: " + PROPERTIES);
                System.exit(0);
            }
            solTrainFilename = typesProp.getProperty(SOL_TRAIN_KEY);
            bbTrainFilename = typesProp.getProperty(BB_TRAIN_KEY);
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            System.exit(0);
        }
    }
    
    public static void main(String[] args)
    {
        PropertyConfigurator.configure(
               CreateDatabase.class.getResource("regression.log4j"));
        CreateDatabase cd = new CreateDatabase();
        cd.createDatabase();
    }
    
    public void createDatabase()
    {   
        logger.info("Creating Database in file: " + dbFilename);
        ObjectContainer db = Db4o.openFile(dbFilename);
        if( db == null )
        {
            logger.error("Unable to open database: " + dbFilename);
        }
        try
        {
            deleteDatabase(db);
            configure();
            if( !createHou(db) )
            {
                logger.error("FAILED TO CREATE HOU");
            }
            if( !createLuco(db) )
            {
                logger.error("FAILED TO CREATE LUCO");
            }
            logger.info("FINISHED CREATING DATABASE");
              
        }
        catch(IOException ioe)
        {
            logger.error(ioe.getMessage());
        }
        finally
        {
            logger.info("BEFORE COMMIT");
            db.commit();
            logger.info("HAVE COMMITED TO DATABASE");
            db.close();
            logger.info("HAVE CLOSED CONNECTION TO DATABASE");
        }
    }
    
    public void deleteDatabase(ObjectContainer db)
    {
        MLR mlr = new MLR();
        ObjectSet os = db.get(mlr);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
        MoleculePLS pls = new MoleculePLS();
        os = db.get(pls);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
        Test test = new Test();
        os = db.get(test);
        while( os.hasNext() )
        {
            db.delete(os.next());
        }
    }
    
    public void configure()
    {
        Db4o.configure().objectClass("au.edu.wehi.regression.Test").cascadeOnDelete(true);
        Db4o.configure().objectClass("au.edu.wehi.regression.MLR").cascadeOnDelete(true);
        Db4o.configure().objectClass("au.edu.wehi.regression.MoleculePLS").cascadeOnDelete(true);
    }
    
    public boolean createHou(ObjectContainer db)
        throws IOException
    {
        MLR mlr = new MLR(HOU_MLR);
        InputStream in = getClass().getResourceAsStream(solTrainFilename);
        if( in == null )
        {
            logger.error("Unable to find resource: " + solTrainFilename);
            return false;
        }
        BasicIOType type = BasicIOTypeHolder.instance().getIOType(fileType);
        if( type == null )
        {
            logger.error("Unable to get type: " + fileType);
            return false;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        HouRow houRow = new HouRow();
        houRow.init();
        rmv.setIndependentDescriber(houRow);
        rmv.setDependentDescriber(new SolubilityRow());
        if( mlr.train(rmv) )
        {
            logger.info("Hou MLR trained");
            logger.info("HOU REGRESSION RSQ: " + mlr.getRsq());
            logger.info("HOU REGRESSION SDDEV: " + mlr.getSdDev());
            db.set(mlr);
        }
        else
        {
            logger.error("Unable to train MLR");
        }
                
        MoleculePLS pls = new MoleculePLS(HOU_PLS);
        if( pls.train(rmv) )
        {
            logger.info("Hou PLS trained");
            db.set(pls);
        }
        else
        {
            logger.error("Unable to train Hou PLS");
        }
        
        db.set(new Test());
        
        return true;
    }
    
    public boolean createLuco(ObjectContainer db)
        throws IOException
    {
        InputStream in = getClass().getResourceAsStream(bbTrainFilename);
        if( in == null )
        {
            logger.error("Unable to find resource: " + bbTrainFilename);
            return false;
        }
        BasicIOType type = BasicIOTypeHolder.instance().filenameToType(bbTrainFilename);
        if( type == null )
        {
            logger.error("Unable to get type for file: " + bbTrainFilename);
            return false;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        LucoRow lucoRow = new LucoRow();
        rmv.setIndependentDescriber(new LucoRow());
        rmv.setDependentDescriber(new BBRow());
        
        MoleculePLS pls = new MoleculePLS(LUCO_PLS);
        if( pls.train(rmv) )
        {
            logger.info("Luco PLS trained");
            db.set(pls);
        }
        else
        {
            logger.error("Unable to train Luco PLS");
        }
        
        return true;
    }
}
