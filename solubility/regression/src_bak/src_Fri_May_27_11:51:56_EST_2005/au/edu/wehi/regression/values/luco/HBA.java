/*
 * Created on: May 18, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.*;
import joelib2.util.iterator.BondIterator;
import org.apache.log4j.Logger;

import au.edu.wehi.regression.values.MoleculeValueDesc;

import java.util.*;

/**
 * HBA (Hydrogen Bond Acceptors) is the total count of
 * electron pairs on O and N atoms. In the nitro group (NO2) only
 * one acceptor pair is counted for each of the two oxygens
 * in the group.
 * <br><br>
 * Reference:<br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * Juan M Luco<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404
 * 
 * @author eduthie
 */
public class HBA extends MoleculeValueDesc
{
    private Logger logger;
    
    public HBA()
    {
        logger = Logger.getLogger(getClass());
    }
    
    /**
     * Returns the total count of electron pairs on O and N atoms. 
     * In the nitro group (NO2) only one acceptor pair is counted for 
     * each of the two oxygens in the group. The given molecule 
     * must have hydrogens attached.
     * 
     * @author eduthie
     */
    public double value(Molecule mol)
    {
        int acceptorPairs = 0;
        for( int i=1; i <= mol.getAtomsSize(); ++i )
        {
            Atom atom = mol.getAtom(i);
            if( (atom.getAtomicNumber() == 7) ||
                (atom.getAtomicNumber() == 8) )
            {
                if( isOxygenInNitro(atom) )
                {
                    // oxygen in the nitro group only has
                    // one pair that counts
                    acceptorPairs += 1;
                }
                else
                {
                    acceptorPairs += freeElectronPairs(atom);
                }
            }
        }
        return acceptorPairs;
    }
    
    /**
     * Returns the number of free electron pairs on the atom which are
     * calculated as the number of electrons for a full valence shell
     * minus the number of electrons in bonds. The charge on the atom
     * is not taken into account.
     */
    public int freeElectronPairs(Atom atom)
    {
        int bondingElectrons = 0;
        BondIterator bi = atom.bondIterator();
        while( bi.hasNext() )
        {
            Bond bond = bi.nextBond();
            bondingElectrons += bond.getBondOrder()*2;
        }
        if( atom.getAtomicNumber() < 3 )
        {
            return (2 - bondingElectrons)/2;
        }
        else
        {
            return (8 - bondingElectrons)/2;
        }
    }

    /**
     * Returns true if the given Atom is an Oxygen in a nitro (NO2)
     * group. Returns false otherwise.
     */
    public boolean isOxygenInNitro(Atom atom)
    {
        if( !(atom.getAtomicNumber() == 8) )
        {
            return false;
        }
        List bonds = atom.getBonds();
        if( bonds.size() != 1 )
        {
            return false;
        }
        Bond bond = (Bond) bonds.get(0);
        Atom adjacent = bond.getEnd();
        if( adjacent.getIndex() == atom.getIndex())
        {
            adjacent = bond.getBegin();
        }
        if( adjacent.getAtomicNumber() != 7 )
        {
            // not adjacent to N
            return false;
        }
        boolean haveAnotherO = false;
        BondIterator bi = adjacent.bondIterator();
        while( bi.hasNext() )
        {
            bond = bi.nextBond();
            Atom next2N = bond.getEnd();
            if( next2N.getIndex() == adjacent.getIndex() )
            {
                next2N = bond.getBegin();
            }
            if( next2N.getIndex() == atom.getIndex() )
            {
                // gone back to previous O, do nothing
            }
            else if( next2N.getAtomicNumber() == 8 )
            {
                // we have another oxygen
                bonds = next2N.getBonds();
                if( bonds.size() != 1 )
                {
                    // other oxygen is conneted to something other
                    // than the N, probably H, not what we want, 
                    // do nothing.
                }
                else
                {
                    haveAnotherO = true;
                }
            }
        }
        return haveAnotherO;
    }

}
