/*
 * Created on: Jul 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.gui;

import java.awt.*;
import javax.swing.*;
import net.infonode.docking.View;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import javax.swing.tree.*;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * @author eduthie
 */
public class ValuesView extends View
{
    private static Logger logger = Logger.getLogger(ValuesView.class);
    private static String NAME = "Values to Calculate";
    
    private JPanel content;
    private JTree tree;
    private Vector<String> values;
    private Vector<String> properties;
    private Vector<String> descriptors;
    private Hashtable<String,String> valuesDescriptions;
    
    public ValuesView()
    {
        super(NAME, null, new JPanel());
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        if( !loadValues() )
        {
            logger.error("Failed to load values in ValuesView");
            values = new Vector<String>();
        }
        DefaultMutableTreeNode root = 
            new DefaultMutableTreeNode("Available Values");
        DefaultMutableTreeNode propertiesNode =
            new DefaultMutableTreeNode("Properties");
        for( int i=0; i < properties.size(); ++i )
        {
            DefaultMutableTreeNode propertyNode = 
                new DefaultMutableTreeNode(properties.elementAt(i));
            propertiesNode.add(propertyNode);
        }
        root.add(propertiesNode);
        DefaultMutableTreeNode descriptorsNode =
            new DefaultMutableTreeNode("Descriptors");
        for( int i=0; i < descriptors.size(); ++i )
        {
            DefaultMutableTreeNode descriptorNode =
                new DefaultMutableTreeNode(descriptors.elementAt(i));
            descriptorsNode.add(descriptorNode);
        }
        root.add(descriptorsNode);
        
        valuesDescriptions = new Hashtable<String,String>();
        loadValuesDescriptions();
        
        tree = new JTree(root);
        TreeCellRenderer renderer = new CustomTreeCellRenderer();
        tree.setCellRenderer(renderer);
        ToolTipManager.sharedInstance().registerComponent(tree);
        ToolTipManager.sharedInstance().setDismissDelay(10000);
        ToolTipManager.sharedInstance().setInitialDelay(100);
        
        JScrollPane scrollPane = new JScrollPane(tree);
        scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        content.add(scrollPane);
    }
    
    public class CustomTreeCellRenderer extends DefaultTreeCellRenderer
    {
        public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
        {
            Component c = super.getTreeCellRendererComponent(tree,value,sel,expanded,
                leaf,row,hasFocus);
            if( leaf )
            {
                String description = valuesDescriptions.get(value.toString().trim());
                if( description.length() <= 0)
                {
                    description = null;
                }
                ((JComponent) c).setToolTipText(description);
            }
            return c;
        }
    }
    
    public void loadValuesDescriptions()
    {
        for( int i=0; i < values.size(); ++i )
        {
            MoleculeValueDesc desc = getDescriptorByName(values.elementAt(i));
            if( desc != null )
            {
                String description = desc.getHTMLDescription();
                valuesDescriptions.put(values.elementAt(i),description);
            }
        }
    }
    
    public boolean loadValues()
    {
        if( !loadProperties() )
        {
            return false;
        }
        return true;
    }
    
    /**
     * Loads constants from the properties file
     */
    public boolean loadProperties()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + Constants.PROPERTIES);
                return false;
            }
            
            // get database inputFilename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            String valuesString = typesProp.getProperty("ValuesClassNames");
            if( valuesString == null )
            {
                logger.error("Failed to retrieve ValuesClassNames");
                return false;
            }
            values = new Vector<String>();
            String[] valuesArray = valuesString.trim().split(Constants.INPUT_SEPARATOR);
            for( int i=0; i < valuesArray.length; ++i )
            {
                values.add(valuesArray[i]);
            }
            
            // separate properties from descriptors
            String propKeysString = typesProp.getProperty("PropertiesKeywords");
            if( propKeysString == null )
            {
                logger.error("Failed to retrieve PropertiesKeywords");
                return false;
            }
            String[] keywords = propKeysString.split(Constants.INPUT_SEPARATOR);
            properties = new Vector<String>();
            descriptors = new Vector<String>();
            for( int i=0; i < values.size(); ++i )
            {
                String value = values.elementAt(i);
                boolean isProperty = false;
                for( int j=0; j < keywords.length; ++j )
                {
                    if( value.indexOf(keywords[j].trim()) != -1 )
                    {
                        isProperty = true;
                        break;
                    }
                }
                if( isProperty )
                {
                    properties.add(value);
                }
                else
                {
                    descriptors.add(value);
                }
            }
            
            return true;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }        
    }
    
    /**
     * Returns a list of all values currently selected by the user.
     */
    public synchronized String[] getSelectedValues()
    {
        Vector<String> selectedValues = new Vector<String>();
        
        TreePath[] paths = tree.getSelectionPaths();
        for( int i=0; i < paths.length; ++i )
        {
            if( paths[i].getPathCount() >= 3 )
            {
                DefaultMutableTreeNode last = 
                    (DefaultMutableTreeNode) paths[i].getLastPathComponent();
                Object object = last.getUserObject();
                selectedValues.add((String) object);
            }
        }
        String[] returnValues = new String[selectedValues.size()];
        for( int i=0; i < selectedValues.size(); ++i )
        {
            returnValues[i] = selectedValues.elementAt(i);
        }
        return returnValues;
    }
    
    /**
     * Returns a new MoleculeValueDesc object of the given type. Returns
     * null on error.
     */
    public static MoleculeValueDesc getDescriptorByName(String name)
    {
        Class theClass;
        MoleculeValueDesc descriptor;
        
        try
        {
            theClass = Class.forName(Constants.VALUES_PREFIX + "." + name);
        }
        catch( ClassNotFoundException cnfe )
        {
            try
            {
                // second time try the LUCO_PREFIX
                theClass = Class.forName(Constants.LUCO_PREFIX + "." + name);
            }
            catch( ClassNotFoundException cnfe2 )
            {
                logger.error("Unable to locate the value: " + name);
                return null; 
            }
        }
        try
        {
            descriptor = (MoleculeValueDesc) theClass.newInstance();
            return descriptor;
        }
        catch( IllegalAccessException iae )
        {
            logger.error("Illegal Access to class: " + theClass.getName());
            logger.error(iae.getMessage());
            return null;
        }
        catch( InstantiationException ie )
        {
            logger.error("Unable to Instantiate class: " + theClass.getName());
            logger.error(ie.getMessage());
            return null;
        }
    }

}
