/*
 * Created on: May 13, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import org.apache.log4j.Logger;
import joelib2.molecule.*;
import au.edu.wehi.regression.util.*;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * The L descriptor taken from:<br><br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * Juan M Luco<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404<br><br>
 * "topological molecular length defined as the counted distance in the number
 * of edjes between the molecule's two most separate atoms by the shortest means"
 * 
 * @author eduthie
 */
public class L extends MoleculeValueDesc
{
    /**
     * Returns the  "topological molecular length defined as the counted distance in the number
     * of edjes between the molecule's two most separate atoms by the shortest means"
     */
    public double value(Molecule molecule)
    {
        Molecule mol = (Molecule) molecule.clone();
        double[][] distanceMatrix = XMoleculeHelper.distanceMatrix(mol);
        int maxI = 0;
        int maxJ = 0;
        double max = -1.0;
        for( int i=0; i < distanceMatrix.length; ++i )
        {
            for( int j=i+1; j < distanceMatrix[i].length; ++j )
            {
                if( distanceMatrix[i][j] > max )
                {
                    max = distanceMatrix[i][j];
                    maxI = i;
                    maxJ = j;
                }
            }
        }
        Dijkstra dijkstra = new Dijkstra(mol);
        if( !dijkstra.calculate(maxI+1) )
        {
            Logger.getLogger(getClass()).error("Unable to calculate dijkstra in L");
            return Double.NaN;
        }
        return dijkstra.getDistances()[maxJ];
    }
}
