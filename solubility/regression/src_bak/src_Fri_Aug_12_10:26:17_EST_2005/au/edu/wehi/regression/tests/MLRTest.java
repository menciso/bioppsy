/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests;

import joelib2.io.BasicIOType;
import joelib2.io.BasicIOTypeHolder;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.*;
import au.edu.wehi.regression.rows.*;
import java.io.*;
import joelib2.molecule.*;

/**
 * @author eduthie
 */
public class MLRTest extends TestCase
{
    private static Logger logger = Logger.getLogger(MLRTest.class);
    
    String trainingFile = "training.sdf";
    String fileType = "SDF";

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(MLRTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for MLRTest.
     * @param arg0
     */
    public MLRTest(String arg0)
    {
        super(arg0);
    }
    
    public void testAll()
    {
        try
        {
            MLR mlr = new MLR();
            InputStream in = getClass().getResourceAsStream(trainingFile);
            if( in == null )
            {
                fail("Unable to find resource: " + trainingFile);
            }
            BasicIOType type = BasicIOTypeHolder.instance().getIOType(fileType);
            if( type == null )
            {
                fail("Unable to get type: " + fileType);
            }
            RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
            HouRow houRow = new HouRow();
            houRow.init();
            rmv.setIndependentDescriber(houRow);
            rmv.setDependentDescriber(new SolubilityRow());
            if( !mlr.train(rmv) )
            {
                fail("Unable to train MLR");
            }
            logger.info("MLR trained");
            
            /*
            for( int i=0; i < rmv.getSize(); ++i )
            {
                Molecule molecule = rmv.getMol(i);
                double result = mlr.predict(molecule);
            }
            */
            
            logger.info("REGRESSION RSQ: " + mlr.getRsq());
            logger.info("REGRESSION SDDEV: " + mlr.getSdDev());
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
