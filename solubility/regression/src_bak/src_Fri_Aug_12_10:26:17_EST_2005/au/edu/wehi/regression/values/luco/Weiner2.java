/*
 * Created on: May 31, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import org.apache.log4j.Logger;
import joelib2.molecule.Molecule;

/**
 * Simply the Wiener index to the power of 2
 * 
 * @author eduthie
 */
public class Weiner2 extends Weiner
{
    /**
     * Returns the square of the Wiener index.
     */
    public double value(Molecule mol)
    {
        return Math.pow(super.value(mol),2.0);
    }
}
