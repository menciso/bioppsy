/*
 * Created on: May 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import java.util.*;
import org.apache.log4j.Logger;
import joelib2.molecule.*;

/**
 * A Subgraph has nodes and edges.
 * 
 * @author eduthie
 */
public class Subgraph
{
    /**
     * All nodes in a PATH Subgraph have valence <= 2.
     * No chains can be present.
     */
    public static final int PATH = 1;
    /**
     * A CLUSTER Subgraph has at least one node with 
     * valence 3 or 4 but no nodes with valence 2.
     * No chains can be present.
     */
    public static final int CLUSTER = 2;
    /**
     * A PATH_CLUSTER Subgraph must have at least one node
     * with valence 2 and at least one node with valence 3 or 4.
     * No chains can be present.
     */
    public static final int PATH_CLUSTER = 3;
    /**
     * A CHAIN Subgraph is any subgraph that contains at least one
     * cycle.
     */
    public static final int CHAIN = 4;
    
    private static Logger logger = Logger.getLogger(Subgraph.class);
    private TreeSet<Integer> nodes;
    private TreeSet<XEdge> edges;
    
    public Subgraph()
    {
        nodes = new TreeSet<Integer>();
        edges = new TreeSet<XEdge>();
    }
    
    public Subgraph(TreeSet<Integer> nodes, TreeSet<XEdge> edges)
    {
        this.nodes = (TreeSet<Integer>) nodes.clone();
        this.edges = (TreeSet<XEdge>) edges.clone();
    }
    
    public boolean addNode(Integer node)
    {
        return nodes.add(node);
    }
    
    public boolean addEdge(XEdge edge)
    {
        return edges.add(edge);
    }
    
    /**
     * If the subgraph already contains the node and given edge
     * they are not added. If the node is already contained the
     * edge is still added.
     */
    public boolean addNodeEdge(Integer node, XEdge edge)
    {
        if( !nodes.contains(node) && !edges.contains(edge) )
        {
            // if we don't have the node or the edge, add both
            nodes.add(node);
            edges.add(edge);
            return true;
        }
        else if( nodes.contains(node) && !edges.contains(edge) )
        {
            // if we already have the node, but not the edge,
            // we just add the edge
            edges.add(edge);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public TreeSet<Integer> getNodes()
    {
        return nodes;
    }
    
    public TreeSet<XEdge> getEdges()
    {
        return edges;
    }
    
    public Iterator<Integer> nodeIterator()
    {
        return nodes.iterator();
    }
    
    /**
     * Returns true if the other object is a Subgraph and has identical
     * sets of nodes and edges in regards to combinations.
     */
    public boolean equals(Object other)
    {
        if( !other.getClass().getName().equals(getClass().getName()) )
        {
            return false;
        }
        
        Subgraph otherSG = (Subgraph) other;
        
        if( !nodes.equals(otherSG.getNodes()) )
        {
            return false;
        }
        
        if( !edges.equals(otherSG.getEdges()) )
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * If two Subgraph objects are equal, they will return the same
     * hashCode() value.
     */
    public int hashCode()
    {
        int hashCode = 0;
        Iterator<Integer> nodeI = nodes.iterator();
        while( nodeI.hasNext() )
        {
            hashCode += nodeI.next().intValue();
        }
        Iterator<XEdge> edgeI = edges.iterator();
        while( edgeI.hasNext() )
        {
            XEdge edge = edgeI.next();
            hashCode += edge.getFirst();
            hashCode += edge.getSecond();
        }
        return hashCode;
    }
    
    /**
     * Returns a Subgraph with cloned nodes and edges. Note the Integers
     * regpresenting node indicies and the XEdges representing edges are
     * not duplicated, just their references are copied.
     */
    public Subgraph clone()
    {
        return new Subgraph(nodes,edges);
    }
    
    /**
     * Returns the number of edges incident on the given node.
     */
    public int getValence(Integer atom)
    {
        int valence = 0;
        
        Iterator<XEdge> edgesI = edges.iterator();
        while( edgesI.hasNext() )
        {
            XEdge edge = edgesI.next();
            if( (edge.getFirst() == atom.intValue()) ||
                (edge.getSecond() == atom.intValue()) )
            {
                ++valence;
            }
        }
        
        return valence;
    }
    
    /**
     * Returns true if this Subgraph is of the given type, false
     * otherwise. Returns false if the type is not recognised.
     * 
     * @param type - int type, must be one of the static int variables
     * of Subgraph, e.g. Subgraph.PATH
     */
    public boolean isOfType(int type)
    {
        boolean allLessThanEqualTwo = true;
        boolean noneEqualTwo = true;
        boolean atLeastOneThreeOrFour = false;
        boolean atLeastOneTwo = false;

        if( edges.size() >= nodes.size() )
        {
            if(type == CHAIN)
            {
                return true;
            }
            else
            {
                // if it is not a chain type it cannot
                // contain a chain
                return false;
            }
        }
        else if( type == CHAIN )
        {
            return false;
        }
        
        Iterator<Integer> nodeI = nodes.iterator();
        while( nodeI.hasNext() )
        {
            Integer node = nodeI.next();
            int valence = getValence(node);
            if( valence > 2 )
            {
                allLessThanEqualTwo = false;
            }
            if( valence == 2 )
            {
                noneEqualTwo = false;
                atLeastOneTwo = true;
            }
            if( (valence==3) || (valence==4) )
            {
                atLeastOneThreeOrFour = true;
            }
        }
        
        switch(type)
        {
            case(PATH) :
            {
                if( allLessThanEqualTwo )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            case(CLUSTER) :
            {    
                if( noneEqualTwo && atLeastOneThreeOrFour )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }   
            case(PATH_CLUSTER) :
            {    
                if( atLeastOneTwo && atLeastOneThreeOrFour )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }   
            default :
            {
                logger.error("Unrecognised subgraph type: " + type);
                return false;
            }
        }
    }
}
