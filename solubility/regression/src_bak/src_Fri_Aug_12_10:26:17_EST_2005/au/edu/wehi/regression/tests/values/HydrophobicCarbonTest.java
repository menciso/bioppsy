/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import joelib2.molecule.*;
import au.edu.wehi.regression.tests.TestUtil;
import au.edu.wehi.regression.values.HydrophobicCarbon;
import java.util.Vector;

/**
 * @author eduthie
 */
public class HydrophobicCarbonTest extends TestCase
{
    private static Logger logger = Logger.getLogger(HydrophobicCarbonTest.class);
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(HydrophobicCarbonTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for HydrophobicCarbonTest.
     * @param arg0
     */
    public HydrophobicCarbonTest(String arg0)
    {
        super(arg0);
    }

    public void testAll()
    {
        Molecule mol = (new TestUtil()).getMolecule();
        if( mol == null )
        {
            fail("Unable to get test molecule in HydrophobicCarbonTest");
        }
        HydrophobicCarbon hc = new HydrophobicCarbon();
        
        Vector<BasicConformerAtom> heteroatoms = hc.getHeteroatoms(mol);
        assertEquals(2,heteroatoms.size());
        Vector<BasicConformerAtom> carbons = hc.getCarbons(mol);
        assertEquals(2,carbons.size());
        
        int value = (int) hc.value(mol);
        assertEquals(0,value);
    }

}
