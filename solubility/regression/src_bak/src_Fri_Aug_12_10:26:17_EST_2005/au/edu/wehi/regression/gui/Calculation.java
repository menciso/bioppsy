/*
 * Created on: Jul 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.gui;

import java.awt.*;
import javax.swing.*;
import net.infonode.tabbedpanel.titledtab.*;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;
import au.edu.wehi.regression.Constants;
import au.edu.wehi.regression.RegressionMoleculeVector;
import java.io.*;
import joelib2.io.BasicIOType;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.molecule.Molecule;

/**
 * @author eduthie
 */
public class Calculation extends TitledTab implements Runnable
{   
    private Logger logger;
    
    private File file;
    private String[] values;
    private JPanel content;
    
    private MoleculeValueDesc[] descriptors;
    private String[][] results;
    
    public Calculation(File file, String[] values)
    {
        super(file.getName(), null, new JPanel(),null);
        logger = Logger.getLogger(getClass());
        content = (JPanel) getContentComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        this.file = file;
        this.values = values;
    }
    
    public void runCalc()
    {
        Thread thread = new Thread(this,file.getName());
        thread.start();
    }
    
    public void run()
    {
        if( !prepareDescriptors() )
        {
            errorMessage("Failed to prepare descriptors");
            return;
        }
        if( !performCalculation() )
        {
            errorMessage("Failed to calculate values");
            return;
        }
        displayTable();
    }
    
    public void errorMessage(String message)
    {
        content.removeAll();
        JTextArea text = new JTextArea(message + 
                " See console output for more information");
        content.add(text);
        content.validate();
    }
    
    /**
     * Loads all the descriptors chosen by the user. Returns true
     * if all descriptors have been created successfully and stored
     * in the descriptors array, false otherwise.
     */
    public boolean prepareDescriptors()
    {
        descriptors = new MoleculeValueDesc[values.length];
        Class theClass = null;
        
        for( int i=0; i < values.length; ++i )
        {
            try
            {
                theClass = Class.forName(Constants.VALUES_PREFIX + "." + values[i]);
            }
            catch( ClassNotFoundException cnfe )
            {
                try
                {
                    // second time try the LUCO_PREFIX
                    theClass = Class.forName(Constants.LUCO_PREFIX + "." + values[i]);
                }
                catch( ClassNotFoundException cnfe2 )
                {
                    logger.error("Unable to locate the value: " + values[i]);
                    logger.error("Please remove from the list to perform calculation");
                    return false; 
                }
            }
            try
            {
                descriptors[i] = (MoleculeValueDesc) theClass.newInstance();
            }
            catch( IllegalAccessException iae )
            {
                logger.error("Illegal Access to class: " + theClass.getName());
                logger.error(iae.getMessage());
                logger.error("Please remove the value: " + values[i] + 
                        " from the list to perform calculation");
                return false;
            }
            catch( InstantiationException ie )
            {
                logger.error("Unable to Instantiate class: " + theClass.getName());
                logger.error(ie.getMessage());
                logger.error("Please remove the value: " + values[i] + 
                    " from the list to perform calculation");
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Calculates the values and stores them as Strings
     * in the results matrix
     */
    public boolean performCalculation()
    {       
        try
        {
            RegressionMoleculeVector rmv = getRMV(file.getAbsolutePath());
            if( rmv == null )
            {
                logger.error("Failed to read input file: " + file.getAbsolutePath());
                return false;
            }
            
            // initialise the output matrix
            results = new String[rmv.getSize()+1][descriptors.length];
            
            int index = 0;
            
            // add the headings
            for( int i=0; i < values.length; ++i )
            {
                results[index][i] = values[i];
            }
            
            // do the calculations
            for( int i=0; i < rmv.getSize(); ++i )
            {
                ++index;
                Molecule mol = rmv.getMol(i);
                for( int j=0; j < descriptors.length; ++j )
                {
                    double value = descriptors[j].value(mol);
                    results[index][j] = round(value,Constants.DP_OUTPUT);
                }
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        
        return true;
    }
    
    public RegressionMoleculeVector getRMV(String filename)
        throws IOException
    {
        if( !BasicIOTypeHolder.instance().canReadExtension(filename) )
        {
            logger.error("Invalid file extension: " + filename);
            return null;
        }
        BasicIOType type = BasicReader.checkGetInputType(filename);
        if( type == null )
        {
            logger.error("Invalid file extension: " + filename);
            return null;
        }
        InputStream in = new FileInputStream(filename);
        if( in == null )
        {
            logger.error("Unable to read file: " + filename);
            return null;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        return rmv;
    }
    
    /**
     * Rounds the given double to the given number of decimal places and
     * returns its string representation
     */
    public String round(double value, int dp)
    {
        String valueString = new Double(value).toString();
        int dpIndex = valueString.indexOf('.');
        if( (dpIndex+dp+1) >= valueString.length() )
        {
            return valueString;
        }
        else
        {
            return valueString.substring(0,dpIndex+dp+1);
        }
    }
    
    public void displayTable()
    {
        String[] columnNames = results[0];
        String[][] rowData = new String[results.length-1][results[0].length];
        for( int i=1; i < results.length; ++i )
        {
            rowData[i-1] = results[i];
        }
        JTable table = new JTable(rowData,columnNames);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setHorizontalScrollBarPolicy(
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        content.removeAll();
        scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        content.add(scrollPane);
        content.validate();
    }

}
