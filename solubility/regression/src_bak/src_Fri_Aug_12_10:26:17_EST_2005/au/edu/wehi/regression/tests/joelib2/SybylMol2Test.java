/*
 * Created on: Jun 7, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.joelib2;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import java.io.*;
import joelib2.molecule.*;
import joelib2.io.*;
import joelib2.io.types.SybylMol2;

/**
 * @author eduthie
 */
public class SybylMol2Test extends TestCase
{
    private InputStream in;
    
    private static Logger logger = Logger.getLogger(SybylMol2Test.class);

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(SybylMol2Test.class);
    }
    
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        String name = "sybylmol2test.mol2";
        in = getClass().getResourceAsStream("sybylmol2test.mol2");
        if( in == null )
        {
            logger.error("Failed to open: " + name);
            return;
        }
        
    }

    /*
     * Class under test for boolean read(Molecule)
     */
    public void testReadMolecule()
    {
        try
        {
            if( in == null )
            {
                fail("No input stream to test");
            }
            SybylMol2 syb = new SybylMol2();
            syb.initReader(in);
            Molecule mol = new BasicConformerMolecule();
            if( !syb.read(mol) )
            {
                fail("Failed to read first molecule");
            }
            mol = new BasicConformerMolecule();
            if( !syb.read(mol) )
            {
                fail("Failed to read molecule");
            }
            assertEquals(2,mol.getAtom(7).getFormalCharge());
            assertEquals(2,mol.getAtom(14).getFormalCharge());
            for( int i=0; i < 3; ++i )
            {
                mol = new BasicConformerMolecule();
                if( !syb.read(mol) )
                {
                    fail("Failed to read propane number: " + i);
                }
                mol.deleteHydrogens();
                assertEquals(3,mol.getAtomsSize());
                for( int j=1; j <= mol.getAtomsSize(); ++j )
                {
                    Atom atom = mol.getAtom(j);
                    assertEquals(0,atom.getFormalCharge());
                }
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

}
