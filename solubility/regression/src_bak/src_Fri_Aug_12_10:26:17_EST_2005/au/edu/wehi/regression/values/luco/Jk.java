/*
 * Created on: May 11, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 */
public class Jk extends Gk
{
    public Jk(int k)
    {
        super(k);
    }
    /**
     * @param k - parameter 
     * @param valence - if true the valence form of Gk is calculated. This
     * sets the main diagonal elements in the adjacency matrix to the
     * electronegativity values of the associated atoms.
     */
    public Jk(int k, boolean valence)
    {
        super(k,valence);
    }
    
    public double value(Molecule mol)
    {
        return super.value(mol)/(mol.getAtomsSize()-1);
    }

}
