/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import joelib2.feature.*;

/**
 * Returns the Molecular Weight.
 * 
 * @author eduthie
 */
public class MWT extends MoleculeValueDesc
{
    /**
     * Returns the molecular weight of the given molecule.
     */
    public double value(Molecule mol)
    {
        String mwString = "joelib2.feature.types.MolecularWeight";
        
        try
        {
            FeatureValue mw = new FeatureValue(mwString);
            double result = mw.value(mol);
            if( new Double(result).isNaN() )
            {
                return Double.NaN;
            }
            else
            {
                return result;
            }
        }
        catch( FeatureException fe )
        {
            Logger.getLogger(MWT.class).error(fe.getMessage());
            return Double.NaN;
        }
        
    }

}
