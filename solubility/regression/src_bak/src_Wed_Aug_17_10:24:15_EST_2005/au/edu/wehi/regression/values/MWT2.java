/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import joelib2.feature.*;

/**
 * Returns the square of the Molecular Weight.
 * 
 * @author eduthie
 */
public class MWT2 extends MoleculeValueDesc
{
    /**
     * Returns the square of the molecular weight.
     */
    public double value(Molecule mol)
    {
        String mwString = "joelib2.feature.types.MolecularWeight";
        
        try
        {
            FeatureValue mw = new FeatureValue(mwString);
            double result = mw.value(mol);
            if( new Double(result).isNaN() )
            {
                return Double.NaN;
            }
            else
            {
                return Math.pow(result,2);
            }
        }
        catch( FeatureException fe )
        {
            Logger.getLogger(MWT2.class).error(fe.getMessage());
            return Double.NaN;
        }
        
    }

}
