/*
 * CREATED: Feb 8, 2005
 * FILENAME: TetkoRowTest.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.tests.regression.rows;

import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import junit.framework.TestCase;
import au.edu.wehi.mdm.regression.rows.TetkoRow;
import java.io.*;
import joelib2.molecule.*;
import joelib2.io.*;

/**
 * @author eduthie
 *
 */
public class TetkoRowTest extends TestCase
{

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(TetkoRowTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for TetkoRowTest.
     * @param arg0
     */
    public TetkoRowTest(String arg0)
    {
        super(arg0);
    }

    public void testInit()
    {
        TetkoRow tr = new TetkoRow();
        if( !tr.init() )
        {
            fail("Unable to initialise TetkoRow");
        }
    }

    public void testGetRow()
    {
        try
        {
            TetkoRow tr = new TetkoRow();
            if( !tr.init() )
            {
                fail("Unable to initialise TetkoRow");
            }
            InputStream in = getClass().getResourceAsStream("../test.mol2");
            Molecule mol = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(mol) )
            {
                fail("Failed to read mol");
            }
            
            // get the row
            double[] row = tr.getRow(mol);
            for( int i=0; i < row.length; ++i )
            {
                Double value = new Double(row[i]);
                if( value.isNaN() )
                {
                    fail("Returned value number: " + i + " was NaN");
                }
            }
        }
        catch( IOException ioe)
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }
}
