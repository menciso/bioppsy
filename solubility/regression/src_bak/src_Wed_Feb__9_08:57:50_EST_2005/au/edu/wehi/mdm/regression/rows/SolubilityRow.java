/*
 * CREATED: Feb 7, 2005
 * FILENAME: SolubilityRow.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.regression.rows;

import au.edu.wehi.mdm.regression.values.Solubility;

/**
 * @author eduthie
 *
 * Returns a row of double about a Molecule. The row
 * will contain a single value, the logS value of 
 * the molecule or Double.NaN if the value is not
 * avaliable.
 */
public class SolubilityRow extends MoleculeRowDesc
{

    /**
     * Adds a single value to the SolubilityRow, a
     * Solubility.
     */
    public SolubilityRow()
    {
        super();
        Solubility sv = new Solubility();
        addFeature(sv);
    }

}
