/*
 * CREATED: Feb 11, 2005
 * FILENAME: UtilMatrix.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import org.apache.log4j.Logger;
import java.util.Vector;
import Jama.Matrix;
import java.util.Arrays;

/**
 * @author eduthie
 *
 * A utility class for matricies. Consists of an internal
 * data array of double[][], each double[] in the array
 * representing a row of the matrix. The internal array
 * is just a reference to the array provided.
 */
public class UtilMatrix extends Matrix
{
    static Logger logger = Logger.getLogger(UtilMatrix.class);
    
    /**
     * Initialises the internal matrix with the given array
     * of rows.
     */
    public UtilMatrix(double[][] matrix)
    {
        super(matrix);
    }
    
    /*
     * Prints the given double[][] to the given OutputStream, one
     * row per line, each column separated by spaces
     */
    public boolean printMatrix(OutputStream out)
    {
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
        String string;
        double[][] M = getArray();
        
        for( int i=0; i < M.length; ++i )
        {
            string = "";
            
            for( int j=0; j < M[i].length; ++j )
            {
                string += new Float( (float) M[i][j]).toString() + " ";    
            }
            writer.println(string);
        }
        writer.flush();
        
        return true;
    }
    
    /*
     * Prints the given double[][] to the given filename, one
     * row per line, each column separated by spaces
     */
    public boolean printMatrix(String filename)
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(filename);
            if( !printMatrix(fos) )
            {
                return false;
            }
            fos.close();
            return true;
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error("File Not Found: " + filename);
            return false;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
    }
    
    /**
     * Checks to see if any of the rows are compeletely zero,
     * returns true if at least one is, false otherwise.
     */
    public boolean anyZeroRows()
    {
        double[][] M = getArray();
        
        for( int i=0; i < M.length; ++i )
        {
            double[] row = M[i];
            boolean allZero = true;
            for( int j=0; j < row.length; ++j )
            {
                if( row[j] != 0.0 )
                {
                    allZero = false;
                    break;
                }
            }
            if( allZero )
            {
                logger.info("Row: " + i + " is all zero");
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks to see if any of the columns are compeletely zero,
     * Returns the indicies of the columns that are all zero, 
     * or an empty Vector if none are all zero.
     */
    public Vector<Integer> anyZeroColumns()
    {
        double[][] M = getArray();
        
        Vector<Integer> ret = new Vector<Integer>();
        
        if( M.length <= 0 )
        {
            return ret;
        }
        boolean[] columnsNotZero = new boolean[M[0].length];
        for( int i=0; i< columnsNotZero.length; ++i )
        {
            columnsNotZero[i] = false;
        }
        for( int i=0; i < M.length; ++i)
        {
            double[] row = M[i];
            for( int j=0; j < row.length; ++j )
            {
                if( row[j] != 0.0 )
                {
                    columnsNotZero[j] = true;
                }
            }
        }
        for( int i=0; i < columnsNotZero.length; ++i )
        {
            if( !columnsNotZero[i] )
            {
                ret.add(new Integer(i));
            }
        }
        
        return ret;
    }
    
    /**
     * Normalises the given matrix by subtracting the mean from each column
     * and dividing by its standard deviation. Returns an array  the length
     * of the number of columns in the matrix containing the mean and standard
     * deviation of each column in an array of two doubles. Returns null on
     * error.
     */
    public static double[][] normalise(double[][] matrix)
    {
        if( matrix.length <= 0 )
        {
            return null;
        }
        
        double[][] results = new double[matrix[0].length][];
        
        for( int i=0; i < matrix[0].length; ++i )
        {
            double[] column = new double[matrix.length];
            for( int j=0; j < matrix.length; ++j )
            {
                column[j] = matrix[j][i];
            }
            results[i] = new double[2];
            double mean = Stats.mean(column);
            double sdDev = Stats.standardDeviation(column);
            results[i][0] = mean;
            results[i][1] = sdDev;
            for( int j=0; j < matrix.length; ++j )
            {
                matrix[j][i] = matrix[j][i]-mean;
                if( sdDev != 0.0 )
                {
                    matrix[j][i] = matrix[j][i]/sdDev;
                }
            }
        }
        
        return results;
    }
    
    /**
     * Normalises the given matrix by subtracting the mean from each column
     * and dividing by its standard deviation. Returns an array  the length
     * of the number of columns in the matrix containing the mean and standard
     * deviation of each column in an array of two doubles. Returns null on
     * error.
     */
    public static double[][] normalise(double[][] matrix, double[][] matrixStats)
    {
        if( matrix.length <= 0 )
        {
            return null;
        }
        
        double[][] results = new double[matrix[0].length][];
        
        for( int i=0; i < matrix[0].length; ++i )
        {
            for( int j=0; j < matrix.length; ++j )
            {
                double mean = matrixStats[i][0];
                double sdDev = matrixStats[i][1];
                matrix[j][i] = matrix[j][i]-mean;
                if( sdDev != 0.0 )
                {
                    matrix[j][i] = matrix[j][i]/sdDev;
                }
            }
        }
        
        return results;
    }
    
    public static double[][] reverseNormalise(double[][] matrix, double[][] matrixStats)
    {
        if( matrix.length <= 0 )
        {
            return null;
        }
        
        double[][] results = new double[matrix[0].length][];
        
        for( int i=0; i < matrix[0].length; ++i )
        {
            for( int j=0; j < matrix.length; ++j )
            {
                double mean = matrixStats[i][0];
                double sdDev = matrixStats[i][1];
                matrix[j][i] = matrix[j][i]/sdDev;
                matrix[j][i] = matrix[j][i]+mean;
            }
        }
        
        return results;  
    }
    
    /**
     * Returns a copy of the given array.
     */
    public static double[][] copy(double[][] array)
    {
        double[][] copy = new double[array.length][];
        for( int i=0; i < array.length; ++i )
        {
            copy[i] = new double[array[i].length];
            for( int j=0; j < array[i].length; ++j)
            {
                copy[i][j] = array[i][j];
            }
        }
        return copy;
    }
}
