/*
 * Created on: Apr 29, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import org.apache.log4j.Logger;
import Jama.Matrix;
import au.edu.wehi.regression.util.*;
import java.util.Vector;

/**
 * @author eduthie
 */
public class PLS
{
    private static Logger logger = Logger.getLogger(PLS.class);
    
    Matrix E, F;
    Matrix Fold;
    Matrix wt,t,qt,u,pt,bh;
    Matrix told,ptold;
    Vector<Matrix> Pv,Qv,Wv;
    Vector<Double> Bv;
    double b;
    Matrix B,Q;
    
    int h,p,q,n,k;
    
    double tolerance;
    
    double[][] xMatrixStats;
    double[][] yMatrixStats;
    
    public PLS()
    {
        tolerance = 1E-10;
    }
    
    public double getTolerance()
    {
        return tolerance;
    }
    
    public boolean train(double[][] X, double[][] Y)
    {
        init(X,Y);
        
        boolean fStillReducing = true;
        while( fStillReducing )
        {
            ++h;
            boolean tStillReducing = true;
            while( tStillReducing )
            {
                // calculate wt
                wt = u.transpose().times(E).times(1/u.normF());
                // calculate t
                t = E.times(wt.transpose());
                // calculate qt
                qt = t.transpose().times(F).times(1/t.transpose().times(t).get(0,0));
                // calculate u
                u = F.times(qt.transpose());
                // check for convergence of t
                if( h == 1 )
                {
                    tStillReducing = false;
                }
                else
                {
                    tStillReducing = !hasConverged(t,told);
                }
                told = t;
            }
            logger.info("T has converged");
            
            // calculate pt
            pt = t.transpose().times(E).times(1/t.transpose().times(t).get(0,0));
            // renormalise pt,t,wt
            ptold = pt;
            pt = pt.times(1/pt.normF());
            t = t.times(ptold.normF());
            wt = wt.times(ptold.normF());
            // calculate b
            b = u.transpose().times(t).times(1/t.transpose().times(t).get(0,0)).get(0,0);
            // calculate E and F
            E = E.minus(t.times(pt));
            Fold = F;
            F = F.minus(t.times(qt).times(b));
            // save pt,wt,qt,b
            Pv.add(pt);
            Wv.add(wt);
            Qv.add(qt);
            qt.print(3,3);
            Bv.add(new Double(b));
            
            // check for convergence of F
            if( h >= p )
            {
                fStillReducing = false;
            }
            else
            {
                fStillReducing = !hasConverged(F,Fold);
            }
        }
        logger.info("F has converged");
        
        k = Bv.size();
        B = new Matrix(k,k,0.0);
        for( int i=0; i < k; ++i )
        {
            B.set(i,i,Bv.elementAt(i).doubleValue());
        }
        Q = new Matrix(k,q);
        for( int i=0; i < Qv.size(); ++i)
        {
            Q.setMatrix(i,i,0,q-1,Qv.get(i));
        }
        
        return true;
    }
    
    private void init(double[][] X, double[][] Y)
    {        
        E = new Matrix(X);
        F = new Matrix(Y);
        xMatrixStats = UtilMatrix.normalise(E.getArray());
        yMatrixStats = UtilMatrix.normalise(F.getArray());
        
        h = 0;
        n = E.getRowDimension();
        logger.info("n: " + n);
        p = E.getColumnDimension();
        logger.info("p: " + p);
        q = F.getColumnDimension();
        logger.info("q: " + q);
        
        Fold = new Matrix(n,q,0.0);
        u = F.getMatrix(0,n-1,0,0);
        logger.info("u rows: " + u.getRowDimension());
        logger.info("u columns: " + u.getColumnDimension());
        told = new Matrix(n,1,0.0);
        
        Pv = new Vector<Matrix>();
        Qv = new Vector<Matrix>();
        Wv = new Vector<Matrix>();
        Bv = new Vector<Double>();
    }
    
    public boolean hasConverged(Matrix X, Matrix Xold)
    {   
        if( (X.getRowDimension() != Xold.getRowDimension()) || 
             (X.getColumnDimension() != Xold.getColumnDimension()))
        {
            logger.error("Matricies of unequal size passed to hasConverged()");
            return true;
        }
        Matrix D = X.minus(Xold);
        for( int i=0; i < D.getRowDimension(); ++i )
        {
            for( int j=0; j < D.getColumnDimension(); ++j )
            {
                if( Math.abs(D.get(i,j)) > tolerance )
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    public double[][] predict(double[] x)
    {
        double[][] data = {x};
        return predict(data);
    }
    
    public double[][] predict(double[][] X)
    {   
        Matrix Es = new Matrix(X);
        UtilMatrix.normalise(Es.getArray(),xMatrixStats);
        int ns = X.length;
        Matrix Ts = new Matrix(ns,k);
        Matrix ts;
        
        for( int i=0; i < k; ++i )
        {
            ts = Es.times(Wv.elementAt(i).transpose());
            Es = Es.minus(ts.times(Pv.elementAt(i)));
            Ts.setMatrix(0,ns-1,i,i,ts);
        }
        
        Matrix Ys = Ts.times(B.times(Q));
        
        UtilMatrix.reverseNormalise(Ys.getArray(),yMatrixStats);
        return Ys.getArray();
    }
}
