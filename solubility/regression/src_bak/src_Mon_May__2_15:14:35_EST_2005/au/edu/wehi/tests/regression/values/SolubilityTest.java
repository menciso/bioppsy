/*
 * CREATED: Feb 7, 2005
 * FILENAME: SolubilityTest.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.tests.regression.values;

import java.io.IOException;
import java.io.InputStream;

import au.edu.wehi.regression.values.Solubility;
import junit.framework.TestCase;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.*;

/**
 * @author eduthie
 *
 */
public class SolubilityTest extends TestCase
{

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(SolubilityTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for SolubilityTest.
     * @param arg0
     */
    public SolubilityTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        try
        {            
	        Solubility sol = new Solubility();
	        Molecule mol = new BasicConformerMolecule();
            InputStream in = getClass().getResourceAsStream("../test.sdf");
            if( in.available() == 0 )
            {
                fail("Unable to open test.sdf");
            }
	        BasicReader reader = new BasicReader(in,
	                BasicIOTypeHolder.instance().getIOType("SDF"));
	        if( !reader.readNext(mol) )
	        {
	            fail("Failed to read mol");
	        }
            double value = sol.value(mol);
            double expected = -7.89;
            if( value != expected )
            {
                fail("Incorrect property value returned: " + value +
                    " expected: " + expected);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

}
