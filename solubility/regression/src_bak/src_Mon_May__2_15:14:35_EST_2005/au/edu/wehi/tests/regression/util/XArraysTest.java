/*
 * CREATED: Feb 24, 2005
 * FILENAME: XArraysTest.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.tests.regression.util;

import java.util.Arrays;
import java.util.Vector;

import au.edu.wehi.regression.util.XArrays;
import junit.framework.TestCase;

/**
 * @author eduthie
 *
 */
public class XArraysTest extends TestCase
{

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(XArraysTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for XArraysTest.
     * @param arg0
     */
    public XArraysTest(String arg0)
    {
        super(arg0);
    }

    public void testAddNoDuplicates()
    {
        Vector<int[]> list1 = new Vector<int[]>();
        Vector<int[]> list2 = new Vector<int[]>();
        int[] element;
        
        for( int i=0; i < 10; ++i)
        {
            element = new int[1];
            element[0] = i;
            list1.add(element);
            list2.add(element);
        }
        for( int i=0; i < 20; ++i)
        {
            element = new int[1];
            element[0] = i;
            list1.add(element);
        }

        XArrays.addNoDuplicates(list1,list2);
        
        for( int i=0; i < 20; ++i )
        {
            int[] ret = list2.get(i);
            element = new int[1];
            element[0] = i;
            if( !Arrays.equals(ret,element))
            {
                fail("Incorrect element: " + i);
            }
        }
    }

}
