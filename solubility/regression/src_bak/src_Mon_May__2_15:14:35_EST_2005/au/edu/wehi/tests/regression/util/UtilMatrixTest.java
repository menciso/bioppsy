/*
 * Created on: Apr 29, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.tests.regression.util;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import au.edu.wehi.regression.util.UtilMatrix;
import au.edu.wehi.regression.util.Stats;
import au.edu.wehi.tests.regression.AllTests;

/**
 * @author eduthie
 */
public class UtilMatrixTest extends TestCase
{
    private static Logger logger = Logger.getLogger(UtilMatrixTest.class);
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(UtilMatrixTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for UtilMatrixTest.
     * @param arg0
     */
    public UtilMatrixTest(String arg0)
    {
        super(arg0);
    }
    
    public void testNormalise()
    {
        double tolerance = 1E-10;
        
        int rows = 100;
        int columns = 100;
        double[][] M = new double[rows][columns];
        
        for( int i=0; i < M.length; ++i )
        {
            double[] row = M[i];
            for( int j=0; j < row.length; ++j )
            {
                row[j] = Math.random();
            }
        }
        
        UtilMatrix.normalise(M);
        
        for( int i=0; i < M[0].length; ++i )
        {
            double[] column = new double[M.length];
            for( int j=0; j < M.length; ++j )
            {
                column[j] = M[j][i];
            }
            double mean = Stats.mean(column);
            if( (mean - 0.0) > tolerance )
            {
                fail("MEAN IS NOT ZERO FOR NORMALISED MATRIX");
            }
            double sdDev = Stats.standardDeviation(column);
            if( (sdDev - 1.0) > tolerance )
            {
                fail("SDDEV IS NOT 1 FOR NORMALISED MATRIX");
            }
        }
    }

}
