/*
 * Created on: Jul 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.gui;

import java.awt.*;
import javax.swing.*;
import net.infonode.docking.View;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Vector;
import javax.swing.tree.*;

/**
 * @author eduthie
 */
public class ValuesView extends View
{
    private static Logger logger = Logger.getLogger(ValuesView.class);
    private static String NAME = "Values to Calculate";
    
    private JPanel content;
    private JTree tree;
    private Vector<String> values;
    
    public ValuesView()
    {
        super(NAME, null, new JPanel());
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        if( !loadValues() )
        {
            logger.error("Failed to load values in ValuesView");
            values = new Vector<String>();
        }
        tree = new JTree(values);
        JScrollPane scrollPane = new JScrollPane(tree);
        scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        content.add(scrollPane);
    }
    
    public boolean loadValues()
    {
        if( !loadProperties() )
        {
            return false;
        }
        return true;
    }
    
    /**
     * Loads constants from the properties file
     */
    public boolean loadProperties()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + Constants.PROPERTIES);
                return false;
            }
            
            // get database inputFilename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            String valuesString = typesProp.getProperty("ValuesClassNames");
            if( valuesString == null )
            {
                logger.error("Failed to retrieve ValuesClassNames");
                return false;
            }
            values = new Vector<String>();
            String[] valuesArray = valuesString.trim().split(Constants.INPUT_SEPARATOR);
            for( int i=0; i < valuesArray.length; ++i )
            {
                values.add(valuesArray[i]);
            }
            
            return true;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }        
    }
    
    /**
     * Returns a list of all values currently selected by the user.
     */
    public String[] getSelectedValues()
    {
        int[] selectionRows = tree.getSelectionRows();
        String[] selectedValues = new String[selectionRows.length];
        for( int i=0; i < selectionRows.length; ++i )
        {
            selectedValues[i] = values.elementAt(selectionRows[i]);
        }
        return selectedValues;
    }

}
