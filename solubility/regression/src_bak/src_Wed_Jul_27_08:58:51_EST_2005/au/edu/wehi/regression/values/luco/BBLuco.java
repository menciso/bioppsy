/*
 * Created on: Jul 22, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;

import com.db4o.*;

import au.edu.wehi.regression.Constants;
import au.edu.wehi.regression.Database;
import au.edu.wehi.regression.values.MoleculeValueDesc;
import au.edu.wehi.regression.luco.LucoPLS;

/**
 * The logBB value calculated using the model given in the following publication:<br><br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * Juan M Luco<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404
 * 
 * @author eduthie
 */
public class BBLuco implements MoleculeValueDesc
{
    private LucoPLS lucoPLS = null;
    
    public BBLuco()
    {
        super();
        int expectedSize = 1;
        Database database = Database.instance();
        if( database == null )
        {
            return;
        }
        ObjectContainer db = database.db();
        LucoPLS luco = new LucoPLS(Constants.LUCO_PLS);
        ObjectSet objectSet = db.get(luco);
        if( !(objectSet.size()==expectedSize) )
        {
            Logger.getLogger(getClass()).error("Unexpected number of LUCO_PLS objects returned");
            Logger.getLogger(getClass()).error("Recieved: " + objectSet.size() + " Expected: " + expectedSize);
            db.close();
            return;
        }
        lucoPLS = (LucoPLS) objectSet.next();
        db.close();
    }

    /* (non-Javadoc)
     * @see au.edu.wehi.regression.values.MoleculeValueDesc#value(joelib2.molecule.Molecule)
     */
    public double value(Molecule mol)
    {
        if( lucoPLS != null )
        {
            return lucoPLS.value(mol);
        }
        else
        {
            return Double.NaN;
        }
    }

}
