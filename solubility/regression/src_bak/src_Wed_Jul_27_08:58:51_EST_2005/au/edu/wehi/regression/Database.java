/*
 * Created on: May 31, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import org.apache.log4j.Logger;
import com.db4o.*;
import java.util.Properties;
import java.io.*;

/**
 * A singleton Database instance. Obtain the instance via the static instance() method,
 * please check that the instance is not null.
 * The database connection is then availiable via the db() method.
 * 
 * @author eduthie
 */
public class Database
{
    private static Database database = null;
    private static Logger logger = Logger.getLogger(Database.class);
    
    private String databaseFile = null;
    // the current connection to the database
    private ObjectContainer objectContainer = null;
    
    private Database()
    {
    }

    /**
     * Returns the single Database instance. Returns null on error.
     */
    public static Database instance()
    {
        if( database == null )
        {
            database = new Database();
            if( !database.init() )
            {
                logger.error("Failed to obtain connection to database");
                return null;
            }
        }
        return database;
    }
    
    /**
     * Obtains a connection to the database and returns true. Returns
     * false if a connection could not be reached.
     */
    public boolean init()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + Constants.PROPERTIES);
                return false;
            }
            
            // get database filename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            databaseFile = typesProp.getProperty(Constants.DATABASE_FILE_KEY);
            if( databaseFile == null )
            {
                logger.error("Unable to get property: " + Constants.DATABASE_FILE_KEY +
                        " from file: " + Constants.PROPERTIES);
                return false;
            }
            
            // connect to the database
            objectContainer = Db4o.openFile(databaseFile);
            if(objectContainer == null)
            {
                logger.error("Unable to open database: " + databaseFile);
                return false;
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            if( objectContainer != null )
            {
                objectContainer.close();
            }
            return false;
        }
        
        return true;
    }
    
    /**
     * Returns the current connection to the database.
     */
    public ObjectContainer db()
    {
        if( objectContainer == null )
        {
            logger.error("Unreachable error, there is no current connection to the database");
        }
        return objectContainer;
    }
    
    /**
     * Closes the connection to the database on object disposal
     */
    protected void finalize() throws Throwable
    {
        if( objectContainer != null )
        {
            objectContainer.close();
        }
    }
}
