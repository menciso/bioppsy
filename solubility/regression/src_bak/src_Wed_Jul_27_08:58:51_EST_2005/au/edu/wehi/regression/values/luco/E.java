/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;
import au.edu.wehi.regression.util.*;

/**
 * E - the shape index. Defined as the sum of the 
 * distances plus one of each atom from the longest 
 * chain in the molecule. The longest chain in the
 * molecule is the shortest chain between the two
 * most separated atoms.
 * <br><br>
 * Reference:<br>
 * A Topological Approach to Drug Design<br>
 * Galvez, J. et al.<br>
 * J. Chem. Inf. Comput. Sci. 1995, 35, 272-284
 * 
 * @author eduthie
 */
public class E implements MoleculeValueDesc
{
    /**
     * Returns E - the shape index. Defined as the sum of the 
     * distances plus one of each atom from the longest 
     * chain in the molecule. The longest chain in the
     * molecule is the shortest chain between the two
     * most separated atoms.
     */
    public double value(Molecule molecule)
    {
        Molecule mol = (Molecule) molecule.clone();
        mol.deleteHydrogens();
        int[] longestChain = XMoleculeHelper.getLongestChain(mol);
        double[][] distanceMatrix = XMoleculeHelper.distanceMatrix(mol);
        return calculateE(longestChain,distanceMatrix);
    }
    
    /**
     * Calculates E for the given longest chain in the moleculule
     * and the given Matrix of distances between all the atoms.
     */
    public double calculateE(int[] longestChain, double[][] distanceMatrix)
    {
        double result = 0.0;
        double minDistance;
        double currentDistance;
        
        for( int i=0; i < distanceMatrix.length; ++i )
        {
            minDistance = distanceMatrix[i][0];
            for( int j=0; j < longestChain.length; ++j )
            {
                currentDistance = distanceMatrix[i][longestChain[j]-1];
                if( currentDistance < minDistance )
                {
                    minDistance = currentDistance;
                }
            }
            result += minDistance + 1;
        }
        
        // divide by number of edges in longestChain
        return result / (longestChain.length-1);
    }

}
