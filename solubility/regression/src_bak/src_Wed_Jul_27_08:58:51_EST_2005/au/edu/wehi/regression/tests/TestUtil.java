/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests;

import java.io.IOException;
import java.io.InputStream;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.BasicConformerMolecule;
import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;

import au.edu.wehi.regression.values.FeatureValue;

/**
 * Utilities shared among tests
 * 
 * @author eduthie
 */
public class TestUtil
{
    private static Logger logger = Logger.getLogger(TestUtil.class);
    
    public TestUtil()
    {
        super();
    }

    /**
     *  Returns the test molecule: CH3-C(=O)O
     *  
     * @return - a test Molecule or null on error
     */
    public Molecule getMolecule()
    {
        Molecule mol = null;
        
        try
        {
            InputStream in = getClass().getResourceAsStream("test.mol2");
            mol = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(mol) )
            {
                logger.error("Failed to read mol");
                return null;
            }
            return mol;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return null;
        }
        catch( MoleculeIOException mioe )
        {
            logger.error(mioe.getMessage());
            return null;
        }
    }
}
