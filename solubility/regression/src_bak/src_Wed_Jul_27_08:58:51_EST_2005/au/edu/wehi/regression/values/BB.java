/*
 * Created on: May 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import joelib2.molecule.types.PairData;
import au.edu.wehi.regression.Named;
import org.apache.log4j.Logger;

/**
 * Returns the experimental logBB value of a molecule.
 * 
 * @author eduthie
 */
public class BB implements MoleculeValueDesc, Named
{
    /**
     * Returns the experimental logBB value of a molecule, or
     * Double.NaN if it is not available.
     */
    public double value(Molecule mol)
    {
        PairData pair = mol.getData("logBB");
        if( pair == null )
        {
            return Double.NaN;
        }
        Double value = new Double(pair.toString());
        return value.doubleValue();
    }
    
    public String getName()
    {
        return "Exp logBB";
    }

}
