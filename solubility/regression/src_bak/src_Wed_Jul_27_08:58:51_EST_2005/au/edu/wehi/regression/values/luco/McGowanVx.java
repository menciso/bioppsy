/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.*;
import java.util.Properties;
import org.apache.log4j.Logger;

import au.edu.wehi.regression.values.MoleculeValueDesc;

import java.io.*;
import java.util.*;

/**
 * Calculates the McGowan characteristic molecular volume for
 * a molecule. The McGowan is a simple additive method that sums
 * the volumes of the constituent atoms and subtracts a volume for
 * each bond.
 * <br><br>
 * Reference:<br>
 * The Use of Characteristic Volumes to Measure Cavity Terms in
 * Reversed Phase Liquid Chromatography<br>
 * M.H. Abraham; J.C. McGowan<br>
 * Chromatographia Vol. 23, No. 4, April 1985
 * 
 * @author eduthie
 */
public class McGowanVx implements MoleculeValueDesc
{
    /**
     * Returns Vx in cm3mol-1
     */
    public double value(Molecule mol)
    {
        String currentProperty = null;
        String propertyValue = null;
        
        try
        {
            Properties volumes = new Properties();
            String filename = "mcgowan.properties";
            InputStream volumesStream = getClass().getResourceAsStream(filename);
            if( volumesStream == null )
            {
                Logger.getLogger(getClass()).error("Could not load the file: " + filename);
                return Double.NaN;
            }
            volumes.loadFromXML(getClass().getResourceAsStream("mcgowan.properties"));
            
            double volume = 0.0;
            
            currentProperty = "BOND";
            propertyValue = volumes.getProperty(currentProperty);
            if( propertyValue == null )
            {
                Logger.getLogger(getClass()).error("Missing volume for " + currentProperty + " in file " + filename);
                return Double.NaN;
            }
            double bondVolume = new Double(propertyValue).doubleValue();
            List bonds = mol.getBonds();
            // subtract the BONDS value for every bond in the molecule
            volume -= bondVolume*bonds.size();
            
            for( int i=1; i <= mol.getAtomsSize(); ++i )
            {
                Atom atom = mol.getAtom(i);
                currentProperty = atom.getType().replaceAll("\\d","");
                if( currentProperty.charAt(0) == 'H' )
                {
                    currentProperty = "H";
                }
                propertyValue = null;
                propertyValue = volumes.getProperty(currentProperty);
                if( propertyValue == null)
                {
                    Logger.getLogger(getClass()).error("Missing volume for " + currentProperty + " in file " + filename);
                    return Double.NaN;
                }
                double atomVolume = new Double(propertyValue).doubleValue();
                volume += atomVolume;
            }
            
            return volume;
        }
        catch( IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
            return Double.NaN;
        }
        catch( NumberFormatException nfe )
        {
            Logger.getLogger(getClass()).error("Invalid value for " + currentProperty + " : " + propertyValue);
            return Double.NaN;
        }
    }

}
