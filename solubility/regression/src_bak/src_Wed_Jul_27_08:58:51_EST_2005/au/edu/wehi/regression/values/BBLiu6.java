/*
 * Created on: Jun 20, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import java.io.IOException;
import joelib2.feature.types.MolecularWeight;

/**
 * logBB calculated directly from MLR with two parameters;
 * TPSA and MW
 * <br><br>
 * Reference:<br>
 * Development of Quantitive Structure-Property Relationship Models for Early 
 * ADME Evaluation in Drug Discovery. 2. Blood-Brain Barrier Penetration<br>
 * Ruifeng Liu, Hongmao Sun, and Sung-Sau So<br>
 * J. Chem. Inf. Comput. Sci., Vol. 41, No. 6, 2001
 * 
 * @author eduthie
 */
public class BBLiu6 implements MoleculeValueDesc
{
    /**
     * Returns logBB calculated as:<br>
     * 0.423 - 0.0171*TPSA + 0.000822*MW
     */
    public double value(Molecule mol)
    {
        try
        {
            TPSA tpsa = new TPSA();
            double mwValue = MolecularWeight.getMolecularWeight(mol);
            double tpsaValue = tpsa.value(mol);
            return 0.423 - (0.0171*tpsaValue) + (0.000822*mwValue);
        }
        catch(IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
            return Double.NaN;
        }
    }

}
