/*
 * Created on: Jun 10, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import java.io.IOException;

/**
 * logBB calculated directly from MLR with just one parameter, TPSA.
 * <br><br>
 * Reference:<br>
 * Development of Quantitive Structure-Property Relationship Models for Early 
 * ADME Evaluation in Drug Discovery. 2. Blood-Brain Barrier Penetration<br>
 * Ruifeng Liu, Hongmao Sun, and Sung-Sau So<br>
 * J. Chem. Inf. Comput. Sci., Vol. 41, No. 6, 2001
 * 
 * @author eduthie
 */
public class BBLiu2 implements MoleculeValueDesc
{
    /**
     * Returns logBB calculated as:<br>
     * 0.524 - 0.0153*TPSA
     */
    public double value(Molecule mol)
    {
        try
        {
            TPSA tpsa = new TPSA();
            return 0.524 - (0.0153*tpsa.value(mol));
        }
        catch(IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
            return Double.NaN;
        }
    }

}
