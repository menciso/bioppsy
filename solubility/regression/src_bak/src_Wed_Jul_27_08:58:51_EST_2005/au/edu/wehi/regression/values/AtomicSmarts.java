/*
 * Created on: Jun 10, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;

import au.edu.wehi.regression.rows.MoleculeRowDesc;

import java.io.*;
import java.util.Vector;

/**
 * @author eduthie
 */
public class AtomicSmarts implements MoleculeValueDesc
{
    private String smartsFilename;
    private String coeffFilename;
    private Vector<SmartsMatchCount> smartsVector;
    private Vector<Double> coeffs;
    private int lastSmartsMatched;
    
    public AtomicSmarts(String smartsFilename, String coeffFilename)
        throws IOException
    {
        super();
        this.smartsFilename = smartsFilename;
        this.coeffFilename = coeffFilename;
        smartsVector = new Vector<SmartsMatchCount>();
        
        InputStream in = getClass().getResourceAsStream(smartsFilename);
        if( in == null )
        {
            throw new IOException("Unable to open: " + smartsFilename);
        }
        if( !MoleculeRowDesc.addSmartsToVector(in,smartsVector) )
        {
            throw new IOException("Failed to read smarts from file: " + smartsFilename);
        }
        coeffs = new Vector<Double>();
        in = getClass().getResourceAsStream(coeffFilename);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line;
        while( (line = br.readLine()) != null )
        {
            line = line.trim();
            if( line.length() > 0 )
            {
                Double value = new Double(line);
                coeffs.add(value);
            }
        }
        if( coeffs.size() != smartsVector.size() )
        {
            throw new IOException("PSA file: " + coeffFilename + " contains " + 
                    coeffs.size() + " values. " + smartsVector.size() + " are required");
        }
    }

    public double value(Molecule molecule)
    {
        Molecule mol = molecule;
        double result = 0.0;

        Vector<int[]> alreadyMatched = new Vector<int[]>();
        for( int i = (smartsVector.size()-1); i >= 0; --i )
        {
            SmartsMatchCount smc = smartsVector.elementAt(i);
            Vector<int[]> matches = smc.atoms(mol);
            int matchCount = 0;
            for( int j=0; j < matches.size(); ++j )
            {
                int[] match = matches.elementAt(j);
                boolean foundAlready = false;
                for( int k=0; k < alreadyMatched.size(); ++k )
                {
                    int[] alreadyMatch = alreadyMatched.elementAt(k);
                    if( alreadyMatch[0] == match[0] )
                    {
                        foundAlready = true;
                        break;
                    }
                }
                if( !foundAlready )
                {
                    //Logger.getLogger(getClass()).info("MATCHED SMARTS: " + i);
                    ++matchCount;
                    alreadyMatched.add(match);
                }
            }
            result += (matchCount*coeffs.elementAt(i).doubleValue());
            if( matchCount > 0 )
            {
                lastSmartsMatched = i;
            }
        }
        
        return result;
    }
    
    /**
     * Returns the index (starting from 0) of the last smarts 
     * pattern matched
     */
    public int getLastSmartsMatched()
    {
        return lastSmartsMatched;
    }

}
