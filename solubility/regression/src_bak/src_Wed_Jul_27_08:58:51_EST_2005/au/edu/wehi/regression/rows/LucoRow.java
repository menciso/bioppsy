/*
 * Created on: May 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.rows;

import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.*;
import au.edu.wehi.regression.values.luco.*;
import au.edu.wehi.regression.util.Subgraph;

/**
 * Calculates the independent variables for the Luco model
 * of logBB values. It is a set of mainly topological descriptors.
 * <br><br>
 * Reference:<br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * Juan M Luco<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404
 * 
 * @author eduthie
 */
public class LucoRow extends MoleculeRowDesc
{

    public LucoRow()
    {
        super();
        addFeature(new Chi(3,Subgraph.PATH,true));
        addFeature(new Chi(3,Subgraph.CLUSTER,true));
        // removed since they are 0 for the entire training set
        //addFeature(new Chi(3,Subgraph.PATH_CLUSTER,true)); 
        //addFeature(new Chi(3,Subgraph.CHAIN,true));
        addFeature(new C4PC(false));
        addFeature(new Gk(3));
        addFeature(new Gk(3,true));
        addFeature(new Jk(3));
        addFeature(new Jk(3,true));
        addFeature(new Weiner());
        // removed since it weakens the model
        //addFeature(new Weiner2());
        addFeature(new S());
        addFeature(new E());
        addFeature(new Pk(2));
        addFeature(new Pk(3));
        addFeature(new HBA());
        addFeature(new HBD());
        addFeature(new NumberOfX(-1));
        addFeature(new NumberOfX(7));
        addFeature(new Icooh());
    }

}
