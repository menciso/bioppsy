/*
 * Created on: Jul 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.gui;

import java.awt.*;
import javax.swing.*;
import org.apache.log4j.Logger;
import java.io.File;
import net.infonode.tabbedpanel.*;
import net.infonode.docking.*;

/**
 * @author eduthie
 */
public class CalculationView extends View
{
    private static Logger logger = Logger.getLogger(CalculationView.class);
    private static String name = "Calculations";
    
    private JPanel content;
    private TabbedPanel tabbedPane;
    
    public CalculationView()
    {
        super(name, null, new JPanel());
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        tabbedPane = new TabbedPanel();
        tabbedPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        content.add(tabbedPane);
        validate();
    }
    
    /**
     * Calculates the given values for the given file.
     */
    public void calculate(File file, String[] values)
    {
        Calculation calculation = new Calculation(file,values);
        tabbedPane.addTab(calculation);
        tabbedPane.setSelectedTab(calculation);
        content.validate();
        calculation.runCalc();
    }

}
