/*
 * CREATED: Feb 4, 2005
 * FILENAME: SmartsMatchCount.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.regression.values;

import joelib2.molecule.Molecule;
import au.edu.wehi.mdm.regression.MoleculeValueDesc;
import joelib2.smarts.BasicSMARTSPatternMatcher;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 *
 */
public class SmartsMatchCount extends MoleculeValueDesc
{
    Logger logger = Logger.getLogger(SmartsMatchCount.class);
    
    BasicSMARTSPatternMatcher smarts;
    
    public SmartsMatchCount()
    {
        smarts = new BasicSMARTSPatternMatcher();
    }
    
    /*
     * The given pattern is set as the SMARTS pattern.
     */
    public boolean setPattern(String pattern)
    {
        if( !smarts.init(pattern) )
        {
            String message = "Invalid SMARTS pattern: " + pattern;
            logger.error(message);
            return false;
        }
        else
        {
            return true;
        }
    }
    
    /* 
     * Returns the number of matches for the internal SMARTS
     * pattern for the given molecule.
     */
    public double value(Molecule mol)
    {
        smarts.match(mol);
        return (double) smarts.getMatchesSize();
    }

}
