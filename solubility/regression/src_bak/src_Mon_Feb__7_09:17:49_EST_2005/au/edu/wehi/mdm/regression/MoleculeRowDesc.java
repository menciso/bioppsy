/*
 * CREATED: Feb 3, 2005
 * FILENAME: MoleculeRowDesc.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.regression;

import joelib2.molecule.Molecule;
import java.util.Vector;
import au.edu.wehi.mdm.regression.values.SmartsMatchCount;
import java.io.*;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 *
 * A MolecularRowDesc object produces an array of double values
 * that describe a given molecule. These values are characteristics
 * of a molecule that can either be used as dependent or independent
 * variables in a regression model.
 */
public class MoleculeRowDesc
{
    Logger logger = Logger.getLogger(MoleculeRowDesc.class);
    
    private Vector features;
    
    public MoleculeRowDesc()
    {
        features = new Vector();
    }
    
    /*
     * Returns an array of double values calculated from the
     * given molecule. If there are no features in this
     * MoleculeRowDesc an empty array will be returned.
     */
    public double[] getRow(Molecule mol)
    {
        double[] results = new double[features.size()];
        for( int i=0; i < features.size(); ++i)
        {
            MoleculeValueDesc mv = (MoleculeValueDesc) features.elementAt(i);
            results[i] = mv.value(mol);
        }
        
        return results;
    }
    
    /*
     * Adds a new feature/descriptor to this MoleculeRowDesc. The
     * row that is returned will have an extra value on the
     * end corresponding to the value() result of this added
     * feature.
     */
    public void addFeature(MoleculeValueDesc feature)
    {
        features.add(feature);
    }
    
    /*
     * Adds the SMARTS patterns listed in the given stream,
     * one per line, as features. Returns true on success
     * and false on failure.
     */
    public boolean addSmarts(InputStream in)
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while( (line = br.readLine()) != null )
            {
                SmartsMatchCount smc = new SmartsMatchCount();
                if( !smc.setPattern(line.trim()) )
                {
                    logger.error("Invalid SMARTS pattern: " + line);
                    return false;
                }
                else
                {
                    features.add(smc);
                }
            }
        }
        catch(FileNotFoundException fnfe)
        {
            logger.error(fnfe.getMessage());
            return false;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        finally
        {
            try
            {
                in.close();
            }
            catch( IOException ioe )
            {
            }
        }
        
        return true;
    }
}
