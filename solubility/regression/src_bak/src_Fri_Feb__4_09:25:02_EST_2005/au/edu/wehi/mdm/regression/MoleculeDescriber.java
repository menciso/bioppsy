/*
 * CREATED: Feb 3, 2005
 * FILENAME: MoleculeDescriber.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.regression;

import joelib2.molecule.Molecule;

/**
 * @author eduthie
 *
 * A MolecularDesciber object produces an array of double values
 * that describe a given molecule. These values are characteristics
 * of a molecule that can either be used as dependent or independent
 * variables in a regression model.
 */
public class MoleculeDescriber
{
    /*
     * Returns an array of double values calculated from the
     * given molecule.
     */
    public double[] getRow(Molecule mol)
    {
        return null;
    }
}
