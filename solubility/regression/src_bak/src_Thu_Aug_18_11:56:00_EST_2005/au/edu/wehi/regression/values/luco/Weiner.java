/*
 * Created on: May 13, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.util.*;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * The Weiner descriptor taken from:<br><br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404<br><br>
 * "Wiener index calculated as the half-sum of the off-diagonal elements of the 
 * distance matrix of a graph plus the number of verticies pairs separated by 
 * three edges.
 * 
 * @author eduthie
 */
public class Weiner extends MoleculeValueDesc
{

    /**
     * Returns the "Wiener index calculated as the half-sum of the off-diagonal elements 
     * of the distance matrix of a graph plus the number of verticies pairs separated by 
     * three edges.
     */
    public double value(Molecule molecule)
    {
        Molecule mol = (Molecule) molecule.clone();
        double[][] distanceMatrix = XMoleculeHelper.distanceMatrix(mol);
        double halfSum = 0.0;
        int countOfThreePairs = 0;
        for( int i=0; i < distanceMatrix.length; ++i )
        {
            for( int j=i+1; j < distanceMatrix[i].length; ++j )
            {
                halfSum += distanceMatrix[i][j];
                if( ((int) distanceMatrix[i][j]) == 3 )
                {
                    ++countOfThreePairs;
                }
            }
        }
        return halfSum + countOfThreePairs;
    }

}
