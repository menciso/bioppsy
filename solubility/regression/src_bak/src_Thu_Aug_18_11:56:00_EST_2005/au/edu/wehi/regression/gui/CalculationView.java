/*
 * Created on: Jul 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.gui;

import java.awt.*;
import javax.swing.*;
import org.apache.log4j.Logger;
import java.io.*;
import net.infonode.tabbedpanel.*;
import net.infonode.docking.*;

/**
 * @author eduthie
 */
public class CalculationView extends View
{
    private static Logger logger = Logger.getLogger(CalculationView.class);
    private static String name = "Calculations";
    
    private JPanel content;
    private TabbedPanel tabbedPane;
    private String[] propertiesKeywords;
    private GUI parent;
    
    public CalculationView(GUI parent)
    {
        super(name, null, new JPanel());
        this.parent = parent;
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        tabbedPane = new TabbedPanel();
        tabbedPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        content.add(tabbedPane);
        validate();
    }
    
    public GUI getParent()
    {
        return parent;
    }
    
    /**
     * Calculates the given values for the given file.
     */
    public void calculate(File file, String[] values)
    {
        Calculation calculation = new Calculation(file,values,this);
        if( calculation.askUserToProceed() )
        {
            tabbedPane.addTab(calculation);
            tabbedPane.setSelectedTab(calculation);
            content.validate();
            calculation.runCalc();
        }
    }
    
    /**
     * Removes the given tab from the tabbed pane.
     */
    public void removeTab(Tab tab)
    {
        tabbedPane.removeTab(tab);
    }

}
