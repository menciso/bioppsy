/*
 * Created on: May 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import org.apache.log4j.Logger;

import au.edu.wehi.regression.util.XMoleculeHelper;
import joelib2.molecule.*;

/**
 * S - area shape index defined as E*(L^2), where L
 * is the number of edges in the longest chain of the
 * molecule and E is the shape index (see E.java).
 * <br><br>
 * Reference:<br>
 * A Topological Approach to Drug Design<br>
 * Galvez, J. et al.<br>
 * J. Chem. Inf. Comput. Sci. 1995, 35, 272-284
 * @author eduthie
 */
public class S extends E
{   
    public S()
    {
        super();
    }

    /**
     * Returns E*(L^2) where E is the shape index and L is the number 
     * of edges in the longest chain of the molecule.
     */
    public double value(Molecule molecule)
    {
        Molecule mol = (Molecule) molecule.clone();
        mol.deleteHydrogens();
        int[] longestChain = XMoleculeHelper.getLongestChain(mol);
        double[][] distanceMatrix = XMoleculeHelper.distanceMatrix(mol);
        double L = longestChain.length - 1;
        return calculateE(longestChain,distanceMatrix)*Math.pow(L,2);
    }
}
