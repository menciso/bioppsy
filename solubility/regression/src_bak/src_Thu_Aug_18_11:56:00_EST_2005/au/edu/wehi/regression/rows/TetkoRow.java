/*
 * CREATED: Feb 8, 2005
 * FILENAME: TetkoRow.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.rows;

import java.io.InputStream;
import org.apache.log4j.Logger;
import java.io.*;

import au.edu.wehi.regression.values.FeatureValue;
import joelib2.feature.*;

/**
 * @author eduthie
 *
 * The TetkoRow class calculates the independent
 * variables for the Tetko model. These variables 
 * include a set of SMARTS patterns and the
 * molecular weight. Please use the init() method
 * before using a TetkoRow object.
 */
public class TetkoRow extends MoleculeRowDesc
{
    static Logger logger = Logger.getLogger(TetkoRow.class);
    
    public TetkoRow()
    {
        super();
    }
    
    /*
     * Initialises the SMARTS patterns and the molecular
     * weight variable. Returns true on success and false
     * on failure.
     */
    public boolean init()
    {
        try
        {
            String tetkoSmarts = "tetko.smarts";
            InputStream in = getClass().getResourceAsStream(
                tetkoSmarts);
            if( in.available() == 0 )
            {
                logger.error("Unable to read: " + tetkoSmarts);
                return false;
            }
            
            // add the smarts as features
            if( !addSmarts(in))
            {
                logger.error("Unable to add smarts from: " + tetkoSmarts);
                return false;
            }
            
            // add the MWT
            addFeature(new FeatureValue("joelib2.feature.types.MolecularWeight"));
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        catch( FeatureException fe )
        {
            logger.error(fe.getMessage());
            return false;
        }
            
        return true;
    }
}
