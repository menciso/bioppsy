/*
 * Created on: Jul 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.RootWindow;
import net.infonode.docking.SplitWindow;
import net.infonode.docking.TabWindow;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.ViewMap;
import net.infonode.docking.View;
import net.infonode.util.Direction;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import java.io.*;
import java.util.Vector;


/**
 * @author eduthie
 */
public class GUI extends JFrame
{
    private static Logger logger = Logger.getLogger(GUI.class);
    private static String APPLICATION_NAME = "WEHI Molecule Property Calculator";
    private static String FILE_VIEW = "File of Molecules";
    private static float TOP_WINDOWS_FRACTION = 0.3f;
    private static float BOTTOM_WINDOWS_FRACTION = 0.5f; 
    private static int WIDTH = 800;
    private static int HEIGHT = 900;
    
    private ViewMap viewMap;
    private RootWindow rootWindow = null;
    private ValuesView valuesView;
    private CalculationView calcView;
    
    private JFileChooser fileChooser;
    private JPanel filePanel;
    private View fileView;
    
    
    public static void main(String[] args)
    {
        GUI gui = new GUI();
    }
    
    public GUI()
    {
        super(APPLICATION_NAME);
        javax.swing.SwingUtilities.invokeLater
        (
            new Runnable() 
            {
                public void run() 
                {
                    createAndShowGUI();
                }
            }
        );
    }
    
    public void createAndShowGUI()
    {
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(WIDTH,HEIGHT));
        setVisible(true);
        
        viewMap = new ViewMap();
        filePanel = new JPanel();
        filePanel.setLayout(new BoxLayout(filePanel,BoxLayout.PAGE_AXIS));
        fileChooser = new JFileChooser();
        fileChooser.setAlignmentX(Component.LEFT_ALIGNMENT);
        fileChooser.addActionListener(new FileChooserActionListener());
        fileBrowserEnabled(false);
        filePanel.add(fileChooser);
        fileView = new View(FILE_VIEW,null,filePanel);
        viewMap.addView(0,fileView);
        valuesView = new ValuesView(this);
        viewMap.addView(1,valuesView);
        calcView = new CalculationView(this);
        viewMap.addView(2,calcView);
        
        rootWindow = DockingUtil.createRootWindow(viewMap, true);
        rootWindow.getWindowBar(Direction.DOWN).setEnabled(true);
        
        DockingWindow top = new SplitWindow(true,TOP_WINDOWS_FRACTION,
                valuesView,fileView);
        rootWindow.setWindow(new SplitWindow(false,TOP_WINDOWS_FRACTION,
                    top,calcView));
        
        setContentPane(rootWindow);
        fileView.restoreFocus();
        validate();
    }
    
    /**
     * Enables or disables the file browser
     */
    public void fileBrowserEnabled(boolean state)
    {
        fileChooser.setVisible(state);
        fileChooser.validate();
    }
    
    public class FileChooserActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if( e.getActionCommand().equals(JFileChooser.APPROVE_SELECTION) )
            {
                File file = ((JFileChooser) e.getSource()).getSelectedFile();
                String[] selectedValues = valuesView.getSelectedValues();
                calcView.calculate(file,selectedValues);
            }
        }
    }

}
