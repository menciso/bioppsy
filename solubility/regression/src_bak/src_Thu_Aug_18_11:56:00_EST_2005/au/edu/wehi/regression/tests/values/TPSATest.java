/*
 * Created on: Jun 6, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values;

import java.io.*;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.IOType;
import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.TPSA;

/**
 * @author eduthie
 */
public class TPSATest extends TestCase
{
    private BasicMoleculeVector molecules;
    private static Logger logger = Logger.getLogger(TPSATest.class);

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(TPSATest.class);
    }
    
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        String name = "tpsaTest.mol2";
        InputStream in = getClass().getResourceAsStream(name);
        IOType ioType = BasicIOTypeHolder.instance().getIOType("mol2");
        molecules = new BasicMoleculeVector(in,ioType,ioType);
    }

    public void testValue()
    {
        try
        {
            TPSA tpsa = new TPSA();
            for( int i=0; i < molecules.getSize(); ++i )
            {
                Molecule mol = molecules.getMol(i);
                
                // correct for kekulization, since a N atom
                // gains a + charge in a double ring (etc..)
                if( i == 19 )
                {
                    mol.getAtom(1).setFormalCharge(0);
                }
                if( i == 20 )
                {
                    mol.getAtom(8).setFormalCharge(0);
                }
                
                double value = tpsa.value(mol);
                //logger.info("VALUE " + i + " : " + value);
                assertEquals(i,tpsa.getLastSmartsMatched());
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
