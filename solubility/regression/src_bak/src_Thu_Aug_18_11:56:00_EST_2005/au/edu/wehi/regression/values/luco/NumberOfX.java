/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.molecule.*;

import org.apache.log4j.Logger;

import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * NumberofX counts the number of atoms with a particular atomic
 * number in a given Molecule.
 * 
 * @author eduthie
 */
public class NumberOfX extends MoleculeValueDesc
{
    private int atomicNumber;
    
    /**
     * @param atomicNumber - atomic number of atom type to count.
     * If set to -1 the number of elements 
     */
    public NumberOfX(int atomicNumber)
    {
        this.atomicNumber = atomicNumber;
    }

    /**
     * Returns the number of X atoms in the molecule where X is the atom with
     * the atomic number provided to the constructor.
     */
    public double value(Molecule mol)
    {
        int num = 0;
        
        if( atomicNumber <= 0 )
        {
            return mol.getAtomsSize();
        }
        
        for(int i = 1; i <= mol.getAtomsSize(); ++i )
        {
            Atom atom = mol.getAtom(i);
            if( atom.getAtomicNumber() == atomicNumber )
            {
                ++num;
            }
        }
        
        return num;
    }

}
