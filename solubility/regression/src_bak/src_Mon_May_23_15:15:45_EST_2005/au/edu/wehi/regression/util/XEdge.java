/*
 * Created on: May 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import org.apache.log4j.Logger;
import joelib2.molecule.Bond;

/**
 * @author eduthie
 */
public class XEdge implements Comparable
{
    private int first;
    private int second;
    private static Logger logger = Logger.getLogger(XEdge.class);
    
    public XEdge(Bond bond)
    {
        this.first = bond.getBeginIndex();
        this.second = bond.getEndIndex();
    }
    
    public int compareTo(Object other)
    {
        XEdge otherEdge = (XEdge) other;
        if( (otherEdge.getFirst() == getFirst()) &&
            (otherEdge.getSecond() == getSecond()) )
        {
            return 0;
        }
        if( (otherEdge.getFirst() == getSecond()) &&
                (otherEdge.getSecond() == getFirst()) )
        {
                return 0;
        }
        if( getFirst() < otherEdge.getFirst() )
        {
            return -1;
        }
        else if( getFirst() == otherEdge.getFirst() )
        {
            if( getSecond() < otherEdge.getSecond() )
            {
                return -1;
            }
            if( getSecond() == otherEdge.getSecond() )
            {
                return 0;
            }
            if( getSecond() > otherEdge.getSecond() )
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
        logger.error("Unreachable error");
        return 0;
    }
    
    public int getFirst()
    {
        return first;
    }
    
    public int getSecond()
    {
        return second;
    }
}
