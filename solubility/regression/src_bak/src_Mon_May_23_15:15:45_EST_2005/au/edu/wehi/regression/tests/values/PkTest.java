/*
 * Created on: May 17, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values;

import java.io.IOException;
import java.io.InputStream;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.BasicConformerMolecule;
import joelib2.molecule.Molecule;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.Pk;
import java.util.Vector;

/**
 * @author eduthie
 */
public class PkTest extends TestCase
{
    private Molecule current;
    private Molecule benzene;
    private Molecule hexane;
    private static Logger logger = Logger.getLogger(PkTest.class);
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(PkTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            InputStream in = getClass().getResourceAsStream("current.mol2");
            current = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(current) )
            {
                fail("Failed to read current");
            }
            in = getClass().getResourceAsStream("benzene.mol2");
            benzene = new BasicConformerMolecule();
            reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(benzene) )
            {
                fail("Failed to read benzene");
            }
            String name = "2,3,4,5-tetramethylhexane.mol2";
            in = getClass().getResourceAsStream(name);
            hexane = new BasicConformerMolecule();
            reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(hexane) )
            {
                fail("Failed to read: " + name);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for PkTest.
     * @param arg0
     */
    public PkTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        Pk pk = new Pk(1);
        double value = pk.value(hexane);
        assertEquals(3.0,value);
        value = pk.value(benzene);
        assertEquals(0.0,value);
        value = pk.value(current);
        assertEquals(0.0,value);
        
        pk = new Pk(2);
        value = pk.value(hexane);
        assertEquals(2.0,value);
        
        pk = new Pk(3);
        value = pk.value(hexane);
        assertEquals(1.0,value);

        pk = new Pk(4);
        value = pk.value(hexane);
        assertEquals(0.0,value);
    }

}
