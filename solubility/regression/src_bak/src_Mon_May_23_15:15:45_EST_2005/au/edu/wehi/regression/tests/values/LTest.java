/*
 * Created on: May 13, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values;

import java.io.IOException;
import java.io.InputStream;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.L;

/**
 * @author eduthie
 */
public class LTest extends TestCase
{
    private static Logger logger = Logger.getLogger(LTest.class);
    private Molecule current;
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(LTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            InputStream in = getClass().getResourceAsStream("current.mol2");
            current = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(current) )
            {
                fail("Failed to read butane");
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for LTest.
     * @param arg0
     */
    public LTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        L l = new L();
        double value = l.value(current);
        assertEquals(11,(int) value);
    }

}
