/*
 * CREATED: Feb 7, 2005
 * FILENAME: Solubility.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import joelib2.molecule.types.PairData;

/**
 * @author eduthie
 * 
 * A MoleculeValueDesc that returns the aqueous property of 
 * a Molecule as logS, where S is the aqeous property in
 * mol/L.
 */
public class Solubility extends MoleculeValueDesc
{

    /*
     * Returns logS, where S is the property in mol/L.
     * Returns Double.NaN if the property value is not
     * found.
     */
    public double value(Molecule mol)
    {
        PairData pair = mol.getData("logS");
        if( pair == null )
        {
            return Double.NaN;
        }
        Double value = new Double(pair.toString());
        return value.doubleValue();
    }

}
