/*
 * Created on: May 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.util;

import java.io.IOException;
import java.io.InputStream;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.*;
import joelib2.util.iterator.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.util.*;
import java.util.*;

/**
 * @author eduthie
 */
public class GraphTest extends TestCase
{
    private Molecule tetramethylbutane;
    private Molecule cyclopropane;
    private static Logger logger = Logger.getLogger(GraphTest.class);
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(GraphTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            InputStream in = getClass().getResourceAsStream("tetramethylbutane.mol2");
            tetramethylbutane = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(tetramethylbutane) )
            {
                fail("Failed to read tetramethylbutane");
            }
            String string = "cyclopropane.mol2";
            in = getClass().getResourceAsStream(string);
            cyclopropane = new BasicConformerMolecule();
            reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(cyclopropane) )
            {
                fail("Failed to read " + string);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for GraphTest.
     * @param arg0
     */
    public GraphTest(String arg0)
    {
        super(arg0);
    }

    public void testSubgraphs()
    {        
        Set<Subgraph> subgraphs = GraphHelper.subgraphs(tetramethylbutane,0);
        assertEquals(8,subgraphs.size());
        subgraphs = GraphHelper.subgraphs(tetramethylbutane,1);
        
        tetramethylbutane.deleteHydrogens();
        BondIterator bi = tetramethylbutane.bondIterator();
        while( bi.hasNext() )
        {
            Bond bond = bi.nextBond();
            Subgraph subgraph = new Subgraph();
            subgraph.addNodeEdge(new Integer(bond.getBeginIndex()), new XEdge(bond));
            subgraph.addNodeEdge(new Integer(bond.getEndIndex()), new XEdge(bond));
            if( !subgraphs.contains(subgraph) )
            {
                logger.info("Edge not found: " + subgraph);
            }
        }
        assertEquals(7,subgraphs.size());
        subgraphs = GraphHelper.subgraphs(tetramethylbutane,2);
        assertEquals(12,subgraphs.size());
        subgraphs = GraphHelper.subgraphs(tetramethylbutane,3);
        assertEquals(17,subgraphs.size());
        subgraphs = GraphHelper.subgraphs(tetramethylbutane,7);
        assertEquals(1,subgraphs.size());
        
        // test with a cyclic molecule
        subgraphs = GraphHelper.subgraphs(cyclopropane,0);
        assertEquals(3, subgraphs.size());
        subgraphs = GraphHelper.subgraphs(cyclopropane,1);
        assertEquals(3, subgraphs.size());
        subgraphs = GraphHelper.subgraphs(cyclopropane,2);
        assertEquals(3, subgraphs.size());
        subgraphs = GraphHelper.subgraphs(cyclopropane,3);
        assertEquals(1, subgraphs.size());
        subgraphs = GraphHelper.subgraphs(cyclopropane,4);
        assertEquals(0, subgraphs.size());
    }

}
