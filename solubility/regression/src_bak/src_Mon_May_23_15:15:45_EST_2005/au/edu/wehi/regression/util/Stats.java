/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import java.util.Vector;
import org.apache.log4j.Logger;

/**
 * static statistics utility functions.
 * 
 * @author eduthie
 */
public class Stats
{
    private static Logger logger = Logger.getLogger(Stats.class);
    
    /**
     * Returns the mean of the array of values.
     */
    public static double mean(double[] x)
    {
        double mean = 0.0;
        for( int i=0; i < x.length; ++i )
        {
            mean += x[i];
        }
        return mean/x.length;
    }
    
    /**************************************************
     * Returns the variance of the given array
     * of doubles.<br>
     * v = ( n*sum(x^2) + sum(x)^2 ) / n(n-1)
     **************************************************/
    public static double variance(double[] values)
    {
        double value;
        double sumX = 0.0;
        double sumX2 = 0.0;
        double n = (double) values.length;

        if( n == 1 )
        {
            return 0.0;
        }
        
        for( int i=0; i < values.length; ++i )
        {
            value = values[i];
            sumX += value;
            sumX2 += Math.pow(value,2);
        }
        
        return ( (n*sumX2) - Math.pow(sumX,2) ) / (n*(n-1.0));
    }

    /****************************************************
     * Returns the standard deviation of the given array
     * of doubles.<br>
     * s = sqrt( ( n*sum(x^2) + sum(x)^2 ) / n(n-1) )
     ***************************************************/
    public static double standardDeviation(double[] values)
    {
        return Math.pow(variance(values),0.5);
    }
}
