/*
 * Created on: May 18, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.*;
import org.apache.log4j.Logger;
import java.util.List;

/**
 * HBD (Hydrogen Bond Donors) is the count of O-H and
 * N-H bonds in the molecule.
 * <br><br>
 * Reference:<br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * Juan M Luco<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404
 * 
 * @author eduthie
 */
public class HBD extends MoleculeValueDesc
{
    private Logger logger;
    
    public HBD()
    {
        logger = Logger.getLogger(getClass());
    }
    
    /**
     * Returns the cound of O-H and N-H bonds in the molecule.
     * The given molecule must have explicit hydrogens included.
     */
    public double value(Molecule mol)
    {
        int count = 0;
        
        List bonds = mol.getBonds();
        for( int i=0; i < bonds.size(); ++i)
        {
            Bond bond = (Bond) bonds.get(i);
            if( bond.getBondOrder() == 1 )
            {
                Atom begin = bond.getBegin();
                Atom end = bond.getEnd();
                if( begin.getAtomicNumber() == 1 )
                {
                    if( isNitrogenOrOxygen(end) )
                    {
                        ++count;
                    }
                }
                else if( end.getAtomicNumber() == 1)
                {
                    if( isNitrogenOrOxygen(begin) )
                    {
                        ++count;
                    }
                }
            }
        }
        return count;
    }
    
    public boolean isNitrogenOrOxygen(Atom atom)
    {
        if( (atom.getAtomicNumber() == 7) ||
            (atom.getAtomicNumber() == 8) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
