/*
 * Created on: May 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import java.util.*;
import org.apache.log4j.Logger;
import joelib2.molecule.Bond;

/**
 * @author eduthie
 */
public class Subgraph
{
    private static Logger logger = Logger.getLogger(Subgraph.class);
    private TreeSet<Integer> nodes;
    private TreeSet<XEdge> edges;
    
    public Subgraph()
    {
        nodes = new TreeSet<Integer>();
        edges = new TreeSet<XEdge>();
    }
    
    public Subgraph(TreeSet<Integer> nodes, TreeSet<XEdge> edges)
    {
        this.nodes = (TreeSet<Integer>) nodes.clone();
        this.edges = (TreeSet<XEdge>) edges.clone();
    }
    
    public boolean addNode(Integer node)
    {
        return nodes.add(node);
    }
    
    public boolean addEdge(XEdge edge)
    {
        return edges.add(edge);
    }
    
    public boolean addNodeEdge(Integer node, XEdge edge)
    {
        if( !nodes.contains(node) && !edges.contains(edge) )
        {
            // if we don't have the node or the edge, add both
            nodes.add(node);
            edges.add(edge);
            return true;
        }
        else if( nodes.contains(node) && !edges.contains(edge) )
        {
            // if we already have the node, but not the edge,
            // we just add the edge
            edges.add(edge);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public TreeSet<Integer> getNodes()
    {
        return nodes;
    }
    
    public TreeSet<XEdge> getEdges()
    {
        return edges;
    }
    
    public Iterator<Integer> nodeIterator()
    {
        return nodes.iterator();
    }
    
    public boolean equals(Object other)
    {
        if( !other.getClass().getName().equals(getClass().getName()) )
        {
            return false;
        }
        
        Subgraph otherSG = (Subgraph) other;
        
        if( !nodes.equals(otherSG.getNodes()) )
        {
            return false;
        }
        
        if( !edges.equals(otherSG.getEdges()) )
        {
            return false;
        }
        
        return true;
    }
    
    public int hashCode()
    {
        return 1;
    }
    
    public Subgraph clone()
    {
        return new Subgraph(nodes,edges);
    }
}
