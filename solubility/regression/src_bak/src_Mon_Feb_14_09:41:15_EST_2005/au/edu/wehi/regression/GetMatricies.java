/*
 * CREATED: Feb 11, 2005
 * FILENAME: GetMatricies.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression;

import java.io.IOException;
import java.io.InputStream;
import joelib2.io.BasicIOType;
import joelib2.io.BasicIOTypeHolder;
import au.edu.wehi.regression.rows.SolubilityRow;
import au.edu.wehi.regression.rows.TetkoRow;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.UtilMatrix;

/**
 * @author eduthie
 *
 * <p>
 * The GetMatricies class is an application that can be run from the 
 * command line to obtain the regression X and Y matricies for a 
 * database of molecules.
 * </p>
 * 
 * <p>
 * USAGE: Provide the filename of the database to process as a single
 * command line argument, the X and Y matricies will be output as
 * files in the current directoy called X.matrix and Y.matrix.
 * </p>
 */
public class GetMatricies
{
    static Logger logger = Logger.getLogger(GetMatricies.class);
    
    private String filename;
    
    public static void main(String[] args)
    {
        PropertyConfigurator.configure(GetMatricies.class.getResource("regression.log4j"));
        GetMatricies gm = new GetMatricies();
        gm.printMatricies();
    }
    
    
    public void printMatricies()
    {
        String filename = "training.sdf";
        String fileType = "SDF";
        
        try
        {
            InputStream trainingStream = getClass().
                getResourceAsStream(filename);
            BasicIOType sdfType = BasicIOTypeHolder.instance().getIOType(fileType);
            RegressionMoleculeVector rmv = new RegressionMoleculeVector(
                trainingStream, sdfType, sdfType);
            TetkoRow tetkoRow = new TetkoRow();
            if( !tetkoRow.init() )
            {
                logger.error("Failed to init TetkoRow");
                return;
            }
            rmv.setIndependentDescriber(tetkoRow);
            SolubilityRow solRow = new SolubilityRow();
            rmv.setDependentDescriber(solRow);
            
            (new UtilMatrix(rmv.getXMatrix())).printMatrix("X.matrix");
            (new UtilMatrix(rmv.getYMatrix())).printMatrix("Y.matrix");
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return;
        }   
        
        logger.info("Matricies printed");
    
    }
}
