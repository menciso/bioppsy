/*
 * CREATED: Feb 11, 2005
 * FILENAME: UtilMatrix.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 *
 * A utility class for matricies. Consists of an internal
 * data array of double[][], each double[] in the array
 * representing a row of the matrix. The internal array
 * is just a reference to the array provided.
 */
public class UtilMatrix
{
    static Logger logger = Logger.getLogger(UtilMatrix.class);
    
    private double[][] M;
    
    /**
     * Initialises the internal matrix with the given array
     * of rows.
     */
    public UtilMatrix(double[][] matrix)
    {
        M = matrix;
    }
    
    /*
     * Prints the given double[][] to the given OutputStream, one
     * row per line, each column separated by spaces
     */
    public boolean printMatrix(OutputStream out)
    {
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
        String string;
        
        for( int i=0; i < M.length; ++i )
        {
            string = "";
            
            for( int j=0; j < M[i].length; ++j )
            {
                string += new Float( (float) M[i][j]).toString() + " ";    
            }
            writer.println(string);
        }
        writer.flush();
        
        return true;
    }
    
    /*
     * Prints the given double[][] to the given filename, one
     * row per line, each column separated by spaces
     */
    public boolean printMatrix(String filename)
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(filename);
            if( !printMatrix(fos) )
            {
                return false;
            }
            fos.close();
            return true;
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error("File Not Found: " + filename);
            return false;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
    }   

}
