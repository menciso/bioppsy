/*
 * CREATED: Feb 8, 2005
 * FILENAME: FeatureValue.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.regression.values;

import joelib2.molecule.Molecule;
import joelib2.feature.*;
import joelib2.feature.result.*;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 *
 * A FeatureValue allows the user to use any Feature as a
 * MoleculeValueDesc as long as it returns a DoubleResult
 * or an IntResult. Provide the name of the required Feature
 * to the FeatureValue(String) constructor. If the given
 * Feature does not return a recognised result the
 * value(Molecule) method will return Double.NaN.
 */
public class FeatureValue extends MoleculeValueDesc
{
    static Logger logger = Logger.getLogger(FeatureValue.class);
    
    private Feature feature;
    private String name;

    /*
     * Sets the internal Feature given the Feature name feature.
     * To see avaliable Feature names use the 
     * joelib2.feature.FeatureHelper.getFeatureNames() method which
     * returns a java.util.Enumeration of String Feature names.
     * A FeatureException is thrown if the given feature name
     * is not currently avaliable.
     */
    public FeatureValue(String feature)
        throws FeatureException
    {
        name = feature;
        this.feature = FeatureFactory.getFeature(feature);
        if( this.feature == null )
        {
            throw new FeatureException("Feature not avaliable: " + feature);
        }
    }

    /*
     * Returns the double value calculated by the internal Feature for
     * the given Molecule. The Feature's FeatureResult is cast to
     * a DoubleResult and the doubleValue() is returned. If this
     * fails an IntResult is attempted. If this is unsuccessfull 
     * Double.NaN is returned.
     */
    public double value(Molecule mol)
    {
        FeatureResult result = null;
        
        try
        {
            result = feature.calculate(mol);
            if( result == null )
            {
                logger.error("Failed to calculate feature: " + name);
                return Double.NaN;
            }
            DoubleResult doubleR = (DoubleResult) result;
            return doubleR.getDouble();
        }
        catch( ClassCastException cce )
        {
            try
            {
                IntResult intR = (IntResult) result;
                return (double) intR.getInt();
            }
            catch( ClassCastException cce2 )
            {
                logger.error("Feature did not return an IntResult: " + name);
            }
            
            logger.error("Feature did not return a DoubleResult: " + name);
            return Double.NaN;
        }
        catch( FeatureException fe )
        {
            logger.error(fe.getMessage());
            return Double.NaN;
        }
    }

}
