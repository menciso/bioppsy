/*
 * CREATED: Feb 3, 2005
 * FILENAME: RegressionMoleculeVector.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.regression;

import java.io.IOException;
import java.io.InputStream;

import joelib2.io.IOType;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.BasicMoleculeVector;

/**
 * @author eduthie
 *
 * A RegressionMoleculeVector is simply a set of Molecule objects
 * which can be loaded from a file.
 * The methods getXMatrix() and getYMatrix() can be used to form
 * and use models based on this set.
 * Before using a RegressionMoleculeVector the MolecularDescribers
 * must be set using setIndependentDescriber() and 
 * setDependentDescriber().
 */
public class RegressionMoleculeVector extends BasicMoleculeVector
{
    private MoleculeRowDesc xDesc, yDesc;

    /**
     * 
     */
    public RegressionMoleculeVector()
    {
        super();
    }

    /**
     * @param ifs
     * @throws java.io.IOException
     */
    public RegressionMoleculeVector(InputStream ifs) 
        throws IOException
    {
        super(ifs);
    }

    /**
     * @param ifs
     * @param nToRead
     * @throws java.io.IOException
     */
    public RegressionMoleculeVector(InputStream ifs, int nToRead)
        throws IOException
    {
        super(ifs, nToRead);
    }

    /**
     * @param ifs
     * @param inType
     * @param outType
     * @throws java.io.IOException
     */
    public RegressionMoleculeVector(InputStream ifs, IOType inType, IOType outType)
        throws IOException
    {
        super(ifs, inType, outType);
    }

    /**
     * @param ifs
     * @param inType
     * @param outType
     * @param nToRead
     * @throws java.io.IOException
     * @throws joelib2.io.MoleculeIOException
     */
    public RegressionMoleculeVector(InputStream ifs, IOType inType, IOType outType, int nToRead) 
        throws IOException, MoleculeIOException
    {
        super(ifs, inType, outType, nToRead);
    }
    
    /*
     * Returns the X matrix of independent variables for this set of molecules.
     * Returns a double[r][c] where r is the number of molecules in this
     * set and c is the number of values returned by the independent
     * MoleculeRowDesc set in setIndependentDescriber().
     */
    public double[][] getXMatrix()
    {   
        return null;
    }

    /*
     * Returns the Y matrix of independent variables for this set of molecules.
     * Returns a double[r][c] where r is the number of molecules in this
     * set and c is the number of values returned by the dependent 
     * MolecularDescriber set in setDependentDescriber().
     */
    public double[][] getYMatrix()
    {
        return null;
    }
    
    /*
     * Sets the MolecularDescriber used for each molecule to calculate
     * each row in the X matrix.
     */
    public void setIndependentDescriber(MoleculeRowDesc independent)
    {
        xDesc = independent;
    }
    
    /*
     * Sets the MolecularDescriber used for each molecule to calculate
     * each row in the Y matrix.
     */
    public void setDependentDescriber(MoleculeRowDesc dependent)
    {
        yDesc = dependent;
    }
}
