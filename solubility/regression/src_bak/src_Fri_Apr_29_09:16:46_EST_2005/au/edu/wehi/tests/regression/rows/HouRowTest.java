/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.tests.regression.rows;

import java.io.IOException;
import java.io.InputStream;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.BasicConformerMolecule;
import joelib2.molecule.Molecule;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import au.edu.wehi.regression.rows.HouRow;
import au.edu.wehi.tests.regression.AllTests;

/**
 * @author eduthie
 */
public class HouRowTest extends TestCase
{
    private static Logger logger = Logger.getLogger(HouRowTest.class);

    public static void main(String[] args)
    {
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for HouRowTest.
     * @param arg0
     */
    public HouRowTest(String arg0)
    {
        super(arg0);
    }
    
    public void testGetRow()
    {
        try
        {
            HouRow hr = new HouRow();
            if( !hr.init() )
            {
                fail("Unable to initialise HouRow");
            }
            InputStream in = getClass().getResourceAsStream("../test.mol2");
            Molecule mol = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(mol) )
            {
                fail("Failed to read mol");
            }
            
            // get the row
            double[] row = hr.getRow(mol);
            for( int i=0; i < row.length; ++i )
            {
                Double value = new Double(row[i]);
                if( value.isNaN() )
                {
                    fail("Returned value number: " + i + " was NaN");
                }
            }
        }
        catch( IOException ioe)
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

}
