/*
 * Created on: Apr 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.tests.regression.values;

import java.io.InputStream;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

import au.edu.wehi.regression.values.*;
import au.edu.wehi.tests.regression.AllTests;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.molecule.*;
import java.io.*;
import joelib2.io.*;
import java.util.Vector;

/**
 * Tests the SingleMatchSmarts functions
 * 
 * @author eduthie
 */
public class SingleMatchSmartsTest extends TestCase
{
    private static Logger logger = Logger.getLogger(SingleMatchSmartsTest.class);

    public static void main(String[] args)
    {
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for SingleMatchSmartsTest.
     * @param arg0
     */
    public SingleMatchSmartsTest(String arg0)
    {
        super(arg0);
    }
    
    public void test()
    {
        int NUM_SMS = 10;
        String pattern1 = "[C]";
        String pattern2 = "[OH]";
        
        try
        {
        
            SingleMatchSmartsHelper helper = new SingleMatchSmartsHelper();;
            for( int i=0; i < NUM_SMS; ++i )
            {
                SingleMatchSmarts sms = new SingleMatchSmarts(helper);
                sms.addPattern(pattern1);
                sms.addPattern(pattern2);
                helper.addSingleMatchSmarts(sms);
            }
            
            Molecule mol = new BasicConformerMolecule();
            InputStream in = getClass().getResourceAsStream("../test.mol2");
            if( in.available() == 0 )
            {
                fail("Unable to open test.mol2");
            }
            BasicReader reader = new BasicReader(in,
                    BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(mol) )
            {
                fail("Failed to read mol");
            }
            
            Vector<int[]> matches = helper.getMatches(NUM_SMS-1,mol);
            for( int i=0; i < matches.size(); ++i )
            {
                assertEquals(i+1,matches.elementAt(i)[0]);
            }
            for( int i=0; i < (NUM_SMS-1); ++i )
            {
                matches = helper.getMatches(i,mol);
                assertEquals(0,matches.size());
            }
        
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

}
