/*
 * CREATED: Feb 24, 2005
 * FILENAME: SingleMatchSmarts.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import java.util.Vector;

/**
 * @author eduthie
 *
 */
public class SingleMatchSmarts extends SmartsMatchCount
{
    private SingleMatchSmartsHelper helper;
    private int position; // position in helper
    
    /**
     * @param helper - Matches of all SingleMatchSmarts
     * objects in this SingleMatchSmarts' associated list.
     */
    public SingleMatchSmarts(SingleMatchSmartsHelper helper)
    {
        super();
        this.helper = helper;
        this.position = helper.addSingleMatchSmarts(this);
    }
    
    /**
     * Returns the number of atoms in the given Molecule
     * that match this SingleMatchSmarts pattern but do not match
     * any of the patterns further on in the list.
     */
    public double value(Molecule mol)
    {
        return helper.getMatches(position,mol).size();
    }
    
    /**
     * Returns a list of atoms in the given Molecule
     * that match this SingleMatchSmarts pattern but do not match
     * any of the patterns further on in the list.
     */
    public Vector<int[]> getMatches(Molecule mol)
    {
        return helper.getMatches(position,mol);
    }

}
