/*
 * CREATED: Feb 11, 2005
 * FILENAME: UtilMatrix.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import org.apache.log4j.Logger;
import java.util.Vector;

/**
 * @author eduthie
 *
 * A utility class for matricies. Consists of an internal
 * data array of double[][], each double[] in the array
 * representing a row of the matrix. The internal array
 * is just a reference to the array provided.
 */
public class UtilMatrix
{
    static Logger logger = Logger.getLogger(UtilMatrix.class);
    
    private double[][] M;
    
    /**
     * Initialises the internal matrix with the given array
     * of rows.
     */
    public UtilMatrix(double[][] matrix)
    {
        M = matrix;
    }
    
    /*
     * Prints the given double[][] to the given OutputStream, one
     * row per line, each column separated by spaces
     */
    public boolean printMatrix(OutputStream out)
    {
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
        String string;
        
        for( int i=0; i < M.length; ++i )
        {
            string = "";
            
            for( int j=0; j < M[i].length; ++j )
            {
                string += new Float( (float) M[i][j]).toString() + " ";    
            }
            writer.println(string);
        }
        writer.flush();
        
        return true;
    }
    
    /*
     * Prints the given double[][] to the given filename, one
     * row per line, each column separated by spaces
     */
    public boolean printMatrix(String filename)
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(filename);
            if( !printMatrix(fos) )
            {
                return false;
            }
            fos.close();
            return true;
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error("File Not Found: " + filename);
            return false;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
    }
    
    /**
     * Checks to see if any of the rows are compeletely zero,
     * returns true if at least one is, false otherwise.
     */
    public boolean anyZeroRows()
    {
        for( int i=0; i < M.length; ++i )
        {
            double[] row = M[i];
            boolean allZero = true;
            for( int j=0; j < row.length; ++j )
            {
                if( row[j] != 0.0 )
                {
                    allZero = false;
                    break;
                }
            }
            if( allZero )
            {
                logger.info("Row: " + i + " is all zero");
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks to see if any of the columns are compeletely zero,
     * Returns the indicies of the columns that are all zero, 
     * or an empty Vector if none are all zero.
     */
    public Vector<Integer> anyZeroColumns()
    {
        Vector<Integer> ret = new Vector<Integer>();
        
        if( M.length <= 0 )
        {
            return ret;
        }
        boolean[] columnsNotZero = new boolean[M[0].length];
        for( int i=0; i< columnsNotZero.length; ++i )
        {
            columnsNotZero[i] = false;
        }
        for( int i=0; i < M.length; ++i)
        {
            double[] row = M[i];
            for( int j=0; j < row.length; ++j )
            {
                if( row[j] != 0.0 )
                {
                    columnsNotZero[j] = true;
                }
            }
        }
        for( int i=0; i < columnsNotZero.length; ++i )
        {
            if( !columnsNotZero[i] )
            {
                ret.add(new Integer(i));
            }
        }
        
        return ret;
    }

}
