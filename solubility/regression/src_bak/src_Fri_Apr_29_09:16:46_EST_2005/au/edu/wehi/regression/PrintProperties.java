/*
 * Created on: Apr 28, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import joelib2.io.*;
import joelib2.molecule.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import java.io.*;
import java.util.Properties;
import com.db4o.*;
import java.util.Vector;
import au.edu.wehi.regression.values.*;

/**
 * PrintProperties can be run from the command line, passing
 * a filename as a single argument. Properties from the
 * Molecules in the given filename are printed as a table
 * to standard out.
 * 
 * @author eduthie
 */
public class PrintProperties
{
    private static Logger logger = Logger.getLogger(PrintProperties.class);
    private static int NUM_COLUMNS = 1;
    private String filename = null;
    
    public PrintProperties(String filename)
    {
        this.filename = filename;    
    }

    public static void main(String[] args)
    {
        PropertyConfigurator.configure(
                CreateDatabase.class.getResource("regression.log4j"));
        
        if( args.length != 1 )
        {
            logger.error("Incorrect number of arguments. Please provide a single " +
                    "argument, the filename of a file containing molecules");
            System.exit(0);
        }
        else
        {
            PrintProperties pp = new PrintProperties(args[0]);
            pp.printProperties();
        }
    }
    
    /**
     * Prints the properties.
     *
     */
    public void printProperties()
    {
        String databaseFile;
        ObjectContainer db = null;
     
        try
        {
            // load molecules from the filename
            if( !BasicIOTypeHolder.instance().canReadExtension(filename) )
            {
                logger.error("Invalid file extension: " + filename);
                return;
            }
            BasicIOType type = BasicReader.checkGetInputType(filename);
            if( type == null )
            {
                logger.error("Invalid file extension: " + filename);
                return;
            }
            InputStream in = new FileInputStream(filename);
            if( in == null )
            {
                logger.error("Unable to read file: " + filename);
                return;
            }
            RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
            InputStream stream = getClass().getResourceAsStream(CreateDatabase.PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + CreateDatabase.PROPERTIES);
                return;
            }
            
            // get database filename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            databaseFile = typesProp.getProperty(CreateDatabase.DATABASE_FILE_KEY);
            if( databaseFile == null )
            {
                logger.error("Unable to get property: " + CreateDatabase.DATABASE_FILE_KEY +
                        " from file: " + CreateDatabase.PROPERTIES);
                return;
            }
            
            // connect to the database
            db = Db4o.openFile(databaseFile);
            if(db == null)
            {
                logger.error("Unable to open database: " + databaseFile);
                return;
            }
            
            Test test = new Test();
            ObjectSet testOS = db.get(test);
            logger.info("NUMBER OF OBJECTS: " + testOS.size());
            while( testOS.hasNext() )
            {
                test = (Test) testOS.next();
                logger.info("Name: " + test.getName());
                Vector<MoleculeValueDesc> vector = test.getVector();
                logger.info("Vector size: " + vector.size());
                MoleculeValueDesc mvd = vector.elementAt(1);
                logger.info("CLASS OF MVD: " + mvd.getClass());
            }
            
            // retrieve the HOU_MLR by example
            MLR mlr = new MLR(CreateDatabase.HOU_MLR);
            ObjectSet objectSet = db.get(mlr);
            int expectedSize = 1;
            if( !(objectSet.size()==expectedSize) )
            {
                logger.error("Unexpected number of HOU_MLR objects returned");
                logger.error("Recieved: " + objectSet.size() + " Expected: " + expectedSize);
                return;
            }
            mlr = (MLR) objectSet.next();
            
            // print headings
            System.out.println("Molecule Number,Hou logS");
            
            // print results
            String lineSeparator = System.getProperty("line.separator");
            if( lineSeparator == null )
            {
                lineSeparator = "/n";
            }
            for( int i=0; i < rmv.getSize(); ++i )
            {
                Molecule molecule = rmv.getMol(i);
                System.out.print(i);
                double result = mlr.predict(molecule);
                if( new Double(result).isNaN() )
                {
                    continue;
                }
                System.out.print("," + result);
                System.out.print(lineSeparator);
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
        }
        finally
        {
            if( db != null )
            {
                db.close();
            }
        }
    }
}
