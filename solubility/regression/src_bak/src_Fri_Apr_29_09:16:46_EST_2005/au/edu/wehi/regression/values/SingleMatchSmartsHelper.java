/*
 * CREATED: Feb 24, 2005
 * FILENAME: SingleMatchSmartsHelper.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import java.util.Vector;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 *
 * A utility class used by the SingleMatchSmarts MoleculeValueDesc.
 * On construction a list of SingleMatchSmarts objects are stored.
 * The getMatches(int,Molecule) is then used to retrieve the
 * atoms that match a particular SingleMatchSmarts object in the
 * list. If the given Molecule is different to the last
 * molecule calculated, the helper of all SingleMatchSmarts 
 * patterns are calculated for this new Molecule.
 * <br><br>
 * Each SingleMatchSmarts object will have a copy of a 
 * SingleMatchSmartsHelper list. This is because an atom
 * is only counted as matching a SingleMatchSmarts pattern
 * if it does not match any patterns futher on in
 * the list.
 */
public class SingleMatchSmartsHelper
{
    private Vector<SingleMatchSmarts> smarts;
    private Vector<int[]>[] matches = null;
    private Molecule currentMolecule = null;

    public SingleMatchSmartsHelper()
    {
        smarts = new Vector<SingleMatchSmarts>();
    }
    
    /**
     * Adds the given SingleMatchSmarts to the helper and returns
     * the index of the added sms in the helper.
     * 
     * @param sms - SingleMatchSmarts to add.
     * @return int - position of added smarts in the helper
     */
    public int addSingleMatchSmarts(SingleMatchSmarts sms)
    {
        smarts.add(sms);
        return (smarts.size() - 1);
    }

    /**
     * Returns a list of atoms that match the SingleMatchSmarts
     * pattern at the given index 'position' for the given
     * Molecule 'mol'.
     * 
     * @param position - Index of SingleMatchSmarts object
     * @param mol - Molecule to match
     * @return Vector<int[]> - Vector of atom indicies in
     *  the given molecule that match the SingleMatchSmarts object
     *  with index 'position'.
     */
    public Vector<int[]> getMatches(int position, Molecule mol)
    {
        if( currentMolecule != mol )
        {
            currentMolecule = mol;
            if( !reCalculateMatches() )
            {
                Logger.getLogger(SingleMatchSmartsHelper.class).error("Unable to calculate helper for mol: " + mol.getTitle());
                return null;
            }
        }
        
        if( matches == null )
        {
            Logger.getLogger(SingleMatchSmartsHelper.class).error("Matches array null in SingleMatchSmartsHelper");
            return null;
        }
        else
        {
            if( position >= matches.length )
            {
                throw new IndexOutOfBoundsException("Position out of bounds: " + position);
            }
            else
            {
                return matches[position];
            }
        }
    }
    
    /**
     * Calculates the matching atoms for each SingleMatchSmarts pattern
     * in 'smarts' and stores them in 'helper'. This is begun
     * from the end of 'smarts'. All indicies of atoms that have
     * already been matched are stored in the process. If an
     * atom helper a SingleMatchSmarts pattern, it is only included
     * in 'helper' if has not matched a previous pattern.
     * 
     * @return true on success, false on failure
     */
    private boolean reCalculateMatches()
    {
        if( currentMolecule == null )
        {
            Logger.getLogger(SingleMatchSmartsHelper.class).error("No current molecule in SingleMatchSmartsHelper");
            return false;
        }
        
        Vector<int[]> alreadyMatched = new Vector<int[]>();
        Vector<int[]> currentSmartsMatches;
        
        matches = new Vector[smarts.size()];
        
        for( int i = smarts.size() - 1; i >= 0; --i)
        {
            currentSmartsMatches = new Vector<int[]>();
            SingleMatchSmarts sms = smarts.elementAt(i);
            Vector<int[]> atoms = sms.atoms(currentMolecule);
            for( int j=0; j < atoms.size(); ++j )
            {
                int[] atomList = atoms.elementAt(j);
                boolean haveAlready = false;
                for( int k=0; (k < alreadyMatched.size()) && !haveAlready; ++k )
                {
                    if(alreadyMatched.elementAt(k)[0] == atomList[0])
                    {
                        haveAlready = true;
                        break;
                    }
                }
                if( !haveAlready )
                {
                    currentSmartsMatches.add(atomList);
                    alreadyMatched.add(atomList);
                }
            }
            matches[i] = currentSmartsMatches;
        }
        
        
        return true;
    }
}
