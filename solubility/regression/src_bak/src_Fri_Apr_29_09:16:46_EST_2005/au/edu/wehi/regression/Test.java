/*
 * Created on: Apr 28, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;
import au.edu.wehi.regression.rows.*;
import au.edu.wehi.regression.values.*;

/**
 * @author eduthie
 */
public class Test
{
    private String name;
    private Vector<MoleculeValueDesc> features;
    
    public Test()
    {
        name = "ya";
        features = new Vector<MoleculeValueDesc>();
        features.add(new Intercept());
        InputStream in = HouRow.class.getResourceAsStream("hou.smarts");
        addSingleMatchSmarts(in);
    }
    
    public String getName()
    {
        return name;
    }
    
    public Vector<MoleculeValueDesc> getVector()
    {
        return features;
    }
    
    public boolean addSmarts(InputStream in)
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while( (line = br.readLine()) != null )
            {
                SmartsMatchCount smc = new SmartsMatchCount();
                String[] splitLine = line.split("[ ]+");
                for( int i=0; i < splitLine.length; ++i )
                {
                    if( !smc.addPattern(splitLine[i]) )
                    {
                        Logger.getLogger(Test.class).error("Invalid SMARTS pattern: " + line);
                        return false;
                    }
                }
                features.add(smc);
            }
        }
        catch(FileNotFoundException fnfe)
        {
            Logger.getLogger(Test.class).error(fnfe.getMessage());
            return false;
        }
        catch( IOException ioe )
        {
            Logger.getLogger(Test.class).error(ioe.getMessage());
            return false;
        }
        finally
        {
            try
            {
                in.close();
            }
            catch( IOException ioe )
            {
            }
        }
        
        return true;
    }
    
    public boolean addSingleMatchSmarts(InputStream in)
    {
        try
        {
            SingleMatchSmartsHelper helper = new SingleMatchSmartsHelper();
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while( (line = br.readLine()) != null )
            {
                SingleMatchSmarts sms = new SingleMatchSmarts(helper);
                String[] splitLine = line.split("[ ]+");
                for( int i=0; i < splitLine.length; ++i )
                {
                    if( !sms.addPattern(splitLine[i]) )
                    {
                       Logger.getLogger(Test.class).error("Invalid SMARTS pattern: " + line);
                        return false;
                    }
                }
                features.add(sms);
            }
        }
        catch(FileNotFoundException fnfe)
        {
            Logger.getLogger(Test.class).error(fnfe.getMessage());
            return false;
        }
        catch( IOException ioe )
        {
            Logger.getLogger(Test.class).error(ioe.getMessage());
            return false;
        }
        finally
        {
            try
            {
                in.close();
            }
            catch( IOException ioe )
            {
            }
        }
        
        return true;
    }
}
