/*
 * CREATED: Feb 14, 2005
 * FILENAME: ANN.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression;

import org.joone.engine.*;
import org.joone.engine.learning.*;
import org.joone.io.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.joone.net.*;
import java.io.*;
import au.edu.wehi.regression.util.XMath;
import java.util.Vector;

/**
 * @author eduthie
 *
 * Base class for training and using an ANN for prediction.
 */
public class ANN implements NeuralNetListener
{
    static Logger logger = Logger.getLogger(ANN.class);
    
    int MAX_CYCLES = (int) 1E6;
    int MAX_TRIES = 100000;
    double MAX_LEARNING = 0.55;
    double MIN_LEARNING = 0.1;
    double LEARNING_INCREMENT = 0.05;
    double ERROR_DIFF_THRESHOLD = 0.01;
    double ERROR_THRESHOLD = 0.5;
    int MAX_LAYERS = 2;
    int LAYER_ROWS = 1000;
    
    double[][] independentM;
    double[][] dependentM;
    int n = 1000;
    int p = 1;
    int testN = 10;
    NeuralNet nnet;
    double currentLearning = 0.02;
    double currentMomentum = 0.6;
    double lastError = 1E10; // extra big
    int tryCount = 0;
    int networkNumber = 0;
    boolean testing = false;
    Vector<Layer> hiddenLayers = new Vector<Layer>();

    public static void main(String[] args)
    {
        PropertyConfigurator.configure(ANN.class.getResource("regression.log4j"));
        ANN ann = new ANN();
        ann.begin();
    }
    
    public void begin()
    {
        constructANN();
        trainANN(currentLearning,currentMomentum);
    }
  
    
    public boolean constructANN()
    {
        if( !setTrainingMatricies() )
        {
            logger.error("Failed to set training matricies");
            return false;
        }
        
        // display input and output matricies
        /*
        PrintStream stream = System.out;
        stream.println("Training X Matrix:");
        (new UtilMatrix(independentM)).printMatrix(stream);
        stream.println("");
        stream.println("Training Y Matrix:");
        (new UtilMatrix(dependentM)).printMatrix(stream);
        */
        
        LinearLayer input = new LinearLayer();
        TanhLayer hidden = new TanhLayer();
        hiddenLayers.add(hidden);
        BiasedLinearLayer output = new BiasedLinearLayer();
        input.setRows(independentM[0].length);
        hidden.setRows(LAYER_ROWS);
        output.setRows(1);
        
        FullSynapse synapse_IH = new FullSynapse();
        FullSynapse synapse_HO = new FullSynapse();
        input.addOutputSynapse(synapse_IH);
        hidden.addInputSynapse(synapse_IH);
        hidden.addOutputSynapse(synapse_HO);
        output.addInputSynapse(synapse_HO);
        
        input.addInputSynapse(getNormInputSynapse(independentM));
        
        TeachingSynapse trainer = new TeachingSynapse();
        MemoryInputSynapse desiredSynapse = new MemoryInputSynapse();
        desiredSynapse.setAdvancedColumnSelector("1-" + dependentM[0].length);
        desiredSynapse.setInputArray(dependentM);
        trainer.setDesired(desiredSynapse);
        output.addOutputSynapse(trainer);
        
        nnet = new NeuralNet();
        nnet.addLayer(input, NeuralNet.INPUT_LAYER);
        nnet.addLayer(hidden, NeuralNet.HIDDEN_LAYER);
        nnet.addLayer(output, NeuralNet.OUTPUT_LAYER);
        nnet.setTeacher(trainer);
        
        Monitor monitor = nnet.getMonitor();
        monitor.setLearningRate(currentLearning);
        monitor.setMomentum(currentMomentum);
        monitor.setTrainingPatterns(independentM.length);
        monitor.setTotCicles(MAX_CYCLES);
        monitor.setLearning(true);
        monitor.addNeuralNetListener(this);
        
        addHiddenLayers();
        
        return true;
    }
    
    public void trainANN(double learning, double momentum)
    {
        Monitor monitor = nnet.getMonitor();
        monitor.setLearningRate(learning);
        monitor.setMomentum(momentum);

        nnet.start();
        nnet.getMonitor().Go();
    }
    
    public boolean testANN()
    {   
        if( !setTestMatricies() )
        {
            logger.error("Failed to set training matricies");
            return false;
        }
        
        Layer input = nnet.getInputLayer();
        input.removeAllInputs();
        
        input.addInputSynapse(getNormInputSynapse(independentM));
        
        Layer output = nnet.getOutputLayer();
        output.removeAllOutputs();
        MemoryOutputSynapse mos = new MemoryOutputSynapse();
        output.addOutputSynapse(mos);
        
        Monitor monitor = nnet.getMonitor();
        monitor.setTotCicles(1);
        monitor.setTrainingPatterns(independentM.length);
        monitor.setLearning(false);
        nnet.start();
        monitor.Go();
        
        double[] pattern;
        int i=0;
        while( (pattern = mos.getNextPattern()) != null )
        {
            System.out.println("CALCULATED: " + pattern[0] +
                " EXPECTED: " + dependentM[i][0]);
            ++i;
        }
        
        return true;
    }
    
    public boolean setTrainingMatricies()
    {
        int i,j;
        independentM = new double[n][p];
        for( i=0; i < n; ++i )
        {
            for( j=0; j < p; ++j )
            {
                independentM[i][j] = Math.random();//*XMath.randomSign();
            }
        }
        
        // set the dependent variable to 
        // z = x^2 + y^2
        dependentM = new double[n][1];
        for( i=0; i < n; ++i )
        {
            for( j=0; j < p; ++j )
            {
                dependentM[i][0] += XMath.stairs(independentM[i][j]);
            }
        }
       
        return true;
    }
    
    public boolean setTestMatricies()
    {
        int i,j;
        independentM = new double[testN][p];
        
        for( i=0; i < testN; ++i )
        {
            for( j=0; j < p; ++j )
            {
                independentM[i][j] = Math.random();//*XMath.randomSign();
            }
        }
        
        dependentM = new double[testN][1];
        for( i=0; i < testN; ++i )
        {
            dependentM[i][0] = 0.0;
            for( j=0; j < p; ++j )
            {
                dependentM[i][0] += XMath.stairs(independentM[i][j]);
            }
        }
        
        return true;
    }
    
    public StreamInputSynapse getNormInputSynapse(double[][] matrix)
    {
        MemoryInputSynapse inputSynapse = new MemoryInputSynapse();
        inputSynapse.setAdvancedColumnSelector("1-" + matrix[0].length);
        inputSynapse.setInputArray(matrix);  
        return inputSynapse;
    }
    
    public void netStopped(NeuralNetEvent nne)
    {
        logger.info("NET STOPPED");
        
        if( !testing )
        {
            Monitor mon = nnet.getMonitor();
            double globalError = mon.getGlobalError();
            
            logger.info("Network number: " + networkNumber);
            
            /*
             * globalError is sometimes NaN, then we want to try and change the
             * network and try again. Otherwise if the globalError is not low
             * enough, we also want to change the network and try again.
             */
            if( (new Double(globalError)).isNaN() || globalError > ERROR_THRESHOLD )
            {
                logger.info("Local minimum reached");
                logger.info("Global error: " + globalError);
                if( !changeNetwork() )
                {
                    logger.error("Network failed to converge");
                }
                else
                {
                    trainANN(currentLearning,currentMomentum);
                }
            }
            else
            {
                logger.info("Network has converged");
                logger.info("Network number: " + networkNumber);
                logger.info("Number of hidden layers: " + hiddenLayers.size());
                logger.info("Learning rate: " + currentLearning);
                logger.info("Global error: " + globalError );
                testing = true;
                testANN();
            }
        }
    }
    
    public void netStoppedError(NeuralNetEvent nne, String error)
    {
        logger.info("NET STOPPED ERROR: " + error);
    }
    
    public void errorChanged(NeuralNetEvent nne)
    {
        //logger.info("ERROR: " + nnet.getMonitor().getGlobalError());
        //logger.info("ERROR CHANGED");
    }
    
    public void cicleTerminated(NeuralNetEvent nne)
    {   
        Monitor mon = nnet.getMonitor();
        double globalError = mon.getGlobalError();
        
        if( (new Double(globalError)).isNaN() )
        {
            logger.info("Error is NaN");
            stopNetwork();
        }
        
        long c = mon.getTotCicles() - mon.getCurrentCicle();
        if ( (c != 0) && ((c % 1000) == 0))
        {
            logger.info("ERROR: " + globalError);
        }
        
        if( (c!= 0) && (Math.abs(globalError-lastError) > ERROR_DIFF_THRESHOLD) )
        {
            logger.info("Have hit minimum");
            stopNetwork();
        }
    }
    
    public void stopNetwork()
    {
        ++networkNumber;
        nnet.stop();
        nnet.getMonitor().Stop(); 
    }
    
    public boolean changeNetwork()
    {
        if( tryCount < MAX_TRIES )
        {
            ++tryCount;
            nnet.randomize(1.0);
            return true;
        }
        else
        {
            tryCount = 0;
            return false;
        }
    }
    
    public boolean changeLearningRate()
    {
        currentLearning += LEARNING_INCREMENT;
        if( currentLearning <= MAX_LEARNING )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public boolean changeStructure()
    {
        int size = hiddenLayers.size();
        if( size >= MAX_LAYERS )
        {
            resetHiddenLayers();
            return false;
        }
        else
        {
            addHiddenLayers();
            logger.info("Number of hidden layers: " +
                hiddenLayers.size());
        }
        return true;
    }
    
    public boolean resetHiddenLayers()
    {
        int i;
        
        // remove all hidden layers
        for(i=0; i < hiddenLayers.size(); ++i )
        {
            nnet.removeLayer(hiddenLayers.elementAt(i));
        }
        hiddenLayers.removeAllElements();
        
        TanhLayer hidden = new TanhLayer();
        hidden.setRows(LAYER_ROWS);
        Layer input = nnet.getInputLayer();
        input.removeAllOutputs();
        FullSynapse synapse_IH = new FullSynapse();
        input.addOutputSynapse(synapse_IH);
        hidden.addInputSynapse(synapse_IH);
        Layer output = nnet.getOutputLayer();
        output.removeAllInputs();
        FullSynapse synapse_HO = new FullSynapse();
        hidden.addOutputSynapse(synapse_HO);
        output.addInputSynapse(synapse_HO);
        
        hiddenLayers.add(hidden);
        nnet.addLayer(hidden,NeuralNet.HIDDEN_LAYER);
        return true;
    }
    
    public void addHiddenLayer()
    {
        Layer lastHidden = hiddenLayers.elementAt(hiddenLayers.size()-1);
        Layer outputLayer = nnet.getOutputLayer();
        outputLayer.removeAllInputs();
        lastHidden.removeAllOutputs();
        TanhLayer hidden = new TanhLayer();
        hidden.setRows(LAYER_ROWS);
        FullSynapse synapse_HH = new FullSynapse();
        lastHidden.addOutputSynapse(synapse_HH);
        hidden.addInputSynapse(synapse_HH);
        FullSynapse synapse_HO = new FullSynapse();
        hidden.addOutputSynapse(synapse_HO);
        outputLayer.addInputSynapse(synapse_HO);
        hiddenLayers.add(hidden);
        nnet.addLayer(hidden,NeuralNet.HIDDEN_LAYER);
    }
    
    public void addHiddenLayers()
    {
        Layer lastHidden = hiddenLayers.elementAt(hiddenLayers.size()-1);
        lastHidden.removeAllOutputs();
        Layer output = nnet.getOutputLayer();

        for( int  i=1; i < MAX_LAYERS; ++i)
        {
            TanhLayer hidden = new TanhLayer();
            hidden.setRows(LAYER_ROWS);
            FullSynapse synapse_HH = new FullSynapse();
            lastHidden.addOutputSynapse(synapse_HH);
            hidden.addInputSynapse(synapse_HH);
            nnet.addLayer(hidden, NeuralNet.HIDDEN_LAYER);
            hiddenLayers.add(hidden);
        }
        
        output.removeAllInputs();
        lastHidden = hiddenLayers.elementAt(hiddenLayers.size()-1);
        FullSynapse synapse_HO = new FullSynapse();
        lastHidden.addOutputSynapse(synapse_HO);
        output.addInputSynapse(synapse_HO);
    }
    
    public void netValidated(NeuralValidationEvent event) 
    { 
        // Shows the RMSE at the end of the cycle 
        //NeuralNet NN = (NeuralNet) event.getSource();
        //System.out.println(" Validation Error: "+ NN.getMonitor().getGlobalError());
    }
    
    public void netStarted(NeuralNetEvent nne)
    {
        logger.info("NET STARTED");
    }
    
    public void setIndependentM(double[][] independentM)
    {
        this.independentM = independentM;
    }
    
    public void setDependentM(double[][] dependentM)
    {
        this.dependentM = dependentM;
    }
    
    
}
