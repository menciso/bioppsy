/*
 * CREATED: Feb 7, 2005
 * FILENAME: TetkoANN.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.regression;

import org.joone.engine.*;
import org.joone.engine.learning.*;
import org.joone.io.*;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 *
 */
public class TetkoANN implements NeuralNetListener
{
    Logger logger = Logger.getLogger(getClass());
    
    LinearLayer input;
    SigmoidLayer hidden;
    SigmoidLayer output;
    Monitor monitor;
    boolean tested = false;
    
    private static final double[][] inputArray =
    {
        {0.0,0.0},        
        {0.0,1.0},
        {1.0,0.0},
        {1.0,1.0}
    };
    
    private static final double[][] outputArray =
    {
        {0.0},
        {1.0},
        {1.0},
        {0.0}
    };
    
    /**
     * 
     */
    public TetkoANN()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public static void main(String[] args)
    {
        TetkoANN ta = new TetkoANN();
        ta.constructANN();
    }
    
    public void constructANN()
    {
        input = new LinearLayer();
        hidden = new SigmoidLayer();
        output = new SigmoidLayer();
        input.setRows(2);
        hidden.setRows(3);
        output.setRows(1);
        FullSynapse synapse_IH = new FullSynapse(); /* Input  -> Hidden conn. */
        FullSynapse synapse_HO = new FullSynapse(); /* Hidden -> Output conn. */
        input.addOutputSynapse(synapse_IH);
        hidden.addInputSynapse(synapse_IH);
        hidden.addOutputSynapse(synapse_HO);
        output.addInputSynapse(synapse_HO);
        monitor = new Monitor();
        monitor.setLearningRate(0.7);
        monitor.setMomentum(0.5);
        input.setMonitor(monitor);
        hidden.setMonitor(monitor);
        output.setMonitor(monitor);
        monitor.addNeuralNetListener(this);
        
        MemoryInputSynapse inputStream = new MemoryInputSynapse();
        inputStream.setInputArray(inputArray);
        inputStream.setAdvancedColumnSelector("1,2");
        input.addInputSynapse(inputStream);
     
        TeachingSynapse trainer = new TeachingSynapse();
        MemoryInputSynapse samples = new MemoryInputSynapse();
        samples.setInputArray(outputArray);
        samples.setAdvancedColumnSelector("1");
        trainer.setDesired(samples);
        trainer.setMonitor(monitor);
        output.addOutputSynapse(trainer);
        
        input.start();
        hidden.start();
        output.start();
        monitor.setTrainingPatterns(4);
        monitor.setTotCicles(2000000);
        monitor.setLearning(true);
        monitor.Go();
    }
    
    public void netStopped(NeuralNetEvent nne)
    {
        logger.info("NET STOPPED");
        testANN();
    }
    
    public void netStoppedError(NeuralNetEvent nne, String error)
    {
        logger.info("NET STOPPED ERROR: " + error);
    }
    
    public void errorChanged(NeuralNetEvent nne)
    {
        //logger.info("ERROR CHANGED");
    }
    
    public void cicleTerminated(NeuralNetEvent nne)
    {
        //logger.info("Cycle Terminated");
    }
    
    public void netStarted(NeuralNetEvent nne)
    {
        logger.info("NET STARTED");
    }
    
    public void testANN()
    {
        if( !tested )
        {
            tested = true;
        }
        
        double[][] inputArray = {{1.0,1.0}};
        
        input.removeAllInputs();
        MemoryInputSynapse inputStream = new MemoryInputSynapse();
        inputStream.setInputArray(inputArray);
        inputStream.setAdvancedColumnSelector("1,2");
        input.addInputSynapse(inputStream);
        
        output.removeAllOutputs();
        MemoryOutputSynapse mos = new MemoryOutputSynapse();
        output.addOutputSynapse(mos);
        
        input.start();
        hidden.start();
        output.start();
        monitor.setTotCicles(1);
        monitor.setTrainingPatterns(1);
        monitor.setLearning(false);
        monitor.Go();
        double[] pattern = mos.getNextPattern();
        System.out.println("RESULT: " + pattern[0]); 
    }
}
