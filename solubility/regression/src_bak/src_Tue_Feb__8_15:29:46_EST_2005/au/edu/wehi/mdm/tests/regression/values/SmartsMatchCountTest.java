/*
 * CREATED: Feb 4, 2005
 * FILENAME: SmartsMatchCountTest.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.tests.regression.values;

import junit.framework.TestCase;
import au.edu.wehi.mdm.regression.values.SmartsMatchCount;
import joelib2.molecule.*;
import joelib2.io.*;

import java.io.*;

/**
 * @author eduthie
 *
 */
public class SmartsMatchCountTest extends TestCase
{
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(SmartsMatchCountTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for SmartsMatchCountTest.
     * @param arg0
     */
    public SmartsMatchCountTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        try
        {            
	        SmartsMatchCount smc = new SmartsMatchCount();
	        String pattern = "[C]";
	        if( !smc.addPattern(pattern) )
	        {
	            fail("Unable to set pattern: " + pattern);
	        }
	        Molecule mol = new BasicConformerMolecule();
            InputStream in = getClass().getResourceAsStream("../test.mol2");
            if( in.available() == 0 )
            {
                fail("Unable to open test.mol2");
            }
	        BasicReader reader = new BasicReader(in,
	                BasicIOTypeHolder.instance().getIOType("MOL2"));
	        if( !reader.readNext(mol) )
	        {
	            fail("Failed to read mol");
	        }
	        double value = smc.value(mol);
	        if( value != 2.0 )
	        {
	            fail("Unexpected number of carbon atoms");
	        }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
        
    }

}
