/*
 * CREATED: Feb 4, 2005
 * FILENAME: MoleculeRowDescTest.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.mdm.tests.regression.rows;

import java.io.*;
import junit.framework.TestCase;
import au.edu.wehi.mdm.regression.rows.MoleculeRowDesc;
import au.edu.wehi.mdm.regression.values.Intercept;
import au.edu.wehi.mdm.regression.values.MoleculeValueDesc;
import joelib2.molecule.Molecule;
import joelib2.molecule.BasicConformerMolecule;
import joelib2.io.*;

/**
 * @author eduthie
 *
 */
public class MoleculeRowDescTest extends TestCase
{   
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(MoleculeRowDescTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for MoleculeRowDescTest.
     * @param arg0
     */
    public MoleculeRowDescTest(String arg0)
    {
        super(arg0);
    }

    public void testGetRow()
    {
        MoleculeRowDesc mr = new MoleculeRowDesc();
        MoleculeValueDesc intercept = new Intercept();
        mr.addFeature(intercept);
        Molecule mol = new BasicConformerMolecule();
        double[] result = mr.getRow(mol);
        assertEquals(result.length,1);
        if( result[0] != 1.0 )
        {
            fail("Incorrect MoleculeRowDesc returned");
        }
    }
    
    public void testAddSmarts()
    {  
        try
        {
            MoleculeRowDesc mr = new MoleculeRowDesc();
            InputStream in = getClass().getResourceAsStream(
                "../test.smarts");
            if( in.available() == 0 )
            {
                fail("Did not read tests.smarts correctly");
            }
            if( !mr.addSmarts(in) )
            {
                fail("Failed to add smarts from file: " + "test.smarts");
            }
            
	        Molecule mol = new BasicConformerMolecule();
	        InputStream molStream  = getClass().getResourceAsStream(
                    "../test.mol2");
	        BasicReader reader = new BasicReader(
	                molStream, BasicIOTypeHolder.instance().getIOType("MOL2"));
	        if( !reader.readNext(mol) )
	        {
	            fail("Failed to read mol");
	        }
            double[] row = mr.getRow(mol);
            assertEquals(5, row.length);
            assertEquals(2, (int) row[0]);
            assertEquals(0, (int) row[1]);
            assertEquals(0, (int) row[2]);
            assertEquals(0, (int) row[3]);
            assertEquals(2, (int) row[4]);
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

}
