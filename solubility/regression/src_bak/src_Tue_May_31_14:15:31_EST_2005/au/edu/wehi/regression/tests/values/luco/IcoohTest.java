/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values.luco;

import java.io.InputStream;

import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.luco.Icooh;

/**
 * @author eduthie
 */
public class IcoohTest extends TestCase
{
    private Molecule butanoicacid;
    private Molecule benzene;

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(IcoohTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        
        String name = "butanoicacid.mol2";
        InputStream in = getClass().getResourceAsStream(name);
        butanoicacid = new BasicConformerMolecule();
        BasicReader reader = new BasicReader(
                in, BasicIOTypeHolder.instance().getIOType("MOL2"));
        if( !reader.readNext(butanoicacid) )
        {
            fail("Failed to read: " + name);
        }
        
        name = "benzene.mol2";
        in = getClass().getResourceAsStream(name);
        benzene = new BasicConformerMolecule();
        reader = new BasicReader(
                in, BasicIOTypeHolder.instance().getIOType("MOL2"));
        if( !reader.readNext(benzene) )
        {
            fail("Failed to read: " + name);
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for IcoohTest.
     * @param arg0
     */
    public IcoohTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        Icooh icooh = new Icooh();
        double value = icooh.value(butanoicacid);
        assertEquals(-1.0,value);
        value = icooh.value(benzene);
        assertEquals(0.0,value);
    }

}
