/*
 * Created on: May 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import org.apache.log4j.Logger;

/**
 * Application-wide constant values
 * 
 * @author eduthie
 */
public class Constants
{
    public static final String PROPERTIES = "/au/edu/wehi/regression/regression.properties";
    public static final String DATABASE_FILE_KEY = "DatabaseFilename";
    
    public static final String HOU_MLR = "HOU_MLR"; 
    public static final String HOU_PLS = "HOU_PLS";
    public static final String LUCO_PLS = "LUCO_PLS";
}
