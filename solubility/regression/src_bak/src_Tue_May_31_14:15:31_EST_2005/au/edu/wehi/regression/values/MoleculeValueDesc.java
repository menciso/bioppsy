/*
 * CREATED: Feb 4, 2005
 * FILENAME: MoleculeValueDesc.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;

/**
 * @author eduthie
 *
 * A MoleculeValueDesc object is an abstract class which returns
 * a double value when given a Molecule. This object is used
 * to calculate descriptor values used in regression models.
 */
public interface MoleculeValueDesc
{
    /**
     * Returns the calculated feature of the given molecule
     */
    public double value(Molecule mol);
}
