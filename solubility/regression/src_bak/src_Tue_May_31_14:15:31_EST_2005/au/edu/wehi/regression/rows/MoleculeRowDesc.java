/*
 * CREATED: Feb 3, 2005
 * FILENAME: MoleculeRowDesc.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.rows;

import joelib2.molecule.Molecule;
import java.util.*;
import au.edu.wehi.regression.values.*;
import java.io.*;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 *
 * A MolecularRowDesc object produces an array of double values
 * that describe a given molecule. These values are characteristics
 * of a molecule that can either be used as dependent or independent
 * variables in a regression model.
 */
public class MoleculeRowDesc
{   
    private Vector<MoleculeValueDesc> features;
    private Vector<SmartsMatchCount> singleMatchSmarts;
    
    public MoleculeRowDesc()
    {
        features = new Vector<MoleculeValueDesc>();
        singleMatchSmarts = null;
    }
    
    /**
     * Returns an array of double values calculated from the
     * given molecule. If there are no features in this
     * MoleculeRowDesc an empty array will be returned.
     */
    public double[] getRow(Molecule mol)
    {
        int size = features.size();
        if( singleMatchSmarts != null )
        {
            size += singleMatchSmarts.size();
        }
        double[] results = new double[size];
        
        for( int i=0; i < features.size(); ++i)
        {
            MoleculeValueDesc mv = features.elementAt(i);
            results[i] = mv.value(mol);
        }
        
        if( singleMatchSmarts != null )
        {
            Vector<int[]> alreadyMatched = new Vector<int[]>();
            for( int i = (singleMatchSmarts.size()-1); i >= 0; --i )
            {
                SmartsMatchCount smc = singleMatchSmarts.elementAt(i);
                Vector<int[]> matches = smc.atoms(mol);
                int matchCount = 0;
                for( int j=0; j < matches.size(); ++j )
                {
                    int[] match = matches.elementAt(j);
                    boolean foundAlready = false;
                    for( int k=0; k < alreadyMatched.size(); ++k )
                    {
                        int[] alreadyMatch = alreadyMatched.elementAt(k);
                        if( alreadyMatch[0] == match[0] )
                        {
                            foundAlready = true;
                            break;
                        }
                    }
                    if( !foundAlready )
                    {
                        ++matchCount;
                        alreadyMatched.add(match);
                    }
                }
                results[i+features.size()] = (double) matchCount;
            }
        }
        
        return results;
    }
    
    /**
     * Adds a new feature/descriptor to this MoleculeRowDesc. The
     * row that is returned will have an extra value on the
     * end corresponding to the value() result of this added
     * feature.
     */
    public void addFeature(MoleculeValueDesc feature)
    {
        features.add(feature);
    }
    
    /**
     * Adds the SMARTS patterns listed in the given stream,
     * returns true on success and false on failure.
     * Each SMARTS pattern in the file is on a single line.
     * To allow several SMARTS to match for the same variable,
     * list them on a single line separated by spaces.
     * The SMARTS patterns cannot contain spaces.
     */
    public boolean addSmarts(InputStream in)
    {
        return addSmartsToVector(in,features);
    }
    
    /**
     * Adds a list of SingleMatchSmarts patterns to the row of descriptors.
     * An atom in a molecule can match at most one of
     * these patterns. If an atoms helper more than one
     * pattern, the last matching pattern in the list
     * is the matching pattern.
     * <br><br>
     * Each pattern in the list can consist of one or more
     * SMARTS patterns. The matching atoms in a given molecule
     * to these patterns are obtained, without duplicates. If 
     * any of these atoms match a pattern further on in the
     * list, they are removed. The number of matching atoms
     * are returned as a value in the row of descriptors.
     * <br><br>
     * @param in - InputStream containing SMARTS patterns.
     * Each line in the InputStream is taken as a single
     * matching pattern. Each matching pattern contain one
     * or more SMARTS patterns separated by a space. 
     * @return true if successfull, false otherwise
     */
    public boolean addSingleMatchSmarts(InputStream in)
    {
        singleMatchSmarts = new Vector<SmartsMatchCount>();
        return addSmartsToVector(in,singleMatchSmarts);
    }
    
    /**
     * Reads the SMARTS patterns from the given input stream and adds them
     * to the given Vector.
     * 
     * @param in - InputStream containing SMARTS patterns, one pattern per
     * line. A SMARTS pattern can consist of multiple expressions separated
     * by spaces, a match occurs if any of the expressions match.
     * @param vector - Vector of MoleculeValueDesc to add SMARTS features to.
     * @return true on success, false on failure.
     */
    public boolean addSmartsToVector(InputStream in, Vector vector)
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while( (line = br.readLine()) != null )
            {
                SmartsMatchCount smc = new SmartsMatchCount();
                String[] splitLine = line.split("[ ]+");
                for( int i=0; i < splitLine.length; ++i )
                {
                    if( !smc.addPattern(splitLine[i]) )
                    {
                        Logger.getLogger(MoleculeRowDesc.class).error("Invalid SMARTS pattern: " + line);
                        return false;
                    }
                }
                vector.add(smc);
            }
        }
        catch(FileNotFoundException fnfe)
        {
            Logger.getLogger(MoleculeRowDesc.class).error(fnfe.getMessage());
            return false;
        }
        catch( IOException ioe )
        {
            Logger.getLogger(MoleculeRowDesc.class).error(ioe.getMessage());
            return false;
        }
        finally
        {
            try
            {
                in.close();
            }
            catch( IOException ioe )
            {
            }
        }
        
        return true;
    }
}
