/*
 * Created on: May 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.luco;

import java.io.InputStream;

import joelib2.io.BasicIOTypeHolder;
import joelib2.io.IOType;
import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.luco.LucoPLS;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.*;
import au.edu.wehi.regression.util.Stats;
import java.io.*;

/**
 * @author eduthie
 */
public class LucoPLSTest extends TestCase
{
    public static Logger logger = Logger.getLogger(LucoPLSTest.class);
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(LucoPLSTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for LucoPLSTest.
     * @param arg0
     */
    public LucoPLSTest(String arg0)
    {
        super(arg0);
    }

    /*
     * Class under test for boolean train()
     */
    public void testTrain()
    {
        LucoPLS pls = new LucoPLS("LUCO_PLS");
        if( !pls.train() )
        {
            fail("FAILED TO TRAIN PLS");
        }
        double rsq = pls.rSquared();
        logger.info("LUCO PLS RSQ: " + rsq);
        double minRsq = 0.8;
        if( rsq < minRsq )
        {
            fail("LUCO PLS RSQ is less than " + minRsq + " : " + rsq);
        }
        double sError = pls.standardError();
        logger.info("LUCO PLS STANDARD ERROR: " + sError);
        double maxError = 0.35;
        if( sError > 0.35 )
        {
            fail("LUCO PLS standard error is greater than " + maxError + " : " + sError);
        }
        
        try
        {
            String name = "/au/edu/wehi/regression/luco/bbTest62-75.mol2";
            InputStream in = getClass().getResourceAsStream(name);
            IOType ioType = BasicIOTypeHolder.instance().getIOType("mol2");
            RegressionMoleculeVector molecules = new RegressionMoleculeVector(in,ioType,ioType);
            
            double[] results = new double[molecules.getSize()];
            for( int i=0; i < results.length; ++i )
            {
                Molecule mol = molecules.getMol(i);
                results[i] = pls.value(mol);
                results[i] = Math.random()*results[i];
            }
            //results[0] = -0.036;
            //results[1] = -0.292;
            //results[2] = -0.263;
            //results[3] = 0.170;
            //results[4] = -0.139;
            //results[5] = 0.016;
            //results[6] = -0.510;
            //results[7] = -0.285;
            //results[8] = -0.005;
            //results[9] = 0.005;
            //results[10] = -0.447;
            //results[11] = -0.931;
            //results[12] = -1.308;
            //results[13] = 0.966;        
            
            in = getClass().getResourceAsStream(
                    "/au/edu/wehi/regression/luco/bbTest62-75.logBB");
            double[] expected = new double[molecules.getSize()];
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            int i=0;
            while( (line = br.readLine()) != null )
            {
                line = line.trim();
                if( (line != null) && (line.length() > 0) )
                {
                    expected[i] = new Double(line).doubleValue();
                    ++i;
                }
            }
            
            rsq = Stats.rSquared(expected,results);
            logger.info("LUCO RSQ FOR TEST SET 1: " + rsq);
            double err = Stats.standardError(expected,results);
            logger.info("LUCO STANDARD ERROR FOR TEST SET 1: " + err);
            
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }
    

}
