/*
 * Created on: May 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.util;

import org.apache.log4j.Logger;
import java.util.*;
import joelib2.molecule.*;
import joelib2.util.iterator.*;

/**
 * The GraphHelper class contains various static algorithms that
 * provide operations on the molecule graph.
 * 
 * @author eduthie
 */
public class GraphHelper
{
    private static Logger logger = Logger.getLogger(GraphHelper.class);

    /**
     * Calculates all the possible subgraphs with m edges in the given
     * molecule.
     * 
     * @param molecule - Molecule graph to process
     * @param m - number of edges of calculated subgraphs
     * @return - A Set of Subgraph which are TreeSets of Integer, the indicies
     * of the atoms in the subgraph.
     */
    public static Set<Subgraph> subgraphs(Molecule molecule, int m)
    {
        Molecule mol = (Molecule) molecule.clone();
        mol.deleteHydrogens();
        
        HashSet<Subgraph> lastSubgraphs = new HashSet<Subgraph>();
        HashSet<Subgraph> currentSubgraphs = null;
        
        if( m < 0 ) // done
        {
            return lastSubgraphs;
        }
        
        // we need to populate the lastSubgraphs List with subgraphs containing
        // all the single nodes in the molecule
        for( int i=1; i <= mol.getAtomsSize(); ++i )
        {
            Subgraph subgraph = new Subgraph();
            subgraph.addNode(new Integer(i));
            lastSubgraphs.add(subgraph);
        }
        
        if( m == 0 ) // done
        {
            return lastSubgraphs;
        }
        
        for( int k=1; k <= m; ++k )
        {
            currentSubgraphs = new HashSet<Subgraph>();
                
            // Here we start with a baseSubgraph that is one of the subgraphs from
            // the last iteration. Then we visit all the nodes of the base. For
            // each of these nodes we visit all adjacent nodes. If these adjacent 
            // nodes are not in the baseSubgraph, we add them to the baseSubgraph
            // to create a newSubgraph. If we don't already have this subgraph,
            // we add it.
            Iterator<Subgraph> lastSubgraphsI = lastSubgraphs.iterator();
            while( lastSubgraphsI.hasNext() )
            {
                Subgraph baseSubgraph = lastSubgraphsI.next();
                Iterator<Integer> atomIterator = baseSubgraph.nodeIterator();
                while( atomIterator.hasNext() ) // visit all atoms in baseSubgraph
                {
                    Integer atomIndex = atomIterator.next();
                    Atom atom = mol.getAtom(atomIndex.intValue());
                    if( atom == null )
                    {
                        return null;
                    }
                    else
                    {
                        BondIterator bi = atom.bondIterator();
                        while( bi.hasNext() ) // visit all adjacent atoms
                        {
                            Bond bond = bi.nextBond();
                            Atom adjacent = bond.getEnd();
                            if( adjacent.getIndex() == atom.getIndex() )
                            {
                                adjacent = bond.getBegin();
                            }
                            Integer adjacentIndex = new Integer(adjacent.getIndex());
                            // if the adjacent node is not currently in the base subgraph, we
                            // create a newSubgraph and add it ht the currentSubgraphs
                            Subgraph newSubgraph = (Subgraph) baseSubgraph.clone();
                            if( newSubgraph.addNodeEdge(adjacentIndex, new XEdge(bond)) )
                            {
                                currentSubgraphs.add(newSubgraph);
                            }
                        }
                    }
                }
            }
            
            lastSubgraphs = currentSubgraphs;
        }
        
        return currentSubgraphs;
    }
    
    /**
     * Calculates all the possible subgraphs with up to and including
     * m edges in the given molecule.
     * 
     * @param molecule - Molecule graph to process
     * @param m - number of edges of calculated subgraphs
     * @return - A Vector of HashSets of Subgraph. The HashSet at index i in
     * the Vector is a HashSet of all possible Subgraphs in the molecule 
     * with i edges.
     */
    public static Vector<HashSet<Subgraph>> subgraphsAll(Molecule molecule, int m)
    {
        Molecule mol = (Molecule) molecule.clone();
        mol.deleteHydrogens();
        
        Vector<HashSet<Subgraph>> subgraphs = new Vector<HashSet<Subgraph>>();
        
        if( m < 0 ) // done
        {
            return subgraphs;
        }
        
        HashSet<Subgraph> singleNodes = new HashSet<Subgraph>();
        // populate the first subgraphs Set with subgraphs containing
        // all the single nodes in the molecule
        for( int i=1; i <= mol.getAtomsSize(); ++i )
        {
            Subgraph subgraph = new Subgraph();
            subgraph.addNode(new Integer(i));
            singleNodes.add(subgraph);
        }
        subgraphs.add(singleNodes);
        
        if( m == 0 ) // done
        {
            return subgraphs;
        }
        
        for( int k=1; k <= m; ++k )
        {
            HashSet<Subgraph> currentSubgraphs = new HashSet<Subgraph>();
            HashSet<Subgraph> lastSubgraphs = subgraphs.elementAt(k-1);
                
            // Here we start with a baseSubgraph that is one of the subgraphs from
            // the last iteration. Then we visit all the nodes of the base. For
            // each of these nodes we visit all adjacent nodes. If these adjacent 
            // nodes are not in the baseSubgraph, we add them to the baseSubgraph
            // to create a newSubgraph. If we don't already have this subgraph,
            // we add it.
            Iterator<Subgraph> lastSubgraphsI = lastSubgraphs.iterator();
            while( lastSubgraphsI.hasNext() )
            {
                Subgraph baseSubgraph = lastSubgraphsI.next();
                Iterator<Integer> atomIterator = baseSubgraph.nodeIterator();
                while( atomIterator.hasNext() ) // visit all atoms in baseSubgraph
                {
                    Integer atomIndex = atomIterator.next();
                    Atom atom = mol.getAtom(atomIndex.intValue());
                    if( atom == null )
                    {
                        return null;
                    }
                    else
                    {
                        BondIterator bi = atom.bondIterator();
                        while( bi.hasNext() ) // visit all adjacent atoms
                        {
                            Bond bond = bi.nextBond();
                            Atom adjacent = bond.getEnd();
                            if( adjacent.getIndex() == atom.getIndex() )
                            {
                                adjacent = bond.getBegin();
                            }
                            Integer adjacentIndex = new Integer(adjacent.getIndex());
                            // if the adjacent node is not currently in the base subgraph, we
                            // create a newSubgraph and add it ht the currentSubgraphs
                            Subgraph newSubgraph = (Subgraph) baseSubgraph.clone();
                            if( newSubgraph.addNodeEdge(adjacentIndex, new XEdge(bond)) )
                            {
                                currentSubgraphs.add(newSubgraph);
                            }
                        }
                    }
                }
            }
            
            subgraphs.add(currentSubgraphs);
        }
        
        return subgraphs;
    }
}
