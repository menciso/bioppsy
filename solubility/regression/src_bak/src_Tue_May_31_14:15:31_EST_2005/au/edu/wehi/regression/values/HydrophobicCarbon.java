/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import java.util.Vector;

import joelib2.molecule.*;
import joelib2.util.iterator.BasicAtomIterator;
import org.apache.log4j.Logger;
import joelib2.feature.types.atomlabel.AtomInRing;

/**
 * Calculates the number of hydrophobic carbons in the molecule.
 * A hydrophobic carbon has been chosen as being a sp3 or sp2 hybridised
 * carbon atom that does not have a heteroatom (any atom other
 * than carbon or hydrogen) attached to itself, attached to a neighbouring
 * carbon atom, or attached to a neighbouring carbon's neighbour.
 * sp3 carbons that are aromatic or in a ring are excluded.
 * 
 * @author eduthie
 */
public class HydrophobicCarbon implements MoleculeValueDesc
{
    /* (non-Javadoc)
     * @see au.edu.wehi.regression.values.MoleculeValueDesc#value(joelib2.molecule.Molecule)
     */
    public double value(Molecule mol)
    {
        // TODO Auto-generated method stub
        return numHydrophobic(mol);
    }
    
    /**************************************************************************
     * Returns the number of hydrophobic carbon atoms present in this molecule.
     * A hydrophobic carbon has been chosen as being a sp3 or sp2 hybridised
     * carbon atom that does not have a heteroatom (any atom other
     * than carbon or hydrogen) attached to itself, attached to a neighbouring
     * carbon atom, or attached to a neighbouring carbon's neighbour.
     * sp3 carbons that are aromatic or in a ring are excluded.
     *************************************************************************/
    public int numHydrophobic(Molecule mol)
    {
        // atoms that could be hydrophobic if they
        // are not connected 1-2,1-3,1-4 to a
        // heteroatom
        Vector<BasicConformerAtom> targets = getCarbons(mol);

        Vector<BasicConformerAtom> heteros = getHeteroatoms(mol);

        return getTargetsNotAttached14(targets,heteros);
    }

    /********************************************************
     * Returns a Vector of all heteroatoms in this Mol, a Vector
     * of JOEAtom objects.
     ********************************************************/
    public Vector<BasicConformerAtom> getHeteroatoms(Molecule mol)
    {
        BasicConformerMolecule molecule = 
            (BasicConformerMolecule) mol;
        int atomicNum;
        BasicAtomIterator ait;
        BasicConformerAtom atom;
        // all heteroatoms
        Vector<BasicConformerAtom> heteros = new Vector<BasicConformerAtom>();
 
        // Search for heteroatoms
        ait = molecule.atomIterator();
        while (ait.hasNext())
        {
            atom = (BasicConformerAtom) ait.nextAtom();
            atomicNum = atom.getAtomicNumber();
            if( (atomicNum != 6) && (atomicNum != 1) )
            {
                // add to list of heteroatoms
                heteros.add(atom);
            }
        }

        return heteros;
    }

    /***********************************************************
     * Returns a list of all sp3 and sp2 Carbons in this
     * Mol. Sp2 carbons that are in a ring or are aromatic are
     * not returned. A Vector of JOEAtom objects is returned.
     ***********************************************************/
    public Vector<BasicConformerAtom> getCarbons(Molecule mol)
    {
        BasicConformerMolecule molecule = (BasicConformerMolecule) mol;
        BasicAtomIterator ait;
        BasicConformerAtom atom;
        int hyb;
        int atomicNum;
        // atoms that could be hydrophobic if they
        // are not connected 1-2,1-3,1-4 to a
        // heteroatom
        Vector<BasicConformerAtom> targets = new Vector<BasicConformerAtom>();

        // Search for targets
        ait = molecule.atomIterator();
        while (ait.hasNext())
        {
            atom = (BasicConformerAtom) ait.nextAtom();
            
            atomicNum = atom.getAtomicNumber();
            if( atomicNum == 6 )
            {
                // Carbon
                hyb = atom.getValence();
                if( hyb == 4 )
                {
                    // sp3 Carbon
                    targets.add(atom);
                }
                else if( hyb == 3 )
                {
                    // sp2 Carbon
                    if( (!atom.hasAromaticBondOrder()) &&
                        !AtomInRing.isInRing(atom)  )
                    {
                        // not Aromatic or in Ring
                        targets.add(atom);
                    }
                }
            }
        }

        return targets;
    }

    /******************************************************
     * Returns the number of atoms in the given targets
     * Vector that are not connected in the 1-2, 1-4, or 1-3 
     * relationship to any of the atoms in the given hetero 
     * list.
     ******************************************************/
    public int getTargetsNotAttached14(Vector<BasicConformerAtom> targets,
            Vector<BasicConformerAtom> heteros)
    {
        int i, j;
        BasicConformerAtom target, hetero;
        int count = 0;
        boolean good;

        for( i=0; i < targets.size(); ++i )
        {
            target = targets.elementAt(i);
            good = true;
            for( j=0; j < heteros.size(); ++j)
            {
                hetero = heteros.elementAt(j);
                if( target.isConnected(hetero) ||
                    AtomHelper.isOneThree(target,hetero) ||
                    AtomHelper.isOneFour(target,hetero) )
                {
                    good = false;
                    break;
                }
            }
            if( good )
            {
                ++count;
            }
        }

        return count;
    }

}
