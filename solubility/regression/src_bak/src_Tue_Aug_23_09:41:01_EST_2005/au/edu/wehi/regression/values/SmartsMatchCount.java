/*
 * CREATED: Feb 4, 2005
 * FILENAME: SmartsMatchCount.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import joelib2.smarts.BasicSMARTSPatternMatcher;
import org.apache.log4j.Logger;
import java.util.*;
import au.edu.wehi.regression.util.XArrays;

/**
 * @author eduthie
 *
 * A SmartsMatchCount contains zero or many SMARTS patterns
 * which can be added with the addPattern(String) method.
 * When the value(Molecule) method is called, each SMARTS
 * pattern is run against the given molecule and the number
 * of total helper are returned.
 */
public class SmartsMatchCount extends MoleculeValueDesc
{
    Vector<String> smarts;
    
    public SmartsMatchCount()
    {
        smarts = new Vector<String>();
    }
    
    /*
     * Add a new SMARTS pattern, returns false if it is an
     * invalid pattern.
     */
    public boolean addPattern(String pattern)
    {
        BasicSMARTSPatternMatcher pm = new BasicSMARTSPatternMatcher();
        if( !pm.init(pattern) )
        {
            String message = "Invalid SMARTS pattern: " + pattern;
            Logger.getLogger(SmartsMatchCount.class).error(message);
            return false;
        }
        else
        {
            smarts.add(pattern);
            return true;
        }
    }
    
    /* 
     * Returns the total number of helper all the internal SMARTS
     * patterns have with the given Molecule.
     */
    public double value(Molecule mol)
    {
        int count = 0;
        BasicSMARTSPatternMatcher pm;
        int i;
        
        for( i=0; i < smarts.size(); ++i )
        {
            String pattern = smarts.elementAt(i);
            pm = new BasicSMARTSPatternMatcher();
            if( !pm.init(pattern) )
            {
                String message = "Invalid SMARTS pattern: " + pattern;
                Logger.getLogger(SmartsMatchCount.class).error(message);
                return Double.NaN;
            }
            pm.match(mol);
            count += pm.getMatchesSize();
        }
        
        return (double) count;
    }
    
    /* 
     * Returns a list of atom indicies that match one
     * of the SMARTS patterns for the given Molecule.
     */
    public Vector<int[]> atoms(Molecule mol)
    {
        Vector<int[]> matches = new Vector<int[]>();
        List<int[]> tempMatches;
        BasicSMARTSPatternMatcher pm;
        int i;
        
        for( i=0; i < smarts.size(); ++i )
        {
            String pattern = smarts.elementAt(i);
            pm = new BasicSMARTSPatternMatcher();
            if( !pm.init(pattern) )
            {
                String message = "Invalid SMARTS pattern: " + pattern;
                Logger.getLogger(SmartsMatchCount.class).error(message);
                continue;
            }
            pm.match(mol);
            tempMatches = pm.getMatchesUnique();
            XArrays.addNoDuplicates(tempMatches,matches);
        }
        
        return matches;
    }
}
