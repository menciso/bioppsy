/*
 * Created on: Jun 10, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import java.io.IOException;

/**
 * @author eduthie
 */
public class LA extends AtomicSmarts
{
    public LA() throws IOException
    {
        super("la.smarts","la.coeff");
    }

}
