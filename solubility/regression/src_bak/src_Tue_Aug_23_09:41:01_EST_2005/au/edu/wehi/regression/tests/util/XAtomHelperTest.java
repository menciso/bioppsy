/*
 * Created on: May 25, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.util;

import java.io.IOException;
import java.io.InputStream;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.*;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.util.XAtomHelper;

/**
 * @author eduthie
 */
public class XAtomHelperTest extends TestCase
{
    public static Logger logger = Logger.getLogger(XAtomHelperTest.class);
    
    private Molecule tetramethylbutane;
    private Molecule ethanol;
    private Molecule ethene;
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(XAtomHelperTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            InputStream in = getClass().getResourceAsStream("tetramethylbutane.mol2");
            tetramethylbutane = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(tetramethylbutane) )
            {
                fail("Failed to read tetramethylbutane");
            }
            String string = "ethanol.mol2";
            in = getClass().getResourceAsStream(string);
            ethanol = new BasicConformerMolecule();
            reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(ethanol) )
            {
                fail("Failed to read " + string);
            }
            string = "ethene.mol2";
            in = getClass().getResourceAsStream(string);
            ethene = new BasicConformerMolecule();
            reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(ethene) )
            {
                fail("Failed to read " + string);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for XAtomHelperTest.
     * @param arg0
     */
    public XAtomHelperTest(String arg0)
    {
        super(arg0);
    }
    
    public void testGetValenceElectrons()
    {
        Atom atom = new TestAtom();
        atom.setAtomicNumber(1);
        int electrons = XAtomHelper.getValenceElectrons(atom);
        assertEquals(1,electrons);
        atom.setAtomicNumber(2);
        electrons = XAtomHelper.getValenceElectrons(atom);
        assertEquals(2,electrons);
        atom.setAtomicNumber(6);
        electrons = XAtomHelper.getValenceElectrons(atom);
        assertEquals(4,electrons);
        atom.setAtomicNumber(27);
        electrons = XAtomHelper.getValenceElectrons(atom);
        assertEquals(9,electrons);
        atom.setAtomicNumber(77);
        electrons = XAtomHelper.getValenceElectrons(atom);
        assertEquals(9,electrons);
        
        // O +4
        atom.setAtomicNumber(8);
        atom.setFormalCharge(4);
        electrons = XAtomHelper.getValenceElectrons(atom);
        assertEquals(2,electrons);
        
        // P -2
        atom.setAtomicNumber(15);
        atom.setFormalCharge(-2);
        electrons = XAtomHelper.getValenceElectrons(atom);
        assertEquals(7,electrons);
        
    }
    
    public class TestAtom extends BasicConformerAtom
    {
        public TestAtom()
        {
            super();
        }
    }
    
    public void testGetValenceZmH()
    {
        tetramethylbutane.deleteHydrogens();
        for( int i=1; i <= tetramethylbutane.getAtomsSize(); ++i )
        {
            Atom atom = tetramethylbutane.getAtom(i);
            int valence = XAtomHelper.getValenceZmH(atom);
            if( (i == 2) || (i == 5) )
            {
                assertEquals(4,valence);
            }
            else
            {
                assertEquals(1,valence);
            }
        }
        
        ethanol.deleteHydrogens();
        for( int i=1; i <= ethanol.getAtomsSize(); ++i )
        {
            Atom atom = ethanol.getAtom(i);
            int valence = XAtomHelper.getValenceZmH(atom);
            switch(i)
            {
                case(1) :
                    assertEquals(1,valence);
                    break;
                case(2) :
                    assertEquals(2,valence);
                    break;
                case(3) :
                    assertEquals(5,valence);
                    break;
            }
        }
    }
    
    public void testGetBondValence()
    {
        
        tetramethylbutane.deleteHydrogens();
        for( int i=1; i <= tetramethylbutane.getAtomsSize(); ++i )
        {
            Atom atom = tetramethylbutane.getAtom(i);
            int valence = XAtomHelper.getGraphValence(atom,false);
            if( (i == 2) || (i == 5) )
            {
                assertEquals(4,valence);
            }
            else
            {
                assertEquals(1,valence);
            }
        }
        
        ethene.deleteHydrogens();
        for( int i=1; i <= ethene.getAtomsSize(); ++ i )
        {
            Atom atom = ethene.getAtom(i);
            int valence = XAtomHelper.getGraphValence(atom,false);
            assertEquals(1,valence);
            valence = XAtomHelper.getGraphValence(atom,true);
            assertEquals(2,valence);
        }
    }
    

}
