/*
 * Created on: Jun 10, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.IOType;
import joelib2.molecule.Molecule;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.RegressionMoleculeVector;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.util.Stats;
import au.edu.wehi.regression.values.*;

/**
 * @author eduthie
 */
public class BBLiuTest extends TestCase
{
    private static Logger logger = Logger.getLogger(BBLiuTest.class);

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(BBLiuTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for BBLiuTest.
     * @param arg0
     */
    public BBLiuTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        try
        {
            String name = "/au/edu/wehi/regression/luco/bbTest62-75.mol2";
            InputStream in = getClass().getResourceAsStream(name);
            IOType ioType = BasicIOTypeHolder.instance().getIOType("mol2");
            RegressionMoleculeVector molecules = new RegressionMoleculeVector(in,ioType,ioType);
            
            double[] results2 = new double[molecules.getSize()];
            double[] results3 = new double[molecules.getSize()];
            double[] results4 = new double[molecules.getSize()];
            double[] results6 = new double[molecules.getSize()];
            BBLiu2 liu2 = new BBLiu2();
            BBLiu3 liu3 = new BBLiu3();
            BBLiu4 liu4 = new BBLiu4();
            BBLiu6 liu6 = new BBLiu6();
            
            for( int i=0; i < results2.length; ++i )
            {
                Molecule mol = molecules.getMol(i);
                results2[i] = liu2.value(mol);
                results3[i] = liu3.value(mol);
                results4[i] = liu4.value(mol);
                results6[i] = liu6.value(mol);
            }    
            
            in = getClass().getResourceAsStream(
                    "/au/edu/wehi/regression/luco/bbTest62-75.logBB");
            double[] expected = new double[molecules.getSize()];
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            int i=0;
            while( (line = br.readLine()) != null )
            {
                line = line.trim();
                if( (line != null) && (line.length() > 0) )
                {
                    expected[i] = new Double(line).doubleValue();
                    ++i;
                }
            }
            
            double rsq = Stats.rSquared(expected,results2);
            logger.info("RSQ FOR LIU 2: " + rsq);
            double err = Stats.standardError(expected,results2);
            logger.info("STANDARD ERROR FOR LIU 2: " + err);
            
            rsq = Stats.rSquared(expected,results3);
            logger.info("RSQ FOR LIU 3: " + rsq);
            err = Stats.standardError(expected,results3);
            logger.info("STANDARD ERROR FOR LIU 3: " + err);
            
            rsq = Stats.rSquared(expected,results4);
            logger.info("RSQ FOR LIU 4: " + rsq);
            err = Stats.standardError(expected,results4);
            logger.info("STANDARD ERROR FOR LIU 4: " + err);
            
            rsq = Stats.rSquared(expected,results6);
            logger.info("RSQ FOR LIU 6: " + rsq);
            err = Stats.standardError(expected,results6);
            logger.info("STANDARD ERROR FOR LIU 6: " + err);
            
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
