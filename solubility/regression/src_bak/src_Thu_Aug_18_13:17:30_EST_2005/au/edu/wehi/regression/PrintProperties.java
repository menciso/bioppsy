/*
 * Created on: Apr 28, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import java.util.Vector;
import au.edu.wehi.regression.values.*;
import java.io.*;
import java.util.Properties;
import com.db4o.*;
import joelib2.io.*;
import joelib2.molecule.*;
import au.edu.wehi.regression.luco.LucoPLS;

/**
 * PrintProperties can be run from the command line, passing
 * a inputFilename as a single argument. Properties from the
 * Molecules in the given inputFilename are printed as a table
 * to the file PrintProperties.OUTPUT_FILENAME.
 * 
 * @author eduthie
 */
public class PrintProperties
{
    private static Logger logger = Logger.getLogger(PrintProperties.class);
    private String inputFilename = null;
    private String outputFilename = null;
    private Vector<MoleculeValueDesc> features;
    
    public PrintProperties(String filename)
    {
        this.inputFilename = filename;
    }

    public static void main(String[] args)
    {
        PropertyConfigurator.configure(
                Constants.class.getResource("regression.log4j"));
        
        if( args.length != 1 )
        {
            logger.error("Incorrect number of arguments. Please provide a single " +
                    "argument, the inputFilename of a file containing molecules");
            System.exit(0);
        }
        else
        {
            PrintProperties pp = new PrintProperties(args[0]);
            if( !pp.loadConstants() )
            {
                logger.error("Failed to load constants");
                return;
            }
            if( !pp.loadFeatures() )
            {
                logger.error("Failed to load features");
                return;
            }
            if( !pp.printProperties() )
            {
                logger.error("Failed to print properties");
                return;
            }
        }
    }
    
    private boolean loadConstants()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + Constants.PROPERTIES);
                return false;
            }
            
            // get database inputFilename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            outputFilename = typesProp.getProperty("OutputFilename");
            if( outputFilename == null )
            {
                logger.error("Failed to retrieve OutputFilename");
                return false;
            }
            return true;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
    }
    
    private boolean loadFeatures()
    {           
        features = new Vector<MoleculeValueDesc>();
        
        //features.add(new Solubility());
        
        Database database = Database.instance();
        if( database == null )
        {
            return false;
        }
        
        MLR mlr = new MLR(Constants.HOU_MLR);
        ObjectSet objectSet = database.get(mlr);
        int expectedSize = 1;
        if( !(objectSet.size()==expectedSize) )
        {
            logger.error("Unexpected number of HOU_MLR objects returned");
            logger.error("Recieved: " + objectSet.size() + " Expected: " + expectedSize);
            return false;
        }
        mlr = (MLR) objectSet.next();
        //features.add(mlr);
        
        //features.add(new BB());
        
        LucoPLS luco = new LucoPLS(Constants.LUCO_PLS);
        objectSet = database.get(luco);
        if( !(objectSet.size()==expectedSize) )
        {
            logger.error("Unexpected number of LUCO_PLS objects returned");
            logger.error("Recieved: " + objectSet.size() + " Expected: " + expectedSize);
            return false;
        }
        luco = (LucoPLS) objectSet.next();
        //features.add(luco);
        
        return true;
    }
    
    private boolean printProperties()
    {
        try
        {
            // load output file
            OutputStream outputStream = new FileOutputStream(outputFilename);
            if( outputStream == null )
            {
                logger.error("Unable to write to file: " + outputFilename);
                return false;
            }
            PrintStream printStream = new PrintStream(outputStream);
            
            RegressionMoleculeVector rmv = getRMV(inputFilename);
            if( rmv == null )
            {
                logger.error("Failed to read input file: " + inputFilename);
                return false;
            }
            
            String lineSeparator = System.getProperty("line.separator");
            if( lineSeparator == null )
            {
                lineSeparator = "/n";
            }
            
            // print column headings
            String line = "Molecule Number";
            for( int j=0; j < features.size(); ++j )
            {
                line += ",";
                MoleculeValueDesc feature = features.elementAt(j);
                if( feature instanceof Named)
                {
                    Named named = (Named) feature;
                    line += named.getName();
                }
                else
                {
                    line += "n/a";
                }
            }
            line += lineSeparator;
            printStream.print(line);
            
            for( int i=0; i < rmv.getSize(); ++i )
            {
                Molecule mol = rmv.getMol(i);
                line = "";
                line += i;
                for( int j=0; j < features.size(); ++j )
                {
                    MoleculeValueDesc feature = features.elementAt(j);
                    float value = (float) feature.value(mol);
                    line += "," + value;
                }
                line += lineSeparator;
                printStream.print(line);
            }
            
            logger.info("DATA PRINTED TO: " + outputFilename);
            return true;
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error("File not found exception " + fnfe.getMessage());
            return false;
        }
        catch( IOException ioe)
        {
            logger.error(ioe.getMessage());
            return false;
        }
    }
    
    public RegressionMoleculeVector getRMV(String filename)
        throws IOException
    {
        if( !BasicIOTypeHolder.instance().canReadExtension(filename) )
        {
            logger.error("Invalid file extension: " + filename);
            return null;
        }
        BasicIOType type = BasicReader.checkGetInputType(filename);
        if( type == null )
        {
            logger.error("Invalid file extension: " + filename);
            return null;
        }
        InputStream in = new FileInputStream(filename);
        if( in == null )
        {
            logger.error("Unable to read file: " + filename);
            return null;
        }
        RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
        return rmv;
    }
}
