/*
 * Created on: Jun 6, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import java.io.*;

/**
 * @author eduthie
 */
public class TPSA extends AtomicSmarts
{
    public TPSA()
        throws IOException
    {
        super("tpsa.smarts","tpsa.psa");
    }
}
