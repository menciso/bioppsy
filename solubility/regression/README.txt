1. Installation

The installation package unzips or untars into a directory called regression.

Contents:

a. Source code release:

    src - The source code for the project. We recommend using Eclipse to
          compile any changes since there are no ant build files. Eclipse
          .project and related files are included in the installation
          directory.
          
    JOELib2-alpha-20050303 
        - Modified JOELib2 source package. To compile the source create
          a new project for JOELib2 and include it in the regression project.

    lib - Other jars used.
    
    doc - API and user documentation.

b. Source code and binary release:

    doc - User documentation.

    mainDatabase.db40 - database storing regression parameters for models.

    regression.jar - The complete packaged application. Only this file and 
                     mainDatabase.db40 are required to run the application.
                 
2. Requirements

The Java Runtime Environment 1.5 or later (J2SE 5.0 or later) must be installed.
This is availiable for download from 'http://java.sun.com/'. Please ensure that
the 'java' executable is listed in your PATH environment variable.


There are 2 interfaces to the application. A command-line interface and a Swing GUI.

3. Command-line Interface

To execute the application type the following in the installation directory (the
directory called regression containing regression.jar and mainDatabase.db40):

'java -jar -Xmx80M regression.jar ValuesToDisplay filename'

Where ValuesToDisplay is a comma-separated list of values that you wish to be
calculated and filename is the name of the file which contains the molecules
to be processed. For example try:

'java -jar -Xmx80M regression.jar Solubility,BB test.sdf'

This will print out the experimental logS values and logBB values for all the 
molecules in the test.sdf file. Try this out. The logS values should be displayed
correctly but 'NaN' will be displayed for all the logBB values. This is because no
experimental logBB values are stored in the file test.sdf.

To obtain the availiable ValuesToDisplay run the application with only one
argument, anything will do. For example:

'java -jar -Xmx80M regression.jar singleArgument'

will list all the currently availiable values. Alternatively look in the 
regression/src/au/edu/wehi/regression/regression.properties file. The
key 'ValuesClassNames' lists all the available values.

4. Graphical User Interface

To run the graphical interface to the application (any platform) type the following
in the installation directory:

'java -jar -Xmx80M regression.jar'

Or alternatively on some systems, using the system file explorer,
you can just double click on the 'regression.jar' file in the installation directory.

5. Compiling the source code. 

The source code was compiled using the Eclipse IDE. The software was then distrubuted 
using the FatJar and OneJar pluggins for Eclipse. If you wish to the souce code firstly
download the source distribution. You must have Eclipe installed with support for java
1.5 or above. The Eclipse project files are included with the source distribution. Either
use the project in the regression directory or alternatively create a new project
including the source. Creating a new project will be a little complicated as you need
to also create joone and JOELIB2 projects and include them in the regression project.
If you need any help please contact eduthie@wehi.edu.au
