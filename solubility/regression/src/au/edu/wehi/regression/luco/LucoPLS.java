/*
 * Created on: May 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.luco;


import java.io.*;
import java.util.Properties;

import joelib2.io.BasicIOType;
import joelib2.io.BasicIOTypeHolder;

import org.apache.log4j.Logger;

import Jama.Matrix;
import au.edu.wehi.regression.Constants;
import au.edu.wehi.regression.MoleculePLS;
import au.edu.wehi.regression.RegressionMoleculeVector;
import au.edu.wehi.regression.rows.BBRow;
import au.edu.wehi.regression.rows.LucoRow;
import au.edu.wehi.regression.util.Stats;


/**
 * LucoPLS, PLS model for logBB permiability.
 * <br><br>
 * Reference:<br>
 * Prediction of the Brain-Blood Distribution of a Large Set of Drugs from Structurally
 * Derived Descriptors Using Partial Least-Squares (PLS) Modeling<br>
 * Juan M Luco<br>
 * J.Chem. Inf. Comput. Sci. 1999, 39, 396-404
 * @author eduthie
 */
public class LucoPLS extends MoleculePLS
{
    private String bbTrainFilename;
    private String bbDataFilename;
    
    private double rsq;
    private double error;
    
    public LucoPLS()
    {
        super();
    }

    public LucoPLS(String name)
    {
        super(name);
    }
    
    /**
     * Trains the model from the stored training set. This method must be called before
     * using predict() or the statistical methods.
     */
    public boolean train()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            if( stream == null )
            {
                Logger.getLogger(getClass()).error("Could not find properties: "
                        + Constants.PROPERTIES);
                System.exit(0);
            }
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            
            bbTrainFilename = typesProp.getProperty("BBTrainingFilename");
            bbDataFilename = typesProp.getProperty("BBTrainingData");
            
            InputStream in = getClass().getResourceAsStream(bbTrainFilename);
            if( in == null )
            {
                Logger.getLogger(getClass()).error("Unable to find resource: " + bbTrainFilename);
                return false;
            }
            BasicIOType type = BasicIOTypeHolder.instance().filenameToType(bbTrainFilename);
            if( type == null )
            {
                Logger.getLogger(getClass()).error("Unable to get type for file: " + bbTrainFilename);
                return false;
            }
            RegressionMoleculeVector rmv = new RegressionMoleculeVector(in,type,type);
            LucoRow lucoRow = new LucoRow();
            rmv.setIndependentDescriber(new LucoRow());
            rmv.setDependentDescriber(new BBRow());
            in = getClass().getResourceAsStream(bbDataFilename);
            if( in == null )
            {
                Logger.getLogger(getClass()).error("Unable to find resource: " + bbDataFilename);
                return false;
            }
            if( !rmv.loadBBData(in) )
            {
                Logger.getLogger(getClass()).error("Failed to load training BB data");
                return false;
            }
            return train(rmv);           
        }
        catch( IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
            return false;
        }
    }
}
