/*
 * Created on: Apr 29, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import Jama.Matrix;
import au.edu.wehi.regression.PLS;
import au.edu.wehi.regression.util.XMatrix;

import java.util.Vector;

/**
 * @author eduthie
 */
public class PLSTest extends TestCase
{
    private static Logger logger = Logger.getLogger(PLSTest.class);

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(PLSTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for PLSTest.
     * @param arg0
     */
    public PLSTest(String arg0)
    {
        super(arg0);
    }
    
    public void testHasConverged()
    {
        int rows = 100;
        int cols = 10;
        double value = 5.0;
        PLS pls = new PLS();
        Matrix M = new Matrix(rows,cols,value);
        Matrix B = new Matrix(rows,cols,value);
        if( !pls.hasConverged(M,B,pls.getTolerance()) )
        {
            fail("Should have registered as converged");
        }
        Matrix C = new Matrix(rows,cols,value*2);
        if( pls.hasConverged(M,C,pls.getTolerance()) )
        {
            fail("Should not have registered as converged");
        }
    }
    
    /**
     * Tests the PLS prediction using the relationship:<br>
     * y1 = x1 + ...... + xp<br>
     * y2 = x1 + 2x2 + 3x3 + ..... + pxp<br>
     * where p = 10
     */
    public void testAll()
    {
        int p = 10; // number of parameters
        int q = 2; // number of dependent variables
        int N = 100; // number of samples
        double[][] X = new double[N][p];
        double[][] Y = new double[N][q];
        
        for( int i=0; i < N; ++i )
        {
            for( int j=0; j < p; ++j)
            {
                X[i][j] = Math.random();
            }
            
            double sum1 = 0.0;
            double sum2 = 0.0;
            for( int j=0; j < p; ++j)
            {
                sum1 += X[i][j];
                sum2 += X[i][j]*(j+1);
            }
            Y[i][0] = sum1;
            Y[i][1] = sum2;
        }
        
        PLS pls = new PLS();
        if( !pls.train(XMatrix.copy(X),XMatrix.copy(Y)) )
        {
            fail("Unable to train PLS");
        }
        
        double[][] predicted = pls.predict(XMatrix.copy(X));
        //new Matrix(predicted).minus(new Matrix(Y)).print(3,3);
        
        //plsTestResults(pls);
    }
    
    /**
     * Test using the Linnerud Data
     *
     */
    public void testLinnerud()
    {
        int p = 3;
        int q = 3;
        double tolerance = 1E-13;
        
        double[][] X =
        {
            { 191,36,50 },
            { 189,37,52 },
            { 193,38,58 },
            { 162,35,62 },
            { 189,35,46 },
            { 182,36,56 },
            { 211,38,56 },
            { 167,34,60 },
            { 176,31,74 },
            { 154,33,56 },
            { 169,34,50 },
            { 166,33,52 },
            { 154,34,64 },
            { 247,46,50 },
            { 193,36,46 },
            { 202,37,62 },
            { 176,37,54 },
            { 157,32,52 },
            { 156,33,54 },
            { 138,33,68 } 
        };

        double[][] Y =
        {
            { 5 ,162,60 },
            { 2 ,110,60 },
            { 12,101,101 },
            { 12,105,37 },
            { 13,155,58 },
            { 4 ,101,42 },
            { 8 ,101,38 },
            { 6 ,125,40 },
            { 15,200,40 },
            { 17,251,250 },
            { 17,120,38 },
            { 13,210,115 },
            { 14,215,105 },
            { 1 ,50, 50 },
            { 6 ,70, 31 },
            { 12,210,120 },
            { 4 ,60, 25 },
            { 11,230,80 },
            { 15,225,73 },
            { 2 ,120,43 }
        };
        
        PLS pls = new PLS();
        if( !pls.train(XMatrix.copy(X),XMatrix.copy(Y)) )
        {
            fail("Unable to train PLS");
        }
        
        double[][] predicted = pls.predict(XMatrix.copy(X));
        Matrix Ys = new Matrix(predicted);
        //Ys.print(3,3);
        Matrix F = Ys.minus(new Matrix(Y));
        //F.print(2,2);
        
        plsTestResults(pls);
    }
    
    public void plsTestResults(PLS pls)
    {
        double tolerance = 1E-10;
        Vector<double[]> T = pls.getT();
        
        // all t must be centered around 0
        assertTrue(XMatrix.zeroSum(T,tolerance));
        
        // all t must be orthogonal
        assertTrue(XMatrix.orthogonal(T,tolerance));
        
        // add u must be centered around 0
        assertTrue(XMatrix.zeroSum(pls.getU(),tolerance));
        
        Vector<Matrix> W = pls.getW();
        Vector<double[]> Wv = new Vector<double[]>();
        for( int i=0; i < Wv.size(); ++i )
        {
            Matrix Mw = W.elementAt(i);
            double[] wv = Mw.transpose().getArray()[0];
            Wv.add(wv);
        }
        
        // all w must be orthogonal
        assertTrue(XMatrix.orthogonal(Wv,tolerance));
        
        // P and Q must all be of unit length
        assertTrue(XMatrix.allUnitLength(pls.getP(),tolerance));
        assertTrue(XMatrix.allUnitLength(pls.getQ(),tolerance));
    }
}
