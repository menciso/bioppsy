/*
 * Created on: Sep 13, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.util;

import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.tests.TestUtil;
import au.edu.wehi.regression.util.CDKConverter;
import joelib2.util.iterator.AtomIterator;

/**
 * @author eduthie
 */
public class CDKConverterTest extends TestCase
{
    private Logger logger = Logger.getLogger(getClass());

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(CDKConverterTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for CDKConverterTest.
     * @param arg0
     */
    public CDKConverterTest(String arg0)
    {
        super(arg0);
    }
    
    public void testConvertToCDK()
    {
        Molecule mol = (new TestUtil()).getMolecule();
        if( mol == null )
        {
            fail("Unable to get test molecule in CDKConverterTest");
        }
        Object[] atoms = mol.getAtoms().toArray();
        
        org.openscience.cdk.Molecule cdkMol = CDKConverter.convertToCDK(mol);
        
        org.openscience.cdk.Atom[] cdkAtoms = cdkMol.getAtoms();
        for( int i=0; i < cdkAtoms.length; ++i )
        {
            assertEquals(cdkAtoms[i].getAtomicNumber(),
                    ((Atom)atoms[i]).getAtomicNumber());
        }
        
        Object[] bonds = mol.getBonds().toArray();
        org.openscience.cdk.Bond[] cdkBonds = cdkMol.getBonds();
        for( int i=0; i < bonds.length; ++i )
        {
            assertEquals( new Double(((Bond)bonds[i]).getBondOrder()).doubleValue(),
                    cdkBonds[i].getOrder() );
        }
       
    }

}
