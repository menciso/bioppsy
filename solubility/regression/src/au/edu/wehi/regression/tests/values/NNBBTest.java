/*
 * Created on: Jun 1, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values;

import java.io.*;

import joelib2.io.BasicIOTypeHolder;
import joelib2.io.IOType;
import joelib2.molecule.Molecule;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.RegressionMoleculeVector;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.NNBB;
import au.edu.wehi.regression.util.Stats;

/**
 * @author eduthie
 */
public class NNBBTest extends TestCase
{
    private static Logger logger = Logger.getLogger(NNBBTest.class);

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(NNBBTest.class);
    }
    
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    public void testValue()
    {
        NNBB nnbb = NNBB.instance();
        
        try
        {
            String name = "/au/edu/wehi/regression/luco/bbTest62-75.mol2";
            InputStream in = getClass().getResourceAsStream(name);
            IOType ioType = BasicIOTypeHolder.instance().getIOType("mol2");
            RegressionMoleculeVector molecules = new RegressionMoleculeVector(in,ioType,ioType);
            
            double[] results = new double[molecules.getSize()];
            for( int i=0; i < results.length; ++i )
            {
                Molecule mol = molecules.getMol(i);
                results[i] = nnbb.value(mol);
                logger.info("RESULT " + i + " : " + results[i]);
            }
            
            in = getClass().getResourceAsStream(
                "/au/edu/wehi/regression/luco/bbTest62-75.logBB");
            double[] expected = new double[molecules.getSize()];
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            int i=0;
            while( (line = br.readLine()) != null )
            {
                line = line.trim();
                if( (line != null) && (line.length() > 0) )
                {
                    expected[i] = new Double(line).doubleValue();
                    ++i;
                }
            }
            
            double rsq = Stats.rSquared(expected,results);
            logger.info("RSQ: " + rsq);
            double err = Stats.standardError(expected,results);
            logger.info("STANDARD ERROR: " + err);
    
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
