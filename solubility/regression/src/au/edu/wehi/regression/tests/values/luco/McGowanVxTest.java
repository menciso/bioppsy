/*
 * Created on: May 26, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values.luco;

import java.io.IOException;
import java.io.InputStream;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.BasicConformerMolecule;
import joelib2.molecule.Molecule;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.values.luco.McGowanVx;

/**
 * @author eduthie
 */
public class McGowanVxTest extends TestCase
{
    private Molecule butanone;
    private static Logger logger = Logger.getLogger(McGowanVxTest.class);
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(McGowanVxTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            String name = "1-hydroxy-3-amino-2-butanone.mol2";
            InputStream in = getClass().getResourceAsStream(name);
            butanone = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(butanone) )
            {
                fail("Failed to read: " + name);
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for McGowanVxTest.
     * @param arg0
     */
    public McGowanVxTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        McGowanVx Vx = new McGowanVx();
        double value = Vx.value(butanone);
        assertEquals(84.64000000000004,value);
    }

}
