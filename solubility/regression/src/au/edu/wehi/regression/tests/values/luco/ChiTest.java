/*
 * Created on: May 25, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.values.luco;


import java.io.*;
import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import Jama.Matrix;
import au.edu.wehi.regression.tests.AllTests;
import joelib2.io.*;
import au.edu.wehi.regression.values.luco.Chi;
import au.edu.wehi.regression.util.Subgraph;

/**
 * @author eduthie
 */
public class ChiTest extends TestCase
{
    private static Logger logger = Logger.getLogger(ChiTest.class);
    private MoleculeVector molecules;

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(ChiTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        String name = "chitest.mol2";
        InputStream in = getClass().getResourceAsStream(name);
        IOType ioType = BasicIOTypeHolder.instance().getIOType("mol2");
        molecules = new BasicMoleculeVector(in,ioType,ioType);
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for ChiTest.
     * @param arg0
     */
    public ChiTest(String arg0)
    {
        super(arg0);
    }

    public void testValue()
    {
        int maxChi = 5;
        double[] results = new double[maxChi+1];
        
        double[] expectedP =
        {
                5.207106781186548,
                2.560660171779821,
                2.914213562373095,
                1.0606601717798212,
                0.0,
                0.0
        };
        double[] expectedC =
        {
                0.0,
                0.0,
                0.0,
                1.5606601717798212,
                0.35355339059327373,
                0.0
        };
        double[] expectedPC =
        {
                0.0,
                0.0,
                0.0,
                0.0,
                1.0606601717798212,
                0.35355339059327373
        };
        
        Molecule mol = molecules.getMol(10);
        for( int m=0; m <= maxChi; ++m )
        {
            Chi chi = new Chi(m,Subgraph.PATH,true);
            double P = chi.value(mol);
            assertEquals(expectedP[m],P);
            chi = new Chi(m,Subgraph.CLUSTER,true);
            double C = chi.value(mol);
            assertEquals(expectedC[m],C);
            chi = new Chi(m,Subgraph.PATH_CLUSTER,true);
            double PC = chi.value(mol);
            assertEquals(expectedPC[m],PC);
            chi = new Chi(m,Subgraph.CHAIN,true);
            double CH = chi.value(mol);
            assertEquals(0.0,CH);
        }
        
        mol = molecules.getMol(11);
        for( int m=0; m <= 8; ++m )
        {
            Chi chi = new Chi(m,Subgraph.PATH,true);
            double P = chi.value(mol);
            if( m == 6 )
            {
                assertEquals(0.0,P);
            }
        }
        
        mol = molecules.getMol(14);
        Chi chi = new Chi(4,Subgraph.CLUSTER,true);
        double C = chi.value(mol);
        assertEquals(0.25,C);
        
        chi = new Chi(7,Subgraph.PATH_CLUSTER,true);
        double PC = chi.value(mol);
        assertEquals(0.5303300858899106,PC);
    }

}
