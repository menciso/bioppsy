/*
 * Created on: Sep 15, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.util;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.util.XMath;

/**
 * @author eduthie
 */
public class XMathTest extends TestCase
{

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(XMathTest.class);
    }
    
    public void testTest()
    {
        System.out.println(XMath.factorial(3));
    }

}
