/*
 * Created on: May 13, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.tests.util;


import java.io.IOException;
import java.io.InputStream;
import joelib2.io.BasicIOTypeHolder;
import joelib2.io.BasicReader;
import joelib2.io.MoleculeIOException;
import joelib2.molecule.*;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import Jama.Matrix;
import au.edu.wehi.regression.tests.AllTests;
import au.edu.wehi.regression.util.Dijkstra;

/**
 * @author eduthie
 */
public class DijkstraTest extends TestCase
{
    private Logger logger = Logger.getLogger(DijkstraTest.class);
    private Molecule mol;
    // current is 1,4-ethylbutylbenzene, the numbering starts at 1 from the tip
    // of the butyl side-chain, goes towards the benzene ring, around the ring, 
    // going out the ethyl side-chain as it comes to it (numbering the side chain
    // 9 and 10) and finally ending up at 12 on the atom on the ring just before
    // hitting the butyl side chain again.
    private Molecule current;

    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(DijkstraTest.class);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
        try
        {
            InputStream in = getClass().getResourceAsStream("2,4,4-trimethylhexane.mol2");
            mol = new BasicConformerMolecule();
            BasicReader reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(mol) )
            {
                fail("Failed to read mol");
            }
            in = getClass().getResourceAsStream("1,4-ethylbutylbenzene.mol2");
            current = new BasicConformerMolecule();
            reader = new BasicReader(
                    in, BasicIOTypeHolder.instance().getIOType("MOL2"));
            if( !reader.readNext(current) )
            {
                fail("Failed to read 1,4-ethylbutylbenzene");
            }
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        catch( MoleculeIOException mioe )
        {
            fail(mioe.getMessage());
        }
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for DijkstraTest.
     * @param arg0
     */
    public DijkstraTest(String arg0)
    {
        super(arg0);
    }
    
    public void testAll()
    {
        Dijkstra dijkstra = new Dijkstra(mol);
        if( !dijkstra.calculate(1) )
        {
            fail("Unable to calcualte dijkstra");
        }
        double[] distances = dijkstra.getDistances();
        for( int i=0; i < distances.length; ++i )
        {
            int intDist = (int) distances[i];
            switch(i)
            {
                case 0 : assertEquals(0,intDist); break;
                case 1 : assertEquals(1,intDist); break;
                case 2 : assertEquals(2,intDist); break;
                case 3 : assertEquals(2,intDist); break;
                case 4 : assertEquals(3,intDist); break;
                case 5 : assertEquals(2,intDist); break;
                case 6 : assertEquals(3,intDist); break;
                case 7 : assertEquals(4,intDist); break;
                case 8 : assertEquals(4,intDist); break;
            }
        }
    }
    
    public void testEthylButylBenzene()
    {
        Dijkstra dijkstra = new Dijkstra(current);
        if( !dijkstra.calculate(1) )
        {
            fail("Unable to calcualte dijkstra");
        }
        double[] distances = dijkstra.getDistances();
        for( int i=0; i < distances.length; ++i )
        {
            if( i <= 9 )
            {
                if( ((int) distances[i]) != i)
                {
                    fail("Incorrect distance: " + distances[i] + " for atom: " + i);
                }
            }
            if( i == 10 )
            {
                assertEquals(6,(int) distances[i]);
            }
            if( i == 11 )
            {
                assertEquals(5,(int) distances[i]);
            }
        }
    }
    
    public void testPath()
    {
        Dijkstra dijkstra = new Dijkstra(current);
        int root = 1;
        int dest = 12;
        if( !dijkstra.calculate(root) )
        {
            fail("Unable to calcualte dijkstra");
        }
        int[] path = dijkstra.getShortestPath(dest);
        for( int i=root; i <= 5; ++i )
        {
            assertEquals(i,path[i-1]);
        }
        assertEquals(dest,path[path.length-1]);
    }
}
