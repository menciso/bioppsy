/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression;

import org.apache.log4j.Logger;

import Jama.Matrix;
import au.edu.wehi.regression.rows.MoleculeRowDesc;
import joelib2.molecule.*;
import au.edu.wehi.regression.util.XMatrix;

import java.util.Vector;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * A MLR object allows the user to build predictive models using
 * a training set of Molecules and then predict values using
 * this linear model. After constructing an MLR, you must train
 * it on a training set using one of the train() methods. If
 * the train() method returns true then the predict() method
 * can be used to predict the values of the chosen property.
 * 
 * @author eduthie
 */
public class MLR implements MoleculeModel, Named
{
    private Matrix B;
    private MoleculeRowDesc xDesc;
    private double rsq;
    private double sdDev;
    private String name;
    
    public MLR()
    {
        
    }
    
    /**
     * Creates a MLR with the given name to allow later retrieval 
     * from the database.
     * 
     * @param name - name to identify this MLR
     */
    public MLR(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return name;
    }
    
    /**
     * Trains the MLR from the given data. The given RegressionMoleculeVector
     * must have been initialised with the molecules from the training set.
     * Additionally the independent and dependent describers of the 
     * RegressionMoleculeVector must be set to determine what descriptors
     * are calculated from the molecules and what properties are being 
     * predicted. Note that the dependent describer must have only one
     * dependent variable for MLR to work.
     * 
     * @param rmv - training data
     */
    public boolean train(RegressionMoleculeVector rmv)
    {
        Matrix X = new Matrix(rmv.getXMatrix());
        Vector<Integer> allZero =  (new XMatrix(X.getArray())).anyZeroColumns();
        if( allZero.size() > 0)
        {
            Logger.getLogger(MLR.class).error("One or more descriptors are all zero for the entire " +
                    " training set. Please remove them:");
            for( int i=0; i < allZero.size(); ++i)
            {
                Logger.getLogger(MLR.class).error(" descriptor index: " + allZero.elementAt(i));
            }
            return false;
        }
        Matrix Y = new Matrix(rmv.getYMatrix());
        B = calculateB(X,Y);
        rsq = rsq(Y,B,X);
        sdDev = sdDev(Y,B,X);
        xDesc = rmv.getIndependentDescriber();
        return true;
    }
    
    /*****************************************************************
     * Returns a (k+1)x1 Matrix B = (X'X)-1*X'*Y. X must 
     * be a Nx(k+1) Matrix and Y must be a Nx1 Matrix.
     *****************************************************************/
    public static Matrix calculateB(Matrix X, Matrix Y)
    {
        //(new XMatrix(X.getArray())).printMatrix("matrixX.txt");
        Matrix Xt = X.transpose();     // (k+1)xN
        //(new XMatrix(Xt.getArray())).printMatrix("matrixXt.txt");
        Matrix XtX = Xt.times(X);      // (k+1)x(k+1)
        //(new XMatrix(XtX.getArray())).printMatrix("matrix.txt");
        Matrix XtXinv = XtX.inverse();  // (k+1)x(k+1)
        Matrix XtY = Xt.times(Y);      // (k+1)x1
        return XtXinv.times(XtY);      // (k+1)x1
    }

    /**
     * Predicts the logS value of the given Molecule
     * 
     * @param mol - Molecule to predict
     * @return - logS of given Molecule
     */
    public double value(Molecule mol)
    {
        if( xDesc == null )
        {
            Logger.getLogger(MLR.class).error("Null independent describer in MLR");
            return Double.NaN;
        }
        double[] row = xDesc.getRow(mol);
        double[][] rows = {xDesc.getRow(mol)};
        Matrix X = new Matrix(rows);
        return X.times(B).get(0,0);
    }
    
    /*****************************************************************
     * Returns the r squared value of the regression.<br><br>
     *
     * Inputs:<br><br>
     *
     * Matrix Y - Nx1 matrix of experimental property values.<br>
     * Matrix B - (k+1)x1 matrix of calculated regression coefficients<br>
     * Matrix X - Nx(k+1) matrix, each row is the calculated occurences
     * of each molecule in the training set, each column the occurence
     * for a particular parameter.<br>
     * int N - number of molecules in the training set.<br><br>
     *
     * The r squared value is calculated using the following equation:<br><br>
     *
     * rsq = <br>
     * (B'X')Y - (sumY)^2/N
     * ------------------
     * Y'Y - (sumY)^2/N
     * 
     *****************************************************************/
    public double rsq(Matrix Y, Matrix B, Matrix X)
    {
        int N = Y.getRowDimension();
        // k+1 = K
        // Y - Nx1
        // B - Kx1
        // X - NxK

        Matrix Bt = B.transpose(); // 1xK
        Matrix Yt = Y.transpose(); // 1xN
        Matrix Xt = X.transpose(); // KxN

        Matrix BtXtY = Bt.times(Xt).times(Y); // 1x1
        Matrix YtY = Yt.times(Y); // 1x1
    
        double sumY = sum(Y,N);
        double sq = sumY*sumY/N;

        return (BtXtY.get(0,0) - sq) / (YtY.get(0,0) - sq);
    }
    
    /*****************************************************************
     * Returns the standard deviation value of the regression.<br><br>
     *
     * Inputs:<br><br>
     *
     * Matrix Y - Nx1 matrix of experimental property values.<br>
     * Matrix B - (k+1)x1 matrix of calculated regression coefficients<br>
     * Matrix X - Nx(k+1) matrix, each row is the calculated occurences
     * of each molecule in the training set, each column the occurence
     * for a particular parameter.<br>
     * int N - number of molecules in the training set.<br><br>
     *
     * The standard deviation value is calculated using the following
     * equation:<br><br>
     *
     * sdDevSq = sqrt of: <br>
     * Y'Y - B'X'Y<br>
     * -----------<br>
     * N - 2
     * 
     *****************************************************************/
    public double sdDev(Matrix Y, Matrix B, Matrix X)
    {
        int N = X.getRowDimension();
        // k+1 = K
        // Y - Nx1
        // B - Kx1
        // X - NxK

        Matrix Bt = B.transpose(); // 1xK
        Matrix Yt = Y.transpose(); // 1xN
        Matrix Xt = X.transpose(); // KxN

        Matrix BtXtY = Bt.times(Xt).times(Y); // 1x1
        Matrix YtY = Yt.times(Y); // 1x1

        return Math.sqrt((YtY.get(0,0) - BtXtY.get(0,0)) / (N-2));
    }
    
    /*******************************************************************
     * Returns all the values in the given Matrix Y of the given size N
     * summed together. The given Matrix must be a column vector with
     * N rows.
     ******************************************************************/
    public double sum(Matrix Y, int N)
    {
        double sum = 0.0;

        for( int i=0; i < N; ++i )
        {
            sum += Y.get(i,0);
        }

        return sum;
    }
    
    /**
     * @return the R squared value of the regression, returns
     * Double.NaN if this MLR has not been trained.
     */
    public double getRsq()
    {
        return rsq;
    }
    
    /**
     * @return the standard deviation of the regression, returns
     * Double.NaN if this MLR has not been trained.
     */
    public double getSdDev()
    {
        return sdDev;
    }
}
