/*
 * Created on: Aug 22, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.gui;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

import net.infonode.tabbedpanel.titledtab.TitledTab;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 */
public class Chart extends TitledTab
{
    private Logger logger;
    private JPanel content;
    private String[][] data;
    private JLabel closeButton;
    private CalculationView parent;
    private String title;

    public Chart(String name, String[][] data, CalculationView parent)
    {
        super(name, null, new JPanel(),null);
        logger = Logger.getLogger(getClass());
        content = (JPanel) getContentComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        this.data = data;
        this.parent = parent;
        this.title = name;
        closeButton = new JLabel("X");
        closeButton.setForeground(Color.RED);
        closeButton.addMouseListener(new CloseButtonMouseListener());
        setTitleComponent(closeButton);
        
        try
        {
            String[] rows = data[0];
            double[] columns = new double[data.length];
            
            for( int i=1; i < data.length; ++i )
            {
                columns[i] = new Double(data[i][0].trim());
            }
            
            Number[][] chartData = new Number[data[0].length-1][data.length-1];
            for( int i=1; i < data.length; ++i)
            {
                for( int j=1; j < data[i].length; ++j )
                {
                    chartData[j-1][i-1] = new Double(data[i][j]);
                }
            }
            
            /*
            JScrollPane scrollPane = new JScrollPane(chartPanel);
            scrollPane.setHorizontalScrollBarPolicy(
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
            content.add(scrollPane);
            */
            content.validate();
            
        }
        catch( NumberFormatException nfe )
        {
            logger.error(nfe.getMessage());
            JOptionPane.showMessageDialog(content,"Error plotting chart data",
                    "Error",JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        parent.addTab(this);
    }
    
    public class CloseButtonMouseListener extends MouseAdapter
    {
        public void mousePressed(MouseEvent e)
        {
            closeButton.setForeground(Color.BLUE);
        }
        
        public void mouseClicked(MouseEvent e)
        {
            close();
        }
    }
    
    public void close()
    {
        parent.removeTab(this);
    }

}
