/*
 * CREATED: Feb 24, 2005
 * FILENAME: XArrays.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.util;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.*;

/**
 * @author eduthie
 *
 * Array util functions like the java.util.Arrays class
 */
public class XArrays
{
    private static Logger logger = Logger.getLogger(XArrays.class);
    
    /**
     * Adds all the elements from list 'from' to the list 'to'
     * unless the element is already in to. The lists must
     * contain int[] elements.
     */
    public static void addNoDuplicates(List<int[]> from, List<int[]> to)
    {
        Iterator<int[]> i,j;
        int k;
        int[] x,y;
        boolean match;
        
        // iterate through the from array
        i = from.iterator();
        while(i.hasNext())
        {
            x = i.next();
            j = to.iterator();
            match = false;
            // check for a match in the to array
            while( j.hasNext() )
            {
                y = j.next();
                if( Arrays.equals(x,y) )
                {
                    match = true;
                }
            }
            // if there was no match, add the 
            // element to the to array
            if( !match )
            {
                to.add(x);
            }
        }
    }
    
    /**
     * Returns the distance between the two vectors as the Frobeneious norm.
     */
    public static double distance(double[] a, double[] b)
    {
        if( a.length != b.length )
        {
            logger.error("Vectors must be of the same length");
            return Double.NaN;
        }
        
        double distance = 0.0;
        
        for( int i=0; i < a.length; ++i )
        {
            distance += Math.pow((a[i]-b[i]),2.0);
        }
        
        return Math.pow(distance,0.5);
    }
}
