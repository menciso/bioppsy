/*
 * CREATED: Feb 11, 2005
 * FILENAME: XMatrix.java
 * WRITTEN BY: eduthie
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 **/
package au.edu.wehi.regression.util;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import org.apache.log4j.Logger;

import Jama.Matrix;

import java.util.Vector;
import java.util.Arrays;

/**
 * A utility class for matricies. Consists of an internal
 * data array of double[][], each double[] in the array
 * representing a row of the matrix. The internal array
 * is just a reference to the array provided.
 * 
 * @author eduthie
 */
public class XMatrix extends Matrix
{
    static Logger logger = Logger.getLogger(XMatrix.class);
    
    /**
     * Initialises the internal matrix with the given array
     * of rows.
     */
    public XMatrix(double[][] matrix)
    {
        super(matrix);
    }
    
    /*
     * Prints the given double[][] to the given OutputStream, one
     * row per line, each column separated by spaces
     */
    public boolean printMatrix(OutputStream out)
    {
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
        String string;
        double[][] M = getArray();
        
        for( int i=0; i < M.length; ++i )
        {
            string = "";
            
            for( int j=0; j < M[i].length; ++j )
            {
                string += new Float( (float) M[i][j]).toString() + " ";    
            }
            writer.println(string);
        }
        writer.flush();
        
        return true;
    }
    
    /*
     * Prints the given double[][] to the given filename, one
     * row per line, each column separated by spaces
     */
    public boolean printMatrix(String filename)
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(filename);
            if( !printMatrix(fos) )
            {
                return false;
            }
            fos.close();
            return true;
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error("File Not Found: " + filename);
            return false;
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
    }
    
    /**
     * Checks to see if any of the rows are compeletely zero,
     * returns true if at least one is, false otherwise.
     */
    public boolean anyZeroRows()
    {
        double[][] M = getArray();
        
        for( int i=0; i < M.length; ++i )
        {
            double[] row = M[i];
            boolean allZero = true;
            for( int j=0; j < row.length; ++j )
            {
                if( row[j] != 0.0 )
                {
                    allZero = false;
                    break;
                }
            }
            if( allZero )
            {
                logger.info("Row: " + i + " is all zero");
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks to see if any of the columns are compeletely zero,
     * Returns the indicies of the columns that are all zero, 
     * or an empty Vector if none are all zero.
     */
    public Vector<Integer> anyZeroColumns()
    {
        double[][] M = getArray();
        
        Vector<Integer> ret = new Vector<Integer>();
        
        if( M.length <= 0 )
        {
            return ret;
        }
        boolean[] columnsNotZero = new boolean[M[0].length];
        for( int i=0; i< columnsNotZero.length; ++i )
        {
            columnsNotZero[i] = false;
        }
        for( int i=0; i < M.length; ++i)
        {
            double[] row = M[i];
            for( int j=0; j < row.length; ++j )
            {
                if( row[j] != 0.0 )
                {
                    columnsNotZero[j] = true;
                }
            }
        }
        for( int i=0; i < columnsNotZero.length; ++i )
        {
            if( !columnsNotZero[i] )
            {
                ret.add(new Integer(i));
            }
        }
        
        return ret;
    }
    
    /**
     * Normalises the given matrix by subtracting the mean from each column
     * and dividing by its standard deviation. Returns an array  the length
     * of the number of columns in the matrix containing the mean and standard
     * deviation of each column in an array of two doubles. Returns null on
     * error.
     */
    public static double[][] normalise(double[][] matrix)
    {
        if( matrix.length <= 0 )
        {
            return null;
        }
        
        double[][] results = new double[matrix[0].length][];
        
        for( int i=0; i < matrix[0].length; ++i )
        {
            double[] column = new double[matrix.length];
            for( int j=0; j < matrix.length; ++j )
            {
                column[j] = matrix[j][i];
            }
            results[i] = new double[2];
            double mean = Stats.mean(column);
            double sdDev = Stats.standardDeviation(column);
            results[i][0] = mean;
            results[i][1] = sdDev;
            for( int j=0; j < matrix.length; ++j )
            {
                matrix[j][i] = matrix[j][i]-mean;
                if( sdDev != 0.0 )
                {
                    matrix[j][i] = matrix[j][i]/sdDev;
                }
            }
        }
        
        return results;
    }
    
    /**
     * Normalises the given matrix by subtracting the mean from each column
     * and dividing by its standard deviation. The mean and standard deviation
     * for each column is taken from matrixStats. matrixStats consists of an array of
     * double[2]; the first element is the mean of the column, the second is the 
     * standard deviation of the column. Returns matrixStats on success or null
     * on failure. matrix stats must contain the same number of elements as
     * matrix has columns.
     */
    public static double[][] normalise(double[][] matrix, double[][] matrixStats)
    {
        if( matrix.length <= 0 )
        {
            return null;
        }
        
        for( int i=0; i < matrix[0].length; ++i )
        {
            for( int j=0; j < matrix.length; ++j )
            {
                double mean = matrixStats[i][0];
                double sdDev = matrixStats[i][1];
                matrix[j][i] = matrix[j][i]-mean;
                if( sdDev != 0.0 )
                {
                    matrix[j][i] = matrix[j][i]/sdDev;
                }
            }
        }
        
        return matrixStats;
    }
    
    /**
     * Un-Normalises the given matrix by multiplying each column by its standard
     * deviation and then adding the mean. The mean and standard deviation
     * for each column is taken from matrixStats. matrixStats consists of an array of
     * double[2]; the first element is the mean of the column, the second is the 
     * standard deviation of the column. Returns matrixStats on success or null
     * on failure. matrix stats must contain the same number of elements as
     * matrix has columns.
     */
    public static double[][] reverseNormalise(double[][] matrix, double[][] matrixStats)
    {
        if( matrix.length <= 0 )
        {
            return null;
        }
        
        for( int i=0; i < matrix[0].length; ++i )
        {
            for( int j=0; j < matrix.length; ++j )
            {
                double mean = matrixStats[i][0];
                double sdDev = matrixStats[i][1];
                matrix[j][i] = matrix[j][i]*sdDev;
                matrix[j][i] = matrix[j][i]+mean;
            }
        }
        
        return matrixStats;  
    }
    
    /**
     * Returns a deep copy of the given array.
     */
    public static double[][] copy(double[][] array)
    {
        double[][] copy = new double[array.length][];
        for( int i=0; i < array.length; ++i )
        {
            copy[i] = new double[array[i].length];
            for( int j=0; j < array[i].length; ++j)
            {
                copy[i][j] = array[i][j];
            }
        }
        return copy;
    }
    
    /**
     * Returns the sume of all the elemetns in the given array
     */
    public static double sum(double[] array)
    {
        double sum = 0.0;
        for( int i=0; i < array.length; ++i)
        {
            sum += array[i];
        }
        return sum;
    }
    
    /**
     * Returns true if the given vectors are all orthogonal inside the
     * given rounding error, false otherwise.
     */
    public static boolean orthogonal(Vector<double[]> vectors, double tolerance)
    {
        for( int i=0; i < vectors.size(); ++i )
        {
            for( int j=0; j < vectors.size(); ++j )
            {
                double[][] iA = {vectors.elementAt(i)};
                Matrix Mi = new Matrix(iA).transpose();
                double[][] jA = {vectors.elementAt(j)};
                Matrix Mj = new Matrix(jA).transpose();
                
                double outerProduct = Mi.transpose().times(Mj).get(0,0);
                
                if( i != j )
                {
                    if( outerProduct > tolerance )
                    {
                        return false;
                    }
                }
                else
                {
                    double normSq = Math.pow(Mi.normF(),2.0);
                    if( (outerProduct - normSq) > tolerance )
                    {
                        return false;
                    }
                }
            }
        }
        
        return true;
    }
    
    /** 
     * Returns true if all the given vectors are centered around 0.0 within the given
     * tolerance, false otherwise.
     */
    public static boolean zeroSum(Vector<double[]> vectors, double tolerance)
    {
        for( int i=0; i < vectors.size(); ++i )
        {
            double[] tTrans = vectors.elementAt(i);
            double sum = XMatrix.sum(tTrans);
            if( sum > tolerance )
            {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Returns true if all the given matricies have 1.0 F norms within the given tolerance,
     * false otherwise
     */
    public static boolean allUnitLength(Vector<Matrix> matricies, double tolerance)
    {
        for( int i=0; i < matricies.size(); ++i )
        {
            if( (matricies.elementAt(i).normF()-1.0) > tolerance )
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Returns a copy of the given matrix with all the terms x
     * converted to 1/(x^2). If x is 0.0 it is left unchanged.
     */
    public static double[][] inverseSquare(double[][] M)
    {
        if( M.length <=0 )
        {
            return new double[0][0];
        }
        double[][] copy = new double[M.length][M[0].length];
        
        for( int i=0; i < M.length; ++i )
        {
            for( int j=0; j < M[i].length; ++j)
            {
                if( M[i][j] != 0.0 )
                {
                    copy[i][j] = 1.0/Math.pow(M[i][j],2.0);
                }
                else
                {
                    copy[i][j] = M[i][j];
                }
            }
        }
        
        return copy;
    }
    
    /**
     * Returns the product of the two given matricies. The inner
     * dimensions (n) must be equal.
     * 
     * @param A - m by n matrix
     * @param B - n my p matrix
     * @return - A*B m by p matrix
     */
    public static double[][] times(double[][] A, double[][] B)
    {
        if( A.length <= 0 )
        {
            return new double[0][0];
        }
        if( A[0].length != B.length )
        {
            logger.error("Inner matrix dimensions different in XMatrix.times()");
            return null;
        }
        if( B.length <= 0 )
        {
            return new double[0][0];
        }
        
        double[][] AB = new double[A.length][B[0].length];
        
        for( int i=0; i < A.length; ++i )
        {

            for( int k=0; k < B[0].length; ++k )
            {
                double product = 0.0;
                for( int j=0; j < B.length; ++j)
                {
                    product += A[i][j]*B[j][k];
                }
                AB[i][k] = product;
            }
        }
        
        return AB;
    }
}
