/*
 * Created on: Sep 16, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;

/**
 * Returns model (2) from the Subramanian paper
 * for prediction of logBB:
 * 
 * logBB = -0.0204 + 0.122*S_sssN - 0.114*Rotlbonds
 * + 0.0359*WNSA3 - 0.615*S_dsN + 0.313*AlogP
 * -0.0959*S_sssCH + 0.108*RadiusOfGyration
 * 
 * Computational models to predict blood-brain
 * barrier permeation and CNS activity
 * Govindan Subramanian and Douglas B. Kitchen
 * Journal of Computer-AIded Molecular Design
 * 1-20, 2003";
 * 
 * @author eduthie
 */
public class BBSubramanian3 extends MoleculeValueDesc
{
    public String getDescription()
    {
        return "Returns model (3) from the Subramanian paper<br>" +
        "for prediction of logBB. Requires 3D data.<br><br>" +
        "logBB = -0.0204 + 0.122*S_sssN - 0.114*Rotlbonds<br>" +
        "+ 0.0359*WNSA3 - 0.615*S_dsN + 0.313*AlogP<br>" +
        "-0.0959*S_sssCH + 0.108*RadiusOfGyration<br><br>" +
        "Computational models to predict blood-brain<br>" +
        "barrier permeation and CNS activity<br>" +
        "Govindan Subramanian and Douglas B. Kitchen<br>" +
        "Journal of Computer-Aided Molecular Design<br>" +
        "1-20, 2003";
    }
    

    /**
     * Returns Subramanian model number 3 
     */
    public double value(Molecule mol)
    {

        SmartsMatchCount S_sssN = new SmartsMatchCount();
        S_sssN.addPattern("[NX3H0]");
        SmartsMatchCount S_dsN = new SmartsMatchCount();
        S_dsN.addPattern("[NX2H0]");
        SmartsMatchCount S_sssCH = new SmartsMatchCount();
        S_sssCH.addPattern("[CX4H1]");
        
        double rotBonds = new RotatableBonds().value(mol);
        double wnsa3 = new WNSA3().value(mol);
        double logp = new XLogP().value(mol);
        double gyr = new RadiusOfGyration().value(mol);
        
        return
            -0.0204 + 0.122*S_sssN.value(mol) - 0.114*rotBonds
            + 0.0359*wnsa3 - 0.615*S_dsN.value(mol) + 0.313*logp
            -0.0959*S_sssCH.value(mol) + 0.108*gyr;
    }

}
