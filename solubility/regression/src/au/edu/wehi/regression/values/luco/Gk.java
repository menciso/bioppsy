/*
 * Created on: May 9, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.luco;

import joelib2.feature.types.atomlabel.AbstractENPauling;
import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;

import Jama.Matrix;
import au.edu.wehi.regression.util.*;
import au.edu.wehi.regression.values.MoleculeValueDesc;

import java.util.HashMap;

/**
 * The Gk topological charge index defined in:<br>
 * J. Chem Inf. Comput. Sci., Vol. 35, No. 2, 1995<br>
 * Pass the parameter k to the constructor.
 * 
 * @author eduthie
 */
public class Gk extends MoleculeValueDesc
{
    private boolean valence;
   
    int k;
    
    public Gk(int k)
    {
       this.k = k;
       this.valence = false;
    }
    
    /**
     * @param k - parameter 
     * @param valence - if true the valence form of Gk is calculated. This
     * sets the main diagonal elements in the adjacency matrix to the
     * electronegativity values of the associated atoms.
     */
    public Gk(int k, boolean valence)
    {
        this.k = k;
        this.valence = valence;
    }
    
    /**
     * Returns the Gk value where k was given to the constructor. Gk is defined as:<br>
     * Gk = sum over i>j of ( |CTij|*d(k,Dij) ) <br>
     * where d() is the Kronecker delta function.
     */
    public double value(Molecule molecule)
    {
        Molecule mol = (Molecule) molecule.clone();
        double[][] distance = XMoleculeHelper.distanceMatrix(mol);
        double[][] invSq = XMatrix.inverseSquare(distance);
        double[][] adj = XMoleculeHelper.adjacencyMatrix(distance);
        if(valence)
        {
            if( !addValence(mol,adj ))
            {
                return Double.NaN;
            }
        }
        double[][] M = XMatrix.times(adj,invSq);
        double[][] CT = new double[M.length][M[0].length];
        for( int i=0; i < M.length; ++i)
        {
            for( int j=0; j < M[i].length; ++j)
            {
                if( i == j )
                {
                    CT[i][j] = M[i][j];
                }
                else
                {
                    CT[i][j] = M[i][j] - M[j][i];
                }
            }
        }
        
        double gk = 0.0;
        
        for( int j=0; j < (CT.length-1); ++j)
        {
            for( int i=(j+1); i < CT.length; ++i)
            {
                int dij = (int) distance[i][j];
                if( dij == k )
                {
                    gk += Math.abs(CT[i][j]);
                }
            }
        }
        
        return gk;
    }
    
    /**
     * The main diagonal elements of the given adjacency matrix are replaced with the 
     * electronegativity values of the corresponding atom in the given Molecule.
     * 
     * @param mol - Molecule electronegativity values are obtained from.
     * @param adj - Adjacency matrix to process.
     * @return - true on success, false on failure
     */
    public boolean addValence(Molecule mol, double[][] adj)
    {
        for( int i=0; i < adj.length; ++i )
        {
            adj[i][i] = AbstractENPauling.calculate(mol.getAtom(i+1));
        }
        return true;
    }
}
