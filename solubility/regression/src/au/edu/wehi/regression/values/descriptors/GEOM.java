/*
 * Created on: Dec 8, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.descriptors;


import java.util.Arrays;
import joelib2.molecule.Atom;
import joelib2.util.iterator.AtomIterator;
import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import au.edu.wehi.regression.values.MoleculeValueDesc;
import joelib2.feature.types.atomlabel.AtomMass;

/*********************************************************
 * GEOM is a Descriptor that calculates the geometric moment 
 * of a molecule. The geometric moment is the principal moment
 * of inertia.
 * See <a href="http://kwon3d.com/theory/moi/prin.html">the reference</a>
 * for a description of how principal moments of inertia are 
 * calculated. 
 *
 * @author eduthie
 */
public class GEOM extends MoleculeValueDesc
{
    int axis;
    
    /**
     * @param axis - the axis around which to calculate the moment
     * can be 1, 2, or 3
     */
    public GEOM(int axis)
    {
        this.axis = axis;
    }
    
    
    public double value(Molecule mol)
    {
        if( (axis <= 0) || (axis > 3) )
        {
            Logger.getLogger(getClass()).error("Cannot calculate principal moment " +
                " of inertia of number: " + axis);
            return Double.NaN;
        }

        // values of the Inertia Tensor
        double Ixx = 0.0, Iyy = 0.0, Izz = 0.0,
               Ixy = 0.0, Ixz = 0.0, Iyz = 0.0;

        // the Inertia Tensor
        Matrix iTensor = new Matrix(3,3);

        // the eigenvalues
        double[] eigenvalues;

        double mass;
        Atom atom;
        AtomIterator ai;
        EigenvalueDecomposition eigenDecomp;

        // firstly we calculated the values 
        // of the Inertia Tensor
        ai = mol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            mass = AtomMass.getDoubleValue(atom);
            
            Ixx += mass *
                ( Math.pow(atom.get3Dy(),2) + Math.pow(atom.get3Dz(),2) );
            Iyy += mass *
                ( Math.pow(atom.get3Dx(),2) + Math.pow(atom.get3Dz(),2) );
            Izz += mass *
                ( Math.pow(atom.get3Dx(),2) + Math.pow(atom.get3Dy(),2) );

            Ixy += mass * atom.get3Dx() * atom.get3Dy();
            Ixz += mass * atom.get3Dx() * atom.get3Dz();
            Iyz += mass * atom.get3Dy() * atom.get3Dz();
        }

        iTensor.set(0,0,Ixx);
        iTensor.set(0,1,Ixy);
        iTensor.set(0,2,Ixz);
        iTensor.set(1,0,Ixy);
        iTensor.set(1,1,Iyy);
        iTensor.set(1,2,Iyz);
        iTensor.set(2,0,Ixz);
        iTensor.set(2,1,Iyz);
        iTensor.set(2,2,Izz);
        
        // note that the iTensor is symetric so it will
        // only have real eigenvalues

        eigenDecomp = new EigenvalueDecomposition(iTensor);
        eigenvalues = eigenDecomp.getRealEigenvalues();

        // the eigenvalues are the principal moments of
        // inertia. We sort then to get the corect
        // order Ix <= Iy <= Iz
        Arrays.sort(eigenvalues);
        return eigenvalues[axis-1];
    }

}
