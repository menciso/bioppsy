/*
 * Created on: Dec 8, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.descriptors;

import joelib2.molecule.Bond;
import joelib2.molecule.Atom;
import joelib2.util.iterator.BondIterator;
import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;
import joelib2.feature.types.atomlabel.AtomMass;
import joelib2.feature.types.atompair.BasicAPDistanceMetric;

/**
 * GRAV6, the  cube root of the gravitational index of the given molecule.
 * The gravitational index is a sum over all heavy atom pairs
 * in a molecule and is computed as the product of the
 * atom masses divided by the square of the distance between them.
 * <br>grav = sum((mi*mj)/rij^2) <br>
 * This descriptor is taken from:
 * Katritzky, A. R. et al.
 * J.Phys.Chem 1996, 100, 10400-10407
 * 
 * @author eduthie
 */
public class GRAV6 extends MoleculeValueDesc
{

    /* (non-Javadoc)
     * @see au.edu.wehi.regression.values.MoleculeValueDesc#value(joelib2.molecule.Molecule)
     */
    public double value(Molecule mol)
    {
        Bond bond;
        double grav = 0.0;

        BondIterator bi = mol.bondIterator();
        while( bi.hasNext() )
        {
            bond = bi.nextBond();
            double distance = getDistance(bond);
            grav += 
            (
                (
                    AtomMass.getDoubleValue(bond.getBegin())*
                    AtomMass.getDoubleValue(bond.getEnd())
                )
                / Math.pow(distance,2.0)
            );
        }

        return Math.pow(grav,1/3.0);
    }
    
    /**
     * Returns the 3d distance between the two atoms in the given bond
     */
    private double getDistance(Bond bond)
    {
        Atom atom1 = bond.getBegin();
        Atom atom2 = bond.getEnd();
        
        double distance = 
            Math.pow(atom1.get3Dx()-atom2.get3Dx(),2.0) +
            Math.pow(atom1.get3Dy()-atom2.get3Dy(),2.0) +
            Math.pow(atom2.get3Dz()-atom2.get3Dz(),2.0);
        
        distance = Math.pow(distance,0.5);
        
        return distance;
        
    }

}
