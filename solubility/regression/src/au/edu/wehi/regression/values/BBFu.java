/*
 * Created on: Sep 19, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.luco.McGowanVx;
import java.io.IOException;

/**
 * Returns the logBB value calucated as:
 * 
 * logBB = -13.31V^2 + 9.601V - 2.231PSA - 0.5290
 * 
 * McGowan characteristic volumes and TPSA are
 * used hence only 2D data is required.
 * 
 * A Predictive Model for Blood-Brain Barrier Penetration
 * Xuchun Fu, Zhifang Song, Wenquan Liang
 * email: fuxc@zucc.edu.cn
 * Phone: 86-571-88018711
 * Fax: 86-571-88018441
 * 
 * @author eduthie
 */
public class BBFu extends MoleculeValueDesc
{
    public String getDescription()
    {
        return
        
        "Returns the logBB value calucated as:<br><br>" +
        "logBB = -13.31V^2 + 9.601V - 2.231PSA - 0.5290<br><br>" +
        "McGowan characteristic volumes and TPSA are<br>" +
        "used hence only 2D data is required. However the<br>" +
        "method does not work if there are atoms that are not <br>" +
        "recognised in the McGowan list.<br><br>" +
        "A Predictive Model for Blood-Brain Barrier Penetration<br>" +
        "Xuchun Fu, Zhifang Song, Wenquan Liang<br>" +
        "email: fuxc@zucc.edu.cn<br>" +
        "Phone: 86-571-88018711<br>" +
        "Fax: 86-571-88018441";
    }
    
    /**
     * Returns logBB = -13.31V^2 + 9.601V - 2.231PSA - 0.5290
     */
    public double value(Molecule mol)
    {
        try
        {
            double V = new McGowanVx().value(mol);
            double psa = new TPSA().value(mol);
            System.out.println(psa);
            double result =
                (-13.31*(Math.pow(V,2.0))) + 9.601*V - 2.231*psa - 0.5290*10000;
            return result/100000;
        }
        catch( IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
            return Double.NaN;
        }
    }

}
