/*
 * Created on: Sep 15, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;

import joelib2.molecule.Molecule;

import org.apache.log4j.Logger;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.qsar.DescriptorValue;
import org.openscience.cdk.qsar.MomentOfInertiaDescriptor;
import org.openscience.cdk.qsar.result.DoubleArrayResult;

import au.edu.wehi.regression.util.CDKConverter;

/**
 * Moment of Inertia of the molecule
 * Requires 3d data
 * 
 * @author eduthie
 */
public class MomentOfInertia extends MoleculeValueDesc
{

    /**
     * Returns a text description of the descriptor for the user
     */
    public String getDescription()
    {
        return "Moment of Intertia of the molecule<br>" +
               "Requires 3D data";
    }
    
    /**
     * Returns Moment of Inertia
     */
    public double value(Molecule mol)
    {
        try
        {
            org.openscience.cdk.Molecule cdkMol = 
                CDKConverter.convertToCDK(mol);
            MomentOfInertiaDescriptor moi = new MomentOfInertiaDescriptor();
            DescriptorValue value = moi.calculate(cdkMol);
            DoubleArrayResult result = (DoubleArrayResult) value.getValue();
            return result.get(0);
        }
        catch( CDKException cdke)
        {
            Logger.getLogger(getClass()).error(cdke.getMessage());
            return Double.NaN;
        }
    }

}
