/*
 * Created on: Dec 9, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.descriptors;

import joelib2.molecule.Bond;
import joelib2.molecule.Molecule;
import joelib2.util.iterator.BondIterator;

import org.apache.log4j.Logger;

import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * Returns the number of double bonds in the molecule
 * 
 * @author eduthie
 */
public class NDB extends MoleculeValueDesc
{

    /**
     * Returns the number of double bonds in the molecule
     */
    public double value(Molecule mol)
    {
        int ndb = 0;
        BondIterator bi = mol.bondIterator();
        while(bi.hasNext())
        {
            Bond bond = bi.nextBond();
            if( (bond.getBondOrder() == 2) && (!bond.isBondOrderAromatic()))
            {
                ++ndb;
            }
        }
        return ndb;
    }

}
