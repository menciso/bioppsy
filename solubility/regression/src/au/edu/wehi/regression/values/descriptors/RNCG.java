/*
 * Created on: Dec 8, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.descriptors;

import joelib2.molecule.Atom;
import joelib2.util.iterator.AtomIterator;
import joelib2.molecule.Molecule;
import joelib2.feature.types.atomlabel.AtomPartialCharge;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/**
 * RNCG, the Relative Negative chage of a molecule.
 * This descriptor is taken from:
 * Stanton, D. T.; Jurs, P. C.
 * Anal. Chem 1990, 62, 2323-2329
 * 
 * @author eduthie
 */
public class RNCG extends MoleculeValueDesc
{

    /* (non-Javadoc)
     * @see au.edu.wehi.regression.values.MoleculeValueDesc#value(joelib2.molecule.Molecule)
     */
    public double value(Molecule mol)
    {
        double lowestCharge = 0.0;
        double sum = 0.0;
        double charge;
        Atom atom;

        AtomIterator ai = mol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            charge = AtomPartialCharge.getPartialCharge(atom);
            if( charge < 0.0 )
            {
                sum += charge;
                lowestCharge = Math.min(charge,lowestCharge);
            }
        }

        // make sure we are not dividing by 0
        if( sum == 0.0 )
        {
            // return 1.0 here, the highest possible number
            // returned.
            return 1.0;
        }

        return lowestCharge / sum;
    }

}
