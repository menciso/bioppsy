/*
 * Created on: Dec 8, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values.descriptors;

import au.edu.wehi.regression.util.XMoleculeHelper;
import java.util.Vector;
import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.MoleculeValueDesc;

/*********************************************************
 * The Molecular Distance Edge value for this molecule.
 * This descriptor is calculated between atoms with heavy valence
 * (valence excluding hydrogens) i and heavy valence j.
 * Thus several MDE descriptors are possible and these
 * exist as subclasses of MDE.
 * This descriptor is taken from:<br>
 * Shushen Liu, Chenzhong Cao, and Zhiliang Li, 
 * J. Chem. Inf. Comput. Sci. 1998, 38, 387-394
 * 
 * @author eduthie
 *****************************************************************/
public class MDE extends MoleculeValueDesc
{
    int i,j;
    static Logger logger = Logger.getLogger(MDE.class);

    /***************************************************************
    * This descriptor is calculated between atoms with heavy valence
    * (valence excluding hydrogens) i and heavy valence j, j >= i.
    ****************************************************************/
    public MDE(int i, int j)
    {
        super();
        this.i = i;
        this.j = j;
    }
    
    
    /**
     * Returns MDE i j where i and j are passed to the constructor
     */
    public double value(Molecule originalMol)
    {
        Molecule mol = (Molecule) originalMol.clone(true);
        mol.deleteHydrogens();
        
        if( (i<0) || (j<0) )
        {
            logger.error("MDE calculated with negative i or j");
            return Double.NaN;
        }

        if( j < i )
        {
            logger.error("MDE calculated for j < i");
            return Double.NaN;
        }

        int count, iCount, jCount, hvyValence, iIndex, jIndex;
        // the MDE d must be a double since it gets rather large
        double d = 1.0;
        int n = 0; // number of distances calculated

        // Vector of Integer containing indicies of
        // atoms with heavy valence i and j respectively
        Vector vectorI = new Vector();
        Vector vectorJ = new Vector();

        // Find the indicies of the atoms with heavy
        // valence i and j.
        for( count = 1; count <= mol.getAtomsSize(); ++count )
        {
            hvyValence = mol.getAtom(count).getValence();
            if( hvyValence == i )
            {
                vectorI.add(new Integer(count));
            }
            if( hvyValence == j )
            {
                vectorJ.add(new Integer(count));
            }
        }

        // get the shortest distances between all atoms
        // in terms of number of edges on the molecular 
        // graph
        double[][] distanceMatrix = XMoleculeHelper.distanceMatrix(mol);

        // For each atom with heavy valence i, 
        // find the distance between it and all
        // atoms with heavy valence j. 
        // Then d is multiplied by this value
        // and n incremented by one
        for( iCount = 0; iCount < vectorI.size(); ++iCount)
        {
            iIndex = ((Integer) vectorI.elementAt(iCount)).intValue();
            if( i == j )
            {
                // don't count the same combination twice
                jCount = iCount;
            }
            else
            {
                jCount = 0;
            }
            for( ; jCount < vectorJ.size(); ++jCount)
            {
                jIndex = ((Integer) vectorJ.elementAt(jCount)).intValue();
                if( iIndex != jIndex )
                {
                    d *= distanceMatrix[iIndex-1][jIndex-1];
                    ++n;
                }
            }
        }

        if( n == 0.0 || d == 0.0 )
        {
            return 0.0;
        }

        return Math.pow(d, 1.0 / (2 * n) );
    }

}
