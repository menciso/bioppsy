/*
 * Created on: Jun 1, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.values;


import java.io.*;
import java.util.Properties;

import joelib2.io.BasicIOType;
import joelib2.io.BasicReader;
import joelib2.molecule.Molecule;
import org.apache.log4j.Logger;

import Jama.Matrix;
import au.edu.wehi.regression.Constants;
import au.edu.wehi.regression.RegressionMoleculeVector;
import au.edu.wehi.regression.rows.*;
import au.edu.wehi.regression.util.*;

/**
 * @author eduthie
 */
public class NNBB extends MoleculeValueDesc
{
    private String trainingFilename;
    private String bbFilename;
    private RegressionMoleculeVector rmv;
    private double[][] X;
    private double[][] xStats;
    private double[][] Y;
    
    private static NNBB nnbb = null;
    
    public static NNBB instance()
    {
        if( nnbb == null )
        {
            System.out.println("CONSTRUCTING !!!");
            nnbb = new NNBB();
        }
        return nnbb;
    }
    
    private NNBB()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(Constants.PROPERTIES);
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            trainingFilename = typesProp.getProperty("BBTrainingFilename");
            BasicIOType type = BasicReader.checkGetInputType(trainingFilename);
            InputStream in = getClass().getResourceAsStream(trainingFilename);
            rmv = new RegressionMoleculeVector(in,type,type);
            bbFilename = typesProp.getProperty("BBTrainingData");
            in = getClass().getResourceAsStream(bbFilename);
            rmv.loadBBData(in);
            rmv.setDependentDescriber(new BBRow());
            rmv.setIndependentDescriber(new LucoRow());
            X = rmv.getXMatrix();
            Y = rmv.getYMatrix();
            xStats = XMatrix.normalise(X);
        }
        catch( IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
        }
    }

    public double value(Molecule mol)
    {
        MoleculeRowDesc xDesc = rmv.getIndependentDescriber();
        double[] xs = xDesc.getRow(mol);
        double[][] Xs = new double[1][];
        Xs[0] = xs;
        XMatrix.normalise(Xs,xStats);
        
        double min = XArrays.distance(X[0],xs);
        int minIndex = 0;
        
        for( int i=0; i < X.length; ++i )
        {
            double distance = XArrays.distance(X[i],xs);
            if( distance < min )
            {
                min = distance;
                minIndex = i;
            }
        }
        return Y[minIndex][0];
    }

}
