/*
 * Created on: Apr 27, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.regression.rows;

import java.io.IOException;
import java.io.InputStream;
import org.apache.log4j.Logger;
import au.edu.wehi.regression.values.*;

/**
 * The HouRow class calculates the independent
 * variables for the Hou model. These variables 
 * include a set of SMARTS patterns, the hydrophobic
 * carbon correction factor and the square of the 
 * molecular weight. Please use the init() method
 * before using a HouRow object.
 * 
 * @author eduthie
 */
public class HouRow extends MoleculeRowDesc
{
    static Logger logger = Logger.getLogger(HouRow.class);
    
    public HouRow()
    {
        super();
    }
    
    /*
     * Initialises the SMARTS patterns and the molecular
     * weight variable. Returns true on success and false
     * on failure.
     */
    public boolean init()
    {
        try
        {
            String houSmarts = "hou.smarts";
            InputStream in = getClass().getResourceAsStream(
                houSmarts);
            if( in.available() == 0 )
            {
                logger.error("Unable to read: " + houSmarts);
                return false;
            }
            
            // add the smarts as features
            if( !addSingleMatchSmarts(in))
            {
                logger.error("Unable to add smarts from: " + houSmarts);
                return false;
            }
            
            // add the hydrophobic carbon
            addFeature(new HydrophobicCarbon());
            // add the MWT2
            addFeature(new MWT2());
            // add the intercept
            addFeature(new Intercept());
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        /*
        catch( FeatureException fe )
        {
            logger.error(fe.getMessage());
            return false;
        }
        */
        return true;
    }
}
