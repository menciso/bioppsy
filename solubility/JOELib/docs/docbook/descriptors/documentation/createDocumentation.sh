#!/bin/bash
echo "#!/bin/sh" > generate.sh
for filename in ../*.sgml
do
  file=`basename ${filename%.sgml}`
  echo generating single descriptor SGML file from ${file} ...
  #
  echo "<!doctype article public \"-//OASIS//DTD DocBook XML V4.2//EN\" [" > ${file}.sgml
  echo "<!ENTITY "${file}" SYSTEM \"../"${file}.sgml \"\> >> ${file}.sgml
  echo "<!ENTITY bibliography SYSTEM \"../../bibliography.sgml"\"\> >> ${file}.sgml
  echo "]>" >> ${file}.sgml
  echo "  <article>" >> ${file}.sgml
  echo "  <?dbhtml filename='"${file}".html' output-dir='.'>" >> ${file}.sgml
  echo "  <sect1 id=\"joelib.descriptors.\"" >> ${file}.sgml
  echo "  >" >> ${file}.sgml
  echo "  <title>Descriptor</title>" >> ${file}.sgml
  echo "    &"${file}";" >> ${file}.sgml
  echo "  </sect1>" >> ${file}.sgml
  echo "    &bibliography;" >> ${file}.sgml
  echo "  </article>" >> ${file}.sgml
  #
  echo "echo create single descriptor documentation for "${file}.sgml" ..." >> generate.sh
  echo ${JADE}" -t sgml -d $DB_HTML "${file}.sgml >> generate.sh
  echo ${JADE}" -t rtf -d $DB_PRINT "${file}.sgml >> generate.sh
  #
done
#
exit 0
