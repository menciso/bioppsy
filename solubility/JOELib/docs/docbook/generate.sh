#!/bin/sh

## csh-entries for DocBook
## unix
export DB_HTML=/usr/share/sgml/docbook/dsssl-stylesheets-1.78/html/docbook.dsl
export DB_PRINT=/usr/share/sgml/docbook/dsssl-stylesheets-1.78/print/docbook.dsl
export SGML_CATALOG_FILES=/usr/share/sgml/docbook_4.2/docbook.cat:/usr/share/sgml/openjade/catalog
export JADE="jade"
export CONVERT="convert"

## windows
#setenv DB_HTML "C:/cygwin/usr/share/sgml/docbook/db177/html/docbook.dsl"
#setenv DB_PRINT "C:/cygwin/usr/share/sgml/docbook/db177/print/docbook.dsl"
#setenv SGML_CATALOG_FILES "C:/cygwin/usr/share/sgml/docbook/docbook41/docbook.cat;C:/cygwin/usr/share/sgml/docbook/db177/common/catalog;C:/cygwin/usr/share/sgml/docbook/jade121/catalog"
#setenv JADE "C:/cygwin/usr/share/sgml/docbook/jade121/jade"
#setenv CONVERT "/cygdrive/c/cygwin/usr/bin/convert.exe"

#${JADE} -t rtf -d $DB_PRINT  C:/cygwin/usr/share/sgml/docbook/jade121/xml.dcl  JOELibTutorial.xml
#${JADE} -t sgml -d $DB_HTML C:/cygwin/usr/share/sgml/docbook/jade121/xml.dcl  JOELibTutorial.xml

# create temporary index file
perl /usr/share/sgml/docbook/dsssl-stylesheets-1.78/bin/collateindex.pl -N -o tmpindex.sgml
# create RTF from SGML
${JADE} -t rtf -d $DB_PRINT JOELibTutorial.sgml
# create HTML from SGML
${JADE} -t sgml -d $DB_HTML -V html-index JOELibTutorial.sgml
# create SGML index file from HTML.index
perl /usr/share/sgml/docbook/dsssl-stylesheets-1.78/bin/collateindex.pl -o tmpindex.sgml HTML.index
# index file postprocessing for using later 'index.html'
sed s/"<index>"/"<index><?dbhtml filename='index.html' output-dir='.' >"/g tmpindex.sgml > tmpindex2.sgml
# index file postprocessing to adapt the path's
sed s/"\.\.\/"/""/g tmpindex2.sgml > generatedindex.sgml

# create PS and PDF versions
# stable JadeTeX->DVI ?
# does anyone know an automatic RTF2PS utility
# please mail me: wegnerj@informatik.uni-tuebingen.de
