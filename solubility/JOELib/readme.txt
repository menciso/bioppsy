QuickStart:

1. To start an example use

   'convert.bat joelib/test/test.mol' under windows
   or
  'sh convert.sh joelib/test/test.mol' or
  './convert.sh joelib/test/test.mol'  under linux

  For further targets try:

  sh build.sh -projecthelp
  or
  build.bat -projecthelp
  and then
  sh build.sh SMARTTest
  or
  build.bat SMARTTest

Here are some links:
CYGWIN: http://www.cygwin.com/
ANT:    http://jakarta.apache.org/ant/

2. A short documentation can be found in

   'docs/tutorial'

3. The more technical JOELib-API documentation is located in

   'docs/api/index.html'


Have much fun, JOErg !;-)

--------------------------------------------------------------
JOELib-Homepage:    http://www.joelib.de
JOELib@SourceForge: http://sourceforge.net/projects/joelib
JOELib@FreshMeat:   http://freshmeat.net/projects/joelib/?topic_id=813%2C913
More questions (developer list): http://sourceforge.net/mail/?group_id=39708
More questions (help list):      http://sourceforge.net/mail/?group_id=39708
More questions (admin):          wegnerj@informatik.uni-tuebingen.de
