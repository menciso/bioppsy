@echo off

REM --------------------------------------------
REM No need to edit anything past here
REM --------------------------------------------
set _BUILDFILE=%BUILDFILE%
set BUILDFILE=ant/build.xml

:final

set _CLASSPATH=%CLASSPATH%

if "%JAVA_HOME%" == "" goto javahomeerror
if exist %JAVA_HOME%\lib\tools.jar set CLASSPATH=%CLASSPATH%;%JAVA_HOME%\lib\tools.jar;%JOELIBDIR%\build;.

if "%JOELIBDIR%" == "" goto joelibdirerror

set CLASSPATH=%CLASSPATH%;lib/ant.jar
set CLASSPATH=%CLASSPATH%;lib/crimson.jar
set CLASSPATH=%CLASSPATH%;lib/itext-0.94.jar
set CLASSPATH=%CLASSPATH%;lib/jaxp.jar
set CLASSPATH=%CLASSPATH%;lib/jdom.jar
set CLASSPATH=%CLASSPATH%;lib/junit-3.2.jar
set CLASSPATH=%CLASSPATH%;lib/log4j.jar
set CLASSPATH=%CLASSPATH%;lib/mm.mysql-2.0.14-bin.jar
set CLASSPATH=%CLASSPATH%;lib/optional.jar
set CLASSPATH=%CLASSPATH%;lib/sgt.jar
set CLASSPATH=%CLASSPATH%;lib/vecmath.jar
set CLASSPATH=%CLASSPATH%;lib/xerces.jar

echo %JAVA_HOME%/bin/java.exe -cp %CLASSPATH% joelib.test.Convert %1 %2 %3 %4 %5 %6 %7 %8 %9

%JAVA_HOME%/bin/java.exe -cp %CLASSPATH% joelib.test.Convert %1 %2 %3 %4 %5 %6 %7 %8 %9

goto end

REM -----------ERROR-------------
:javahomeerror
echo "ERROR: JAVA_HOME not found in your environment."
echo "Please, set the JAVA_HOME variable in your environment to match the"
echo "location of the Java Virtual Machine you want to use."
goto end

:joelibdirerror
echo "ERROR: JOELIBDIR not found in your environment."
echo "Please, set the JOELIBDIR variable in your environment to match the"
echo "location of the JOELib tools you want to use."
goto end

:end
set BUILDFILE=%_BUILDFILE%
set _BUILDFILE=
set CLASSPATH=%_CLASSPATH%
set _CLASSPATH=

:usage

:final