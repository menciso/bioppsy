///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOE.java,v $
//  Purpose:  File converting.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2003/08/22 15:56:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib;


/*==========================================================================*
 * IMPORTS
 *==========================================================================     */
import joelib.test.Convert;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 * Class for converting molecules and calculating descriptors.
 *
 * <p>
 * <blockquote><pre>
 * Usage:
 * java -cp . joelib.JOE [options] &lt;input file> [&lt;output file>]
 *
 * Options:
 * [-i&lt;inputFormat>]       - Format of the input file
 * [-o&lt;outputFormat>]      - Format of the output file
 * [-h]                    - Remove hydrogens from molecule
 * [+h]                    - Add hydrogens to molecule
 * [+p]                    - Add only polar hydrogens (+h implicit)
 * [-e]                    - Converts only non-empty molecules
 * [-d]                    - Remove all descriptors from the molecule
 * [+d]                    - Add all available descriptors to the molecule
 * [+v]                    - Switch verbosity ON
 * [+snd]                  - Show all available native value descriptors
 * [+sad]                  - Show all available atom property descriptors
 * [+sall]                 - Show all available descriptors
 * [+x&lt;descriptor name>]   - Converts only molecules where &lt;descriptor name> exists
 * [-r&lt;skip  desc. rule>]  - Skips molecules, if rule fits
 * [+r&lt;conv. desc. rule>]  - Converts only molecules where rule fits
 * [+f&lt;lineStructure>]     - Required if you use FLAT output format which other input format
 * [+s&lt;lineStructure>]     - Can be used for an alternate SMILES entry line structure
 * [-m&lt;SMARTS rule>]       - Skips molecules, if SMARTS rule fits
 * [+m&lt;SMARTS rule>]       - Converts only molecules where SMARTS rule fits
 * [-um&lt;SMARTS rule>]      - Skips molecules, if SMARTS rule fits
 * [+um&lt;SMARTS rule>]      - Converts only molecules where SMARTS rule fits
* [-?][--help]            - Shows this message
 *
 *If no output file is defined, all molecules will be written to stdout.
 *
 *Filter rules have the form:
 *&lt;native value descriptor>&lt;relation>&lt;value>
 *where &lt;relation> is &lt;, &lt;=, ==, >, >= or !=
 *Example:
 *"+rNumber_of_halogen_atoms==2"
 *
 *SMARTS filter rules have the form:
 *&lt;SMARTS pattern>&lt;relation>&lt;value>
 *where &lt;relation> is &lt;, &lt;=, ==, >, >= or !=
 *Example:
 *"+umaNC=O==1"
 *Converts all molecules, where the molecule contains ONE NC=O group connected to an aromatic atom (aNC=O).
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2003/08/22 15:56:15 $
 * @see joelib.test.Convert
 * @see joelib.process.filter.NativeValueFilter
 * @cite smarts
 */
public class JOE
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * main
     *-------------------------------------------------------------------------*/

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        Convert convert = new Convert();

        int status = convert.parseCommandLine(args);

        if (status == Convert.CONTINUE)
        {
            convert.convert();
        }
        else if (status == Convert.STOP_USAGE)
        {
            convert.usage();
            System.exit(1);
        }
        else if (status == Convert.STOP)
        {
            System.exit(0);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
