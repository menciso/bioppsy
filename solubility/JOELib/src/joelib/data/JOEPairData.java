///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEPairData.java,v $
//  Purpose:  Use to store attribute/value relationships.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.18 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import org.apache.log4j.Category;

import joelib.desc.DescResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;

import joelib.util.JOEHelper;


/**
 *  Use to store attribute/value relationships.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.18 $, $Date: 2004/03/15 23:16:13 $
 */
public class JOEPairData extends JOEGenericData implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private static Category logger = Category.getInstance(
            "joelib.data.JOEPairData");

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    protected Object _value;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEPairData object
     */
    public JOEPairData()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute(dataType.getDefaultAttr());
    }

    /**
     *  Constructor for the JOEPairData object
     *
     * @param  attribute  Description of the Parameter
     * @param  value      Description of the Parameter
     */
    public JOEPairData(String attribute, Object value)
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute(attribute);
        _value = value;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */

    /**
     *  Sets the value attribute of the JOEPairData object
     *
     * @param  v  The new value value
     */
    public void setValue(final Object v)
    {
        _value = v;
    }

    //public void    setValue(String v)       {_value = v;}

    /**
     *  Gets the value attribute of the JOEPairData object
     *
     * @return    The value value
     */
    public Object getValue()
    {
        return (_value);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        if (_value == null)
        {
            logger.warn("Pair data '" + getAttribute() +
                "' contains no value.");

            return "";
        }

        if (JOEHelper.hasInterface(_value, "DescResult"))
        {
            return ((DescResult) _value).toString(ioType);
        }
        else
        {
            return _value.toString();
        }
    }

    public String toString()
    {
        return toString(IOTypeHolder.instance().getIOType("UNDEFINED"));
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
