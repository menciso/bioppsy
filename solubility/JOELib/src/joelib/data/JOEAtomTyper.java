///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEAtomTyper.java,v $
//  Purpose:  Atom typer.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.25 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.PropertyHolder;

import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;
import joelib.smarts.types.SMARTSPatternInt;
import joelib.smarts.types.SMARTSPatternString;

import joelib.util.JHM;
import joelib.util.iterator.AtomIterator;


/**
 * Atom typer based on structural expert rules.
 * The definition file can be defined in the
 * <tt>joelib.data.JOEAtomTyper.resourceFile</tt> property in the {@link wsi.ra.tool.PropertyHolder}.
 * The {@link wsi.ra.tool.ResourceLoader} loads the <tt>joelib.properties</tt> file for default.
 *
 * <p>
 * For assigning atom types using a geometry-based algorithm have a look at {@cite ml91} and the dot
 * connecting method {@link joelib.molecule.JOEMol#connectTheDots()}.
 *
 * <p>
 * Default:<br>
 * joelib.data.JOEAtomTyper.resourceFile=<a href="http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/joelib/joelib/src/joelib/data/plain/atomtype.txt?rev=HEAD&content-type=text/vnd.viewcvs-markup">joelib/data/plain/atomtype.txt</a>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.25 $, $Date: 2004/07/25 20:42:58 $
 * @see wsi.ra.tool.PropertyHolder
 * @see wsi.ra.tool.ResourceLoader
 */
public class JOEAtomTyper extends JOEGlobalDataBase implements HardCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private static final String VENDOR = "http://joelib.sf.net";
    private static final String RELEASE_VERSION = "$Revision: 1.25 $";
    private static final String RELEASE_DATE = "$Date: 2004/07/25 20:42:58 $";

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.data.JOEAtomTyper");
    private static JOEAtomTyper atomtyper;
    protected static final String DEFAULT_RESOURCE = "joelib/data/plain/atomtype.txt";

    //~ Instance fields ////////////////////////////////////////////////////////

    private String releaseDate;
    private String releaseVersion;
    private String vendor;

    // of type SMARTSPatternInt

    /**
     *  {@link java.util.Vector} of {@link joelib.smarts.types.SMARTSPatternString}  to store the atom type definitions.
     */
    private Vector externalTypeRule;

    // of type int[1]-Vector

    /**
     *  {@link java.util.Vector} of {@link joelib.smarts.types.SMARTSPatternInt} to store the atom hybridisation definitions.
     */
    private Vector hybridisation;

    // of type SMARTSPatternInt

    /**
     *  {@link java.util.Vector} of {@link joelib.smarts.types.SMARTSPatternInt} to store the atom implicite valence definitions.
     */
    private Vector impliciteValence;

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    //  private         int           _rc;

    /**
     *  {@link java.util.Vector} of <tt>int[1]</tt>.
     */
    private Vector matchList;

    //~ Constructors ///////////////////////////////////////////////////////////

    // of type SMARTSPatternString

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the JOEAtomTyper object
     */
    private JOEAtomTyper()
    {
        initialized = false;

        Properties prop = PropertyHolder.instance().getProperties();
        resourceFile = prop.getProperty(this.getClass().getName() +
                ".resourceFile", DEFAULT_RESOURCE);

        matchList = new Vector();
        hybridisation = new Vector();
        impliciteValence = new Vector();
        externalTypeRule = new Vector();

        vendor = VENDOR;
        releaseVersion = JOEKernel.transformCVStag(RELEASE_VERSION);
        releaseDate = JOEKernel.transformCVStag(RELEASE_DATE);

        JOEKernel.instance().addHardCodedKernel(this);
        init();
        JOEKernel.instance().addSoftCodedKernel(this);

        logger.info("Using atom type model: " + resourceFile);

        //logger.info("... to estimate hybridisation, implicite valence, and atom type.");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     */
    public void assignHyb(JOEMol mol)
    {
        if (!initialized)
        {
            init();
        }

        JOEAromaticTyper.instance().assignAromaticFlags(mol);

        mol.setHybridizationPerceived();

        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atom.setHyb(0);
        }

        int[] itmp;

        for (int i = 0; i < hybridisation.size(); i++)
        {
            SMARTSPatternInt pi = (SMARTSPatternInt) hybridisation.get(i);

            if (pi.sp.match(mol))
            {
                matchList = pi.sp.getMapList();

                for (int j = 0; j < matchList.size(); j++)
                {
                    //Vector v = (Vector)matchList.get(j);
                    //itmp = (int[])v.get(0);
                    itmp = (int[]) matchList.get(j);
                    mol.getAtom(itmp[0]).setHyb(pi.i);
                }
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     */
    public void assignImplicitValence(JOEMol mol)
    {
        int[] itmp;

        if (!initialized)
        {
            init();
        }

        mol.setImplicitValencePerceived();

        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atom.setImplicitValence(atom.getValence());
        }

        for (int i = 0; i < impliciteValence.size(); i++)
        {
            SMARTSPatternInt pi = (SMARTSPatternInt) impliciteValence.get(i);

            if (pi.sp.match(mol))
            {
                matchList = pi.sp.getMapList();

                //if(logger.isDebugEnabled())logger.debug("assign imp. val.:"+pi.sp.getSMARTS());
                for (int j = 0; j < matchList.size(); j++)
                {
                    //Vector v = (Vector)matchList.get(j);
                    //itmp = (int[])v.get(0);
                    itmp = (int[]) matchList.get(j);
                    mol.getAtom(itmp[0]).setImplicitValence(pi.i);

                    //          System.out.println("a:"+mol.getAtom(itmp[0]).getIdx()+" h:"+pi.i+" "+pi.sp);
                }
            }
        }

        if (!mol.hasAromaticCorrected())
        {
            correctAromaticNitrogens(mol);
        }

        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (atom.getImplicitValence() < atom.getValence())
            {
                atom.setImplicitValence(atom.getValence());
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     */
    public void assignTypes(JOEMol mol)
    {
        int[] itmp;

        if (!initialized)
        {
            init();
        }

        mol.setAtomTypesPerceived();

        for (int i = 0; i < externalTypeRule.size(); i++)
        {
            SMARTSPatternString ps = (SMARTSPatternString) externalTypeRule.get(i);

            if (ps.sp.match(mol))
            {
                matchList = ps.sp.getMapList();

                for (int j = 0; j < matchList.size(); j++)
                {
                    //Vector v = (Vector)matchList.get(j);
                    //itmp = (int[])v.get(0);
                    itmp = (int[]) matchList.get(j);
                    mol.getAtom(itmp[0]).setType(ps.s);
                }
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     */
    public void correctAromaticNitrogens(JOEMol mol)
    {
        if (!initialized)
        {
            init();
        }

        if (mol.hasAromaticCorrected())
        {
            return;
        }

        mol.setAromaticCorrected();

        return;

        //    int j;
        //    JOEAtom atom, nbr, a;
        //    JOEBitVec curr=new JOEBitVec();
        //    JOEBitVec used=new JOEBitVec();
        //    JOEBitVec next=new JOEBitVec();
        //    Vector v_N=new Vector();
        //    Vector v_OS=new Vector();    // of type JOEAtom
        //
        //    Vector _aromNH = new Vector();     // of type boolean[1]
        //    boolean btmp[];
        //    _aromNH.clear();
        //    _aromNH.setSize(mol.numAtoms()+1);
        //
        //    AtomIterator ait1 = mol.atomIterator();
        //    while(ait1.hasNext())
        //    {
        //      atom = ait1.nextAtom();
        //      if (atom.isAromatic() && !atom.isCarbon() && !used.get(atom.getIdx()) )
        //      {
        //        Vector rsys=new Vector(); // of type JOEAtom
        //        rsys.add(atom);
        //        curr.set( atom.getIdx() );
        //        used.set( atom.getIdx() );
        //
        //        while (curr.size()!=0)
        //  	    {
        //      	  next.clear();
        //      	  //for(j=curr.nextSetBit(0); j>=0; j=curr.nextSetBit(j+1))
        //                for (j = curr.nextBit(-1);j != curr.endBit();j = curr.nextBit(j))
        //      	  {
        //    	      atom = mol.getAtom(j);
        //
        //            NbrAtomIterator nait = atom.nbrAtomIterator();
        //            while(nait.hasNext())
        //            {
        //              nbr = nait.nextNbrAtom();
        //              if (!nait.actualBond().isAromatic()) continue;
        //        		  if ( used.get(nbr.getIdx()) ) continue;
        //
        //        		  rsys.add(nbr);
        //        		  next.set( nbr.getIdx() );
        //        		  used.set( nbr.getIdx() );
        //            }
        //  	      }
        //
        //  	      curr.set(next);
        //  	    }
        //
        //        //count number of electrons in the ring system
        //        v_N.clear();
        //        v_OS.clear();
        //        int nelectrons = 0;
        //
        //        for (int m=0; m< rsys.size(); m++)
        //      	{
        //      	  a = (JOEAtom) rsys.get(m);
        //
        //      	  boolean hasExoDoubleBond = false;
        //
        //      	  NbrAtomIterator nait = a.nbrAtomIterator();
        //          while(nait.hasNext())
        //          {
        //            nbr = nait.nextNbrAtom();
        //      	    if (nait.actualBond().getBO() == 2 && !nait.actualBond().isInRing())
        //      	      if (nbr.isOxygen() || nbr.isSulfur() || nbr.isNitrogen())
        //      		      hasExoDoubleBond = true;
        //          }
        //
        //      	  if (a.isCarbon() && hasExoDoubleBond) continue;
        //
        //      	  if (a.isCarbon()) nelectrons++;
        //      	  else
        //      	  {
        //      	    if (a.isOxygen() || a.isSulfur())
        //      	    {
        //          		v_OS.add(a);
        //          		nelectrons += 2;
        //      	    }
        //      	    else
        //      	    {
        //      	      if (a.isNitrogen())
        //          		{
        //          		  v_N.add(a); //store nitrogens
        //          		  nelectrons++;
        //          		}
        //            }
        //          }
        //      	}
        //
        //        //calculate what the number of electrons should be for aromaticity
        //        int naromatic = 2+4*((int)((double)(rsys.size()-2)*0.25+0.5));
        //
        //        if (nelectrons > naromatic) //try to give one electron back to O or S
        //        {
        //        	for (int m=0; m< v_OS.size();m++)
        //          {
        //            a = (JOEAtom) v_OS.get(m);
        //
        //            if (naromatic == nelectrons) break;
        //        	  if (a.getValence() == 2 && a.getHvyValence() == 2)
        //        	  {
        //        		  nelectrons--;
        //        		  a.setFormalCharge(1);
        //        	  }
        //        	}
        //        }
        //
        //        if (v_N.size()==0) continue; //no nitrogens found in ring
        //
        //        //check for protonated nitrogens
        //        for (int m=0; m< v_N.size();m++)
        //      	{
        //      	  a = (JOEAtom) v_N.get(m);
        //
        //      	  if (naromatic == nelectrons) break;
        //      	  if (a.getValence() == 3 && a.getHvyValence() == 2)
        //      	  {
        //      	    nelectrons++;
        //      	    btmp = (boolean[])_aromNH.get(a.getIdx()); btmp[0] = true;
        //      	  }
        //      	  if (a.getFormalCharge() == -1)
        //      	  {
        //      	    nelectrons++;
        //      	    btmp = (boolean[])_aromNH.get(a.getIdx()); btmp[0] = false;
        //      	  }
        //      	}
        //
        //         //charge up tert nitrogens
        //        for (int m=0; m< v_N.size();m++)
        //      	{
        //      	  a = (JOEAtom) v_N.get(m);
        //        	if (a.getHvyValence() == 3 && a.BOSum() < 5) a.setFormalCharge(1);
        //        }
        //
        //        //try to uncharge nitrogens first
        //        for (int m=0; m< v_N.size();m++)
        //      	{
        //      	  a = (JOEAtom) v_N.get(m);
        //
        //      	  if (a.BOSum() > 4) continue; //skip n=O
        //      	  if (naromatic == nelectrons) break;
        //      	  if (a.getHvyValence() == 3)
        //      	  {
        //      	    nelectrons++;
        //      	    a.setFormalCharge(0);
        //      	  }
        //      	}
        //
        //        if (naromatic == nelectrons) continue;
        //
        //        //try to protonate amides next
        //        for (int m=0; m< v_N.size();m++)
        //      	{
        //      	  a = (JOEAtom) v_N.get(m);
        //
        //      	  if (naromatic == nelectrons) break;
        //
        //    	    btmp = (boolean[])_aromNH.get(a.getIdx());
        //      	  if (a.isAmideNitrogen() && a.getValence() == 2 &&
        //      	      a.getHvyValence() == 2 && !btmp[0])
        //      	  {
        //      	    nelectrons++;
        //      	    btmp[0] = true;
        //      	  }
        //      	}
        //
        //        if (naromatic == nelectrons) continue;
        //
        //        //protonate amines in 5 membered rings first - try to match kekule
        //        for (int m=0; m< v_N.size();m++)
        //      	{
        //      	  a = (JOEAtom) v_N.get(m);
        //
        //      	  if (naromatic == nelectrons) break;
        //
        //    	    btmp = (boolean[])_aromNH.get(a.getIdx());
        //      	  if (a.getValence() == 2 && !btmp[0] &&
        //      	      a.isInRingSize(5) && a.BOSum() == 2)
        //      	  {
        //      	    nelectrons++;
        //      	    btmp[0] = true;
        //      	  }
        //      	}
        //
        //        if (naromatic == nelectrons) continue;
        //
        //        //protonate amines in 5 membered rings first - no kekule restriction
        //        for (int m=0; m< v_N.size();m++)
        //      	{
        //      	  a = (JOEAtom) v_N.get(m);
        //
        //      	  if (naromatic == nelectrons) break;
        //
        //    	    btmp = (boolean[])_aromNH.get(a.getIdx());
        //      	  if (a.getValence() == 2 && !btmp[0] &&
        //      	      a.isInRingSize(5))
        //      	  {
        //      	    nelectrons++;
        //      	    btmp[0] = true;
        //      	  }
        //      	}
        //
        //        if (naromatic == nelectrons) continue;
        //
        //        //then others -- try to find an atom w/o a double bond first
        //        for (int m=0; m< v_N.size();m++)
        //      	{
        //      	  a = (JOEAtom) v_N.get(m);
        //
        //      	  if (naromatic == nelectrons) break;
        //
        //    	    btmp = (boolean[])_aromNH.get(a.getIdx());
        //      	  if (a.getHvyValence() == 2 && !btmp[0] &&
        //      	      !a.hasDoubleBond())
        //      	  {
        //      	    nelectrons++;
        //      	    btmp[0] = true;
        //      	  }
        //      	}
        //
        //        for (int m=0; m< v_N.size();m++)
        //      	{
        //      	  a = (JOEAtom) v_N.get(m);
        //
        //      	  if (naromatic == nelectrons) break;
        //
        //    	    btmp = (boolean[])_aromNH.get(a.getIdx());
        //      	  if (a.getHvyValence() == 2 && !btmp[0])
        //      	  {
        //      	    nelectrons++;
        //      	    btmp[0] = true;
        //      	  }
        //      	}
        //      }
        //    }
        //
        //    ait1.reset();
        //    while(ait1.hasNext())
        //    {
        //      atom = ait1.nextAtom();
        // 	    btmp = (boolean[])_aromNH.get(atom.getIdx());
        //      if (btmp[0] && atom.getValence() == 2) atom.setImplicitValence(3);
        //    }
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static synchronized JOEAtomTyper instance()
    {
        if (atomtyper == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Getting " + JOEAtomTyper.class.getName() +
                    " instance.");
            }

            atomtyper = new JOEAtomTyper();
        }

        return atomtyper;
    }

    /**
     * Release date for this expert system (hard coded).
     *
     * @return Release date for this expert system (hard coded).
     */
    public String getReleaseDate()
    {
        return releaseDate;
    }

    /**
     * Release version for this expert system (hard coded).
     *
     * @return Release version for this expert system (hard coded).
     */
    public String getReleaseVersion()
    {
        return releaseVersion;
    }

    /**
     * Vendor for this expert system (hard coded).
     *
     * @return Vendor for this expert system (hard coded).
     */
    public String getVendor()
    {
        return vendor;
    }

    /**
     *  Description of the Method
     *
     * @param  buffer  Description of the Parameter
     */
    public void parseLine(String buffer)
    {
        Vector vs = new Vector();

        // of type String
        JOESmartsPattern sp;

        if (buffer.indexOf("INTHYB") == 0)
        {
            JHM.instance().tokenize(vs, buffer);

            if ((vs.size() == 0) || (vs.size() < 3))
            {
                return;
            }

            sp = new JOESmartsPattern();

            if (sp.init((String) vs.get(1)))
            {
                hybridisation.add(new SMARTSPatternInt(sp,
                        Integer.parseInt((String) vs.get(2))));
            }
            else
            {
                sp = null;
            }
        }
        else if (buffer.indexOf("IMPVAL") == 0)
        {
            JHM.instance().tokenize(vs, buffer);

            if ((vs.size() == 0) || (vs.size() < 3))
            {
                return;
            }

            sp = new JOESmartsPattern();

            if (sp.init((String) vs.get(1)))
            {
                impliciteValence.add(new SMARTSPatternInt(sp,
                        Integer.parseInt((String) vs.get(2))));
            }
            else
            {
                sp = null;
            }
        }
        else if (buffer.indexOf("EXTTYP") == 0)
        {
            JHM.instance().tokenize(vs, buffer);

            if ((vs.size() == 0) || (vs.size() < 3))
            {
                return;
            }

            sp = new JOESmartsPattern();

            if (sp.init((String) vs.get(1)))
            {
                externalTypeRule.add(new SMARTSPatternString(sp,
                        (String) vs.get(2)));
            }
            else
            {
                sp = null;
            }
        }
    }

    /**
     *  Description of the Method
     */
    protected void finalize()
    {
        for (int i = 0; i < hybridisation.size(); i++)
        {
            ((SMARTSPatternInt) hybridisation.get(i)).sp = null;
        }

        for (int i = 0; i < impliciteValence.size(); i++)
        {
            ((SMARTSPatternInt) impliciteValence.get(i)).sp = null;
        }

        for (int i = 0; i < externalTypeRule.size(); i++)
        {
            ((SMARTSPatternString) externalTypeRule.get(i)).sp = null;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
