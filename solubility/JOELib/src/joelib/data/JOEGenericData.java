///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEGenericData.java,v $
//  Purpose:  Generic data.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.14 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;


/**
 * Base class for generic data elements.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.14 $, $Date: 2004/03/15 23:16:13 $
 */
public class JOEGenericData implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    //attribute tag

    /**
     *  Description of the Field
     */
    protected JOEDataType dataType;
    private String attribute;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEGenericData object
     */
    public JOEGenericData()
    {
        dataType = JOEDataType.JOE_UNDEFINED_DATA;

        //attribute = "undefined";
    }

    /**
     *  Constructor for the JOEGenericData object
     *
     * @param  src  Description of the Parameter
     */
    public JOEGenericData(final JOEGenericData src)
    {
        dataType = src.getDataType();
        attribute = src.getAttribute();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the attribute attribute of the JOEGenericData object
     *
     * @param  v  The new attribute value
     */
    public void setAttribute(String v)
    {
        attribute = v;
    }

    /**
     *  Gets the attribute attribute of the JOEGenericData object
     *
     * @return    The attribute value
     */
    public String getAttribute()
    {
        if (attribute != null)
        {
            return attribute;
        }
        else
        {
            return dataType.getDefaultAttr();
        }
    }

    /**
     *  Gets the dataType attribute of the JOEGenericData object
     *
     * @return    The dataType value
     */
    public final JOEDataType getDataType()
    {
        return (dataType);
    }

    public boolean equals(JOEGenericData is)
    {
        //System.out.println(is.toString()+"=GGG="+getAttribute());
        if (is instanceof JOEGenericData && (is != null))
        {
            if (is.getDataType().equals(dataType))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }

    public String toString()
    {
        return getAttribute();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
