///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEDataType.java,v $
//  Purpose:  Data type.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.15 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import java.util.Iterator;
import java.util.LinkedList;


/**
 * Data type.
 *
 * There exists a lot of default data types which where defined in
 * {@link JOEDataType}. These data types are used for caching ring
 * searches and storing special data types like comments or virtual bonds.
 * Furthermore there exist the most important data type {@link JOEPairData}
 * for storing descriptor values.
 *
 * <p>
 * There exists an important difference
 * between {@link JOEDataType#JOE_PAIR_DATA} and all other data types.
 * Because there are many descriptor values possible for one molecule, they are hashed to
 * enable a fast access to these values. This causes that every descriptor
 * value must have a unique attribute name. It's not possible to store two
 * {@link JOEPairData} data elements with the same attribute name
 * ({@link JOEGenericData#setAttribute(String)}).<br>
 * All other {@link JOEDataType} can occur multiple times, even if they
 * have the same attribute name. It's e.g. possible to have multiple comments
 * ({@link JOECommentData}) entries for one molecule.
 *
 *
 * <p>
 * <TABLE BORDER="1"><THEAD>
 * <TR>
 *   <TH WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">Data type</TH>
 *   <TH WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">Data name</TH>
 *   <TH WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">Allowed occurence</TH>
 * </TR>
 * </THEAD>
 * <TBODY>
 *   <TR>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">{@link JOEDataType#JOE_UNDEFINED_DATA}</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">Undefined</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">multiple</TD>
 *   </TR>
 *   <TR>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">{@link JOEDataType#JOE_VIRTUAL_BOND_DATA}</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">VirtualBondData</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">multiple</TD>
 *   </TR>
 *   <TR>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">{@link JOEDataType#JOE_ROTAMER_LIST}</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">RotamerList</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">multiple</TD>
 *   </TR>
 *   <TR>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">{@link JOEDataType#JOE_EXTERNAL_BOND_DATA}</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">ExternalBondData</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">multiple</TD>
 *   </TR>
 *   <TR>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">{@link JOEDataType#JOE_COMPRESS_DATA}</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">CompressData</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">multiple</TD>
 *   </TR>
 *   <TR>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">{@link JOEDataType#JOE_COMMENT_DATA}</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">Comment</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">multiple</TD>
 *   </TR>
 *   <TR>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">{@link JOEDataType#JOE_ENERGY_DATA}</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">EnergyData</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">multiple</TD>
 *   </TR>
 *   <TR>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">{@link JOEDataType#JOE_PAIR_DATA}</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">PairData</TD>
 *     <TD WIDTH="33%" ALIGN="LEFT" VALIGN="TOP">single attribute name ({@link JOEGenericData#setAttribute(String)})</TD>
 *   </TR>
 * </TBODY>
 * </TABLE>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.15 $, $Date: 2004/07/25 20:42:58 $
 */
public final class JOEDataType implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private final static LinkedList usedTypes = new LinkedList();

    /**
     *  Description of the Field
     */
    public final static JOEDataType JOE_UNDEFINED_DATA = new JOEDataType(0,
            "Undefined");

    /**
     * Pair data (descriptor data).
     *
     * @see joelib.data.JOEPairData
     */
    public final static JOEDataType JOE_PAIR_DATA = new JOEDataType(1,
            "PairData");

    /**
     *  Description of the Field
     */
    public final static JOEDataType JOE_ENERGY_DATA = new JOEDataType(2,
            "EnergyData");

    /**
     * Comment data.
     *
     * @see joelib.data.JOECommentData
     */
    public final static JOEDataType JOE_COMMENT_DATA = new JOEDataType(3,
            "Comment");

    /**
     *  Description of the Field
     */
    public final static JOEDataType JOE_COMPRESS_DATA = new JOEDataType(4,
            "CompressData");

    /**
     *  Description of the Field
     */
    public final static JOEDataType JOE_EXTERNAL_BOND_DATA = new JOEDataType(5,
            "ExternalBondData");

    /**
     *  Description of the Field
     */
    public final static JOEDataType JOE_ROTAMER_LIST = new JOEDataType(6,
            "RotamerList");

    /**
     *  Description of the Field
     */
    public final static JOEDataType JOE_VIRTUAL_BOND_DATA = new JOEDataType(7,
            "VirtualBondData");

    /**
     * Ring data.
     *
     * @see joelib.ring.JOERingData
     */
    public final static JOEDataType JOE_RING_DATA = new JOEDataType(8,
            "RingData");

    /**
     *  Description of the Field
     */
    public final static JOEDataType JOE_SPECTRA_DATA = new JOEDataType(9,
            "SpectraData");

    /**
     * Rgroup data.
     *
     * @see joelib.data.JOERgroupData
     */
    public final static JOEDataType JOE_RGROUP_DATA = new JOEDataType(10,
            "RgroupData");

    //~ Instance fields ////////////////////////////////////////////////////////

    private String defaultAttribut;

    /**
     *  Description of the Field
     */

    //  private static  int          minType                 = 0;

    /**
     *  Description of the Field
     */

    //  private static  int          maxType                 = 10;
    //  private final static  LinkedList   freeNumbers             = new LinkedList();
    private int value;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEDataType object
     *
     * @param  _value   Description of the Parameter
     * @param  defAttr  Description of the Parameter
     */
    private JOEDataType(int _value, String _defaultAttr)
    {
        value = _value;
        defaultAttribut = _defaultAttr;

        usedTypes.add(this);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    //  public static JOEDataType getNewDataType(String _defaultAttr)
    //  {
    //    // return type, if it already exists
    //    if(usedTypes.contains(_defaultAttr))
    //    {
    //      return (JOEDataType)usedTypes.get(usedTypes.indexOf(_defaultAttr));
    //    }
    //
    //    // create new type
    //    int newNumber;
    //    if( freeNumbers.size()!=0 ) newNumber = ((Integer)freeNumbers.removeFirst()).intValue();
    //    else
    //    {
    //      newNumber = ++maxType;
    //    }
    //
    //    return new JOEDataType( newNumber, _defaultAttr );
    //  }
    //
    //  public static boolean releaseDataType(JOEDataType dataType)
    //  {
    //    if(dataType.equals(JOE_UNDEFINED_DATA) ||
    //       dataType.equals(JOE_PAIR_DATA) ||
    //       dataType.equals(JOE_ENERGY_DATA) ||
    //       dataType.equals(JOE_COMMENT_DATA) ||
    //       dataType.equals(JOE_COMPRESS_DATA) ||
    //       dataType.equals(JOE_EXTERNAL_BOND_DATA) ||
    //       dataType.equals(JOE_ROTAMER_LIST) ||
    //       dataType.equals(JOE_VIRTUAL_BOND_DATA) ||
    //       dataType.equals(JOE_RING_DATA) ) return false;
    //
    //    Integer oldNumber = new Integer(dataType.value);
    //    freeNumbers.add(oldNumber);
    //
    //    return usedTypes.remove(dataType);
    //  }
    public static Iterator getAllTypes()
    {
        return usedTypes.iterator();
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/

    /**
     *  Gets the defaultAttr attribute of the JOEDataType object
     *
     * @return    The defaultAttr value
     */
    public String getDefaultAttr()
    {
        return defaultAttribut;
    }

    /**
     *  Description of the Method
     *
     * @param  is  Description of the Parameter
     * @return     Description of the Return Value
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof JOEDataType && (obj != null))
        {
            JOEDataType is = (JOEDataType) obj;

            //System.out.println(is.value+"=="+value);
            if (is.value == value)
            {
                return true;
            }
        }

        if (obj instanceof String && (obj != null))
        {
            String is = (String) obj;

            if (is.equals(defaultAttribut))
            {
                return true;
            }
        }

        return false;
    }

    public String toString()
    {
        return defaultAttribut;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
