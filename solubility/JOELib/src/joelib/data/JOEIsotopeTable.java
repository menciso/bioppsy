///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEIsotopeTable.java,v $
//  Purpose:  Isotope element table.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.PropertyHolder;

import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.util.JHM;
import joelib.util.types.DoubleInt;


/**
 * Isotope element table.
 * The definition file can be defined in the
 * <tt>joelib.data.JOEIsotopeTable.resourceFile</tt> property in the {@link wsi.ra.tool.PropertyHolder}.
 * The {@link wsi.ra.tool.ResourceLoader} loads the <tt>joelib.properties</tt> file for default.
 *
 * <p>
 * Default:<br>
 * joelib.data.JOEIsotopeTable.resourceFile=<a href="http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/joelib/joelib/src/joelib/data/plain/isotope.txt?rev=HEAD&content-type=text/vnd.viewcvs-markup">joelib/data/plain/isotope.txt</a>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/03/15 23:16:13 $
 */
public class JOEIsotopeTable extends JOEGlobalDataBase
    implements HardCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.data.JOEIsotopeTable");
    private static JOEIsotopeTable isotopeTable;
    protected static final String DEFAULT_RESOURCE = "joelib/data/plain/isotope.txt";
    private static final String VENDOR = "http://joelib.sf.net";
    private static final String RELEASE_VERSION = "$Revision: 1.9 $";
    private static final String RELEASE_DATE = "$Date: 2004/03/15 23:16:13 $";

    //~ Instance fields ////////////////////////////////////////////////////////

    private String releaseDate;
    private String releaseVersion;
    private String vendor;

    /**
     *  {@link java.util.Vector} of type {@link joelib.util.types.DoubleInt} to store isotope number and mass.
     */
    private Vector _isotopes;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOETypeTable object
     */
    private JOEIsotopeTable()
    {
        initialized = false;

        Properties prop = PropertyHolder.instance().getProperties();
        resourceFile = prop.getProperty(this.getClass().getName() +
                ".resourceFile", DEFAULT_RESOURCE);

        _isotopes = new Vector();

        vendor = VENDOR;
        releaseVersion = JOEKernel.transformCVStag(RELEASE_VERSION);
        releaseDate = JOEKernel.transformCVStag(RELEASE_DATE);

        JOEKernel.instance().addHardCodedKernel(this);
        init();
        JOEKernel.instance().addSoftCodedKernel(this);

        logger.info("Using isotope table: " + resourceFile);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static synchronized JOEIsotopeTable instance()
    {
        if (isotopeTable == null)
        {
            isotopeTable = new JOEIsotopeTable();
        }

        return isotopeTable;
    }

    /**
     * Return the exact masss of the isotope.
     * (or by default, the most abundant isotope)
     */
    public double getExactMass(int atomicNum)
    {
        return getExactMass(atomicNum, 0);
    }

    /**
     * Return the exact masss of the isotope.
     * (or by default, the most abundant isotope)
     */
    public double getExactMass(int atomicNum, int isotope)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicNum > _isotopes.size()) || (atomicNum < 0))
        {
            return 0.0;
        }

        int iso;
        Vector elemVector = (Vector) _isotopes.get(atomicNum);
        DoubleInt entry;

        for (iso = 0; iso < elemVector.size(); iso++)
        {
            entry = (DoubleInt) elemVector.get(iso);

            if (isotope == entry.i)
            {
                return entry.d;
            }
        }

        return 0.0;
    }

    /**
     * Release date for this expert system (hard coded).
     *
     * @return Release date for this expert system (hard coded).
     */
    public String getReleaseDate()
    {
        return releaseDate;
    }

    /**
     * Release version for this expert system (hard coded).
     *
     * @return Release version for this expert system (hard coded).
     */
    public String getReleaseVersion()
    {
        return releaseVersion;
    }

    /**
     * Vendor for this expert system (hard coded).
     *
     * @return Vendor for this expert system (hard coded).
     */
    public String getVendor()
    {
        return vendor;
    }

    /**
     *  Description of the Method
     *
     * @param  buffer  Description of the Parameter
     */
    public void parseLine(String buffer)
    {
        // of type String
        Vector vs = new Vector();

        //int atomicNum;
        int i;

        // of type DoubleInt
        Vector row = new Vector();
        DoubleInt di;

        // skip comment line (at the top)
        if (!buffer.trim().equals("") && (buffer.charAt(0) != '#'))
        {
            JHM.tokenize(vs, buffer);

            if (vs.size() > 3) // atomic number, 0, most abundant mass (...)
            {
                //atomicNum = Integer.parseInt((String) vs.get(0));
                for (i = 1; i < (vs.size() - 1); i += 2) // make sure i+1 still exists
                {
                    di = new DoubleInt();
                    di.i = Integer.parseInt((String) vs.get(i));
                    di.d = Double.parseDouble((String) vs.get(i + 1));
                    row.add(di);
                }

                _isotopes.add(row);
            }
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
