///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEGlobalDataBase.java,v $
//  Purpose:  Base class for handling JOELib data.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import java.util.Properties;

import org.apache.log4j.Category;


/**
 * Base class for handling JOELib data.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:42:58 $
 */
public abstract class JOEGlobalDataBase extends SoftCodedKernelBase
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.data.JOEGlobalDataBase");

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEGlobalDataBase object
     */
    public JOEGlobalDataBase()
    {
        this(null);
    }

    /**
     *  Constructor for the JOEGlobalDataBase object
     *
     * @param  _prop  Description of the Parameter
     */
    public JOEGlobalDataBase(Properties _prop)
    {
        initialized = false;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @param  buffer  Description of the Parameter
     */
    public abstract void parseLine(String buffer);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
