///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SoftCodedKernel.java,v $
//  Purpose:  Descriptor base class.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;


/**
 * Interface for defining a hard coded kernel part.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2004/03/15 23:16:13 $
 */
public interface SoftCodedKernel
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Release date of external (soft coded) data for this expert system.
     *
     * @return Release date of external (soft coded) data for this expert system.
     */
    public String getReleaseDateExternal();

    /**
     * Release version of external (soft coded) data for this expert system.
     *
     * @return Release version of external (soft coded) data for this expert system.
     */
    public String getReleaseVersionExternal();

    /**
     * Resource for this soft coded expert system.
     *
     * @return Resource for this soft coded expert system.
     */
    public String getResourceExternal();

    /**
     * Vendor of external (soft coded) data for this expert system.
     *
     * @return Vendor of external (soft coded) data for this expert system.
     */
    public String getVendorExternal();
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
