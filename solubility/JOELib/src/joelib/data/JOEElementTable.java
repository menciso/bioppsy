///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEElementTable.java,v $
//  Purpose:  Element table.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.27 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.PropertyHolder;

import java.awt.Color;

import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEElement;

import joelib.util.JHM;


/**
 * Element table.
 * The definition file can be defined in the
 * <tt>joelib.data.JOEElementTable.resourceFile</tt> property in the {@link wsi.ra.tool.PropertyHolder}.
 * The {@link wsi.ra.tool.ResourceLoader} loads the <tt>joelib.properties</tt> file for default.
 *
 * <p>
 * Default:<br>
 * joelib.data.JOEElementTable.resourceFile=<a href="http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/joelib/joelib/src/joelib/data/plain/element.txt?rev=HEAD&content-type=text/vnd.viewcvs-markup">joelib/data/plain/element.txt</a>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.27 $, $Date: 2004/07/25 20:42:58 $
 * @see wsi.ra.tool.PropertyHolder
 * @see wsi.ra.tool.ResourceLoader
 */
public class JOEElementTable extends JOEGlobalDataBase
    implements HardCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.data.JOEElementTable");
    private final static String DEFAULT_RESOURCE = "joelib/data/plain/element.txt";
    private static JOEElementTable etab;
    private static final String VENDOR = "http://joelib.sf.net";
    private static final String RELEASE_VERSION = "$Revision: 1.27 $";
    private static final String RELEASE_DATE = "$Date: 2004/07/25 20:42:58 $";

    //~ Instance fields ////////////////////////////////////////////////////////

    private String releaseDate;
    private String releaseVersion;
    private String vendor;

    /**
     *  of type <tt>JOEElement</tt>
     */
    private Vector _element;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEElementTable object
     */
    private JOEElementTable()
    {
        initialized = false;

        Properties prop = PropertyHolder.instance().getProperties();
        resourceFile = prop.getProperty(this.getClass().getName() +
                ".resourceFile", DEFAULT_RESOURCE);

        //    resourceFile = "element.txt";
        //    _subdir = "joelib/data/plain";
        //    //_dataptr  = ElementData;
        _element = new Vector();

        vendor = VENDOR;
        releaseVersion = JOEKernel.transformCVStag(RELEASE_VERSION);
        releaseDate = JOEKernel.transformCVStag(RELEASE_DATE);

        JOEKernel.instance().addHardCodedKernel(this);
        init();
        JOEKernel.instance().addSoftCodedKernel(this);

        logger.info("Using element table: " + resourceFile);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the atom electronegativity after Allred and Rochow.
     *
     * @return   The allredRochowEN value
     */
    public double getAllredRochowEN(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0;
        }

        return (((JOEElement) _element.get(atomicnum)).getAllredRochowEN());
    }

    /**
     *  Gets the bORad attribute of the JOEElementTable object
     *
     * @param atomicnum  Description of the Parameter
     * @return           The bORad value
     */
    public double getBORad(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0.0f;
        }

        return (((JOEElement) _element.get(atomicnum)).getBoRad());
    }

    /**
     * Gets the color attribute of the JOEElementTable object
     *
     * @param atomicnum  Description of the Parameter
     * @return           The color value
     */
    public Color getColor(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return (((JOEElement) _element.get(0)).getColor());
        }

        return (((JOEElement) _element.get(atomicnum)).getColor());
    }

    /**
     *  Gets the covalentRad attribute of the JOEElementTable object
     *
     * @param atomicnum  Description of the Parameter
     * @return           The covalentRad value
     */
    public double getCovalentRad(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0.0f;
        }

        return (((JOEElement) _element.get(atomicnum)).getCovalentRad());
    }

    /**
     * Gets the electronAffinity attribute of the JOEElement object
     *
     * @return   The electronAffinity value
     */
    public double getElectronAffinity(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0;
        }

        return (((JOEElement) _element.get(atomicnum)).getElectronAffinity());
    }

    /**
     * Gets the exteriorElectrons attribute of the JOEElementTable object
     *
     * @param atomicnum  Description of the Parameter
     * @return           The exteriorElectrons value
     */
    public int getExteriorElectrons(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0;
        }

        return (((JOEElement) _element.get(atomicnum)).getExteriorElectrons());
    }

    /**
     *  Gets the mass attribute of the JOEElementTable object
     *
     * @param atomicnum  Description of the Parameter
     * @return           The mass value
     */
    public double getMass(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0.0f;
        }

        return (((JOEElement) _element.get(atomicnum)).getMass());
    }

    /**
     *  Gets the maxBonds attribute of the JOEElementTable object
     *
     * @param atomicnum  Description of the Parameter
     * @return           The maxBonds value
     */
    public int getMaxBonds(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0;
        }

        return (((JOEElement) _element.get(atomicnum)).getMaxBonds());
    }

    /**
     * Gets the atom electronegativity after Pauling.
     *
     * @return   The paulingEN value
     */
    public double getPaulingEN(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0;
        }

        return (((JOEElement) _element.get(atomicnum)).getPaulingEN());
    }

    /**
     * Gets the period of the JOEElement object.
     *
     * @return   The electronAffinity value
     */
    public int getPeriod(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0;
        }

        return (((JOEElement) _element.get(atomicnum)).getPeriod());
    }

    public double getSandersonEN(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0;
        }

        return (((JOEElement) _element.get(atomicnum)).getSandersonEN());
    }

    /**
     *  Gets the vdwRad attribute of the JOEElementTable object
     *
     * @param atomicnum  Description of the Parameter
     * @return           The vdwRad value
     */
    public double getVdwRad(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 0.0f;
        }

        return (((JOEElement) _element.get(atomicnum)).getVdwRad());
    }

    /**
     *  Description of the Method
     *
     * @param atomicnum  Description of the Parameter
     * @return           Description of the Return Value
     */
    public double correctedBondRad(int atomicnum)
    {
        return correctedBondRad(atomicnum, 3);
    }

    /**
     *  Description of the Method
     *
     * @param atomicnum  Description of the Parameter
     * @param hyb        Description of the Parameter
     * @return           Description of the Return Value
     */
    public double correctedBondRad(int atomicnum, int hyb)
    {
        // atomic #, hybridization
        double rad;

        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 1.0f;
        }

        rad = ((JOEElement) _element.get(atomicnum)).getBoRad();

        if (hyb == 2)
        {
            rad *= 0.95f;
        }
        else if (hyb == 1)
        {
            rad *= 0.90f;
        }

        return rad;
    }

    /**
     *  Description of the Method
     *
     * @param atomicnum  Description of the Parameter
     * @return           Description of the Return Value
     */
    public double correctedVdwRad(int atomicnum)
    {
        return correctedVdwRad(atomicnum, 3);
    }

    /**
     *  Description of the Method
     *
     * @param atomicnum  Description of the Parameter
     * @param hyb        Description of the Parameter
     * @return           Description of the Return Value
     */
    public double correctedVdwRad(int atomicnum, int hyb)
    {
        // atomic #, hybridization
        double rad;

        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return 1.95f;
        }

        rad = ((JOEElement) _element.get(atomicnum)).getVdwRad();

        if (hyb == 2)
        {
            rad *= 0.95f;
        }
        else if (hyb == 1)
        {
            rad *= 0.90f;
        }

        return rad;
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
        //for (int i=0; i< _element.size();i++) _element.get(i) = null;
        _element.clear();
        _element = null;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public static synchronized JOEElementTable instance()
    {
        if (etab == null)
        {
            etab = new JOEElementTable();
        }

        return etab;
    }

    /**
     *  Gets the atomicNum attribute of the JOEElementTable object
     *
     * @param sym  Description of the Parameter
     * @return     The atomicNum value
     */
    public int getAtomicNum(final String sym)
    {
        if (!initialized)
        {
            init();
        }

        for (int i = 0; i < _element.size(); i++)
        {
            JOEElement elem = (JOEElement) _element.get(i);

            //      System.out.print("symtab "+elem.getSymbol()+" sym "+sym);
            if (sym.equals(elem.getSymbol()))
            {
                //        System.out.println(" "+elem.getAtomicNum());
                return elem.getAtomicNum();
            }

            //      System.out.println();
        }

        //    System.out.println("0");
        return 0;
    }

    /**
     * Release date for this expert system (hard coded).
     *
     * @return Release date for this expert system (hard coded).
     */
    public String getReleaseDate()
    {
        return releaseDate;
    }

    /**
     * Release version for this expert system (hard coded).
     *
     * @return Release version for this expert system (hard coded).
     */
    public String getReleaseVersion()
    {
        return releaseVersion;
    }

    /**
     *  Gets the symbol attribute of the JOEElementTable object
     *
     * @param atomicnum  Description of the Parameter
     * @return           The symbol value
     */
    public String getSymbol(int atomicnum)
    {
        if (!initialized)
        {
            init();
        }

        if ((atomicnum < 0) || (atomicnum > _element.size()))
        {
            return ("");
        }

        //System.out.println(""+atomicnum+" symbol "+((JOEElement)_element.get(atomicnum)).getSymbol());
        return (((JOEElement) _element.get(atomicnum)).getSymbol());
    }

    /**
     * Vendor for this expert system (hard coded).
     *
     * @return Vendor for this expert system (hard coded).
     */
    public String getVendor()
    {
        return vendor;
    }

    /**
     *  Description of the Method
     *
     * @param buffer  Description of the Parameter
     */
    public void parseLine(String buffer)
    {
        int num;
        int maxbonds;
        String symbol;
        double rBO;
        double rCov;
        double rVdw;
        double mass;
        float r;
        float g;
        float b;
        String exteriorElectrons;
        byte period;
        byte group;
        double enAllredRochow;
        double enPauling;
        double enSanderson;
        double eAffinity;

        // skip comment line (at the top)
        if (!buffer.trim().equals("") && (buffer.charAt(0) != '#'))
        {
            Vector vs = new Vector();

            // of type String
            JHM.instance().tokenize(vs, buffer);

            String tmp;

            try
            {
                // Ignore RGB columns
                num = Integer.parseInt((String) vs.get(0));
                symbol = (String) vs.get(1);
                rCov = Float.parseFloat((String) vs.get(2));
                rBO = Float.parseFloat((String) vs.get(4));
                rVdw = Float.parseFloat((String) vs.get(5));
                maxbonds = Integer.parseInt((String) vs.get(6));
                r = Float.parseFloat((String) vs.get(7));
                g = Float.parseFloat((String) vs.get(8));
                b = Float.parseFloat((String) vs.get(9));
                mass = Float.parseFloat((String) vs.get(10));
                exteriorElectrons = (String) vs.get(11);
                period = Byte.parseByte((String) vs.get(12));
                tmp = (String) vs.get(13);

                if (tmp.indexOf("Lanthanoids") != -1)
                {
                    group = 50;
                }
                else if (tmp.indexOf("Actinoids") != -1)
                {
                    group = 60;
                }
                else
                {
                    group = Byte.parseByte(tmp);
                }

                enAllredRochow = Float.parseFloat((String) vs.get(14));
                enPauling = Float.parseFloat((String) vs.get(15));
                enSanderson = Float.parseFloat((String) vs.get(16));
                eAffinity = Float.parseFloat((String) vs.get(17));

                //        System.out.println("Affinity: "+eAffinity);
            }
             catch (NumberFormatException ex)
            {
                logger.error("Error in line: " + buffer);
                logger.error(ex.getMessage());
                logger.error(ex.toString());

                return;
            }

            JOEElement ele = new JOEElement(num, symbol, rCov, rBO, rVdw,
                    maxbonds, new Color(r, g, b), mass, exteriorElectrons,
                    period, group, enAllredRochow, enPauling, enSanderson,
                    eAffinity);
            _element.add(ele);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
