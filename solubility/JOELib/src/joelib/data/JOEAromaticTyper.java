///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEAromaticTyper.java,v $
//  Purpose:  Aromatic typer.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.30 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.PropertyHolder;

import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;
import org.apache.log4j.Priority;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.ring.JOERing;

import joelib.smarts.JOESmartsPattern;

import joelib.util.JHM;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;
import joelib.util.iterator.NbrAtomIterator;
import joelib.util.types.IntInt;


/**
 *  Aromatic typer.
 * The definition file can be defined in the
 * <tt>joelib.data.JOEAromaticTyper.resourceFile</tt> property in the {@link wsi.ra.tool.PropertyHolder}.
 * The {@link wsi.ra.tool.ResourceLoader} loads the <tt>joelib.properties</tt> file for default.
 *
 * <p>
 * Default:<br>
 * joelib.data.JOEAromaticTyper.resourceFile=<a href="http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/joelib/joelib/src/joelib/data/plain/aromatic.txt?rev=HEAD&content-type=text/vnd.viewcvs-markup">joelib/data/plain/aromatic.txt</a>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.30 $, $Date: 2004/07/25 20:42:58 $
 * @see wsi.ra.tool.PropertyHolder
 * @see wsi.ra.tool.ResourceLoader
 */
public class JOEAromaticTyper extends JOEGlobalDataBase
    implements HardCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.data.JOEAromaticTyper");
    private static JOEAromaticTyper aromtyper;
    private final static String DEFAULT_RESOURCE = "joelib/data/plain/aromatic.txt";
    private final static boolean DEFAULT_AVOID_INNER_RING_FLAG = true;
    private static final String VENDOR = "http://joelib.sf.net";
    private static final String RELEASE_VERSION = "$Revision: 1.30 $";
    private static final String RELEASE_DATE = "$Date: 2004/07/25 20:42:58 $";

    //~ Instance fields ////////////////////////////////////////////////////////

    private String releaseDate;
    private String releaseVersion;
    private String vendor;

    /**
     * SMARTS mapping list.
     *  of type <tt>int[1]</tt> -{@link java.util.Vector}
     */
    private Vector matchList;

    /**
     * Min and max number of electrons.
     *  of type <tt>IntInt</tt>
     */
    private Vector minMaxElectrons;

    /**
     * SMARTS of potentially aromatic atoms.
     *  of type <tt>JOESmartsPattern</tt>
     */
    private Vector smarts;
    private boolean[] _root;

    //potentially aromatic atoms
    private boolean[] _visit;
    private IntInt[] numberOfElectrons;

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------   */

    /**
     * Potentially aromatic atoms.
     */
    private boolean[] potentiallyAromatic;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initializes the aromatic typer factory class.
     */
    private JOEAromaticTyper()
    {
        initialized = false;

        Properties prop = PropertyHolder.instance().getProperties();
        resourceFile = prop.getProperty(this.getClass().getName() +
                ".resourceFile", DEFAULT_RESOURCE);

        minMaxElectrons = new Vector();
        potentiallyAromatic = null;
        _visit = null;
        _root = null;
        numberOfElectrons = null;

        matchList = new Vector();
        smarts = new Vector();

        vendor = VENDOR;
        releaseVersion = JOEKernel.transformCVStag(RELEASE_VERSION);
        releaseDate = JOEKernel.transformCVStag(RELEASE_DATE);

        JOEKernel.instance().addHardCodedKernel(this);
        init();
        JOEKernel.instance().addSoftCodedKernel(this);

        logger.info("Using aromaticity model: " + resourceFile);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Release date for this expert system (hard coded).
     *
     * @return Release date for this expert system (hard coded).
     */
    public String getReleaseDate()
    {
        return releaseDate;
    }

    /**
     * Release version for this expert system (hard coded).
     *
     * @return Release version for this expert system (hard coded).
     */
    public String getReleaseVersion()
    {
        return releaseVersion;
    }

    /**
     * Vendor for this expert system (hard coded).
     *
     * @return Vendor for this expert system (hard coded).
     */
    public String getVendor()
    {
        return vendor;
    }

    /**
     * Check aromaticity starting from the root atom.
     *
     * @param  atom   the root atom
     * @param  depth  the search depth, e.g. 6 or 20 for typical aromatic systems
     *
     * @see @see #selectRootAtoms(JOEMol, boolean}
     */
    public void checkAromaticity(JOEAtom atom, int depth)
    {
        JOEAtom nbr;
        JOEBond bond;
        IntInt erange = null;

        NbrAtomIterator nait = atom.nbrAtomIterator();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();
            bond = nait.actualBond();

            if (bond.isInRing() && !bond.isAromatic())
            {
                erange = numberOfElectrons[atom.getIdx()];

                if (traverseCycle(atom, nbr, bond, erange, depth - 1))
                {
                    atom.setAromatic();
                    bond.setAromatic();
                }
            }
        }

        //System.out.println("check depth:"+depth+" atom "+atom.getIdx()+" potAr:"+potentiallyAromatic[atom.getIdx()]+" minEl:"+(numberOfElectrons[atom.getIdx()]).i1+" maxEl:"+(numberOfElectrons[atom.getIdx()]).i2+" aromatic? "+atom.isAromatic()+" erange: "+erange);
    }

    /**
     * Remove 3 membered rings from consideration.
     *
     * @param  mol  the molecule
     */
    public void excludeSmallRing(JOEMol mol)
    {
        JOEAtom atom;
        JOEAtom nbr1;
        JOEAtom nbr2;

        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (_root[atom.getIdx()])
            {
                NbrAtomIterator nait1 = atom.nbrAtomIterator();

                while (nait1.hasNext())
                {
                    nbr1 = nait1.nextNbrAtom();

                    if (nait1.actualBond().isInRing() &&
                            potentiallyAromatic[nbr1.getIdx()])
                    {
                        NbrAtomIterator nait2 = nbr1.nbrAtomIterator();

                        while (nait2.hasNext())
                        {
                            nbr2 = nait2.nextNbrAtom();

                            if ((nbr2 != atom) &&
                                    nait2.actualBond().isInRing() &&
                                    potentiallyAromatic[nbr2.getIdx()])
                            {
                                if (atom.isConnected(nbr2))
                                {
                                    _root[atom.getIdx()] = false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //    protected void finalize()
    //    {
    //        //		JOESmartsPattern tmp;
    //        //		for (int i = 0; i < smarts.size(); i++)
    //        //		{
    //        //			tmp = ((JOESmartsPattern) smarts.get(i));
    //        //			tmp = null;
    //        //		}
    //    }

    /**
     * Gets an instance of this aromatic typer factory class.
     *
     * @return    the instance of this aromatic typer factory class
     */
    public static synchronized JOEAromaticTyper instance()
    {
        if (aromtyper == null)
        {
            aromtyper = new JOEAromaticTyper();
        }

        return aromtyper;
    }

    /**
     * Assign the aromaticity flag to atoms and bonds.
     *
     * 3 rings will be excluded.
     * Please remember that the aromaticity typer JOEAromaticTyper.assignAromaticFlags(JOEMol)
     * assign ONLY aromaticity flags and NOT the internal aromatic bond order JOEBond.JOE_AROMATIC_BOND_ORDER.
     *
     * @param  mol  the molecule
     */
    public void assignAromaticFlags(JOEMol mol)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("assignAromaticFlags");
        }

        if (!initialized)
        {
            init();
        }

        if (mol.hasAromaticPerceived())
        {
            return;
        }

        mol.setAromaticPerceived();

        int size = mol.numAtoms() + 1;

        potentiallyAromatic = new boolean[size];
        _root = new boolean[size];
        _visit = new boolean[size];
        numberOfElectrons = new IntInt[size];

        for (int i = 0; i < size; i++)
        {
            numberOfElectrons[i] = new IntInt();
        }

        JOEBond bond;
        JOEAtom atom;

        //unset all aromatic flags
        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atom.unsetAromatic();
        }

        BondIterator bit = mol.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            bond.unsetAromatic();
        }

        if (logger.isDebugEnabled())
        {
            StringBuffer debugBuffer = new StringBuffer();
            debugBuffer.append("unsetAtoms:");
            ait.reset();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();
                debugBuffer.append("a" + atom.getIdx() + "(BO" +
                    atom.getValence() + ") ");
            }

            debugBuffer.append("\nunsetBonds:");
            bit.reset();

            while (bit.hasNext())
            {
                bond = bit.nextBond();
                debugBuffer.append("b" + bond.getIdx() + ":" + bond.getFlags() +
                    "(" + bond.getBeginAtomIdx() + "," + bond.getEndAtomIdx() +
                    ") ");
            }

            logger.debug(debugBuffer.toString());
        }

        int idx;
        int[] itmp;
        int k;

        //mark atoms as potentially aromatic
        for (idx = 0, k = 0; k < smarts.size(); k++, idx++)
        {
            JOESmartsPattern tmpSP = (JOESmartsPattern) smarts.get(k);

            //      System.out.println(tmpSP.getSMARTS()+" idx "+idx);
            //      System.out.println(tmpSP.toString());
            if (tmpSP.match(mol))
            {
                matchList = tmpSP.getMapList();

                //        System.out.print("mlist:");
                for (int m = 0; m < matchList.size(); m++)
                {
                    itmp = (int[]) matchList.get(m);

                    //                          System.out.println("get ("+idx+")"+(itmp[0]));
                    if (logger.isDebugEnabled())
                    {
                        logger.debug("pot. aromatic " + itmp[0] + " " +
                            tmpSP.getSMARTS());
                    }

                    potentiallyAromatic[itmp[0]] = true;

                    IntInt iiMinMaxE = (IntInt) minMaxElectrons.get(idx);
                    IntInt iiElectrons = numberOfElectrons[itmp[0]];
                    iiElectrons.i1 = iiMinMaxE.i1;
                    iiElectrons.i2 = iiMinMaxE.i2;
                }

                //        System.out.println("");
            }
        }

        if (logger.isDebugEnabled())
        {
            StringBuffer debugBuffer = new StringBuffer();
            debugBuffer.append("potentiallyAromatic:");

            for (int i = 0; i < potentiallyAromatic.length; i++)
            {
                debugBuffer.append(" " + potentiallyAromatic[i]);
            }

            logger.debug(debugBuffer.toString());
        }

        //sanity check - exclude all 4 substituted atoms and sp centers
        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (atom.getImplicitValence() > 3)
            {
                potentiallyAromatic[atom.getIdx()] = false;

                if (logger.isDebugEnabled())
                {
                    if (potentiallyAromatic[atom.getIdx()] == true)
                    {
                        logger.debug("Remove potentially aromatic atom " +
                            atom.getIdx() + ", BO>3.");
                    }
                }

                continue;
            }

            switch (atom.getAtomicNum())
            {
            //phosphorus and sulfur may be initially typed as sp3
            case 6:
            case 7:
            case 8:

                if (atom.getHyb() != 2)
                {
                    potentiallyAromatic[atom.getIdx()] = false;

                    if (logger.isDebugEnabled())
                    {
                        if (potentiallyAromatic[atom.getIdx()] == true)
                        {
                            logger.debug("Remove potentially aromatic atom " +
                                atom.getIdx() + ", Hyb!=2.");
                        }
                    }
                }

                break;
            }
        }

        //propagate potentially aromatic atoms
        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (potentiallyAromatic[atom.getIdx()])
            {
                propagatePotentialAromatic(atom);
            }
        }

        selectRootAtoms(mol, DEFAULT_AVOID_INNER_RING_FLAG);

        //remove 3 membered rings from consideration
        excludeSmallRing(mol);

        //loop over root atoms and look for 5-6 membered aromatic rings
        //    _visit.clear(); _visit.ensureCapacity(mol.numAtoms()+1);
        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            // check only for root atoms
            if (_root[atom.getIdx()])
            {
                //System.out.println("Check aromaticity: "+atom.getIdx());
                checkAromaticity(atom, 6);
            }
        }

        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            // check only for root atoms
            if (_root[atom.getIdx()])
            {
                checkAromaticity(atom, 20);
            }
        }

        if (logger.isDebugEnabled())
        {
            StringBuffer debugBuffer = new StringBuffer();
            debugBuffer.append("aromatic-atoms:");

            ait.reset();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                if (atom.isAromatic())
                {
                    debugBuffer.append(" " + atom.getIdx());
                }
            }

            debugBuffer.append("\naromatic-bonds:");
            bit.reset();

            while (bit.hasNext())
            {
                bond = bit.nextBond();

                if (bond.isAromatic())
                {
                    debugBuffer.append(" " + bond.getIdx());
                }
            }

            logger.debug(debugBuffer.toString());
        }
    }

    /**
     * Parses the atom typer definition line to initialize the aromatic typer.
     *
     * @param  buffer  the atom typer definition line
     */
    public void parseLine(String buffer)
    {
        JOESmartsPattern sp;

        //    System.out.println("buffer: "+buffer);
        if (buffer.trim().equals("") || (buffer.charAt(0) == '#'))
        {
            return;
        }

        Vector vs = new Vector();

        // of type String
        JHM.tokenize(vs, buffer);

        if ((vs.size() != 0) && (vs.size() == 3))
        {
            String tmp = (String) vs.get(0);
            sp = new JOESmartsPattern();

            //      System.out.println("apat: "+tmp);
            if (sp.init(tmp))
            {
                smarts.add(sp);

                //minimum and maximum number of electrons
                int i1 = Integer.parseInt((String) vs.get(1));
                int i2 = Integer.parseInt((String) vs.get(2));
                minMaxElectrons.add(new IntInt(i1, i2));
            }
            else
            {
                sp = null;
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  atom  Description of the Parameter
     */
    public void propagatePotentialAromatic(JOEAtom atom)
    {
        int count = 0;

        // count potentially aromatic neighbour atoms of
        // this atoms which are included in a ring
        JOEAtom nbr = null;
        NbrAtomIterator nait = atom.nbrAtomIterator();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();

            if (nait.actualBond().isInRing() &&
                    potentiallyAromatic[nbr.getIdx()])
            {
                count++;
            }
        }

        //System.out.println("atom: "+atom.getIdx()+" counter: "+count);
        if (count < 2)
        {
            potentiallyAromatic[atom.getIdx()] = false;

            if (logger.isDebugEnabled())
            {
                if (potentiallyAromatic[atom.getIdx()] == true)
                {
                    logger.debug("Remove potentially aromatic atom " +
                        atom.getIdx() + ", less than 2 aromatic neighbours.");
                }
            }

            if (count == 1)
            {
                nait.reset();

                while (nait.hasNext())
                {
                    nbr = nait.nextNbrAtom();

                    if (nait.actualBond().isInRing() &&
                            potentiallyAromatic[nbr.getIdx()])
                    {
                        propagatePotentialAromatic(nbr);
                    }
                }
            }
        }
    }

    /*-------------------------------------------------------------------------*
     * private methods
     *-------------------------------------------------------------------------   */

    /**
     * Select the root atoms for traversing atoms in rings.
     *
     * Picking only the begin atom of a closure bond can cause
     * difficulties when the selected atom is an inner atom
     * with three neighbour ring atoms. Why ? Because this atom
     * can get trapped by the other atoms when determining aromaticity,
     * because a simple visited flag is used in the
     * {@link #traverseCycle(JOEAtom, JOEAtom, JOEBond, IntInt, int)}
     *
     * @param mol the molecule
     * @param avoidInnerRingAtoms inner closure ring atoms with more than 2 neighbours will be avoided
     *
     * @see #traverseCycle(JOEAtom, JOEAtom, JOEBond, IntInt, int)
     */
    private void selectRootAtoms(JOEMol mol, boolean avoidInnerRingAtoms)
    {
        BondIterator bit = mol.bondIterator();
        Vector sssRings = mol.getSSSR();
        JOEBond bond;
        bit.reset();

        int rootAtom;
        NbrAtomIterator nait;
        JOEAtom nbrAtom;
        int ringNbrs;
        int heavyNbrs;
        JOERing ring;
        int[] tmp;
        int newRoot = -1;
        Vector tmpRootAtoms = new Vector();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isClosure())
            {
                rootAtom = bond.getBeginAtomIdx();
                tmpRootAtoms.add(new Integer(rootAtom));
            }
        }

        bit.reset();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isClosure())
            {
                // BASIC APPROACH
                // pick begin atom at closure bond
                // this is really greedy, isn't it !;-)
                rootAtom = bond.getBeginAtomIdx();
                _root[rootAtom] = true;

                // EXTENDED APPROACH
                if (avoidInnerRingAtoms)
                {
                    // count the number of neighbour ring atoms
                    nait = mol.getAtom(rootAtom).nbrAtomIterator();
                    ringNbrs = heavyNbrs = 0;

                    while (nait.hasNext())
                    {
                        nbrAtom = nait.nextNbrAtom();

                        if (!nbrAtom.isHydrogen())
                        {
                            heavyNbrs++;

                            if (nbrAtom.isInRing())
                            {
                                ringNbrs++;
                            }
                        }
                    }

                    // if this atom has more than 2 neighbour
                    // ring atoms, we could get trapped later
                    // when traversing the cycles, which
                    // can cause aromaticity false detection
                    newRoot = -1;

                    if (ringNbrs > 2)
                    {
                        //try to find an other root atom
                        for (int i = 0; i < sssRings.size(); i++)
                        {
                            ring = (JOERing) sssRings.get(i);
                            tmp = ring.getAtoms();

                            //System.out.println("ring: "+i+" "+ring);
                            boolean checkThisRing = false;

                            int rootAtomNumber = 0;
                            int idx = 0;

                            // avoiding two root atoms in one ring !
                            for (int j = 0; j < tmpRootAtoms.size(); j++)
                            {
                                idx = ((Integer) tmpRootAtoms.get(j)).intValue();

                                if (ring.isInRing(idx))
                                {
                                    rootAtomNumber++;

                                    if (rootAtomNumber >= 2)
                                    {
                                        break;
                                    }
                                }
                            }

                            if (rootAtomNumber < 2)
                            {
                                for (int j = 0; j < tmp.length; j++)
                                {
                                    // find critical ring
                                    if (tmp[j] == rootAtom)
                                    {
                                        checkThisRing = true;
                                    }
                                    else
                                    {
                                        // second root atom in this ring ?
                                        if (_root[tmp[j]] == true)
                                        {
                                            // when there is a second root
                                            // atom this ring can not be
                                            // used for getting an other
                                            // root atom
                                            checkThisRing = false;

                                            break;
                                        }
                                    }
                                }
                            }

                            // check ring for getting an other
                            // root atom to aromaticity typer avoid
                            // problems
                            if (checkThisRing)
                            {
                                // check if we can find another root
                                // atom
                                for (int m = 0; m < tmp.length; m++)
                                {
                                    //System.out.println("ring "+i+"atom "+mol.getAtom(tmp[m]).getIdx());
                                    nait = mol.getAtom(tmp[m]).nbrAtomIterator();
                                    ringNbrs = heavyNbrs = 0;

                                    while (nait.hasNext())
                                    {
                                        nbrAtom = nait.nextNbrAtom();

                                        if (!nbrAtom.isHydrogen())
                                        {
                                            heavyNbrs++;

                                            if (nbrAtom.isInRing())
                                            {
                                                ringNbrs++;
                                            }
                                        }
                                    }

                                    // if the number of neighboured heavy atoms is also
                                    // the number of neighboured ring atoms, the aromaticity
                                    // typer could be stuck in a local traversing trap
                                    if ((ringNbrs <= 2) &&
                                            ring.isInRing(
                                                mol.getAtom(tmp[m]).getIdx()))
                                    {
                                        newRoot = tmp[m];
                                    }
                                }
                            }
                        }

                        if ((newRoot != -1) && (rootAtom != newRoot))
                        {
                            // unset root atom
                            _root[rootAtom] = false;

                            // pick new root atom
                            _root[newRoot] = true;
                        }
                        else
                        {
                            if (logger.isEnabledFor(Priority.WARN))
                            {
                                logger.warn("Root atom " + rootAtom +
                                    " could cause false aromaticity detection in " +
                                    mol.getTitle());
                            }
                        }
                    }
                }

                if (_root[rootAtom] == true)
                {
                    if (logger.isDebugEnabled())
                    {
                        logger.debug("Set root atom (simple) " + rootAtom);
                    }
                }
                else
                {
                    if (logger.isDebugEnabled())
                    {
                        logger.debug("Set root atom (ext.) " + newRoot);
                    }
                }
            }
        }
    }

    /**
     * Traverse cycles to assign aromaticity flags to the atoms and bonds starting from a root atom.
     *
     * It's important that the root atoms are not trapped by further neighboured ring atoms.
     * See {@link #selectRootAtoms(JOEMol, boolean}} for details.
     *
     * @param  root   the root atom from which we will start
     * @param  atom   the actual atom which will be checked
     * @param  prev   previous atom
     * @param  er     minimal and maximal number of electrons
     * @param  depth  depth of the search, e.g. 6 or 20 for typical aromatic systems
     * @return        <tt>true</tt> if the actual visited atom is aromatic
     *
     * @see #selectRootAtoms(JOEMol, boolean}
     */
    private boolean traverseCycle(JOEAtom root, JOEAtom atom, JOEBond prev,
        IntInt er, int depth)
    {
        if (atom == root)
        {
            for (int i = er.i1; i <= er.i2; i++)
            {
                if (((i % 4) == 2) && (i > 2))
                {
                    //					System.out.println(
                    //						"traverse root atom "
                    //							+ atom.getIdx()
                    //							+ " is aromatic, el%4="
                    //							+ (i % 4));
                    return true;
                }
            }

            //			System.out.println(
            //				"traverse root atom " + atom.getIdx() + " is NOT aromatic.");
            return false;
        }

        if ((depth == 0) || !potentiallyAromatic[atom.getIdx()] ||
                _visit[atom.getIdx()])
        {
            return false;
        }

        boolean result = false;

        depth--;
        er.i1 += (numberOfElectrons[atom.getIdx()]).i1;
        er.i2 += (numberOfElectrons[atom.getIdx()]).i2;

        _visit[atom.getIdx()] = true;

        JOEAtom nbr;
        JOEBond bond;
        NbrAtomIterator nait = atom.nbrAtomIterator();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();
            bond = nait.actualBond();

            //      System.out.println("trvs: "+nbr.getIdx()+" "+bvpa[0]+" depth"+depth);
            if ((bond != prev) && bond.isInRing() &&
                    potentiallyAromatic[nbr.getIdx()])
            {
                //        System.out.println("er "+er.i1+" "+er.i2);
                if (traverseCycle(root, nbr, bond, er, depth))
                {
                    result = true;
                    bond.setAromatic();

                    //					System.out.println(
                    //						"traverse atom " + atom.getIdx() + " is aromatic (electrons: "+er+").");
                }
//                else
//                {
//                    //					System.out.println(
//                    //						"traverse atom " + atom.getIdx() + " is NOT aromatic (electrons: "+er+").");
//                }
            }
        }

        _visit[atom.getIdx()] = false;

        if (result)
        {
            atom.setAromatic();
        }

        er.i1 -= (numberOfElectrons[atom.getIdx()]).i1;
        er.i2 -= (numberOfElectrons[atom.getIdx()]).i2;

        //		System.out.println(
        //			"traverse: "
        //				+ atom.getIdx()
        //				+ " elMin:"
        //				+ er.i1
        //				+ " elMax:"
        //				+ er.i2);
        return result;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
