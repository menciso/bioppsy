///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEVirtualBond.java,v $
//  Purpose:  Virtual bond.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;


/**
 * Virtual bond.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/03/15 23:16:13 $
 */
public class JOEVirtualBond extends JOEGenericData
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    protected int _bgn;

    /**
     *  Description of the Field
     */
    protected int _end;

    /**
     *  Description of the Field
     */
    protected int _ord;

    /**
     *  Description of the Field
     */
    protected int _stereo;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEVirtualBond object
     */
    public JOEVirtualBond()
    {
        this(0, 0, 0, 0);
    }

    /**
     *  Constructor for the JOEVirtualBond object
     *
     * @param  bgn  Description of the Parameter
     * @param  end  Description of the Parameter
     * @param  ord  Description of the Parameter
     */
    public JOEVirtualBond(int bgn, int end, int ord)
    {
        this(bgn, end, ord, 0);
    }

    /**
     *  Constructor for the JOEVirtualBond object
     *
     * @param  bgn     Description of the Parameter
     * @param  end     Description of the Parameter
     * @param  ord     Description of the Parameter
     * @param  stereo  Description of the Parameter
     */
    public JOEVirtualBond(int bgn, int end, int ord, int stereo)
    {
        dataType = JOEDataType.JOE_VIRTUAL_BOND_DATA;
        this.setAttribute(dataType.getDefaultAttr());

        //_attr = "VirtualBondData";
        _bgn = bgn;
        _end = end;
        _ord = ord;
        _stereo = stereo;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the bgn attribute of the JOEVirtualBond object
     *
     * @return    The bgn value
     */
    public int getBgn()
    {
        return (_bgn);
    }

    /**
     *  Gets the end attribute of the JOEVirtualBond object
     *
     * @return    The end value
     */
    public int getEnd()
    {
        return (_end);
    }

    /**
     *  Gets the order attribute of the JOEVirtualBond object
     *
     * @return    The order value
     */
    public int getOrder()
    {
        return (_ord);
    }

    /**
     *  Gets the stereo attribute of the JOEVirtualBond object
     *
     * @return    The stereo value
     */
    public int getStereo()
    {
        return (_stereo);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
