///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOERgroupData.java,v $
//  Purpose:  Rgroup data.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/02/20 13:11:43 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.Vector;

import joelib.util.types.IntInt;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Stores Rgroup data.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/02/20 13:11:43 $
 */
public class JOERgroupData extends JOEGenericData
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * protected member variables
     *------------------------------------------------------------------------- */

    /**
     *  {@link java.util.Vector} of <tt>IntInt</tt>
     */
    protected Vector _vr;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the JOERgroup object
     */
    public JOERgroupData()
    {
        dataType = JOEDataType.JOE_RGROUP_DATA;
        this.setAttribute(dataType.getDefaultAttr());
        _vr = new Vector();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     * Set all atom-Rgroup entries.
     * {@link IntInt} contains in the first value the atom index and in the
     * second value the rgroup number.
     * @param  vr  {@link java.util.Vector} of <tt>IntInt</tt>
     */
    public void setData(Vector vr)
    {
        _vr = vr;
    }

    /**
     * Gets the Rgroup informations.
     * {@link IntInt} contains in the first value the atom index and in the
     * second value the rgroup number.
     *
     * @return    {@link java.util.Vector} of <tt>IntInt</tt>
     */
    public Vector getData()
    {
        return (_vr);
    }

    /**
     * Add new atom-Rgroup entry.
     * {@link IntInt} contains in the first value the atom index and in the
     * second value the rgroup number.
     *
     * @param  r  Description of the Parameter
     */
    public void add(IntInt r)
    {
        _vr.add(r);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
