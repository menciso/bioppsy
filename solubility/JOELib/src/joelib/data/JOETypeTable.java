///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOETypeTable.java,v $
//  Purpose:  Type table.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.18 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.PropertyHolder;

import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.util.JHM;


/**
 * Type table to map internal atom types to atom types of other file formats.
 * The definition file can be defined in the
 * <tt>joelib.data.JOETypeTable.resourceFile</tt> property in the {@link wsi.ra.tool.PropertyHolder}.
 * The {@link wsi.ra.tool.ResourceLoader} loads the <tt>joelib.properties</tt> file for default.
 *
 * <p>
 * Default:<br>
 * joelib.data.JOETypeTable.resourceFile=<a href="http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/joelib/joelib/src/joelib/data/plain/types.txt?rev=HEAD&content-type=text/vnd.viewcvs-markup">joelib/data/plain/types.txt</a>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.18 $, $Date: 2004/03/15 23:16:13 $
 */
public class JOETypeTable extends JOEGlobalDataBase implements HardCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.data.JOETypeTable");
    private static JOETypeTable typeTable;
    protected static final String DEFAULT_RESOURCE = "joelib/data/plain/types.txt";
    private static final String VENDOR = "http://joelib.sf.net";
    private static final String RELEASE_VERSION = "$Revision: 1.18 $";
    private static final String RELEASE_DATE = "$Date: 2004/03/15 23:16:13 $";

    //~ Instance fields ////////////////////////////////////////////////////////

    private String releaseDate;
    private String releaseVersion;
    private String vendor;

    /**
     *  of type <tt>String</tt>
     */
    private Vector _colnames;

    /**
     *  of type <tt>String</tt>-{@link java.util.Vector}
     */
    private Vector _table;
    private int _from;
    private int _ncols;
    private int _nrows;
    private int _to;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOETypeTable object
     */
    private JOETypeTable()
    {
        initialized = false;

        Properties prop = PropertyHolder.instance().getProperties();
        resourceFile = prop.getProperty(this.getClass().getName() +
                ".resourceFile", DEFAULT_RESOURCE);

        //    _linecount  = 0;
        _from = _to = -1;

        _colnames = new Vector();
        _table = new Vector();

        vendor = VENDOR;
        releaseVersion = JOEKernel.transformCVStag(RELEASE_VERSION);
        releaseDate = JOEKernel.transformCVStag(RELEASE_DATE);

        JOEKernel.instance().addHardCodedKernel(this);
        init();
        JOEKernel.instance().addSoftCodedKernel(this);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the fromType attribute of the JOETypeTable object
     *
     * @param  from  The new fromType value
     * @return       Description of the Return Value
     */
    public boolean setFromType(String from)
    {
        if (!initialized)
        {
            init();
        }

        String tmp = from;

        for (int i = 0; i < _colnames.size(); i++)
        {
            if (tmp.equals((String) _colnames.get(i)))
            {
                _from = i;

                return true;
            }
        }

        //throw new Exception("Requested type column not found");
        logger.error("Requested type column not found");

        return false;
    }

    /**
     * Release date for this expert system (hard coded).
     *
     * @return Release date for this expert system (hard coded).
     */
    public String getReleaseDate()
    {
        return releaseDate;
    }

    /**
     * Release version for this expert system (hard coded).
     *
     * @return Release version for this expert system (hard coded).
     */
    public String getReleaseVersion()
    {
        return releaseVersion;
    }

    /**
     *  Sets the toType attribute of the JOETypeTable object
     *
     * @param  to  The new toType value
     * @return     Description of the Return Value
     */
    public boolean setToType(String to)
    {
        if (!initialized)
        {
            init();
        }

        String tmp = to;

        for (int i = 0; i < _colnames.size(); i++)
        {
            if (tmp.equals((String) _colnames.get(i)))
            {
                _to = i;

                return true;
            }
        }

        //throw new Exception("Requested type column not found");
        logger.error("Requested type column not found");

        return false;
    }

    /**
     * Vendor for this expert system (hard coded).
     *
     * @return Vendor for this expert system (hard coded).
     */
    public String getVendor()
    {
        return vendor;
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static synchronized JOETypeTable instance()
    {
        if (typeTable == null)
        {
            typeTable = new JOETypeTable();
        }

        return typeTable;
    }

    /**
     *  Description of the Method
     *
     * @param  buffer  Description of the Parameter
     */
    public void parseLine(String buffer)
    {
        //if (_linecount == 0)
        //      System.out.println("line "+getLineCounter()+": "+buffer);
        if (getLineCounter() == 5)
        {
            Vector vs = new Vector();

            // of type String
            JHM.instance().tokenize(vs, buffer);

            _ncols = Integer.parseInt((String) vs.get(0));
            _nrows = Integer.parseInt((String) vs.get(1));
        }
        else if (getLineCounter() == 6)
        {
            JHM.instance().tokenize(_colnames, buffer);
        }
        else
        {
            // skip empty lines
            if (buffer.trim().equals(""))
            {
                return;
            }

            // store all types
            Vector vc = new Vector();
            JHM.instance().tokenize(vc, buffer);

            //      System.out.println("vc.size "+vc.size() +"_nrows"+_nrows);
            if (vc.size() == _nrows)
            {
                _table.add(vc);
            }
            else
            {
                logger.error("Wrong number of rows " + vc.size() + " (" +
                    _nrows + " expected) in " + resourceFile + " in line " +
                    getLineCounter() + ":\n" + buffer);
            }
        }

        //    _linecount++;
    }

    /**
     *  Description of the Method
     *
     * @param  from  Description of the Parameter
     * @return       Description of the Return Value
     */
    public String translate(char[] from)
    {
        if (!initialized)
        {
            init();
        }

        String to = translate(new String(from));

        return to;
    }

    /**
     *  Description of the Method
     *
     * @param  from  Description of the Parameter
     * @return       Description of the Return Value
     */
    public String translate(String from)
    {
        //    System.out.println("from:"+from);
        if (!initialized)
        {
            init();
        }

        if ((from == null) || from.equals(""))
        {
            logger.error("Empty atom type can not be translated !");

            return null;
        }

        //    String  to=null;
        for (int i = 0; i < _table.size(); i++)
        {
            Vector v = (Vector) _table.get(i);

            //      System.out.println("check: "+((String) v.get(_from)));
            if ((v.size() > _from) && ((String) v.get(_from)).equals(from))
            {
                return (String) v.get(_to);
            }
        }

        logger.error("Atom type (" + from + ") can not be found! Check '" +
            PropertyHolder.instance().getProperties().getProperty(JOETypeTable.class.getName() +
                ".resourceFile", DEFAULT_RESOURCE) + "' and '" +
            PropertyHolder.instance().getProperties().getProperty(JOEAtomTyper.class.getName() +
                ".resourceFile", JOEAtomTyper.DEFAULT_RESOURCE) +
            "' definition files.");

        return null;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
