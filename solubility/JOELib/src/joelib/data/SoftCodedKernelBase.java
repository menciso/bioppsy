///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SoftCodedKernelBase.java,v $
//  Purpose:  Descriptor base class.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.ResourceLoader;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.util.JHM;


/**
 * Interface for defining a hard coded kernel part.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2004/03/15 23:16:13 $
 */
public abstract class SoftCodedKernelBase implements SoftCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.data.SoftCodedKernelBase");
    private static final String VENDOR = "VENDOR:";
    private static final String RELEASE_VERSION = "RELEASE_VERSION:";
    private static final String RELEASE_DATE = "RELEASE_DATE:";

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  External soft coded chemistry kernel data resource file.
     */
    protected String resourceFile;

    /**
     *  Soft coded chemistry kernel initialized.
     */
    protected boolean initialized;
    private String releaseDateExternal;
    private String releaseVersionExternal;
    private String vendorExternal;
    private int lineCounter;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the lineCounter attribute of the JOEGlobalDataBase object
     *
     * @return    The lineCounter value
     */
    public final int getLineCounter()
    {
        return lineCounter;
    }

    /**
     * Release date of external (soft coded) data for this expert system.
     *
     * @return Release date of external (soft coded) data for this expert system.
     */
    public String getReleaseDateExternal()
    {
        return releaseDateExternal;
    }

    /**
     * Release version of external (soft coded) data for this expert system.
     *
     * @return Release version of external (soft coded) data for this expert system.
     */
    public String getReleaseVersionExternal()
    {
        return releaseVersionExternal;
    }

    /**
     * Vendor of external (soft coded) data for this expert system.
     *
     * @return Vendor of external (soft coded) data for this expert system.
     */
    public String getVendorExternal()
    {
        return vendorExternal;
    }

    /**
     *  Description of the Method
     */
    public synchronized void init()
    {
        if (initialized)
        {
            return;
        }

        initialized = true;

        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(resourceFile);

        //    System.out.println("Loaded "+resourceFile+" sucessfull:"+(bytes!=null));
        if (bytes != null)
        {
            try
            {
                getLinesFromBytes(bytes);
            }
             catch (Exception ex)
            {
                logger.error("Problems in loading soft kernel from " +
                    resourceFile + ": " + ex.getMessage());
            }
        }
        else
        {
            logger.error("Unable to open data file '" + resourceFile + "'");

            //throw new Exception("Unable to open data file '" + _filename +"'");
            //System.exit(1);
            initialized = false;
        }
    }

    /**
     *  Description of the Method
     *
     * @param  buffer  Description of the Parameter
     */
    public abstract void parseLine(String buffer);

    /**
     * Resource for this soft coded expert system.
     *
     * @return Resource for this soft coded expert system.
     */
    public String getResourceExternal()
    {
        return this.resourceFile;
    }

    /**
     *  Gets the linesFromBytes attribute of the JOEGlobalDataBase object
     *
     * @param  bytes  Description of the Parameter
     */
    protected synchronized void getLinesFromBytes(byte[] bytes)
        throws Exception
    {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        InputStreamReader isr = new InputStreamReader(bais);
        LineNumberReader lnr = new LineNumberReader(isr);
        String nextLine;
        int index = 0;
        Vector vs = new Vector();

        for (;;)
        {
            nextLine = lnr.readLine();

            if (nextLine == null)
            {
                break;
            }

            lineCounter = lnr.getLineNumber();

            if (index == 0)
            {
                if (!nextLine.startsWith(VENDOR))
                {
                    throw new Exception(
                        "External data definition must contain a VENDOR information, but contains: '" +
                        nextLine + "'");
                }
                else
                {
                    String tmp = nextLine.substring(VENDOR.length()).trim();

                    if (vendorExternal == null)
                    {
                        vendorExternal = tmp;
                    }
                    else
                    {
                        vendorExternal = vendorExternal + ":" + tmp;
                    }
                }
            }
            else if (index == 1)
            {
                if (!nextLine.startsWith(RELEASE_VERSION))
                {
                    throw new Exception(
                        "External data definition must contain a RELEASE_VERSION information, but contains: '" +
                        nextLine + "'");
                }
                else
                {
                    String tmp = JOEKernel.transformCVStag(nextLine.substring(
                                RELEASE_VERSION.length()).trim());

                    if (releaseVersionExternal == null)
                    {
                        releaseVersionExternal = tmp;
                    }
                    else
                    {
                        releaseVersionExternal = releaseVersionExternal + ":" +
                            tmp;
                    }
                }
            }
            else if (index == 2)
            {
                if (!nextLine.startsWith(RELEASE_DATE))
                {
                    throw new Exception(
                        "External data definition must contain a RELEASE_DATE information, but contains: '" +
                        nextLine + "'");
                }
                else
                {
                    String tmp = JOEKernel.transformCVStag(nextLine.substring(
                                RELEASE_DATE.length()).trim());

                    if (releaseDateExternal == null)
                    {
                        releaseDateExternal = tmp;
                    }
                    else
                    {
                        releaseDateExternal = releaseDateExternal + ":" + tmp;
                    }
                }
            }
            else
            {
                parseLine(nextLine);
            }

            index++;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
