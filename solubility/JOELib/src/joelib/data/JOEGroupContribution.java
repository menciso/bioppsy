///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEGroupContribution.java,v $
//  Purpose:  Holds group contribution lists for different models (e.g. logP, MR, PSA).
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner, Stephen Jelfs
//  Version:  $Revision: 1.12 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.PropertyHolder;
import wsi.ra.tool.ResourceLoader;

import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.algo.contribution.GroupContributions;

import joelib.smarts.JOESmartsPattern;

import joelib.util.JHM;


/**
 * Holds group contribution lists for different models (e.g. logP, MR, PSA).
 *
 * @author     wegnerj
 * @author  Stephen Jelfs
 * @license GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/07/25 20:42:58 $
 * @see PropertyHolder
 * @see ResourceLoader
 * @cite ers00
 * @cite wc99
 */
public class JOEGroupContribution extends SoftCodedKernelBase
    implements HardCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.data.JOEGroupContribution");

    //	private final static String FILE_SEP_CHAR =System.getProperty("file.separator");
    private static JOEGroupContribution instance;
    private static final String DEFAULT_RESOURCE_DIR = "joelib/data/plain";
    private static final String DEFAULT_CONTRIBUTION = ".contributions";
    private static final String DEFAULT_MODELS = "LogP MR PSA";
    private static final String VENDOR = "http://joelib.sf.net";
    private static final String RELEASE_VERSION = "$Revision: 1.12 $";
    private static final String RELEASE_DATE = "$Date: 2004/07/25 20:42:58 $";

    //~ Instance fields ////////////////////////////////////////////////////////

    protected String _contribExt;
    protected String _models;
    protected String _resourceDir;
    protected byte[] _dataptr;
    protected boolean _init;
    private GroupContributions actualGC;
    private Hashtable contributions;
    private String releaseDate;
    private String releaseVersion;
    private String vendor;
    private boolean heavyAtoms;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEGroupContribution object
     */
    private JOEGroupContribution()
    {
        _init = false;

        Properties prop = PropertyHolder.instance().getProperties();
        _resourceDir = prop.getProperty(this.getClass().getName() +
                ".resourceDir", DEFAULT_RESOURCE_DIR);
        _contribExt = prop.getProperty(this.getClass().getName() +
                ".contributionExtension", DEFAULT_CONTRIBUTION);
        _models = prop.getProperty(this.getClass().getName() + ".models",
                DEFAULT_MODELS);

        vendor = VENDOR;
        releaseVersion = JOEKernel.transformCVStag(RELEASE_VERSION);
        releaseDate = JOEKernel.transformCVStag(RELEASE_DATE);

        JOEKernel.instance().addHardCodedKernel(this);
        init();
        JOEKernel.instance().addSoftCodedKernel(this);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Release date for this expert system (hard coded).
     *
     * @return Release date for this expert system (hard coded).
     */
    public String getReleaseDate()
    {
        return releaseDate;
    }

    /**
     * Release version for this expert system (hard coded).
     *
     * @return Release version for this expert system (hard coded).
     */
    public String getReleaseVersion()
    {
        return releaseVersion;
    }

    /**
     * Vendor for this expert system (hard coded).
     *
     * @return Vendor for this expert system (hard coded).
     */
    public String getVendor()
    {
        return vendor;
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
        contributions.clear();
        contributions = null;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static synchronized JOEGroupContribution instance()
    {
        if (instance == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Getting " + JOEGroupContribution.class.getName() +
                    " instance.");
            }

            instance = new JOEGroupContribution();
        }

        return instance;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     */
    public GroupContributions getGroupContributions(String model)
    {
        if (!_init)
        {
            init();
        }

        GroupContributions tmp;
        tmp = (GroupContributions) contributions.get(model);

        if (tmp == null)
        {
            logger.error("Group contribution model " + model +
                " does not exist.");
        }

        return tmp;
    }

    /**
     *  Description of the Method
     */
    public synchronized void init()
    {
        if (_init)
        {
            return;
        }

        _init = true;

        Vector vs = new Vector();
        byte[] bytes = null;

        JHM.tokenize(vs, _models);

        if (vs.size() == 0)
        {
            logger.error("No group contribution models defined.");

            return;
        }

        contributions = new Hashtable(vs.size());

        String filename;

        for (int i = 0; i < vs.size(); i++)
        {
            filename = _resourceDir + "/" + (String) vs.get(i) + _contribExt;
            bytes = ResourceLoader.instance().getBytesFromResourceLocation(filename);

            if (resourceFile == null)
            {
                resourceFile = filename;
            }
            else
            {
                resourceFile = resourceFile + ":" + filename;
            }

            actualGC = new GroupContributions((String) vs.get(i));
            heavyAtoms = true;

            //    System.out.println("Loaded "+resourceFile+" sucessfull:"+(bytes!=null));
            if (bytes != null)
            {
                try
                {
                    getLinesFromBytes(bytes);
                }
                 catch (Exception ex)
                {
                    logger.error("Problems in loading soft kernel from " +
                        resourceFile + ": " + ex.getMessage());
                }
            }
            else
            {
                logger.error("Unable to open data file '" + filename + "'");

                //throw new Exception("Unable to open data file '" + _filename +"'");
                System.exit(1);
            }

            contributions.put((String) vs.get(i), actualGC);
        }
    }

    /**
     *  Description of the Method
     *
     * @param  buffer  Description of the Parameter
     */
    public void parseLine(String buffer)
    {
        // skip comments and blanks
        if (buffer.startsWith("#") || buffer.startsWith("\n"))
        {
            return;
        }

        // check for heavy/hydrogen atom switch
        if (buffer.startsWith(";hydrogen"))
        {
            heavyAtoms = false;

            return;
        }
        else if (buffer.startsWith(";heavy"))
        {
            heavyAtoms = true;

            return;
        }

        // extract smarts and value tokens
        StringTokenizer tokens = new StringTokenizer(buffer);

        if (tokens.countTokens() < 2)
        {
            return;
        }

        // parse smarts
        JOESmartsPattern smarts = new JOESmartsPattern();
        smarts.init(tokens.nextToken());

        // parse value
        Double value = new Double(tokens.nextToken());

        // store values for atom or hydrogen
        if (heavyAtoms)
        {
            actualGC.atomSmarts.add(smarts);
            actualGC.atomContributions.add(value);
        }
        else
        {
            actualGC.hydrogenSmarts.add(smarts);
            actualGC.hydrogenContributions.add(value);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
