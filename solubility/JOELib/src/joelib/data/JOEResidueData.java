///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEResidueData.java,v $
//  Purpose:  Type table.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.PropertyHolder;

import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.util.JOEBitVec;
import joelib.util.types.StringInt;


/**
 * Type table.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/03/15 23:16:13 $
 */
public class JOEResidueData extends JOEGlobalDataBase implements HardCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.data.JOEResidueData");
    private static JOEResidueData residueData;
    private static final String DEFAULT_RESOURCE = "joelib/data/plain/residue.txt";
    private static final String VENDOR = "http://joelib.sf.net";
    private static final String RELEASE_VERSION = "$Revision: 1.12 $";
    private static final String RELEASE_DATE = "$Date: 2004/03/15 23:16:13 $";

    //~ Instance fields ////////////////////////////////////////////////////////

    private String releaseDate;
    private String releaseVersion;
    private String vendor;
    private Vector _resatoms;
    private Vector _resbonds;
    private Vector _resname;
    private Vector _vatmtmp;
    private Vector _vtmp;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOETypeTable object
     */
    private JOEResidueData()
    {
        initialized = false;

        Properties prop = PropertyHolder.instance().getProperties();
        resourceFile = prop.getProperty(this.getClass().getName() +
                ".resourceFile", DEFAULT_RESOURCE);

        _resname = new Vector();
        _resatoms = new Vector();
        _resbonds = new Vector();

        _vatmtmp = new Vector();
        _vtmp = new Vector();

        vendor = VENDOR;
        releaseVersion = JOEKernel.transformCVStag(RELEASE_VERSION);
        releaseDate = JOEKernel.transformCVStag(RELEASE_DATE);

        JOEKernel.instance().addHardCodedKernel(this);
        init();
        JOEKernel.instance().addSoftCodedKernel(this);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Release date for this expert system (hard coded).
     *
     * @return Release date for this expert system (hard coded).
     */
    public String getReleaseDate()
    {
        return releaseDate;
    }

    /**
     * Release version for this expert system (hard coded).
     *
     * @return Release version for this expert system (hard coded).
     */
    public String getReleaseVersion()
    {
        return releaseVersion;
    }

    public boolean setResName(String s)
    {
        //   int i;
        //    for (i = 0;i < _resname.size();i++)
        //	if (_resname[i] == s)
        //	{
        //	    _resnum = i;
        //	    return(true);
        //	}
        //
        //    _resnum = -1;
        return (false);
    }

    /**
     * Vendor for this expert system (hard coded).
     *
     * @return Vendor for this expert system (hard coded).
     */
    public String getVendor()
    {
        return vendor;
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static synchronized JOEResidueData instance()
    {
        if (residueData == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Getting " + JOEResidueData.class.getName() +
                    " instance.");
            }

            residueData = new JOEResidueData();
        }

        return residueData;
    }

    public boolean assignBonds(JOEMol mol, JOEBitVec bv)
    {
        //  JOEAtom a1,a2;
        //  JOEResidue r1,r2;
        //  vector<OEAtom*>::iterator i,j;
        //
        //  //assign alpha peptide bonds
        //  for (a1 = mol.BeginAtom(i);a1;a1 = mol.NextAtom(i))
        //    if (bv.bitIsOn(a1.getIdx()))
        //      {
        //	r1 = a1.getResidue();
        //	if (!(r1.getAtomID(a1) == "C")) continue;
        //	for (j=i,a2 = mol.NextAtom(j);a2;a2 = mol.NextAtom(j))
        //	  {
        //	    r2 = a2.getResidue();
        //	    if (!(r2.getAtomID(a2) == "N")) continue;
        //	    if (r1.getNum() < r2.getNum()-1) break;
        //	    if (r1.getChainNum() == r2.getChainNum())
        //	      {
        //		mol.AddBond(a1.getIdx(),a2.getIdx(),1);
        //		break;
        //	      }
        //	  }
        //      }
        //
        //  Vector v;
        //  int bo,skipres=0;
        //  string rname = "";
        //  //assign other residue bonds
        //  for (a1 = mol.BeginAtom(i);a1;a1 = mol.NextAtom(i))
        //    {
        //      r1 = a1.getResidue();
        //      if (skipres && r1.getNum() == skipres) continue;
        //
        //      if (r1.getName() != rname)
        //	{
        //	  skipres = setResName(r1.getName()) ? 0 : r1.getNum();
        //	  rname = r1.getName();
        //	}
        //      //assign bonds for each atom
        //      for (j=i,a2 = mol.NextAtom(j);a2;a2 = mol.NextAtom(j))
        //	{
        //	  r2 = a2.getResidue();
        //	  if (r1.getNum() != r2.getNum()) break;
        //	  if (r1.getName() != r2.getName()) break;
        //
        //	  if ((bo = lookupBO(r1.getAtomID(a1),r2.getAtomID(a2))))
        //	    {
        //	      v = a1.getVector() - a2.getVector();
        //	      if (v.length_2() < 3.5) //float check by distance
        //		  mol.addBond(a1.getIdx(),a2.getIdx(),bo);
        //	    }
        //	}
        //    }
        //
        //  int hyb;
        //  string type;
        //
        //  //types and hybridization
        //  for (a1 = mol.BeginAtom(i);a1;a1 = mol.NextAtom(i))
        //    {
        //      if (a1.isOxygen() && !a1.getValence())
        //	{
        //	  a1.setType("O3");
        //	  continue;
        //	}
        //
        //      if (a1.isHydrogen())
        //	{
        //	  a1.setType("H");
        //	  continue;
        //	}
        //
        //      r1 = a1.getResidue();
        //      if (skipres && r1.getNum() == skipres) continue;
        //
        //      if (r1.getName() != rname)
        //	{
        //	  skipres = setResName(r1.getName()) ? 0 : r1.getNum();
        //	  rname = r1.getName();
        //	}
        //
        //      //***valence rule for O-
        //      if (a1.isOxygen() && a1.getValence() == 1)
        //	{
        //	  JOEBond bond;
        //	  bond = *(a1.beginBonds());
        //	  if (bond.getBO() == 2)
        //	    {
        //	      a1.setType("O2"); a1.setHyb(2);
        //	    }
        //	  if (bond.getBO() == 1)
        //	    {
        //	      a1.setType("O-"); a1.setHyb(3);
        //	      a1.setFormalCharge(-1);
        //	    }
        //	}
        //      else
        //	if (lookupType(r1.getAtomID(a1),type,hyb))
        //	  {
        //	    a1.setType(type);
        //	    a1.setHyb(hyb);
        //	  }
        //	else // try to figure it out by bond order ???
        //	  {
        //	  }
        //    }
        return (true);
    }

    public int lookupBO(String s)
    {
        //    if (_resnum == -1) return(0);
        //
        //     int i;
        //    for (i = 0;i < _resbonds[_resnum].size();i++)
        //	if (_resbonds[_resnum][i].first == s)
        //	    return(_resbonds[_resnum][i].second);
        return (0);
    }

    public int lookupBO(String s1, String s2)
    {
        //    if (_resnum == -1) return(0);
        //    string s;
        //
        //    s = (s1 < s2) ? s1 + " " + s2 : s2 + " " + s1;
        //
        //     int i;
        //    for (i = 0;i < _resbonds[_resnum].size();i++)
        //	if (_resbonds[_resnum][i].first == s)
        //	    return(_resbonds[_resnum][i].second);
        return (0);
    }

    public StringInt lookupType(String atmid)
    {
        //    if (_resnum == -1) return(false);
        //
        //    String s;
        //    //vector<string>::iterator i;
        //    String type;
        //    int hyb;
        //    for (i = _resatoms[_resnum].begin();i != _resatoms[_resnum].end();i+=3)
        //      if (atmid == i)
        //	{
        //	  i++;	  type = i;
        //	  i++;	  hyb = atoi((i).c_str());
        //
        //	  return new StringInt(type, hyb);
        //	}
        return null;
    }

    public void parseLine(String buffer)
    {
        //   int bo;
        //   String s;
        //
        //
        //    if (!buffer.trim().equals("") && buffer.charAt(0) != '#')
        //    {
        //      Vector      vs   = new Vector();
        //      // of type String
        //      JHM.instance().tokenize(vs, buffer);
        //	if (vs.size()!=0)
        //	 {
        //          s = (String) vs.get(0);
        //	    if ( s.equals("BOND") && vs.size() == 4 )
        //	      {
        //			s = (vs[1] < vs[2]) ? vs[1] + " " + vs[2] :
        //			vs[2] + " " + vs[1];
        //			bo = atoi(vs[3].c_str());
        //			_vtmp.add(pair<string,int> (s,bo));
        //	      }
        //
        //	    if ( s.equals("ATOM") && vs.size() == 4)
        //	      {
        //			_vatmtmp.add(vs[1]);
        //			_vatmtmp.add(vs[2]);
        //			_vatmtmp.add(vs[3]);
        //	      }
        //
        //	    if ( s.equals("RES") )
        //            {	_resname.add(vs[1]);
        //            }
        //
        //	    if ( s.equals("END") )
        //	      {
        //			_resatoms.add(_vatmtmp);
        //			_resbonds.add(_vtmp);
        //			_vtmp.clear();
        //			_vatmtmp.clear();
        //	      }
        //	  }
        //    }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
