///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEExternalBond.java,v $
//  Purpose:  External bond.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/03/15 23:16:13 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;


/**
 * External bond.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/03/15 23:16:13 $
 */
public class JOEExternalBond
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private JOEAtom _atom;
    private JOEBond _bond;
    private int _idx;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEExternalBond object
     */
    public JOEExternalBond()
    {
    }

    /**
     *  Constructor for the JOEExternalBond object
     *
     * @param  atom  Description of the Parameter
     * @param  bond  Description of the Parameter
     * @param  idx   Description of the Parameter
     */
    public JOEExternalBond(JOEAtom atom, JOEBond bond, int idx)
    {
        _idx = idx;
        _atom = atom;
        _bond = bond;
    }

    /**
     *  Constructor for the JOEExternalBond object
     *
     * @param  src  Description of the Parameter
     */
    public JOEExternalBond(final JOEExternalBond src)
    {
        _idx = src.getIdx();
        _atom = src.getAtom();
        _bond = src.getBond();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the atom attribute of the JOEExternalBond object
     *
     * @return    The atom value
     */
    public final JOEAtom getAtom()
    {
        return (_atom);
    }

    /**
     *  Gets the bond attribute of the JOEExternalBond object
     *
     * @return    The bond value
     */
    public final JOEBond getBond()
    {
        return (_bond);
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     *  Gets the idx attribute of the JOEExternalBond object
     *
     * @return    The idx value
     */
    public final int getIdx()
    {
        return (_idx);
    }

    /**
     *  Sets the atom attribute of the JOEExternalBond object
     *
     * @param  atom  The new atom value
     */
    public void setAtom(JOEAtom atom)
    {
        _atom = atom;
    }

    /**
     *  Sets the bond attribute of the JOEExternalBond object
     *
     * @param  bond  The new bond value
     */
    public void setBond(JOEBond bond)
    {
        _bond = bond;
    }

    /**
     *  Sets the idx attribute of the JOEExternalBond object
     *
     * @param  idx  The new idx value
     */
    public void setIdx(int idx)
    {
        _idx = idx;
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
