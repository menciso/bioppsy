///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEExternalBondData.java,v $
//  Purpose:  External bond data.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/03/15 13:33:41 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import java.util.Vector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;


/**
 * External bond data.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/03/15 13:33:41 $
 */
public class JOEExternalBondData extends JOEGenericData
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  {@link java.util.Vector} of <tt>JOEExternalBond</tt>
     */
    protected Vector _vexbnd;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEExternalBondData object
     */
    public JOEExternalBondData()
    {
        dataType = JOEDataType.JOE_EXTERNAL_BOND_DATA;
        this.setAttribute(dataType.getDefaultAttr());

        //_attr = "ExternalBondData";
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the data attribute of the JOEExternalBondData object
     *
     * @param  atom  The new data value
     * @param  bond  The new data value
     * @param  idx   The new data value
     */
    public void setData(JOEAtom atom, JOEBond bond, int idx)
    {
        JOEExternalBond xb = new JOEExternalBond(atom, bond, idx);
        _vexbnd.add(xb);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
