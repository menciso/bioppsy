///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEKernel.java,v $
//  Purpose:  Atom typer.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.util.types.StringString;


/**
 * Chemistry kernel informations.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:42:58 $
 */
public class JOEKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.data.JOEKernel");
    public static final String CML_KERNEL_REFERENCE = "dictRef";
    public static final String CML_KERNEL_REFERENCE_PREFIX = "jk:k";
    public static final String CML_SOFT_KERNEL = "softCoded";
    public static final String CML_HARD_KERNEL = "hardCoded";
    private static JOEKernel chemistryKernel;

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable hardInfos = new Hashtable();
    private Hashtable softInfos = new Hashtable();
    private StringString kernelID;
    private String[] infos;
    private String[] titles;

    //~ Methods ////////////////////////////////////////////////////////////////

    public static synchronized JOEKernel instance()
    {
        if (chemistryKernel == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Getting " + JOEKernel.class.getName() +
                    " instance.");
            }

            chemistryKernel = new JOEKernel();
        }

        return chemistryKernel;
    }

    public static String transformCVStag(String tag)
    {
        String tmp = tag.replace('$', ' ');
        tmp = tmp.trim();

        if (tmp.startsWith("Revision:"))
        {
            tmp = tmp.substring("Revision:".length());
        }

        if (tmp.startsWith("Date:"))
        {
            tmp = tmp.substring("Date:".length());
        }

        tmp = tmp.replace('/', '-');
        tmp = tmp.replace(':', '-');
        tmp = tmp.trim();

        return tmp.replace(' ', '_');
    }

    /**
     * @return
     */
    public int getKernelHash()
    {
        getKernelInformations();

        int kernelHash = 0;

        for (int i = 0; i < infos.length; i++)
        {
            kernelHash = (31 * kernelHash) + infos[i].hashCode();
        }

        //StringString property
        return kernelHash;
    }

    public StringString getKernelID()
    {
        if (kernelID == null)
        {
            kernelID = new StringString(CML_KERNEL_REFERENCE,
                    CML_KERNEL_REFERENCE_PREFIX +
                    Integer.toString(getKernelHash()));
        }

        return kernelID;
    }

    public String[] getKernelInformations()
    {
        if (infos == null)
        {
            getKernelTitles();
            infos = new String[titles.length];
            infos[0] = (String) softInfos.get(titles[0]);
            infos[1] = (String) softInfos.get(titles[1]);
            infos[2] = (String) softInfos.get(titles[2]);
            infos[3] = (String) softInfos.get(titles[3]);
            infos[4] = (String) softInfos.get(titles[4]);
            infos[5] = (String) softInfos.get(titles[5]);
            infos[6] = (String) softInfos.get(titles[6]);
            infos[7] = (String) softInfos.get(titles[7]);
            infos[8] = (String) hardInfos.get(titles[8]);
            infos[9] = (String) hardInfos.get(titles[9]);
            infos[10] = (String) hardInfos.get(titles[10]);
            infos[11] = (String) hardInfos.get(titles[11]);
            infos[12] = (String) hardInfos.get(titles[12]);
            infos[13] = (String) hardInfos.get(titles[13]);
            infos[14] = (String) hardInfos.get(titles[14]);
            infos[15] = (String) hardInfos.get(titles[15]);
        }

        return infos;
    }

    public String[] getKernelTitles()
    {
        if (titles == null)
        {
            // guarantee that all chemistry kernels are initialized
            SoftCodedKernelBase aromaticTyper = JOEAromaticTyper.instance();
            aromaticTyper.init();

            SoftCodedKernelBase atomTyper = JOEAtomTyper.instance();
            atomTyper.init();

            SoftCodedKernelBase element = JOEElementTable.instance();
            element.init();

            SoftCodedKernelBase gc = JOEGroupContribution.instance();
            gc.init();

            SoftCodedKernelBase isotope = JOEIsotopeTable.instance();
            isotope.init();

            SoftCodedKernelBase phModel = JOEPhModel.instance();
            phModel.init();

            SoftCodedKernelBase residue = JOEResidueData.instance();
            residue.init();

            SoftCodedKernelBase typeTable = JOETypeTable.instance();
            typeTable.init();

            titles = new String[16];

            String kernelType = CML_SOFT_KERNEL + ":";
            titles[0] = kernelType + aromaticTyper.getClass().getName();
            titles[1] = kernelType + atomTyper.getClass().getName();
            titles[2] = kernelType + element.getClass().getName();
            titles[3] = kernelType + gc.getClass().getName();
            titles[4] = kernelType + isotope.getClass().getName();
            titles[5] = kernelType + phModel.getClass().getName();
            titles[6] = kernelType + typeTable.getClass().getName();
            titles[7] = kernelType + residue.getClass().getName();
            kernelType = CML_HARD_KERNEL + ":";
            titles[8] = kernelType + aromaticTyper.getClass().getName();
            titles[9] = kernelType + atomTyper.getClass().getName();
            titles[10] = kernelType + element.getClass().getName();
            titles[11] = kernelType + gc.getClass().getName();
            titles[12] = kernelType + isotope.getClass().getName();
            titles[13] = kernelType + phModel.getClass().getName();
            titles[14] = kernelType + typeTable.getClass().getName();
            titles[15] = kernelType + residue.getClass().getName();
        }

        return titles;
    }

    public void addHardCodedKernel(HardCodedKernel hardCodedKernel)
    {
        String kernelString = hardCodedKernel.getClass().getName() + " " +
            hardCodedKernel.getVendor() + " " +
            hardCodedKernel.getReleaseVersion() + " " +
            hardCodedKernel.getReleaseDate();

        if (logger.isDebugEnabled())
        {
            logger.debug("hard coded chemistry kernel: " + kernelString);
        }

        hardInfos.put(CML_HARD_KERNEL + ":" +
            hardCodedKernel.getClass().getName(), kernelString);
    }

    public void addSoftCodedKernel(SoftCodedKernel softCodedKernel)
    {
        String kernelString = softCodedKernel.getClass().getName() + " " +
            softCodedKernel.getVendorExternal() + " " +
            softCodedKernel.getResourceExternal() + " " +
            softCodedKernel.getReleaseVersionExternal() + " " +
            softCodedKernel.getReleaseDateExternal();

        if (logger.isDebugEnabled())
        {
            logger.debug("soft coded chemistry kernel: " + kernelString);
        }

        softInfos.put(CML_SOFT_KERNEL + ":" +
            softCodedKernel.getClass().getName(), kernelString);
    }

    public static void main(String[] args)
    {
        String[] infos = JOEKernel.instance().getKernelInformations();
        String[] titles = JOEKernel.instance().getKernelTitles();
        Hashtable attributes = new Hashtable();
        int kernelHash = JOEKernel.instance().getKernelHash();
        StringBuffer sb = new StringBuffer();
        String title;

        String delimiterHuge = "===========================================================================";
        String delimiterSmall = "---------------------------------------------------------------------------";
        System.out.println(delimiterHuge);
        System.out.println(("Date: " + (new Date()).toGMTString()));
        System.out.println("JOELib chemistry kernel (expert systems) ID: " +
            kernelHash);
        System.out.println("The following expert systems are used:");
        System.out.println(delimiterSmall);

        for (int i = 0; i < infos.length; i++)
        {
            attributes.clear();
            title = JOEKernel.CML_KERNEL_REFERENCE_PREFIX + kernelHash + ":" +
                titles[i];

            if (i < (infos.length - 1))
            {
                sb.append(", ");
            }

            System.out.println(title + " (" + infos[i].hashCode() + ") ");
            System.out.println(infos[i]);
        }

        System.out.println(delimiterHuge);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
