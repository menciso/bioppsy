///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEChemTransformation.java,v $
//  Purpose:  Transformation of chemical groups.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.17 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;

import joelib.util.types.IntInt;
import joelib.util.types.IntIntInt;


/**
 * Transformation of chemical groups.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.17 $, $Date: 2004/07/25 20:42:58 $
 * @see joelib.data.JOEPhModel
 */
public class JOEChemTransformation
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.data.JOEChemTransformation");

    //~ Instance fields ////////////////////////////////////////////////////////

    private JOESmartsPattern beginPattern;
    private JOESmartsPattern endPattern;

    /**
     * Stores atoms which atom type should be changed.
     *  {@link java.util.Vector} of <tt>IntInt</tt>
     */
    private Vector atom2Change;

    /**
     * Stores atoms which should be deleted.
     * {@link java.util.Vector} of <tt>int[1]</tt>
     */
    private Vector atom2Delete;

    /**
     *  {@link java.util.Vector} of <tt>Int</tt>
     */

    //private         Vector            bond2Delete;

    /**
     * Stores bonds which bond type should be changed.
     *  {@link java.util.Vector} of <tt>IntIntInt</tt>
     */
    private Vector bond2Change;

    /**
     * Stores atoms which charge should be deleted.
     *  {@link java.util.Vector} of <tt>IntInt</tt>
     */
    private Vector charge2Change;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEChemTransformation object
     */
    public JOEChemTransformation()
    {
        atom2Delete = new Vector();
        atom2Change = new Vector();
        charge2Change = new Vector();

        //bond2Delete = new Vector();
        bond2Change = new Vector();

        beginPattern = new JOESmartsPattern();
        endPattern = new JOESmartsPattern();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean apply(JOEMol mol)
    {
        if (!beginPattern.match(mol))
        {
            return (false);
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("Apply SMARTS transformation for pattern: " +
                beginPattern.getSMARTS() + " >> " + endPattern.getSMARTS() +
                " on " + mol.getTitle());
        }

        Vector mlist = beginPattern.getUMapList();

        // of type int[]
        //Vector v;
        int[] itmp;
        IntInt ii;

        if (charge2Change.size() != 0)
        {
            //modify charges
            if (logger.isDebugEnabled())
            {
                logger.debug("modify " + charge2Change.size() + " charges");
            }

            for (int i = 0; i < mlist.size(); i++)
            {
                //v = (Vector)mlist.get(i);
                itmp = (int[]) mlist.get(i);

                for (int j = 0; j < charge2Change.size(); j++)
                {
                    ii = (IntInt) charge2Change.get(j);

                    if (ii.i1 < itmp.length)
                    {
                        //goof proofing
                        //itmp = (int[])v.get(ii.i1);
                        mol.getAtom(itmp[ii.i1]).setFormalCharge(ii.i2);
                    }
                }
            }

            mol.unsetImplicitValencePerceived();
        }

        IntIntInt iii;
        int[] itmp2;

        if (bond2Change.size() != 0)
        {
            //modify bond orders
            if (logger.isDebugEnabled())
            {
                logger.debug("modify " + bond2Change.size() + " bonds");
            }

            JOEBond bond;

            for (int i = 0; i < mlist.size(); i++)
            {
                //v = (Vector)mlist.get(i);
                itmp = (int[]) mlist.get(i);

                for (int j = 0; j < bond2Change.size(); j++)
                {
                    iii = (IntIntInt) charge2Change.get(j);

                    //itmp  = (int[])v.get(iii.ii.i1);
                    //itmp2 = (int[])v.get(iii.i);
                    bond = mol.getBond(itmp[iii.ii.i1], itmp[iii.i]);

                    if (bond == null)
                    {
                        logger.error("Unable to find bond.");

                        continue;
                    }

                    bond.setBO(iii.i);
                }
            }
        }

        if ((atom2Delete.size() != 0) || (atom2Change.size() != 0))
        {
            //delete atoms and change elements
            //      if(logger.isDebugEnabled()) logger.debug("change/delete atom");
            if (atom2Change.size() != 0)
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("modify " + atom2Change.size() + " atoms");
                }

                for (int i = 0; i < mlist.size(); i++)
                {
                    //v = (Vector)mlist.get(i);
                    itmp = (int[]) mlist.get(i);

                    for (int k = 0; k < atom2Change.size(); k++)
                    {
                        ii = (IntInt) charge2Change.get(k);

                        //itmp  = (int[])v.get(ii.i1);
                        mol.getAtom(itmp[ii.i1]).setAtomicNum(ii.i2);
                    }
                }
            }

            //make sure same atom isn't delete twice
            Vector vda = new Vector();

            // of type boolean[1]
            Vector vdel = new Vector();

            // of type JOEAtom
            boolean[] btmp;

            vda.ensureCapacity(mol.numAtoms() + 1);

            for (int i = 0; i <= mol.numAtoms(); i++)
            {
                vda.add(new boolean[]{false});
            }

            for (int i = 0; i < mlist.size(); i++)
            {
                //v = (Vector)mlist.get(i);
                itmp = (int[]) mlist.get(i);

                if (logger.isDebugEnabled())
                {
                    logger.debug("delete " + atom2Delete.size() + " atoms");
                }

                for (int j = 0; j < atom2Delete.size(); j++)
                {
                    itmp2 = (int[]) atom2Delete.get(j);

                    //itmp2 = (int[]) v.get(itmp2[0]);
                    btmp = (boolean[]) vda.get(itmp[itmp2[0]]);

                    if (!btmp[0])
                    {
                        btmp[0] = true;
                        vdel.add(mol.getAtom(itmp[itmp2[0]]));
                    }
                }
            }

            //			if (logger.isDebugEnabled())
            //			{
            //				logger.debug("delete " + atom2Delete.size() + " atoms: begin modifiy");
            //			}
            mol.beginModify();

            for (int k = 0; k < vdel.size(); k++)
            {
                mol.deleteAtom((JOEAtom) vdel.get(k));
            }

            //			if (logger.isDebugEnabled())
            //			{
            //				logger.debug("delete " + atom2Delete.size() + " atoms: begin modifiy");
            //			}
            mol.endModify();
        }

        //System.out.println("555555");
        return (true);
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  bgn  Description of the Parameter
     * @param  end  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean init(String bgn, String end)
    {
        //    System.out.println("beginPattern "+beginPattern);
        if (!beginPattern.init(bgn))
        {
            return (false);
        }

        if (end.length() != 0)
        {
            if (!endPattern.init(end))
            {
                return (false);
            }
        }

        //find atoms to be deleted
        int i;
        int j;
        int vb;
        boolean found;

        for (i = 0; i < beginPattern.numAtoms(); i++)
        {
            if ((vb = beginPattern.getVectorBinding(i)) != 0)
            {
                found = false;

                for (j = 0; j < endPattern.numAtoms(); j++)
                {
                    if ((vb = endPattern.getVectorBinding(j)) != 0)
                    {
                        found = true;

                        break;
                    }
                }

                if (!found)
                {
                    atom2Delete.add(new int[]{i});
                }
            }
        }

        //find elements to be changed
        int ele;

        for (i = 0; i < beginPattern.numAtoms(); i++)
        {
            if ((vb = beginPattern.getVectorBinding(i)) != 0)
            {
                ele = beginPattern.getAtomicNum(i);

                for (j = 0; j < endPattern.numAtoms(); j++)
                {
                    if ((vb == endPattern.getVectorBinding(j)))
                    {
                        if (ele != endPattern.getAtomicNum(j))
                        {
                            atom2Change.add(new IntInt(i,
                                    endPattern.getAtomicNum(j)));

                            break;
                        }
                    }
                }
            }
        }

        //find charges to modify
        int chrg;

        for (i = 0; i < beginPattern.numAtoms(); i++)
        {
            if ((vb = beginPattern.getVectorBinding(i)) != 0)
            {
                chrg = beginPattern.getCharge(i);

                for (j = 0; j < endPattern.numAtoms(); j++)
                {
                    if ((vb == endPattern.getVectorBinding(j)))
                    {
                        if (chrg != endPattern.getCharge(j))
                        {
                            charge2Change.add(new IntInt(i,
                                    endPattern.getCharge(j)));
                        }
                    }
                }
            }
        }

        //find bonds to be modified
        IntIntInt bbb = new IntIntInt();
        int bvb1;
        int bvb2;
        IntIntInt eee = new IntIntInt();
        int evb1;
        int evb2;

        for (i = 0; i < beginPattern.numBonds(); i++)
        {
            beginPattern.getBond(bbb, i);
            bvb1 = beginPattern.getVectorBinding(bbb.ii.i1);
            bvb2 = beginPattern.getVectorBinding(bbb.ii.i2);

            if ((bvb1 == 0) || (bvb2 == 0))
            {
                continue;
            }

            for (j = 0; j < endPattern.numBonds(); j++)
            {
                endPattern.getBond(eee, j);
                evb1 = endPattern.getVectorBinding(eee.ii.i1);
                evb2 = endPattern.getVectorBinding(eee.ii.i2);

                if (((bvb1 == evb1) && (bvb2 == evb2)) ||
                        ((bvb1 == evb2) && (bvb2 == evb1)))
                {
                    if (bbb.i == eee.i)
                    {
                        break;
                    }

                    //nothing to modify if bond orders identical
                    bond2Change.add(new IntIntInt(bbb.ii, eee.i));

                    //          System.out.println("bond2change:"+bgn+" "+ end);
                    break;
                }
            }
        }

        //make sure there is some kind of transform to do here
        if ((atom2Delete.size() == 0) && (charge2Change.size() == 0) &&
                (bond2Change.size() == 0))
        {
            return (false);
        }

        return (true);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
