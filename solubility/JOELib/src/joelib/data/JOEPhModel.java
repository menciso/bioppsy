///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEPhModel.java,v $
//  Purpose:  Model for PH values.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.26 $
//            $Date: 2004/07/25 20:42:58 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.data;

import wsi.ra.tool.PropertyHolder;

import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;
import joelib.smarts.types.SMARTSPatternFVec;

import joelib.util.JHM;
import joelib.util.JOEHelper;


/**
 * Model for the protonation/deprotonation of molecules.
 * The definition file can be defined in the
 * <tt>joelib.data.JOEPhModel.resourceFile</tt> property in the {@link wsi.ra.tool.PropertyHolder}.
 * The {@link wsi.ra.tool.ResourceLoader} loads the <tt>joelib.properties</tt> file for default.
 *
 * <p>
 * Default:<br>
 * joelib.data.JOEPhModel.resourceFile=<a href="http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/joelib/joelib/src/joelib/data/plain/phmodel.txt?rev=HEAD&content-type=text/vnd.viewcvs-markup">joelib/data/plain/phmodel.txt</a>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.26 $, $Date: 2004/07/25 20:42:58 $
 * @see wsi.ra.tool.PropertyHolder
 * @see wsi.ra.tool.ResourceLoader
 * @see joelib.data.JOEChemTransformation
 */
public class JOEPhModel extends JOEGlobalDataBase implements HardCodedKernel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.data.JOEPhModel");
    private static JOEPhModel phmodel;
    private static final String DEFAULT_RESOURCE = "joelib/data/plain/phmodel.txt";
    private static final String VENDOR = "http://joelib.sf.net";
    private static final String RELEASE_VERSION = "$Revision: 1.26 $";
    private static final String RELEASE_DATE = "$Date: 2004/07/25 20:42:58 $";

    //~ Instance fields ////////////////////////////////////////////////////////

    private String releaseDate;
    private String releaseVersion;
    private String vendor;

    /**
     * Match list.
     *  {@link java.util.Vector} of <tt>int[1]</tt>{@link java.util.Vector}
     */
    private Vector matchList;

    /**
     * Seed charges for the Gasteiger-Marsili partial charges.
     *  {@link java.util.Vector} of <tt>SMARTSPatternFVec</tt>
     */
    private Vector seedChargeGM;

    /**
     * SMARTS based transformation patterns.
     *  {@link java.util.Vector} of <tt>JOEChemTransformation</tt>
     */
    private Vector transformation;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEPhModel object
     */
    private JOEPhModel()
    {
        initialized = false;

        Properties prop = PropertyHolder.instance().getProperties();
        resourceFile = prop.getProperty(this.getClass().getName() +
                ".resourceFile", DEFAULT_RESOURCE);

        matchList = new Vector();
        transformation = new Vector();
        seedChargeGM = new Vector();

        vendor = VENDOR;
        releaseVersion = JOEKernel.transformCVStag(RELEASE_VERSION);
        releaseDate = JOEKernel.transformCVStag(RELEASE_DATE);

        JOEKernel.instance().addHardCodedKernel(this);
        init();
        JOEKernel.instance().addSoftCodedKernel(this);

        logger.info("Using pH value correction model: " + resourceFile);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Release date for this expert system (hard coded).
     *
     * @return Release date for this expert system (hard coded).
     */
    public String getReleaseDate()
    {
        return releaseDate;
    }

    /**
     * Release version for this expert system (hard coded).
     *
     * @return Release version for this expert system (hard coded).
     */
    public String getReleaseVersion()
    {
        return releaseVersion;
    }

    /**
     * Vendor for this expert system (hard coded).
     *
     * @return Vendor for this expert system (hard coded).
     */
    public String getVendor()
    {
        return vendor;
    }

//    public void finalize()
//    {
//        for (int k = 0; k < transformation.size(); k++)
//        {
//            Object obj = transformation.get(k);
//            obj = null;
//        }
//
//        for (int m = 0; m < seedChargeGM.size(); m++)
//        {
//            ((SMARTSPatternFVec) seedChargeGM.get(m)).sp = null;
//        }
//    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static synchronized JOEPhModel instance()
    {
        if (phmodel == null)
        {
            phmodel = new JOEPhModel();
        }

        return phmodel;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     */
    public void assignSeedPartialCharge(JOEMol mol)
    {
        if (!initialized)
        {
            init();
        }

        mol.setPartialChargesPerceived();

        if (!mol.automaticPartialCharge())
        {
            return;
        }

        SMARTSPatternFVec sfvec;

        for (int i = 0; i < seedChargeGM.size(); i++)
        {
            sfvec = (SMARTSPatternFVec) seedChargeGM.get(i);

            if (sfvec.sp.match(mol))
            {
                matchList = sfvec.sp.getUMapList();

                int k;
                int[] iarr;

                for (int j = 0; j < matchList.size(); j++)
                {
                    iarr = (int[]) matchList.get(j);

                    for (k = 0; k < iarr.length; k++)
                    {
                        mol.getAtom(iarr[k]).setPartialCharge(sfvec.seedCharge[k]);
                    }
                }
            }
        }
    }

    /**
     * Corrects the molecule for PH.
     * Changes the state of oxygen and nitrogen atoms, if it
     * is allowed to change the formal charges of the atoms, that means
     * if <tt>JOEMol.automaticFormalCharge()</tt> returns <tt>true</tt>
     *
     * @param  mol  Description of the Parameter
     */
    public void correctForPH(JOEMol mol)
    {
        //System.out.print("a_");
        if (!initialized)
        {
            init();
        }

        if (mol.isCorrectedForPH())
        {
            return;
        }

        if (!mol.automaticFormalCharge())
        {
            return;
        }

        mol.setCorrectedForPH();

        // set only the atoms to 0 which where considered by
        // the PH value correction, leave all other unchanged
        // Set O,N to zero
        //    JOEAtom       atom;
        //    AtomIterator  ait    = mol.atomIterator();
        //    while (ait.hasNext())
        //    {
        //      atom = ait.nextAtom();
        //      atom.setFormalCharge(0);
        //    }
        JOEChemTransformation ctsfm;

        //System.out.print("b_");
        for (int i = 0; i < transformation.size(); i++)
        {
            ctsfm = (JOEChemTransformation) transformation.get(i);
            ctsfm.apply(mol);
        }

        JOEAtomTyper.instance().correctAromaticNitrogens(mol);

        /*AtomIterator ait = mol.atomIterator();
        JOEAtom atom;
        System.out.println("PH: atoms=" + mol.numAtoms());
        while (ait.hasNext())
        {
                atom = ait.nextAtom();
                System.out.println("idx: " + atom.getIdx());
        }*/
    }

    /**
     *  Description of the Method
     *
     * @param  buffer  Description of the Parameter
     */
    public void parseLine(String buffer)
    {
        Vector vs = new Vector();

        // of type String
        JOESmartsPattern sp;

        if (buffer.trim().equals("") || (buffer.charAt(0) == '#'))
        {
            return;
        }

        if (JOEHelper.EQn(buffer, "TRANSFORM", 7))
        {
            JHM.tokenize(vs, buffer);

            if ((vs.size() == 0) || (vs.size() < 4))
            {
                return;
            }

            JOEChemTransformation tsfm = new JOEChemTransformation();

            //      System.out.println("vs1: "+(String) vs.get(1));
            //      System.out.println("vs2: "+(String) vs.get(2));
            //      System.out.println("vs3: "+(String) vs.get(3));
            if (!tsfm.init((String) vs.get(1), (String) vs.get(3)))
            {
                tsfm = null;

                return;
            }

            transformation.add(tsfm);
        }
        else if (JOEHelper.EQn(buffer, "SEEDCHARGE", 10))
        {
            JHM.tokenize(vs, buffer);

            if ((vs.size() == 0) || (vs.size() < 2))
            {
                return;
            }

            sp = new JOESmartsPattern();

            if (!sp.init((String) vs.get(1)) ||
                    ((vs.size() - 2) != sp.numAtoms()))
            {
                sp = null;

                return;
            }

            double[] seedCharge = new double[vs.size() - 2];
            int index = 0;

            for (int i = 2; i < vs.size(); i++, index++)
            {
                seedCharge[index] = Double.parseDouble((String) vs.get(i));
            }

            seedChargeGM.add(new SMARTSPatternFVec(sp, seedCharge));
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
