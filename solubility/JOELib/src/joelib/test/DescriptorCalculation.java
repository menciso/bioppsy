///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DescriptorCalculation.java,v $
//  Purpose:  Example for loading molecules and get atom properties.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/08/27 09:30:44 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
///////////////////////////////////////////////////////////////////////////////
package joelib.test;

import wsi.ra.tool.ResourceLoader;
import wsi.ra.tool.StopWatch;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.util.AtomPropertyDescriptors;
import joelib.desc.util.SMARTSDescriptors;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;
import joelib.io.SimpleReader;
import joelib.io.SimpleWriter;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcessException;
import joelib.process.ProcessFactory;
import joelib.process.types.DescVarianceNorm;

import joelib.util.JHM;


/**
 *  Example for loading molecules and get atom properties.
 *
 * @author     wegnerj
 */
public class DescriptorCalculation
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.test.DescriptorCalculation");
    public static final int CONTINUE = 0;
    public static final int STOP = 1;
    public static final int STOP_USAGE = 2;
    private static final double[] RDF_SMOOTHINGFACTORS = new double[]
        {
            1.0, 5.0, 25.0, 150.0
        };
    private static final String NUMERIC = ".numeric";
    private static final String NUMERIC_NORMALIZED = ".numeric.normalized";

    //~ Instance fields ////////////////////////////////////////////////////////

    private AtomPropertyDescriptors calculatorAP = new AtomPropertyDescriptors();
    private IOType inType;
    private IOType outType;
    private SMARTSDescriptors calculatorSMARTS = new SMARTSDescriptors();
    private String inputFile;
    private String outputFile;
    private String smartsFile;
    private String[] smartsPatterns = null;
    private String[] smartsDescriptions = null;
    private String trueDescName = null;
    private boolean calculateAP = true;
    private boolean calculateBinarySMARTS = true;
    private boolean calculateCountSMARTS = false;
    private boolean calculateJCC = true;

    //private boolean calculateMACCS = true;
    private boolean calculateSSKey = true;
    private boolean normalize = false;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
    *  The main program for the TestSmarts class
    *
    * @param  args  The command line arguments
    */
    public static void main(String[] args)
    {
        DescriptorCalculation descs = new DescriptorCalculation();

        int status = descs.parseCommandLine(args);

        if (status == CONTINUE)
        {
            descs.initializeSMARTS();
            descs.calculateNumericDescriptors();
            descs.calculateNormalization();
            descs.calculateNominalDescriptors();
        }
        else if (status == STOP_USAGE)
        {
            descs.usage();
            System.exit(1);
        }
        else if (status == STOP)
        {
            System.exit(0);
        }
    }

    /**
    *  A unit test for JUnit
    *
    * @param  molURL   Description of the Parameter
    * @param  inType   Description of the Parameter
    * @param  outType  Description of the Parameter
    */
    public void calculateNumericDescriptors()
    {
        if ((inType == null) || (inputFile == null) || (outType == null) ||
                (outputFile == null))
        {
            logger.error("Not correctly initialized.");
            logger.error("input type: " + inType.getName());
            logger.error("input file: " + inputFile);
            logger.error("output type: " + outType.getName());
            logger.error("output file: " + outputFile);
            System.exit(1);
        }

        FileInputStream in = null;
        FileOutputStream out = null;

        // get molecule loader/writer
        MoleculeFileType loader = null;
        MoleculeFileType writer = null;

        try
        {
            in = new FileInputStream(inputFile);

            if (!normalize)
            {
                //				if (!calculateBinarySMARTS && !calculateMACCS &&
                //						!calculateSSKey)
                //				{
                if (!calculateBinarySMARTS && !calculateSSKey)
                {
                    out = new FileOutputStream(outputFile);
                }
                else
                {
                    out = new FileOutputStream(generateFileName(inputFile,
                                NUMERIC));
                }
            }
            else
            {
                out = new FileOutputStream(generateFileName(inputFile, NUMERIC));
            }

            loader = JOEFileFormat.getMolReader(in, inType);
            writer = JOEFileFormat.getMolWriter(out, outType);
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
        }

        String[] defDescNames = 
        {
            "Burden_modified_eigenvalues", "Auto_correlation",
            "Global_topological_charge_index", "RDF"
        };

        try
        {
            calculatorAP.setDescriptors2Calculate(defDescNames);
        }
         catch (DescriptorException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        calculatorAP.setRDFSmoothings(RDF_SMOOTHINGFACTORS);

        Enumeration enum = DescriptorHelper.instance().descriptors();
        String tmp;
        Vector jccDescriptorsV = new Vector(100);

        while (enum.hasMoreElements())
        {
            tmp = (String) enum.nextElement();

            jccDescriptorsV.add(tmp);
        }

        String[] jccDescriptors = new String[jccDescriptorsV.size()];

        for (int i = 0; i < jccDescriptors.length; i++)
        {
            jccDescriptors[i] = (String) jccDescriptorsV.get(i);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);
        int molCounter = 0;
        StopWatch watch = new StopWatch();
        logger.info("Calculate JCC, AP and counting SMARTS ...");

        for (;;)
        {
            try
            {
                if (!loader.read(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            //System.out.println("MOLECULE: "+mol.getTitle());
            try
            {
                if (calculateAP)
                {
                    //System.out.print("AP ");
                    calculatorAP.calculate(mol);
                }

                if (smartsPatterns != null)
                {
                    if (calculateCountSMARTS)
                    {
                        calculatorSMARTS.calculate(mol, true);
                    }
                }

                if (calculateJCC)
                {
                    //System.out.print("JCC ");
                    for (int i = 0; i < jccDescriptors.length; i++)
                    {
                        //System.out.print(jccDescriptors[i]);
                        //System.out.println("DESC: "+jccDescriptors[i]);
                        getAndStoreDescriptor(mol, jccDescriptors[i]);
                    }
                }
            }
             catch (DescriptorException e1)
            {
                logger.error(e1.getMessage());
            }

            //System.out.println("\nWRITE ");
            // write molecule to file
            try
            {
                if (!writer.write(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            molCounter++;

            if ((molCounter % 500) == 0)
            {
                logger.info("... " + molCounter +
                    " molecules successful calculated in " +
                    watch.getPassedTime() + " ms.");
            }
        }

        logger.info("... " + molCounter +
            " molecules successful calculated in " + watch.getPassedTime() +
            " ms.");
    }

    /**
    *  Description of the Method
    *
    * @param  args  Description of the Parameter
    * @return       Description of the Return Value
    */
    public int parseCommandLine(String[] args)
    {
        String arg;

        for (int i = 0; i < args.length; i++)
        {
            arg = args[i];

            if (arg.startsWith("--help"))
            {
                return STOP_USAGE;
            }
            else if (arg.startsWith("-?"))
            {
                return STOP_USAGE;
            }
            else if (arg.startsWith("+countSMARTS"))
            {
                logger.info(
                    "PARAMETER: Counting SMARTS values will be calculated.");
                calculateCountSMARTS = true;
            }
            else if (arg.startsWith("-countSMARTS"))
            {
                logger.info(
                    "PARAMETER: Counting SMARTS values will not be calculated.");
                calculateCountSMARTS = false;
            }
            else if (arg.startsWith("+binarySMARTS"))
            {
                logger.info(
                    "PARAMETER: Binary SMARTS values will be calculated.");
                calculateBinarySMARTS = true;
            }
            else if (arg.startsWith("-binarySMARTS"))
            {
                logger.info(
                    "PARAMETER: Binary SMARTS values will not be calculated.");
                calculateBinarySMARTS = false;
            }
            else if (arg.startsWith("+normalize"))
            {
                logger.info(
                    "PARAMETER: Normalize descriptors, except binary SMARTS and fingerprints.");
                normalize = true;
            }
            else if (arg.startsWith("-normalize"))
            {
                logger.info("PARAMETER: Do not normalize descriptors.");
                normalize = false;
            }
            else if (arg.startsWith("+jcc"))
            {
                logger.info(
                    "PARAMETER: Calculate native descriptors in JCompChem.");
                calculateJCC = true;
            }
            else if (arg.startsWith("-jcc"))
            {
                logger.info(
                    "PARAMETER: Does not calculate native descriptors in JCompChem.");
                calculateJCC = false;
            }
            else if (arg.startsWith("+ap"))
            {
                logger.info(
                    "PARAMETER: Calculate atom property descriptors in JCompChem.");
                calculateAP = true;
            }
            else if (arg.startsWith("-ap"))
            {
                logger.info(
                    "PARAMETER: Does not calculate atom property descriptors in JCompChem.");
                calculateAP = false;
            }

            //            else if (arg.startsWith("+MACCS"))
            //            {
            //                logger.info(
            //                    "PARAMETER: Calculate MACCS fingerprint descriptors.");
            //                calculateMACCS = true;
            //            }
            //            else if (arg.startsWith("-MACCS"))
            //            {
            //                logger.info(
            //                    "PARAMETER: Does not calculate MACCS fingerprint descriptors.");
            //                calculateMACCS = false;
            //            }
            else if (arg.equalsIgnoreCase("+SSKey"))
            {
                logger.info(
                    "PARAMETER: Calculate SSKey fingerprint descriptors.");
                calculateSSKey = true;
            }
            else if (arg.equalsIgnoreCase("-SSKey"))
            {
                logger.info(
                    "PARAMETER: Does not calculate SSKey fingerprint descriptors.");
                calculateSSKey = false;
            }
            else if (arg.startsWith("-i"))
            {
                String inTypeS = arg.substring(2);
                inType = IOTypeHolder.instance().getIOType(inTypeS.toUpperCase());

                if (inType == null)
                {
                    logger.error("Input type '" + inTypeS + "' not defined.");

                    return STOP_USAGE;
                }
            }
            else if (arg.startsWith("-t"))
            {
                trueDescName = arg.substring(2);
            }
            else
            {
                if (inputFile == null)
                {
                    inputFile = arg;
                }
                else if (outputFile == null)
                {
                    outputFile = arg;

                    if (outputFile.equalsIgnoreCase(inputFile))
                    {
                        logger.error("'" + inputFile + "' and '" + outputFile +
                            "' are the same file.");

                        return STOP_USAGE;
                    }
                }
                else
                {
                    smartsFile = arg;
                }
            }
        }

        if (inputFile == null)
        {
            logger.error("No input file defined.");

            return STOP_USAGE;
        }

        if (!checkInputType())
        {
            return STOP_USAGE;
        }

        if (!checkOutputType())
        {
            return STOP_USAGE;
        }

        if (trueDescName == null)
        {
            logger.warn("No output descriptor (TRUE VALUE) defined !!!");

            if (normalize)
            {
                logger.warn("Now all descriptors will be normalized !!!");
            }
        }

        if (!normalize)
        {
            logger.warn(
                "If you plan to use the calculated descriptors for creating models,");
            logger.warn("you should normalize the data set.");
        }

        return CONTINUE;
    }

    /**
    *  Description of the Method
    */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("Usage is :\n");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" [options]");
        sb.append(" <input file>");
        sb.append(" <output file>");
        sb.append(" [SMARTS definition file]\n");
        sb.append("Options:\n");
        sb.append(" [-i<inputFormat>]       - Format of the input file\n");
        sb.append(" [-o<outputFormat>]      - Format of the output file\n");
        sb.append(
            " [+jcc]          - Calculate all available native descriptors in JCompChem (default)\n");
        sb.append(
            " [-jcc]          - Does not calculate all available native descriptors in JCompChem\n");
        sb.append(
            " [+ap]           - Calculate all available atom property descriptors in JCompChem (default)\n");
        sb.append(
            " [-ap]           - Does not calculate all available atom property descriptors in JCompChem\n");

        //        sb.append(
        //            " [+MACCS]         - Calculate MACCS fingerprint  (default)\n");
        //        sb.append(" [-MACCS]         - Does not calculate MACCS fingerprint\n");
        sb.append(
            " [+SSKey]         - Calculate SSkey fingerprint  (default)\n");
        sb.append(" [-SSKey]         - Does not calculate SSkey fingerprint\n");
        sb.append(" [+countSMARTS]   - Calculate counting SMARTS\n");
        sb.append(
            " [-countSMARTS]   - Does not calculate counting SMARTS  (default)\n");
        sb.append(" [+binarySMARTS]  - Calculate binary SMARTS (default)\n");
        sb.append(" [-binarySMARTS]  - Does not calculate binary SMARTS\n");
        sb.append(
            " [-t<TRUE_VALUE>] - Defines TRUE VALUE descriptor (will NOT be normalized)\n");
        sb.append(
            " [+normalize] - Normalize descriptor values. SMARTS and fingerprints are ignored\n");
        sb.append(
            " [-normalize] - Do not normalize the descriptor values (default)\n");
        sb.append("\nWarning:");
        sb.append(
            "\nIf you use binary AND counting SMARTS you should NOT use your own names, or only the binary SMARTS results will be stored. That's obvious, ONE name, ONE result.");
        sb.append("\n\nSupported molecule types:");
        sb.append(IOTypeHolder.instance().toString());
        System.out.println(sb.toString());

        System.exit(0);
    }

    private void getAndStoreDescriptor(JOEMol mol, String descName)
    {
        DescResult result = null;

        try
        {
            result = DescriptorHelper.instance().descFromMol(mol, descName);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
        }

        if (result == null)
        {
            logger.error("Descriptor '" + descName +
                "' was not calculated and will not be stored.");
        }
        else
        {
            JOEPairData dp = new JOEPairData();
            dp.setAttribute(descName);
            dp.setValue(result);
            mol.addData(dp);
        }
    }
    
    private void initializeSMARTS()
    {
    	
        Vector smartsPatternsV = null;
        Vector smartsLine = new Vector();

        if (smartsFile != null)
        {
            smartsPatternsV = ResourceLoader.readLines(smartsFile);

            if (smartsPatternsV != null)
            {
                smartsPatterns = new String[smartsPatternsV.size()];
                smartsDescriptions = new String[smartsPatternsV.size()];

                for (int i = 0; i < smartsPatterns.length; i++)
                {
                    JHM.tokenize(smartsLine, (String) smartsPatternsV.get(i),
                        " \t\r\n");

                    if (smartsLine.size() == 0)
                    {
                        smartsPatterns[i] = null;
                    }
                    else
                    {
                        smartsPatterns[i] = (String) smartsLine.get(0);
                    }

                    if (smartsLine.size() > 1)
                    {
                        smartsDescriptions[i] = (String) smartsLine.get(1);
                    }
                }

                try
                {
                    calculatorSMARTS.setSMARTS2Calculate(smartsPatterns,
                        smartsDescriptions);
                }
                 catch (DescriptorException e)
                {
                    logger.error(e.getMessage());
                    System.exit(1);
                }
            }
        }
    }

    /**
     *
     */
    private void calculateNominalDescriptors()
    {
        if (!calculateBinarySMARTS && !calculateSSKey)
        {
            return;
        }

        //        if (!calculateBinarySMARTS && !calculateMACCS && !calculateSSKey)
        //        {
        //            return;
        //        }
        FileInputStream in = null;
        FileOutputStream out = null;

        // get molecule loader/writer
        MoleculeFileType loader = null;
        MoleculeFileType writer = null;

        try
        {
            if (!normalize)
            {
                in = new FileInputStream(generateFileName(inputFile, NUMERIC));
            }
            else
            {
                in = new FileInputStream(generateFileName(inputFile,
                            NUMERIC_NORMALIZED));
            }

            out = new FileOutputStream(outputFile);
            loader = JOEFileFormat.getMolReader(in, inType);
            writer = JOEFileFormat.getMolWriter(out, outType);
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
        }


        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);
        int molCounter = 0;
        StopWatch watch = new StopWatch();
        logger.info("Calculate binary SMARTS and fingerprints...");

        for (;;)
        {
            try
            {
                if (!loader.read(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            try
            {
                //                if (calculateMACCS)
                //                {
                //                    calculatorSMARTS.calculateMACCS(mol);
                //                }
                if (calculateSSKey)
                {
                    calculatorSMARTS.calculateSSKeys(mol);
                }

                if (smartsPatterns != null)
                {
                    if (calculateBinarySMARTS)
                    {
                        calculatorSMARTS.calculate(mol, false);
                    }
                }
            }
             catch (DescriptorException e1)
            {
                logger.error(e1.getMessage());
            }

            // write molecule to file
            try
            {
                if (!writer.write(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            molCounter++;

            if ((molCounter % 500) == 0)
            {
                logger.info("... " + molCounter +
                    " molecules successful calculated in " +
                    watch.getPassedTime() + " ms.");
            }
        }

        logger.info("... " + molCounter +
            " molecules successful calculated in " + watch.getPassedTime() +
            " ms.");
    }

    /**
     *
     */
    private void calculateNormalization()
    {
        if (!normalize)
        {
            return;
        }

        DescVarianceNorm norm = null;

        //		get process
        try
        {
            norm = (DescVarianceNorm) ProcessFactory.instance().getProcess("VarianceNormalization");
        }
         catch (JOEProcessException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        try
        {
            norm.init(inType, generateFileName(inputFile, NUMERIC));

            if (trueDescName != null)
            {
                norm.descriptors2ignore().add(trueDescName);
            }
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        // create simple reader
        FileInputStream input = null;
        FileOutputStream output = null;

        try
        {
            input = new FileInputStream(generateFileName(inputFile, NUMERIC));
            output = new FileOutputStream(generateFileName(inputFile,
                        NUMERIC_NORMALIZED));
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        SimpleReader reader = null;
        SimpleWriter writer = null;

        logger.info("Normalize data set...");

        try
        {
            reader = new SimpleReader(input, inType);
            writer = new SimpleWriter(output, outType);
        }
         catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);
        int molCounter = 0;
        StopWatch watch = new StopWatch();

        for (;;)
        {
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            try
            {
                norm.process(mol, null);
            }
             catch (JOEProcessException ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            try
            {
                writer.writeNext(mol);
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            molCounter++;

            //      System.out.println(mol);
            if ((molCounter % 500) == 0)
            {
                logger.info("... " + molCounter +
                    " molecules successful normalized in " +
                    watch.getPassedTime() + " ms.");
            }
        }

        logger.info("... " + molCounter +
            " molecules successful normalized in " + watch.getPassedTime() +
            " ms.");

        if (trueDescName != null)
        {
            norm.descriptors2ignore().remove(trueDescName);
        }
    }

    private boolean checkInputType()
    {
        if (inType == null)
        {
            inType = IOTypeHolder.instance().filenameToType(inputFile);

            if (inType == null)
            {
                System.out.println("Input type of " + inputFile +
                    " could not be estimated.");

                return false;
            }

            MoleculeFileType mfType = null;

            try
            {
                mfType = JOEFileFormat.getMoleculeFileType(inType);
            }
             catch (MoleculeIOException ex)
            {
                ex.printStackTrace();
            }

            logger.info("Input type set to " + inType.toString() + ": " +
                mfType.inputDescription());
        }

        return true;
    }

    private boolean checkOutputType()
    {
        if (outputFile != null)
        {
            if (outType == null)
            {
                try
                {
                    outType = SimpleWriter.checkGetOutputType(outputFile);
                }
                 catch (IOException e)
                {
                    System.out.println(e.getMessage());

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param inputFile2
     * @param string
     * @return
     */
    private String generateFileName(String _inputFile, String nameExtension)
    {
        int index = _inputFile.lastIndexOf(".");

        if (index == -1)
        {
            return _inputFile + nameExtension;
        }
        else
        {
            return _inputFile.substring(0, index) + nameExtension +
            _inputFile.substring(index);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
