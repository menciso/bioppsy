///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TestDescriptor.java,v $
//  Purpose:  Example for loading molecules and get atom properties.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.28 $
//            $Date: 2004/08/27 09:30:44 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;

import wsi.ra.tool.ResourceLoader;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.algo.BFS;
import joelib.algo.BFSResult;
import joelib.algo.DFS;

import joelib.data.JOEPairData;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorFactory;
import joelib.desc.DescriptorHelper;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.SimpleReader;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;


/**
 *  Example for loading molecules and get atom properties.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.28 $, $Date: 2004/08/27 09:30:44 $
 */
public class TestDescriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////


    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.test.TestDescriptor");
    private static final String delimiter = "----------------------------------------------------";

    //~ Methods ////////////////////////////////////////////////////////////////


    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        TestDescriptor joeDescTest = new TestDescriptor();

        if (args.length != 1)
        {
            joeDescTest.usage();
            System.exit(0);
        }
        else
        {
            //        String molURL = new String("joelib/test/test.mol");
            joeDescTest.test(args[0], IOTypeHolder.instance().getIOType("SDF"),
                IOTypeHolder.instance().getIOType("SDF"));
        }

        System.exit(0);
    }

    /**
     *  A unit test for JUnit
     *
     * @param  molURL   Description of the Parameter
     * @param  inType   Description of the Parameter
     * @param  outType  Description of the Parameter
     */
    public void test(String molURL, IOType inType, IOType outType)
    {
        // get molecules from resource URL
        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(molURL);

        if (bytes == null)
        {
            logger.error("Molecule can't be loaded at \"" + molURL + "\".");
            System.exit(1);
        }

        ByteArrayInputStream sReader = new ByteArrayInputStream(bytes);

        // show all available descriptors
        Enumeration enum = DescriptorHelper.instance().descriptors();
        System.out.println("Available Descriptors:");
        System.out.println(delimiter);

        int index = 1;
        boolean hbdAvailable = false;
        String tmp;

        for (; enum.hasMoreElements(); index++)
        {
            tmp = (String) enum.nextElement();
            System.out.println("" + index + ": " + tmp);

            if (tmp.indexOf("Number_of_HBD_1") != -1)
            {
                hbdAvailable = true;
            }
        }

        //show available atom property descriptors
        Vector atomPropDescs = DescriptorHelper.instance().getAtomPropDescs();
        System.out.println("\nAtom property descriptors:");
        System.out.println(delimiter);

        for (int i = 0; i < atomPropDescs.size(); i++)
        {
            System.out.println(atomPropDescs.get(i));
        }

        //show available native value descriptors
        Vector nativeDescs = DescriptorHelper.instance().getNativeDescs();
        System.out.println("\nNative value descriptors:");
        System.out.println(delimiter);

        for (int i = 0; i < nativeDescs.size(); i++)
        {
            System.out.println(nativeDescs.get(i));
        }

        // get descriptor names
        String[] defDescNames = 
        {
            "Kier_shape_1", "Breadth_first_search", "Depth_first_search",
            "Gasteiger_Marsili", "Number_of_N_atoms"
        };
        String[] descNames = null;

        if (hbdAvailable)
        {
            descNames = new String[defDescNames.length + 1];
            descNames[defDescNames.length] = "Number_of_HBD_1";
        }
        else
        {
            descNames = new String[defDescNames.length];
        }

        for (int i = 0; i < defDescNames.length; i++)
        {
            descNames[i] = defDescNames[i];
        }

        // get descriptor base
        Descriptor[] descriptor = new Descriptor[descNames.length];
        DescDescription[] descDescription = new DescDescription[descNames.length];

        try
        {
            for (int i = 0; i < descriptor.length; i++)
            {
                descriptor[i] = DescriptorFactory.getDescriptor(descNames[i]);

                if (descriptor[i] == null)
                {
                    logger.error("Descriptor " + descNames[i] +
                        " can't be loaded.");
                    System.exit(1);
                }

                System.out.println(delimiter);
                System.out.println("Loaded descriptor (" + descNames[i] +
                    "):\n" + descriptor[i].getDescInfo());
                descDescription[i] = descriptor[i].getDescription();
                System.out.println(descDescription[i].getTextDescription());
            }
        }
         catch (DescriptorException ex)
        {
            //      ex.printStackTrace();
            logger.error(ex.toString());
            System.exit(1);
        }

        // create simple reader
        SimpleReader reader = null;

        try
        {
            reader = new SimpleReader(sReader, inType);
        }
         catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);

        for (;;)
        {
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            //------------------------------------------------------------------
            // calculate descriptors and print results
            //------------------------------------------------------------------
            DescResult[] results = new DescResult[descriptor.length];
            Hashtable[] inits = new Hashtable[descriptor.length];

            // initialize BFS
            JOEAtom startAtom = mol.getAtom(1);
            inits[1] = new Hashtable();
            inits[1].put(BFS.STARTING_ATOM, startAtom);
            inits[2] = new Hashtable();
            inits[2].put(DFS.STARTING_ATOM, startAtom);

            // start calculations
            try
            {
                for (int i = 0; i < descriptor.length; i++)
                {
                    // check if descriptor has already been calculated
                    //          if(mol.hasData("Kier Shape 1"))
                    //          {
                    //            JOEPairData pdTmp = (JOEPairData)mol.getData("Kier Shape 1");
                    //            System.out.println("Kier Shape 1 calculated: "+pdTmp.getValue());
                    //          }
                    descriptor[i].clear();
                    results[i] = descriptor[i].calculate(mol, inits[i]);

                    // has something weird happen
                    if (results[i] == null)
                    {
                        System.exit(1);
                    }

                    // add descriptor data to molecule
                    JOEPairData dp = new JOEPairData();
                    dp.setAttribute(descriptor[i].getDescInfo().getName());
                    dp.setValue(results[i]);
                    mol.addData(dp);
                }
            }
             catch (DescriptorException ex)
            {
                ex.printStackTrace();
            }

            // show SD molecule file
            System.out.println(delimiter);
            System.out.println(mol);

            // use your own data operation on the descriptor result
            // just print result
            System.out.println(delimiter);
            System.out.println("Descriptor result output");

            // print kier 1 result
            //      System.out.println("Kier shape 1 result: " + results[0]);
            // print BFS result
            BFSResult resultBFS = (BFSResult) results[1];

            //      System.out.println("BFS result: " + resultBFS);
            StringBuffer sb = new StringBuffer("BFS result:\n");

            for (int i = 0; i < resultBFS.traverse.length; i++)
            {
                sb.append("atom #");
                sb.append(i + 1);
                sb.append(" visited at ");
                sb.append(resultBFS.traverse[i]);
                sb.append(" position when search starts from atom #" +
                    startAtom.getIdx());
                sb.append(".\n");
            }

            System.out.println(sb.toString());

            //      // print DFS result
            //      DFSResult       resultDFS  = (DFSResult) results[2];
            //      sb = new StringBuffer("DFS result:\n");
            //      for (int i = 0; i < resultDFS.discovered.length; i++)
            //      {
            //        sb.append("atom #");
            //        sb.append(i + 1);
            //        sb.append(" discovered at ");
            //        sb.append(resultDFS.discovered[i]);
            //        sb.append(" finished at ");
            //        sb.append(resultDFS.finished[i]);
            //        sb.append(" position when search starts from atom #" + startAtom.getIdx());
            //        sb.append(".\n");
            //      }
            //      System.out.println(sb.toString());
            //
            //      if (hbdAvailable)
            //      {
            //        IntResult  resultHBA  = (IntResult) results[3];
            //        System.out.println("Number of HBA:" + resultHBA.getInt());
            //      }
        }
    }

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("Usage is : ");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" <SDF file>");
        sb.append(
            "\n\nThis is version $Revision: 1.28 $ ($Date: 2004/08/27 09:30:44 $)\n");

        System.out.println(sb.toString());

        System.exit(0);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
