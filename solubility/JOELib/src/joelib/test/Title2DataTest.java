///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Title2DataTest.java,v $
//  Purpose:  Example for converting molecules.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/02/20 13:12:21 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;


/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Category;

import joelib.ext.ExternalException;
import joelib.ext.ExternalFactory;
import joelib.ext.Title2Data;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.SimpleReader;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcessException;
import joelib.process.ProcessFactory;
import joelib.process.ProcessPipe;
import joelib.process.filter.FilterException;
import joelib.process.filter.FilterFactory;
import joelib.process.filter.HasDataFilter;
import joelib.process.filter.NOTFilter;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 *  Example for converting molecules.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/02/20 13:12:21 $
 */
public class Title2DataTest
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------*/

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "jcompchem.joelib.test.Title2DataTest");

    //~ Instance fields ////////////////////////////////////////////////////////

    private IOType inType;
    private ProcessPipe processPipe;
    private String inputFile;

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * main
     *-------------------------------------------------------------------------*/

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        Title2DataTest convert = new Title2DataTest();

        if (args.length != 3)
        {
            convert.usage();
            System.exit(0);
        }
        else
        {
            if (convert.parseCommandLine(args))
            {
                convert.test();
            }
            else
            {
                System.exit(1);
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  args  Description of the Parameter
     * @return       Description of the Return Value
     */
    public boolean parseCommandLine(String[] args)
    {
        if (args[0].indexOf("-i") == 0)
        {
            String inTypeS = args[0].substring(2);
            inType = IOTypeHolder.instance().getIOType(inTypeS.toUpperCase());

            if (inType == null)
            {
                logger.error("Input type '" + inTypeS + "' not defined.");

                return false;
            }
        }

        inputFile = args[1];

        String titleAttribute = args[2];

        // get filter
        HasDataFilter hasDataFilter = null;
        NOTFilter notFilter = null;

        try
        {
            hasDataFilter = (HasDataFilter) FilterFactory.instance().getFilter("HasDataFilter");
            notFilter = (NOTFilter) FilterFactory.instance().getFilter("NOTFilter");
        }
         catch (FilterException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        //initialize filters
        hasDataFilter.init(titleAttribute);
        notFilter.init(hasDataFilter);

        // get process
        Title2Data title2data = null;

        try
        {
            title2data = (Title2Data) ExternalFactory.instance().getExternal("Title2Data");
            processPipe = (ProcessPipe) ProcessFactory.instance().getProcess("ProcessPipe");
        }
         catch (ExternalException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
         catch (JOEProcessException ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        processPipe.addProcess(title2data, notFilter);

        return true;
    }

    /**
     *  A unit test for JUnit
     */
    public void test()
    {
        // create simple reader
        FileInputStream input = null;

        try
        {
            input = new FileInputStream(inputFile);
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        SimpleReader reader = null;

        try
        {
            reader = new SimpleReader(input, inType);
        }
         catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, inType);

        for (;;)
        {
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            //process data
            try
            {
                processPipe.process(mol, null);
            }
             catch (JOEProcessException ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            System.out.println(mol);
        }
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("Usage is :\n");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" -i<inputFormat>");
        sb.append(" <input file>");
        sb.append(" <title_data_attribute>");
        sb.append(
            "\n\nThis is version $Revision: 1.10 $ ($Date: 2004/02/20 13:12:21 $)\n");

        System.out.println(sb.toString());

        System.exit(0);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
