///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TestSmarts.java,v $
//  Purpose:  'SMiles ARbitrary Target Specification' (SMARTS) example for finding substructures.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.19 $
//            $Date: 2004/07/25 20:43:27 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;

import wsi.ra.tool.ResourceLoader;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.SimpleReader;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;


/**
 *  'SMiles ARbitrary Target Specification' (SMARTS) example for finding
 *  substructures.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.19 $, $Date: 2004/07/25 20:43:27 $
 */
public class TestSmarts
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.test.TestSmarts");

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        TestSmarts joeMolTest = new TestSmarts();

        if (args.length != 2)
        {
            joeMolTest.usage();
            System.exit(0);
        }
        else
        {
            //        String molURL = new String("joelib/test/test.mol");
            joeMolTest.test(args[0], args[1],
                IOTypeHolder.instance().getIOType("SDF"),
                IOTypeHolder.instance().getIOType("SDF"));
        }

        System.exit(0);
    }

    /**
     *  A unit test for JUnit
     *
     * @param  molURL   Description of the Parameter
     * @param  smart    Description of the Parameter
     * @param  inType   Description of the Parameter
     * @param  outType  Description of the Parameter
     */
    public void test(String molURL, String smart, IOType inType, IOType outType)
    {
        // get molecules from resource URL
        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(molURL);

        if (bytes == null)
        {
            logger.error("Molecule can't be loaded at \"" + molURL + "\".");
            System.exit(1);
        }

        ByteArrayInputStream sReader = new ByteArrayInputStream(bytes);

        // initialize SMART pattern
        JOESmartsPattern sp = new JOESmartsPattern();
        System.out.println("... generate atom expression...");
        sp.init(smart);
        System.out.println(sp);

        // create simple reader
        SimpleReader reader = null;

        try
        {
            reader = new SimpleReader(sReader, inType);
        }
         catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);
        System.out.println(" ... try to match: '" + smart + "' ...");

        for (;;)
        {
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            // molecule for debugging purpose
            System.out.println(mol.toString(IOTypeHolder.instance().getIOType("SMILES")));

            for (int i = 0; i < mol.numBonds(); i++)
            {
                System.out.println("bond " + i + " up " +
                    mol.getBond(i).isUp() + " down " + mol.getBond(i).isDown());
            }

            Vector maplist;
            int[] itmp;

            if (sp.match(mol))
            {
                //maplist = sp.getMapList();
                maplist = sp.getUMapList();

                if (maplist.size() > 0)
                {
                    System.out.println("Found pattern in " + mol.getTitle());
                }

                //print out the results
                for (int ii = 0; ii < maplist.size(); ii++)
                {
                    itmp = (int[]) maplist.get(ii);

                    for (int j = 0; j < itmp.length; j++)
                    {
                        System.out.print(itmp[j] + " ");
                    }

                    // show detailed atom properties for atom SMARTS pattern
                    if (itmp.length == 1)
                    {
                        JOEAtom atom = mol.getAtom(itmp[0]);
                        System.out.print(" ImplicitValence:" +
                            atom.getImplicitValence());
                        System.out.print(" Valence:" + atom.getValence());

                        int hatoms = atom.getImplicitValence() -
                            atom.getValence();
                        System.out.print(" Hydrogens:" + hatoms);
                    }

                    System.out.println("");
                }
            }
            else
            {
                System.out.println("Pattern NOT found in " + mol.getTitle());
            }
        }
    }

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("\nUsage is : ");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" <SDF file> <SMARTS pattern>");
        sb.append(
            "\n\nThis is version $Revision: 1.19 $ ($Date: 2004/07/25 20:43:27 $)\n");

        System.out.println(sb.toString());

        System.exit(0);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
