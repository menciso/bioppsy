///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GroupContributionTest.java,v $
//  Purpose:  Value prediction based on a group contribution model.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Stephen Jelfs, Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2004/02/20 13:12:21 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;

import wsi.ra.tool.*;

/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.io.*;

import java.util.*;

import org.apache.log4j.*;

import joelib.algo.contribution.*;

import joelib.data.*;

import joelib.molecule.*;

import joelib.smiles.*;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Test for contribution lists for different models (e.g. logP, MR, PSA).
 *
 * @author  Stephen Jelfs
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/02/20 13:12:21 $
 * @see PropertyHolder
 * @see ResourceLoader
 */
public class GroupContributionTest
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.test.GroupContributionTest");
    static GCPredictor predictor = new GCPredictor();

    //~ Methods ////////////////////////////////////////////////////////////////

    // analyse/predict values for a given dataset (smiles first)
    public static void analyseDataset(String fileName,
        GroupContributions _contrib)
    {
        // smiles parser
        JOESmilesParser smilesParser = new JOESmilesParser();

        try
        {
            // open reader
            byte[] bytes = ResourceLoader.instance()
                                         .getBytesFromResourceLocation(fileName);

            if (bytes == null)
            {
                logger.error("Experimental values can't be loaded from \"" +
                    fileName + "\".");
                System.exit(1);
            }

            ByteArrayInputStream sReader = new ByteArrayInputStream(bytes);

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                        sReader));

            // read lines
            String line;

            while ((line = reader.readLine()) != null)
            {
                // tokenize line
                StringTokenizer tokens = new StringTokenizer(line);

                if (!tokens.hasMoreTokens())
                {
                    continue;
                }

                // build molecule from smiles
                JOEMol molecule = new JOEMol();
                smilesParser.smiToMol(molecule, tokens.nextToken());

                // print prediction
                System.out.println("Prediction: " +
                    (float) predictor.predict(_contrib, molecule) +
                    " Original:" + line + "\n");
            }

            // close reader
            reader.close();
        }
         catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // test property predictions
    public static void main(String[] args)
    {
        GroupContributions contrib = null;

        // FIRST LETS TRY SOME KNOWN PREDICTIONS FROM THE LITERATURE
        // predict polar surface area
        System.out.println("Polar Surface Area(PSA):");
        contrib = JOEGroupContribution.instance().getGroupContributions("PSA");

        // analyse molecules
        analyseDataset("joelib/test/psa.txt", contrib);

        // predict molar refractivity
        System.out.println("Molar Refractivity (MR):");
        contrib = JOEGroupContribution.instance().getGroupContributions("MR");

        // analyse molecules
        analyseDataset("joelib/test/logP.mr.txt", contrib);

        // predict hydrophobicity
        System.out.println("Hydrophobicity (logP):");
        contrib = JOEGroupContribution.instance().getGroupContributions("LogP");

        // analyse molecules
        analyseDataset("joelib/test/logP.mr.txt", contrib);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
