///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TestMolecule.java,v $
//  Purpose:  Example for loading molecules and get atom properties.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.32 $
//            $Date: 2004/07/25 20:43:27 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;

import wsi.ra.tool.ResourceLoader;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.SimpleReader;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.NbrAtomIterator;


/**
 *  Example for loading molecules and get atom properties.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.32 $, $Date: 2004/07/25 20:43:27 $
 */
public class TestMolecule
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.test.TestMolecule");
    private static final boolean SHOW_ATOM_INFOS = true;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        TestMolecule joeMolTest = new TestMolecule();

        if (args.length != 1)
        {
            joeMolTest.usage();
            System.exit(0);
        }
        else
        {
            //        String molURL = new String("joelib/test/test.mol");
            joeMolTest.test(args[0], IOTypeHolder.instance().getIOType("SDF"),
                IOTypeHolder.instance().getIOType("SDF"));
        }
    }

    /**
     *  A unit test for JUnit
     *
     * @param  molURL   Description of the Parameter
     * @param  inType   Description of the Parameter
     * @param  outType  Description of the Parameter
     */
    public void test(String molURL, IOType inType, IOType outType)
    {
        System.out.println("Supported molecule types:");
        System.out.println(IOTypeHolder.instance().toString());

        // get molecules from resource URL
        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(molURL);

        if (bytes == null)
        {
            logger.error("Molecule can't be loaded at \"" + molURL + "\".");
            System.exit(1);
        }

        ByteArrayInputStream sReader = new ByteArrayInputStream(bytes);

        // create simple reader
        SimpleReader reader = null;

        try
        {
            reader = new SimpleReader(sReader, inType);
        }
         catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);

        for (;;)
        {
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            System.out.println("Original molecule:");
            System.out.println(mol);

            System.out.println("Add H atoms:");
            mol.addHydrogens(false, true, true);
            System.out.println(mol);

            //			System.out.println("Delete H atoms:");
            //			mol.deleteHydrogens();
            //			System.out.println(mol);

            /*        NativeValueIterator nvit=mol.nativeValueIterator();
                    NativeValue nVal=null;
                    while(nvit.hasNext())
                    {
                            nVal=nvit.nextNativeValue();
                            System.out.println(nvit.actualName()+" is "+nVal.getDoubleNV());
                    }*/

            // atom iterator part
            JOEAtom atom;
            JOEAtom nbr;
            AtomIterator ait = mol.atomIterator();
            NbrAtomIterator nait;
            boolean firstAromAtomFound = false;

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                if (SHOW_ATOM_INFOS)
                {
                    //atom.isAromatic())
                    //          if (!firstAromAtomFound)
                    //          {
                    //            System.out.println("Show aromatic atoms:");
                    //            firstAromAtomFound = true;
                    //          }
                    System.out.print("atom idx:" + atom.getIdx());
                    System.out.print(", atomic number:" + atom.getAtomicNum());
                    System.out.print(", element symbol:" +
                        JOEElementTable.instance().getSymbol(atom.getAtomicNum()));
                    System.out.print(", aromatic flag:" + atom.isAromatic());
                    System.out.print(", atom vector:" + atom.getVector());
                    System.out.print(", hybridisation:" + atom.getHyb());
                    System.out.print(", implicit valence:" +
                        atom.getImplicitValence());
                    System.out.print(", charge:" + atom.getFormalCharge());
                    System.out.print(", partial charge:" +
                        atom.getPartialCharge());
                    System.out.print(", valence:" + atom.getValence());
                    System.out.print(", ext Electrons:" +
                        JOEElementTable.instance().getExteriorElectrons(atom.getAtomicNum()));
                    System.out.print(", pauling electronegativity:" +
                        atom.getENPauling());
                    System.out.println(", free electrons:" +
                        atom.getFreeElectrons());

                    nait = atom.nbrAtomIterator();

                    while (nait.hasNext())
                    {
                        nbr = nait.nextNbrAtom();
                        System.out.println("  atom #" + atom.getIdx() +
                            " is attached to atom #" + nbr.getIdx() +
                            " with bond of order " + nait.actualBond().getBO());
                    }
                }
            }

            if (!firstAromAtomFound)
            {
                System.out.println("Molecule has no aromatic atoms.");
            }

            System.out.println("Molecule weight:" + mol.getMolWt());
        }
    }

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("Usage is : ");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" <SDF file>");
        sb.append(
            "\n\nThis is version $Revision: 1.32 $ ($Date: 2004/07/25 20:43:27 $)\n");

        System.out.println(sb.toString());

        System.exit(0);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
