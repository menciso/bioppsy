///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TestSMILES.java,v $
//  Purpose:  Example for generating a molecule using SMILES.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/02/20 13:12:21 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;


/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import java.io.FileOutputStream;

import org.apache.log4j.Category;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.SimpleWriter;

import joelib.molecule.JOEMol;

import joelib.smiles.JOESmilesParser;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 * Example for generating a molecule using SMILES.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/02/20 13:12:21 $
 */
public class TestSMILES
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------*/

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.test.TestSMILES");

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/
    public TestSMILES()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * main
     *-------------------------------------------------------------------------       */

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        TestSMILES joeMolTest = new TestSMILES();

        if (args.length != 2)
        {
            joeMolTest.usage();
            System.exit(0);
        }
        else
        {
            JOEMol mol = joeMolTest.test(args[0],
                    IOTypeHolder.instance().getIOType("SDF"),
                    IOTypeHolder.instance().getIOType("SDF"));
            joeMolTest.write(mol, args[1],
                IOTypeHolder.instance().getIOType("SDF"));
        }

        System.exit(0);
    }

    /**
     *  A unit test for JUnit
     *
     * @param  molURL   Description of the Parameter
     * @param  inType   Description of the Parameter
     * @param  outType  Description of the Parameter
     */
    public JOEMol test(String smiles, IOType inType, IOType outType)
    {
        JOEMol mol = new JOEMol(inType, outType);

        System.out.println("Generate molecule from \"" + smiles + "\"");

        // create molecule from SMILES string
        if (!JOESmilesParser.smiToMol(mol, smiles, "Name:" + smiles))
        {
            System.err.println("Molecule could not be generated from \"" +
                smiles + "\".");
        }

        //        JOEMol mol1 = new JOEMol(IOTypeHolder.instance().getIOType("SMILES"),IOTypeHolder.instance().getIOType("SMILES"));
        //        JOEMol mol2 = new JOEMol(IOTypeHolder.instance().getIOType("SMILES"),IOTypeHolder.instance().getIOType("SMILES"));
        //        joelib.smiles.JOESmilesParser parser = new joelib.smiles.JOESmilesParser();
        //        parser.smiToMol(mol1,"C0c1ccc(Cl)cc1Cl");
        //        parser.smiToMol(mol2,"C0c1ccc(Cl)cc1C");
        //
        //        System.out.print(mol1);
        //        System.out.print(mol2); // ERROR: terminal 'C' is converted to a Cl
        System.out.println(mol);
        mol.addHydrogens();
        System.out.println("Add hydrogens:");
        System.out.println(mol);

        return mol;
    }

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("\nUsage is : ");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append("<SMILES pattern> <SDF file>");
        sb.append(
            "\n\nThis is version $Revision: 1.9 $ ($Date: 2004/02/20 13:12:21 $)\n");

        System.out.println(sb.toString());

        System.exit(0);
    }

    public void write(JOEMol mol, String molFile, IOType outType)
    {
        // create simple writer
        FileOutputStream fos = null;

        try
        {
            fos = new FileOutputStream(molFile);
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }

        // write molecule to file
        try
        {
            SimpleWriter writer = new SimpleWriter(fos, outType);

            if (!writer.writeNext(mol))
            {
                System.err.println("Error writing SMILES.");
            }
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
