///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TestSSSR.java,v $
//  Purpose:  Example for getting the 'Smallest Set of Smallest Rings' (SSSR).
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2004/02/20 13:12:21 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;

import wsi.ra.tool.ResourceLoader;

/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.SimpleReader;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.ring.JOERing;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 *  Example for getting the 'Smallest Set of Smallest Rings' (SSSR).
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/02/20 13:12:21 $
 */
public class TestSSSR
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------*/

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.test.TestSSSR");

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * main
     *-------------------------------------------------------------------------       */

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        TestSSSR joeMolTest = new TestSSSR();

        if (args.length != 1)
        {
            joeMolTest.usage();
            System.exit(0);
        }
        else
        {
            //        String molURL = new String("joelib/test/test.mol");
            joeMolTest.test(args[0], IOTypeHolder.instance().getIOType("SDF"),
                IOTypeHolder.instance().getIOType("SDF"));
        }

        System.exit(0);
    }

    /**
     *  A unit test for JUnit
     *
     * @param  molURL   Description of the Parameter
     * @param  inType   Description of the Parameter
     * @param  outType  Description of the Parameter
     */
    public void test(String molURL, IOType inType, IOType outType)
    {
        // get molecules from resource URL
        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(molURL);

        if (bytes == null)
        {
            logger.error("Molecule can't be loaded at \"" + molURL + "\".");
            System.exit(1);
        }

        ByteArrayInputStream sReader = new ByteArrayInputStream(bytes);

        // create simple reader
        SimpleReader reader = null;

        try
        {
            reader = new SimpleReader(sReader, inType);
        }
         catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);

        for (;;)
        {
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            System.out.println(mol);

            // JOERing-Vector
            Vector sssRings = mol.getSSSR();

            JOERing ring;
            int[] tmp;
            JOEAtom atom;
            JOEElementTable eTable = JOEElementTable.instance();

            for (int i = 0; i < sssRings.size(); i++)
            {
                ring = (JOERing) sssRings.get(i);
                tmp = ring.getAtoms();

                // show numbers
                System.out.print("ring #" + (i + 1) + ":");

                for (int j = 0; j < tmp.length; j++)
                {
                    System.out.print(" " + tmp[j]);
                }

                System.out.println("");

                // show elements
                System.out.print("ring #" + (i + 1) + ":");

                for (int j = 0; j < tmp.length; j++)
                {
                    atom = mol.getAtom(tmp[j]);
                    System.out.print(" " +
                        eTable.getSymbol(atom.getAtomicNum()));
                }

                System.out.println("\n");
            }
        }
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("\nUsage is : ");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" <SDF file> <SMARTS pattern>");
        sb.append(
            "\n\nThis is version $Revision: 1.12 $ ($Date: 2004/02/20 13:12:21 $)\n");

        System.out.println(sb.toString());

        System.exit(0);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
