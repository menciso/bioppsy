///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TestPatty.java,v $
//  Purpose:  Example for the pattern assignment of SMARTS pattern.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/02/20 13:12:21 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;

import wsi.ra.tool.ResourceLoader;

/*==========================================================================*
 * IMPORTS
 *==========================================================================     */
import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.SimpleReader;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.smarts.Patty;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================     */

/**
 *  Example for the pattern assignment of SMARTS pattern.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/02/20 13:12:21 $
 */
public class TestPatty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------     */

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.test.TestPatty");

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------     */
    public TestPatty()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * main
     *-------------------------------------------------------------------------     */

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        TestPatty joeMolTest = new TestPatty();

        if (args.length != 2)
        {
            joeMolTest.usage();
            System.exit(0);
        }
        else
        {
            //        String pattyFile = new String("joelib/test/patty.txt");
            //        String molURL = new String("joelib/test/multiple.mol");
            joeMolTest.test(args[0], args[1],
                IOTypeHolder.instance().getIOType("SDF"),
                IOTypeHolder.instance().getIOType("SDF"));
        }

        System.exit(0);
    }

    /**
     *  A unit test for JUnit
     *
     * @param  molURL     Description of the Parameter
     * @param  pattyFile  Description of the Parameter
     * @param  inType     Description of the Parameter
     * @param  outType    Description of the Parameter
     */
    public void test(String molURL, String pattyFile, IOType inType,
        IOType outType)
    {
        // get molecules from resource URL
        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(molURL);

        if (bytes == null)
        {
            logger.error("Molecule can't be loaded at \"" + molURL + "\".");
            System.exit(1);
        }

        ByteArrayInputStream sReader = new ByteArrayInputStream(bytes);

        // get patty rules
        Patty patty = new Patty();

        if (!patty.readRules(pattyFile))
        {
            System.exit(1);
        }

        // create simple reader
        SimpleReader reader = null;

        try
        {
            reader = new SimpleReader(sReader, inType);
        }
         catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);
        int[] pattyTypes;

        for (;;)
        {
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            System.out.println(mol);

            System.out.println("patty assignment:");
            pattyTypes = patty.assignTypes(mol);

            int i1;

            for (int i = 0; i < mol.numAtoms(); i++)
            {
                i1 = i + 1;

                JOEAtom atom = mol.getAtom(i1);
                String typeString = patty.getStringFromType(pattyTypes[i]);

                if (typeString == null)
                {
                    System.out.println("atom #" + i1 + " " +
                        JOEElementTable.instance().getSymbol(atom.getAtomicNum()));
                }
                else
                {
                    System.out.println("atom #" + i1 + " " +
                        JOEElementTable.instance().getSymbol(atom.getAtomicNum()) +
                        " is of type " + typeString + "");
                }
            }
        }
    }

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("\nUsage is : ");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" <SDF file> <patty file>");
        sb.append(
            "\n\nThis is version $Revision: 1.13 $ ($Date: 2004/02/20 13:12:21 $)\n");

        System.out.println(sb.toString());

        System.exit(0);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
