///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Convert.java,v $
//  Purpose:  Example for converting molecules.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.36 $
//            $Date: 2004/07/25 20:43:27 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;

import wsi.ra.tool.PropertyHolder;
import wsi.ra.tool.StopWatch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeCallback;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;
import joelib.io.PropertyWriter;
import joelib.io.SimpleReader;
import joelib.io.SimpleWriter;
import joelib.io.types.cml.CMLSequentialSAXReader;

import joelib.molecule.JOEMol;

import joelib.process.filter.HasAllDataFilter;
import joelib.process.filter.NativeValueFilter;
import joelib.process.filter.SMARTSFilter;

import joelib.util.JOEHelper;


/**
 * Example for converting molecules and calculating descriptors.
 *
 * <p>
 * <blockquote><pre>
 * Usage:
 *java -cp . joelib.test.Convert [options] &lt;input file> [&lt;output file>]
 *
 *Options:
 * [-i&lt;inputFormat>]       - Format of the input file
 * [-o&lt;outputFormat>]      - Format of the output file
 * [-h]                    - Removes all hydrogens from molecule
 * [+h]                    - Adds hydrogens to molecule
 * [+p]                    - Adds only polar hydrogens (+h implicit)
 * [-e]                    - Converts only non-empty molecules
 * [-d]                    - Remove all descriptors from the molecule
 * [+d]                    - Adds all available descriptors to the molecule
 * [+v]                    - Switch verbosity ON
 * [+snd]                  - Shows all available native value descriptors
 * [+sad]                  - Shows all available atom property descriptors
 * [+sall]                 - Shows all available descriptors
 * [-salt]                 - Strip salts and gets only largest contigous fragment
 * [+split&lt;SIZE>]       - Generated splitted output file of SIZE
 * [+x&lt;descriptor name>]   - Converts only molecules where &lt;descriptor name> exists
 * [-r&lt;skip  desc. rule>]  - Skips molecules, if rule fits
 * [+r&lt;conv. desc. rule>]  - Converts only molecules where rule fits
 * [+f&lt;lineStructure>]     - Required if you use FLAT output format which other input format
 * [+s&lt;lineStructure>]     - Can be used for an alternate SMILES entry line structure
 * [-m&lt;SMARTS rule>]       - Skips molecules, if SMARTS rule fits
 * [+m&lt;SMARTS rule>]       - Converts only molecules where SMARTS rule fits
 * [-um&lt;SMARTS rule>]      - Skips molecules, if SMARTS rule fits
 * [+um&lt;SMARTS rule>]      - Converts only molecules where SMARTS rule fits
 * [-?][--help]            - Shows this message
 *
 *If no output file is defined, all molecules will be written to stdout.
 *
 *Filter rules have the form:
 *&lt;native value descriptor>&lt;relation>&lt;value>
 *where &lt;relation> is &lt;, &lt;=, ==, >, >= or !=
 *Example:
 *"+rNumber_of_halogen_atoms==2"
 *
 *SMARTS filter rules have the form:
 *&lt;SMARTS pattern>&lt;relation>&lt;value>
 *where &lt;relation> is &lt;, &lt;=, ==, >, >= or !=
 *Example:
 *"+umaNC=O==1"
 *Converts all molecules, where the molecule contains ONE NC=O group connected to an aromatic atom (aNC=O).
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.36 $, $Date: 2004/07/25 20:43:27 $
 * @see joelib.JOE
 * @see joelib.process.filter.NativeValueFilter
 * @cite smarts
 */
public class Convert implements MoleculeCallback
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance("joelib.test.Convert");
    public static final int CONTINUE = 0;
    public static final int STOP = 1;
    public static final int STOP_USAGE = 2;
    private static IOType verboseType = IOTypeHolder.instance().getIOType("SMILES");

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public PrintStream generatedOutput;
    private FileOutputStream out = null;
    private HasAllDataFilter allDescFilter = null;
    private IOType inType;
    private IOType outType;
    private MoleculeFileType writer = null;
    private StopWatch watch = new StopWatch();
    private String inputFile;
    private String outputFile;
    private Vector exists = null;
    private Vector rules = null;
    private Vector smartsRules = null;
    private boolean addDescriptors = false;
    private boolean addHydrogens = false;
    private boolean polarOnly = false;
    private boolean removeDescriptors = false;
    private boolean removeHydrogens = false;
    private boolean skipEmpty = false;

    // not implemented
    private boolean splitFile = false;
    private boolean stripSalt = false;
    private boolean usePHvalueCorrection = true;
    private boolean verbose = false;
    private int actualSplitNumber = 1;
    private int molCounter = 0;
    private int splitMoleculeNumber = 1;
    private int splitSize = 1;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  A unit test for JUnit
     *
     */
    public void convert()
    {
        FileInputStream in = null;

        // get molecule loader/writer
        MoleculeFileType loader = null;

        try
        {
            //      in = new BufferedInputStream(new FileInputStream(inputFile));
            in = new FileInputStream(inputFile);
        }
         catch (FileNotFoundException ex)
        {
            logger.error("Can not find input file: " + inputFile);

            return;
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
        }

        if (outputFile != null)
        {
            try
            {
                if (splitFile)
                {
                    //      out = new BufferedOutputStream( new FileOutputStream(outputFile));
                    out = new FileOutputStream(getSplitName(outputFile,
                                splitMoleculeNumber - 1));
                    actualSplitNumber = 1;
                }
                else
                {
                    //      out = new BufferedOutputStream( new FileOutputStream(outputFile));
                    out = new FileOutputStream(outputFile);
                }

                writer = JOEFileFormat.getMolWriter(out, outType);
            }
             catch (FileNotFoundException ex)
            {
                logger.error("Can not create output file: " + outputFile);

                return;
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
            }

            if (!writer.writeable())
            {
                logger.error(outType.getRepresentation() +
                    " is not writeable.");
                logger.error("You're invited to write one !;-)");

                return;
            }
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);
        boolean success = true;
        watch.resetTime();
        logger.info("Start file conversion ...");

        if (inType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            CMLSequentialSAXReader cmlReader = new CMLSequentialSAXReader();

            try
            {
                cmlReader.initReader(in, this);
            }
             catch (FileNotFoundException ex)
            {
                logger.error("Can not find input file: " + inputFile);

                return;
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
            }

            try
            {
                cmlReader.read(mol);
            }
             catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
        else
        {
            if (inType.equals(IOTypeHolder.instance().getIOType("ZIP")))
            {
                // force slower preparser for CML files !!!
                PropertyHolder.instance().getProperties().setProperty("joelib.io.types.ChemicalMarkupLanguage.useSlowerMemorySavingPreparser",
                    "true");
                logger.info(
                    "Activating slower preparser for CML files in ZIP files.");
            }

            try
            {
                loader = JOEFileFormat.getMolReader(in, inType);

                if (!loader.readable())
                {
                    logger.error(inType.getRepresentation() +
                        " is not readable.");
                    logger.error("You're invited to write one !;-)");

                    return;
                }
            }
             catch (FileNotFoundException ex)
            {
                logger.error("Can not find input file: " + inputFile);

                return;
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
            }

            for (;;)
            {
                mol.clear();

                try
                {
                    success = loader.read(mol);

                    if (!success)
                    {
                        break;
                    }

                    //                else
                    //                {
                    //                	System.out.println("LOADED:");
                    //					System.out.println(mol);
                    //                }
                    if (!handleSingle(mol))
                    {
                        break;
                    }
                }
                 catch (IOException ex)
                {
                    ex.printStackTrace();

                    return;
                }
                 catch (MoleculeIOException ex)
                {
                    ex.printStackTrace();
                    molCounter++;
                    logger.info("Molecule entry (#" + molCounter +
                        ") was skipped: " + mol.getTitle());

                    continue;
                }
            }
        }

        try
        {
            if (!inType.equals(IOTypeHolder.instance().getIOType("CML")))
            {
                if (loader != null)
                {
                    loader.closeReader();
                }
            }

            if (writer != null)
            {
                writer.closeWriter();
            }
        }
         catch (IOException e)
        {
            e.printStackTrace();

            return;
        }

        logger.info("... " + molCounter +
            " molecules successful converted in " + watch.getPassedTime() +
            " ms.");
    }

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        Convert convert = new Convert();

        int status = convert.parseCommandLine(args);

        //for (int i = 0; i < args.length; i++)
        //{
        //	System.out.println("ARG: "+args[i]);
        //}
        if (status == CONTINUE)
        {
            convert.convert();
        }
        else if (status == STOP_USAGE)
        {
            convert.usage();
            System.exit(1);
        }
        else if (status == STOP)
        {
            System.exit(0);
        }
    }

    public boolean handleMolecule(JOEMol mol)
    {
        try
        {
            handleSingle(mol);
        }
         catch (IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace();
        }
         catch (MoleculeIOException ex)
        {
            logger.error(ex.getMessage());
            molCounter++;
            logger.info("Molecule entry (#" + molCounter + ") was skipped: " +
                mol.getTitle());
        }

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  args  Description of the Parameter
     * @return       Description of the Return Value
     */
    public int parseCommandLine(String[] args)
    {
        // get default properties
        Properties prop = PropertyHolder.instance().getProperties();

        String arg;

        for (int i = 0; i < args.length; i++)
        {
            arg = args[i];

            if (arg.startsWith("--help"))
            {
                return STOP_USAGE;
            }
            else if (arg.startsWith("-?"))
            {
                return STOP_USAGE;
            }
            else if (arg.startsWith("-h"))
            {
                removeHydrogens = true;
            }
            else if (arg.startsWith("+h"))
            {
                addHydrogens = true;
            }
            else if (arg.startsWith("-pH"))
            {
                usePHvalueCorrection = false;
            }
            else if (arg.startsWith("+pH"))
            {
                usePHvalueCorrection = true;
            }
            else if (arg.startsWith("+p"))
            {
                addHydrogens = true;
                polarOnly = true;
            }
            else if (arg.startsWith("+v"))
            {
                verbose = true;
            }
            else if (arg.startsWith("-v"))
            {
                verbose = false;
            }
            else if (arg.startsWith("+split"))
            {
                splitFile = true;

                String size = arg.substring(6);

                //System.out.println("splitsize: "+size);
                if (size.trim().length() != 0)
                {
                    splitSize = Integer.parseInt(size);
                }

                splitMoleculeNumber = 1;
            }
            else if (arg.startsWith("-salt"))
            {
                stripSalt = true;
            }
            else if (arg.startsWith("+snd"))
            {
                Vector nativeDescs = DescriptorHelper.instance().getNativeDescs();
                StringBuffer sb = new StringBuffer(nativeDescs.size() * 20);
                sb.append("\nNative value descriptors:\n");

                int s = nativeDescs.size();

                for (int ii = 0; ii < s; ii++)
                {
                    sb.append(nativeDescs.get(ii));
                    sb.append('\n');
                }

                System.out.println(sb.toString());

                return STOP;
            }
            else if (arg.startsWith("+sad"))
            {
                Vector atomPropDescs = DescriptorHelper.instance()
                                                       .getAtomPropDescs();
                StringBuffer sb = new StringBuffer(atomPropDescs.size() * 20);
                sb.append("\nAtom property descriptors:\n");

                int s = atomPropDescs.size();

                for (int ii = 0; ii < s; ii++)
                {
                    sb.append(atomPropDescs.get(ii));
                    sb.append('\n');
                }

                System.out.println(sb.toString());

                return STOP;
            }
            else if (arg.startsWith("+sall"))
            {
                Enumeration enum = DescriptorHelper.instance().descriptors();
                StringBuffer sb = new StringBuffer(1000);
                sb.append("\nDescriptors:\n");

                String tmp;

                while (enum.hasMoreElements())
                {
                    tmp = (String) enum.nextElement();
                    sb.append(tmp);
                    sb.append('\n');
                }

                System.out.println(sb.toString());

                return STOP;
            }
            else if (arg.startsWith("-e"))
            {
                skipEmpty = true;
            }
            else if (arg.startsWith("-d"))
            {
                removeDescriptors = true;
            }
            else if (arg.startsWith("+d"))
            {
                addDescriptors = true;
            }
            else if (arg.startsWith("-i"))
            {
                String inTypeS = arg.substring(2);
                inType = IOTypeHolder.instance().getIOType(inTypeS.toUpperCase());

                if (inType == null)
                {
                    logger.error("Input type '" + inTypeS + "' not defined.");

                    return STOP_USAGE;
                }
            }
            else if (arg.startsWith("-o"))
            {
                String outTypeS = arg.substring(2);
                outType = IOTypeHolder.instance().getIOType(outTypeS.toUpperCase());

                if (outType == null)
                {
                    logger.error("Output type '" + outTypeS + "' not defined.");

                    return STOP_USAGE;
                }
            }
            else if (arg.startsWith("+x"))
            {
                String exist = arg.substring(2);

                if (exists == null)
                {
                    exists = new Vector();
                }

                exists.add(exist);
            }
            else if (arg.startsWith("-r"))
            {
                String rule = arg.substring(2);

                if (rules == null)
                {
                    rules = new Vector();
                }

                NativeValueFilter filter = new NativeValueFilter();

                if (filter.fromString(rule))
                {
                    rules.add(filter);
                }
                else
                {
                    logger.warn("Rule '" + rule +
                        "' was not added to filter rules.");
                }
            }
            else if (arg.startsWith("+r"))
            {
                String rule = arg.substring(2);

                if (rules == null)
                {
                    rules = new Vector();
                }

                NativeValueFilter filter = new NativeValueFilter();

                if (filter.fromString(rule))
                {
                    filter.invertRelation();
                    rules.add(filter);
                }
                else
                {
                    logger.warn("Rule '" + rule +
                        "' was not added to filter rules.");
                }
            }
            else if (arg.startsWith("+f"))
            {
                String lineStructure = arg.substring(2);
                prop.setProperty("joelib.io.types.Flat.lineStructure",
                    lineStructure);
            }
            else if (arg.startsWith("+s"))
            {
                String lineStructure = arg.substring(2);
                prop.setProperty("joelib.io.types.Smiles.lineStructure",
                    lineStructure);
            }
            else if (arg.startsWith("-m"))
            {
                String smartsRule = arg.substring(2);

                if (smartsRules == null)
                {
                    smartsRules = new Vector();
                }

                SMARTSFilter filter = new SMARTSFilter();

                if (filter.fromString(smartsRule, false))
                {
                    smartsRules.add(filter);
                }
                else
                {
                    logger.warn("SMARTS Rule '" + smartsRule +
                        "' was not added to rules.");
                }
            }
            else if (arg.startsWith("+m"))
            {
                String smartsRule = arg.substring(2);

                if (smartsRules == null)
                {
                    smartsRules = new Vector();
                }

                SMARTSFilter filter = new SMARTSFilter();

                if (filter.fromString(smartsRule, false))
                {
                    filter.invertRelation();
                    smartsRules.add(filter);
                }
                else
                {
                    logger.warn("SMARTS Rule '" + smartsRule +
                        "' was not added to rules.");
                }
            }
            else if (arg.startsWith("-um"))
            {
                String smartsRule = arg.substring(3);

                if (smartsRules == null)
                {
                    smartsRules = new Vector();
                }

                SMARTSFilter filter = new SMARTSFilter();

                if (filter.fromString(smartsRule, true))
                {
                    smartsRules.add(filter);
                }
                else
                {
                    logger.warn("Unique SMARTS Rule '" + smartsRule +
                        "' was not added to rules.");
                }
            }
            else if (arg.startsWith("+um"))
            {
                String smartsRule = arg.substring(3);

                if (smartsRules == null)
                {
                    smartsRules = new Vector();
                }

                SMARTSFilter filter = new SMARTSFilter();

                if (filter.fromString(smartsRule, true))
                {
                    filter.invertRelation();
                    smartsRules.add(filter);
                }
                else
                {
                    logger.warn("Unique SMARTS Rule '" + smartsRule +
                        "' was not added to rules.");
                }
            }
            else
            {
                if (inputFile == null)
                {
                    inputFile = arg;
                }
                else
                {
                    outputFile = arg;

                    if (outputFile.equalsIgnoreCase(inputFile))
                    {
                        logger.error("'" + inputFile + "' and '" + outputFile +
                            "' are the same file.");

                        return STOP_USAGE;
                    }
                }
            }
        }

        if (inputFile == null)
        {
            logger.error("No input file defined.");

            return STOP_USAGE;
        }

        if (!checkInputType())
        {
            return STOP_USAGE;
        }

        if (!checkOutputType())
        {
            return STOP_USAGE;
        }

        if (exists != null)
        {
            allDescFilter = new HasAllDataFilter(exists);
        }

        if (rules != null)
        {
            logger.info("Using skip rules:");

            for (int i = 0; i < rules.size(); i++)
            {
                logger.info("Skip when " + rules.get(i));
            }
        }

        return CONTINUE;
    }

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("Usage is :\n");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" [options]");
        sb.append(" <input file>");
        sb.append(" [<output file>]");
        sb.append("\n\n");
        sb.append("Options:\n");
        sb.append(" [-i<inputFormat>]       - Format of the input file\n");
        sb.append(" [-o<outputFormat>]      - Format of the output file\n");
        sb.append(
            " [-h]                    - Removes all hydrogens from molecule\n");
        sb.append(" [+h]                    - Adds hydrogens to molecule\n");
        sb.append(
            " [+p]                    - Adds only polar hydrogens (+h implicit)\n");
        sb.append(
            " [-e]                    - Converts only non-empty molecules\n");
        sb.append(
            " [-d]                    - Removes all descriptors from the molecule\n");
        sb.append(
            " [+d]                    - Adds all available descriptors to the molecule\n");
        sb.append(" [+v]                    - Switch verbosity ON\n");
        sb.append(
            " [+split<SIZE>]             - Generated splitted output file of SIZE\n");
        sb.append(
            " [+snd]                  - Shows all available native value descriptors\n");
        sb.append(
            " [+sad]                  - Shows all available atom property descriptors\n");
        sb.append(
            " [+sall]                 - Shows all available descriptors\n");
        sb.append(
            " [-salt]                 - Strip salts and gets only largest contigous fragment\n");
        sb.append(
            " [+x<descriptor name>]   - Converts only molecules where <descriptor name> exists\n");
        sb.append(" [-r<skip  desc. rule>]  - Skips molecules, if rule fits\n");
        sb.append(
            " [+r<conv. desc. rule>]  - Converts only molecules where rule fits\n");
        sb.append(
            " [+f<lineStructure>]     - Required if you use FLAT output format which other input format\n");
        sb.append(
            " [+s<lineStructure>]     - Can be used for an alternate SMILES entry line structure\n");
        sb.append(
            " [-m<SMARTS rule>]       - Skips molecules, if SMARTS rule fits\n");
        sb.append(
            " [+m<SMARTS rule>]       - Converts only molecules where SMARTS rule fits\n");
        sb.append(
            " [-um<SMARTS rule>]      - Skips molecules, if unique SMARTS rule fits\n");
        sb.append(
            " [+um<SMARTS rule>]      - Converts only molecules where unique SMARTS rule fits\n");
        sb.append(" [-?][--help]            - Shows this message\n\n");
        sb.append(
            "If no output file is defined, all molecules will be written to stdout.\n");
        sb.append("\nFilter rules have the form:\n");
        sb.append("<native value descriptor><relation><value>\n");
        sb.append("where <relation> is <, <=, ==, >, >= or !=\n");
        sb.append("Example:\n");
        sb.append("\"+rNumber_of_halogen_atoms==2\"\n");
        sb.append("\nSMARTS filter rules have the form:\n");
        sb.append("<SMARTS pattern><relation><value>\n");
        sb.append("where <relation> is <, <=, ==, >, >= or !=\n");
        sb.append("Example:\n");
        sb.append("\"+umaNC=O==1\"\n");
        sb.append(
            "Converts all molecules, where the molecule contains ONE NC=O group connected to an aromatic atom (aNC=O).\n");
        sb.append("\nSupported molecule types:\n");
        sb.append(IOTypeHolder.instance().toString());

        //show available native value descriptors
        Vector nativeDescs = DescriptorHelper.instance().getNativeDescs();
        sb.append("\n\nNative value descriptors: ");

        int s = nativeDescs.size();

        for (int i = 0; i < s; i++)
        {
            sb.append(nativeDescs.get(i));

            if (i < (s - 1))
            {
                sb.append(", ");
            }
        }

        sb.append(
            "\n\nThis is version $Revision: 1.36 $ ($Date: 2004/07/25 20:43:27 $)\n");

        System.out.println(sb.toString());
    }

    private void getAndStoreDescriptor(JOEMol mol, String descName)
    {
        DescResult result = null;

        try
        {
            result = DescriptorHelper.instance().descFromMol(mol, descName);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
        }

        if (result == null)
        {
            logger.error("Descriptor '" + descName +
                "' was not calculated and will not be stored.");
        }
        else
        {
            JOEPairData dp = new JOEPairData();
            dp.setAttribute(descName);
            dp.setValue(result);
            mol.addData(dp);
        }
    }

    private String getSplitName(String name, int number)
    {
        int index = name.lastIndexOf('.');

        if (index == -1)
        {
            return name + number;
        }
        else
        {
            if (splitSize == 1)
            {
                return name.substring(0, index) + "." + (number + 1) +
                name.substring(index);
            }
            else
            {
                return name.substring(0, index) + "." + (number + 1) + "-" +
                (number + splitSize) + name.substring(index);
            }
        }
    }

    private boolean checkInputType()
    {
        if (inType == null)
        {
            try
            {
                inType = SimpleReader.checkGetInputType(inputFile);
            }
             catch (IOException e)
            {
                System.out.println(e.getMessage());

                return false;
            }
        }

        return true;
    }

    private boolean checkOutputType()
    {
        if (outputFile != null)
        {
            if (outType == null)
            {
                try
                {
                    outType = SimpleWriter.checkGetOutputType(outputFile);
                }
                 catch (IOException e)
                {
                    System.out.println(e.getMessage());

                    return false;
                }
            }
        }

        return true;
    }

    private boolean handleSingle(JOEMol mol)
        throws MoleculeIOException, IOException
    {
        boolean success;

        if (skipEmpty && mol.empty())
        {
            logger.warn("Empty molecule '" + mol.getTitle() + "' was ignored.");

            return true;
        }

        if (removeHydrogens)
        {
            // use begin and end modify to update 
            // rotamer informations
            mol.beginModify();
            mol.deleteHydrogens();
            mol.endModify();
        }

        if (stripSalt)
        {
            mol.stripSalts();
        }

        if (addHydrogens)
        {
            mol.addHydrogens(polarOnly, usePHvalueCorrection, true);
        }

        if (addDescriptors)
        {
            Enumeration enum = DescriptorHelper.instance().descriptors();
            String tmp;

            while (enum.hasMoreElements())
            {
                tmp = (String) enum.nextElement();

                getAndStoreDescriptor(mol, tmp);
            }
        }

        if (!skipCriteria(mol))
        {
            if (outputFile != null)
            {
                if (verbose)
                {
                    System.out.println("write " + mol.toString(verboseType));
                }

                if (removeDescriptors)
                {
                    if (JOEHelper.hasInterface(writer, "PropertyWriter"))
                    {
                        success = ((PropertyWriter) writer).write(mol, null,
                                false, null);
                    }
                    else
                    {
                        success = writer.write(mol);
                    }
                }
                else
                {
                    success = writer.write(mol);
                }

                if (!success)
                {
                    return false;
                }
            }
            else
            {
                System.out.println(mol);
            }
        }

        molCounter++;

        if ((molCounter % 500) == 0)
        {
            logger.info("... " + molCounter +
                " molecules successful converted in " + watch.getPassedTime() +
                " ms.");
        }

        if (splitFile)
        {
            if (splitSize == actualSplitNumber)
            {
                actualSplitNumber = 1;

                try
                {
                    if (writer != null)
                    {
                        writer.closeWriter();

                        //      out = new BufferedOutputStream( new FileOutputStream(outputFile));
                        out = new FileOutputStream(getSplitName(outputFile,
                                    splitMoleculeNumber));

                        try
                        {
                            writer = JOEFileFormat.getMolWriter(out, outType);
                        }
                         catch (MoleculeIOException e1)
                        {
                            e1.printStackTrace();

                            return false;
                        }
                    }
                }
                 catch (IOException e)
                {
                    e.printStackTrace();

                    return false;
                }
            }
            else
            {
                actualSplitNumber++;
            }

            splitMoleculeNumber++;
        }

        return true;
    }

    private boolean skipCriteria(JOEMol mol)
    {
        boolean what = false;

        if (allDescFilter != null)
        {
            what = allDescFilter.accept(mol);

            if (!what)
            {
                return true;
            }
            else
            {
                what = false;
            }
        }

        if (rules != null)
        {
            for (int i = 0; i < rules.size(); i++)
            {
                what = ((NativeValueFilter) rules.get(i)).accept(mol);

                //System.out.println(rules.get(i) + "=" + what);
                if (what)
                {
                    return true;
                }
            }
        }

        if (smartsRules != null)
        {
            for (int i = 0; i < smartsRules.size(); i++)
            {
                what = ((SMARTSFilter) smartsRules.get(i)).accept(mol);

                //System.out.println(smartsRules.get(i) + "=" + what);
                if (what)
                {
                    return true;
                }
            }
        }

        return what;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
