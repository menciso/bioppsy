///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DescriptorStatistic.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/02/20 13:12:21 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.test;


/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.apache.log4j.Category;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;

import joelib.process.types.DescBinning;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 *  Example for converting molecules.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/02/20 13:12:21 $
 */
public class DescriptorStatistic
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.test.DescriptorStatistic");
    public static final int CONTINUE = 0;
    public static final int STOP = 1;
    public static final int STOP_USAGE = 2;
    private static DescBinning binning = null;

    //~ Instance fields ////////////////////////////////////////////////////////

    private IOType inType;
    private String inputFile;
    private String outputFile;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        DescriptorStatistic convert = new DescriptorStatistic();

        int status = convert.parseCommandLine(args);

        if (status == CONTINUE)
        {
            convert.test();
        }
        else if (status == STOP_USAGE)
        {
            convert.usage();
            System.exit(1);
        }
        else if (status == STOP)
        {
            System.exit(0);
        }
    }

    /**
     *  Description of the Method
     *
     * @param  args  Description of the Parameter
     * @return       Description of the Return Value
     */
    public int parseCommandLine(String[] args)
    {
        // get default properties
        //Properties prop = PropertyHolder.instance().getProperties();
        String arg;

        for (int i = 0; i < args.length; i++)
        {
            arg = args[i];

            if (arg.startsWith("--help"))
            {
                return STOP_USAGE;
            }
            else if (arg.startsWith("-?"))
            {
                return STOP_USAGE;
            }
            else if (arg.startsWith("-i"))
            {
                String inTypeS = arg.substring(2);
                inType = IOTypeHolder.instance().getIOType(inTypeS.toUpperCase());

                if (inType == null)
                {
                    logger.error("Input type '" + inTypeS + "' not defined.");

                    return STOP_USAGE;
                }
            }
            else
            {
                if (inputFile == null)
                {
                    inputFile = arg;
                }
                else
                {
                    outputFile = arg;

                    if (outputFile.equalsIgnoreCase(inputFile))
                    {
                        logger.error("'" + inputFile + "' and '" + outputFile +
                            "' are the same file.");

                        return STOP_USAGE;
                    }
                }
            }
        }

        if (inputFile == null)
        {
            logger.error("No input file defined.");

            return STOP_USAGE;
        }

        if (!checkInputType())
        {
            return STOP_USAGE;
        }

        return CONTINUE;
    }

    /**
     *  A unit test for JUnit
     */
    public boolean test()
    {
        PrintStream result = null;

        if (outputFile != null)
        {
            try
            {
                result = new PrintStream(new FileOutputStream(outputFile));
            }
             catch (Exception ex)
            {
                ex.printStackTrace();

                return false;
            }
        }

        binning = DescBinning.getDescBinning(inType, inputFile);

        if (binning != null)
        {
            if (outputFile != null)
            {
                result.println("Descriptor statistic:\n " +
                    binning.getDescStatistic().toString());
                result.println("Descriptor binning:\n " + binning.toString());
            }
            else
            {
                System.out.println("Descriptor statistic:\n " +
                    binning.getDescStatistic().toString() + "\n");
                System.out.println("Descriptor binning:\n " +
                    binning.toString());
            }
        }

        return true;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("Usage is :\n");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" [options]");
        sb.append(" <input file>");
        sb.append(" [<output file>]");
        sb.append("\n\n");
        sb.append("Options:\n");
        sb.append(" [-i<inputFormat>]       - Format of the input file\n");
        sb.append("\n\nSupported molecule types:");
        sb.append(IOTypeHolder.instance().toString());
        sb.append(
            "\n\nThis is version $Revision: 1.13 $ ($Date: 2004/02/20 13:12:21 $)\n");

        System.out.println(sb.toString());

        //System.exit(0);
    }

    private boolean checkInputType()
    {
        if (inType == null)
        {
            inType = IOTypeHolder.instance().filenameToType(inputFile);

            if (inType == null)
            {
                System.out.println("Input type of " + inputFile +
                    " could not be estimated.");

                return false;
            }

            MoleculeFileType mfType = null;

            try
            {
                mfType = JOEFileFormat.getMoleculeFileType(inType);
            }
             catch (MoleculeIOException ex)
            {
                ex.printStackTrace();
            }

            logger.info("Input type set to " + inType.toString() + ": " +
                mfType.inputDescription());
        }

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
