///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: IsomerismDetection.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/29 10:16:49 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util;

import org.apache.log4j.Category;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;

import joelib.util.iterator.NbrAtomIterator;


/**
 * Helper class to detect E/Z isomerism.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/29 10:16:49 $
 */
public class IsomerismDetection
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.util.IsomerismDetection");
    public static final int EZ_ISOMERISM_UNDEFINED = 0;
    public static final int CISTRANS_ISOMERISM_UNDEFINED = 0;
    public static final int Z_ISOMERISM = 1;
    public static final int E_ISOMERISM = 2;
    public static final int CIS_ISOMERISM = 1;
    public static final int TRANS_ISOMERISM = 2;

    //~ Constructors ///////////////////////////////////////////////////////////

    public IsomerismDetection()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Sets up/down informations for cis/trans isomerism of a double bond.<br>
     * Cases:<br>
     * E/trans -- bondUP/doubleBond/bondUP<br>
     * Z/cis   -- bondDOWN/doubleBond/bondUP<br>
     * <br>
     * The method gets two heavy atoms with the highest atomic number and sets the corresponding
     * up/down bond flags for cis/trans isomeres.
     *
     *
     * @param bond the double bond
     * @return ezType new cis/trans type for this double bond
     * @see #EZ_ISOMERISM_UNDEFINED
     * @see #Z_ISOMERISM
     * @see #E_ISOMERISM
     * @see #isCisTransBond(JOEBond)
     * @see #getCisTransFrom2D3D(JOEBond)
     */
    public static void setCisTransBond(JOEBond bond, int ezType)
    {
        if (!bond.isDouble())
        {
            return;
        }

        JOEAtom begin = bond.getBeginAtom();
        JOEAtom end = bond.getEndAtom();

        begin.nbrAtomIterator();

        NbrAtomIterator nait = begin.nbrAtomIterator();
        JOEBond tmpBond;
        JOEBond beginBond = null;
        JOEAtom nbrAtom;

        while (nait.hasNext())
        {
            nbrAtom = nait.nextNbrAtom();
            tmpBond = nait.actualBond();

            if (nbrAtom != end)
            {
                if ((beginBond == null) ||
                        (nbrAtom.getAtomicNum() > begin.getAtomicNum()))
                {
                    beginBond = tmpBond;
                }
            }
        }

        nait = end.nbrAtomIterator();

        JOEBond endBond = null;

        while (nait.hasNext())
        {
            nbrAtom = nait.nextNbrAtom();
            tmpBond = nait.actualBond();

            if (nbrAtom != begin)
            {
                if ((endBond == null) ||
                        (nbrAtom.getAtomicNum() > end.getAtomicNum()))
                {
                    endBond = tmpBond;
                }
            }
        }

        if ((beginBond == null) || (endBond == null))
        {
            return;
        }

        if (ezType == Z_ISOMERISM)
        {
            beginBond.setDown();
            endBond.setUp();
        }
        else if (ezType == E_ISOMERISM)
        {
            beginBond.setUp();
            endBond.setUp();
        }
    }

    /**
     * Checks bonds for cis/trans isomerism using the SMILES flags up/down bond connected to a double bond.<br>
     * Cases:<br>
     * E/trans -- bondUP/doubleBond/bondUP<br>
     * E/trans -- bondDOWN/doubleBond/bondDOWN<br>
     * Z/cis   -- bondUP/doubleBond/bondDOWN<br>
     * Z/cis   -- bondDOWN/doubleBond/bondUP<br>
     * <br>
     * This method does not check multiple definitions.
     * If no up/down informations are available, the {@link #getCisTransFrom2D3D(JOEBond)} method is used
     * also to get cis/trans informations.
     * Single bond flags will be not set if a cis/trans double bond is detected.
     *
     * @param bond
     * @return int Z_ISOMERISM for cis, E_ISOMERISM for trans and EZ_ISOMERISM_UNDEFINED for undefined isomerism
     * @see #EZ_ISOMERISM_UNDEFINED
     * @see #Z_ISOMERISM
     * @see #E_ISOMERISM
     * @see #getCisTransFrom2D3D(JOEBond)
     */
    public static int isCisTransBond(JOEBond bond)
    {
        return isCisTransBond(bond, false);
    }

    /**
     * Checks bonds for cis/trans isomerism using the SMILES flags up/down bond connected to a double bond.<br>
     * Cases:<br>
     * E/trans -- bondUP/doubleBond/bondUP<br>
     * E/trans -- bondDOWN/doubleBond/bondDOWN<br>
     * Z/cis   -- bondUP/doubleBond/bondDOWN<br>
     * Z/cis   -- bondDOWN/doubleBond/bondUP<br>
     * <br>
     * This method does not check multiple definitions.
     * If no up/down informations are available, the {@link #getCisTransFrom2D3D(JOEBond)} method is used
     * also to get cis/trans informations.
     *
     *
     * @param bond
     * @param setSingleBondFlags set the single bond flags {@link JOEBond#JOE_TORUP_BOND}/{@link JOEBond#JOE_TORDOWN_BOND}, if <tt>true</tt> and a cis/trans bond is detected.
     * @return int Z_ISOMERISM for cis, E_ISOMERISM for trans and EZ_ISOMERISM_UNDEFINED for undefined isomerism
     * @see #EZ_ISOMERISM_UNDEFINED
     * @see #Z_ISOMERISM
     * @see #E_ISOMERISM
     * @see #getCisTransFrom2D3D(JOEBond)
     */
    public static int isCisTransBond(JOEBond bond, boolean setSingleBondFlags)
    {
        if (!bond.isDouble())
        {
            return EZ_ISOMERISM_UNDEFINED;
        }

        JOEAtom begin = bond.getBeginAtom();
        JOEAtom end = bond.getEndAtom();

        begin.nbrAtomIterator();

        NbrAtomIterator nait = begin.nbrAtomIterator();
        JOEBond tmpBond;
        JOEBond beginBond = null;
        JOEAtom nbrAtom;

        while (nait.hasNext())
        {
            nbrAtom = nait.nextNbrAtom();
            tmpBond = nait.actualBond();

            if (((tmpBond.getFlags() & JOEBond.JOE_TORUP_BOND) != 0) ||
                    ((tmpBond.getFlags() & JOEBond.JOE_TORDOWN_BOND) != 0))
            {
                beginBond = tmpBond;

                break;
            }
        }

        nait = end.nbrAtomIterator();

        JOEBond endBond = null;

        while (nait.hasNext())
        {
            nbrAtom = nait.nextNbrAtom();
            tmpBond = nait.actualBond();

            if (((tmpBond.getFlags() & JOEBond.JOE_TORUP_BOND) != 0) ||
                    ((tmpBond.getFlags() & JOEBond.JOE_TORDOWN_BOND) != 0))
            {
                endBond = tmpBond;

                break;
            }
        }

        if ((beginBond == null) || (endBond == null))
        {
            //try to resolve cis/trans isomerism from 2D/3D structure
            int ez = getCisTransFrom2D3D(bond, setSingleBondFlags);

            return ez;
        }

        if ((beginBond.isUp() && endBond.isUp()) ||
                (beginBond.isDown() && endBond.isDown()))
        {
        	//System.out.println("E_ISOMERISM detected.");
            return E_ISOMERISM;
        }

        if ((beginBond.isDown() && endBond.isUp()) ||
                (beginBond.isUp() && endBond.isDown()))
        {
        	//System.out.println("Z_ISOMERISM detected.");
        	return Z_ISOMERISM;
        }

        return EZ_ISOMERISM_UNDEFINED;
    }

    /**
     * Checks bonds for cis/trans isomerism using 2D/3D informations.
     * Single bond flags will be not set if a cis/trans double bond is detected.
     *
     * @param bond
     * @param setSingleBondFlags set the single bond flags {@link JOEBond#JOE_TORUP_BOND}/{@link JOEBond#JOE_TORDOWN_BOND}, if <tt>true</tt> and a cis/trans bond is detected.
     * @return int Z_ISOMERISM for cis, E_ISOMERISM for trans and EZ_ISOMERISM_UNDEFINED for undefined isomerism
     * @see #EZ_ISOMERISM_UNDEFINED
     * @see #Z_ISOMERISM
     * @see #E_ISOMERISM
     * @see #isCisTransBond(JOEBond)
     */
    public static int getCisTransFrom2D3D(JOEBond bond)
    {
        return getCisTransFrom2D3D(bond);
    }

    /**
     * Checks bonds for cis/trans isomerism using 2D/3D informations.
     *
     * @param bond
     * @param setSingleBondFlags set the single bond flags {@link JOEBond#JOE_TORUP_BOND}/{@link JOEBond#JOE_TORDOWN_BOND}, if <tt>true</tt> and a cis/trans bond is detected.
     * @return int Z_ISOMERISM for cis, E_ISOMERISM for trans and EZ_ISOMERISM_UNDEFINED for undefined isomerism
     * @see #EZ_ISOMERISM_UNDEFINED
     * @see #Z_ISOMERISM
     * @see #E_ISOMERISM
     * @see #isCisTransBond(JOEBond)
     */
    public static int getCisTransFrom2D3D(JOEBond bond,
        boolean setSingleBondFlags)
    {
        if (!bond.isDouble() && bond.isInRing())
        {
            return EZ_ISOMERISM_UNDEFINED;
        }

        JOEAtom begin = bond.getBeginAtom();
        JOEAtom end = bond.getEndAtom();

        //skip allenes
        if ((begin.getHyb() == 1) || (end.getHyb() == 1))
        {
            return EZ_ISOMERISM_UNDEFINED;
        }

        begin.nbrAtomIterator();

        NbrAtomIterator nait = begin.nbrAtomIterator();
        JOEBond tmpBond;
        JOEBond beginBond = null;
        JOEAtom nbrAtom;
        JOEAtom afterBeginAtom = null;
        JOEAtom afterEndAtom = null;
        boolean uniqueHeavyAtom = false;

        while (nait.hasNext())
        {
            nbrAtom = nait.nextNbrAtom();
            tmpBond = nait.actualBond();

            if (nbrAtom != end)
            {
                if (beginBond == null)
                {
                    // skip hydrogens
                    if (!nbrAtom.isHydrogen())
                    {
                        beginBond = tmpBond;
                        afterBeginAtom = nbrAtom;
                        uniqueHeavyAtom = true;
                    }
                }
                else
                {
                    // skip hydrogens
                    if (!nbrAtom.isHydrogen())
                    {
                        if (nbrAtom.getAtomicNum() > begin.getAtomicNum())
                        {
                            beginBond = tmpBond;
                            afterBeginAtom = nbrAtom;
                            uniqueHeavyAtom = true;
                        }
                        else if (nbrAtom.getAtomicNum() == begin.getAtomicNum())
                        {
                            uniqueHeavyAtom = false;
                        }
                    }
                }
            }
        }

        //do not use multiple heavy atoms
        if (uniqueHeavyAtom == false)
        {
            beginBond = null;
            afterBeginAtom = null;
        }

        nait = end.nbrAtomIterator();

        JOEBond endBond = null;

        while (nait.hasNext())
        {
            nbrAtom = nait.nextNbrAtom();
            tmpBond = nait.actualBond();

            if (nbrAtom != begin)
            {
                if (endBond == null)
                {
                    // skip hydrogens
                    if (!nbrAtom.isHydrogen())
                    {
                        endBond = tmpBond;
                        afterEndAtom = nbrAtom;
                        uniqueHeavyAtom = true;
                    }
                }
                else
                {
                    // skip hydrogens
                    if (!nbrAtom.isHydrogen())
                    {
                        if (nbrAtom.getAtomicNum() > end.getAtomicNum())
                        {
                            endBond = tmpBond;
                            afterEndAtom = nbrAtom;
                            uniqueHeavyAtom = true;
                        }
                        else if (nbrAtom.getAtomicNum() == end.getAtomicNum())
                        {
                            uniqueHeavyAtom = false;
                        }
                    }
                }
            }
        }

        //do not use multiple heavy atoms
        if (uniqueHeavyAtom == false)
        {
            endBond = null;
            afterEndAtom = null;
        }

        // one side of the double bond is not really cis/trans !
        if ((beginBond == null) || (endBond == null))
        {
            return EZ_ISOMERISM_UNDEFINED;
        }
        
        if (Math.abs(XYZVector.calcTorsionAngle(afterBeginAtom.getVector(),
                        begin.getVector(), end.getVector(),
                        afterEndAtom.getVector())) > 10.0)
        {
            if (setSingleBondFlags)
            {
                beginBond.setUp();
                endBond.setUp();
            }

            return E_ISOMERISM;
        }
        else
        {
            if (setSingleBondFlags)
            {
                beginBond.setUp();
                endBond.setDown();
            }

            return Z_ISOMERISM;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
