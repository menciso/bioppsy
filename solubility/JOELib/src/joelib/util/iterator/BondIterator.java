///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BondIterator.java,v $
//  Purpose:  Iterator for the standard Vector.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.iterator;

import java.util.Vector;

import joelib.molecule.JOEBond;


/**
 * Gets an iterator over all bonds in a molecule.
 *
 * <blockquote><pre>
 * BondIterator ait = mol.bondIterator();
 * JOEBond bond;
 * while (ait.hasNext())
 * {
 *   bond = bit.nextBond();
 *
 * }
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:29 $
 * @see VectorIterator
 * @see joelib.molecule.JOEMol#bondIterator()
 */
public class BondIterator extends VectorIterator
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the BondIterator object
     *
     * @param  v  Description of the Parameter
     */
    public BondIterator(Vector v)
    {
        super(v);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public JOEBond nextBond()
    {
        return (JOEBond) next();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
