///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GenericDataIterator.java,v $
//  Purpose:  Iterator for the standard Vector.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.iterator;

import java.util.*;

import joelib.data.*;


/**
 * Iterator over generic data elements.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:29 $
 */
public class GenericDataIterator implements Iterator
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private Enumeration hEnum;
    private VectorIterator vIter;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the GenericDataIterator object
     *
     * @param v  Description of the Parameter
     * @param h  Description of the Parameter
     */
    public GenericDataIterator(Vector v, Hashtable h)
    {
        vIter = new VectorIterator(v);

        hEnum = h.elements();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Description of the Method
     *
     * @return   Description of the Return Value
     */
    public boolean hasNext()
    {
        if (vIter.hasNext())
        {
            return true;
        }
        else if (hEnum.hasMoreElements())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Description of the Method
     *
     * @return   Description of the Return Value
     */
    public Object next()
    {
        if (vIter.hasNext())
        {
            return vIter.next();
        }
        else if (hEnum.hasMoreElements())
        {
            return hEnum.nextElement();
        }
        else
        {
            return null;
        }
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public JOEGenericData nextGenericData()
    {
        return (JOEGenericData) next();
    }

    /**
     * Description of the Method
     */
    public void remove()
    {
        try
        {
            throw new NoSuchMethodException("Method not implemented.");
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
