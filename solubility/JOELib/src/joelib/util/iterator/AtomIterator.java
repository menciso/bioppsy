///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomIterator.java,v $
//  Purpose:  Iterator for the standard Vector.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.iterator;

import java.util.Vector;

import joelib.molecule.JOEAtom;


/**
 * Gets an iterator over all atoms in a molecule.
 *
 * <blockquote><pre>
 * AtomIterator ait = mol.atomIterator();
 * JOEAtom atom;
 * while (ait.hasNext())
 * {
 *   atom = ait.nextAtom();
 *
 * }
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:29 $
 * @see VectorIterator
 * @see joelib.molecule.JOEMol#atomIterator()
 */
public class AtomIterator extends VectorIterator implements Cloneable
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the AtomIterator object
     *
     * @param  v  Description of the Parameter
     */
    public AtomIterator(Vector v)
    {
        super(v);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Object clone()
    {
        AtomIterator vIter = new AtomIterator(getVector());
        vIter.setActualIndex(actualIndex());

        return vIter;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public JOEAtom nextAtom()
    {
        return (JOEAtom) next();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
