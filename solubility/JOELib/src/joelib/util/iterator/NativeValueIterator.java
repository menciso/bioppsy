///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: NativeValueIterator.java,v $
//  Purpose:  Iterator for the standard Vector.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.iterator;

import java.util.Iterator;

import joelib.data.JOEDataType;
import joelib.data.JOEGenericData;
import joelib.data.JOEPairData;

import joelib.desc.NativeValue;

import joelib.molecule.JOEMol;

import joelib.util.JOEHelper;


/**
 *  Gets an iterator over native descriptor values (<tt>int</tt> or <tt>double</tt>)
 * of this molecule.
 *
 * <blockquote><pre>
 * NativeValueIterator nativeIt = mol.nativeValueIterator();
 * double value;
 * String descName;
 * while (nativeIt.hasNext())
 * {
 *   value = nativeIt.nextDouble();
 *   descName = nativeIt.actualName();
 *
 * }
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:29 $
 * @see joelib.molecule.JOEMol#nativeValueIterator()
 */
public class NativeValueIterator implements Iterator
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private GenericDataIterator gdit;
    private JOEGenericData genericData;
    private JOEMol mol;
    private boolean valueTaken;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the GenericDataIterator object
     *
     * @param v  Description of the Parameter
     * @param h  Description of the Parameter
     */
    public NativeValueIterator(JOEMol _mol, GenericDataIterator _gdit)
    {
        gdit = _gdit;
        mol = _mol;
        valueTaken = true;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public synchronized String actualName()
    {
        return ((JOEPairData) genericData).getAttribute();
    }

    /**
     * Description of the Method
     *
     * @return   Description of the Return Value
     */
    public synchronized boolean hasNext()
    {
        if (valueTaken == false)
        {
            return true;
        }

        while (gdit.hasNext())
        {
            genericData = gdit.nextGenericData();

            if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
            {
                JOEPairData pairData = (JOEPairData) genericData;
                genericData = mol.getData(pairData.getAttribute(), true);
                pairData = (JOEPairData) genericData;

                if (JOEHelper.hasInterface(pairData, "NativeValue"))
                {
                    valueTaken = false;

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Description of the Method
     *
     * @return   Description of the Return Value
     */
    public synchronized Object next()
    {
        if (valueTaken == false)
        {
            valueTaken = true;

            return genericData;
        }

        while (gdit.hasNext())
        {
            genericData = gdit.nextGenericData();

            if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
            {
                JOEPairData pairData = (JOEPairData) genericData;
                genericData = mol.getData(pairData.getAttribute(), true);
                pairData = (JOEPairData) genericData;

                if (JOEHelper.hasInterface(pairData, "NativeValue"))
                {
                    valueTaken = true;

                    return genericData;
                }
            }
        }

        return null;
    }

    public synchronized double nextDouble()
    {
        return ((NativeValue) next()).getDoubleNV();
    }

    public synchronized int nextInt()
    {
        return ((NativeValue) next()).getIntNV();
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public synchronized NativeValue nextNativeValue()
    {
        return (NativeValue) next();
    }

    public synchronized String nextString()
    {
        return ((NativeValue) next()).getStringNV();
    }

    /**
     * Description of the Method
     */
    public synchronized void remove()
    {
        try
        {
            throw new NoSuchMethodException("Method not implemented.");
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
