///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: NbrAtomIterator.java,v $
//  Purpose:  Iterator for the standard Vector.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.iterator;

import java.util.*;

import joelib.molecule.*;


/**
 * Gets an iterator over all neighbour atoms in a atom.
 *
 * <blockquote><pre>
 * NbrAtomIterator nait = atom.nbrAtomIterator();
 * JOEBond bond;
 * JOEAtom nbrAtom;
 * while (nait.hasNext())
 * {
 *          nbrAtom=nait.nextNbrAtom();
 *   bond = nait.actualBond();
 *
 * }
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:29 $
 * @see VectorIterator
 * @see joelib.molecule.JOEAtom#nbrAtomIterator()
 */
public class NbrAtomIterator extends VectorIterator
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private JOEAtom atom;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the NbrAtomIterator object
     *
     * @param  bonds  Description of the Parameter
     * @param  atom   Description of the Parameter
     */
    public NbrAtomIterator(Vector bonds, JOEAtom atom)
    {
        super(bonds);
        this.atom = atom;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public Object actual()
    {
        JOEBond bond = (JOEBond) super.actual();

        return bond.getNbrAtom(atom);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public JOEBond actualBond()
    {
        return (JOEBond) super.actual();
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public Object next()
    {
        JOEBond bond = (JOEBond) super.next();

        return bond.getNbrAtom(atom);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public JOEAtom nextNbrAtom()
    {
        return (JOEAtom) next();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
