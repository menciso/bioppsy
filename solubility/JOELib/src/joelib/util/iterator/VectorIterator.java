///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: VectorIterator.java,v $
//  Purpose:  Iterator for the standard Vector.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;


/**
 *  Iterator for the standard {@link Vector}.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/25 20:43:29 $
 */
public class VectorIterator implements Iterator, Cloneable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    private Vector vector = null;

    /**
     *  Description of the Field
     */
    private int currentIndex = 0;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the VectorIterator object
     *
     * @param  vector  Description of the Parameter
     */
    public VectorIterator(Vector vector)
    {
        this.vector = vector;
        this.currentIndex = 0;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the actual index.
     *
     * @param  index  The new actualIndex value
     */
    final public void setActualIndex(int index)
    {
        if (index == 0)
        {
            currentIndex = 0;
        }
        else
        {
            currentIndex = index + 1;
        }
    }

    /**
     *  Returns the actual <tt>Object</tt> .
     *
     * @return    Description of the Return Value
     */
    public Object actual()
    {
        if (currentIndex > 0)
        {
            return vector.get(currentIndex - 1);
        }
        else
        {
            return vector.get(currentIndex);
        }
    }

    /**
     *  Returns the index number ot the sctual <tt>Object</tt> . Warning: Util now
     *  you should not use setIndex(actualIndex()) because the internal index will
     *  be not set correctly !!!
     *
     * @return    Description of the Return Value
     */
    final public int actualIndex()
    {
        if (currentIndex > 0)
        {
            return currentIndex - 1;
        }
        else
        {
            return 0;
        }
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public Object clone()
    {
        VectorIterator vIter = new VectorIterator(vector);
        vIter.setActualIndex(actualIndex());

        return vIter;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasNext()
    {
        //System.out.println("vector.size():"+vector.size());
        if (vector.size() == 0)
        {
            return false;
        }

        return (((currentIndex >= 0) && (currentIndex < vector.size())) ? true
                                                                        : false);
    }

    /**
     *  Description of the Method
     *
     * @param  newObject  Description of the Parameter
     */
    public void insert(Object newObject)
    {
        vector.add(currentIndex, newObject);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public Object next()
    {
        if (vector == null)
        {
            throw new NoSuchElementException();
        }

        if (hasNext())
        {
            return vector.get(currentIndex++);
        }
        else
        {
            throw new NoSuchElementException();
        }
    }

    /**
     *  Description of the Method
     */
    public void remove()
    {
        //throw new UnsupportedOperationException();
        currentIndex--;

        if (currentIndex < 0)
        {
            throw new IllegalStateException();
        }

        //if ( hasNext() )
        //{
        try
        {
            vector.remove(currentIndex);
        }
         catch (ArrayIndexOutOfBoundsException e)
        {
            throw new IllegalStateException();
        }

        //}
        //else
        //{
        //  throw new IllegalStateException();
        //}
    }

    /**
     *  Description of the Method
     */
    public void reset()
    {
        currentIndex = 0;
    }

    protected final Vector getVector()
    {
        return vector;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
