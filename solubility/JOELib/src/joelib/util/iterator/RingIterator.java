///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: RingIterator.java,v $
//  Purpose:  Iterator for the standard Vector.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.iterator;

import java.util.Vector;

import joelib.ring.JOERing;


/**
 * Gets an iterator over all rings in a molecule.
 *
 * <blockquote><pre>
 * RingIterator rit = mol.getRingIterator();
 * JOERing ring;
 * while(rit.hasNext())
 * {
 *   ring = rit.nextRing();
 *
 * }
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:29 $
 * @see VectorIterator
 * @see joelib.molecule.JOEMol#getRingIterator()
 */
public class RingIterator extends VectorIterator
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the RingIterator object
     *
     * @param  v  Description of the Parameter
     */
    public RingIterator(Vector v)
    {
        super(v);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public JOERing nextRing()
    {
        return (JOERing) next();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
