///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ComparisonHelper.java,v $
//  Purpose:  Holds all native value descriptors as double matrix for all known molecules.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:43:28 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util;

import java.io.FileInputStream;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEPairData;

import joelib.desc.BinaryValue;
import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DistanceMetricValue;
import joelib.desc.NativeValue;

import joelib.io.IOType;
import joelib.io.SimpleReader;

import joelib.molecule.JOEMol;


/**
 *  Example for converting molecules.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:28 $
 */
public class ComparisonHelper
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.util.ComparisonHelper");
    private final static int NOT_DEFINED = -1;
    private final static int BINARY_COMPARISON = 0;
    private final static int EUKLIDIAN_COMPARISON = 1;
    private final static int DIST_METRIC_COMPARISON = 2;

    //~ Instance fields ////////////////////////////////////////////////////////

    private String targetFile;
    private Vector descBinaryValue;
    private Vector descValues;
    private Vector distMetricValue;
    private Vector targetMols;
    private String[] descriptors;
    private double[] tmpValues;
    private int comparisonType = NOT_DEFINED;

    //~ Constructors ///////////////////////////////////////////////////////////

    public ComparisonHelper(IOType inType, String _targetFile)
    {
        this(inType, _targetFile, 10);
    }

    /**
     *  Constructor for the ComparisonHelper object
     *
     * @param  _targetFile  Description of the Parameter
     * @param  inType       Description of the Parameter
     */
    public ComparisonHelper(IOType inType, String _targetFile, int _initialSize)
    {
        targetFile = _targetFile;

        SimpleReader reader = null;

        try
        {
            reader = new SimpleReader(new FileInputStream(_targetFile), inType);
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            //            throw ex;
        }

        targetMols = new Vector(_initialSize);
        descValues = new Vector(_initialSize);
        descBinaryValue = new Vector(_initialSize);

        JOEMol targetMol = null;

        for (;;)
        {
            targetMol = new JOEMol(inType, inType);

            //load only the first molecule !
            try
            {
                if (!reader.readNext(targetMol))
                {
                    //        targetMols = null;
                    break;
                }

                targetMols.add(targetMol);
            }
             catch (Exception ex)
            {
                targetMol = null;
                ex.printStackTrace();

                break;

                //            throw ex;
            }
        }

        reader.close();
        reader = null;
    }

    /**
     *  Constructor for the ComparisonHelper object
     *
     * @param  _targetMolecule  Description of the Parameter
     */
    public ComparisonHelper(JOEMol _targetMolecule)
    {
        int _initialSize = 1;
        targetMols = new Vector(_initialSize);
        descValues = new Vector(_initialSize);
        descBinaryValue = new Vector(_initialSize);

        targetMols.add(_targetMolecule);
    }

    public ComparisonHelper()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the comparisonDescriptor attribute of the ComparisonHelper object
     *
     * @param  _descriptor  The new comparisonDescriptor value
     * @return              Description of the Return Value
     */
    public boolean setComparisonDescriptor(String _descriptor)
    {
        if (_descriptor == null)
        {
            logger.error("No descriptors for comparison defined.");

            return false;
        }

        if ((targetMols == null) || (targetMols.size() == 0))
        {
            logger.error("No target molecule available.");

            return false;
        }

        descriptors = new String[1];
        descriptors[0] = _descriptor;
        tmpValues = new double[1];

        DescResult result = null;

        int size = targetMols.size();
        JOEMol targetMol;
        double[] tmpVal;
        JOEBitVec tmpBit;
        DistanceMetricValue tmpDistMetricValue;

        for (int i = 0; i < size; i++)
        {
            targetMol = (JOEMol) targetMols.get(i);

            //      System.out.println("targetMol("+i+"):"+targetMol);
            try
            {
                result = DescriptorHelper.descFromMol(targetMol, _descriptor,
                        true);
            }
             catch (DescriptorException ex)
            {
                ex.printStackTrace();

                return false;
            }

            if (result == null)
            {
                logger.error("Descriptor '" + _descriptor +
                    "' not found in target molecule " + targetMol.getTitle() +
                    ".");

                return false;
            }

            if (JOEHelper.hasInterface(result, "NativeValue"))
            {
                tmpVal = new double[1];
                tmpVal[0] = ((NativeValue) result).getDoubleNV();
                descValues.add(tmpVal);
                comparisonType = EUKLIDIAN_COMPARISON;
            }
            else if (JOEHelper.hasInterface(result, "BinaryValue"))
            {
                tmpBit = ((BinaryValue) result).getBinaryValue();

                //      System.out.println("add BIT:"+tmpBit.toString());
                descBinaryValue.add(tmpBit);
                comparisonType = BINARY_COMPARISON;
            }
            else if (JOEHelper.hasInterface(result, "DistanceMetricValue"))
            {
                tmpDistMetricValue = (DistanceMetricValue) result;
                distMetricValue.add(tmpDistMetricValue);
                comparisonType = DIST_METRIC_COMPARISON;
            }
            else
            {
                logger.error("Descriptor '" + _descriptor +
                    "' must be a double, a 'bit set' value or allow a distance metric.");

                return false;
            }
        }

        return true;
    }

    /**
     *  Sets the comparisonDescriptors attribute of the ComparisonHelper object
     *
     * @param  _descriptors  The new comparisonDescriptors value
     * @return               Description of the Return Value
     */
    public boolean setComparisonDescriptors(String[] _descriptors)
    {
        if (_descriptors == null)
        {
            logger.error("No descriptors for comparison defined.");

            return false;
        }

        if (_descriptors.length == 0)
        {
            logger.error("Descriptors for comparison seems to be empty.");

            return false;
        }

        if ((targetMols == null) || (targetMols.size() == 0))
        {
            logger.error("No target molecule available.");

            return false;
        }

        if (_descriptors.length == 1)
        {
            return setComparisonDescriptor(_descriptors[0]);
        }

        descriptors = _descriptors;

        int size = _descriptors.length;
        tmpValues = new double[size];

        //        descriptors= new String[size];
        int sizeMols = targetMols.size();
        JOEMol targetMol;
        double[] tmpVal;
        double value;

        for (int ii = 0; ii < sizeMols; ii++)
        {
            targetMol = (JOEMol) targetMols.get(ii);
            tmpVal = new double[size];

            for (int i = 0; i < size; i++)
            {
                //          descriptors[i]=_descriptors[i];
                try
                {
                    value = getDoubleDesc(targetMol, descriptors[i]);
                }
                 catch (DescriptorException ex)
                {
                    logger.error(ex.toString());
                    logger.error("Can't load double value of descriptor '" +
                        descriptors[i] + "' in target molecule.");

                    return false;
                }

                tmpVal[i] = value;
            }

            descValues.add(tmpVal);
        }

        comparisonType = EUKLIDIAN_COMPARISON;

        return true;
    }

    public final Vector getTargetMols()
    {
        return targetMols;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public double[] compare(JOEMol mol)
    {
        return compare(mol, null, null);
    }

    /**
     *  Description of the Method. <code>Double.NaN</code> is returned if no
     *  comparison value could be calulated
     *
     * @param  mol              Description of the Parameter
     * @param  _doubleDescName  Stores the distance value at the double value
     *      descriptor with this name.
     * @return                  double distance value
     */
    public double[] compare(JOEMol mol, String _distResultName,
        double[] distances)
    {
        // get the actual descriptor values
        int size = targetMols.size();

        if (distances == null)
        {
            distances = new double[size];
        }

        JOEBitVec bitset = null;
        DistanceMetricValue dMetricValue = null;

        switch (comparisonType)
        {
        case BINARY_COMPARISON:
            bitset = getBitset(mol);

            if (bitset == null)
            {
                return null;
            }

            break;

        case EUKLIDIAN_COMPARISON:
            tmpValues = getDescriptors(mol, tmpValues);

            if (tmpValues == null)
            {
                return null;
            }

            break;

        case DIST_METRIC_COMPARISON:
            dMetricValue = getDistMetricValue(mol);

            if (dMetricValue == null)
            {
                return null;
            }

            break;

        default:
            logger.error(
                "Descriptor value(s) does not contain valid comparison values (or are not initialized).");

            return null;
        }

        // calculate distance values for all target patterns
        for (int ii = 0; ii < size; ii++)
        {
            switch (comparisonType)
            {
            case BINARY_COMPARISON:
                distances[ii] = distance(bitset, ii);

                break;

            case EUKLIDIAN_COMPARISON:
                distances[ii] = distance(tmpValues, ii);

                break;

            case DIST_METRIC_COMPARISON:
                distances[ii] = distance(dMetricValue, ii);

                break;
            }

            // add distance result to molecule
            // store as String value !!!!
            // if you like another representation you must define
            // a descriptor result for this case !!!
            if (_distResultName != null)
            {
                JOEPairData dp = new JOEPairData();
                dp.setAttribute(_distResultName + "_" + ii);
                dp.setValue(Double.toString(distances[ii]));
                mol.addData(dp);
            }
        }

        return distances;
    }

    public double compare(JOEMol source, JOEMol target)
    {
        // get the actual descriptor values
        JOEBitVec bitsetSource = null;
        double[] descSource = new double[tmpValues.length];
        DistanceMetricValue dMetricValueSource = null;
        JOEBitVec bitsetTarget = null;
        double[] descTarget = new double[tmpValues.length];
        DistanceMetricValue dMetricValueTarget = null;

        switch (comparisonType)
        {
        case BINARY_COMPARISON:
            bitsetSource = getBitset(source);

            if (bitsetSource == null)
            {
                return Double.NaN;
            }

            bitsetTarget = getBitset(target);

            if (bitsetTarget == null)
            {
                return Double.NaN;
            }

            break;

        case EUKLIDIAN_COMPARISON:
            descSource = getDescriptors(source, descSource);

            if (tmpValues == null)
            {
                return Double.NaN;
            }

            descTarget = getDescriptors(target, descTarget);

            if (tmpValues == null)
            {
                return Double.NaN;
            }

            break;

        case DIST_METRIC_COMPARISON:
            dMetricValueSource = getDistMetricValue(source);

            if (dMetricValueSource == null)
            {
                return Double.NaN;
            }

            dMetricValueTarget = getDistMetricValue(target);

            if (dMetricValueTarget == null)
            {
                return Double.NaN;
            }

            break;

        default:
            logger.error(
                "Descriptor value(s) does not contain valid comparison values (or are not initialized).");

            return Double.NaN;
        }

        // calculate distance values for all target patterns
        switch (comparisonType)
        {
        case BINARY_COMPARISON:
            return distance(bitsetSource, bitsetSource);

        case EUKLIDIAN_COMPARISON:
            return distance(descSource, descSource);

        case DIST_METRIC_COMPARISON:
            return distance(dMetricValueSource, dMetricValueTarget);
        }

        return Double.NaN;
    }

    private final JOEBitVec getBitset(final JOEMol mol)
    {
        DescResult result = null;

        try
        {
            result = DescriptorHelper.descFromMol(mol, descriptors[0], true);
        }
         catch (DescriptorException ex)
        {
            ex.printStackTrace();

            return null;
        }

        if (result == null)
        {
            logger.error("Descriptor '" + descriptors[0] +
                "' not found in comparison molecule " + mol.getTitle() + ".");

            return null;
        }

        JOEBitVec bitset = null;

        if (JOEHelper.hasInterface(result, "BinaryValue"))
        {
            bitset = ((BinaryValue) result).getBinaryValue();
        }
        else
        {
            logger.error("Descriptor '" + descriptors[0] +
                "' must be a 'bit set' value.");

            return null;
        }

        return bitset;
    }

    private final double[] getDescriptors(final JOEMol mol, double[] vals)
    {
        DescResult result = null;
        int size = vals.length;

        for (int i = 0; i < size; i++)
        {
            try
            {
                result = DescriptorHelper.descFromMol(mol, descriptors[i], true);
            }
             catch (DescriptorException ex)
            {
                logger.error(ex.toString());
                vals[i] = Double.NaN;
            }

            if (result == null)
            {
                logger.error("Descriptor '" + descriptors[i] +
                    "' not found in comparison molecule " + mol.getTitle() +
                    ".");
                vals[i] = Double.NaN;
            }

            vals[i] = ((NativeValue) result).getDoubleNV();

            if (!JOEHelper.hasInterface(result, "NativeValue"))
            {
                logger.error("Descriptor '" + descriptors[i] +
                    "' must be a native value.");
                vals[i] = Double.NaN;
            }
        }

        return vals;
    }

    private final DistanceMetricValue getDistMetricValue(final JOEMol mol)
    {
        DescResult result = null;

        try
        {
            result = DescriptorHelper.descFromMol(mol, descriptors[0], true);
        }
         catch (DescriptorException ex)
        {
            ex.printStackTrace();

            return null;
        }

        if (result == null)
        {
            logger.error("Descriptor '" + descriptors[0] +
                "' not found in comparison molecule " + mol.getTitle() + ".");

            return null;
        }

        DistanceMetricValue dMetricValue = null;

        if (JOEHelper.hasInterface(result, "DistanceMetricValue"))
        {
            dMetricValue = ((DistanceMetricValue) result);
        }
        else
        {
            logger.error("Descriptor '" + descriptors[0] +
                "' must be a 'distance metric value' value.");

            return null;
        }

        return dMetricValue;
    }

    /**
     *  Gets the doubleDesc attribute of the ComparisonHelper object
     *
     * @param  mol                      Description of the Parameter
     * @param  _descName                Description of the Parameter
     * @return                          The doubleDesc value
     * @exception  DescriptorException  Description of the Exception
     */
    private final double getDoubleDesc(final JOEMol mol, String _descName)
        throws DescriptorException
    {
        double value = Double.NaN;
        DescResult result = null;

        //        try
        //        {
        result = DescriptorHelper.descFromMol(mol, _descName, true);

        //        }
        //        catch (DescriptorException ex)
        //        {
        //            throw ex;
        //        }
        if (JOEHelper.hasInterface(result, "NativeValue"))
        {
            value = ((NativeValue) result).getDoubleNV();
        }

        return value;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private final double distance(final JOEBitVec bitset, int index)
    {
        if (bitset == null)
        {
            return Double.NaN;
        }

        JOEBitVec target = (JOEBitVec) descBinaryValue.get(index);

        return bitset.tanimoto(target);
    }

    private final double distance(final JOEBitVec source, final JOEBitVec target)
    {
        if ((source == null) || (target == null))
        {
            return Double.NaN;
        }

        return source.tanimoto(target);
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private final double distance(final DistanceMetricValue dMetricValue,
        int index)
    {
        if (dMetricValue == null)
        {
            return Double.NaN;
        }

        DistanceMetricValue targetValue = (DistanceMetricValue) distMetricValue.get(index);

        return dMetricValue.getDistance(targetValue);
    }

    private final double distance(final DistanceMetricValue source,
        final DistanceMetricValue target)
    {
        if ((source == null) || (target == null))
        {
            return Double.NaN;
        }

        return source.getDistance(target);
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private final double distance(final double[] ds, int index)
    {
        double[] tmpVal = (double[]) descValues.get(index);

        return distance(ds, tmpVal);
    }

    private final double distance(final double[] source, final double[] target)
    {
        if ((source == null) || (target == null))
        {
            return Double.NaN;
        }

        int s = source.length;
        double val = 0.0;
        double sum = 0.0;

        for (int i = 0; i < s; i++)
        {
            val = target[i] - source[i];
            sum += (val * val);
        }

        return Math.sqrt(sum);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
