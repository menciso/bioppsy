///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DoubleInt.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.types;


/**
 * Double and integer value.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:29 $
 */
public class DoubleInt implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public double d;

    /**
     *  Description of the Field
     */
    public int i;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the IntInt object
     */
    public DoubleInt()
    {
    }

    /**
     *  Constructor for the IntInt object
     *
     * @param  _i1  Description of the Parameter
     * @param  _i2  Description of the Parameter
     */
    public DoubleInt(double _d, int _i)
    {
        d = _d;
        i = _i;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public boolean equals(Object otherObj)
    {
        if (otherObj instanceof DoubleInt)
        {
            DoubleInt di = (DoubleInt) otherObj;

            if ((di.d == this.d) && (di.i == this.i))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
