///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: IntIntInt.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.types;


/**
 * {@link IntInt} and one more integer value.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:29 $
 */
public class IntIntInt implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public IntInt ii;

    /**
     *  Description of the Field
     */
    public int i;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the IntIntInt object
     */
    public IntIntInt()
    {
        ii = new IntInt();
    }

    /**
     *  Constructor for the IntIntInt object
     *
     * @param  _ii  Description of the Parameter
     * @param  _i   Description of the Parameter
     */
    public IntIntInt(IntInt _ii, int _i)
    {
        i = _i;
        ii = _ii;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public boolean equals(Object otherObj)
    {
        if (otherObj instanceof IntIntInt)
        {
            IntIntInt iii = (IntIntInt) otherObj;

            if ((iii.i == this.i) && iii.ii.equals(this.ii))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer(10);
        sb.append('<');
        sb.append(i);
        sb.append(',');
        sb.append(ii.toString());
        sb.append('>');

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
