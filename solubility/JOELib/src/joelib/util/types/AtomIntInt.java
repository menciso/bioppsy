///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomIntInt.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.types;

import joelib.molecule.*;


/**
 * Atom and {@link IntInt}.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:29 $
 */
public class AtomIntInt implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public IntInt ii;

    /**
     *  Description of the Field
     */
    public JOEAtom a;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the AtomIntInt object
     *
     * @param  _a   Description of the Parameter
     * @param  _ii  Description of the Parameter
     */
    public AtomIntInt(JOEAtom _a, IntInt _ii)
    {
        a = _a;
        ii = _ii;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
