///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Template.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.types;


/**
 * Template values.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:29 $
 */
public class Template implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public int flag;

    /**
     *  Description of the Field
     */
    public int n1;

    /**
     *  Description of the Field
     */
    public int n2;

    /**
     *  Description of the Field
     */
    public int n3;

    /**
     *  Description of the Field
     */
    public int n4;

    /**
     *  Description of the Field
     */
    public short count;

    /**
     *  Description of the Field
     */
    public short elem;
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
