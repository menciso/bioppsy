///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEInternalCoord.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.types;

import joelib.molecule.*;


/**
 * Internal coordinates for three atoms.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/07/25 20:43:29 $
 */
public class JOEInternalCoord implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    public JOEAtom _a;
    public JOEAtom _b;
    public JOEAtom _c;
    public double _ang;
    public double _dst;
    public double _tor;

    //~ Constructors ///////////////////////////////////////////////////////////

    public JOEInternalCoord()
    {
        this(null, null, null);
    }

    public JOEInternalCoord(JOEAtom a, JOEAtom b, JOEAtom c)
    {
        _a = a;
        _b = b;
        _c = c;
        _dst = _ang = _tor = 0.0;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
