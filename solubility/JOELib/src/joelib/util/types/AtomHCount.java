///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomHCount.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.types;

import joelib.molecule.*;


/**
 * Atom and number of hydrogen atoms.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:29 $
 */
public class AtomHCount implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public JOEAtom atom;

    /**
     *  Description of the Field
     */
    public int hCount;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the AtomHCount object
     *
     * @param  _atom    Description of the Parameter
     * @param  _hCount  Description of the Parameter
     */
    public AtomHCount(JOEAtom _atom, int _hCount)
    {
        atom = _atom;
        hCount = _hCount;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
