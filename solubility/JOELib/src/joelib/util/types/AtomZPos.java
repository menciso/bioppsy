///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomZPos.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.types;

import joelib.molecule.JOEAtom;


/**
 * Atom and double z value.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:29 $
 */
public class AtomZPos implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public JOEAtom atom;

    /**
     *  Description of the Field
     */
    public double z;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the AtomZPos object
     *
     * @param  _atom  Description of the Parameter
     * @param  _z     Description of the Parameter
     */
    public AtomZPos(JOEAtom _atom, double _z)
    {
        atom = _atom;
        z = _z;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Compare this object with another object.
     *
     * @param  obj  the reference object with which to compare.
     * @return      <tt>true</tt> only if the specified object is also a
     *      comparator and it imposes the same ordering as this comparator.
     * @see         java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof AtomZPos)
        {
            throw new ClassCastException("Object must of type AtomZPos");
        }

        AtomZPos a = (AtomZPos) obj;

        if (this.z == a.z)
        {
            return true;
        }

        return false;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
