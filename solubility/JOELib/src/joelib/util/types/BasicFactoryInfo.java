///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BasicFactoryInfo.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:43:29 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.types;


/**
 * Create informations for a class which can be get by a factory class using Java reflection.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:29 $
 */
public class BasicFactoryInfo implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Base directory for the descriptorion for the class.
     */
    protected String descriptionFile;

    /**
     *  Name to access the class.
     */
    protected String name;

    /**
     *  Representation for class with package path and class name.
     */
    protected String representation;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Create informations for a class which can be get by a factory class.
     *
     * @param  _name             name to access the class
     * @param  _representation   representation for class with package path and class name
     * @param  _descriptionFile  base directory for the descriptorion for the class
     */
    public BasicFactoryInfo(String _name, String _representation,
        String _descriptionFile)
    {
        name = _name;
        representation = _representation;
        descriptionFile = _descriptionFile;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets base directory for the descriptorion for the class.
     *
     * @param  _descriptionFile  base directory for the descriptorion for the class
     */
    public void setDescriptionFile(String _descriptionFile)
    {
        descriptionFile = _descriptionFile;
    }

    /**
     *  Gets base directory for the descriptorion for the class.
     *
     * @return    base directory for the descriptorion for the class
     */
    public String getDescriptionFile()
    {
        return descriptionFile;
    }

    public void setName(String _name)
    {
        name = _name;
    }

    /**
     * Gets name to access the class.
     *
     * @return    name to access the class
     */
    public String getName()
    {
        return name;
    }

    public void setRepresentation(String _representation)
    {
        representation = _representation;
    }

    /**
     * Representation for class with package path and class name.
     *
     * @return    representation for class with package path and class name
     */
    public String getRepresentation()
    {
        return representation;
    }

    /**
     * Gets informations for a class which can be get by a factory class.
     *
     * @return    informations for a class which can be get by a factory class
     */
    public String toString()
    {
        StringBuffer sb = new StringBuffer(100);

        sb.append("<name:");
        sb.append(name);
        sb.append(", representation class:");
        sb.append(representation);
        sb.append(", description file:");
        sb.append(descriptionFile);
        sb.append(">");

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
