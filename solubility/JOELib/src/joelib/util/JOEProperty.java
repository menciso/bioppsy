///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEProperty.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2004/07/25 20:43:28 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util;


/**
 * JOELib property representation.
 *
 * <p>
 * Properties will be defined here for three reasons:
 * <ol>
 * <li> Type checking, for all possible parameters you can imagine
 * <li> Is this an optional parameter entry ?
 * <li> Allow helper classes to access these data elements (easier)...
 * </ol>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/07/25 20:43:28 $
 * @see JOEPropertyHelper
 */
public class JOEProperty
{
    //~ Instance fields ////////////////////////////////////////////////////////

    protected Object defaultProperty;
    protected String description;
    protected String propName;
    protected String representation;

    /**
     * <tt>true</tt> if this parameter is optional, <tt>false</tt> otherwise.
     */
    protected boolean optional;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for a JOELib property without default value.
     *
     * <p>
     * The <tt>_representation</tt> defines the object type. Here are some examples
     * <blockquote><pre>
     * java.lang.Boolean      - {@link Boolean}
     * [Ljava.lang.String;    - {@link String} array !!!
     * java.lang.Integer      - {@link Integer}
     * </pre></blockquote>
     *
     * @param  _name             name of the parameter
     * @param  _representation   data type (representation) of the parameter.
     *                                                         Arrays looks like: '[Lclass;', e.g. '[Ljava.lang.String;'
     * @param  _description  description of this parameter
     * @param  _optional  <tt>true</tt> if this parameter is optional
     */
    public JOEProperty(String _name, String _representation,
        String _description, boolean _optional)
    {
        this(_name, _representation, _description, _optional, null);
    }

    /**
     *  Constructor for a JOELib property.
     *
     * <p>
     * The <tt>_representation</tt> defines the object type. Here are some examples
     * <blockquote><pre>
     * java.lang.Boolean      - {@link Boolean}
     * [Ljava.lang.String;    - {@link String} array !!!
     * java.lang.Integer      - {@link Integer}
     * </pre></blockquote>
     *
     *
     *
     * @param  _name             name of the parameter
     * @param  _representation   data type (representation) of the parameter.
     *                                                         Arrays looks like: '[Lclass;', e.g. '[Ljava.lang.String;'.
     * @param  _description  description of this parameter
     * @param  _optional  <tt>true</tt> if this parameter is optional
     * @param  _defaultProperty  default value of this parameter which must be of type <tt>_representation</tt>
     */
    public JOEProperty(String _name, String _representation,
        String _description, boolean _optional, Object _defaultProperty)
    {
        propName = _name;
        representation = _representation;
        description = _description;
        optional = _optional;
        defaultProperty = _defaultProperty;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Object setDefaultProperty(Object defaultObj)
    {
        Object tmp = defaultProperty;
        defaultProperty = defaultObj;

        return tmp;
    }

    public Object getDefaultProperty()
    {
        return defaultProperty;
    }

    public boolean isOptional()
    {
        return optional;
    }

    public boolean equals(Object obj)
    {
        if (obj instanceof JOEProperty)
        {
            if (propName.equals(((JOEProperty) obj).propName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
