///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DatabaseInterface.java,v $
//  Purpose:  Descriptor base class.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:28 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.database;

import java.sql.ResultSet;

import joelib.molecule.JOEMol;


/**
 * Database interface definition.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/07/25 20:43:28 $
 */
public interface DatabaseInterface
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public void createTable(String tableName) throws Exception;

    public boolean existsTable(String tableName) throws Exception;

    public void insertMolecule(String tableName, JOEMol mol, int id)
        throws Exception;

    public ResultSet selectBy(String tableName, String select, String by,
        String value) throws Exception;
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
