///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AbstractDatabase.java,v $
//  Purpose:  Descriptor base class.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:28 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.database;

import wsi.ra.database.DatabaseConnection;

import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Category;

import joelib.algo.morgan.Morgan;
import joelib.algo.morgan.types.BasicTieResolver;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.smiles.JOEMol2Smi;

import joelib.util.types.StringString;


/**
 * Helper class to access and store molecules.
 *
 * @author     wegnerj
 */
public abstract class AbstractDatabase implements DatabaseInterface
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.util.database.AbstractDatabase");

    //~ Instance fields ////////////////////////////////////////////////////////

    public Morgan morgan = new Morgan(new BasicTieResolver());
    public StringString propertyHASH = new StringString("HASH", "BIGINT");
    public StringString propertyID = new StringString("ID", "BIGINT");
    public StringString propertyNAME = new StringString("NAME", "VARCHAR(100)");
    public StringString propertySHASH = new StringString("SHASH", "BIGINT");

    //~ Constructors ///////////////////////////////////////////////////////////

    public AbstractDatabase()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public abstract void createTable(String tableName)
        throws Exception;

    public abstract boolean existsTable(String tableName)
        throws Exception;

    public abstract void insertMolecule(String tableName, JOEMol mol, int id)
        throws Exception;

    /**
     * Sloppy Hashcode which uses only topological informations
     * without E/Z isomerism and S/R chirality. This hascode is really helpfull
     * to identify duplicate topological molecule entries.<br>
     * <br>
     * Strict SMILES hashcode which uses only the (hopefully) unique SMILES
     * pattern. Unfortunetaly the Morgen algorithm produces not generally
     * Unique renumberings (depends on the tie resolvers !!!), so
     * different numberings causes different SMILES patterns and
     * different hashcodes !:-(<br>
     * <br>
     * But this hashcode can be used to check for molecules, which exists
     * already in their actual form taking topological, E/Z isomerism and
     * S/R chirality informations into account.
     * <br>
     * The hashcode uses also partial charges, so you will get another result
     * if you are using non-default (Gasteiger-Marsili) partial charges.
     *
     * @param mol
     * @return int value[0] contains the sloppy hashcode, value[1] the strict
     * SMILES hascode
     */
    public static int getHashcode(JOEMol mol)
    {
        int hash = mol.numRotors();
        JOEAtom atom;

        // get number of SSSR
        hash = (31 * hash) + mol.getSSSR().size();

        for (int i = 1; i <= mol.numAtoms(); i++)
        {
            atom = mol.getAtom(i);

            hash = (31 * hash) + atom.getIdx();
            hash = (31 * hash) + atom.getAtomicNum();
            hash = (31 * hash) + atom.getHvyValence();
            hash = (31 * hash) + atom.getImplicitValence();
            hash = (31 * hash) + ((int) (atom.getPartialCharge() * 100.0));

            if (atom.isInRing())
            {
                hash = (31 * hash) + 1;
            }
            else
            {
                hash = (31 * hash) + 2;
            }

            //System.out.println("atom "+atom.getIdx()+" is aromatic "+atom.isAromatic());
        }

        return hash;
    }

    /**
     * Sloppy Hashcode which uses only topological informations
     * without E/Z isomerism and S/R chirality
     */
    public int[] getMoleculeHASH(JOEMol mol)
    {
        JOEMol tMol = (JOEMol) mol.clone(false);
        tMol.deleteHydrogens();

        morgan.calculate(tMol);

        JOEMol rMol = morgan.renumber(tMol);

        return new int[]{getHashcode(rMol), getSMILESHashcode(rMol)};
    }

    /**
     * Strict SMILES hashcode which uses the (hopefully) unique SMILES
     * pattern. Unfortunately the Morgen algorithm produces not generally
     * Unique renumberings (depends on the tie resolvers !!!), so
     * different numberings causes different SMILES patterns and
     * different hashcodes !:-(<br>
     * But this hashcode can be used to check for molecules, which exists
     * already in their actual form taking topological, E/Z isomerism and
     * S/R chirality informations into account as represented in the
     * joelib/smiles/JOEMol2Smi class.
     */
    public static int getSMILESHashcode(JOEMol mol)
    {
        JOEMol2Smi m2s = new JOEMol2Smi();

        m2s.init();
        m2s.correctAromaticAmineCharge(mol);

        StringBuffer smiles = new StringBuffer(1000);
        m2s.createSmiString(mol, smiles);

        String smilesS = smiles.toString();

        //System.out.println("SMILES: "+smilesS);
        if (smilesS != null)
        {
            return smilesS.hashCode();
        }
        else
        {
            return 0;
        }
    }

    public ResultSet selectBy(String tableName, String select, String by,
        String value) throws Exception
    {
        Statement statement = null;

        //		PreparedStatement insertStatement = null;
        //		Connection connection = null;
        if (DatabaseConnection.instance().isConnectionAvailable())
        {
            //			connection = DatabaseConnection.instance().getConnection();
            statement = DatabaseConnection.instance().getStatement();

            statement.execute("SELECT " + select + " FROM " + tableName +
                " WHERE " + by + " = '" + value + "'");

            return statement.getResultSet();
        }

        return null;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
