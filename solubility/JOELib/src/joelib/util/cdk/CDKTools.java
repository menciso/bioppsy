///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: CDKTools.java,v $
//  Purpose:  Connection to the tools of the Chemical Development Kit (CDK).
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/08/27 09:30:45 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.cdk;

import org.apache.log4j.*;

import org.openscience.cdk.*;
import org.openscience.cdk.layout.*;
import org.openscience.cdk.libio.joelib.Convertor;

import joelib.molecule.*;


/**
 * Helper class to faciliate access to tools of the
 * <a href="http://sourceforge.net/projects/cdk" target="_top">Chemical
 * Development Kit (CDK)</a>.
 *
 * @author     wegnerj
 * @license GPL
 */
public class CDKTools
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.util.cdk.CDKTools");

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the CDKTools.
     *
     */
    public CDKTools()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public static boolean generate2D(JOEMol mol)
    {
        Molecule cdkMol = Convertor.convert(mol);
        StructureDiagramGenerator layout = new StructureDiagramGenerator(cdkMol);

        try
        {
            layout.generateCoordinates();
        }
         catch (Exception ex)
        {
            //logger.error(ex.toString());
            ex.printStackTrace();

            return false;
        }

        Molecule tmpCDKmol = layout.getMolecule();
        Atom[] cdkAtoms = tmpCDKmol.getAtoms();

        for (int i = 0; i < cdkAtoms.length; i++)
        {
            mol.getAtom(i + 1).setVector(cdkAtoms[i].getX2d(),
                cdkAtoms[i].getY2d(), 0.0);
        }

        // remove all 3D coordinates in all atoms
        // or the converter will return the 3D
        // coordinates, which were all zero.
        //
        //for (int i = 0; i < cdkAtoms.length; i++) {
        //  cdkAtoms[i].setPoint3D(null);
        //}
        //JOEMol newMol=Convertor.convert(layout.getMolecule());
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
