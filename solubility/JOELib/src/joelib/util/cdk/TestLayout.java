///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TestLayout.java,v $
//  Purpose:  Example for generating 2D coordinates from SMILES.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/03/15 23:16:46 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util.cdk;

import org.apache.log4j.Category;

import joelib.io.IOTypeHolder;

import joelib.molecule.JOEMol;

import joelib.test.TestSMILES;


/**
 * Example for generating 2D coordinates from SMILES.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/03/15 23:16:46 $
 */
public class TestLayout extends TestSMILES
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.util.cdk.TestLayout");

    //~ Constructors ///////////////////////////////////////////////////////////

    public TestLayout()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        TestSMILES joeMolTest = new TestSMILES();

        if (args.length != 2)
        {
            joeMolTest.usage();
            System.exit(0);
        }
        else
        {
            JOEMol mol = joeMolTest.test(args[0],
                    IOTypeHolder.instance().getIOType("SDF"),
                    IOTypeHolder.instance().getIOType("SDF"));

            // Layout test
            System.out.println("Generate 2D coordinates:");
            CDKTools.generate2D(mol);
            System.out.println(mol);

            joeMolTest.write(mol, args[1],
                IOTypeHolder.instance().getIOType("SDF"));
        }

        System.exit(0);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
