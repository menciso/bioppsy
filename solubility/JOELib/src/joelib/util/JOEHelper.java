///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEHelper.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.16 $
//            $Date: 2004/07/25 20:43:28 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util;


/**
 * JOELib helper methods.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.16 $, $Date: 2004/07/25 20:43:28 $
 */
public class JOEHelper
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public final static String CONTACT_E_MAIL = "wegnerj@informatik.uni-tuebingen.de";

    /**
     *  Description of the Field
     */
    public final static int BUFF_SIZE = 1024;

    /**
     *  Description of the Field
     */
    public static int OEPolarGrid = 0x01;

    /**
     *  Description of the Field
     */
    public static int OELipoGrid = 0x02;

    /**
     *  Description of the Field
     */
    public static int PT_CATION = 1;

    /**
     *  Description of the Field
     */
    public static int PT_ANION = 2;

    /**
     *  Description of the Field
     */
    public static int PT_ACCEPTOR = 3;

    /**
     *  Description of the Field
     */
    public static int PT_POLAR = 4;

    /**
     *  Description of the Field
     */
    public static int PT_DONOR = 5;

    /**
     *  Description of the Field
     */
    public static int PT_HYDROPHOBIC = 6;

    /**
     *  Description of the Field
     */
    public static int PT_OTHER = 7;

    /**
     *  Description of the Field
     */
    public static int PT_METAL = 8;

    //~ Methods ////////////////////////////////////////////////////////////////

    //    private static Hashtable hasInterfaceCache = new Hashtable(500);

    /**
     *  Description of the Method
     *
     * @param a  Description of the Parameter
     * @param b  Description of the Parameter
     * @return   Description of the Return Value
     */
    public final static boolean EQ(String a, String b)
    {
        return a.equals(b);
    }

    /**
     *  Description of the Method
     *
     * @param a  Description of the Parameter
     * @param b  Description of the Parameter
     * @param n  Description of the Parameter
     * @return   Description of the Return Value
     */
    public final static boolean EQn(String a, String b, int n)
    {
        return (a.substring(0, n - 1).equals(b.substring(0, n - 1)));
    }

    /**
     *  Description of the Method
     *
     * @param x  Description of the Parameter
     * @return   Description of the Return Value
     */
    public final static double SQUARE(double x)
    {
        return x * x;
    }

    /**
     *  Gets the unsatType attribute of the JOEHelper class
     *
     * @param x  Description of the Parameter
     * @return   The unsatType value
     */
    public static boolean isUnsatType(String x)
    {
        return (EQ(x, "Car") || EQ(x, "C2") || EQ(x, "Sox") || EQ(x, "Sac") ||
        EQ(x, "Pac") || EQ(x, "So2"));
    }

    /**
     * Checks if an class implements an interface. This version allows short Interface name definitions.
     * E.g. joelib.io.PropertyWriter or (faster) PropertyWriter.
     *
     * @todo                 Cache search routine for object/interface pairs
     * @todo                 Use shorter interface names
     * @param o              Description of the Parameter
     * @param interfaceName  Description of the Parameter
     * @return               Description of the Return Value
     */
    public static boolean hasInterface(Object o, String interfaceName)
    {
        //    	System.out.println("check: "+o);
        if (o == null)
        {
            return false;
        }

        Class c = o.getClass();

        int size;

        while (true)
        {
            Class[] theInterfaces = c.getInterfaces();
            size = theInterfaces.length;

            for (int i = 0; i < size; i++)
            {
                String _interfaceName = theInterfaces[i].getName();

                //           System.out.println("class:"+o.getClass().getName()+"_interfaceName:"+_interfaceName);
                // absoulutely correct, but a little bit slower
                //               if(_interfaceName.equals(interfaceName))return true;
                //               if (_interfaceName.lastIndexOf(interfaceName) != -1)
                // faster and more problematic by multiple interface names, if you choose short
                // interface names without package names ! Your (efficiency) choice !;-)
                if (_interfaceName.regionMatches(_interfaceName.length() -
                            interfaceName.length(), interfaceName, 0,
                            interfaceName.length()))
                {
                    return true;
                }
            }

            theInterfaces = null;
            c = c.getSuperclass();

            if (c == null)
            {
                break;
            }
        }

        c = null;

        return false;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
