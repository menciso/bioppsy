///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JHM.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.25 $
//            $Date: 2004/07/25 20:43:28 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util;

import wsi.ra.tool.PropertyHolder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.ext.ExternalHelper;

import joelib.io.IOType;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;
import joelib.io.PropertyWriter;

import joelib.math.Matrix3x3;
import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;
import joelib.molecule.JOERTree;

import joelib.smarts.JOESmartsPattern;

import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;
import joelib.util.iterator.NbrAtomIterator;
import joelib.util.types.JOEInternalCoord;


/**
 * JOELib hepler methods.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.25 $, $Date: 2004/07/25 20:43:28 $
 */
public class JHM
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance("joelib.util.JHM");

    /**
     *  Description of the Field
     */
    public final static double RAD_TO_DEG = 180.0f / Math.PI;

    /**
     *  Description of the Field
     */
    public final static double DEG_TO_RAD = Math.PI / 180.0f;
    public static final String eol = System.getProperty("line.separator", "\n");
    private final static int OE_RTREE_CUTOFF = 20;
    private static JHM jhm;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JHM object
     */
    private JHM()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the currentValence attribute of the JHM class
     *
     * @param  atom  Description of the Parameter
     * @return       The currentValence value
     */
    public static int getCurrentValence(JOEAtom atom)
    {
        int count = atom.getImplicitValence();

        JOEBond bond;
        BondIterator bit = atom.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isKDouble())
            {
                count++;
            }
            else if (bond.isKTriple())
            {
                count += 2;
            }
        }

        return (count);
    }

    /*public static int PDBSort(ChainsAtom arg1[], ChainsAtom arg2[])
     *{
     *throw Exception("use ChainAtomComparator");
     *}
     *static void outputPDBFile(ChainsMolecule mol, OutputStream os)
     *{
     *int src,dst;
     *ChainsAtom atom;
     *String ptr;
     *char tmpc[]=new char[1];
     *int i;
     *PrintfStream fp = new PrintfStream(os);
     *for( i=0; i<mol.acount; i++ )
     *PDBOrder[i] = mol.atom[i];
     *qsort(PDBOrder,mol.acount, new ChainAtomComparator());
     *ptr = mol.name;
     *if( ptr!=null )
     *{
     *fp.print("COMPND    ");
     *fp.prinln( ptr.getBytes() );
     *}
     *for( i=0; i<mol.acount; i++ )
     *{
     *atom = PDBOrder[i];
     *atom.serno = i+1;
     *if( atom.hetflag )
     *{
     *fp.print( "HETATM" );
     *}
     *else
     *{
     *fp.print( "ATOM   " );
     *}
     *fp.printf("%4d ", atom.serno);
     *if( atom.atomid == -1 )
     *{
     *fp.printf("%s  ", chainsElem[atom.elem].symbol);
     *}
     *else
     *{
     *if( atom.elem == 1 )
     *{
     *if( atom.hcount )
     *{
     *tmpc[0] = atom.hcount+'0';
     *fp.write(tmpc);
     *}
     *else
     *{
     *tmpc[0] = ' ';
     *fp.write(tmpc);
     *}
     *fp.printf("H%.2s", chainsAtomName[atom.atomid]+2);
     *}
     *else
     *{
     *fp.printf( ,"%.4s", chainsAtomName[atom.atomid]);
     *}
     *fp.printf( " %s ",  chainsResName[atom.resid]);
     *fp.printf( "%c%4d", atom.chain);
     *fp.printf( "%4d",   atom.resno);
     *fp.printf( "    %8.3lf",  atom.x);
     *fp.printf( "%8.3lf",      atom.y);
     *fp.printf( "%8.3lf",      atom.z);
     *fp.println("  1.00  0.00");
     *}
     *for( i=0; i<mol.bcount; i++ )
     *{
     *if( mol.bond[i].flag & BF_DOUBLE )
     *{
     *src = mol.atom[mol.bond[i].src].serno;
     *dst = mol.atom[mol.bond[i].dst].serno;
     *fp.printf( "CONECT%5d", src);
     *fp.printf( "%5d",       dst);
     *fp.printf( "%5d\n",     dst);
     *fp.printf( "CONECT%5d", dst);
     *fp.printf( "%5d",       src);
     *fp.printf( "%5d\n",     src);
     *}
     *}
     *fp.println("END ");
     *} */

    /**
     * @param  dffv  {@link java.util.Vector} of <tt>int[1]</tt>
     * @param  mol   Description of the Parameter
     * @param  bv    Description of the Parameter
     * @return       The dFFVector value
     */
    public static boolean getDFFVector(JOEMol mol, Vector dffv, JOEBitVec bv)
    {
        dffv.clear();
        dffv.setSize(mol.numAtoms());

        int dffcount;

        int natom;
        JOEBitVec used = new JOEBitVec();
        JOEBitVec curr = new JOEBitVec();
        JOEBitVec next = new JOEBitVec();
        JOEAtom atom;
        JOEAtom atom1;
        JOEBond bond;

        next.clear();

        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (bv.get(atom.getIdx()))
            {
                ((int[]) dffv.get(atom.getIdx() - 1))[0] = 0;

                continue;
            }

            dffcount = 0;
            used.clear();
            curr.clear();
            used.setBitOn(atom.getIdx());
            curr.setBitOn(atom.getIdx());

            while (!curr.isEmpty() && ((bv.and(curr)).size() == 0))
            {
                next.clear();

                //  	  for(natom=curr.nextSetBit(0); natom>=0; natom=curr.nextSetBit(natom+1))
                for (natom = curr.nextBit(-1); natom != curr.endBit();
                        natom = curr.nextBit(natom))
                {
                    atom1 = mol.getAtom(natom);

                    BondIterator bit = atom1.bondIterator();

                    while (bit.hasNext())
                    {
                        bond = bit.nextBond();

                        if (!used.bitIsOn(bond.getNbrAtomIdx(atom1)) &&
                                !curr.bitIsOn(bond.getNbrAtomIdx(atom1)))
                        {
                            if (!(bond.getNbrAtom(atom1)).isHydrogen())
                            {
                                next.setBitOn(bond.getNbrAtomIdx(atom1));
                            }
                        }
                    }
                }

                used.orSet(next);
                curr.set(next);
                dffcount++;
            }

            dffv.set(atom.getIdx() - 1, new int[]{dffcount});
        }

        return true;
    }

    /**
     * @param  v      of type <tt>int[1]</tt>
     * @param  value  The new intVectorToValue value
     */
    public static void setIntVectorToValue(Vector v, int value)
    {
        int[] itmp;

        for (int i = 0; i < v.size(); i++)
        {
            itmp = (int[]) v.get(i);

            if (itmp != null)
            {
                itmp[0] = value;
            }
            else
            {
                v.set(i, new int[]{value});
            }
        }
    }

    /**
     *  This methodName will rotate the coordinates of 'atoms' such that tor == ang
     *  - atoms in 'tor' should be ordered such that the 3rd atom is the pivot
     *  around which atoms rotate ang is in degrees.
     *
     * @param  atoms  {@link java.util.Vector} of <tt>int[1]</tt>
     * @param  c      The new rotorToAngle value
     * @param  ref    The new rotorToAngle value
     * @param  ang    The new rotorToAngle value
     */
    public static void setRotorToAngle(double[] c, JOEAtom[] ref, double ang,
        Vector atoms)
    {
        double v1x;
        double v1y;
        double v1z;
        double v2x;
        double v2y;
        double v2z;
        double v3x;
        double v3y;
        double v3z;
        double c1x;
        double c1y;
        double c1z;
        double c2x;
        double c2y;
        double c2z;
        double c3x;
        double c3y;
        double c3z;
        double c1mag;
        double c2mag;
        double radang;
        double costheta;
        double[] m = new double[9];
        double x;
        double y;
        double z;
        double mag;
        double rotang;
        double sn;
        double cs;
        double t;
        double tx;
        double ty;
        double tz;

        int[] tor = new int[4];
        tor[0] = ref[0].getCIdx();
        tor[1] = ref[1].getCIdx();
        tor[2] = ref[2].getCIdx();
        tor[3] = ref[3].getCIdx();

        //
        //calculate the torsion angle
        //
        v1x = c[tor[0]] - c[tor[1]];
        v2x = c[tor[1]] - c[tor[2]];
        v1y = c[tor[0] + 1] - c[tor[1] + 1];
        v2y = c[tor[1] + 1] - c[tor[2] + 1];
        v1z = c[tor[0] + 2] - c[tor[1] + 2];
        v2z = c[tor[1] + 2] - c[tor[2] + 2];
        v3x = c[tor[2]] - c[tor[3]];
        v3y = c[tor[2] + 1] - c[tor[3] + 1];
        v3z = c[tor[2] + 2] - c[tor[3] + 2];

        c1x = (v1y * v2z) - (v1z * v2y);
        c2x = (v2y * v3z) - (v2z * v3y);
        c1y = (-v1x * v2z) + (v1z * v2x);
        c2y = (-v2x * v3z) + (v2z * v3x);
        c1z = (v1x * v2y) - (v1y * v2x);
        c2z = (v2x * v3y) - (v2y * v3x);
        c3x = (c1y * c2z) - (c1z * c2y);
        c3y = (-c1x * c2z) + (c1z * c2x);
        c3z = (c1x * c2y) - (c1y * c2x);

        c1mag = (c1x * c1x) + (c1y * c1y) + (c1z * c1z);
        c2mag = (c2x * c2x) + (c2y * c2y) + (c2z * c2z);

        if ((c1mag * c2mag) < 0.01)
        {
            costheta = 1.0;
        }

        //avoid div by zero error
        else
        {
            costheta = ((c1x * c2x) + (c1y * c2y) + (c1z * c2z)) / (Math.sqrt(c1mag * c2mag));
        }

        if (costheta < -0.999999)
        {
            costheta = -0.999999f;
        }

        if (costheta > 0.999999)
        {
            costheta = 0.999999f;
        }

        if (((v2x * c3x) + (v2y * c3y) + (v2z * c3z)) > 0.0)
        {
            radang = -Math.acos(costheta);
        }
        else
        {
            radang = Math.acos(costheta);
        }

        //
        // now we have the torsion angle (radang) - set up the rot matrix
        //
        //find the difference between current and requested
        rotang = (DEG_TO_RAD * ang) - radang;

        sn = Math.sin(rotang);
        cs = Math.cos(rotang);
        t = 1 - cs;

        //normalize the rotation vector
        mag = Math.sqrt((v2x * v2x) + (v2y * v2y) + (v2z * v2z));
        x = v2x / mag;
        y = v2y / mag;
        z = v2z / mag;

        //set up the rotation matrix
        m[0] = (t * x * x) + cs;
        m[1] = (t * x * y) + (sn * z);
        m[2] = (t * x * z) - (sn * y);
        m[3] = (t * x * y) - (sn * z);
        m[4] = (t * y * y) + cs;
        m[5] = (t * y * z) + (sn * x);
        m[6] = (t * x * z) + (sn * y);
        m[7] = (t * y * z) - (sn * x);
        m[8] = (t * z * z) + cs;

        //
        //now the matrix is set - time to rotate the atoms
        //
        tx = c[tor[1]];
        ty = c[tor[1] + 1];
        tz = c[tor[1] + 2];

        int j;

        for (int i = 0; i < atoms.size(); i++)
        {
            int[] atom = (int[]) atoms.get(i);
            j = (atom[0] - 1) * 3;
            c[j] -= tx;
            c[j + 1] -= ty;
            c[j + 2] -= tz;

            x = (c[j] * m[0]) + (c[j + 1] * m[1]) + (c[j + 2] * m[2]);
            y = (c[j] * m[3]) + (c[j + 1] * m[4]) + (c[j + 2] * m[5]);
            z = (c[j] * m[6]) + (c[j + 1] * m[7]) + (c[j + 2] * m[8]);
            c[j] = x;
            c[j + 1] = y;
            c[j + 2] = z;
            c[j] += tx;
            c[j + 1] += ty;
            c[j + 2] += tz;
        }
    }

    public static String getTempFileBase()
    {
        Properties prop = PropertyHolder.instance().getProperties();
        String tempDir = "joelib.temporary.directory." +
            ExternalHelper.getOperationSystemName();
        String temp = prop.getProperty(tempDir);

        if (temp == null)
        {
            logger.error("You must define a temporary directory in  '" +
                tempDir + "'.");

            return null;
        }

        String fileSeparator = System.getProperty("file.separator");

        return temp + fileSeparator;
    }

    /**
     * @param  prv   {@link java.util.Vector} of <tt>JOERTree</tt>
     * @param  atom  Description of the Parameter
     * @param  vt    Description of the Parameter
     * @param  bv    Description of the Parameter
     */
    public static void buildOERTreeVector(JOEAtom atom, JOERTree prv,
        JOERTree[] vt, JOEBitVec bv)
    {
        vt[atom.getIdx()] = new JOERTree(atom, prv);

        int i;
        JOEAtom nbr;
        JOEMol mol = atom.getParent();
        JOEBitVec curr = new JOEBitVec();
        JOEBitVec next = new JOEBitVec();
        JOEBitVec used;

        curr.set(atom.getIdx());

        //  System.out.println("b20"+bv.get(20));
        used = JOEBitVec.or(bv, curr);

        //  System.out.println("b20"+used.get(20));
        //  System.out.println("first: "+atom.getIdx());
        int level = 0;

        for (;;)
        {
            next.clear();

            //      for(i=curr.nextSetBit(0); i>=0; i=curr.nextSetBit(i+1))
            for (i = curr.nextBit(0); i != bv.endBit(); i = curr.nextBit(i))
            {
                //        System.out.println("bit "+i);
                atom = mol.getAtom(i);

                NbrAtomIterator nait = atom.nbrAtomIterator();

                while (nait.hasNext())
                {
                    nbr = nait.nextNbrAtom();

                    //          System.out.println("nbr "+nbr.getIdx()+" u "+used.get(nbr.getIdx()));
                    if (!used.get(nbr.getIdx()))
                    {
                        next.set(nbr.getIdx());
                        used.set(nbr.getIdx());

                        //            System.out.println("tree "+nbr.getIdx());
                        vt[nbr.getIdx()] = new JOERTree(nbr, vt[atom.getIdx()]);
                    }
                }
            }

            if (next.size() == 0)
            {
                break;
            }

            curr.set(next);
            level++;

            if (level > OE_RTREE_CUTOFF)
            {
                break;
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     */
    public static void correctBadResonanceForm(JOEMol mol)
    {
        JOEBond b1;
        JOEBond b2;
        JOEBond b3;
        JOEAtom a1;
        JOEAtom a2;
        JOEAtom a3;
        JOEAtom a4;
        Vector mlist;

        //Vector of int[]
        //carboxylic acid
        JOESmartsPattern acid = new JOESmartsPattern();
        acid.init("[oD1]c[oD1]");

        //Vector vec;
        int[] itmp;

        if (acid.match(mol))
        {
            mlist = acid.getUMapList();

            for (int i = 0; i < mlist.size(); i++)
            {
                itmp = (int[]) mlist.get(i);

                //vec = (Vector)mlist.get(i);
                //itmp = (int[])vec.get(0); a1 = mol.getAtom( itmp[0] );
                //itmp = (int[])vec.get(1); a2 = mol.getAtom( itmp[0] );
                //itmp = (int[])vec.get(2); a3 = mol.getAtom( itmp[0] );
                a1 = mol.getAtom(itmp[0]);
                a2 = mol.getAtom(itmp[1]);
                a3 = mol.getAtom(itmp[2]);
                b1 = a2.getBond(a1);
                b2 = a2.getBond(a3);

                if ((b1 == null) || (b2 == null))
                {
                    continue;
                }

                b1.setKDouble();
                b2.setKSingle();
            }
        }

        //phosphonic acid
        JOESmartsPattern phosphate = new JOESmartsPattern();
        phosphate.init("[p]([oD1])([oD1])([oD1])[#6,#8]");

        //Vector vec;
        if (phosphate.match(mol))
        {
            mlist = phosphate.getUMapList();

            for (int i = 0; i < mlist.size(); i++)
            {
                //vec   = (Vector)mlist.get(i);
                //itmp  = (int[])vec.get(0); a1 = mol.getAtom( itmp[0] );
                //itmp  = (int[])vec.get(1); a2 = mol.getAtom( itmp[0] );
                //itmp  = (int[])vec.get(2); a3 = mol.getAtom( itmp[0] );
                //itmp  = (int[])vec.get(3); a4 = mol.getAtom( itmp[0] );
                itmp = (int[]) mlist.get(i);
                a1 = mol.getAtom(itmp[0]);
                a2 = mol.getAtom(itmp[1]);
                a3 = mol.getAtom(itmp[2]);
                a4 = mol.getAtom(itmp[3]);
                b1 = a1.getBond(a2);
                b2 = a1.getBond(a3);
                b3 = a1.getBond(a4);

                if ((b1 == null) || (b2 == null) || (b3 == null))
                {
                    continue;
                }

                b1.setKDouble();
                b2.setKSingle();
                b3.setKSingle();
            }
        }

        //amidene and guanidine
        JOESmartsPattern amidene = new JOESmartsPattern();
        amidene.init("[nD1]c([nD1])*");

        if (amidene.match(mol))
        {
            mlist = amidene.getUMapList();

            for (int i = 0; i < mlist.size(); i++)
            {
                //vec = (Vector)mlist.get(i);
                //itmp = (int[])vec.get(0);  a1 = mol.getAtom( itmp[0] );
                //itmp = (int[])vec.get(1);  a2 = mol.getAtom( itmp[0] );
                //itmp = (int[])vec.get(2);  a3 = mol.getAtom( itmp[0] );
                itmp = (int[]) mlist.get(i);
                a1 = mol.getAtom(itmp[0]);
                a2 = mol.getAtom(itmp[1]);
                a3 = mol.getAtom(itmp[2]);

                b1 = a2.getBond(a1);
                b2 = a2.getBond(a3);

                if ((b1 == null) || (b2 == null))
                {
                    continue;
                }

                b1.setKDouble();
                b2.setKSingle();
            }
        }
    }

    /**
     * @param  va          {@link java.util.Vector} of <tt>JOEAtom</tt>
     * @param  mol         Description of the Parameter
     * @param  i           Description of the Parameter
     * @param  maxv        Description of the Parameter
     * @param  secondpass  Description of the Parameter
     * @return             Description of the Return Value
     */
    public static boolean expandKekule(JOEMol mol, Vector va, AtomIterator ii,
        int[] maxv, boolean secondpass)
    {
        AtomIterator i = (AtomIterator) ii.clone();

        //    System.out.println("expKekul:"+i.actualIndex()+" "+i.hasNext());
        if (!i.hasNext())
        {
            //check to see that the ideal valence has been achieved for all atoms
            JOEAtom atom;
            AtomIterator ait = new AtomIterator(va);

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                //let erroneously aromatic carboxylates pass
                if (atom.isOxygen() && (atom.getValence() == 1))
                {
                    continue;
                }

                if (getCurrentValence(atom) != maxv[atom.getIdx()])
                {
                    return false;
                }
            }

            return true;
        }

        //jump to next atom in list if current atom doesn't have any attached
        //aromatic bonds
        JOEBond bond;
        JOEAtom atom = (JOEAtom) i.actual();

        BondIterator bit = atom.bondIterator();
        boolean done = true;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.getBO() == 5)
            {
                done = false;

                break;
            }
        }

        if (done)
        {
            i.setActualIndex(i.actualIndex() + 1);

            return (expandKekule(mol, va, i, maxv, secondpass));

            //      i.setActualIndex( i.actualIndex()-1 );
        }

        //store list of attached aromatic atoms
        JOEAtom nbr;
        Vector vb = new Vector();

        //type JOEBond
        NbrAtomIterator nait = atom.nbrAtomIterator();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();
            bond = nait.actualBond();

            if (bond.getBO() == 5)
            {
                vb.add(bond);
                bond.setBO(1);
                bond.setKSingle();
            }
        }

        //try setting a double bond
        if (getCurrentValence(atom) < maxv[atom.getIdx()])
        {
            for (int j = 0; j < vb.size(); j++)
            {
                bond = (JOEBond) vb.get(j);
                nbr = bond.getNbrAtom(atom);

                if (getCurrentValence(nbr) <= maxv[nbr.getIdx()])
                {
                    bond.setKDouble();
                    bond.setBO(2);
                    i.setActualIndex(i.actualIndex() + 1);

                    if (expandKekule(mol, va, i, maxv, secondpass))
                    {
                        return true;
                    }

                    i.setActualIndex(i.actualIndex() - 1);
                    bond.setKSingle();
                    bond.setBO(1);
                }
            }

            if (secondpass && atom.isNitrogen() &&
                    (atom.getFormalCharge() == 0) &&
                    (atom.getImplicitValence() == 2))
            {
                atom.incrementImplicitValence();
                i.setActualIndex(i.actualIndex() + 1);

                if (expandKekule(mol, va, i, maxv, secondpass))
                {
                    return true;
                }

                i.setActualIndex(i.actualIndex() - 1);
                atom.decrementImplicitValence();
            }
        }
        else
        {
            //full valence - no double bond to set
            i.setActualIndex(i.actualIndex() + 1);

            if (expandKekule(mol, va, i, maxv, secondpass))
            {
                return true;
            }

            i.setActualIndex(i.actualIndex() - 1);

            boolean trycharge = false;

            if (secondpass && (atom.getFormalCharge() == 0))
            {
                if (atom.isNitrogen() && (atom.getHvyValence() == 3))
                {
                    trycharge = true;
                }

                if (atom.isOxygen() && (atom.getHvyValence() == 2))
                {
                    trycharge = true;
                }

                if (atom.isSulfur() && (atom.getHvyValence() == 2))
                {
                    trycharge = true;
                }
            }

            if (trycharge)
            {
                maxv[atom.getIdx()]++;
                atom.setFormalCharge(1);

                for (int j = 0; j < vb.size(); j++)
                {
                    bond = (JOEBond) vb.get(j);
                    nbr = bond.getNbrAtom(atom);

                    if (getCurrentValence(nbr) <= maxv[nbr.getIdx()])
                    {
                        bond.setKDouble();
                        bond.setBO(2);
                        i.setActualIndex(i.actualIndex() + 1);

                        if (expandKekule(mol, va, i, maxv, secondpass))
                        {
                            return true;
                        }

                        i.setActualIndex(i.actualIndex() - 1);
                        bond.setKSingle();
                        bond.setBO(1);
                    }
                }

                maxv[atom.getIdx()]--;
                atom.setFormalCharge(0);
            }

            if (secondpass && atom.isNitrogen() &&
                    (atom.getFormalCharge() == 0) &&
                    (atom.getImplicitValence() == 2))
            {
                //try protonating the nitrogen
                atom.incrementImplicitValence();
                i.setActualIndex(i.actualIndex() + 1);

                if (expandKekule(mol, va, i, maxv, secondpass))
                {
                    return (true);
                }

                i.setActualIndex(i.actualIndex() - 1);
                atom.decrementImplicitValence();
            }
        }

        //failed to find a valid solution - reset attached bonds
        for (int j = 0; j < vb.size(); j++)
        {
            bond = (JOEBond) vb.get(j);
            bond.setKSingle();
            bond.setBO(5);
        }

        return false;
    }

    /**
     * @param  path    {@link java.util.Vector} of <tt>int[1]</tt>
     * @param  mol     Description of the Parameter
     * @param  avisit  Description of the Parameter
     * @param  bvisit  Description of the Parameter
     * @param  natom   Description of the Parameter
     * @param  depth   Description of the Parameter
     */
    public static void findRings(JOEMol mol, Vector path, JOEBitVec avisit,
        JOEBitVec bvisit, int natom, int depth)
    {
        JOEAtom atom;
        JOEBond bond;
        int[] itmp;

        if (avisit.get(natom))
        {
            int j = depth - 1;

            itmp = (int[]) path.get(j--);
            bond = mol.getBond(itmp[0]);

            bond.setInRing();

            while (j >= 0)
            {
                itmp = (int[]) path.get(j--);
                bond = mol.getBond(itmp[0]);

                bond.setInRing();
                (bond.getBeginAtom()).setInRing();
                (bond.getEndAtom()).setInRing();

                if ((bond.getBeginAtomIdx() == natom) ||
                        (bond.getEndAtomIdx() == natom))
                {
                    break;
                }
            }
        }
        else
        {
            avisit.setBitOn(natom);
            atom = mol.getAtom(natom);

            BondIterator bit = atom.bondIterator();

            while (bit.hasNext())
            {
                bond = bit.nextBond();

                if (!bvisit.get(bond.getIdx()))
                {
                    itmp = (int[]) path.get(depth);
                    itmp[0] = bond.getIdx();
                    bvisit.setBitOn(bond.getIdx());

                    findRings(mol, path, avisit, bvisit,
                        bond.getNbrAtomIdx(atom), depth + 1);
                }
            }
        }
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static synchronized JHM instance()
    {
        if (jhm == null)
        {
            jhm = new JHM();
        }

        return jhm;
    }

    /**
     *
     * @param vic of type <tt>JOEInternalCoord</tt>
     */
    public static void internalToCartesian(Vector vic, JOEMol mol)
    {
        XYZVector n = new XYZVector();
        XYZVector nn = new XYZVector();
        XYZVector v1 = new XYZVector();
        XYZVector v2 = new XYZVector();
        XYZVector v3 = new XYZVector();
        JOEAtom atom = null;

        //    vector<OEAtom*>::iterator i;
        int index;

        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            index = atom.getIdx() - 1;

            JOEInternalCoord intCoord = (JOEInternalCoord) vic.get(index);

            if (index == 0)
            {
                atom.setVector(0.0f, 0.0f, 0.0f);

                continue;
            }
            else if (index == 1)
            {
                v1.setX(-intCoord._dst);
                atom.setVector(v1);

                continue;
            }
            else if (index == 2)
            {
                v1.setX(-(intCoord._dst * Math.cos(intCoord._ang)));
                v1.setZ(-(intCoord._dst * Math.sin(intCoord._ang)));
                atom.setVector(v1);

                continue;
            }

            XYZVector.sub(v1, intCoord._a.getVector(), intCoord._b.getVector());
            XYZVector.sub(v2, intCoord._a.getVector(), intCoord._c.getVector());
            XYZVector.cross(n, v1, v2);
            XYZVector.cross(nn, v1, n);
            n.normalize();
            nn.normalize();

            n.muling(-Math.sin(intCoord._tor));
            nn.muling(Math.cos(intCoord._tor));
            XYZVector.add(v3, n, nn);
            v3.normalize();
            v3.muling(intCoord._dst * Math.sin(intCoord._ang));
            v1.normalize();
            v1.muling(intCoord._dst * Math.cos(intCoord._ang));
            XYZVector.add(v2, intCoord._a.getVector(), v3);
            v2.subing(v1);

            atom.setVector(v2);
        }

        // Delete dummy atoms
        ait.reset();

        while (ait.hasNext())
        {
            if (atom.getAtomicNum() == 0)
            {
                mol.deleteAtom(atom);
            }
        }
    }

    /**
     * @param  visit  {@link java.util.Vector} of <tt>int[0]</tt>
     * @param  ival   {@link java.util.Vector} of <tt>int[0]</tt>
     * @param  atom   Description of the Parameter
     * @param  depth  Description of the Parameter
     * @return        Description of the Return Value
     */
    public static boolean kekulePropagate(JOEAtom atom, Vector visit,
        Vector ival, int depth)
    {
        int count = 0;
        JOEBond bond;
        BondIterator bit = atom.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (((int[]) visit.get(bond.getIdx()))[0] == 0)
            {
                count++;
            }
        }

        if (count == 0)
        {
            return (valenceSum(atom) == ((int[]) ival.get(atom.getIdx()))[0]);
        }

        boolean result = true;

        JOEAtom nbr;
        NbrAtomIterator nait = atom.nbrAtomIterator();

        if (valenceSum(atom) >= ((int[]) ival.get(atom.getIdx()))[0])
        {
            while (nait.hasNext())
            {
                nbr = nait.nextNbrAtom();
                bond = nait.actualBond();

                if (nbr.isAromatic() &&
                        (((int[]) visit.get(bond.getIdx()))[0] == 0))
                {
                    ((int[]) visit.get(bond.getIdx()))[0] = depth;
                    bond.setKSingle();
                    result = kekulePropagate(nbr, visit, ival, depth);

                    if (result)
                    {
                        break;
                    }

                    //          if (!result) break;
                }
            }
        }
        else if (count == 1)
        {
            while (nait.hasNext())
            {
                nbr = nait.nextNbrAtom();
                bond = nait.actualBond();

                if (nbr.isAromatic() &&
                        (((int[]) visit.get(bond.getIdx()))[0] == 0))
                {
                    ((int[]) visit.get(bond.getIdx()))[0] = depth;
                    bond.setKDouble();
                    result = kekulePropagate(nbr, visit, ival, depth);

                    //break;
                    if (result)
                    {
                        break;
                    }
                }
            }
        }

        return result;
    }

    /**
     *  Description of the Method
     *
     * @param  mol      Description of the Parameter
     * @param  a        Description of the Parameter
     * @param  b        Description of the Parameter
     * @param  one2one  Description of the Parameter
     * @return          Description of the Return Value
     */
    public static double minimumPairRMS(JOEMol mol, double[] a, double[] b,
        boolean[] one2one)
    {
        int i;
        int j;
        int k = 0;
        double min;
        double tmp;
        double d_2 = 0.0;
        JOEBitVec bset = new JOEBitVec();
        one2one[0] = true;

        Vector _atom = new Vector();

        // of type JOEAtom
        _atom.setSize(mol.numAtoms());

        for (i = 0; i < mol.numAtoms(); i++)
        {
            _atom.set(i, mol.getAtom(i + 1));
        }

        for (i = 0; i < mol.numAtoms(); i++)
        {
            min = Double.MAX_VALUE;

            for (j = 0; j < mol.numAtoms(); j++)
            {
                if ((((JOEAtom) _atom.get(i)).getAtomicNum() == ((JOEAtom) _atom.get(
                            j)).getAtomicNum()) &&
                        (((JOEAtom) _atom.get(i)).getHyb() == ((JOEAtom) _atom.get(
                            j)).getHyb()))
                {
                    if (!bset.get(j))
                    {
                        double tmp1 = a[3 * i] - b[3 * j];
                        double tmp2 = a[(3 * i) + 1] - b[(3 * j) + 1];
                        double tmp3 = a[(3 * i) + 2] - b[(3 * j) + 2];
                        tmp = (tmp1 * tmp1) + (tmp2 * tmp2) + (tmp3 * tmp3);

                        if (tmp < min)
                        {
                            k = j;
                            min = tmp;
                        }
                    }
                }
            }

            if (i != j)
            {
                one2one[0] = false;
            }

            bset.setBitOn(k);
            d_2 += min;
        }

        d_2 /= (double) mol.numAtoms();

        return Math.sqrt(d_2);
    }

    public static synchronized boolean moleculeToFile(String filename,
        JOEMol mol, IOType type, boolean writeDescriptors)
    {
        FileOutputStream out;

        try
        {
            out = new FileOutputStream(filename);
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            return false;
        }

        MoleculeFileType writer = null;

        try
        {
            writer = JOEFileFormat.getMolWriter(out, type);

            if (!writer.writeable())
            {
                logger.warn(type.getRepresentation() + " is not writeable.");

                return false;
            }

            if (!writeDescriptors &&
                    JOEHelper.hasInterface(writer, "PropertyWriter"))
            {
                ((PropertyWriter) writer).write(mol, null, false, null);
            }
            else
            {
                writer.write(mol, null);
            }

            // close molecule writer
            writer.closeWriter();
            out.close();
        }
         catch (IOException ex)
        {
            ex.printStackTrace();

            return false;
        }
         catch (MoleculeIOException ex)
        {
            ex.printStackTrace();

            return false;
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * @param  visit  {@link java.util.Vector} of <tt>int[0]</tt>
     * @param  mol    Description of the Parameter
     * @param  depth  Description of the Parameter
     */
    public static void resetVisit(JOEMol mol, Vector visit, int depth)
    {
        JOEBond bond;
        BondIterator bit = mol.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isAromatic() &&
                    (((int[]) visit.get(bond.getIdx()))[0] >= depth))
            {
                ((int[]) visit.get(bond.getIdx()))[0] = 0;
            }
        }
    }

    /**
     * @param  a          Description of the Parameter
     * @param  b          Description of the Parameter
     * @param  c          Description of the Parameter
     * @param  d          Description of the Parameter
     * @return            Description of the Return Value
     */

    //public void getChirality(JOEMol mol, Vector chirality)
    //{
    //  int itmp[];
    //
    //  chirality.setSize(mol.numAtoms()+1);
    //  setIntVectorToValue(chirality, 0);
    //
    //  JOEAtom atom;
    //  AtomIterator ait = mol.atomIterator();
    //  while(ait.hasNext())
    //  {
    //    atom = ait.nextAtom();
    //    if (atom.isChiral())
    //    {
    //    	double sv = calcSignedVolume(mol,atom);
    //    	itmp    = (int[])chirality.get(atom.getIdx()-1);
    //    	if (sv < 0.0f)	itmp[0] = -1;
    //    	else if (sv > 0.0)  itmp[0] = 1;
    //    }
    //  }
    //}

    /**
     *  Calculate the signed volume for an atom. If the atom has a valence of 3
     *  the coordinates of an attached hydrogen are calculated
     *
     * @param  a          Description of the Parameter
     * @param  b          Description of the Parameter
     * @param  c          Description of the Parameter
     * @param  d          Description of the Parameter
     * @return            Description of the Return Value
     */

    //public static double calcSignedVolume(JOEMol mol, JOEAtom atm)
    //{
    //  XYZVector tmp_crd;
    //  Vector nbr_atms; // of type int[1]
    //  Vector nbr_crds; // of type XYZVector
    //  double hbrad = JOEElementTable.instance().correctedBondRad(1,0);
    //
    //  if (atm.getHvyValence() < 3)
    //  {
    //    logger.error("Cannot calculate a signed volume for an atom with a heavy atom valence of "+atm.getHvyValence());
    //    System.exit(0);
    //  }
    //
    //  // Create a vector with the coordinates of the neighbor atoms
    //  JOEAtom nbr;
    //  NbrAtomIterator nait = atm.nbrAtomIterator();
    //  while(nait.hasNext())
    //  {
    //    nbr = nait.nextNbrAtom();
    //    nbr_atms.add(new int[]{nbr.getIdx()});
    //  }
    //
    //  // sort the neighbor atoms to insure a consistent ordering
    //  //QuickInsertSort sorting = new QuickInsertSort();
    //  //RingSizeComparator ringSizeComparator = new RingSizeComparator();
    //  //sorting.sort(_rlist, ringSizeComparator);
    //  sort(nbr_atms.begin(),nbr_atms.end());
    //  for (int i = 0; i < nbr_atms.size(); i++)
    //  {
    //    JOEAtom tmp_atm = mol.getAtom( ((int[])nbr_atms.get(i))[0] );
    //    nbr_crds.add(tmp_atm.getVector());
    //  }
    //
    //  // If we have three heavy atoms we need to calculate the position of the fourth
    //  if (atm.getHvyValence() == 3)
    //  {
    //    double bondlen = hbrad+JOEElementTable.instance().correctedBondRad(atm.getAtomicNum(), atm.getHyb());
    //    atm.getNewBondVector(tmp_crd,bondlen);
    //    nbr_crds.add(tmp_crd);
    //  }
    //
    //  return signed_volume( (XYZVector) nbr_crds.get(0),
    //                        (XYZVector) nbr_crds.get(1),
    //                        (XYZVector) nbr_crds.get(2),
    //                        (XYZVector) nbr_crds.get(3));
    //}

    /**
     *  Calculate the signed volume for an atom. If the atom has a valence of 3
     *  the coordinates of an attached hydrogen are calculated
     *
     *  Calculate a signed volume given a set of 4 coordinates.
     *
     * @param  a          Description of the Parameter
     * @param  b          Description of the Parameter
     * @param  c          Description of the Parameter
     * @param  d          Description of the Parameter
     * @return            Description of the Return Value
     */
    public static double signedVolume(final XYZVector a, final XYZVector b,
        final XYZVector c, final XYZVector d)
    {
        XYZVector A;
        XYZVector B;
        XYZVector C;
        A = XYZVector.sub(b, a);
        B = XYZVector.sub(c, a);
        C = XYZVector.sub(d, a);

        Matrix3x3 m = new Matrix3x3(A, B, C);

        return m.determinant();
    }

    /**
     * @param  vcr       {@link java.util.Vector} of <tt>String</tt>
     * @param  s         Description of the Parameter
     * @param  delimstr  Description of the Parameter
     * @param  limit     Description of the Parameter
     * @return           Description of the Return Value
     */
    public static boolean tokenize(Vector vcr, String s, String delimstr,
        int limit)
    {
        System.out.println("Warning: tokenize \"" + s + "\"");
        vcr.clear();
        s = new String(s + "\n");

        int endpos = 0;
        int matched = 0;

        StringTokenizer st = new StringTokenizer(s, delimstr);

        while (st.hasMoreTokens())
        {
            String tmp = st.nextToken();
            vcr.add(tmp);

            matched++;

            if (matched == limit)
            {
                endpos = s.lastIndexOf(tmp);
                vcr.add(s.substring(endpos + tmp.length()));

                break;
            }
        }

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  atom  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static int valenceSum(JOEAtom atom)
    {
        int count = atom.getImplicitValence();

        JOEBond bond;
        BondIterator bit = atom.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isKDouble())
            {
                count++;
            }
        }

        return count;
    }

    /**
     *  Description of the Method
     *
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     * @return    Description of the Return Value
     */
    public boolean compareBonds(final JOEBond a, final JOEBond b)
    {
        if (a.getBeginAtomIdx() == b.getBeginAtomIdx())
        {
            return (a.getEndAtomIdx() < b.getEndAtomIdx());
        }

        return (a.getBeginAtomIdx() < b.getBeginAtomIdx());
    }

    /*void Toupper(string &s)
     *{
     *unsigned int i;
     *for (i = 0;i < s.size();i++)
     *s[i] = toupper(s[i]);
     *}
     *void Tolower(string &s)
     *{
     *unsigned int i;
     *for (i = 0;i < s.size();i++)
     *s[i] = tolower(s[i]);
     *} */

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public static int determineFRJ(JOEMol mol)
    {
        Vector cfl = new Vector();

        // of type int[1]-Vector
        //find all continuous graphs in the mol area
        mol.contiguousFragments(cfl);

        //  System.out.println("cfl size: "+cfl.size());
        if (cfl.size() == 0)
        {
            return (0);
        }

        if (cfl.size() == 1)
        {
            return (mol.numBonds() - mol.numAtoms() + 1);
        }

        //count up the atoms and bonds belonging to each graph
        JOEBond bond = null;

        int numatoms;

        int numbonds;

        int frj = 0;
        JOEBitVec frag = new JOEBitVec();

        //    Vector     vtmp;
        int[] itmp;

        //    System.out.println("cfl size: "+cfl.size());
        for (int i = 0; i < cfl.size(); i++)
        {
            //vtmp = (Vector) cfl.get(i);
            itmp = (int[]) cfl.get(i);

            //    for (int ii = 0; ii < vtmp.size(); ii++)
            //    {
            //      System.out.print(" "+ ((int[])vtmp.get(ii))[0] );
            //    }
            //    System.out.println("");
            frag.clear();

            //frag.fromVecInt(vtmp);
            //numatoms = vtmp.size();
            frag.fromIntArray(itmp);
            numatoms = itmp.length;
            numbonds = 0;

            BondIterator bit = mol.bondIterator();

            while (bit.hasNext())
            {
                bond = bit.nextBond();

                if (frag.bitIsOn(bond.getBeginAtomIdx()) &&
                        frag.bitIsOn(bond.getEndAtomIdx()))
                {
                    numbonds++;
                }
            }

            frj += (numbonds - numatoms + 1);

            //      if(bond!=null)System.out.println("frj: "+frj+" begidx "+bond.getBeginAtomIdx()+" endidx "+bond.getEndAtomIdx());
            //      else System.out.println("not def");
        }

        return (frj);
    }

    /**
     * @param  vcr  {@link java.util.Vector} of <tt>String</tt>
     * @param  buf  Description of the Parameter
     * @return      Description of the Return Value
     */
    public static boolean tokenize(Vector vcr, String buf)
    {
        return tokenize(vcr, buf, " \t\n");
    }

    /**
     * @param  vcr       {@link java.util.Vector} of <tt>String</tt>
     * @param  buf       Description of the Parameter
     * @param  delimstr  Description of the Parameter
     * @return           Description of the Return Value
     */
    public static boolean tokenize(Vector vcr, String buf, String delimstr)
    {
        vcr.clear();
        buf = new String(buf + "\n");

        StringTokenizer st = new StringTokenizer(buf, delimstr);

        while (st.hasMoreTokens())
        {
            vcr.add(st.nextToken());
        }

        return true;
    }

    //public boolean readMolecule(istream ifs, JOEMol mol, final String filename, String title)
    //{
    //  int i, len, exten;
    //
    //  if (filename==null)
    //  {
    //    logger.error("ReadMolecule error: filename is null");
    //    return false;
    //  }
    //
    //  exten = filename.indexOf(".");
    //  final String ext = filename.substring(exten+1);
    //
    //  if ( ext.equals("mol2") ) )  return readMol2(ifs,mol,title);
    //  else if (ext.equals("sdf") ) return readSDFile(ifs,mol,title);
    //  else if (ext.equals("pdb") )
    //  {
    //    boolean b=readPDB(ifs,mol,title);
    //    if ( mol.getTitle().length() == 0) mol.setTitle(filename);
    //    return b;
    //  }
    //  else if (ext.equals("box") ) return readBox(ifs,mol,title);
    //  else if (ext.equals("smi") ) return readSmiles(ifs,mol,title);
    //  else if (ext.equals("gpMM") || ext.equals("gpQM") ) return readGhemical(ifs,mol,title);
    //  else
    //  {
    //    logger.error("ReadMolecule error: "+filename+" : file extension not recognized");
    //    return false;
    //  }
    //}
    //public boolean writeMolecule(ostream ofs, JOEMol mol, final String filename, String dimension)
    //{
    //  int i, len, exten;
    //
    //  if (filename == null)
    //  {
    //    logger.error("WriteMolecule error: filename is null");
    //    return false;
    //  }
    //
    //  exten = filename.indexOf(".");
    //  final String ext = filename.substring(exten+1);
    //
    //  if (ext.equals("mol2") ) return WriteMol2(ofs,mol,dimension);
    //  else if (ext.equals("sdf") )  return WriteSDFile(ofs,mol,dimension);
    //  else if (ext.equals("pdb") )  return WriteDelphiPDB(ofs,mol);
    //  else if (ext.equals("gpMM") ) return WriteGhemical(ofs,mol);
    //  else
    //  {
    //    logger.error("WriteMolecule error: "+filename+" : file extension not recognized");
    //    return false;
    //  }
    //}

    /**
     *  Description of the Method
     *
     * @param  ofs  Description of the Parameter
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean writeTitles(OutputStream ofs, JOEMol mol)
    {
        PrintStream ps = new PrintStream(ofs);
        ps.println(mol.getTitle());

        return true;
    }

    double calcRMS(double[] r, double[] f, int size)
    {
        int i;
        float d2 = 0.0f;

        for (i = 0; i < size; i++)
        {
            d2 += (((r[i * 3] - f[i * 3]) * (r[i * 3] - f[i * 3])) +
            ((r[(i * 3) + 1] - f[(i * 3) + 1]) * (r[(i * 3) + 1] -
            f[(i * 3) + 1])) +
            ((r[(i * 3) + 2] - f[(i * 3) + 2]) * (r[(i * 3) + 2] -
            f[(i * 3) + 2])));
        }

        d2 /= (double) size;

        return (Math.sqrt(d2));
    }

    /**
     *
     * @param vic of type <tt>JOEInternalCoord</tt>
     */
    void cartesianToInternal(Vector vic, JOEMol mol)
    {
        double r;
        double sum;
        JOEAtom atom;
        JOEAtom nbr;
        JOEAtom ref;

        //    vector<OEAtom*>::iterator i,j,m;
        //set reference atoms
        AtomIterator ait = mol.atomIterator();
        AtomIterator aitNbr = mol.atomIterator();
        JOEInternalCoord intCoord;
        JOEInternalCoord intCoordNbr;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            intCoord = (JOEInternalCoord) vic.get(atom.getIdx());

            if (atom.getIdx() == 1)
            {
                continue;
            }
            else if (atom.getIdx() == 2)
            {
                intCoord._a = mol.getAtom(1);

                continue;
            }
            else if (atom.getIdx() == 3)
            {
                intCoord._a = mol.getAtom(2);
                intCoord._b = mol.getAtom(1);

                continue;
            }

            sum = 1.0E10;
            ref = mol.getAtom(1);
            aitNbr.reset();

            while (aitNbr.hasNext() &&
                    (ait.actualIndex() != aitNbr.actualIndex()))
            {
                nbr = aitNbr.nextAtom();
                intCoordNbr = (JOEInternalCoord) vic.get(nbr.getIdx());

                if (nbr.getIdx() < 3)
                {
                    continue;
                }

                XYZVector tmp = XYZVector.sub(atom.getVector(), nbr.getVector());
                r = tmp.length_2();

                if ((r < sum) && (intCoordNbr._a != nbr) &&
                        (intCoordNbr._b != nbr))
                {
                    sum = r;
                    ref = nbr;
                }
            }

            intCoord._a = ref;
            intCoord._b = ((JOEInternalCoord) vic.get(ref.getIdx()))._a;
            intCoord._c = ((JOEInternalCoord) vic.get(ref.getIdx()))._b;
        }

        //fill in geometries
        int k;
        XYZVector v1 = new XYZVector();
        XYZVector v2 = new XYZVector();
        JOEAtom a;
        JOEAtom b;
        JOEAtom c = null;
        JOEInternalCoord icK;

        for (k = 2; k <= mol.numAtoms(); k++)
        {
            atom = mol.getAtom(k);
            icK = (JOEInternalCoord) vic.get(k);
            a = icK._a;
            b = icK._b;
            c = icK._c;

            if (k == 2)
            {
                XYZVector tmp = XYZVector.sub(atom.getVector(), a.getVector());
                icK._dst = tmp.length();

                continue;
            }

            XYZVector.sub(v1, atom.getVector(), a.getVector());
            XYZVector.sub(v2, b.getVector(), a.getVector());
            icK._dst = v1.length();
            icK._ang = XYZVector.xyzVectorAngle(v1, v2);

            if (k == 3)
            {
                continue;
            }

            icK._tor = XYZVector.calcTorsionAngle(atom.getVector(),
                    a.getVector(), b.getVector(), c.getVector());
        }

        //check for linear geometries and try to correct if possible
        boolean done;
        double ang;

        for (k = 2; k <= mol.numAtoms(); k++)
        {
            icK = (JOEInternalCoord) vic.get(k);
            ang = Math.abs(icK._ang);

            if ((ang > 5.0f) && (ang < 175.0f))
            {
                continue;
            }

            atom = mol.getAtom(k);
            done = false;

            ait.reset();

            while (ait.hasNext())
            {
                a = ait.nextAtom();

                if ((a.getIdx() >= k) || done)
                {
                    break;
                }

                AtomIterator ait2 = mol.atomIterator();

                while (ait2.hasNext())
                {
                    b = ait2.nextAtom();

                    if ((b.getIdx() >= a.getIdx()) || done)
                    {
                        break;
                    }

                    XYZVector.sub(v1, atom.getVector(), a.getVector());
                    XYZVector.sub(v2, b.getVector(), a.getVector());
                    ang = Math.abs(XYZVector.xyzVectorAngle(v1, v2));

                    if ((ang < 5.0f) || (ang > 175.0f))
                    {
                        continue;
                    }

                    AtomIterator ait3 = mol.atomIterator();

                    while (ait3.hasNext())
                    {
                        c = ait3.nextAtom();

                        if (c.getIdx() >= atom.getIdx())
                        {
                            break;
                        }

                        if ((c != atom) && (c != a) && (c != b))
                        {
                            break;
                        }
                    }

                    if (c == null)
                    {
                        continue;
                    }

                    icK._a = a;
                    icK._b = b;
                    icK._c = c;
                    icK._dst = v1.length();
                    icK._ang = XYZVector.xyzVectorAngle(v1, v2);
                    icK._tor = XYZVector.calcTorsionAngle(atom.getVector(),
                            a.getVector(), b.getVector(), c.getVector());
                    done = true;
                }
            }
        }
    }

    XYZVector centerCoords(double[] c, int size)
    {
        int i;
        double x = 0;
        double y = 0;
        double z = 0;

        for (i = 0; i < size; i++)
        {
            x += c[i * 3];
            y += c[(i * 3) + 1];
            z += c[(i * 3) + 2];
        }

        x /= (double) size;
        y /= (double) size;
        z /= (double) size;

        for (i = 0; i < size; i++)
        {
            c[i * 3] -= x;
            c[(i * 3) + 1] -= y;
            c[(i * 3) + 2] -= z;
        }

        XYZVector v = new XYZVector(x, y, z);

        return (v);
    }

    /**
     *
     *   @param m of size[3][3]
     */
    void rotateCoords(double[] c, double[][] m, int size)
    {
        int i;
        double x;
        double y;
        double z;

        for (i = 0; i < size; i++)
        {
            x = (c[i * 3] * m[0][0]) + (c[(i * 3) + 1] * m[0][1]) +
                (c[(i * 3) + 2] * m[0][2]);
            y = (c[i * 3] * m[1][0]) + (c[(i * 3) + 1] * m[1][1]) +
                (c[(i * 3) + 2] * m[1][2]);
            z = (c[i * 3] * m[2][0]) + (c[(i * 3) + 1] * m[2][1]) +
                (c[(i * 3) + 2] * m[2][2]);
            c[i * 3] = x;
            c[(i * 3) + 1] = y;
            c[(i * 3) + 2] = z;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
