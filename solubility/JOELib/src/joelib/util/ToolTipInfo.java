///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ToolTipInfo.java,v $
//  Purpose:  Tool tip information for chart visualisations.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:28 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util;


/**
 * Tool tip information for chart visualisations.
 *
 * @author     wegnerj
 *     21. M�rz 2002
 */
public interface ToolTipInfo
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the tool tip text. Returns HTML code <b>without</b> the &lt;html> and
     *  &lt;/html> tokens.
     *
     * @param  x  the x coordinate in a chart visualisation
     * @param  y  the y coordinate in a chart visualisation
     * @return    the HTML tool tip text
     */
    public String getHTMLInfo(double x, double y);

    /**
     *  Gets the information type for this tool tip, e.g "infrared data"
     *
     * @return    the information type
     */
    public String getType();
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
