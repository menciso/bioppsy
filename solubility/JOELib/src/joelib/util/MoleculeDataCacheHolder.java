///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeDataCacheHolder.java,v $
//  Purpose:  Holds all native value descriptors as double matrix for all known molecules.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2004/07/25 20:43:28 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.util;

import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.desc.data.MoleculeCache;


/**
 * Molecule-Data matrix cache.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2004/07/25 20:43:28 $
 */
public final class MoleculeDataCacheHolder
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.util.MoleculeDataCacheHolder");
    private static MoleculeDataCacheHolder instance;

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable mdMatrix;

    //~ Constructors ///////////////////////////////////////////////////////////

    private MoleculeDataCacheHolder() throws Exception
    {
        mdMatrix = new Hashtable(10);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public static synchronized MoleculeDataCacheHolder instance()
        throws Exception
    {
        if (instance == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Getting " +
                    MoleculeDataCacheHolder.class.getName() + " instance.");
            }

            instance = new MoleculeDataCacheHolder();
        }

        return instance;
    }

    public boolean contains(String _file)
    {
        return mdMatrix.containsKey(_file);
    }

    public MoleculeCache get(String _file)
    {
        return (MoleculeCache) mdMatrix.get(_file);
    }

    public Object put(String _file, Object obj)
    {
        return mdMatrix.put(_file, obj);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
