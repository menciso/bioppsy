///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SMARTSPatternString.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2003/08/19 13:11:28 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts.types;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import joelib.smarts.JOESmartsPattern;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 *  Atom representation.
 *
 * @author     wegnerj
 *     30. Januar 2002
 */
public class SMARTSPatternString implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Field
     */
    public JOESmartsPattern sp;

    /**
     *  Description of the Field
     */
    public String s;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the SMARTSPatternString object
     *
     * @param  _sp  Description of the Parameter
     * @param  _s   Description of the Parameter
     */
    public SMARTSPatternString(JOESmartsPattern _sp, String _s)
    {
        sp = _sp;
        s = _s;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
