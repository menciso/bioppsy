///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SMARTSPatternInt.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2003/08/19 13:11:28 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts.types;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import joelib.smarts.JOESmartsPattern;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 *  Atom representation.
 *
 * @author     wegnerj
 *     30. Januar 2002
 */
public class SMARTSPatternInt implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Field
     */
    public JOESmartsPattern sp;

    /**
     *  Description of the Field
     */
    public int i;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the SMARTSPatternInt object
     *
     * @param  _sp  Description of the Parameter
     * @param  _i   Description of the Parameter
     */
    public SMARTSPatternInt(JOESmartsPattern _sp, int _i)
    {
        sp = _sp;
        i = _i;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
