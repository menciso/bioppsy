///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOESmartsPattern.java,v $
//  Purpose:  Class to parse SMART pattern and store the search expressions.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.23 $
//            $Date: 2004/07/29 10:16:48 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts;

import java.io.OutputStream;
import java.io.PrintStream;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.smarts.atomexpr.AtomExpr;
import joelib.smarts.atomexpr.BinAtomExpr;
import joelib.smarts.atomexpr.LeafAtomExpr;

import joelib.util.JOEBitVec;
import joelib.util.types.IntInt;
import joelib.util.types.IntIntInt;


/**
 * Class to parse SMART pattern and store the search expressions.
 *
 * <blockquote><pre>
 * String smartsPattern = "c1ccccc1";
 * JOESmartsPattern smarts = new JOESmartsPattern();
 *
 * // parse, initialize and generate SMARTS pattern
 * // to allow fast pattern matching
 * if(!smarts.init(smartsPattern)
 * {
 *   System.err.println("Invalid SMARTS pattern.");
 * }
 *
 * // find substructures
 * smarts.match(mol);
 * Vector         matchList  = smarts.getUMapList();
 * </pre></blockquote>
 *
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.23 $, $Date: 2004/07/29 10:16:48 $
 * @cite smarts
 */
public class JOESmartsPattern implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public static member variables
     *-------------------------------------------------------------------------*/

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.smarts.JOESmartsPattern");

    //~ Instance fields ////////////////////////////////////////////////////////

    // of type int[]

    /**
     *  Description of the Field
     */
    protected Pattern _pat;

    // of type boolean[1]

    /**
     *  Description of the Field
     */
    protected String _str;

    /**
     *  Description of the Field
     */
    protected Vector _growbond;

    /*-------------------------------------------------------------------------*
     * protected member variables
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Field
     */
    protected Vector _mlist;
    private ParseSmart _parseSmart;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor/destructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the JOESmartsPattern object
     */
    public JOESmartsPattern()
    {
        _mlist = new Vector();
        _pat = null;
        _growbond = new Vector();

        _parseSmart = new ParseSmart();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the atomicNum attribute of the JOESmartsPattern object
     *
     * @param  idx  Description of the Parameter
     * @return      The atomicNum value
     */
    public int getAtomicNum(int idx)
    {
        AtomExpr expr = _pat.atom[idx].expr;

        int size = 0;
        AtomExpr[] stack = new AtomExpr[15];
        boolean lftest = true;

        for (size = 0, stack[size] = expr; size >= 0; expr = stack[size])
        {
            switch (expr.type)
            {
            case AtomExpr.AE_LEAF:

                LeafAtomExpr leafAtomExpr = (LeafAtomExpr) expr;

                if (leafAtomExpr.prop == AtomExpr.AL_ELEM)
                {
                    return (leafAtomExpr.value);
                }

                lftest = true;
                size--;

                break;

            case AtomExpr.AE_OR:
            case AtomExpr.AE_ANDHI:
            case AtomExpr.AE_ANDLO:

                BinAtomExpr binAtomExpr = (BinAtomExpr) expr;

                if (stack[size + 1] == binAtomExpr.rgt)
                {
                    size--;
                }
                else if (stack[size + 1] == binAtomExpr.lft)
                {
                    if (lftest)
                    {
                        size++;
                        stack[size] = binAtomExpr.rgt;
                    }
                    else
                    {
                        size--;
                    }
                }
                else
                {
                    size++;
                    stack[size] = binAtomExpr.lft;
                }

                break;

            case AtomExpr.AE_NOT:
                return (0);

            case AtomExpr.AE_RECUR:
                return (0);
            }
        }

        return (0);
    }

    /**
     *  Gets the bond attribute of the JOESmartsPattern object
     *
     * @param  iii  Description of the Parameter
     * @param  idx  Description of the Parameter
     */
    public void getBond(IntIntInt iii, int idx)
    {
        // source
        iii.ii.i1 = _pat.bond[idx].src;

        // dest
        iii.ii.i2 = _pat.bond[idx].dst;

        // ord
        iii.i = ParseSmart.getExprOrder(_pat.bond[idx].expr);
    }

    /**
     *  Gets the charge attribute of the JOESmartsPattern object
     *
     * @param  idx  Description of the Parameter
     * @return      The charge value
     */
    public int getCharge(int idx)
    {
        AtomExpr expr = _pat.atom[idx].expr;

        int size = 0;
        AtomExpr[] stack = new AtomExpr[15];
        boolean lftest = true;

        size = 0;
        stack[size] = expr;

        for (; size >= 0; expr = stack[size])
        {
            switch (expr.type)
            {
            case AtomExpr.AE_LEAF:

                LeafAtomExpr leafAtomExpr = (LeafAtomExpr) expr;

                switch (leafAtomExpr.prop)
                {
                case AtomExpr.AL_NEGATIVE:
                    return (-1 * (int) leafAtomExpr.value);

                case AtomExpr.AL_POSITIVE:
                    return ((int) leafAtomExpr.value);

                default:
                    lftest = true;
                }

                size--;

                break;

            case AtomExpr.AE_OR:
            case AtomExpr.AE_ANDHI:
            case AtomExpr.AE_ANDLO:

                BinAtomExpr binAtomExpr = (BinAtomExpr) expr;

                if (stack[size + 1] == binAtomExpr.rgt)
                {
                    size--;
                }
                else if (stack[size + 1] == binAtomExpr.lft)
                {
                    if (lftest)
                    {
                        size++;
                        stack[size] = binAtomExpr.rgt;
                    }
                    else
                    {
                        size--;
                    }
                }
                else
                {
                    size++;
                    stack[size] = binAtomExpr.lft;
                }

                break;

            case AtomExpr.AE_NOT:
                return (0);

            case AtomExpr.AE_RECUR:
                return (0);
            }

            if (size < 0)
            {
                break;
            }
        }

        return (0);
    }

    /**
     * Get list of matching atoms.
     *
     * @return    {@link java.util.Vector} of <tt>int[]</tt>
     */
    public Vector getMapList()
    {
        return (_mlist);
    }

    /**
     *  Gets the sMARTS attribute of the JOESmartsPattern object
     *
     * @return    The sMARTS value
     */
    public String getSMARTS()
    {
        return (_str);
    }

    /**
     * Get unique list of matching atoms.
     *
     * @return    {@link java.util.Vector} of <tt>int[]</tt>
     */
    public Vector getUMapList()
    {
        if ((_mlist.size() == 0) || (_mlist.size() == 1))
        {
            return _mlist;
        }

        boolean ok;
        JOEBitVec bv = new JOEBitVec();
        Vector vbv = new Vector();

        // of type JOEBitVec
        Vector mlist = new Vector();

        // of type int[]
        //Vector vtmp;
        int[] itmp;

        //    System.out.println("mlist hits before:"+_mlist.size());
        for (int i = 0; i < _mlist.size(); i++)
        {
            ok = true;
            bv.clear();

            //vtmp = _mlist.get(i);
            itmp = (int[]) _mlist.get(i);
            bv.fromIntArray(itmp);

            for (int j = 0; (j < vbv.size()) && ok; j++)
            {
                if (((JOEBitVec) vbv.get(j)).equals(bv))
                {
                    ok = false;
                }
            }

            if (ok)
            {
                //          System.out.println("add ");
                //mlist.put(vtmp);
                mlist.add(itmp);
                vbv.add(bv.clone());
            }
        }

        _mlist = mlist;

        return _mlist;
    }

    /**
     *  Gets the valid attribute of the JOESmartsPattern object
     *
     * @return    The valid value
     */
    public boolean isValid()
    {
        return (_pat != null);
    }

    /**
     *  Gets the vector binding of this smarts pattern for SMARTS atom with <tt>idx</tt>.
     * E.g.: O=CO[#1:1] where :1 means that the atom #1 (H atom) has the vector binding number 1.
     * This example can be used in <tt>JOEChemTransformation</tt> to delete this H atoms:<br>
     * TRANSFORM O=CO[#1:1] >> O=CO
     *
     * @param  idx  the SMARTS atom <tt>idx</tt>
     * @return     0 if no vector binding is was defined
     */
    public int getVectorBinding(int idx)
    {
        return (_pat.atom[idx].vb);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean empty()
    {
        return (_pat == null);
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
        //    if (_pat!=null) freePattern(_pat);
    }

    /**
     *  Description of the Method
     *
     * @param  _buffer  Description of the Parameter
     * @return          Description of the Return Value
     */
    public boolean init(char[] _buffer)
    {
        return init(new String(_buffer));
    }

    /**
     *  Description of the Method
     *
     * @param  s  Description of the Parameter
     * @return    <tt>true</tt> if the initialisation was succesfull
     */
    public boolean init(String s)
    {
        char[] _buffer = s.toCharArray();
        System.arraycopy(_buffer, 0, _parseSmart.buffer, 0, _buffer.length);
        ParseSmart.initSMARTSParser();
        _pat = _parseSmart.parseSMARTSRecord(_parseSmart.buffer, 0,
                s.length() - 1);
        _str = s;

        // dump atom expression
        if (logger.isDebugEnabled())
        {
            logger.debug("Pattern:" + s + "\n" + this.toString());
        }

        return (_pat != null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return         <tt>true</tt> SMARTS matching was successfull and pattern occur
     */
    public boolean match(JOEMol mol)
    {
        return match(mol, false);
    }

    /**
     *  Description of the Method
     *
     * @param  mol     Description of the Parameter
     * @param  single  Description of the Parameter
     * @return         <tt>true</tt> SMARTS matching was successfull and pattern occur
     */
    public boolean match(JOEMol mol, boolean single)
    {
        _parseSmart.rsCache.clear();

        return (_parseSmart.match(mol, _pat, _mlist, single));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public int numAtoms()
    {
        return ((_pat != null) ? _pat.acount : 0);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public int numBonds()
    {
        return ((_pat != null) ? _pat.bcount : 0);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public int numMatches()
    {
        return _mlist.size();
    }

    /**
     * @param  pr   of type <tt>IntInt</tt> -{@link java.util.Vector}
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean restrictedMatch(JOEMol mol, Vector pr)
    {
        return restrictedMatch(mol, pr, false);
    }

    /**
     * @param  pr      of type <tt>IntInt</tt> -{@link java.util.Vector}
     * @param  mol     Description of the Parameter
     * @param  single  Description of the Parameter
     * @return         Description of the Return Value
     */
    public boolean restrictedMatch(JOEMol mol, Vector pr, boolean single)
    {
        boolean ok;
        Vector mlist = new Vector();

        // of type int[]
        _parseSmart.rsCache.clear();
        _parseSmart.match(mol, _pat, mlist);
        _mlist.clear();

        if (mlist.size() == 0)
        {
            return (false);
        }

        //Vector vtmp;
        int[] itmp;
        IntInt ii;

        for (int i = 0; i < mlist.size(); i++)
        {
            //vtmp = (Vector) mlist.get(i);
            itmp = (int[]) mlist.get(i);
            ok = true;

            for (int j = 0; (j < pr.size()) && ok; j++)
            {
                ii = (IntInt) pr.get(j);

                //itmp = (int[]) vtmp.get(ii.i1);
                //if ( itmp[0] != ii.i2) ok = false;
                if (itmp[ii.i1] != ii.i2)
                {
                    ok = false;
                }
            }

            if (ok)
            {
                _mlist.add(itmp);
            }

            if (single && (_mlist.size() != 0))
            {
                return (true);
            }
        }

        return ((_mlist.size() == 0) ? false : true);
    }

    /**
     *  Description of the Method
     *
     * @param  mol   Description of the Parameter
     * @param  vres  Description of the Parameter
     * @return       Description of the Return Value
     */
    public boolean restrictedMatch(JOEMol mol, JOEBitVec vres)
    {
        return restrictedMatch(mol, vres, false);
    }

    /**
     *  Description of the Method
     *
     * @param  mol     Description of the Parameter
     * @param  vres    Description of the Parameter
     * @param  single  Description of the Parameter
     * @return         Description of the Return Value
     */
    public boolean restrictedMatch(JOEMol mol, JOEBitVec vres, boolean single)
    {
        boolean ok;
        Vector mlist = new Vector();

        // of type int[]
        _parseSmart.rsCache.clear();
        _parseSmart.match(mol, _pat, mlist);

        _mlist.clear();

        if (mlist.size() == 0)
        {
            return (false);
        }

        //Vector vtmp;
        int[] itmp;

        for (int i = 0; i < mlist.size(); i++)
        {
            ok = true;

            //vtmp = (Vector)mlist.get(i);
            itmp = (int[]) mlist.get(i);

            for (int j = 0; j < itmp.length; j++)
            {
                //itmp = (int[]) vtmp.get(j);
                if (!vres.get(itmp[j]))
                {
                    ok = false;

                    break;
                }
            }

            if (!ok)
            {
                continue;
            }

            //_mlist.put( vtmp);
            _mlist.add(itmp);

            if (single && (_mlist.size() != 0))
            {
                return (true);
            }
        }

        return ((_mlist.size() == 0) ? false : true);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String toString()
    {
        StringBuffer sb = new StringBuffer();

        if (_pat != null)
        {
            sb.append(_pat.toString());
        }

        return sb.toString();
    }

    /**
     *  Description of the Method
     *
     * @param  ofs  Description of the Parameter
     */
    public void writeMapList(OutputStream ofs)
    {
        PrintStream ps = new PrintStream(ofs);

        int[] itmp;

        for (int i = 0; i < _mlist.size(); i++)
        {
            //Vector vtmp = (Vector)_mlist.get(i);
            itmp = (int[]) _mlist.get(i);

            for (int j = 0; j < itmp.length; j++)
            {
                //itmp = (int[]) vtmp.get(j);
                ps.print(itmp[j]);
                ps.print(' ');
            }

            ps.println("");
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
