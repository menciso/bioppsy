///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SMARTSEvaluation.java,v $
//  Purpose:  Test SMARTS matching in batch mode allowing different rules, molecules, etc.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2004/07/25 20:43:26 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts.test;

import cformat.PrintfStream;

import wsi.ra.io.RegExpFilenameFilter;

import wsi.ra.tool.ResourceLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;

import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;

import joelib.util.JHM;
import joelib.util.types.IntInt;


/**
 * Test SMARTS matching in batch mode allowing different rules, molecules, etc.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2004/07/25 20:43:26 $
 */
public class SMARTSEvaluation
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "jcompchem.joelib.test.SMARTSEvaluation");
    private static String delimiter = "----------------------------------------------";

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable molecules;
    private Vector matchesAtomIdx;
    private Vector matchesMols;
    private Vector moleculesV;
    private Vector smarts;
    private Vector smartsDescription;
    private boolean printAllMatches = false;

    //~ Constructors ///////////////////////////////////////////////////////////

    public SMARTSEvaluation()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
      *  The main program for the TestSmarts class
      *
      * @param  args  The command line arguments
      */
    public static void main(String[] args)
    {
        SMARTSEvaluation smartsEval = new SMARTSEvaluation();

        if (args.length != 4)
        {
            smartsEval.usage();
            System.exit(0);
        }
        else
        {
            smartsEval.test(args[0], args[1], args[2], args[3]);
        }

        System.exit(0);
    }

    /**
     *  A unit test for JUnit
     *
     * @param  molURL   Description of the Parameter
     * @param  inType   Description of the Parameter
     * @param  outType  Description of the Parameter
     */
    public void test(String molDirectory, String evaluationFile,
        String outputFile, String showType)
    {
        if (showType.equalsIgnoreCase("all"))
        {
            printAllMatches = true;
        }
        else
        {
            printAllMatches = false;
        }

        loadMolecules(molDirectory);
        loadEvaluationFile(evaluationFile);

        // start evaluation
        evaluateSMARTS(outputFile);
    }

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();
        String programPackage = this.getClass().getPackage().getName();

        sb.append("Usage is : ");
        sb.append("java -cp . ");
        sb.append(programName);
        sb.append(" <directory with SDF files>");
        sb.append(" <evaluationFile>");
        sb.append(" <outputFile>");
        sb.append(" <show: 'all' or 'errors'>");
        sb.append("\n\n where the evaluation file has the form:\n");
        sb.append(
            "SMARTS description molName_1#i_1#i_2#i_n molName_m#i_1#i_n ...");

        System.out.println(sb.toString());

        System.exit(0);
    }

    /**
     *
     */
    private void evaluateSMARTS(String outputFile)
    {
        FileOutputStream out = null;
        PrintfStream ps = null;

        try
        {
            out = new FileOutputStream(outputFile);
            ps = new PrintfStream(out);
        }
         catch (Exception ex)
        {
            ex.printStackTrace();
        }

        JOEMol mol;
        String molName;
        String description;
        Vector matchesMolsSingle = null;
        Vector matchesAtomIdxSingle = null;
        JOESmartsPattern pSMARTS = null;
        Vector matchList;
        int[] iaTmp;
        int[] firstAtomsExpected;
        int firstAtomIdx;
        boolean oneFound;
        Vector wronglyRecognized = new Vector();
        Vector notRecognized = new Vector();
        int ok = 0;
        int mismatches = 0;
        int singleMismatches;
        ps.println("Checking " + smarts.size() + " SMARTS patterns.");

        for (int i = 0; i < smarts.size(); i++)
        {
            //System.out.println(smarts.get(i).getClass().getName()+": "+smarts);
            pSMARTS = (JOESmartsPattern) smarts.get(i);
            description = (String) smartsDescription.get(i);

            //System.out.println(matchesMols.get(i).getClass().getName()+": "+matchesMols);
            matchesMolsSingle = (Vector) matchesMols.get(i);

            //System.out.println(matchesAtomIdx.get(i).getClass().getName()+": "+matchesAtomIdx);
            matchesAtomIdxSingle = (Vector) matchesAtomIdx.get(i);

            for (int j = 0; j < matchesMolsSingle.size(); j++)
            {
                molName = (String) matchesMolsSingle.get(j);
                firstAtomsExpected = (int[]) matchesAtomIdxSingle.get(j);
                mol = (JOEMol) molecules.get(molName);

                //System.out.println("Empty: " + mol.empty());
                if ((mol == null) || mol.empty())
                {
                    logger.error("No molecular structure available for " +
                        molName);

                    continue;
                }
                else
                {
                    wronglyRecognized.clear();
                    notRecognized.clear();

                    // find substructures
                    pSMARTS.match(mol);
                    matchList = pSMARTS.getMapList();
                    singleMismatches = 0;

                    for (int nn = 0; nn < matchList.size(); nn++)
                    {
                        iaTmp = (int[]) matchList.get(nn);

                        firstAtomIdx = iaTmp[0];
                        oneFound = false;

                        if (firstAtomsExpected != null)
                        {
                            if (firstAtomsExpected[0] != -1)
                            {
                                for (int k = 0; k < firstAtomsExpected.length;
                                        k++)
                                {
                                    if (firstAtomsExpected[k] == firstAtomIdx)
                                    {
                                        oneFound = true;
                                    }
                                }
                            }
                        }

                        if (firstAtomsExpected == null)
                        {
                            if (!oneFound || (firstAtomsExpected == null))
                            {
                                wronglyRecognized.add(new IntInt(nn,
                                        firstAtomIdx));
                                singleMismatches++;
                            }
                        }
                        else
                        {
                            if (firstAtomsExpected[0] != -1)
                            {
                                if (!oneFound)
                                {
                                    wronglyRecognized.add(new IntInt(nn,
                                            firstAtomIdx));
                                    singleMismatches++;
                                }
                            }
                        }
                    }

                    if (firstAtomsExpected != null)
                    {
                        if (firstAtomsExpected[0] == -1)
                        {
                            if (matchList.size() == 0)
                            {
                                notRecognized.add(new Integer(-1));
                                singleMismatches++;
                            }
                        }
                        else
                        {
                            if (matchList.size() < firstAtomsExpected.length)
                            {
                                for (int k = 0; k < firstAtomsExpected.length;
                                        k++)
                                {
                                    oneFound = false;

                                    for (int nn = 0; nn < matchList.size();
                                            nn++)
                                    {
                                        iaTmp = (int[]) matchList.get(nn);
                                        firstAtomIdx = iaTmp[0];

                                        if (firstAtomsExpected[k] == firstAtomIdx)
                                        {
                                            oneFound = true;
                                        }
                                    }

                                    if (!oneFound)
                                    {
                                        notRecognized.add(new Integer(
                                                firstAtomsExpected[k]));
                                        singleMismatches++;
                                    }
                                }
                            }
                        }
                    }

                    if (firstAtomsExpected == null)
                    {
                        ok++;
                        mismatches += singleMismatches;
                        ok += (matchList.size() - singleMismatches);
                    }
                    else
                    {
                        if (firstAtomsExpected[0] == -1)
                        {
                            mismatches += singleMismatches;
                            ok++;
                        }
                        else
                        {
                            mismatches += singleMismatches;
                            ok += (matchList.size() - singleMismatches);
                        }
                    }

                    if ((wronglyRecognized.size() != 0) ||
                            (notRecognized.size() != 0) || printAllMatches)
                    {
                        ps.println(delimiter);
                        ps.println("check " + description + ": " +
                            pSMARTS.getSMARTS());
                        ps.println(" check molecule: " + molName);

                        if (printAllMatches)
                        {
                            for (int nn = 0; nn < matchList.size(); nn++)
                            {
                                iaTmp = (int[]) matchList.get(nn);
                                printMatch(ps, iaTmp);
                            }
                        }
                    }

                    if (wronglyRecognized.size() != 0)
                    {
                        ps.println("    Too many matches.");

                        IntInt ii;

                        for (int k = 0; k < wronglyRecognized.size(); k++)
                        {
                            ii = (IntInt) wronglyRecognized.get(k);
                            ps.println("    Match at atom " + ii.i2 +
                                " should not occur.");

                            iaTmp = (int[]) matchList.get(ii.i1);
                            printMatch(ps, iaTmp);
                        }
                    }

                    if (notRecognized.size() != 0)
                    {
                        ps.println("    Missing matches.");

                        int tmp;

                        for (int k = 0; k < notRecognized.size(); k++)
                        {
                            tmp = ((Integer) notRecognized.get(k)).intValue();

                            if (tmp == -1)
                            {
                                ps.println("    Expected matches are missing.");
                            }
                            else
                            {
                                ps.println("    Expected match at atom " + tmp +
                                    " is missing.");
                            }

                            if (!printAllMatches)
                            {
                                for (int nn = 0; nn < matchList.size(); nn++)
                                {
                                    iaTmp = (int[]) matchList.get(nn);
                                    printMatch(ps, iaTmp);
                                }
                            }
                        }
                    }
                }
            }
        }

        ps.println(delimiter);
        ps.println("Correct matches: " + ok);
        ps.println("Mismatches:      " + mismatches);
        ps.println(delimiter);
        ps.println("Parsed SMARTS patterns:\n");

        for (int i = 0; i < smarts.size(); i++)
        {
            pSMARTS = (JOESmartsPattern) smarts.get(i);

            ps.println(pSMARTS.getSMARTS() + " " + smartsDescription.get(i));
            ps.println(pSMARTS);
            ps.println(delimiter);
        }
    }

    /**
     * @param ps
     * @param iaTmp
     */
    private final void printMatch(PrintfStream ps, int[] iaTmp)
    {
        ps.print("    match : ");

        for (int jj = 0; jj < iaTmp.length; jj++)
        {
            ps.print(iaTmp[jj] + " ");
        }

        ps.println();
    }

    /**
    * @param evaluationFile
    */
    private void loadEvaluationFile(String evaluationFile)
    {
        Vector evalLines = ResourceLoader.readLines(evaluationFile);

        smarts = new Vector(evalLines.size());
        smartsDescription = new Vector(evalLines.size());
        matchesMols = new Vector(evalLines.size());
        matchesAtomIdx = new Vector(evalLines.size());

        String line;
        Vector lineV = new Vector();
        String smartsPattern;
        String matches;
        Vector matchesV = new Vector();
        String singleMatch;
        Vector singleMatchV = new Vector();

        for (int i = 0; i < evalLines.size(); i++)
        {
            line = (String) evalLines.get(i);

            //System.out.println("------------------------------");
            JHM.tokenize(lineV, line, " \t\r\n");

            // parse, initialize and generate SMARTS pattern
            // to allow fast pattern matching
            smartsPattern = (String) lineV.get(0);

            JOESmartsPattern parsedSmarts = new JOESmartsPattern();

            if (!parsedSmarts.init(smartsPattern))
            {
                logger.error("Invalid SMARTS pattern :" + smartsPattern);

                continue;
            }

            if (lineV.size() > 2)
            {
                Vector atomIdxList = null;
                Vector matchMolNames = null;
                int size = lineV.size() - 2;
                atomIdxList = new Vector(size);
                matchMolNames = new Vector(size);

                // parse SMART matches
                for (int j = 2; j < lineV.size(); j++)
                {
                    matches = (String) lineV.get(j);
                    singleMatch = matches;
                    JHM.tokenize(singleMatchV, singleMatch, "#");

                    String moleculeName = ((String) singleMatchV.get(0)).trim();

                    //System.out.println("molName:" + moleculeName);
                    if (singleMatchV.size() > 1)
                    {
                        int[] ia = new int[singleMatchV.size() - 1];

                        for (int index = 1; index < singleMatchV.size();
                                index++)
                        {
                            //System.out.print(singleMatchV.get(index) + " ");
                            ia[index - 1] = Integer.parseInt(((String) singleMatchV.get(
                                        index)).trim());
                        }

                        //System.out.println();
                        if (molecules.containsKey(moleculeName))
                        {
                            atomIdxList.add(ia);
                            matchMolNames.add(moleculeName);
                        }
                        else
                        {
                            logger.error("Molecule " + moleculeName +
                                " is not available in the SDF files.");

                            continue;
                        }
                    }
                    else
                    {
                        //logger.warn(
                        //	"No atom matching indices defined for molecule: '"
                        //		+ moleculeName
                        //		+ "'");
                        matchMolNames.add(moleculeName);
                        atomIdxList.add(null);
                    }
                }

                matchesMols.add(matchMolNames);
                matchesAtomIdx.add(atomIdxList);

                smarts.add(parsedSmarts);

                //System.out.println("parsed SMARTS: " + parsedSmarts);
                smartsDescription.add(lineV.get(1));

                //				System.out.println("----------------------------------");
                //				System.out.println(
                //					parsedSmarts.getSMARTS()
                //						+ " "
                //						+ lineV.get(1)
                //						+ " "
                //						+ matchMolNames
                //						+ " "
                //						+ atomIdxList);
            }
            else
            {
                logger.error("No matching molecule defined for: " +
                    smartsPattern);

                continue;
            }
        }
    }

    /**
     * @param molDirectory
     */
    private void loadMolecules(String molDirectory)
    {
        String FILTER = ".*mol";
        File dir = new File(molDirectory);
        File[] files = dir.listFiles(new RegExpFilenameFilter(FILTER));

        if (files == null)
        {
            logger.error("No files where found in: " + dir);
            System.exit(1);
        }

        logger.info("" + files.length + " files where found in: " + dir);
        molecules = new Hashtable(files.length);

        IOType inType = IOTypeHolder.instance().getIOType("SDF");
        String filename = null;
        FileInputStream in = null;
        MoleculeFileType loader = null;
        JOEMol mol = null;

        for (int i = 0; i < files.length; i++)
        {
            filename = files[i].toString();

            try
            {
                in = new FileInputStream(filename);
                loader = JOEFileFormat.getMolReader(in, inType);
            }
             catch (FileNotFoundException ex)
            {
                logger.error("Can not find input file: " + filename);
                System.exit(1);
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
            }

            if (!loader.readable())
            {
                logger.error(inType.getRepresentation() + " is not readable.");
                logger.error("You're invited to write one !;-)");
                System.exit(1);
            }

            boolean success = true;

            for (;;)
            {
                mol = new JOEMol(inType, inType);
                mol.clear();

                try
                {
                    success = loader.read(mol);
                }
                 catch (IOException ex)
                {
                    ex.printStackTrace();
                    System.exit(1);
                }
                 catch (MoleculeIOException ex)
                {
                    ex.printStackTrace();
                    logger.info("Molecule was skipped: " + mol.getTitle());

                    continue;
                }

                if (!success)
                {
                    break;
                }
                else
                {
                    //System.out.println("Loaded " +mol.getTitle()+": "+success + " Empty: " + mol.empty());
                    molecules.put(mol.getTitle().trim(), mol);
                }
            }
        }

        // store all loaded molecules in a vector
        moleculesV = new Vector(molecules.size());

        for (Enumeration e = molecules.keys(); e.hasMoreElements();)
        {
            moleculesV.add(e.nextElement());
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
