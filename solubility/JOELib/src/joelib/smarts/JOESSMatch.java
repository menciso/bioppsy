///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOESSMatch.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.17 $
//            $Date: 2004/07/29 10:16:48 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.smarts.atomexpr.AtomExpr;
import joelib.smarts.bondexpr.BondExpr;

import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.NbrAtomIterator;


/**
 *  The JOESSMatch class performs exhaustive matching using recursion Explicit
 *  stack handling is used to find just a single match in match()
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.17 $, $Date: 2004/07/29 10:16:48 $
 * @cite smarts
 */
public class JOESSMatch implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.smarts.JOESSMatch");

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    protected JOEMol _mol;

    /**
     *  Description of the Field
     */
    protected Pattern _pat;

    /**
     *  Description of the Field
     */
    protected int[] _map;

    /**
     *  Description of the Field
     */
    protected boolean[] _uatoms;
    private ParseSmart _parseSmart;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOESSMatch object
     *
     * @param  mol  Description of the Parameter
     * @param  pat  Description of the Parameter
     */
    public JOESSMatch(ParseSmart parseSmart, JOEMol mol, Pattern pat)
    {
        _mol = mol;
        _pat = pat;
        _map = new int[pat.acount];
        _parseSmart = parseSmart;

        //    System.out.println("pat.acount:"+pat.acount);
        if (!mol.empty())
        {
            _uatoms = new boolean[mol.numAtoms() + 1];

            //memset((char*)_uatoms,'\0',sizeof(bool)*(mol.NumAtoms()+1));
        }
        else
        {
            _uatoms = null;
            logger.error("You can't match an empty molecule (" +
                mol.getTitle() + ").");

            Object obj = null;
            obj.toString();
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     */
    public void finalize()
    {
        if (_uatoms != null)
        {
            _uatoms = null;
        }
    }

    /**
     * @param  mlist  of type int[]
     */
    public void match(Vector mlist)
    {
        match(mlist, -1);
    }

    /**
     * @param  mlist  of type int[]
     * @param  bidx   Description of the Parameter
     */
    private void match(Vector mlist, int bidx)
    {
        if (bidx == -1)
        {
            JOEAtom atom;
            AtomIterator ait = _mol.atomIterator();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                //System.out.print(" a"+atom.getIdx()+"e"+_parseSmart.evalAtomExpr(_pat.atom[0].expr, atom));
                //        ParseSmart parseSmart = new ParseSmart();
                if (_parseSmart.evalAtomExpr(_pat.atom[0].expr, atom))
                {
                    _map[0] = atom.getIdx();
                    _uatoms[atom.getIdx()] = true;
                    match(mlist, 0);
                    _map[0] = 0;
                    _uatoms[atom.getIdx()] = false;
                }
            }

            //      System.out.println("");
            return;
        }

        if (bidx == _pat.bcount)
        {
            //save full match here
            int[] tmpArr = new int[_map.length];
            System.arraycopy(_map, 0, tmpArr, 0, _map.length);

            //mlist.add(new int[]{_map[0]});
            //System.out.print("add "+_map[0]+" ");
            mlist.add(tmpArr);

            return;
        }

        if (_pat.bond[bidx].grow)
        {
            //match the next bond
            int src;
            int dst;
            src = _pat.bond[bidx].src;
            dst = _pat.bond[bidx].dst;

            AtomExpr aexpr = _pat.atom[dst].expr;
            BondExpr bexpr = _pat.bond[bidx].expr;

            JOEAtom atom;

            JOEAtom nbr;
            atom = _mol.getAtom(_map[src]);

            NbrAtomIterator nait = atom.nbrAtomIterator();

            while (nait.hasNext())
            {
                nbr = nait.nextNbrAtom();

                //        ParseSmart parseSmart = new ParseSmart();
                //System.out.println("SMARTS (atoms="+_mol.numAtoms()+"): "+_uatoms+" "+_uatoms.length+" "+nbr.getIdx()+" "+nbr);
                if (!_uatoms[nbr.getIdx()] &&
                        _parseSmart.evalAtomExpr(aexpr, nbr) &&
                        ParseSmart.evalBondExpr(bexpr, nait.actualBond()))
                {
                    _map[dst] = nbr.getIdx();
                    _uatoms[nbr.getIdx()] = true;
                    match(mlist, bidx + 1);
                    _uatoms[nbr.getIdx()] = false;
                    _map[dst] = 0;
                }
            }
        }
        else
        {
            //just check bond here
            JOEBond bond = _mol.getBond(_map[_pat.bond[bidx].src],
                    _map[_pat.bond[bidx].dst]);

            if ((bond != null) &&
                    ParseSmart.evalBondExpr(_pat.bond[bidx].expr, bond))
            {
                match(mlist, bidx + 1);
            }
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
