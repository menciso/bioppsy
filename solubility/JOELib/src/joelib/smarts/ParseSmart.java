///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ParseSmart.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.36 $
//            $Date: 2004/07/29 10:16:48 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts;

import cformat.PrintfStream;

import wsi.ra.tool.PropertyHolder;

import java.io.OutputStream;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.smarts.atomexpr.AtomExpr;
import joelib.smarts.atomexpr.BinAtomExpr;
import joelib.smarts.atomexpr.LeafAtomExpr;
import joelib.smarts.atomexpr.MonAtomExpr;
import joelib.smarts.atomexpr.RecurAtomExpr;
import joelib.smarts.bondexpr.BinBondExpr;
import joelib.smarts.bondexpr.BondExpr;
import joelib.smarts.bondexpr.LeafBondExpr;
import joelib.smarts.bondexpr.MonBondExpr;

import joelib.util.JOEBitVec;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.NbrAtomIterator;
import joelib.util.types.StringString;


/**
 *  Contains methods for SMARTS parsing.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.36 $, $Date: 2004/07/29 10:16:48 $
 * @cite smarts
 */
public class ParseSmart implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.smarts.ParseSmart");

    /**
     *  Description of the Field
     */
    private final static boolean STRICT = false;

    /**
     *  Description of the Field
     */
    private final static boolean RECURSIVE = true;

    /**
     *  Description of the Field
     */
    public final static int ELEMMAX = 104;

    /**
     *  Description of the Field
     */
    public final static int ATOMEXPRPOOL = 1;

    /**
     *  Description of the Field
     */
    public final static int BONDEXPRPOOL = 1;

    /**
     *  Description of the Field
     */
    public final static int IFALSE = 0;

    /**
     *  Description of the Field
     */
    public final static int ITRUE = 1;

    /**
     *  Description of the Field
     */
    public final static int BUFMAX = 1024;

    /**
     *  Description of the Field
     */
    public final static int OE_EVAL_STACKSIZE = 40;
    private static boolean recognizeExpH = false;

    static
    {
        String value = PropertyHolder.instance().getProperties().getProperty(ParseSmart.class.getName() +
                ".anyRecognizesExpliciteHydrogens");

        //System.out.println("name:"+ParseSmart.class.getName()+".anyRecognizesExpliciteHydrogens");
        //System.out.println("value:"+value);
        if (((value != null) && value.equalsIgnoreCase("true")))
        {
            recognizeExpH = true;
        }
        else
        {
            recognizeExpH = false;
        }
    }

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  of type RSCacheEntry
     */
    public Vector rsCache = new Vector();

    /**
     *  Description of the Field
     */
    public char[] buffer = new char[BUFMAX];

    /**
     *  Description of the Field
     */
    public char[] lexPtr;

    /**
     *  Description of the Field
     */
    public char[] mainPtr;

    /**
     *  Description of the Field
     */
    public int lexPtrIndex;

    /**
     *  Description of the Field
     */
    public int mainPtrIndex;
    private int theEnd;

    //~ Constructors ///////////////////////////////////////////////////////////

    public ParseSmart()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the chiralFlag attribute of the ParseSmart class
     *
     * @param  expr  Description of the Parameter
     * @return       The chiralFlag value
     */
    public static int getChiralFlag(AtomExpr expr)
    {
        int size = 0;
        AtomExpr[] stack = new AtomExpr[OE_EVAL_STACKSIZE];

        //memset(stack,'\0',sizeof(AtomExpr*)*OE_EVAL_STACKSIZE);
        boolean lftest = true;
        BinAtomExpr bAE = null;

        for (size = 0, stack[size] = expr; size >= 0; expr = stack[size])
        {
            switch (expr.type)
            {
            case AtomExpr.AE_LEAF:

                LeafAtomExpr lAE = (LeafAtomExpr) expr;

                if (lAE.prop == AtomExpr.AL_CHIRAL)
                {
                    return (lAE.value);
                }

                size--;

                break;

            case AtomExpr.AE_ANDHI:
            case AtomExpr.AE_ANDLO:
                bAE = (BinAtomExpr) expr;

                if (stack[size + 1] == bAE.rgt)
                {
                    size--;
                }
                else if (stack[size + 1] == bAE.lft)
                {
                    if (lftest)
                    {
                        size++;
                        stack[size] = bAE.rgt;
                    }
                    else
                    {
                        size--;
                    }
                }
                else
                {
                    size++;
                    stack[size] = bAE.lft;
                }

                break;

            case AtomExpr.AE_OR:
                bAE = (BinAtomExpr) expr;

                if (stack[size + 1] == bAE.rgt)
                {
                    size--;
                }
                else if (stack[size + 1] == bAE.lft)
                {
                    if (!lftest)
                    {
                        size++;
                        stack[size] = bAE.rgt;
                    }
                    else
                    {
                        size--;
                    }
                }
                else
                {
                    size++;
                    stack[size] = bAE.lft;
                }

                break;

            case AtomExpr.AE_NOT:

                MonAtomExpr mAE = (MonAtomExpr) expr;

                if (stack[size + 1] != mAE.arg)
                {
                    size++;
                    stack[size] = mAE.arg;
                }
                else
                {
                    lftest = !lftest;
                    size--;
                }

                break;

            case AtomExpr.AE_RECUR:
                size--;

                break;

                //         default: Vector vvv=null;
                //                  vvv.add(new Object());
                //                  logger.error("AtomExpr type is not properly defined ParseSmart.getChiralFlag");
                //                  System.exit(1);
            }

            size--;

            if (size < 0)
            {
                return 0;
            }
        }

        return 0;
    }

    /**
     *  Gets the exprOrder attribute of the ParseSmart class
     *
     * @param  expr  Description of the Parameter
     * @return       The exprOrder value
     */
    public static int getExprOrder(BondExpr expr)
    {
        int size = 0;
        BondExpr[] stack = new BondExpr[15];

        //memset(stack,'\0',sizeof(AtomExpr*)*15);
        boolean lftest = true;

        for (size = 0, stack[size] = expr; size >= 0; expr = stack[size])
        {
            switch (expr.type)
            {
            case (BondExpr.BE_LEAF):

                LeafBondExpr lBE = (LeafBondExpr) expr;

                if (lBE.prop == BondExpr.BL_CONST)
                {
                    lftest = true;
                }
                else
                {
                    /* expr.leaf.prop == BondExpr.BL_TYPE    */
                    switch (lBE.value)
                    {
                    case (BondExpr.BT_SINGLE):
                        return (1);

                    case (BondExpr.BT_DOUBLE):
                        return (2);

                    case (BondExpr.BT_TRIPLE):
                        return (3);

                    case (BondExpr.BT_AROM):
                        return (JOEBond.JOE_AROMATIC_BOND_ORDER);

                    default:
                        lftest = true;
                    }

                    size--;

                    break;
                }

            case (BondExpr.BE_NOT):
                return (0);

            case (BondExpr.BE_ANDHI):
            case (BondExpr.BE_ANDLO):
            case (BondExpr.BE_OR):

                BinBondExpr bBE = (BinBondExpr) expr;

                if (stack[size + 1] == bBE.rgt)
                {
                    size--;
                }
                else if (stack[size + 1] == bBE.lft)
                {
                    if (lftest)
                    {
                        size++;
                        stack[size] = bBE.rgt;
                    }
                    else
                    {
                        size--;
                    }
                }
                else
                {
                    size++;
                    stack[size] = bBE.lft;
                }

                break;
            }
        }

        return (0);
    }

    /**
     *  Description of the Method
     *
     * @param  pat   Description of the Parameter
     * @param  stat  Description of the Parameter
     * @param  i     Description of the Parameter
     * @param  os    Description of the Parameter
     */
    public static void displaySMARTSPart(Pattern pat, ParseState stat, int i,
        OutputStream os)
    {
        PrintfStream fp;

        if (os instanceof PrintfStream)
        {
            fp = (PrintfStream) os;
        }
        else
        {
            fp = new PrintfStream(os);
        }

        int count;
        int j;
        int k;
        int l;

        AtomExpr.displaySMARTSAtom(pat.atom[i].expr, fp);
        pat.atom[i].visit = 1;

        /* Generate Closure Definitions!    */
        for (j = 0; j < pat.bcount; j++)
        {
            if (pat.bond[j].visit != i)
            {
                if ((pat.bond[j].src == i) || (pat.bond[j].dst == i))
                {
                    k = (pat.bond[j].src == i) ? pat.bond[j].dst : pat.bond[j].src;

                    if (pat.atom[k].visit == 0)
                    {
                        while (stat.closure[stat.closindex] != -1)
                        {
                            stat.closindex = (stat.closindex + 1) % 100;
                        }

                        stat.closure[stat.closindex] = j;

                        if (stat.closindex > 9)
                        {
                            fp.printf("%%%d", stat.closindex);
                        }
                        else
                        {
                            fp.print(stat.closindex + '0');
                        }

                        stat.closindex++;
                    }
                }
            }
        }

        /* Generate Closure Bonds!    */
        count = 0;

        for (j = 0; j < pat.bcount; j++)
        {
            if (pat.bond[j].visit == i)
            {
                if (pat.bond[j].src == i)
                {
                    k = pat.bond[j].dst;
                }
                else
                {
                    /* pat.bond[j].dst == i    */
                    k = pat.bond[j].src;
                }

                if (pat.atom[k].visit != 0)
                {
                    BondExpr.displaySMARTSBond(pat.bond[j].expr, fp);

                    for (l = 0; l < 100; l++)
                    {
                        if (stat.closure[l] == j)
                        {
                            break;
                        }
                    }

                    if (l > 9)
                    {
                        fp.printf("%%%d", l);
                    }
                    else
                    {
                        fp.print(l + '0');
                    }
                }
                else
                {
                    count++;
                }
            }
        }

        /* Generate Non-closure Bonds!    */
        for (j = 0; j < pat.bcount; j++)
        {
            if (pat.bond[j].visit == i)
            {
                if (pat.bond[j].src == i)
                {
                    k = pat.bond[j].dst;
                }
                else
                {
                    /* pat.bond[j].dst == i    */
                    k = pat.bond[j].src;
                }

                if (pat.atom[k].visit == 0)
                {
                    if (count > 1)
                    {
                        fp.print('(');
                    }

                    BondExpr.displaySMARTSBond(pat.bond[j].expr, fp);
                    displaySMARTSPart(pat, stat, k, fp);

                    if (count > 1)
                    {
                        fp.print(')');
                    }

                    count--;
                }
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  pat  Description of the Parameter
     * @param  os   Description of the Parameter
     */
    public static void generateSMARTSString(Pattern pat, OutputStream os)
    {
        PrintfStream fp;

        if (os instanceof PrintfStream)
        {
            fp = (PrintfStream) os;
        }
        else
        {
            fp = new PrintfStream(os);
        }

        ParseState stat = new ParseState();
        int part;
        int dot;
        int i;

        if (pat == null)
        {
            fp.print("[!*]");

            return;
        }

        for (i = 0; i < pat.bcount; i++)
        {
            pat.bond[i].visit = -1;
        }

        for (i = 0; i < pat.acount; i++)
        {
            pat.atom[i].visit = IFALSE;
        }

        /* Determine Closures    */
        for (i = 0; i < pat.acount; i++)
        {
            if (pat.atom[i].visit == 0)
            {
                traverseSMARTS(pat, i);
            }
        }

        stat.closindex = 1;

        for (i = 0; i < 100; i++)
        {
            stat.closure[i] = -1;
        }

        for (i = 0; i < pat.acount; i++)
        {
            pat.atom[i].visit = IFALSE;
        }

        /* Output SMARTS String    */
        for (part = 1; part < pat.parts; part++)
        {
            if (part != 1)
            {
                fp.print('.');
            }

            dot = 0;
            fp.print('(');

            /* stat.closindex = 1;    */
            while ((i = findSMARTSRoot(pat, part)) != -1)
            {
                if (dot != 0)
                {
                    fp.print('.');
                }
                else
                {
                    dot = 1;
                }

                displaySMARTSPart(pat, stat, i, fp);
            }

            fp.print(')');
        }

        dot = (pat.parts > 1) ? ITRUE : IFALSE;

        /* stat.closindex = 1;    */
        while ((i = findSMARTSRoot(pat, 0)) != -1)
        {
            if (dot != 0)
            {
                fp.print('.');
            }
            else
            {
                dot = 1;
            }

            displaySMARTSPart(pat, stat, i, fp);
        }
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------   */

    /**
     *  Description of the Method
     */
    public static void initSMARTSParser()
    {
        //    freeAEList = null;
        //    freeBEList = null;
    }

    /**
     *  Gets the vectorBinding attribute of the ParseSmart class
     *
     * @return    The vectorBinding value
     */
    public int getVectorBinding()
    {
        int vb = 0;

        lexPtrIndex++;

        //skip colon
        if (Character.isDigit(lexPtr[lexPtrIndex]))
        {
            vb = 0;

            while (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                vb = (vb * 10) + ((lexPtr[lexPtrIndex++]) - '0');
            }
        }

        //System.out.println("parsed vb:"+vb);
        return (vb);
    }

    /**
     *  Description of the Method
     *
     * @param  pat  Description of the Parameter
     * @return      Description of the Return Value
     */
    public Pattern SMARTSError(Pattern pat)
    {
        char[] c = new char[theEnd - mainPtrIndex + 1];
        System.arraycopy(mainPtr, mainPtrIndex, c, 0, theEnd - mainPtrIndex +
            1);
        logger.error("SMARTS Error: \"" + (new String(c)) + "\"");

        StringBuffer sb = new StringBuffer("              ");

        for (int cptr = mainPtrIndex; cptr < lexPtrIndex; cptr++)
        {
            sb.append(" ");
        }

        sb.append("^");
        logger.error(sb.toString());

        //force null pointer exception
        //Object obj=null;
        //obj.toString();
        Pattern.freePattern(pat);

        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  pat   Description of the Parameter
     * @param  stat  Description of the Parameter
     * @param  prev  Description of the Parameter
     * @param  part  Description of the Parameter
     * @return       Description of the Return Value
     */
    public Pattern SMARTSParser(Pattern pat, ParseState stat, int prev, int part)
    {
        int vb = 0;
        AtomExpr aexpr;
        BondExpr bexpr;
        int index;

        bexpr = null;

        while (lexPtrIndex <= theEnd)
        {
            switch (lexPtr[lexPtrIndex++])
            {
            case ('.'):

                if ((bexpr != null) || (prev == -1))
                {
                    return parseSMARTSError(pat, bexpr);
                }

                prev = -1;

                break;

            case ('-'):
            case ('='):
            case ('#'):
            case (':'):
            case ('~'):
            case ('@'):
            case ('/'):
            case ('\\'):
            case ('!'):
                lexPtrIndex--;

                if ((prev == -1) || (bexpr != null))
                {
                    return parseSMARTSError(pat, bexpr);
                }

                bexpr = parseBondExpr(0);

                if (bexpr == null)
                {
                    return parseSMARTSError(pat, bexpr);
                }

                break;

            case ('('):

                if (STRICT)
                {
                    if ((prev == -1) || (bexpr != null))
                    {
                        lexPtrIndex--;

                        return parseSMARTSError(pat, bexpr);
                    }

                    pat = SMARTSParser(pat, stat, prev, part);

                    if (pat == null)
                    {
                        return null;
                    }
                }
                else
                {
                    if (bexpr != null)
                    {
                        lexPtrIndex--;

                        return parseSMARTSError(pat, bexpr);
                    }

                    if (prev == -1)
                    {
                        index = pat.acount;
                        pat = SMARTSParser(pat, stat, -1, part);

                        if (pat == null)
                        {
                            return null;
                        }

                        if (index == pat.acount)
                        {
                            return parseSMARTSError(pat, bexpr);
                        }

                        prev = index;
                    }
                    else
                    {
                        pat = SMARTSParser(pat, stat, prev, part);

                        if (pat == null)
                        {
                            return null;
                        }
                    }
                }

                if (lexPtr[lexPtrIndex] != ')')
                {
                    return parseSMARTSError(pat, bexpr);
                }

                lexPtrIndex++;

                break;

            case (')'):
                lexPtrIndex--;
                if ((prev == -1) || (bexpr != null))
                {
                    return parseSMARTSError(pat, bexpr);
                }

                return pat;

            case ('%'):

                if (prev == -1)
                {
                    lexPtrIndex--;

                    return parseSMARTSError(pat, bexpr);
                }

                if (Character.isDigit(lexPtr[lexPtrIndex]) &&
                        Character.isDigit(lexPtr[lexPtrIndex + 1]))
                {
                    index = (10 * (lexPtr[lexPtrIndex] - '0')) +
                        (lexPtr[lexPtrIndex + 1] - '0');
                    lexPtrIndex += 2;
                }
                else
                {
                    return parseSMARTSError(pat, bexpr);
                }

                if (stat.closure[index] == -1)
                {
                    stat.closord[index] = bexpr;
                    stat.closure[index] = prev;
                }
                else if (stat.closure[index] != prev)
                {
                    BondExpr.freeBondExpr(stat.closord[index]);

                    if (bexpr == null)
                    {
                        bexpr = BondExpr.generateDefaultBond();
                    }

                    Pattern.createBond(pat, bexpr, prev, stat.closure[index]);
                    stat.closure[index] = -1;
                    bexpr = null;
                }
                else
                {
                    return parseSMARTSError(pat, bexpr);
                }

                break;

            case ('0'):
            case ('1'):
            case ('2'):
            case ('3'):
            case ('4'):
            case ('5'):
            case ('6'):
            case ('7'):
            case ('8'):
            case ('9'):
                lexPtrIndex--;

                if (prev == -1)
                {
                    return parseSMARTSError(pat, bexpr);
                }

                index = (lexPtr[lexPtrIndex++]) - '0';

                //System.out.println("Closure: "+index);
                if (stat.closure[index] == -1)
                {
                    stat.closord[index] = bexpr;
                    stat.closure[index] = prev;
                    bexpr = null;
                }
                else if (stat.closure[index] != prev)
                {
                    BondExpr.freeBondExpr(stat.closord[index]);

                    if (bexpr == null)
                    {
                        bexpr = BondExpr.generateDefaultBond();
                    }

                    Pattern.createBond(pat, bexpr, prev, stat.closure[index]);
                    stat.closure[index] = -1;
                    bexpr = null;
                }
                else
                {
                    return parseSMARTSError(pat, bexpr);
                }

                break;

            case ('['):
                aexpr = parseAtomExpr(0);
                vb = (lexPtr[lexPtrIndex] == ':') ? getVectorBinding() : 0;

                if ((aexpr == null) || (lexPtr[lexPtrIndex] != ']'))
                {
                    return parseSMARTSError(pat, bexpr);
                }

                index = Pattern.createAtom(pat, aexpr, part, vb);

                if (prev != -1)
                {
                    if (bexpr == null)
                    {
                        bexpr = BondExpr.generateDefaultBond();
                    }

                    Pattern.createBond(pat, bexpr, prev, index);
                    bexpr = null;
                }

                prev = index;
                lexPtrIndex++;

                break;

            default:
                lexPtrIndex--;
                aexpr = parseSimpleAtomPrimitive();

                if (aexpr == null)
                {
                    return parseSMARTSError(pat, bexpr);
                }

                index = Pattern.createAtom(pat, aexpr, part);

                if (prev != -1)
                {
                    if (bexpr == null)
                    {
                        bexpr = BondExpr.generateDefaultBond();
                    }

                    Pattern.createBond(pat, bexpr, prev, index);
                    bexpr = null;
                }

                prev = index;
            }
        }

        if ((prev == -1) || (bexpr != null))
        {
            return parseSMARTSError(pat, bexpr);
        }

        return pat;
    }

    /**
     *  Description of the Method
     *
     * @param  pat  Description of the Parameter
     */
    public static void markGrowBonds(Pattern pat)
    {
        int i;
        JOEBitVec bv = new JOEBitVec();

        for (i = 0; i < pat.bcount; i++)
        {
            pat.bond[i].grow = (bv.get(pat.bond[i].src) &&
                bv.get(pat.bond[i].dst)) ? false : true;

            bv.setBitOn(pat.bond[i].src);
            bv.setBitOn(pat.bond[i].dst);
        }
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @param  atom  Description of the Parameter
     * @return       Description of the Return Value
     */
    public boolean evalAtomExpr(AtomExpr expr, JOEAtom atom)
    {
        BinAtomExpr bAE = null;

        if (RECURSIVE)
        {
            while (true)
            {
                switch (expr.type)
                {
                case AtomExpr.AE_LEAF:

                    LeafAtomExpr lAE = (LeafAtomExpr) expr;

                    switch (lAE.prop)
                    {
                    case AtomExpr.AL_ELEM:

                        // special hydrogen case
                        // this means user is requesting: #1, so this should match ALWAYS !
                        //if (atom.getAtomicNum()==1)
                        //{
                        //    if(recognizeExpH && lAE.value==atom.getAtomicNum())	return true;
                        //    else return false;
                        //}
                        return (lAE.value == atom.getAtomicNum());

                    case AtomExpr.AL_AROM:

                        //System.out.println("lAE.value="+lAE.value+" atom "+atom.getIdx()+" arom:"+atom.isAromatic());
                        if ((lAE.value != 0) && atom.isAromatic())
                        {
                            return true;
                        }
                        else if ((lAE.value == 0) && !atom.isAromatic())
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    case AtomExpr.AL_HCOUNT:

                        //                                System.out.println("atom:"+atom.getIdx()+" exH:"+atom.explicitHydrogenCount()+" imH:"+atom.implicitHydrogenCount());
                        if (atom.explicitHydrogenCount() > atom.implicitHydrogenCount())
                        {
                            return (lAE.value == atom.explicitHydrogenCount());
                        }
                        else
                        {
                            return (lAE.value == atom.implicitHydrogenCount());
                        }

                    case AtomExpr.AL_DEGREE:
                        return (lAE.value == atom.getValence());

                    case AtomExpr.AL_VALENCE:
                        return (lAE.value == atom.KBOSum());

                    case AtomExpr.AL_CONNECT:
                        return (lAE.value == atom.getImplicitValence());

                    case AtomExpr.AL_HEAVY_CONNECT:
                        return (lAE.value == atom.getHvyValence());

                    case AtomExpr.AL_GROUP:
                        return atom.isElementOfGroup(lAE.value);

                    case AtomExpr.AL_ELECTRONEGATIVE:

                        //                                return atom.isElectronegative(lAE.value);
                        return atom.isElectronegative();

                    case AtomExpr.AL_NEGATIVE:
                        return (lAE.value == (-1 * (atom.getFormalCharge())));

                    case AtomExpr.AL_POSITIVE:
                        return (lAE.value == atom.getFormalCharge());

                    case AtomExpr.AL_HYB:
                        return (lAE.value == atom.getHyb());

                    case AtomExpr.AL_RINGS:

                        if (lAE.value == -1)
                        {
                            return (atom.isInRing());
                        }
                        else if (lAE.value == 0)
                        {
                            return (!(atom.isInRing()));
                        }
                        else
                        {
                            return (atom.memberOfRingCount() == lAE.value);
                        }

                    case AtomExpr.AL_SIZE:

                        if (lAE.value == 0)
                        {
                            return (!atom.isInRing());
                        }
                        else
                        {
                            return (atom.isInRingSize(lAE.value));
                        }

                    case AtomExpr.AL_IMPLICIT:
                        return (lAE.value == atom.implicitHydrogenCount());

                    case AtomExpr.AL_CONST:

                        if (recognizeExpH)
                        {
                            return true;
                        }
                        else
                        {
                            return (!atom.isHydrogen());
                        }

                    default:
                        return (false);
                    }

                case AtomExpr.AE_NOT:

                    MonAtomExpr mAE = (MonAtomExpr) expr;

                    return (!evalAtomExpr(mAE.arg, atom));

                case AtomExpr.AE_ANDHI:

                /* Same as AE_ANDLO    */
                case AtomExpr.AE_ANDLO:
                    bAE = (BinAtomExpr) expr;

                    if (!evalAtomExpr(bAE.lft, atom))
                    {
                        return (false);
                    }

                    expr = bAE.rgt;

                    break;

                case AtomExpr.AE_OR:
                    bAE = (BinAtomExpr) expr;

                    if (evalAtomExpr(bAE.lft, atom))
                    {
                        return (true);
                    }

                    expr = bAE.rgt;

                    break;

                case AtomExpr.AE_RECUR:

                    //see if pattern has been matched
                    RecurAtomExpr rAE = (RecurAtomExpr) expr;
                    RSCacheEntry rsCE;

                    for (int i = 0; i < rsCache.size(); i++)
                    {
                        rsCE = (RSCacheEntry) rsCache.get(i);

                        if (rsCE.pattern == (Pattern) rAE.recur)
                        {
                            //System.out.println("AE_RECUR: cache:"+rsCache.size()+" idx: "+atom.getIdx()+" l:"+rsCE.booleans.length);
                            if (atom.getIdx() > rsCE.booleans.length)
                            {
                                logger.error(
                                    "Unsolved 'molecule+SMARTS'-bug in substructure search.");
                                logger.error("Please send message: " +
                                    rsCE.pattern);
                                logger.error("and molecule: " +
                                    atom.getParent().getTitle());
                                logger.error("molecule: " + atom.getParent());
                                logger.error("to the developer mailing list.");

                                return true;
                            }

                            return rsCE.booleans[atom.getIdx()];
                        }
                    }

                    //perceive and match pattern
                    boolean[] vb = new boolean[atom.getParent().numAtoms() + 1];
                    Vector mlist = new Vector();

                    // of type int[]
                    int[] itmp;

                    if (match(atom.getParent(), (Pattern) rAE.recur, mlist))
                    {
                        for (int j = 0; j < mlist.size(); j++)
                        {
                            itmp = (int[]) mlist.get(j);
                            vb[itmp[0]] = true;
                        }
                    }

                    rsCache.add(new RSCacheEntry((Pattern) rAE.recur, vb));

                    return (vb[atom.getIdx()]);
                }
            }

            //    return false;
        }
        else
        {
            // !RECURSIVE
            int size = 0;
            AtomExpr[] stack = new AtomExpr[OE_EVAL_STACKSIZE];

            //memset(stack,'\0',sizeof(AtomExpr*)*OE_EVAL_STACKSIZE);
            boolean lftest = true;

            for (size = 0, stack[size] = expr; size >= 0; expr = stack[size])
            {
                switch (expr.type)
                {
                case AtomExpr.AE_LEAF:

                    LeafAtomExpr lAE = (LeafAtomExpr) expr;

                    switch (lAE.prop)
                    {
                    //expr.leaf.value
                    case AtomExpr.AL_ELEM:

                        // special hydrogen case
                        // this means user is requesting: #1, so this should match ALWAYS !
                        //if (atom.getAtomicNum()==1)
                        //{
                        //    if(recognizeExpH && lAE.value==atom.getAtomicNum())	lftest=true;
                        //    else lftest=false;
                        //}
                        lftest = (lAE.value == atom.getAtomicNum());

                        break;

                    case AtomExpr.AL_AROM:

                        //lftest = (expr.leaf.value == atom.isAromatic());
                        if ((lAE.value != 0) && atom.isAromatic())
                        {
                            lftest = true;
                        }
                        else if ((lAE.value == 0) && !atom.isAromatic())
                        {
                            lftest = true;
                        }
                        else
                        {
                            lftest = false;
                        }

                        break;

                    case AtomExpr.AL_HCOUNT:

                        if (atom.explicitHydrogenCount() > atom.implicitHydrogenCount())
                        {
                            lftest = (lAE.value == atom.explicitHydrogenCount());
                        }
                        else
                        {
                            lftest = (lAE.value == atom.implicitHydrogenCount());
                        }

                        break;

                    case AtomExpr.AL_DEGREE:
                        lftest = (lAE.value == atom.getValence());

                        break;

                    case AtomExpr.AL_VALENCE:
                        lftest = (lAE.value == atom.BOSum());

                        break;

                    case AtomExpr.AL_CONNECT:

                        //X
                        lftest = (lAE.value == atom.getImplicitValence());

                        break;

                    case AtomExpr.AL_HEAVY_CONNECT:

                        //Q
                        lftest = (lAE.value == atom.getHvyValence());

                        break;

                    case AtomExpr.AL_NEGATIVE:
                        lftest = (lAE.value == (-1 * (atom.getFormalCharge())));

                        break;

                    case AtomExpr.AL_POSITIVE:
                        lftest = (lAE.value == atom.getFormalCharge());

                        break;

                    case AtomExpr.AL_HYB:
                        lftest = (lAE.value == atom.getHyb());

                        break;

                    case AtomExpr.AL_RINGS:

                        if (lAE.value == -1)
                        {
                            lftest = (atom.isInRing());
                        }
                        else if (lAE.value == 0)
                        {
                            lftest = !(atom.isInRing());
                        }
                        else
                        {
                            lftest = (atom.memberOfRingCount() == lAE.value);
                        }

                        break;

                    case AtomExpr.AL_SIZE:

                        if (lAE.value == 0)
                        {
                            lftest = !atom.isInRing();
                        }
                        else
                        {
                            lftest = atom.isInRingSize(lAE.value);
                        }

                        break;

                    case AtomExpr.AL_IMPLICIT:
                        lftest = (lAE.value == atom.implicitHydrogenCount());

                        break;

                    case AtomExpr.AL_CONST:

                        if (recognizeExpH)
                        {
                            lftest = true;
                        }
                        else
                        {
                            lftest = !atom.isHydrogen();
                        }

                        break;

                    case AtomExpr.AL_MASS:
                        break;

                    default:
                        break;
                    }

                    size--;

                    break;

                case AtomExpr.AE_ANDHI:
                    bAE = (BinAtomExpr) expr;

                    if (stack[size + 1] == bAE.rgt)
                    {
                        size--;
                    }
                    else if (stack[size + 1] == bAE.lft)
                    {
                        if (lftest)
                        {
                            size++;
                            stack[size] = bAE.rgt;
                        }
                        else
                        {
                            size--;
                        }
                    }
                    else
                    {
                        size++;
                        stack[size] = bAE.lft;
                    }

                    break;

                case AtomExpr.AE_OR:
                    bAE = (BinAtomExpr) expr;

                    if (stack[size + 1] == bAE.rgt)
                    {
                        size--;
                    }
                    else if (stack[size + 1] == bAE.lft)
                    {
                        if (!lftest)
                        {
                            size++;
                            stack[size] = bAE.rgt;
                        }
                        else
                        {
                            size--;
                        }
                    }
                    else
                    {
                        size++;
                        stack[size] = bAE.lft;
                    }

                    break;

                case AtomExpr.AE_ANDLO:
                    bAE = (BinAtomExpr) expr;

                    if (stack[size + 1] == bAE.rgt)
                    {
                        size--;
                    }
                    else if (stack[size + 1] == bAE.lft)
                    {
                        if (lftest)
                        {
                            size++;
                            stack[size] = bAE.rgt;
                        }
                        else
                        {
                            size--;
                        }
                    }
                    else
                    {
                        size++;
                        stack[size] = bAE.lft;
                    }

                    break;

                case AtomExpr.AE_NOT:

                    MonAtomExpr mAE = (MonAtomExpr) expr;

                    if (stack[size + 1] != mAE.arg)
                    {
                        size++;
                        stack[size] = mAE.arg;
                    }
                    else
                    {
                        lftest = !lftest;
                        size--;
                    }

                    break;

                case AtomExpr.AE_RECUR:

                    //see if pattern has been matched
                    RecurAtomExpr rAE = (RecurAtomExpr) expr;
                    boolean matched = false;

                    RSCacheEntry rsCE;

                    for (int i = 0; i < rsCache.size(); i++)
                    {
                        rsCE = (RSCacheEntry) rsCache.get(i);

                        if (rsCE.pattern == (Pattern) rAE.recur)
                        {
                            lftest = rsCE.booleans[atom.getIdx()];
                            matched = true;

                            break;
                        }
                    }

                    if (!matched)
                    {
                        boolean[] vb = new boolean[atom.getParent().numAtoms() +
                            1];
                        Vector mlist = new Vector();

                        // of type int[]
                        int[] itmp;
                        lftest = false;

                        if (match(atom.getParent(), (Pattern) rAE.recur, mlist))
                        {
                            for (int i = 0; i < mlist.size(); i++)
                            {
                                itmp = (int[]) mlist.get(i);

                                if (itmp[0] == atom.getIdx())
                                {
                                    lftest = true;
                                }

                                vb[itmp[0]] = true;
                            }
                        }

                        rsCache.add(new RSCacheEntry((Pattern) rAE.recur, vb));
                    }

                    size--;

                    break;
                }
            }

            return lftest;
        }
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @param  bond  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static boolean evalBondExpr(BondExpr expr, JOEBond bond)
    {
        BinBondExpr bBE = null;

        if (RECURSIVE)
        {
            while (true)
            {
                switch (expr.type)
                {
                case BondExpr.BE_LEAF:

                    LeafBondExpr lBE = (LeafBondExpr) expr;

                    if (lBE.prop == BondExpr.BL_CONST)
                    {
                        return ((lBE.value != 0) ? true : false);
                    }
                    else
                    {
                        switch (lBE.value)
                        {
                        case BondExpr.BT_SINGLE:
                            return ((bond.getBO() == 1) && !bond.isAromatic());

                        case BondExpr.BT_AROM:
                            return (bond.isAromatic());

                        case BondExpr.BT_DOUBLE:
                            return ((bond.getBO() == 2) && !bond.isAromatic());

                        case BondExpr.BT_TRIPLE:
                            return (bond.getBO() == 3);

                        case BondExpr.BT_RING:
                            return (bond.isInRing());

                        case BondExpr.BT_UP:

                            //System.out.println("SMARTS up ? "+bond.isUp());
                            return bond.isUp();

                        case BondExpr.BT_DOWN:

                            //System.out.println("SMARTS down ? "+bond.isDown());
                            return bond.isDown();

                        case BondExpr.BT_UPUNSPEC:

                            //System.out.println(bond.getParent().getTitle()+" SMARTS up unspecific ? up="+bond.isUp()+" down="+bond.isDown());
                            return !bond.isDown();
                            
                        case BondExpr.BT_DOWNUNSPEC:

                            //System.out.println("SMARTS down pecific ? "+bond.isDown());
                            return !bond.isUp();

                        default:
                            return (false);
                        }
                    }

                case BondExpr.BE_NOT:

                    MonBondExpr mBE = (MonBondExpr) expr;

                    return (!evalBondExpr(mBE.arg, bond));

                case BondExpr.BE_ANDHI:
                case BondExpr.BE_ANDLO:
                    bBE = (BinBondExpr) expr;

                    if (!evalBondExpr(bBE.lft, bond))
                    {
                        return false;
                    }

                    expr = bBE.rgt;

                    break;

                case BondExpr.BE_OR:
                    bBE = (BinBondExpr) expr;

                    if (evalBondExpr(bBE.lft, bond))
                    {
                        return true;
                    }

                    expr = bBE.rgt;

                    break;
                }
            }

            //    return false;
        }
        else
        {
            int size = 0;
            BondExpr[] stack = new BondExpr[OE_EVAL_STACKSIZE];

            //memset(stack,'\0',sizeof(AtomExpr*)*OE_EVAL_STACKSIZE);
            boolean lftest = true;

            for (size = 0, stack[size] = expr; size >= 0; expr = stack[size])
            {
                switch (expr.type)
                {
                case (BondExpr.BE_LEAF):

                    LeafBondExpr lBE = (LeafBondExpr) expr;

                    if (lBE.prop == BondExpr.BL_CONST)
                    {
                        lftest = (lBE.value != 0) ? true : false;
                    }
                    else
                    {
                        /* expr.leaf.prop == BondExpr.BL_TYPE    */
                        switch (lBE.value)
                        {
                        case (BondExpr.BT_SINGLE):
                            lftest = ((bond.getBO() == 1) &&
                                !bond.isAromatic());

                            break;

                        case (BondExpr.BT_DOUBLE):
                            lftest = ((bond.getBO() == 2) &&
                                !bond.isAromatic());

                            break;

                        case (BondExpr.BT_TRIPLE):
                            lftest = (bond.getBO() == 3);

                            break;

                        case (BondExpr.BT_AROM):
                            lftest = bond.isAromatic();

                            break;

                        case (BondExpr.BT_RING):
                            lftest = bond.isInRing();

                            break;

                        case (BondExpr.BT_UP):

                            //System.out.println("SMARTS up ? "+bond.isUp());
                            lftest = bond.isUp();

                            break;

                        case (BondExpr.BT_DOWN):

                            //System.out.println("SMARTS down ? "+bond.isDown());
                            lftest = bond.isDown();

                            break;

                        case (BondExpr.BT_UPUNSPEC):
                            break;

                        case (BondExpr.BT_DOWNUNSPEC):
                            break;
                        }
                    }

                    size--;

                    break;

                case (BondExpr.BE_NOT):

                    MonBondExpr mBE = (MonBondExpr) expr;

                    if (stack[size + 1] != mBE.arg)
                    {
                        size++;
                        stack[size] = mBE.arg;
                    }
                    else
                    {
                        lftest = !lftest;
                        size--;
                    }

                    break;

                case (BondExpr.BE_ANDHI):
                    bBE = (BinBondExpr) expr;

                    if (stack[size + 1] == bBE.rgt)
                    {
                        size--;
                    }
                    else if (stack[size + 1] == bBE.lft)
                    {
                        if (lftest)
                        {
                            size++;
                            stack[size] = bBE.rgt;
                        }
                        else
                        {
                            size--;
                        }
                    }
                    else
                    {
                        size++;
                        stack[size] = bBE.lft;
                    }

                    break;

                case (BondExpr.BE_ANDLO):
                    bBE = (BinBondExpr) expr;

                    if (stack[size + 1] == bBE.rgt)
                    {
                        size--;
                    }
                    else if (stack[size + 1] == bBE.lft)
                    {
                        if (lftest)
                        {
                            size++;
                            stack[size] = bBE.rgt;
                        }
                        else
                        {
                            size--;
                        }
                    }
                    else
                    {
                        size++;
                        stack[size] = bBE.lft;
                    }

                    break;

                case (BondExpr.BE_OR):
                    bBE = (BinBondExpr) expr;

                    if (stack[size + 1] == bBE.rgt)
                    {
                        size--;
                    }
                    else if (stack[size + 1] == bBE.lft)
                    {
                        if (!lftest)
                        {
                            size++;
                            stack[size] = bBE.rgt;
                        }
                        else
                        {
                            size--;
                        }
                    }
                    else
                    {
                        size++;
                        stack[size] = bBE.lft;
                    }

                    break;
                }
            }

            return (lftest);
        }
    }

    /**
     * @param  mlist  {@link java.util.Vector} of <tt>int[]</tt>
     * @param  mol    Description of the Parameter
     * @param  pat    Description of the Parameter
     */
    public void fastSingleMatch(JOEMol mol, Pattern pat, Vector mlist)
    {
        if (mol.empty())
        {
            logger.error("Empty molecule '" + mol.getTitle() + "'");

            return;
        }

        JOEAtom atom;
        JOEAtom a1;
        JOEAtom nbr;
        AtomIterator ait = mol.atomIterator();

        JOEBitVec bv = new JOEBitVec(mol.numAtoms() + 1);

        int[] map = new int[pat.acount];

        // Vector of type NbrAtomIterator
        Vector vi = new Vector();

        if (pat.bcount != 0)
        {
            vi.setSize(pat.bcount);
        }

        int bcount;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (evalAtomExpr(pat.atom[0].expr, atom))
            {
                map[0] = atom.getIdx();

                if (pat.bcount != 0)
                {
                    vi.set(0, null);
                }

                bv.clear();
                bv.setBitOn(atom.getIdx());

                for (bcount = 0; bcount >= 0;)
                {
                    //***entire pattern matched***
                    if (bcount == pat.bcount)
                    {
                        //save full match here
                        mlist.add(map);
                        bcount--;

                        return;

                        //found a single match
                    }

                    //***match the next bond***
                    if (!pat.bond[bcount].grow)
                    {
                        //just check bond here
                        if (vi.get(bcount) == null)
                        {
                            JOEBond bond = mol.getBond(map[pat.bond[bcount].src],
                                    map[pat.bond[bcount].dst]);

                            if ((bond != null) &&
                                    evalBondExpr(pat.bond[bcount].expr, bond))
                            {
                                //vi.set(bcount++ , (vector<OEBond*>::iterator)0x1);
                                vi.set(bcount++, new Object());

                                if (bcount < pat.bcount)
                                {
                                    vi.set(bcount, null);
                                }
                            }
                            else
                            {
                                vi.set(bcount--, null);
                            }
                        }
                        else
                        {
                            //bond must have already been visited - backtrack
                            bcount--;
                        }
                    }
                    else
                    {
                        //need to map atom and check bond
                        a1 = mol.getAtom(map[pat.bond[bcount].src]);

                        if (vi.get(bcount) == null)
                        {
                            //figure out which nbr atom we are mapping
                            //nbr = a1.beginNbrAtom(vi[bcount]);
                            NbrAtomIterator nait = a1.nbrAtomIterator();
                            vi.set(bcount, nait);
                            nbr = nait.nextNbrAtom();
                        }
                        else
                        {
                            bv.setBitOff(map[pat.bond[bcount].dst]);

                            //nbr = a1.nextNbrAtom(vi[bcount]);
                            if (((NbrAtomIterator) vi.get(bcount)).hasNext())
                            {
                                nbr = ((NbrAtomIterator) vi.get(bcount)).nextNbrAtom();
                            }
                            else
                            {
                                nbr = null;
                            }
                        }

                        for (;
                                (nbr != null) &&
                                ((NbrAtomIterator) vi.get(bcount)).hasNext();
                                nbr = ((NbrAtomIterator) vi.get(bcount)).nextNbrAtom())
                        {
                            //a1.nextNbrAtom(vi[bcount]))
                            if (!bv.get(nbr.getIdx()))
                            {
                                if (evalAtomExpr(
                                            pat.atom[pat.bond[bcount].dst].expr,
                                            nbr) &&
                                        evalBondExpr(pat.bond[bcount].expr,
                                            ((NbrAtomIterator) vi.get(bcount)).actualBond()))
                                {
                                    bv.setBitOn(nbr.getIdx());
                                    map[pat.bond[bcount].dst] = nbr.getIdx();
                                    bcount++;

                                    if (bcount < pat.bcount)
                                    {
                                        vi.set(bcount, null);
                                    }

                                    break;
                                }
                            }
                        }

                        if (nbr == null)
                        {
                            //no match - time to backtrack
                            bcount--;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param  mlist  {@link java.util.Vector} of an <tt>int[]</tt>
     * @param  mol    Description of the Parameter
     * @param  pat    Description of the Parameter
     * @return         <tt>true</tt> SMARTS matching was successfull and pattern occur
     */
    public boolean match(JOEMol mol, Pattern pat, Vector mlist)
    {
        if (mol.empty())
        {
            logger.error("Empty molecule '" + mol.getTitle() + "'");

            return false;
        }

        return match(mol, pat, mlist, false);
    }

    /**
     * @param  mlist   {@link java.util.Vector} of an <tt>int[]</tt>
     * @param  mol     Description of the Parameter
     * @param  pat     Description of the Parameter
     * @param  single  Description of the Parameter
     * @return         <tt>true</tt> SMARTS matching was successfull and pattern occur
     */
    public boolean match(JOEMol mol, Pattern pat, Vector mlist, boolean single)
    {
        /*            if(mol.empty())
                    {
                            logger.error("Empty molecule '"+mol.getTitle()+"'");
                            return false;
                    }
        */
        mlist.clear();

        if ((pat == null) || (pat.acount == 0))
        {
            logger.error("No pattern available.");

            //shouldn't ever happen
            return false;
        }

        if (single && !pat.ischiral)
        {
            fastSingleMatch(mol, pat, mlist);
        }
        else
        {
            JOESSMatch ssm = new JOESSMatch(this, mol, pat);
            ssm.match(mlist);
        }

        if (logger.isDebugEnabled())
        {
            if (mlist.size() != 0)
            {
                for (int mm = 0; mm < mlist.size(); mm++)
                {
                    int[] ia = (int[]) mlist.get(mm);

                    for (int i = 0; i < ia.length; i++)
                    {
                        System.out.print(" t" + ia[i]);
                    }
                }

                System.out.println("");
            }
        }

        if (pat.ischiral && mol.has3D())
        {
            int j;
            int k;
            int r1;
            int r2;
            int r3;
            int r4;
            JOEAtom ra1;
            JOEAtom ra2;
            JOEAtom ra3;
            JOEAtom ra4;

            Vector tmpmlist = new Vector();

            // of type int[]
            //Vector vtmp;
            int[] itmp;

            for (j = 0; j < pat.acount; j++)
            {
                if (pat.atom[j].chiral != 0)
                {
                    r1 = r2 = r3 = r4 = -1;
                    r2 = j;

                    for (k = 0; k < pat.bcount; k++)
                    {
                        if (pat.bond[k].dst == r2)
                        {
                            if (r1 == -1)
                            {
                                r1 = pat.bond[k].src;
                            }
                            else if (r3 == -1)
                            {
                                r3 = pat.bond[k].src;
                            }
                            else if (r4 == -1)
                            {
                                r4 = pat.bond[k].src;
                            }
                        }
                    }

                    for (k = 0; k < pat.bcount; k++)
                    {
                        if (pat.bond[k].src == r2)
                        {
                            if (r1 == -1)
                            {
                                r1 = pat.bond[k].dst;
                            }
                            else if (r3 == -1)
                            {
                                r3 = pat.bond[k].dst;
                            }
                            else if (r4 == -1)
                            {
                                r4 = pat.bond[k].dst;
                            }
                        }
                    }

                    if ((r1 == -1) || (r2 == -1) || (r3 == -1) || (r4 == -1))
                    {
                        continue;
                    }

                    tmpmlist.clear();

                    for (int m = 0; m < mlist.size(); m++)
                    {
                        //vtmp = (Vector)mlist.get(m);
                        //itmp = (int[])vtmp.get(r1);	ra1 = mol.getAtom( itmp[0] );
                        //itmp = (int[])vtmp.get(r2);	ra2 = mol.getAtom( itmp[0] );
                        //itmp = (int[])vtmp.get(r3);	ra3 = mol.getAtom( itmp[0] );
                        //itmp = (int[])vtmp.get(r4);	ra4 = mol.getAtom( itmp[0] );
                        itmp = (int[]) mlist.get(m);
                        ra1 = mol.getAtom(itmp[r1]);
                        ra2 = mol.getAtom(itmp[r2]);
                        ra3 = mol.getAtom(itmp[r3]);
                        ra4 = mol.getAtom(itmp[r4]);

                        double sign = XYZVector.calcTorsionAngle(ra1.getVector(),
                                ra2.getVector(), ra3.getVector(),
                                ra4.getVector());

                        if ((sign > 0.0) &&
                                (pat.atom[j].chiral == AtomExpr.AL_ANTICLOCKWISE))
                        {
                            continue;
                        }

                        if ((sign < 0.0) &&
                                (pat.atom[j].chiral == AtomExpr.AL_CLOCKWISE))
                        {
                            continue;
                        }

                        //ok - go ahead and save it
                        //tmpmlist.put( vtmp );
                        tmpmlist.add(itmp);
                    }

                    mlist = tmpmlist;
                }
            }
        }

        //if(mlist.size()==0)logger.info("No matching found.");
        return (mlist.size() != 0);
    }

    /**
     *  Description of the Method
     *
     * @param  level  Description of the Parameter
     * @return        Description of the Return Value
     */
    public AtomExpr parseAtomExpr(int level)
    {
        AtomExpr expr1;
        AtomExpr expr2;
        int prev;

        switch (level)
        {
        case (0):

            /* Low Precedence Conjunction    */
            expr1 = parseAtomExpr(1);

            if (expr1 == null)
            {
                return null;
            }

            while (lexPtr[lexPtrIndex] == ';')
            {
                lexPtrIndex++;
                expr2 = parseAtomExpr(1);

                if (expr2 == null)
                {
                    AtomExpr.freeAtomExpr(expr1);

                    return null;
                }

                expr1 = AtomExpr.buildAtomBin(AtomExpr.AE_ANDLO, expr1, expr2);
            }

            return expr1;

        case (1):

            /* Disjunction    */
            expr1 = parseAtomExpr(2);

            if (expr1 == null)
            {
                return null;
            }

            while (lexPtr[lexPtrIndex] == ',')
            {
                lexPtrIndex++;
                expr2 = parseAtomExpr(2);

                if (expr2 == null)
                {
                    AtomExpr.freeAtomExpr(expr1);

                    return null;
                }

                expr1 = AtomExpr.buildAtomBin(AtomExpr.AE_OR, expr1, expr2);
            }

            return (expr1);

        case (2):

            /* High Precedence Conjunction    */
            expr1 = parseAtomExpr(3);

            if (expr1 == null)
            {
                return null;
            }

            while ((lexPtr[lexPtrIndex] != ']') &&
                    (lexPtr[lexPtrIndex] != ';') &&
                    (lexPtr[lexPtrIndex] != ',') && (lexPtrIndex <= theEnd))
            {
                if (lexPtr[lexPtrIndex] == '&')
                {
                    lexPtrIndex++;
                }

                prev = lexPtrIndex;
                expr2 = parseAtomExpr(3);

                if (expr2 == null)
                {
                    if (prev != lexPtrIndex)
                    {
                        AtomExpr.freeAtomExpr(expr1);

                        return null;
                    }
                    else
                    {
                        return (expr1);
                    }
                }

                expr1 = AtomExpr.buildAtomBin(AtomExpr.AE_ANDHI, expr1, expr2);
            }

            return (expr1);

        case (3):

            /* Negation or Primitive    */
            if (lexPtr[lexPtrIndex] == '!')
            {
                lexPtrIndex++;
                expr1 = parseAtomExpr(3);

                if (expr1 == null)
                {
                    return null;
                }

                return (AtomExpr.buildAtomNot(expr1));
            }

            return (parseComplexAtomPrimitive());
        }

        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  level  Description of the Parameter
     * @return        Description of the Return Value
     */
    public BondExpr parseBondExpr(int level)
    {
        BondExpr expr1;
        BondExpr expr2;
        int prev;

        switch (level)
        {
        case (0):

            /* Low Precedence Conjunction    */
            expr1 = parseBondExpr(1);

            if (expr1 == null)
            {
                return null;
            }

            while (lexPtr[lexPtrIndex] == ';')
            {
                lexPtrIndex++;
                expr2 = parseBondExpr(1);

                if (expr2 == null)
                {
                    BondExpr.freeBondExpr(expr1);

                    return null;
                }

                expr1 = BondExpr.buildBondBin(BondExpr.BE_ANDLO, expr1, expr2);
            }

            return expr1;

        case (1):

            /* Disjunction    */
            expr1 = parseBondExpr(2);

            if (expr1 == null)
            {
                return null;
            }

            while (lexPtr[lexPtrIndex] == ',')
            {
                lexPtrIndex++;
                expr2 = parseBondExpr(2);

                if (expr2 == null)
                {
                    BondExpr.freeBondExpr(expr1);

                    return null;
                }

                expr1 = BondExpr.buildBondBin(BondExpr.BE_OR, expr1, expr2);
            }

            return expr1;

        case (2):

            /* High Precedence Conjunction    */
            expr1 = parseBondExpr(3);

            if (expr1 == null)
            {
                return null;
            }

            while ((lexPtr[lexPtrIndex] != ']') &&
                    (lexPtr[lexPtrIndex] != ';') &&
                    (lexPtr[lexPtrIndex] != ',') && (lexPtrIndex <= theEnd))
            {
                if (lexPtr[lexPtrIndex] == '&')
                {
                    lexPtrIndex++;
                }

                prev = lexPtrIndex;
                expr2 = parseBondExpr(3);

                if (expr2 == null)
                {
                    if (prev != lexPtrIndex)
                    {
                        BondExpr.freeBondExpr(expr1);

                        return null;
                    }
                    else
                    {
                        return expr1;
                    }
                }

                expr1 = BondExpr.buildBondBin(BondExpr.BE_ANDHI, expr1, expr2);
            }

            return expr1;

        case (3):

            /* Negation or Primitive    */
            if (lexPtr[lexPtrIndex] == '!')
            {
                lexPtrIndex++;
                expr1 = parseBondExpr(3);

                if (expr1 == null)
                {
                    return null;
                }

                return BondExpr.buildBondNot(expr1);
            }

            return parseBondPrimitive();
        }

        return null;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public BondExpr parseBondPrimitive()
    {
        switch (lexPtr[lexPtrIndex++])
        {
        case ('-'):
            return BondExpr.buildBondLeaf(BondExpr.BL_TYPE, BondExpr.BT_SINGLE);

        case ('='):
            return BondExpr.buildBondLeaf(BondExpr.BL_TYPE, BondExpr.BT_DOUBLE);

        case ('#'):
            return BondExpr.buildBondLeaf(BondExpr.BL_TYPE, BondExpr.BT_TRIPLE);

        case (':'):
            return BondExpr.buildBondLeaf(BondExpr.BL_TYPE, BondExpr.BT_AROM);

        case ('@'):
            return BondExpr.buildBondLeaf(BondExpr.BL_TYPE, BondExpr.BT_RING);

        case ('~'):
            return BondExpr.buildBondLeaf(BondExpr.BL_CONST, ITRUE);

        case ('/'):

            if (lexPtr[lexPtrIndex] == '?')
            {
                lexPtrIndex++;

                return BondExpr.buildBondLeaf(BondExpr.BL_TYPE,
                    BondExpr.BT_UPUNSPEC);
            }

            return BondExpr.buildBondLeaf(BondExpr.BL_TYPE, BondExpr.BT_UP);

        case ('\\'):

            if (lexPtr[lexPtrIndex] == '?')
            {
                lexPtrIndex++;

                return BondExpr.buildBondLeaf(BondExpr.BL_TYPE,
                    BondExpr.BT_DOWNUNSPEC);
            }

            return BondExpr.buildBondLeaf(BondExpr.BL_TYPE, BondExpr.BT_DOWN);
        }

        lexPtrIndex--;

        return null;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public AtomExpr parseComplexAtomPrimitive()
    {
        Pattern pat;
        int index;

        switch (lexPtr[lexPtrIndex++])
        {
        case ('#'):

            if (!Character.isDigit(lexPtr[lexPtrIndex]))
            {
                if (lexPtr[lexPtrIndex] == 'N')
                {
                    lexPtrIndex++;

                    return AtomExpr.buildAtomLeaf(AtomExpr.AL_ELECTRONEGATIVE, 0);
                }
                else
                {
                    return null;
                }
            }

            index = 0;

            while (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = (index * 10) + (lexPtr[lexPtrIndex++] - '0');
            }

            if (index > ELEMMAX)
            {
                lexPtrIndex--;

                return null;
            }
            else if (index == 0)
            {
                return null;
            }

            return AtomExpr.generateElement(index);

        case ('$'):

            if (lexPtr[lexPtrIndex] != '(')
            {
                return null;
            }

            lexPtrIndex++;

            if (STRICT)
            {
                pat = parseSMARTSPart(Pattern.allocPattern(), 0);
            }
            else
            {
                pat = parseSMARTSPattern();
            }

            if (pat == null)
            {
                return null;
            }

            if (lexPtr[lexPtrIndex] != ')')
            {
                Pattern.freePattern(pat);

                return null;
            }

            lexPtrIndex++;

            return AtomExpr.buildAtomRecurs(pat);

        case ('*'):
            return (AtomExpr.buildAtomLeaf(AtomExpr.AL_CONST, ITRUE));

        case ('+'):

            if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + (lexPtr[lexPtrIndex++] - '0');
                }
            }
            else
            {
                index = 1;

                while (lexPtr[lexPtrIndex] == '+')
                {
                    lexPtrIndex++;
                    index++;
                }
            }

            return (AtomExpr.buildAtomLeaf(AtomExpr.AL_POSITIVE, index));

        case ('-'):

            if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + (lexPtr[lexPtrIndex++] - '0');
                }
            }
            else
            {
                index = 1;

                while (lexPtr[lexPtrIndex] == '-')
                {
                    lexPtrIndex++;
                    index++;
                }
            }

            return AtomExpr.buildAtomLeaf(AtomExpr.AL_NEGATIVE, index);

        case '@':

            if (lexPtr[lexPtrIndex] != '@')
            {
                return AtomExpr.buildAtomLeaf(AtomExpr.AL_CHIRAL,
                    AtomExpr.AL_ANTICLOCKWISE);
            }

            lexPtrIndex++;

            return AtomExpr.buildAtomLeaf(AtomExpr.AL_CHIRAL,
                AtomExpr.AL_CLOCKWISE);

        case '^':

            if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + (lexPtr[lexPtrIndex++] - '0');
                }

                return AtomExpr.buildAtomLeaf(AtomExpr.AL_HYB, index);
            }
            else
            {
                return AtomExpr.buildAtomLeaf(AtomExpr.AL_HYB, 1);
            }

        case ('0'):
        case ('1'):
        case ('2'):
        case ('3'):
        case ('4'):
        case ('5'):
        case ('6'):
        case ('7'):
        case ('8'):
        case ('9'):
            index = (lexPtr[lexPtrIndex - 1]) - '0';

            while (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = (index * 10) + (lexPtr[lexPtrIndex++] - '0');
            }

            return AtomExpr.buildAtomLeaf(AtomExpr.AL_MASS, index);

        case ('A'):

            switch (lexPtr[lexPtrIndex++])
            {
            case ('c'):
                return AtomExpr.generateElement(89);

            case ('g'):
                return AtomExpr.generateElement(47);

            case ('l'):
                return AtomExpr.generateElement(13);

            case ('m'):
                return AtomExpr.generateElement(95);

            case ('r'):
                return AtomExpr.generateElement(18);

            case ('s'):
                return AtomExpr.generateElement(33);

            case ('t'):
                return AtomExpr.generateElement(85);

            case ('u'):
                return AtomExpr.generateElement(79);
            }

            lexPtrIndex--;

            return AtomExpr.buildAtomLeaf(AtomExpr.AL_AROM, 0);

        case ('B'):

            switch (lexPtr[lexPtrIndex++])
            {
            case ('a'):
                return AtomExpr.generateElement(56);

            case ('e'):
                return AtomExpr.generateElement(4);

            case ('i'):
                return AtomExpr.generateElement(83);

            case ('k'):
                return AtomExpr.generateElement(97);

            case ('r'):
                return AtomExpr.generateElement(35);
            }

            lexPtrIndex--;

            return AtomExpr.generateElement(5);

        case ('C'):

            switch (lexPtr[lexPtrIndex++])
            {
            case ('a'):
                return AtomExpr.generateElement(20);

            case ('d'):
                return AtomExpr.generateElement(48);

            case ('e'):
                return AtomExpr.generateElement(58);

            case ('f'):
                return AtomExpr.generateElement(98);

            case ('l'):
                return AtomExpr.generateElement(17);

            case ('m'):
                return AtomExpr.generateElement(96);

            case ('o'):
                return AtomExpr.generateElement(27);

            case ('r'):
                return AtomExpr.generateElement(24);

            case ('s'):
                return AtomExpr.generateElement(55);

            case ('u'):
                return AtomExpr.generateElement(29);
            }

            lexPtrIndex--;

            return AtomExpr.generateAromElem(6, IFALSE);

        case ('D'):

            if (lexPtr[lexPtrIndex] == 'y')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(66);
            }
            else if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + (lexPtr[lexPtrIndex++] - '0');
                }

                return AtomExpr.buildAtomLeaf(AtomExpr.AL_DEGREE, index);
            }

            break;

        case ('E'):

            if (lexPtr[lexPtrIndex] == 'r')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(68);
            }
            else if (lexPtr[lexPtrIndex] == 's')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(99);
            }
            else if (lexPtr[lexPtrIndex] == 'u')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(63);
            }

            break;

        case ('F'):

            if (lexPtr[lexPtrIndex] == 'e')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(26);
            }
            else if (lexPtr[lexPtrIndex] == 'm')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(100);
            }
            else if (lexPtr[lexPtrIndex] == 'r')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(87);
            }

            return AtomExpr.generateElement(9);

        case ('G'):

            if (lexPtr[lexPtrIndex] == 'a')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(31));
            }
            else if (lexPtr[lexPtrIndex] == 'd')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(64));
            }
            else if (lexPtr[lexPtrIndex] == 'e')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(32));
            }
            else if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + ((lexPtr[lexPtrIndex++]) - '0');
                }

                return (AtomExpr.buildAtomLeaf(AtomExpr.AL_GROUP, index));
            }

            break;

        case ('H'):

            if (lexPtr[lexPtrIndex] == 'e')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(2));
            }
            else if (lexPtr[lexPtrIndex] == 'f')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(72));
            }
            else if (lexPtr[lexPtrIndex] == 'g')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(80));
            }
            else if (lexPtr[lexPtrIndex] == 'o')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(67));
            }
            else if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + (lexPtr[lexPtrIndex++] - '0');
                }

                return (AtomExpr.buildAtomLeaf(AtomExpr.AL_HCOUNT, index));
            }

            return (AtomExpr.buildAtomLeaf(AtomExpr.AL_HCOUNT, 1));

        /* AtomExpr.buildAtomLeaf(AtomExpr.AL_HCOUNT,1) ???    */
        /* or else AtomExpr.generateElement(1) ???    */
        case ('I'):

            if (lexPtr[lexPtrIndex] == 'n')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(49));
            }
            else if (lexPtr[lexPtrIndex] == 'r')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(77));
            }

            return (AtomExpr.generateElement(53));

        case ('K'):

            if (lexPtr[lexPtrIndex] == 'r')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(36));
            }

            return (AtomExpr.generateElement(19));

        case ('L'):

            if (lexPtr[lexPtrIndex] == 'a')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(57));
            }
            else if (lexPtr[lexPtrIndex] == 'i')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(3));
            }
            else if (lexPtr[lexPtrIndex] == 'r')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(103));
            }
            else if (lexPtr[lexPtrIndex] == 'u')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(71));
            }

            break;

        case ('M'):

            if (lexPtr[lexPtrIndex] == 'd')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(101));
            }
            else if (lexPtr[lexPtrIndex] == 'g')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(12));
            }
            else if (lexPtr[lexPtrIndex] == 'n')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(25));
            }
            else if (lexPtr[lexPtrIndex] == 'o')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(42));
            }

            break;

        case ('N'):

            switch (lexPtr[lexPtrIndex++])
            {
            case ('a'):
                return (AtomExpr.generateElement(11));

            case ('b'):
                return (AtomExpr.generateElement(41));

            case ('d'):
                return (AtomExpr.generateElement(60));

            case ('e'):
                return (AtomExpr.generateElement(10));

            case ('i'):
                return (AtomExpr.generateElement(28));

            case ('o'):
                return (AtomExpr.generateElement(102));

            case ('p'):
                return (AtomExpr.generateElement(93));
            }

            lexPtrIndex--;

            return (AtomExpr.generateAromElem(7, IFALSE));

        case ('O'):

            if (lexPtr[lexPtrIndex] == 's')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(76));
            }

            return (AtomExpr.generateAromElem(8, IFALSE));

        case ('P'):

            switch (lexPtr[lexPtrIndex++])
            {
            case ('a'):
                return (AtomExpr.generateElement(91));

            case ('b'):
                return (AtomExpr.generateElement(82));

            case ('d'):
                return (AtomExpr.generateElement(46));

            case ('m'):
                return (AtomExpr.generateElement(61));

            case ('o'):
                return (AtomExpr.generateElement(84));

            case ('r'):
                return (AtomExpr.generateElement(59));

            case ('t'):
                return (AtomExpr.generateElement(78));

            case ('u'):
                return (AtomExpr.generateElement(94));
            }

            lexPtrIndex--;
            return (AtomExpr.generateElement(15));

        case ('Q'):

            if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + ((lexPtr[lexPtrIndex++]) - '0');
                }

                return (AtomExpr.buildAtomLeaf(AtomExpr.AL_HEAVY_CONNECT, index));
            }

            break;

        case ('R'):

            switch (lexPtr[lexPtrIndex++])
            {
            case ('a'):
                return (AtomExpr.generateElement(88));

            case ('b'):
                return (AtomExpr.generateElement(37));

            case ('e'):
                return (AtomExpr.generateElement(75));

            case ('h'):
                return (AtomExpr.generateElement(45));

            case ('n'):
                return (AtomExpr.generateElement(86));

            case ('u'):
                return (AtomExpr.generateElement(44));
            }

            lexPtrIndex--;

            if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + ((lexPtr[lexPtrIndex++]) - '0');
                }
            }
            else
            {
                index = -1;
            }

            return (AtomExpr.buildAtomLeaf(AtomExpr.AL_RINGS, index));

        case ('S'):

            switch (lexPtr[lexPtrIndex++])
            {
            case ('b'):
                return (AtomExpr.generateElement(51));

            case ('c'):
                return (AtomExpr.generateElement(21));

            case ('e'):
                return (AtomExpr.generateElement(34));

            case ('i'):
                return (AtomExpr.generateElement(14));

            case ('m'):
                return (AtomExpr.generateElement(62));

            case ('n'):
                return (AtomExpr.generateElement(50));

            case ('r'):
                return (AtomExpr.generateElement(38));
            }

            lexPtrIndex--;

            return (AtomExpr.generateAromElem(16, IFALSE));

        case ('T'):

            switch (lexPtr[lexPtrIndex++])
            {
            case ('a'):
                return (AtomExpr.generateElement(73));

            case ('b'):
                return (AtomExpr.generateElement(65));

            case ('c'):
                return (AtomExpr.generateElement(43));

            case ('e'):
                return (AtomExpr.generateElement(52));

            case ('h'):
                return (AtomExpr.generateElement(90));

            case ('i'):
                return (AtomExpr.generateElement(22));

            case ('l'):
                return (AtomExpr.generateElement(81));

            case ('m'):
                return (AtomExpr.generateElement(69));
            }

            lexPtrIndex--;

            break;

        case ('U'):
            return (AtomExpr.generateElement(92));

        case ('V'):
            return (AtomExpr.generateElement(23));

        case ('W'):
            return (AtomExpr.generateElement(74));

        case ('X'):

            if (lexPtr[lexPtrIndex] == 'e')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(54));
            }
            else if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + ((lexPtr[lexPtrIndex++]) - '0');
                }

                return (AtomExpr.buildAtomLeaf(AtomExpr.AL_CONNECT, index));
            }

            break;

        case ('Y'):

            if (lexPtr[lexPtrIndex] == 'b')
            {
                lexPtrIndex++;

                return (AtomExpr.generateElement(70));
            }

            return (AtomExpr.generateElement(39));

        case ('Z'):

            if (lexPtr[lexPtrIndex] == 'n')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(30);
            }
            else if (lexPtr[lexPtrIndex] == 'r')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(40);
            }

            break;

        case ('a'):

            if (lexPtr[lexPtrIndex] == 's')
            {
                lexPtrIndex++;

                return AtomExpr.generateAromElem(33, ITRUE);
            }

            return AtomExpr.buildAtomLeaf(AtomExpr.AL_AROM, 1);

        case ('c'):
            return AtomExpr.generateAromElem(6, ITRUE);

        case ('h'):

            if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + ((lexPtr[lexPtrIndex++]) - '0');
                }
            }
            else
            {
                index = 1;
            }

            return AtomExpr.buildAtomLeaf(AtomExpr.AL_IMPLICIT, index);

        case ('n'):
            return AtomExpr.generateAromElem(7, ITRUE);

        case ('o'):
            return AtomExpr.generateAromElem(8, ITRUE);

        case ('p'):
            return AtomExpr.generateAromElem(15, ITRUE);

        case ('r'):

            if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + ((lexPtr[lexPtrIndex++]) - '0');
                }

                if (index == 0)
                {
                    return AtomExpr.buildAtomLeaf(AtomExpr.AL_RINGS, 0);
                }

                return AtomExpr.buildAtomLeaf(AtomExpr.AL_SIZE, index);
            }

            return AtomExpr.buildAtomLeaf(AtomExpr.AL_RINGS, -1);

        case ('s'):

            if (lexPtr[lexPtrIndex] == 'i')
            {
                lexPtrIndex++;

                return AtomExpr.generateAromElem(14, ITRUE);
            }

            return AtomExpr.generateAromElem(16, ITRUE);

        case ('v'):

            if (Character.isDigit(lexPtr[lexPtrIndex]))
            {
                index = 0;

                while (Character.isDigit(lexPtr[lexPtrIndex]))
                {
                    index = (index * 10) + ((lexPtr[lexPtrIndex++]) - '0');
                }

                return AtomExpr.buildAtomLeaf(AtomExpr.AL_VALENCE, index);
            }

            break;
        }

        lexPtrIndex--;

        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  pat   Description of the Parameter
     * @param  expr  Description of the Parameter
     * @return       Description of the Return Value
     */
    public Pattern parseSMARTSError(Pattern pat, BondExpr expr)
    {
        if (expr != null)
        {
            BondExpr.freeBondExpr(expr);
        }

        return SMARTSError(pat);
    }

    /**
     *  Description of the Method
     *
     * @param  result  Description of the Parameter
     * @param  part    Description of the Parameter
     * @return         Description of the Return Value
     */
    public Pattern parseSMARTSPart(Pattern result, int part)
    {
        ParseState stat = new ParseState();
        int i;
        boolean flag;

        for (i = 0; i < 100; i++)
        {
            stat.closure[i] = -1;
        }

        result = SMARTSParser(result, stat, -1, part);

        flag = false;

        for (i = 0; i < 100; i++)
        {
            if (stat.closure[i] != -1)
            {
                BondExpr.freeBondExpr(stat.closord[i]);
                flag = true;
            }
        }

        if (result != null)
        {
            if (flag)
            {
                return (SMARTSError(result));
            }
            else
            {
                markGrowBonds(result);
                result.ischiral = false;

                for (i = 0; i < result.acount; i++)
                {
                    result.atom[i].chiral = getChiralFlag(result.atom[i].expr);

                    if (result.atom[i].chiral != 0)
                    {
                        result.ischiral = true;
                    }
                }

                return (result);
            }
        }
        else
        {
            return null;
        }
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public Pattern parseSMARTSPattern()
    {
        Pattern result = new Pattern();
        result = Pattern.allocPattern();

        while (lexPtr[lexPtrIndex] == '(')
        {
            lexPtrIndex++;
            result = parseSMARTSPart(result, result.parts);

            if (result == null)
            {
                return null;
            }

            result.parts++;

            if (lexPtr[lexPtrIndex] != ')')
            {
                return SMARTSError(result);
            }

            lexPtrIndex++;

            if ((lexPtrIndex <= theEnd) || (lexPtr[lexPtrIndex] == ')'))
            {
                return result;
            }

            if (lexPtr[lexPtrIndex] != '.')
            {
                return SMARTSError(result);
            }

            lexPtrIndex++;
        }

        return parseSMARTSPart(result, 0);
    }

    //public static Pattern parseSMARTSRecord( String smartsp)
    //{
    //  return parseSMARTSRecord( smartsp.toCharArray() );
    //}
    //
    //public static Pattern parseSMARTSRecord( char ptr[])
    //{
    //  return parseSMARTSRecord(ptr, 0, ptr.length);
    //}

    /**
     *  Description of the Method
     *
     * @param  ptr       Description of the Parameter
     * @param  ptrIndex  Description of the Parameter
     * @param  _theEnd   Description of the Parameter
     * @return           Description of the Return Value
     */
    public Pattern parseSMARTSRecord(char[] ptr, int ptrIndex, int _theEnd)
    {
        char[] src;
        int srcIndex = 0;
        theEnd = _theEnd;

        src = ptr;
        srcIndex = ptrIndex;

        while ((srcIndex <= theEnd) && !Character.isSpaceChar(src[srcIndex]))
        {
            //      System.out.print(" "+srcIndex);
            srcIndex++;
        }

        if (Character.isSpaceChar(src[srcIndex]))
        {
            theEnd = (srcIndex++);

            //lexPtr[srcIndex++] = '\0';
            while (Character.isSpaceChar(src[srcIndex]))
            {
                srcIndex++;
            }
        }

        //    dst = descr;
        //    int descrIndex = 0;
        //    while( srcIndex<=theEnd && (dstIndex<descrIndex+78) )
        //    {
        //      if( Character.isSpaceChar(src[srcIndex]) )
        //      {
        //        dst[dstIndex++] = ' ';
        //        while( Character.isSpaceChar(src[srcIndex]) ) srcIndex++;
        //      }
        //      else dst[dstIndex++] = src[srcIndex++];
        //    }
        //    //theEndDescr=dstIndex;//lexPtr[dstIndex] = '\0';
        return parseSMARTSString(buffer, 0, theEnd);
    }

    /**
     *  Description of the Method
     *
     * @param  smarts  Description of the Parameter
     * @return         Description of the Return Value
     */
    public Pattern parseSMARTSString(String smarts)
    {
        return parseSMARTSString(smarts.toCharArray());
    }

    /**
     *  Description of the Method
     *
     * @param  ptr  Description of the Parameter
     * @return      Description of the Return Value
     */
    public Pattern parseSMARTSString(char[] ptr)
    {
        return parseSMARTSString(ptr, 0, ptr.length - 1);
    }

    /**
     *  Description of the Method
     *
     * @param  ptr      Description of the Parameter
     * @param  index    Description of the Parameter
     * @param  _theEnd  Description of the Parameter
     * @return          Description of the Return Value
     */
    public Pattern parseSMARTSString(char[] ptr, int index, int _theEnd)
    {
        Pattern result;

        theEnd = _theEnd;
        lexPtr = mainPtr = ptr;
        lexPtrIndex = mainPtrIndex = index;

        if ((ptr == null) || (lexPtrIndex > theEnd))
        {
            return null;
        }

        result = parseSMARTSPattern();

        if ((result != null) && (lexPtrIndex <= theEnd))
        {
            return SMARTSError(result);
        }

        return result;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public AtomExpr parseSimpleAtomPrimitive()
    {
        switch (lexPtr[lexPtrIndex++])
        {
        case '*':
            return AtomExpr.buildAtomLeaf(AtomExpr.AL_CONST, ITRUE);

        case 'A':

            if (!STRICT)
            {
                return AtomExpr.buildAtomLeaf(AtomExpr.AL_AROM, IFALSE);
            }
            else
            {
                break;
            }

        case 'B':

            if (lexPtr[lexPtrIndex] == 'r')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(35);
            }

            return AtomExpr.generateElement(5);

        case 'C':

            if (lexPtr[lexPtrIndex] == 'l')
            {
                lexPtrIndex++;

                return AtomExpr.generateElement(17);
            }

            return AtomExpr.generateAromElem(6, IFALSE);

        case 'F':
            return AtomExpr.generateElement(9);

        case 'I':
            return AtomExpr.generateElement(53);

        case 'N':
            return AtomExpr.generateAromElem(7, IFALSE);

        case 'O':
            return AtomExpr.generateAromElem(8, IFALSE);

        case 'P':
            return AtomExpr.generateElement(15);

        case 'S':
            return AtomExpr.generateAromElem(16, IFALSE);

        case 'a':

            if (!STRICT)
            {
                return AtomExpr.buildAtomLeaf(AtomExpr.AL_AROM, 1);
            }
            else
            {
                break;
            }

        case 'c':
            return AtomExpr.generateAromElem(6, ITRUE);

        case 'n':
            return AtomExpr.generateAromElem(7, ITRUE);

        case 'o':
            return AtomExpr.generateAromElem(8, ITRUE);

        case 'p':
            return AtomExpr.generateAromElem(15, ITRUE);

        case 's':
            return AtomExpr.generateAromElem(16, ITRUE);
        }

        lexPtrIndex++;

        return null;
    }

    /*==============================   */
    /*  SMARTS Component Traversal     */
    /*==============================   */

    /**
     *  Description of the Method
     *
     * @param  pat  Description of the Parameter
     * @param  i    Description of the Parameter
     */
    public static void traverseSMARTS(Pattern pat, int i)
    {
        int j;
        int k;

        pat.atom[i].visit = ITRUE;

        for (j = 0; j < pat.bcount; j++)
        {
            if (pat.bond[j].visit == -1)
            {
                if (pat.bond[j].src == i)
                {
                    pat.bond[j].visit = i;
                    k = pat.bond[j].dst;

                    if (pat.atom[k].visit != 0)
                    {
                        traverseSMARTS(pat, k);
                    }
                }
                else if (pat.bond[j].dst == i)
                {
                    pat.bond[j].visit = i;
                    k = pat.bond[j].src;

                    if (pat.atom[k].visit != 0)
                    {
                        traverseSMARTS(pat, k);
                    }
                }
            }
        }
    }

    //**********************************
    //********Pattern Matching**********
    //**********************************

    /**
     * @param  ttab  {@link java.util.Vector} of <tt>boolean[1]</tt> -{@link java.util.Vector}
     * @param  pat   Description of the Parameter
     * @param  mol   Description of the Parameter
     */
    public void setupAtomMatchTable(Vector ttab, Pattern pat, JOEMol mol)
    {
        int i;

        ttab.setSize(pat.acount);

        for (i = 0; i < pat.acount; i++)
        {
            ((Vector) ttab.get(i)).setSize(mol.numAtoms() + 1);
        }

        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();

        for (i = 0; i < pat.acount; i++)
        {
            ait.reset();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                if (evalAtomExpr(pat.atom[0].expr, atom))
                {
                    ((boolean[]) ((Vector) ttab.get(i)).get(atom.getIdx()))[0] = true;
                }
            }
        }
    }

    /**
     * @param  vlex  {@link java.util.Vector} of type <tt>StringString</tt>
     * @param  s     Description of the Parameter
     * @return       Description of the Return Value
     */
    public String smartsLexReplace(String s, Vector vlex)
    {
        int j;
        int pos;
        String token;
        String repstr;
        StringString ssTmp;

        for (pos = s.indexOf("$", 0); pos < s.length();
                pos = s.indexOf("$", pos))
        {
            //for (pos = 0,pos = s.find("$",pos);pos != string::npos;pos = s.find("$",pos))
            pos++;

            for (j = pos; j < s.length(); j++)
            {
                if (!Character.isUnicodeIdentifierStart(s.charAt(j)) &&
                        !Character.isDigit(s.charAt(j)) &&
                        (s.charAt(j) != '_'))
                {
                    break;
                }
            }

            if (pos == j)
            {
                continue;
            }

            token = s.substring(pos, j - pos);

            char[] ca = s.toCharArray();

            for (int i = 0; i < vlex.size(); i++)
            {
                ssTmp = (StringString) vlex.get(i);

                if (token.equals(ssTmp.s1))
                {
                    repstr = new String("(" + ssTmp.s1 + ")");
                    System.arraycopy(repstr.toCharArray(), pos, ca, 0, j - pos);

                    //newString = repstr.substring(pos, j-pos);
                    //s.replace(pos,j-pos,repstr);
                    j = 0;
                }
            }

            pos = j;

            //return newString;
            return new String(ca);
        }

        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  ptr       Description of the Parameter
     * @param  ptrIndex  Description of the Parameter
     */
    void fatalAllocationError(char[] ptr, int ptrIndex)
    {
        char[] c = new char[theEnd - ptrIndex + 1];
        System.arraycopy(ptr, ptrIndex, c, 0, theEnd - ptrIndex + 1);
        logger.error("Error: Unable to allocate " + (new String(c)) + "!\n");
        System.exit(1);
    }

    /*============================   */
    /*  SMARTS String Generation     */
    /*============================   */

    /**
     *  Description of the Method
     *
     * @param  pat   Description of the Parameter
     * @param  part  Description of the Parameter
     * @return       Description of the Return Value
     */
    static int findSMARTSRoot(Pattern pat, int part)
    {
        int i;

        for (i = 0; i < pat.acount; i++)
        {
            if (pat.atom[i].part == part)
            {
                if (pat.atom[i].visit == 0)
                {
                    return i;
                }
            }
        }

        return -1;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
