///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomSpec.java,v $
//  Purpose:  Atom specification of a SMARTS bond expression.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:25 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts;

import joelib.smarts.atomexpr.AtomExpr;


/**
 * Atom specification of a SMARTS bond expression.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:25 $
 */
public class AtomSpec implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     * Representation of the SMARTS atom expression.
     */
    public AtomExpr expr;

    /**
     * Chiral flag.
     */
    public int chiral;

    /**
     * Part number of this atom expression in the SMARTS pattern.
     */
    public int part;

    /**
     * Vector binding.
     * Used for molecule modifications in <tt>JOEChemTransformation</tt>.
     */
    public int vb;

    /**
     * Visited flag.
     */
    public int visit;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the AtomSpec object
     */
    public AtomSpec()
    {
        expr = new AtomExpr();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
