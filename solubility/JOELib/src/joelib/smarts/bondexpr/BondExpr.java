///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BondExpr.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:43:26 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts.bondexpr;

import cformat.PrintfStream;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.apache.log4j.Category;


/**
 * Bond expression for SMARTS.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:43:26 $
 */
public class BondExpr implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.smarts.bonexpr.BondExpr");

    /**
     *  Description of the Field
     */
    public final static boolean FOO = false;

    /**
     *  Description of the Field
     */
    public final static boolean ORIG = false;

    //public static final boolean UNUSED    = false;

    /**
     *  Description of the Field
     */
    public final static int BE_LEAF = 0x01;

    /**
     *  Description of the Field
     */
    public final static int BE_ANDHI = 0x02;

    /**
     *  Description of the Field
     */
    public final static int BE_ANDLO = 0x03;

    /**
     *  Description of the Field
     */
    public final static int BE_NOT = 0x04;

    /**
     *  Description of the Field
     */
    public final static int BE_OR = 0x05;

    /**
     *  Description of the Field
     */
    public final static int BL_CONST = 0x01;

    /**
     *  Description of the Field
     */
    public final static int BL_TYPE = 0x02;

    /**
     *  Description of the Field
     */
    public final static int BT_SINGLE = 0x01;

    /**
     *  Description of the Field
     */
    public final static int BT_DOUBLE = 0x02;

    /**
     *  Description of the Field
     */
    public final static int BT_TRIPLE = 0x03;

    /**
     *  Description of the Field
     */
    public final static int BT_AROM = 0x04;

    /**
     *  Description of the Field
     */
    public final static int BT_UP = 0x05;

    /**
     *  Description of the Field
     */
    public final static int BT_DOWN = 0x06;

    /**
     *  Description of the Field
     */
    public final static int BT_UPUNSPEC = 0x07;

    /**
     *  Description of the Field
     */
    public final static int BT_DOWNUNSPEC = 0x08;

    /**
     *  Description of the Field
     */
    public final static int BT_RING = 0x09;

    /*---------------------
     *  BondExpr Bit Sets
     *---------------------  */

    /**
     *  Description of the Field
     */
    public final static int BF_NONRINGUNSPEC = 0x0001;

    /**
     *  Description of the Field
     */
    public final static int BF_NONRINGDOWN = 0x0002;

    /**
     *  Description of the Field
     */
    public final static int BF_NONRINGUP = 0x0004;

    /**
     *  Description of the Field
     */
    public final static int BF_NONRINGDOUBLE = 0x0008;

    /**
     *  Description of the Field
     */
    public final static int BF_NONRINGTRIPLE = 0x0010;

    /**
     *  Description of the Field
     */
    public final static int BF_RINGUNSPEC = 0x0020;

    /**
     *  Description of the Field
     */
    public final static int BF_RINGDOWN = 0x0040;

    /**
     *  Description of the Field
     */
    public final static int BF_RINGUP = 0x0080;

    /**
     *  Description of the Field
     */
    public final static int BF_RINGAROM = 0x0100;

    /**
     *  Description of the Field
     */
    public final static int BF_RINGDOUBLE = 0x0200;

    /**
     *  Description of the Field
     */
    public final static int BF_RINGTRIPLE = 0x0400;

    /**
     *  Description of the Field
     */
    public final static int BS_ALL = 0x07FF;

    /**
     *  Description of the Field
     */
    public final static int BS_SINGLE = 0x00E7;

    /**
     *  Description of the Field
     */
    public final static int BS_DOUBLE = 0x0208;

    /**
     *  Description of the Field
     */
    public final static int BS_TRIPLE = 0x0410;

    /**
     *  Description of the Field
     */
    public final static int BS_AROM = 0x0100;

    /**
     *  Description of the Field
     */
    public final static int BS_UP = 0x0084;

    /**
     *  Description of the Field
     */
    public final static int BS_DOWN = 0x0042;

    /**
     *  Description of the Field
     */
    public final static int BS_UPUNSPEC = 0x00A5;

    /**
     *  Description of the Field
     */
    public final static int BS_DOWNUNSPEC = 0x0063;

    /**
     *  Description of the Field
     */
    public final static int BS_RING = 0x07E0;

    /**
     *  Description of the Field
     */
    public final static int BS_DEFAULT = 0x01E7;

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public int type;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the BondExpr object
     */
    public BondExpr()
    {
    }

    /**
     *  Constructor for the BondExpr object
     *
     * @param  dataType  Description of the Parameter
     */
    public BondExpr(int _type)
    {
        type = _type;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the bondExprIndex attribute of the ParseSmart class
     *
     * @param  expr  Description of the Parameter
     * @return       The bondExprIndex value
     */
    public static int getBondExprIndex(BondExpr expr)
    {
        int lft;
        int rgt;
        int arg;
        BinBondExpr bBE = null;
        MonBondExpr mBE = null;

        switch (expr.type)
        {
        case (BE_LEAF):
            return getBondLeafIndex(expr);

        case (BE_NOT):
            mBE = (MonBondExpr) expr;
            arg = getBondExprIndex(mBE.arg);

            return (arg ^ BS_ALL);

        case (BE_ANDHI):
        case (BE_ANDLO):
            bBE = (BinBondExpr) expr;
            lft = getBondExprIndex(bBE.lft);
            rgt = getBondExprIndex(bBE.rgt);

            return (lft & rgt);

        case (BE_OR):
            bBE = (BinBondExpr) expr;
            lft = getBondExprIndex(bBE.lft);
            rgt = getBondExprIndex(bBE.rgt);

            return (lft | rgt);
        }

        /* Avoid Compiler Warning   */
        return 0;
    }

    /*==============================  */
    /*  Canonical Bond Expressions    */
    /*==============================  */

    /**
     *  Gets the bondLeafIndex attribute of the ParseSmart class
     *
     * @param  expr  Description of the Parameter
     * @return       The bondLeafIndex value
     */
    public static int getBondLeafIndex(BondExpr expr)
    {
        LeafBondExpr lBE = (LeafBondExpr) expr;

        if (lBE.prop == BL_CONST)
        {
            if (lBE.value != 0)
            {
                return (BS_ALL);
            }
            else
            {
                return (0);
            }
        }
        else
        {
            /* expr.leaf.prop == BL_TYPE   */
            switch (lBE.value)
            {
            case (BT_SINGLE):
                return (BS_SINGLE);

            case (BT_DOUBLE):
                return (BS_DOUBLE);

            case (BT_TRIPLE):
                return (BS_TRIPLE);

            case (BT_AROM):
                return (BS_AROM);

            case (BT_UP):
                return (BS_UP);

            case (BT_DOWN):
                return (BS_DOWN);

            case (BT_UPUNSPEC):
                return (BS_UPUNSPEC);

            case (BT_DOWNUNSPEC):
                return (BS_DOWNUNSPEC);

            case (BT_RING):
                return (BS_RING);
            }
        }

        return 0;
    }

    //public static BondExpr allocBondExpr( )
    //{
    //    BondExpr result = new BondExpr();
    //    //result = (BondExpr)malloc(sizeof(BondExpr));
    //
    //	/*
    //    if( !FreeBEList )
    //    {   result = (BondExpr)malloc(sizeof(BondExpr)BONDEXPRPOOL);
    //        for( i=1; i<BONDEXPRPOOL; i++ )
    //        {   result.mon.arg = freeBEList;
    //            //MonAtomExpr mAE=(MonAtomExpr)expr;
    //            freeBEList = result++;
    //        }
    //    } else
    //    {   result = freeBEList;
    //        freeBEList = result.mon.arg;
    //    }
    //	*/
    //
    //    return result;
    //}

    /**
     *  Description of the Method
     *
     * @param  type  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static BondExpr allocBondExpr(int type)
    {
        BondExpr result = null;

        // = new BondExpr();
        //    result = allocAtomExpr();
        //    result.type = expr.type;
        //    result = allocAtomExpr(expr.type);
        switch (type)
        {
        case (BE_ANDHI):
        case (BE_ANDLO):
        case (BE_OR):
            result = new BinBondExpr(type);

            break;

        case (BE_NOT):
            result = new MonBondExpr(type);

            break;

        case (BE_LEAF):
            result = new LeafBondExpr(type);

            break;
        }

        return result;
    }

    /**
     *  Description of the Method
     *
     * @param  op   Description of the Parameter
     * @param  lft  Description of the Parameter
     * @param  rgt  Description of the Parameter
     * @return      Description of the Return Value
     */
    public static BondExpr buildBondBin(int op, BondExpr lft, BondExpr rgt)
    {
        BondExpr result;

        result = allocBondExpr(op);

        BinBondExpr bBE = (BinBondExpr) result;

        //result.bin.type = op;
        bBE.lft = lft;
        bBE.rgt = rgt;

        return result;
    }

    /**
     *  Description of the Method
     *
     * @param  prop  Description of the Parameter
     * @param  val   Description of the Parameter
     * @return       Description of the Return Value
     */
    public static BondExpr buildBondLeaf(int prop, int val)
    {
        BondExpr result;

        result = allocBondExpr(BE_LEAF);

        LeafBondExpr lBE = (LeafBondExpr) result;

        //result.leaf.type = BE_LEAF;
        lBE.prop = prop;
        lBE.value = val;

        return lBE;
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static BondExpr buildBondNot(BondExpr expr)
    {
        BondExpr result;

        result = allocBondExpr(BE_NOT);

        MonBondExpr mBE = (MonBondExpr) result;

        //result.mon.type = BE_NOT;
        mBE.arg = expr;

        return result;
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static BondExpr canonicaliseBond(BondExpr expr)
    {
        if (FOO)
        {
            //            if (!ORIG)
            //            {
            //                //      int index;
            //                //
            //                //      index = getBondExprIndex(expr);
            //                //      freeBondExpr(expr);
            //                //
            //                //      lexPtr = canBondExpr[index];
            //                //      if( lexPtrIndex<=theEnd )
            //                //      {
            //                //        expr = parseBondExpr(0);
            //                //      }
            //                //      else expr = generateDefaultBond();
            //            }
            return transformBondExpr(expr);
        }
        else
        {
            //    throw Exception(ParseSmart.getName()+".canonicaliseBond not defined.");
            logger.error("ParseSmart.canonicaliseBond not defined.");
        }

        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static BondExpr copyBondExpr(BondExpr expr)
    {
        BondExpr result;

        //    result = allocAtomExpr();
        //    result.type = expr.type;
        result = allocBondExpr(expr.type);

        switch (expr.type)
        {
        case (BE_ANDHI):
        case (BE_ANDLO):
        case (BE_OR):

            BinBondExpr bBE = (BinBondExpr) expr;
            BinBondExpr binResult = (BinBondExpr) result;
            binResult.lft = copyBondExpr(bBE.lft);
            binResult.rgt = copyBondExpr(bBE.rgt);

            break;

        case (BE_NOT):

            MonBondExpr mBE = (MonBondExpr) expr;
            MonBondExpr monResult = (MonBondExpr) result;
            monResult.arg = copyBondExpr(mBE.arg);

            break;

        case (BE_LEAF):

            LeafBondExpr lBE = (LeafBondExpr) expr;
            LeafBondExpr leafResult = (LeafBondExpr) result;
            leafResult.prop = lBE.prop;
            leafResult.value = lBE.value;

            break;
        }

        return result;
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @param  os    Description of the Parameter
     */
    public static void displayBondExpr(BondExpr expr, OutputStream os)
    {
        PrintfStream fp;

        if (os instanceof PrintfStream)
        {
            fp = (PrintfStream) os;
        }
        else
        {
            fp = new PrintfStream(os);
        }

        BinBondExpr bBE = null;

        switch (expr.type)
        {
        case (BE_LEAF):
            displayBondLeaf(expr, fp);

            break;

        case (BE_NOT):

            MonBondExpr mBE = (MonBondExpr) expr;
            fp.print('!');
            displayBondExpr(mBE.arg, fp);

            break;

        case (BE_ANDHI):
            bBE = (BinBondExpr) expr;
            displayBondExpr(bBE.lft, fp);

            /* fp.print('&');   */
            displayBondExpr(bBE.rgt, fp);

            break;

        case (BE_ANDLO):
            bBE = (BinBondExpr) expr;
            displayBondExpr(bBE.lft, fp);
            fp.print(';');
            displayBondExpr(bBE.rgt, fp);

            break;

        case (BE_OR):
            bBE = (BinBondExpr) expr;
            displayBondExpr(bBE.lft, fp);
            fp.print(',');
            displayBondExpr(bBE.rgt, fp);

            break;
        }
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @param  os    Description of the Parameter
     */
    public static void displayBondLeaf(BondExpr expr, OutputStream os)
    {
        PrintfStream fp;

        if (os instanceof PrintfStream)
        {
            fp = (PrintfStream) os;
        }
        else
        {
            fp = new PrintfStream(os);
        }

        LeafBondExpr lBE = (LeafBondExpr) expr;

        if (lBE.prop == BL_CONST)
        {
            if (lBE.value != 0)
            {
                fp.print("!~");
            }
            else
            {
                fp.print('~');
            }
        }
        else
        {
            /* expr.leaf.prop == BL_TYPE   */
            switch (lBE.value)
            {
            case (BT_SINGLE):
                fp.print('-');

                break;

            case (BT_DOUBLE):
                fp.print('=');

                break;

            case (BT_TRIPLE):
                fp.print('#');

                break;

            case (BT_AROM):
                fp.print(':');

                break;

            case (BT_UP):
                fp.print('\\');

                break;

            case (BT_DOWN):
                fp.print('/');

                break;

            case (BT_UPUNSPEC):
                fp.print("\\?");

                break;

            case (BT_DOWNUNSPEC):
                fp.print("/?");

                break;

            case (BT_RING):
                fp.print('@');

                break;
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @param  os    Description of the Parameter
     */
    public static void displaySMARTSBond(BondExpr expr, OutputStream os)
    {
        PrintfStream fp;

        if (os instanceof PrintfStream)
        {
            fp = (PrintfStream) os;
        }
        else
        {
            fp = new PrintfStream(os);
        }

        BondExpr lft;
        BondExpr rgt;

        /* DisplayCanBondExpr(expr);   */
        /* Test for default bond!   */
        if (expr.type == BE_OR)
        {
            BinBondExpr bBE = (BinBondExpr) expr;
            lft = bBE.lft;
            rgt = bBE.rgt;

            if ((lft.type == BE_LEAF) && (rgt.type == BE_LEAF))
            {
                LeafBondExpr lBELft = (LeafBondExpr) lft;
                LeafBondExpr lBERgt = (LeafBondExpr) rgt;

                if ((lBELft.prop == BL_TYPE) && (lBERgt.prop == BL_TYPE))
                {
                    if ((lBELft.value == BT_SINGLE) &&
                            (lBERgt.value == BT_AROM))
                    {
                        return;
                    }

                    if ((lBERgt.value == BT_SINGLE) &&
                            (lBELft.value == BT_AROM))
                    {
                        return;
                    }
                }
            }
        }

        displayBondExpr(expr, fp);
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @param  os    Description of the Parameter
     */
    public static void dumpBondExpr(BondExpr expr, OutputStream os)
    {
        PrintfStream fp;

        if (os instanceof PrintfStream)
        {
            fp = (PrintfStream) os;
        }
        else
        {
            fp = new PrintfStream(os);
        }

        BinBondExpr bBE = null;

        switch (expr.type)
        {
        case (BE_LEAF):
            dumpBondLeaf(expr, fp);

            break;

        case (BE_NOT):

            MonBondExpr mBE = (MonBondExpr) expr;
            fp.print("NOT(");
            dumpBondExpr(mBE.arg, fp);
            fp.print(')');

            break;

        case (BE_ANDHI):
            bBE = (BinBondExpr) expr;
            fp.print("ANDHI(");
            dumpBondExpr(bBE.lft, fp);
            fp.print(',');
            dumpBondExpr(bBE.rgt, fp);
            fp.print(')');

            break;

        case (BE_ANDLO):
            bBE = (BinBondExpr) expr;
            dumpBondExpr(bBE.lft, fp);
            fp.print("ANDLO");
            fp.print(',');
            dumpBondExpr(bBE.rgt, fp);
            fp.print(')');

            break;

        case (BE_OR):
            bBE = (BinBondExpr) expr;
            fp.print("OR(");
            dumpBondExpr(bBE.lft, fp);
            fp.print(',');
            dumpBondExpr(bBE.rgt, fp);
            fp.print(')');

            break;
        }
    }

    /**
    *  Description of the Method
    *
    * @param  expr  Description of the Parameter
    * @param  os    Description of the Parameter
    */
    public static void dumpBondLeaf(BondExpr expr, OutputStream os)
    {
        PrintfStream fp;

        if (os instanceof PrintfStream)
        {
            fp = (PrintfStream) os;
        }
        else
        {
            fp = new PrintfStream(os);
        }

        LeafBondExpr lBE = (LeafBondExpr) expr;

        if (lBE.prop == BL_CONST)
        {
            if (lBE.value != 0)
            {
                fp.print("!~");
            }
            else
            {
                fp.print('~');
            }
        }
        else
        {
            /* expr.leaf.prop == BL_TYPE   */
            switch (lBE.value)
            {
            case (BT_SINGLE):
                fp.print("SINGLE");

                break;

            case (BT_DOUBLE):
                fp.print("DOUBLE");

                break;

            case (BT_TRIPLE):
                fp.print("TRIPLE");

                break;

            case (BT_AROM):
                fp.print("AROM");

                break;

            case (BT_UP):
                fp.print("UP");

                break;

            case (BT_DOWN):
                fp.print("DOWN");

                break;

            case (BT_UPUNSPEC):
                fp.print("UNSPEC_UP");

                break;

            case (BT_DOWNUNSPEC):
                fp.print("UNSPEC_DOWN");

                break;

            case (BT_RING):
                fp.print("RING");

                break;
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     */
    public static void freeBondExpr(BondExpr expr)
    {
        if (expr != null)
        {
            switch (expr.type)
            {
            case (BE_ANDHI):
            case (BE_ANDLO):
            case (BE_OR):

                BinBondExpr bBE = (BinBondExpr) expr;
                freeBondExpr(bBE.lft);
                freeBondExpr(bBE.rgt);

                break;

            case (BE_NOT):

                MonBondExpr mBE = (MonBondExpr) expr;
                freeBondExpr(mBE.arg);

                break;
            }

            if (expr != null)
            {
                expr = null;
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static BondExpr generateDefaultBond()
    {
        BondExpr expr1;
        BondExpr expr2;

        expr1 = buildBondLeaf(BL_TYPE, BT_SINGLE);
        expr2 = buildBondLeaf(BL_TYPE, BT_AROM);

        return buildBondBin(BE_OR, expr1, expr2);
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static BondExpr notBondExpr(BondExpr expr)
    {
        BondExpr result;

        if (expr.type == BE_LEAF)
        {
            LeafBondExpr lBE = (LeafBondExpr) expr;

            if (lBE.prop == BL_CONST)
            {
                lBE.value = ((lBE.value == 0) ? 1 : 0);

                return expr;
            }
        }
        else if (expr.type == BE_NOT)
        {
            MonBondExpr mBE = (MonBondExpr) expr;
            result = mBE.arg;
            mBE.arg = null;
            freeBondExpr(expr);

            return result;
        }

        return buildBondNot(expr);
    }

    /**
     *  Description of the Method
     *
     * @param  expr  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static BondExpr transformBondExpr(BondExpr expr)
    {
        BondExpr lft;
        BondExpr rgt;
        BondExpr arg;

        if (expr.type == BE_LEAF)
        {
            return expr;
        }
        else if (expr.type == BE_NOT)
        {
            MonBondExpr mBE = (MonBondExpr) expr;
            arg = mBE.arg;
            arg = transformBondExpr(arg);
            mBE.arg = null;
            freeBondExpr(expr);

            return notBondExpr(arg);
        }
        else if (expr.type == BE_ANDHI)
        {
            BinBondExpr bBE = (BinBondExpr) expr;
            lft = bBE.lft;
            rgt = bBE.rgt;
            lft = transformBondExpr(lft);
            rgt = transformBondExpr(rgt);
            bBE.lft = lft;
            bBE.rgt = rgt;

            return expr;
        }
        else if (expr.type == BE_ANDLO)
        {
            BinBondExpr bBE = (BinBondExpr) expr;
            lft = bBE.lft;
            rgt = bBE.rgt;
            lft = transformBondExpr(lft);
            rgt = transformBondExpr(rgt);
            bBE.lft = lft;
            bBE.rgt = rgt;

            return expr;
        }
        else if (expr.type == BE_OR)
        {
            BinBondExpr bBE = (BinBondExpr) expr;
            lft = bBE.lft;
            rgt = bBE.rgt;
            lft = transformBondExpr(lft);
            rgt = transformBondExpr(rgt);
            bBE.lft = lft;
            bBE.rgt = rgt;

            return expr;
        }

        return expr;
    }

    public String toString()
    {
        ByteArrayOutputStream bs = new ByteArrayOutputStream(1000);
        PrintStream ps = new PrintStream(bs);

        displayBondExpr(this, ps);

        return bs.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
