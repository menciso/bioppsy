///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: LeafBondExpr.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:43:26 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts.bondexpr;


/**
 * Leaf bond node for SMARTS.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:26 $
 */
public class LeafBondExpr extends BondExpr implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public int prop;

    /**
     *  Description of the Field
     */
    public int value;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the LeafBondExpr object
     */
    public LeafBondExpr()
    {
        super();
    }

    /**
     *  Constructor for the LeafBondExpr object
     *
     * @param  dataType  Description of the Parameter
     */
    public LeafBondExpr(int _type)
    {
        super(_type);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
