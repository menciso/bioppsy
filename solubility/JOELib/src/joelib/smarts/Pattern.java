///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Pattern.java,v $
//  Purpose:  SMARTS pattern representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/02/20 13:12:18 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smarts;


/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.apache.log4j.Category;

import joelib.smarts.atomexpr.AtomExpr;
import joelib.smarts.bondexpr.BondExpr;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================  */

/**
 * SMARTS pattern representation.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/02/20 13:12:18 $
 */
public class Pattern implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------  */

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.smarts.Pattern");

    /**
     *  Description of the Field
     */
    public final static int ATOMPOOL = 1;

    /**
     *  Description of the Field
     */
    public final static int BONDPOOL = 1;

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public AtomSpec[] atom;

    /**
     *  Description of the Field
     */
    public BondSpec[] bond;

    /**
     *  Description of the Field
     */
    public boolean ischiral;

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------  */

    /**
     *  Description of the Field
     */
    public int aalloc;

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------  */

    /**
     *  Description of the Field
     */
    public int acount;

    /**
     *  Description of the Field
     */
    public int balloc;

    /**
     *  Description of the Field
     */
    public int bcount;

    /**
     *  Description of the Field
     */
    public int parts;

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */
    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static Pattern allocPattern()
    {
        Pattern ptr = new Pattern();

        //ptr = (Pattern*)malloc(sizeof(Pattern));
        if (ptr == null)
        {
            logger.error("Can not allocate new Pattern.");
            System.exit(1);

            //            ParseSmart.fatalAllocationError((new String("pattern")).toCharArray(), 0);
        }

        ptr.atom = null;
        ptr.aalloc = 0;
        ptr.acount = 0;

        ptr.bond = null;
        ptr.balloc = 0;
        ptr.bcount = 0;

        ptr.parts = 1;

        return ptr;
    }

    /**
     *  Description of the Method
     *
     * @param  pat  Description of the Parameter
     * @return      Description of the Return Value
     */
    public static Pattern copyPattern(Pattern pat)
    {
        Pattern result;
        AtomExpr aexpr;
        BondExpr bexpr;
        int i;

        result = allocPattern();
        result.parts = pat.parts;

        for (i = 0; i < pat.acount; i++)
        {
            aexpr = AtomExpr.copyAtomExpr(pat.atom[i].expr);
            createAtom(result, aexpr, pat.atom[i].part);
        }

        for (i = 0; i < pat.bcount; i++)
        {
            bexpr = BondExpr.copyBondExpr(pat.bond[i].expr);
            createBond(result, bexpr, pat.bond[i].src, pat.bond[i].dst);
        }

        return result;
    }

    /**
     *  Description of the Method
     *
     * @param  pat   Description of the Parameter
     * @param  expr  Description of the Parameter
     * @param  part  Description of the Parameter
     * @return       Description of the Return Value
     */
    public static int createAtom(Pattern pat, AtomExpr expr, int part)
    {
        return createAtom(pat, expr, part, 0);
    }

    /**
     *  Description of the Method
     *
     * @param  pat   Description of the Parameter
     * @param  expr  Description of the Parameter
     * @param  part  Description of the Parameter
     * @param  vb    vector binding
     * @return       Description of the Return Value
     */
    public static int createAtom(Pattern pat, AtomExpr expr, int part, int vb)
    {
        if (pat == null)
        {
            logger.error("No Pattern defined in ParseSmart.creatAtom.");
        }

        int index;

        if (pat.acount == pat.aalloc)
        {
            int tmpSize = pat.aalloc;
            pat.aalloc += ATOMPOOL;

            //????????
            //    size = pat.aalloc*sizeof(AtomSpec);
            //    if( pat.atom!=null )
            //    {
            //      pat.atom = (AtomSpec)realloc(pat.atom,size);
            //    }
            //    else pat.atom = (AtomSpec)malloc(size);
            AtomSpec[] tmp = new AtomSpec[pat.aalloc];

            if (pat.atom != null)
            {
                System.arraycopy(pat.atom, 0, tmp, 0, tmpSize);
            }

            pat.atom = tmp;

            //    if( pat.atom==null ) fatalAllocationError("atom pool");
            if (pat.atom == null)
            {
                logger.error("Atom pool can't be generated.");
            }
        }

        index = pat.acount++;

        if (pat.atom[index] == null)
        {
            pat.atom[index] = new AtomSpec();
        }

        pat.atom[index].part = part;
        pat.atom[index].expr = expr;
        pat.atom[index].vb = vb;

        //vector binding
        return index;
    }

    /**
     *  Description of the Method
     *
     * @param  pat   Description of the Parameter
     * @param  expr  Description of the Parameter
     * @param  src   Description of the Parameter
     * @param  dst   Description of the Parameter
     * @return       Description of the Return Value
     */
    public static int createBond(Pattern pat, BondExpr expr, int src, int dst)
    {
        int index;

        if (pat.bcount == pat.balloc)
        {
            int tmpSize = pat.balloc;
            pat.balloc += BONDPOOL;

            //????????
            //    size = pat.balloc*sizeof(BondSpec);
            //    if( pat.bond!=null )
            //    {
            //      pat.bond = (BondSpec)realloc(pat.bond,size);
            //    }
            //    else pat.bond = (BondSpec)malloc(size);
            //
            //    if( pat.bond==null ) fatalAllocationError("bond pool");
            BondSpec[] tmp = new BondSpec[pat.balloc];

            if (pat.bond != null)
            {
                System.arraycopy(pat.bond, 0, tmp, 0, tmpSize);
            }

            pat.bond = tmp;

            if (pat.bond == null)
            {
                logger.error("Bond pool can't be generated.");
            }
        }

        index = pat.bcount++;

        if (pat.bond[index] == null)
        {
            pat.bond[index] = new BondSpec();
        }

        pat.bond[index].expr = expr;
        pat.bond[index].src = src;
        pat.bond[index].dst = dst;

        return index;
    }

    /**
     *  Description of the Method
     *
     * @param  pat  Description of the Parameter
     */
    public static void freePattern(Pattern pat)
    {
        int i;

        if (pat != null)
        {
            if (pat.aalloc != 0)
            {
                for (i = 0; i < pat.acount; i++)
                {
                    AtomExpr.freeAtomExpr(pat.atom[i].expr);
                }

                pat.atom = null;
            }

            if (pat.balloc != 0)
            {
                for (i = 0; i < pat.bcount; i++)
                {
                    BondExpr.freeBondExpr(pat.bond[i].expr);
                }

                pat.bond = null;
            }

            pat = null;
        }
    }

    public String toString()
    {
        return toString("");
    }

    public String toString(String startLineWith)
    {
        ByteArrayOutputStream bs = new ByteArrayOutputStream(2000);
        PrintStream ps = new PrintStream(bs);

        int bi = 0;

        if (atom != null)
        {
            // Atom expressions
            for (int i = 0; i < atom.length; i++)
            {
                ps.print(startLineWith);
                AtomExpr.dumpAtomExpr(atom[i].expr, ps);

                //            ps.print(" with part: ");
                //            ps.print(atom[i].part);
                ps.print(" vb: ");
                ps.print(atom[i].vb);
                ps.println();

                //          AtomExpr.displayAtomExpr(atom[i].expr, ps);
                if ((bond != null) && (bi < bond.length))
                {
                    // Bond expressions
                    //            BondExpr.displayBondExpr(bond[bi].expr, ps);
                    BondExpr.dumpBondExpr(bond[bi].expr, ps);
                    ps.print(" with grow: ");
                    ps.print(bond[bi].grow);
                    ps.print(" src: ");
                    ps.print(bond[bi].src);
                    ps.print(" dst: ");
                    ps.print(bond[bi].dst);

                    //            ps.print(" visit: ");
                    //            ps.print(bond[bi].visit);
                    ps.println();
                    bi++;
                }
                else
                {
                    ps.println();
                }
            }
        }

        return bs.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
