///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: APropertyBFS.java,v $
//  Purpose:  Atom pair descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.3 $
//            $Date: 2004/02/20 13:11:41 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo;

import wsi.ra.tool.*;

/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import java.util.*;

import org.apache.log4j.*;

import joelib.desc.*;
import joelib.desc.result.AtomDynamicResult;
import joelib.desc.types.AtomInConjEnvironment;

import joelib.molecule.*;
import joelib.molecule.types.*;

import joelib.util.*;
import joelib.util.iterator.*;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================    */

/**
 *  Breadh First Search.
 *
 * @author     wegnerj
 * @cite clr98complexity
 * @cite clr98bfs
 * @todo Enable real atom properties, not only conjugated atoms
 */
public class APropertyBFS implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    public final static String STARTING_ATOM = "STARTING_ATOM";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(STARTING_ATOM, "joelib.molecule.JOEAtom",
                "The start atom.", true),
        };
    public final static String DESC_KEY = "Atom_property_breadth_first_search";

    /*-------------------------------------------------------------------------*
     * public static member variables
     *-------------------------------------------------------------------------        */

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.algo.APropertyBFS");
    private final static int WHITE = 0;
    private final static int GRAY = 1;
    private final static int BLACK = 2;

    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private variables
     *-------------------------------------------------------------------------    */
    private DescriptorInfo descInfo;
    private JOEAtom startAtom;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------    */

    /**
     *  Constructor for the KierShape1 object
     */
    public APropertyBFS()
    {
        String representation = this.getClass().getName();
        int index = representation.lastIndexOf(".");
        String docs = "docs/algo/" + representation.substring(index + 1);

        descInfo = new DescriptorInfo(DESC_KEY,
                DescriptorInfo.TYPE_NO_COORDINATES, representation, docs,
                "joelib.algo.BFSInit", "joelib.algo.BFSResult");

        //
        //		descInfo = DescriptorHelper.generateDescInfo(DESC_KEY,
        //                this.getClass(),
        //                "topological",
        //                2,
        //                null,
        //                "joelib.algo.BFSResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------    */

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @param  _descInfo  The new descInfo value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     * @return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        if (startAtom == null)
        {
            startAtom = mol.getAtom(1);
        }

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        if (startAtom == null)
        {
            startAtom = mol.getAtom(1);
        }

        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol molOrig, DescResult descResult,
        Map properties) throws DescriptorException
    {
        if (molOrig.empty())
        {
            logger.error("Empty molecule '" + molOrig.getTitle() + "'.");

            return null;
        }

        String propertyName = AtomInConjEnvironment.DESC_KEY;

        //System.out.println("ATOMS:"+mol.numAtoms());
        //	    JOEMol                   mol       = (JOEMol) molOrig.clone(true, new String[]{propertyName});
        //    	mol.deleteHydrogens();
        //System.out.println("ATOMS-H:"+mol.numAtoms());
        //for (int i = 0; i < mol.numAtoms(); i++) {
        //	System.out.println("IDX("+i+"):"+mol.getAtom(i+1).getIdx());
        //}
        //	    for (int i = 0; i < mol.numAtoms(); i++) {
        //	System.out.println("IDX("+i+"):"+mol.getAtom(i+1).getIdx());
        //		if(mol.getAtom(i+1).isHydrogen())
        //		{
        //			System.out.println("Molecule contains hydrogens");
        //		}
        //		}
        BFSResult result = null;
        int atoms = molOrig.numAtoms();

        // check if the result type is correct
        if (!(descResult instanceof BFSResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                BFSResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (BFSResult) descResult;

            if (result.traverse == null)
            {
                result.traverse = new int[atoms];
                result.parent = new int[atoms];
            }
            else if (result.traverse.length != atoms)
            {
                result.traverse = null;
                result.traverse = new int[atoms];
                result.parent = null;
                result.parent = new int[atoms];
            }
        }

        // check if the init type is correct
        if (properties == null)
        {
            if (startAtom == null)
            {
                logger.error(descInfo.getName() +
                    " properties is not defined. Please define: " +
                    STARTING_ATOM);

                return null;
            }
        }
        else
        {
            //System.out.println("init");
            //			Object obj=null;
            //			obj.toString();
            if (!initialize(properties))
            {
                return null;
            }
        }

        DescResult tmpPropResult;
        tmpPropResult = DescriptorHelper.instance().descFromMol(molOrig,
                propertyName);

        AtomProperties atomProperties;

        AtomDynamicResult adr = (AtomDynamicResult) tmpPropResult;
        JOEMol mol = null;

        if (adr.length() < molOrig.numAtoms())
        {
            //logger.warn("Remove hydrogen atoms before calculating "+DESC_KEY);
            //return null;
            // delete hydrogen atoms
            logger.warn("Hydrogens were removed.");
            mol = (JOEMol) molOrig.clone(true, new String[]{propertyName});
            mol.deleteHydrogens();
            atoms = mol.numAtoms();

            if (result.traverse.length != atoms)
            {
                result.traverse = null;
                result.traverse = new int[atoms];
                result.parent = null;
                result.parent = new int[atoms];
            }

            if (startAtom.getIdx() > atoms)
            {
                logger.error("Start atom index is to big, now it's set to 1.");
                startAtom = mol.getAtom(1);
            }
        }
        else
        {
            mol = molOrig;
        }

        if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
        {
            atomProperties = (AtomProperties) tmpPropResult;
        }
        else
        {
            // should never happen
            logger.error("Property '" + propertyName +
                "' must be an atom type to calculate the " + DESC_KEY + ".");

            return null;
        }

        //logger.info("Get "+propertyName+" from "+mol.getTitle());
        //logger.info("Atom ("+mol.numAtoms()+") greater than maximum element at "+(adr.length()-1)+".");
        // create temporary arrays
        // position 0 will be unused, because the
        // lowest atom index will be 1 !
        int[] traverse = new int[atoms + 1];
        int[] parent = new int[atoms + 1];
        int[] color = new int[atoms + 1];
        Deque fifo = new Deque();

        // all atoms are not visited
        for (int i = 0; i < (atoms + 1); i++)
        {
            color[startAtom.getIdx()] = WHITE;
            traverse[i] = Integer.MAX_VALUE;
            parent[i] = -1;
        }

        // start atom for the first time visited
        color[startAtom.getIdx()] = GRAY;
        traverse[startAtom.getIdx()] = 0;

        // add start node to the FIFO
        fifo.pushBack(mol.getAtom(startAtom.getIdx()));

        NbrAtomIterator nbrIterator;

        // repeat until the unfinished stack contains elements
        int tmp = 0;

        while (!fifo.isEmpty())
        {
            JOEAtom atom = (JOEAtom) ((DequeNode) fifo.getFront()).key;
            nbrIterator = atom.nbrAtomIterator();

            JOEAtom nbr;

            while (nbrIterator.hasNext())
            {
                nbr = nbrIterator.nextNbrAtom();

                //System.out.println("ABFS:Atom ("+atom.getIdx()+") neighbour index: "+nbr.getIdx());
                if (nbr.getIdx() > adr.length())
                {
                    logger.error("Atom (" + mol.numAtoms() + ") index " +
                        (nbr.getIdx() - 1) +
                        " greater than maximum element at " +
                        (adr.length() - 1) + ".");

                    return null;
                }

                //System.out.println("NBR("+adr.length()+"):"+nbr.getIdx());
                // visit undeveloped neighbour node
                try
                {
                    tmp = ((AtomProperties) atomProperties).getIntValue(nbr.getIdx());
                }
                 catch (Exception ex)
                {
                    // zip Exception ???
                    // i don't get it ... this should NEVER be happen
                    ex.printStackTrace();
                }

                if ((color[nbr.getIdx()] == WHITE) && (tmp == 1))
                {
                    // mark as developed
                    color[nbr.getIdx()] = GRAY;

                    // set time number node has been traversed
                    traverse[nbr.getIdx()] = traverse[atom.getIdx()] + 1;

                    // set previous visited node
                    parent[nbr.getIdx()] = atom.getIdx();

                    // add this unfinished node to the FIFO
                    fifo.pushBack(nbr);
                }
            }

            // mark node as finished and remove it from the stack
            fifo.popFront();
            color[atom.getIdx()] = BLACK;
        }

        // create temporary array contents to
        // result arrays
        System.arraycopy(traverse, 1, result.traverse, 0, atoms);
        System.arraycopy(parent, 1, result.parent, 0, atoms);

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
        startAtom = null;
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        JOEAtom atom = (JOEAtom) JOEPropertyHelper.getProperty(this,
                STARTING_ATOM, properties);

        if (atom == null)
        {
            startAtom = null;
        }
        else
        {
            startAtom = atom;
        }

        return true;
    }

    /**
    * Test the implementation of this descriptor.
    *
    * @return <tt>true</tt> if the implementation is correct
    */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
