///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GCPredictor.java,v $
//  Purpose:  Value prediction based on a group contribution model.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Stephen Jelfs, Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/02/20 13:11:41 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo.contribution;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.Iterator;

import org.apache.log4j.Category;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Value prediction based on a group contribution model.
 *
 * @author  Stephen Jelfs
 * @author  wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/02/20 13:11:41 $
 * @cite ers00
 * @cite wc99
 */
public class GCPredictor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.algo.contribution.GCPredictor");

    //~ Methods ////////////////////////////////////////////////////////////////

    // predict molecular properties
    public static double predict(GroupContributions gc, JOEMol molecule)
    {
        if (molecule.empty())
        {
            logger.warn("Empty molecule '" + molecule.getTitle() +
                "'. Value set to NaN.");

            return Double.NaN;
        }

        // atom contributions
        double[] atomValues = new double[molecule.numAtoms()];

        // set atom contributions
        for (int i = 0; i < gc.atomSmarts.size(); ++i)
        {
            // get smarts
            JOESmartsPattern smarts = (JOESmartsPattern) gc.atomSmarts.get(i);

            // find atom matches
            smarts.match(molecule);

            // iterate through matches
            Iterator matches = smarts.getMapList().iterator();

            while (matches.hasNext())
            {
                // get matched atom IDs
                int[] atoms = (int[]) matches.next();

                // store value of matched atom
                atomValues[atoms[0] - 1] = ((Double) gc.atomContributions.get(i)).doubleValue();
            }
        }

        // hydrogen contributions
        double[] hydrogenValues = new double[molecule.numAtoms()];

        // set hydrogen contributions
        for (int i = 0; i < gc.hydrogenSmarts.size(); ++i)
        {
            // get smarts
            JOESmartsPattern smarts = (JOESmartsPattern) gc.hydrogenSmarts.get(i);

            // find atom matches
            smarts.match(molecule);

            // iterate through matches
            Iterator matches = smarts.getMapList().iterator();

            while (matches.hasNext())
            {
                // get matched atom IDs
                int[] atoms = (int[]) matches.next();

                // store value of matched atom
                hydrogenValues[atoms[0] - 1] = ((Double) gc.hydrogenContributions.get(i)).doubleValue();
            }
        }

        // total atomic and hydrogen contributions
        double total = 0.0;

        for (int i = 1; i <= molecule.numAtoms(); ++i)
        {
            // get atom
            JOEAtom atom = molecule.getAtom(i);

            // skip hydrogens
            if (atom.isHydrogen())
            {
                continue;
            }

            // get total hydrogen count
            int hydrogenCount = atom.implicitHydrogenCount() +
                atom.explicitHydrogenCount();

            // add atom contribution to total
            total += atomValues[i - 1];

            // add hydrogen contribution to total
            total += (hydrogenValues[i - 1] * hydrogenCount);
        }

        //        for(int i=0; i<atomValues.length; ++i) {
        //           System.out.println("atom "+i+": "+atomValues[i]);
        //        }
        //        for(int i=0; i<hydrogenValues.length; ++i) {
        //           System.out.println("hydrogen "+i+": "+atomValues[i]);
        //        }
        // return total
        return total;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
