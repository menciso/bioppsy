///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GroupContributions.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.3 $
//            $Date: 2003/08/19 13:11:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo.contribution;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.ArrayList;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Group contribution informations.
 *
 * @author     wegnerj
 * @author  Stephen Jelfs
 * @license GPL
 * @cvsversion    $Revision: 1.3 $, $Date: 2003/08/19 13:11:24 $
 */
public class GroupContributions
{
    //~ Instance fields ////////////////////////////////////////////////////////

    // atom contributions
    public ArrayList atomContributions = new ArrayList();

    // atom SMARTS patterns
    public ArrayList atomSmarts = new ArrayList();
    public ArrayList hydrogenContributions = new ArrayList();
    public ArrayList hydrogenSmarts = new ArrayList();

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */
    String model;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the StringString object
     *
     * @param  _s1  Description of the Parameter
     * @param  _s2  Description of the Parameter
     */
    public GroupContributions(String _model)
    {
        model = _model;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
