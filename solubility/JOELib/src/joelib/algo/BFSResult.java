///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BFSResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/02/20 13:11:41 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo;


/*==========================================================================*
 * IMPORTS
 *==========================================================================   */
import java.io.LineNumberReader;
import java.io.StringReader;

import java.util.Vector;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;

import joelib.util.ArrayHelper;
import joelib.util.JHM;
import joelib.util.LineArrayHelper;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================   */

/**
 * Result of a BFS.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/02/20 13:11:41 $
 */
public class BFSResult extends JOEPairData implements DescResult, Cloneable,
    java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private final static String basicFormat = "startAtomIndex\n" +
        "nTraverse<e0,...e(nTraverse-1)>nParent<e0,...e(nParent-1)>\n" +
        "with nTraverse, e0,...,e(nTraverse-1) of type 32-bit integer" +
        "with traverse numbers" +
        "with nParent, e0,...,e(nParent-1) of type 32-bit integer" +
        "with parent of traverse as atom idx, -1 if start atom or undiscovered";
    private final static String lineFormat = "startAtomIndex\n" +
        "nTraverse\n" + "e0\n" + "...\n" + "e(nTraverse-1)>\n" + "nParent\n" +
        "e0\n" + "...\n" + "e(nParent-1)>\n" + "<empty line>\n" +
        "with nTraverse, e0,...,e(nTraverse-1) of type 32-bit integer" +
        "with traverse numbers" +
        "with nParent, e0,...,e(nParent-1) of type 32-bit integer" +
        "with parent of traverse as atom idx, -1 if start atom or undiscovered";

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public int[] parent;

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------   */

    /**
     *  Description of the Field
     */
    public int[] traverse;

    /**
     * Description of the Field
     */
    public int startAtomIndex;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------   */

    /**
     *  Constructor for the BFSResult object
     */
    public BFSResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public BFSResult clone(BFSResult _target)
    {
        _target.startAtomIndex = startAtomIndex;
        System.arraycopy(_target.traverse, 0, this.traverse, 0, traverse.length);
        System.arraycopy(_target.parent, 0, this.parent, 0, parent.length);

        return _target;
    }

    public Object clone()
    {
        BFSResult newObj = new BFSResult();

        newObj.traverse = new int[this.traverse.length];
        newObj.parent = new int[this.parent.length];

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            return lineFormat;
        }
        else
        {
            return basicFormat;
        }
    }

    /**
     *  Description of the Method
     *
     * @param pairData  Description of the Parameter
     * @param ioType    Description of the Parameter
     * @return          Description of the Return Value
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param sValue  Description of the Parameter
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public boolean fromString(IOType ioType, String sValue)
    {
        StringReader sr = new StringReader(sValue);
        LineNumberReader lnr = new LineNumberReader(sr);

        // get start atom index
        try
        {
            startAtomIndex = Integer.parseInt(lnr.readLine());
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            return false;
        }

        String tmp = null;

        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            int index = sValue.indexOf("\n");
            tmp = sValue.substring(index).trim();

            Vector iaVector;
            iaVector = LineArrayHelper.instance().intArrayFromString(tmp);
            traverse = (int[]) iaVector.get(0);
            parent = (int[]) iaVector.get(1);
        }
        else
        {
            int index = sValue.indexOf("\n");
            tmp = sValue.substring(index).trim();

            Vector iaVector;
            iaVector = ArrayHelper.instance().intArrayFromString(tmp);
            traverse = (int[]) iaVector.get(0);
            parent = (int[]) iaVector.get(1);
        }

        return true;
    }

    /**
     *  Constructor for the BFSResult object
     *
     * @param _descName  Description of the Parameter
     * @return           Description of the Return Value
     */

    //    public BFSResult(DescriptorInfo descInfo)
    //    {
    //        dataType = JOEDataType.JOE_PAIR_DATA;
    //        this.setAttribute(descInfo.getName());
    //
    //        this._value = this;
    //
    //        dataType = JOEDataType.getNewDataType( descInfo.getName() );
    //    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------   */
    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        StringBuffer sb = new StringBuffer();

        // start atom index
        sb.append(startAtomIndex);
        sb.append(JHM.eol);

        // bfs result arrays
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            LineArrayHelper.instance().toString(sb, traverse).toString();
            sb.append(JHM.eol);
            LineArrayHelper.instance().toString(sb, parent).toString();
        }
        else
        {
            ArrayHelper.instance().toString(sb, traverse).toString();
            sb.append(JHM.eol);
            ArrayHelper.instance().toString(sb, parent).toString();
        }

        // could be a nice JUnit test routine !!!
        //    Vector vvv = LineArrayHelper.instance().intArrayFromString(sb.toString());
        //    System.out.println("Generated/Parsed:");
        //    StringBuffer sb2 = new StringBuffer();
        //    ArrayHelper.instance().toString(sb2, (int[])vvv.get(0)).toString();
        //    ArrayHelper.instance().toString(sb2, (int[])vvv.get(1)).toString();
        //    System.out.println(""+sb2.toString());
        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
