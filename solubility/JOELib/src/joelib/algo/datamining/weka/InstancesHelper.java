///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: InstancesHelper.java,v $
//  Purpose:  Helper class to create Weka data instances.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:42:57 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo.datamining.weka;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.WekaException;

import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEGenericData;
import joelib.data.JOEPairData;

import joelib.desc.NativeValue;

import joelib.molecule.JOEMol;
import joelib.molecule.JOEMolVector;

import joelib.process.types.DescBinning;

import joelib.util.JOEHelper;


/**
 * Helper class to create Weka data instances.
 *
 * For using Weka you should modify the constructors in
 * weka.core.Attributes from 'protected' to 'public' and use the
 * recompiled Weka sources.
 *
 * @author    wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:42:57 $
 */
public class InstancesHelper
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "jcompchem.joelib.algo.weka.InstancesHelper");

    //~ Constructors ///////////////////////////////////////////////////////////

    public InstancesHelper()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public static Instances createMolInstances(JOEMolVector molecules,
        String[] attributes, int[] attributeTypes) throws WekaException
    {
        // load descriptor binning
        DescBinning binning = DescBinning.getDescBinning(molecules);

        int length = molecules.getSize();

        if (attributes.length != attributeTypes.length)
        {
            throw new WekaException(
                "Different number of attributes and attribute types.");

            //return null;
        }

        Enumeration enum = binning.getDescriptors();
        FastVector attributesV = new FastVector(binning.numberOfDescriptors());
        JOEMol mol;
        JOEGenericData genericData;
        JOEPairData pairData;

        for (int i = 0; i < attributes.length; i++)
        {
            if (attributeTypes[i] == Attribute.NUMERIC)
            {
                // numeric
                attributesV.addElement(new Attribute(
                        (String) enum.nextElement(), attributesV.size()));
            }
            else if (attributeTypes[i] == Attribute.NOMINAL)
            {
                // nominal
                // create a list with all nominal values
                Hashtable hashed = new Hashtable();

                for (int j = 0; j < length; j++)
                {
                    mol = molecules.getMol(j);

                    // get unparsed data
                    genericData = mol.getData(attributes[i], false);

                    if (genericData != null)
                    {
                        if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                        {
                            pairData = (JOEPairData) genericData;

                            if (pairData.getValue() instanceof String)
                            {
                                hashed.put(pairData.getValue(), "");
                            }
                            else
                            {
                                hashed.put(pairData.toString(), "");
                            }
                        }
                    }
                }

                // store list of nominal values in the Weka data structure
                FastVector attributeValues = new FastVector(hashed.size());
                String tmp;

                for (Enumeration e = hashed.keys(); e.hasMoreElements();)
                {
                    tmp = (String) e.nextElement();
                    attributeValues.addElement(tmp);

                    //System.out.println("NOMINAL " + tmp);
                }

                attributesV.addElement(new Attribute(attributes[i],
                        attributeValues, attributesV.size()));
            }
        }

        int size = attributesV.size();
        Attribute attribute;

        // create molecule instances
        Instances instances = new Instances("MoleculeInstances", attributesV,
                attributesV.size());

        // iterate over all instances (to generate them)
        double[] instance;

        for (int i = 0; i < length; i++)
        {
            mol = molecules.getMol(i);
            instance = new double[size];

            for (int j = 0; j < size; j++)
            {
                attribute = (Attribute) attributesV.elementAt(j);

                // get parsed data
                genericData = mol.getData(attribute.name(), true);

                // add nominal or numeric or missing value
                if (genericData == null)
                {
                    instance[attribute.index()] = Instance.missingValue();
                }
                else
                {
                    if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                    {
                        pairData = (JOEPairData) genericData;

                        if (attribute.isNominal())
                        {
                            // nominal
                            String tmpS = pairData.toString().trim();

                            if (tmpS.indexOf("\n") != -1)
                            {
                                throw new WekaException("Descriptor " +
                                    attribute.name() +
                                    " contains multiple lines and is not a valid nominal value.");
                            }
                            else
                            {
                                instance[attribute.index()] = attribute.indexOfValue(pairData.toString());

                                if (instance[attribute.index()] == -1)
                                {
                                    // invalid nominal value
                                    logger.error("Invalid nominal value");

                                    return null;
                                }
                            }
                        }
                        else
                        {
                            // numeric
                            if (JOEHelper.hasInterface(pairData, "NativeValue"))
                            {
                                double tmpD = ((NativeValue) pairData).getDoubleNV();

                                if (Double.isNaN(tmpD))
                                {
                                    instance[attribute.index()] = Instance.missingValue();
                                }
                                else
                                {
                                    instance[attribute.index()] = tmpD;
                                }
                            }
                            else
                            {
                                throw new WekaException("Descriptor " +
                                    attribute.name() +
                                    " is not a native value.");
                            }
                        }
                    }
                }

                attribute.index();
            }

            // add created molecule instance to molecule instances
            instances.add(new Instance(1, instance));
        }

        return instances;
    }

    public static Instances matrix2instances(double[][] matrix,
        String[] descriptors, int[] attributeTypes)
    {
        if ((matrix == null) || (descriptors == null) ||
                (attributeTypes == null))
        {
            return null;
        }

        FastVector attributesV = new FastVector(descriptors.length);
        int molecules = matrix[0].length;

        for (int i = 0; i < descriptors.length; i++)
        {
            if (attributeTypes[i] == Attribute.NUMERIC)
            {
                // numeric
                attributesV.addElement(new Attribute(descriptors[i],
                        attributesV.size()));
            }
            else if (attributeTypes[i] == Attribute.NOMINAL)
            {
                // nominal
                // create a list with all nominal values
                Hashtable hashed = new Hashtable();

                for (int j = 0; j < molecules; j++)
                {
                    hashed.put(new Double(matrix[i][j]), "");
                }

                // store list of nominal values in the Weka data structure
                FastVector attributeValues = new FastVector(hashed.size());
                Double tmp;

                for (Enumeration e = hashed.keys(); e.hasMoreElements();)
                {
                    tmp = (Double) e.nextElement();
                    attributeValues.addElement(tmp.toString());

                    //System.out.println("NOMINAL " + tmp);
                }

                attributesV.addElement(new Attribute(descriptors[i],
                        attributeValues, attributesV.size()));
            }
        }

        int descriptorSize = attributesV.size();
        Attribute attribute = null;

        // create molecule instances
        Instances instances = new Instances("MatrixInstances", attributesV,
                attributesV.size());

        // iterate over all instances (to generate them)
        double[] instance;

        for (int i = 0; i < molecules; i++)
        {
            instance = new double[descriptorSize];

            for (int j = 0; j < descriptorSize; j++)
            {
                attribute = (Attribute) attributesV.elementAt(j);

                if (Double.isNaN(matrix[j][i]))
                {
                    instance[attribute.index()] = Instance.missingValue();
                }
                else
                {
                    if (attributeTypes[j] == Attribute.NUMERIC)
                    {
                        // numeric
                        instance[attribute.index()] = matrix[j][i];
                    }
                    else if (attributeTypes[j] == Attribute.NOMINAL)
                    {
                        // nominal
                        instance[attribute.index()] = attribute.indexOfValue(Double.toString(
                                    matrix[j][i]));

                        if (instance[attribute.index()] == -1)
                        {
                            // invalid nominal value
                            logger.error("Invalid nominal value");

                            return null;
                        }
                    }
                }

                attribute.index();
            }

            // add created molecule instance to molecule instances
            Instance inst = new Instance(1, instance);
            instances.add(inst);

            //System.out.println("instance (attr.:"+inst.numAttributes()+", vals:"+inst.numValues()+"): "+inst);
        }

        //System.out.println(instances.toString());
        return instances;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
