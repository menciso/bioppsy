///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MorganTest.java,v $
//  Purpose:  Unique molecule numbering test.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2004/02/20 13:11:41 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo.morgan;

import wsi.ra.tool.ResourceLoader;

/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.log4j.Category;

import joelib.algo.morgan.types.BasicTieResolver;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.SimpleReader;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================  */

/**
 * Unique molecule numbering test.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/02/20 13:11:41 $
 */
public class MorganTest
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------  */

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.algo.morgan.MorganTest");

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * main
     *-------------------------------------------------------------------------  */

    /**
     *  The main program for the TestSmarts class
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args)
    {
        MorganTest morgan = new MorganTest();

        if (args.length != 1)
        {
            morgan.usage();
            System.exit(0);
        }
        else
        {
            //        String molURL = new String("joelib/test/test.mol");
            morgan.test(args[0], IOTypeHolder.instance().getIOType("SDF"),
                IOTypeHolder.instance().getIOType("SDF"));
        }
    }

    /**
     *  A unit test for JUnit
     *
     * @param  molURL   Description of the Parameter
     * @param  inType   Description of the Parameter
     * @param  outType  Description of the Parameter
     */
    public void test(String molURL, IOType inType, IOType outType)
    {
        // get molecules from resource URL
        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(molURL);

        if (bytes == null)
        {
            logger.error("Molecule can't be loaded at \"" + molURL + "\".");
            System.exit(1);
        }

        ByteArrayInputStream sReader = new ByteArrayInputStream(bytes);

        // create simple reader
        SimpleReader reader = null;

        try
        {
            reader = new SimpleReader(sReader, inType);
        }
         catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        // load molecules and handle test
        JOEMol mol = new JOEMol(inType, outType);
        JOEMol renumberedMol;
        Morgan morgan = new Morgan(new BasicTieResolver());

        for (;;)
        {
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }

            System.out.println("--------------------------------------");
            mol.deleteHydrogens();

            //logger.info("Processing (atoms="+mol.numAtoms()+"):" + mol.getTitle());
            //System.out.println("Hashcode:" + getHashcode(mol));
            //System.out.println("Molecule before renumbering:");
            //System.out.println(mol);
            if (morgan.calculate(mol))
            {
                renumberedMol = morgan.renumber(mol);

                //System.out.println("Molecule after renumbering:");
                //System.out.println(mol);
                String status;
                String statusSMILES;

                if (morgan.tieResolvingProblem())
                {
                    status = "Unsure";
                    statusSMILES = "Nearly Unique/Canonical";
                }
                else
                {
                    status = "Sure";
                    statusSMILES = "Unique/Canonical";
                }

                System.out.println(status +
                    " hashcode(without E/Z and S/R) for " + mol.getTitle() +
                    ": " + getHashcode(renumberedMol));
                System.out.print("Basic SMILES: " +
                    mol.toString(IOTypeHolder.instance().getIOType("SMILES")));
                System.out.print(statusSMILES + " SMILES: " +
                    renumberedMol.toString(IOTypeHolder.instance().getIOType("SMILES")));
            }
            else
            {
                System.out.println("");
            }

            //System.out.println(mol.toString(IOTypeHolder.instance().getIOType("SDF")));
        }
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */

    /**
     *  Description of the Method
     */
    public void usage()
    {
        StringBuffer sb = new StringBuffer();
        String programName = this.getClass().getName();

        sb.append("Usage is : ");
        sb.append("java -cp . ");
        sb.append(programName);

        System.out.println(sb.toString());

        System.exit(0);
    }

    /**
     * Primitive hashcode method without chirality and cis/trans.
     *
     * @param mol
     * @return int
     */
    private int getHashcode(JOEMol mol)
    {
        int hash = mol.numRotors();
        JOEAtom atom;

        // take number of rings into account
        hash = (31 * hash) + mol.getSSSR().size();

        for (int i = 1; i <= mol.numAtoms(); i++)
        {
            atom = mol.getAtom(i);

            hash = (31 * hash) + atom.getIdx();
            hash = (31 * hash) + atom.getAtomicNum();
            hash = (31 * hash) + atom.getHvyValence();
            hash = (31 * hash) + atom.getImplicitValence();
        }

        return hash;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
