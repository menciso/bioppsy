///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SingleTieResolverPartCh.java,v $
//  Purpose:  Helper class for resolving renumbering ties.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2003/08/19 13:11:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo.morgan.types;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import joelib.algo.morgan.AtomDoubleParent;
import joelib.algo.morgan.SingleTieResolver;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.iterator.NbrAtomIterator;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Interface for resolving renumbering ties.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2003/08/19 13:11:24 $
 */
public class SingleTieResolverPartCh implements SingleTieResolver
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member methods
     *------------------------------------------------------------------------- */
    public double getResolvingValue(AtomDoubleParent ap, JOEMol mol)
    {
        //		double tmp=0.0;
        //
        //		NbrAtomIterator nait = mol.getAtom(ap.atomIdx).nbrAtomIterator();
        //		JOEBond bond;
        //		JOEAtom nbrAtom;
        //		while (nait.hasNext())
        //		{
        //			nbrAtom = nait.nextNbrAtom();
        //			bond = nait.actualBond();
        //			tmp+=nbrAtom.getPartialCharge();
        //		}
        //		System.out.println("pChSum:"+tmp);
        //System.out.print("pCh:"+mol.getAtom(ap.atomIdx).getPartialCharge());
        //		double tmp2=0.0;
        //		nait = mol.getAtom(ap.parent).nbrAtomIterator();
        //		while (nait.hasNext())
        //		{
        //			nbrAtom = nait.nextNbrAtom();
        //			bond = nait.actualBond();
        //			tmp2+=nbrAtom.getPartialCharge();
        //		}
        //		System.out.println("pChSumP:"+tmp);
        //System.out.println(" pChP:"+mol.getAtom(ap.parent).getPartialCharge());
        //		return tmp;
        return (double) mol.getAtom(ap.atomIdx).getPartialCharge();
    }

    public boolean init(JOEMol mol)
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
