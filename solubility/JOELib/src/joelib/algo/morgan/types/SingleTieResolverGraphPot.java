///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SingleTieResolverGraphPot.java,v $
//  Purpose:  Helper class for resolving renumbering ties.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2004/02/20 13:11:42 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo.morgan.types;

import org.apache.log4j.Category;

/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import joelib.algo.morgan.AtomDoubleParent;
import joelib.algo.morgan.SingleTieResolver;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.types.GraphPotentials;

import joelib.molecule.JOEMol;
import joelib.molecule.types.AtomProperties;

import joelib.util.JOEHelper;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Interface for resolving renumbering ties.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/02/20 13:11:42 $
 */
public class SingleTieResolverGraphPot implements SingleTieResolver
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.algo.morgan.types.SingleTieResolverGraphPot");

    //~ Instance fields ////////////////////////////////////////////////////////

    private AtomProperties atomProperties;
    private boolean initialized = false;

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member methods
     *------------------------------------------------------------------------- */
    public double getResolvingValue(AtomDoubleParent ap, JOEMol mol)
    {
        // initialize atom properties
        if (!initialized)
        {
            return 0.0;
        }

        //System.out.println("gp:"+atomProperties.getDoubleValue(ap.atomIdx));
        //System.out.println("gpP:"+atomProperties.getDoubleValue(mol.getAtom(ap.parent).getIdx()));
        return atomProperties.getDoubleValue(ap.atomIdx);
    }

    public boolean init(JOEMol mol)
    {
        initialized = false;

        String propertyName = GraphPotentials.DESC_KEY;

        // get atom properties or calculate if not already available
        DescResult tmpPropResult = null;

        try
        {
            tmpPropResult = DescriptorHelper.instance().descFromMol(mol,
                    propertyName);
        }
         catch (DescriptorException e)
        {
            logger.error("Atom property " + propertyName + " does not exist.");

            return false;
        }

        if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
        {
            atomProperties = (AtomProperties) tmpPropResult;
        }
        else
        {
            logger.error("Property '" + propertyName +
                "' must be an atom type.");

            return false;
        }

        initialized = true;

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
