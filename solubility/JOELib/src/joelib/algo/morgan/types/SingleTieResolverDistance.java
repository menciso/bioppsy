///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SingleTieResolverDistance.java,v $
//  Purpose:  Helper class for resolving renumbering ties.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2004/02/20 13:11:42 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo.morgan.types;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import org.apache.log4j.Category;

import joelib.algo.morgan.AtomDoubleParent;
import joelib.algo.morgan.SingleTieResolver;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.result.IntMatrixResult;

import joelib.molecule.JOEMol;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Interface for resolving renumbering ties.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/02/20 13:11:42 $
 */
public class SingleTieResolverDistance implements SingleTieResolver
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.algo.morgan.types.SingleTieResolverDistance");

    //~ Instance fields ////////////////////////////////////////////////////////

    private int[] maxDistances;
    private boolean initialized = false;

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member methods
     *------------------------------------------------------------------------- */
    public double getResolvingValue(AtomDoubleParent ap, JOEMol mol)
    {
        // initialize atom properties
        if (!initialized)
        {
            return 0.0;
        }

        //System.out.println("dist:"+maxDistances[ap.atomIdx - 1]);
        //System.out.println("distP:"+maxDistances[mol.getAtom(ap.parent).getIdx() - 1]);
        return maxDistances[ap.atomIdx - 1];
    }

    public boolean init(JOEMol mol)
    {
        initialized = false;

        // get distance matrix or calculate if not already available
        DescResult tmpResult = null;
        String distanceMatrixKey = "Distance_matrix";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol,
                    distanceMatrixKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate distance matrix.");

            return false;
        }

        if (!(tmpResult instanceof IntMatrixResult))
        {
            logger.error("Needed descriptor '" + distanceMatrixKey +
                "' should be of type " + IntMatrixResult.class.getName() + ".");

            return false;
        }

        IntMatrixResult distResult = (IntMatrixResult) tmpResult;
        int[][] distances = distResult.value;

        maxDistances = new int[mol.numAtoms()];

        for (int i = 0; i < distances.length; i++)
        {
            maxDistances[i] = 0;

            for (int ii = 0; ii < i; ii++)
            {
                if (maxDistances[i] < distances[i][ii])
                {
                    maxDistances[i] = distances[i][ii];
                }
            }
        }

        initialized = true;

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
