///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SingleTieResolverBO.java,v $
//  Purpose:  Helper class for resolving renumbering ties.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.3 $
//            $Date: 2003/08/19 13:11:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo.morgan.types;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import joelib.algo.morgan.AtomDoubleParent;
import joelib.algo.morgan.SingleTieResolver;

import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Interface for resolving renumbering ties.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.3 $, $Date: 2003/08/19 13:11:24 $
 */
public class SingleTieResolverBO implements SingleTieResolver
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member methods
     *------------------------------------------------------------------------- */
    public double getResolvingValue(AtomDoubleParent ap, JOEMol mol)
    {
        JOEBond bond = mol.getBond(mol.getAtom(ap.atomIdx),
                mol.getAtom(ap.parent));

        return (double) bond.getBondOrder();
    }

    public boolean init(JOEMol mol)
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
