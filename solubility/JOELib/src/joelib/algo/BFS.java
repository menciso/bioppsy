///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BFS.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.21 $
//            $Date: 2004/02/20 13:11:41 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo;

import wsi.ra.tool.Deque;
import wsi.ra.tool.DequeNode;

/*==========================================================================*
 * IMPORTS
 *==========================================================================    */
import java.util.Map;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;
import joelib.util.iterator.NbrAtomIterator;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================    */

/**
 *  Breadh First Search.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.21 $, $Date: 2004/02/20 13:11:41 $
 * @cite clr98complexity
 * @cite clr98bfs
 */
public class BFS implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    public final static String STARTING_ATOM = "STARTING_ATOM";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(STARTING_ATOM, "joelib.molecule.JOEAtom",
                "The start atom.", true),
        };
    public final static String DESC_KEY = "Breadth_first_search";

    /*-------------------------------------------------------------------------*
     * public static member variables
     *-------------------------------------------------------------------------        */

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance("joelib.algo.BFS");
    private final static int WHITE = 0;
    private final static int GRAY = 1;
    private final static int BLACK = 2;

    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private variables
     *-------------------------------------------------------------------------    */
    private DescriptorInfo descInfo;
    private JOEAtom startAtom;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------    */

    /**
     *  Constructor for the KierShape1 object
     */
    public BFS()
    {
        String representation = this.getClass().getName();
        descInfo = new DescriptorInfo(DESC_KEY,
                DescriptorInfo.TYPE_NO_COORDINATES, representation,
                "docs/algo/BreadthFirstSearch", "joelib.algo.BFSInit",
                "joelib.algo.BFSResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------    */

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @param  _descInfo  The new descInfo value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     * @return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        if (startAtom == null)
        {
            startAtom = mol.getAtom(1);
        }

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        if (startAtom == null)
        {
            startAtom = mol.getAtom(1);
        }

        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        BFSResult result = null;
        int atoms = mol.numAtoms();

        if (mol.empty())
        {
            logger.error("Empty molecule '" + mol.getTitle() + "'.");

            return null;
        }

        // check if the result type is correct
        if (!(descResult instanceof BFSResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                BFSResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (BFSResult) descResult;

            if (result.traverse == null)
            {
                result.traverse = new int[atoms];
                result.parent = new int[atoms];
            }
            else if (result.traverse.length != atoms)
            {
                result.traverse = null;
                result.traverse = new int[atoms];
                result.parent = null;
                result.parent = new int[atoms];
            }
        }

        // check if the init type is correct
        if (properties == null)
        {
            if (startAtom == null)
            {
                logger.error(descInfo.getName() +
                    " properties is not defined. Please define: " +
                    STARTING_ATOM);

                return null;
            }
        }
        else
        {
            if (!initialize(properties))
            {
                return null;
            }
        }

        // create temporary arrays
        // position 0 will be unused, because the
        // lowest atom index will be 1 !
        int[] traverse = new int[atoms + 1];
        int[] parent = new int[atoms + 1];
        int[] color = new int[atoms + 1];
        Deque fifo = new Deque();

        // all atoms are not visited
        for (int i = 0; i < (atoms + 1); i++)
        {
            color[startAtom.getIdx()] = WHITE;
            traverse[i] = Integer.MAX_VALUE;
            parent[i] = -1;
        }

        // start atom for the first time visited
        color[startAtom.getIdx()] = GRAY;
        traverse[startAtom.getIdx()] = 0;

        // add start node to the FIFO
        fifo.pushBack(startAtom);

        NbrAtomIterator nbrIterator;

        // repeat until the unfinished stack contains elements
        while (!fifo.isEmpty())
        {
            JOEAtom atom = (JOEAtom) ((DequeNode) fifo.getFront()).key;
            nbrIterator = atom.nbrAtomIterator();

            JOEAtom nbr;

            while (nbrIterator.hasNext())
            {
                nbr = nbrIterator.nextNbrAtom();

                // visit undeveloped neighbour node
                if (color[nbr.getIdx()] == WHITE)
                {
                    // mark as developed
                    color[nbr.getIdx()] = GRAY;

                    // set time number node has been traversed
                    traverse[nbr.getIdx()] = traverse[atom.getIdx()] + 1;

                    // set previous visited node
                    parent[nbr.getIdx()] = atom.getIdx();

                    // add this unfinished node to the FIFO
                    fifo.pushBack(nbr);
                }
            }

            // mark node as finished and remove it from the stack
            fifo.popFront();
            color[atom.getIdx()] = BLACK;
        }

        // create temporary array contents to
        // result arrays
        System.arraycopy(traverse, 1, result.traverse, 0, atoms);
        System.arraycopy(parent, 1, result.parent, 0, atoms);

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
        startAtom = null;
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        JOEAtom atom = (JOEAtom) JOEPropertyHelper.getProperty(this,
                STARTING_ATOM, properties);

        if (atom == null)
        {
            startAtom = null;
        }
        else
        {
            startAtom = atom;
        }

        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
