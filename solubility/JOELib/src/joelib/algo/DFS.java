///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DFS.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.22 $
//            $Date: 2004/02/20 13:11:41 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.algo;


/*==========================================================================*
 * IMPORTS
 *==========================================================================        */
import java.util.Map;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;
import joelib.util.iterator.NbrAtomIterator;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================        */

/**
 * Depth First Search.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.22 $, $Date: 2004/02/20 13:11:41 $
 * @cite clr98complexity
 * @cite clr98dfs
 */
public class DFS implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public static member variables
     *-------------------------------------------------------------------------            */

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance("joelib.algo.DFS");
    public final static String DESC_KEY = "Depth_first_search";
    public final static String STARTING_ATOM = "STARTING_ATOM";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(STARTING_ATOM, "joelib.molecule.JOEAtom",
                "The start atom.", true),
        };
    private final static int WHITE = 0;
    private final static int GRAY = 1;
    private final static int BLACK = 2;

    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private variables
     *-------------------------------------------------------------------------        */
    private DescriptorInfo descInfo;
    private JOEAtom startAtom;
    private int[] color = null;
    private int[] discovered = null;
    private int[] finished = null;
    private int[] parent = null;
    private int traverseTime = 0;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------        */

    /**
     *  Constructor for the KierShape1 object
     */
    public DFS()
    {
        String representation = this.getClass().getName();
        descInfo = new DescriptorInfo(DESC_KEY,
                DescriptorInfo.TYPE_NO_COORDINATES, representation,
                "docs/algo/DepthFirstSearch", "joelib.algo.DFSInit",
                "joelib.algo.DFSResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------        */

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @param  _descInfo  The new descInfo value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     * @return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        if (startAtom == null)
        {
            startAtom = mol.getAtom(1);
        }

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        if (startAtom == null)
        {
            startAtom = mol.getAtom(1);
        }

        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        DFSResult result = null;
        int atoms = mol.numAtoms();

        if (mol.empty())
        {
            logger.error("Empty molecule '" + mol.getTitle() + "'.");

            return null;
        }

        // check if the result type is correct
        if (!(descResult instanceof DFSResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                DFSResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());
        }

        // initialize result type, if not already initialized
        else
        {
            result = (DFSResult) descResult;

            if (result.discovered == null)
            {
                result.discovered = new int[atoms];
                result.finished = new int[atoms];
                result.parent = new int[atoms];
            }
            else if (result.discovered.length != atoms)
            {
                result.discovered = null;
                result.discovered = new int[atoms];
                result.finished = null;
                result.finished = new int[atoms];
                result.parent = null;
                result.parent = new int[atoms];
            }
        }

        // check if the init type is correct
        if (properties == null)
        {
            if (startAtom == null)
            {
                logger.error(descInfo.getName() +
                    " initData is not defined. Please define: " +
                    STARTING_ATOM);

                return null;
            }
        }
        else
        {
            if (!initialize(properties))
            {
                return null;
            }
        }

        // create temporary arrays
        // position 0 will be unused, because the
        // lowest atom index will be 1 !
        finished = new int[atoms + 1];
        discovered = new int[atoms + 1];
        parent = new int[atoms + 1];
        color = new int[atoms + 1];

        //int            traverseTime  = 0;
        // all atoms are not visited
        for (int i = 0; i < atoms; i++)
        {
            color[i] = WHITE;
            discovered[i] = Integer.MAX_VALUE;
            finished[i] = Integer.MAX_VALUE;
            parent[i] = -1;
        }

        // start with this atom
        visit(startAtom);

        // visit now all nodes that are still WHITE (not visited)
        for (int i = 1; i <= atoms; i++)
        {
            if (color[i] == WHITE)
            {
                visit(mol.getAtom(i));
            }
        }

        // create temporary array contents to
        // result arrays
        System.arraycopy(finished, 1, result.finished, 0, atoms);
        System.arraycopy(discovered, 1, result.discovered, 0, atoms);
        System.arraycopy(parent, 1, result.parent, 0, atoms);

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
        discovered = null;
        finished = null;
        parent = null;
        color = null;

        traverseTime = 0;
        startAtom = null;
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        JOEAtom atom = (JOEAtom) JOEPropertyHelper.getProperty(this,
                STARTING_ATOM, properties);

        if (atom == null)
        {
            startAtom = null;
        }
        else
        {
            startAtom = atom;
        }

        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }

    /*-------------------------------------------------------------------------*
     * private methods
     *-------------------------------------------------------------------------        */

    /**
     *  Description of the Method
     *
     * @param  atom  Description of the Parameter
     */
    private void visit(JOEAtom atom)
    {
        // mark node as developed
        color[atom.getIdx()] = GRAY;

        // store time when node was discovered
        discovered[atom.getIdx()] = ++traverseTime;

        NbrAtomIterator nbrIterator;
        nbrIterator = atom.nbrAtomIterator();

        JOEAtom nbr;

        while (nbrIterator.hasNext())
        {
            nbr = nbrIterator.nextNbrAtom();

            // visit undeveloped neighbour node
            if (color[nbr.getIdx()] == WHITE)
            {
                parent[nbr.getIdx()] = atom.getIdx();
                visit(nbr);
            }
        }

        // mark node as developed and finished
        color[atom.getIdx()] = BLACK;

        // store time when node was finished
        finished[atom.getIdx()] = ++traverseTime;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
