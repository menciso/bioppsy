///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JCAMPException.java,v $
//  Purpose:  Reader/Writer for SDF files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:22 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.jcamp;


/**
 * Exception for JCAMP parser.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/07/25 20:43:22 $
 * @cite dl93
 * @cite dw88
 * @cite ghhjs91
 * @cite lhdl94
 * @cite dhl90
 */
public class JCAMPException extends Exception
{
    //~ Constructors ///////////////////////////////////////////////////////////

    public JCAMPException()
    {
        super();
    }

    /**
     *  Constructor for the JCAMPException object
     *
     * @param  s  Description of the Parameter
     */
    public JCAMPException(String s)
    {
        super(s);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
