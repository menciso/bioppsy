///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: LabelData.java,v $
//  Purpose:  Reader/Writer for SDF files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:22 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.jcamp;


/**
 * Label/Data entry for JCAMP format.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/07/25 20:43:22 $
 * @cite dl93
 * @cite dw88
 * @cite ghhjs91
 * @cite lhdl94
 * @cite dhl90
 */
public class LabelData
{
    //~ Instance fields ////////////////////////////////////////////////////////

    public String data;
    public String label;

    //~ Constructors ///////////////////////////////////////////////////////////

    public LabelData()
    {
        label = new String();
        data = new String();
    }

    public LabelData(String sl, String sd)
    {
        label = sl;
        data = sd;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public final void setData(String s)
    {
        data = s;
    }

    public final String getData()
    {
        return data;
    }

    public final void setLabel(String s)
    {
        label = s;
    }

    public final String getLabel()
    {
        return label;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
