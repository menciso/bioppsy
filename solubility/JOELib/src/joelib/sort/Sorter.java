///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Sorter.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:43:27 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.sort;

import java.util.Comparator;
import java.util.Vector;


/**
 *  The basic class for sort classes.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:27 $
 *
 * @see        Comparator
 * @see        Comparable
 * @see        java.util.Arrays#sort(java.lang.Object[], java.util.Comparator)
 * @see        joelib.sort.InsertSort
 * @see        joelib.sort.QuickInsertSort
 */
public abstract class Sorter
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     */
    public final static void swap(int[] x, int a, int b)
    {
        int t = x[a];
        x[a] = x[b];
        x[b] = t;
    }

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     */
    public final static void swap(double[] x, int a, int b)
    {
        double t = x[a];
        x[a] = x[b];
        x[b] = t;
    }

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     */
    public final static void swap(long[] x, int a, int b)
    {
        long t = x[a];
        x[a] = x[b];
        x[b] = t;
    }

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     */
    public final static void swap(Object[] x, int a, int b)
    {
        Object t = x[a];
        x[a] = x[b];
        x[b] = t;
    }

    /**
     *  Description of the Method
     *
     * @param  vec  Description of the Parameter
     * @param  a    Description of the Parameter
     * @param  b    Description of the Parameter
     */
    public final static void swap(Vector vec, int a, int b)
    {
        Object t = vec.get(a);
        vec.set(a, vec.get(b));
        vec.set(b, t);
    }

    /**
     *  Description of the Method
     *
     * @param  xy  Description of the Parameter
     * @param  a   Description of the Parameter
     * @param  b   Description of the Parameter
     */
    public final static void swap(XYDoubleArray xy, int a, int b)
    {
        xy.swap(a, b);
    }

    /**
     *  Description of the Method
     *
     * @param  xy  Description of the Parameter
     * @param  a   Description of the Parameter
     * @param  b   Description of the Parameter
     */
    public final static void swap(XYIntArray xy, int a, int b)
    {
        xy.swap(a, b);
    }

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     */
    public abstract void sort(int[] x);

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     */
    public abstract void sort(double[] x);

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     */
    public abstract void sort(long[] x);

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     * @param  c  Description of the Parameter
     */
    public abstract void sort(Object[] x, Comparator c);

    /**
     *  Description of the Method
     *
     * @param  xy  Description of the Parameter
     */
    public abstract void sortX(XYDoubleArray xy);

    /**
     *  Description of the Method
     *
     * @param  xy  Description of the Parameter
     */
    public abstract void sortX(XYIntArray xy);

    /**
     *  Description of the Method
     *
     * @param  xy  Description of the Parameter
     */
    public abstract void sortY(XYDoubleArray xy);

    /**
     *  Description of the Method
     *
     * @param  xy  Description of the Parameter
     */
    public abstract void sortY(XYIntArray xy);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
