///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeFileCreation.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/02/20 13:12:02 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.types;

import wsi.ra.io.FileUtilities;

/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import java.util.Map;

import org.apache.log4j.Category;

import joelib.io.IOTypeHolder;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcess;
import joelib.process.JOEProcessException;
import joelib.process.ProcessInfo;

import joelib.util.JHM;
import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 *  Calling processor classes if the filter rule fits.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/02/20 13:12:02 $
 */
public class MoleculeFileCreation implements JOEProcess
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
       *  public static member variables
       *-------------------------------------------------------------------------*/

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.process.types.MoleculeFileCreation");
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty("IO_TYPE", "java.lang.String",
                "IOType of the file to create", false),
            new JOEProperty("FILE", "java.lang.String",
                "Filename of the file that will be created in the temporary directory.",
                true, "joelib-molecule.tmp")
        };

    //~ Instance fields ////////////////////////////////////////////////////////

    private ProcessInfo info;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     *  constructor
     *-------------------------------------------------------------------------*/

    /**
     *  Constructor for the DescSelectionWriter object
     */
    public MoleculeFileCreation()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the processInfo attribute of the ProcessPipe object
     *
     * @param  _info  The new processInfo value
     */
    public void setProcessInfo(ProcessInfo _info)
    {
        info = _info;
    }

    /*-------------------------------------------------------------------------*
     * public  methods
     *-------------------------------------------------------------------------*/

    /**
     *  Gets the processInfo attribute of the ProcessPipe object
     *
     * @return    The processInfo value
     */
    public ProcessInfo getProcessInfo()
    {
        return info;
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    public boolean clear()
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean process(JOEMol mol, Map properties)
        throws JOEProcessException
    {
        // check properties
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            throw new JOEProcessException(
                "Empty property definition for process or missing property entry.");

            //return false;
        }

        // get plain file name
        String filename = (String) JOEPropertyHelper.getProperty(this, "FILE",
                properties);

        // create full path to file and create file
        String fullFilename = JHM.getTempFileBase() + filename;

        try
        {
            fullFilename = FileUtilities.instance().createNewFileName(fullFilename);
        }
         catch (Exception ex)
        {
            logger.error(ex.toString());

            return false;
        }

        // get io type and create molecule file
        String ioTypeName = (String) JOEPropertyHelper.getProperty("IO_TYPE",
                properties);

        if (!JHM.moleculeToFile(fullFilename, mol,
                    IOTypeHolder.instance().getIOType(ioTypeName), true))
        {
            return false;
        }

        // store created file name in properties
        JOEPropertyHelper.setProperty("FILE", properties, fullFilename);

        if (logger.isDebugEnabled())
        {
            logger.debug("Molecule filename  '" + fullFilename + "' created.");
        }

        return true;
    }

    /*-------------------------------------------------------------------------*
     * private  methods
     *-------------------------------------------------------------------------*/
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
