///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DistanceCalculation.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/02/20 13:12:02 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.types;


/*==========================================================================*
 * IMPORTS
 *==========================================================================    */
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.io.IOType;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcessException;
import joelib.process.SimpleProcess;

import joelib.util.ComparisonHelper;
import joelib.util.JOEProperty;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================    */

/**
 *  Calling processor classes if the filter rule fits.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/02/20 13:12:02 $
 */
public class DistanceCalculation extends SimpleProcess
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     *  public static member variables
     *-------------------------------------------------------------------------    */

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.process.types.DistanceCalculation");

    //~ Instance fields ////////////////////////////////////////////////////////

    //  private final static  JOEProperty[]  ACCEPTED_PROPERTIES    = new JOEProperty[]{
    //      new JOEProperty("NUMBER_OF_BINS", "java.lang.Integer", "Number of bins to create.", true),
    //      };
    private ComparisonHelper comparison;
    private double[] distanceValues;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     *  constructor
     *-------------------------------------------------------------------------    */

    /**
     *  Constructor for the DescSelectionWriter object
     */
    public DistanceCalculation()
    {
        clear();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public final Vector getTargetMols()
    {
        return comparison.getTargetMols();
    }

    public double[] getDistanceValues()
    {
        return distanceValues;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public boolean clear()
    {
        return true;
    }

    /*-------------------------------------------------------------------------*
     * public  methods
     *-------------------------------------------------------------------------    */

    /**
     * Description of the Method
     *
     * @param _statistic  Description of the Parameter
     */
    public void init(ComparisonHelper _comparison)
    {
        comparison = _comparison;
    }

    /**
     *  Description of the Method
     *
     * @param inType         Description of the Parameter
     * @param inStream       Description of the Parameter
     * @param _numberOfBins  Description of the Parameter
     * @exception Exception  Description of the Exception
     */
    public void init(IOType inType, String targetFile, String descriptor)
        throws Exception
    {
        logger.info("Checking target file for comparison.");
        comparison = new ComparisonHelper(inType, targetFile);
        comparison.setComparisonDescriptor(descriptor);
    }

    public void init(IOType inType, String targetFile, String[] descriptors)
        throws Exception
    {
        logger.info("Checking target file for comparison.");
        comparison = new ComparisonHelper(inType, targetFile);
        comparison.setComparisonDescriptors(descriptors);
    }

    public void init(JOEMol target, String descriptor)
        throws Exception
    {
        logger.info("Checking target file for comparison.");
        comparison = new ComparisonHelper(target);
        comparison.setComparisonDescriptor(descriptor);
    }

    public void init(JOEMol target, String[] descriptors)
        throws Exception
    {
        logger.info("Checking target file for comparison.");
        comparison = new ComparisonHelper(target);
        comparison.setComparisonDescriptors(descriptors);
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public JOEProperty[] neededProperties()
    {
        //    return ACCEPTED_PROPERTIES;
        return null;
    }

    /**
     *  Description of the Method
     *
     * @param mol                      Description of the Parameter
     * @param properties               Description of the Parameter
     * @return                         Description of the Return Value
     * @exception JOEProcessException  Description of the Exception
     */
    public boolean process(JOEMol mol, Map properties)
        throws JOEProcessException
    {
        if (comparison == null)
        {
            return false;
        }

        try
        {
            super.process(mol, properties);
        }
         catch (JOEProcessException e)
        {
            throw new JOEProcessException("Properties for " +
                this.getClass().getName() + " not correct.");
        }

        distanceValues = comparison.compare(mol, null, distanceValues);

        return true;
    }

    /*-------------------------------------------------------------------------*
     * private  methods
     *-------------------------------------------------------------------------    */
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
