///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DescVarianceNorm.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:25 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.types;

import wsi.ra.tool.ArrayStatistic;
import wsi.ra.tool.PropertyHolder;
import wsi.ra.tool.ResourceLoader;

import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEGenericData;
import joelib.data.JOEPairData;

import joelib.desc.NativeValue;

import joelib.io.IOType;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcessException;
import joelib.process.SimpleProcess;

import joelib.util.JOEHelper;
import joelib.util.JOEProperty;
import joelib.util.iterator.GenericDataIterator;


/**
 * Scales the values in one descriptor so that they have similar magnitudes.
 *
 * TeX: $x_i^n$ = \frac{x_i-\overline{x}}{\sigma _i}
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:25 $
 */
public class DescVarianceNorm extends SimpleProcess
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.process.types.DescVarianceNorm");

    //~ Instance fields ////////////////////////////////////////////////////////

    //  private final static  JOEProperty[]  ACCEPTED_PROPERTIES    = new JOEProperty[]{
    //      new JOEProperty("NUMBER_OF_BINS", "java.lang.Integer", "Number of bins to create.", true),
    //      };
    private DescStatistic statistic;
    private Vector desc2ignore;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DescSelectionWriter object
     */
    public DescVarianceNorm()
    {
        statistic = new DescStatistic();
        clear();

        // load descriptors which should be ignored
        String value;

        if ((value = PropertyHolder.instance().getProperty(this,
                        "descriptors2ignore")) != null)
        {
            Vector tmpVec = ResourceLoader.readLines(value);

            if (tmpVec == null)
            {
                logger.error("File with descriptor names to ignore not found.");
            }

            desc2ignore = tmpVec;
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean clear()
    {
        if (statistic == null)
        {
            return false;
        }

        return true;
    }

    public Vector descriptors2ignore()
    {
        return desc2ignore;
    }

    /**
     *  Description of the Method
     *
     * @param  _statistic  Description of the Parameter
     */
    public void init(DescStatistic _statistic)
    {
        statistic = _statistic;
    }

    /**
     *  Description of the Method
     *
     * @param  inType         Description of the Parameter
     * @param  inStream       Description of the Parameter
     * @param  _numberOfBins  Description of the Parameter
     * @exception  Exception  Description of the Exception
     */
    public void init(IOType inType, String inFile) throws Exception
    {
        logger.info(
            "Creating statistical data for descriptor variance normalisation.");
        statistic = DescStatistic.getDescStatistic(inType, inFile);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public JOEProperty[] neededProperties()
    {
        //    return ACCEPTED_PROPERTIES;
        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  properties               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  JOEProcessException  Description of the Exception
     */
    public boolean process(JOEMol mol, Map properties)
        throws JOEProcessException
    {
        if (statistic == null)
        {
            return false;
        }

        try
        {
            super.process(mol, properties);
        }
         catch (JOEProcessException e)
        {
            throw new JOEProcessException("Properties for " +
                this.getClass().getName() + " not correct.");
        }

        JOEGenericData genericData;
        GenericDataIterator gdit = mol.genericDataIterator();
        ArrayStatistic arrayStat;
        String descriptor;
        double value = 0.0;
        double newValue;
        boolean ignoreDesc = false;

        while (gdit.hasNext())
        {
            genericData = gdit.nextGenericData();
            descriptor = genericData.getAttribute();

            // ignore descriptors in list
            if (desc2ignore != null)
            {
                ignoreDesc = false;

                for (int i = 0; i < desc2ignore.size(); i++)
                {
                    if (descriptor.equals((String) desc2ignore.get(i)))
                    {
                        ignoreDesc = true;

                        break;
                    }
                }

                if (ignoreDesc)
                {
                    continue;
                }
            }

            if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
            {
                // parse data, if possible
                genericData = mol.getData(descriptor, true);

                // check descriptor statistic entry
                arrayStat = statistic.getDescriptorStatistic(descriptor);

                if (arrayStat == null)
                {
                    logger.error("Statistic for " + descriptor +
                        " don't exist");

                    return false;
                }

                JOEPairData data = (JOEPairData) genericData;

                if (JOEHelper.hasInterface(data, "NativeValue"))
                {
                    value = ((NativeValue) data).getDoubleNV();
                    newValue = arrayStat.varianceNormalization(value);

                    // to avoid precision loss all normalized values will now be internally
                    // stored as double values.
                    ((NativeValue) data).setDoubleNV(newValue);
                }
            }
        }

        return true;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String toString()
    {
        if (statistic == null)
        {
            return null;
        }

        //    StringBuffer    sb            = new StringBuffer(100);
        //    return sb.toString();
        return null;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
