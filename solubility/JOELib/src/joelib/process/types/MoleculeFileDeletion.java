///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeFileDeletion.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/02/20 13:12:02 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.types;


/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import java.io.File;

import java.util.Map;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcess;
import joelib.process.JOEProcessException;
import joelib.process.ProcessInfo;

import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 *  Calling processor classes if the filter rule fits.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/02/20 13:12:02 $
 */
public class MoleculeFileDeletion implements JOEProcess, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
       *  public static member variables
       *-------------------------------------------------------------------------*/

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.process.types.MoleculeFileDeletion");
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty("FILE", "java.lang.String",
                "Full path of the file that will be deleted.", false)
        };

    //~ Instance fields ////////////////////////////////////////////////////////

    private ProcessInfo info;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     *  constructor
     *-------------------------------------------------------------------------*/

    /**
     *  Constructor for the DescSelectionWriter object
     */
    public MoleculeFileDeletion()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the processInfo attribute of the ProcessPipe object
     *
     * @param  _info  The new processInfo value
     */
    public void setProcessInfo(ProcessInfo _info)
    {
        info = _info;
    }

    /*-------------------------------------------------------------------------*
     * public  methods
     *-------------------------------------------------------------------------*/

    /**
     *  Gets the processInfo attribute of the ProcessPipe object
     *
     * @return    The processInfo value
     */
    public ProcessInfo getProcessInfo()
    {
        return info;
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    public boolean clear()
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean process(JOEMol mol, Map properties)
        throws JOEProcessException
    {
        // check properties
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            throw new JOEProcessException(
                "Empty property definition for process or missing property entry.");

            //return false;
        }

        // get plain file name
        String filename = (String) JOEPropertyHelper.getProperty("FILE",
                properties);

        if (logger.isDebugEnabled())
        {
            logger.debug("Delete file: " + filename);
        }

        File file;

        //      try{
        file = new File(filename);
        file.delete();

        //      }
        //      catch( IOException e ) {
        //        logger.error(e.toString());
        //        return false;
        //      }
        return true;
    }

    /*-------------------------------------------------------------------------*
     * private  methods
     *-------------------------------------------------------------------------*/
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
