///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: CreateFileName.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:43:25 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.types;

import wsi.ra.io.FileUtilities;

import java.util.Map;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcess;
import joelib.process.JOEProcessException;
import joelib.process.ProcessInfo;

import joelib.util.JHM;
import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/**
 *  Calling processor classes if the filter rule fits.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:25 $
 */
public class CreateFileName implements JOEProcess
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.process.types.CreateFileName");
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty("FILE", "java.lang.String",
                "Filename of the file that will be created in the temporary directory.",
                false)
        };

    //~ Instance fields ////////////////////////////////////////////////////////

    private ProcessInfo info;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DescSelectionWriter object
     */
    public CreateFileName()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the processInfo attribute of the ProcessPipe object
     *
     * @param  _info  The new processInfo value
     */
    public void setProcessInfo(ProcessInfo _info)
    {
        info = _info;
    }

    /**
     *  Gets the processInfo attribute of the ProcessPipe object
     *
     * @return    The processInfo value
     */
    public ProcessInfo getProcessInfo()
    {
        return info;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean clear()
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  properties               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  JOEProcessException  Description of the Exception
     */
    public boolean process(JOEMol mol, Map properties)
        throws JOEProcessException
    {
        // check properties
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            throw new JOEProcessException(
                "Empty property definition for process or missing property entry.");

            //return false;
        }

        // get plain file name
        String filename = (String) JOEPropertyHelper.getProperty(this, "FILE",
                properties);

        // create full path to file and create file
        String fullFilename = JHM.getTempFileBase() + filename;

        try
        {
            fullFilename = FileUtilities.instance().createNewFileName(fullFilename);
        }
         catch (Exception ex)
        {
            logger.error(ex.toString());

            return false;
        }

        // store created file name in properties
        JOEPropertyHelper.setProperty("FILE", properties, fullFilename);

        if (logger.isDebugEnabled())
        {
            logger.debug("Filename  '" + fullFilename + "' created.");
        }

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
