///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Filter.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/03/15 23:16:40 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.filter;

import joelib.molecule.JOEMol;


/**
 * Interface definition for filter options for a molecule process.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/03/15 23:16:40 $
 * @see joelib.process.JOEProcess
 * @see joelib.process.ProcessPipe
 */
public interface Filter
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public void setFilterInfo(FilterInfo _info);

    public FilterInfo getFilterInfo();

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean accept(JOEMol mol);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
