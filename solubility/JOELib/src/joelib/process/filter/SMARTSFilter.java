///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SMARTSFilter.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:25 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.filter;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;


/**
 * Molecule process filter for SMARTS patterns.
 *
 * <p>
 * Example:
 * <blockquote><pre>
 * // accept all molecules, where the molecule contains ONE NC=O group connected to an aromatic atom (aNC=O)
 * SMARTSFilter filter =new SMARTSFilter(
 *                                "aNC=O",
 *                                NativeValueFilter.EQUAL,
 *                                1);
 *
 * // create molecule
 * JOEMol mol=new JOEMol();
 * String smiles="c1cc(OH)cc1";
 * if (!JOESmilesParser.smiToMol(mol, smiles, setTitle.toString()));
 *
 * // should we accept this molecule, what do you mean ?
 * System.out.println("Accept: ("+filter.toString()+")="+filter.accept(mol));
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:25 $
 * @cite smarts
 * @see joelib.smarts.JOESmartsPattern
 */
public class SMARTSFilter implements Filter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    public static final int SMALLER_EQUAL = 0;
    public static final int SMALLER = 1;
    public static final int EQUAL = 2;
    public static final int GREATER_EQUAL = 3;
    public static final int GREATER = 4;
    public static final int NOT_EQUAL = 5;
    private static final String[] allowedRules = new String[]
        {
            "<=", "<", "==", ">=", ">", "!="
        };

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.process.filter.SMARTSFilter");

    //~ Instance fields ////////////////////////////////////////////////////////

    private FilterInfo info;
    private JOESmartsPattern smarts;
    private boolean unique;
    private int relation;
    private int value;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DescriptorFilter object
     */
    public SMARTSFilter()
    {
    }

    /**
     *  Constructor for the DescriptorFilter object
     *
     * @param  descNamesURL  Description of the Parameter
     */
    public SMARTSFilter(String _smarts, int _relation, int _value,
        boolean _unique)
    {
        init(_smarts, _relation, _value, _unique);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the filterInfo attribute of the DescriptorFilter object
     *
     * @param  _info  The new filterInfo value
     */
    public void setFilterInfo(FilterInfo _info)
    {
        info = _info;
    }

    /**
     *  Gets the processInfo attribute of the DescriptorFilter object
     *
     * @return    The processInfo value
     */
    public FilterInfo getFilterInfo()
    {
        return info;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean accept(JOEMol mol)
    {
        if (smarts == null)
        {
            logger.warn("No SMARTS pattern defined in " +
                this.getClass().getName() + ".");

            return false;
        }

        smarts.match(mol);

        Vector matchList;

        if (unique)
        {
            matchList = smarts.getUMapList();
        }
        else
        {
            matchList = smarts.getMapList();
        }

        int matches = matchList.size();

        boolean result = false;

        switch (relation)
        {
        case SMALLER:
            result = (matches < value);

            break;

        case SMALLER_EQUAL:
            result = (matches <= value);

            break;

        case EQUAL:
            result = (matches == value);

            break;

        case GREATER_EQUAL:
            result = (matches >= value);

            break;

        case GREATER:
            result = (matches > value);

            break;

        case NOT_EQUAL:
            result = (matches != value);

            break;
        }

        return result;
    }

    public boolean fromString(String rule)
    {
        return fromString(rule, true);
    }

    public boolean fromString(String rule, boolean _unique)
    {
        unique = _unique;

        int index;
        boolean parsed = false;

        for (int i = 0; i < allowedRules.length; i++)
        {
            if ((index = rule.lastIndexOf(allowedRules[i])) != -1)
            {
                relation = i;

                //System.out.println("relation: "+allowedRules[i]);
                smarts = new JOESmartsPattern();

                if (!smarts.init(rule.substring(0, index)))
                {
                    logger.error("Invalid SMARTS pattern: " +
                        rule.substring(0, index));

                    return false;
                }

                String tmp = rule.substring(index + allowedRules[i].length());

                try
                {
                    value = Integer.parseInt(tmp);
                }
                 catch (NumberFormatException ex)
                {
                    logger.error("Invalid number: " + tmp);
                    logger.error("in rule: " + rule);

                    return false;
                }

                parsed = true;

                break;
            }
        }

        if (!parsed)
        {
            StringBuffer sb = new StringBuffer(30);
            sb.append("Allowed rules must contain: ");

            for (int i = 0; i < allowedRules.length; i++)
            {
                sb.append("'");
                sb.append(allowedRules[i]);
                sb.append("'");

                if (i < (allowedRules.length - 2))
                {
                    sb.append(", ");
                }

                if (i < (allowedRules.length - 1))
                {
                    sb.append(" or ");
                }
            }

            logger.error(sb.toString());
        }

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  _descNames  Description of the Parameter
     */
    public boolean init(String _smarts, int _relation, int _value,
        boolean _unique)
    {
        smarts = new JOESmartsPattern();

        if (!smarts.init(_smarts))
        {
            logger.error("Invalid SMARTS pattern: " + _smarts);

            return false;
        }

        relation = _relation;
        value = _value;
        unique = _unique;

        return true;
    }

    public void invertRelation()
    {
        switch (relation)
        {
        case SMALLER:
            relation = GREATER_EQUAL;

            break;

        case SMALLER_EQUAL:
            relation = GREATER;

            break;

        case EQUAL:
            relation = NOT_EQUAL;

            break;

        case GREATER_EQUAL:
            relation = SMALLER;

            break;

        case GREATER:
            relation = SMALLER_EQUAL;

            break;

        case NOT_EQUAL:
            relation = EQUAL;

            break;
        }
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer(30);
        sb.append(smarts.getSMARTS());
        sb.append(allowedRules[relation]);
        sb.append(value);

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
