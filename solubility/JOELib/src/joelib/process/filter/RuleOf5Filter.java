///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: RuleOf5Filter.java,v $
//  Purpose:  Filter to avoid molecules with a 'poor absorption or permeability'.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
///////////////////////////////////////////////////////////////////////////////
package joelib.process.filter;

import org.apache.log4j.Category;

import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorFactory;
import joelib.desc.result.DoubleResult;
import joelib.desc.result.IntResult;

import joelib.molecule.JOEMol;


/**
 * Filter to avoid molecules with a 'poor absorption or permeability'.
 * This is only a 'soft' filter and can be used to get an idea about lead/drug-likeness.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/07/25 20:43:24 $
 * @cite lldf01
 * @cite odtl01
 * @see joelib.desc.types.HBD1
 * @see joelib.desc.types.HBD2
 * @see joelib.desc.types.HBA1
 * @see joelib.desc.types.HBD2
 * @see joelib.desc.types.AtomInDonor
 * @see joelib.desc.types.AtomInAcceptor
 * @see joelib.desc.types.AtomInDonAcc
 */
public class RuleOf5Filter implements Filter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.process.filter.RuleOf5Filter");

    //~ Instance fields ////////////////////////////////////////////////////////

    private FilterInfo info;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DescriptorFilter object
     */
    public RuleOf5Filter()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the filterInfo attribute of the DescriptorFilter object
     *
     * @param  _info  The new filterInfo value
     */
    public void setFilterInfo(FilterInfo _info)
    {
        info = _info;
    }

    /**
     *  Gets the processInfo attribute of the DescriptorFilter object
     *
     * @return    The processInfo value
     */
    public FilterInfo getFilterInfo()
    {
        return info;
    }

    /**
     * Returns only <tt>true</tt> if less than two of the four rules fullfills the rules.
     * Otherwise a 'poor absorption or permeability' is possible and <tt>false</tt> will be returned.
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     * @cite lldf01
     */
    public boolean accept(JOEMol mol)
    {
        //    if (false)
        //    {
        //      logger.warn("" + this.getClass().getName() + ".");
        //      return false;
        //    }
        // gets lipinski parameters
        double mw = mol.getMolWt();

        //double logP = getLogP(mol, "XlogP");
        double logP = getLogP(mol, "LogP");
        int hba = getNumberHBA(mol, "HBA1");
        int hbd = getNumberHBD(mol, "HBD1");

        // check lipinski parameters
        boolean fitsRuleOf5 = false;
        int numberOfBreakedRules = 0;

        if (mw >= 500)
        {
            numberOfBreakedRules++;
        }

        if (logP >= 5)
        {
            numberOfBreakedRules++;
        }

        if (hba >= 10)
        {
            numberOfBreakedRules++;
        }

        if (hbd >= 5)
        {
            numberOfBreakedRules++;
        }

        // decide lipinski criteria
        if (numberOfBreakedRules < 2)
        {
            // all fine, no molecule with a
            // 'poor absorption or permeability'-problem
            fitsRuleOf5 = true;
        }

        return fitsRuleOf5;
    }

    private double getLogP(JOEMol mol, String name)
    {
        // call external logP calculation program

        /*      External ext =null;
              boolean success=false;
              Hashtable props = new Hashtable();
              try
              {
                ext= ExternalFactory.instance().getExternal(extName);
                success=ext.process(mol, props);
              }
              catch (ExternalException ex)
              {
                ex.printStackTrace();
                return Double.NaN;
              }
              catch (JOEProcessException ex)
              {
                ex.printStackTrace();
                return Double.NaN;
              }

              Double value=(Double)props.get("XLOGP");
              return value.doubleValue();
        */
        Descriptor logP = null;
        DescResult logPResult = null;

        try
        {
            logP = DescriptorFactory.getDescriptor(name);

            if (logP == null)
            {
                logger.error("Descriptor " + logP + " can't be loaded.");

                return -1;
            }

            logP.clear();
            logPResult = logP.calculate(mol);

            // has something weird happen
            if (logPResult == null)
            {
                return -1;
            }
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());

            return -1;
        }

        return ((DoubleResult) logPResult).value;
    }

    private int getNumberHBA(JOEMol mol, String hbaName)
    {
        Descriptor hba = null;
        DescResult hbaResult = null;

        try
        {
            hba = DescriptorFactory.getDescriptor(hbaName);

            if (hba == null)
            {
                logger.error("Descriptor " + hba + " can't be loaded.");

                return -1;
            }

            hba.clear();
            hbaResult = hba.calculate(mol);

            // has something weird happen
            if (hbaResult == null)
            {
                return -1;
            }
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());

            return -1;
        }

        return ((IntResult) hbaResult).getInt();
    }

    private int getNumberHBD(JOEMol mol, String hbdName)
    {
        Descriptor hbd = null;
        DescResult hbdResult = null;

        try
        {
            hbd = DescriptorFactory.getDescriptor(hbdName);

            if (hbd == null)
            {
                logger.error("Descriptor " + hbd + " can't be loaded.");

                return -1;
            }

            hbd.clear();
            hbdResult = hbd.calculate(mol);

            // has something weird happen
            if (hbdResult == null)
            {
                return -1;
            }
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());

            return -1;
        }

        return ((IntResult) hbdResult).getInt();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
