///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SelectionFilter.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:25 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
///////////////////////////////////////////////////////////////////////////////
package joelib.process.filter;

import joelib.molecule.JOEMol;


/**
 *  Interface definition for calling external programs from JOELib.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/07/25 20:43:25 $
 */
public class SelectionFilter implements Filter
{
    //~ Instance fields ////////////////////////////////////////////////////////

    // Obtain a suitable logger.
    //  private static        Category    logger             = Category.getInstance("joelib.process.filter.SelectionFilter");
    private FilterInfo info;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DescriptorFilter object
     */
    public SelectionFilter()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the filterInfo attribute of the DescriptorFilter object
     *
     * @param  _info  The new filterInfo value
     */
    public void setFilterInfo(FilterInfo _info)
    {
        info = _info;
    }

    /**
     *  Gets the processInfo attribute of the DescriptorFilter object
     *
     * @return    The processInfo value
     */
    public FilterInfo getFilterInfo()
    {
        return info;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean accept(JOEMol mol)
    {
        return false;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
