///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: FilterInfo.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2003/08/19 13:11:28 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.filter;


/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import joelib.util.types.BasicFactoryInfo;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 * Molecule process filter informations.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2003/08/19 13:11:28 $
 */
public class FilterInfo extends BasicFactoryInfo
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private member variables
     *-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the DescriptorInfo object
     *
     * @param  _name             Description of the Parameter
     * @param  _representation   Description of the Parameter
     * @param  _descriptionFile  Description of the Parameter
     */
    public FilterInfo(String _name, String _representation,
        String _descriptionFile)
    {
        super(_name, _representation, _descriptionFile);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
