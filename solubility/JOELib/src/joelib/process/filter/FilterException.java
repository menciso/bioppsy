///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: FilterException.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2003/08/19 13:11:28 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.filter;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Molecule process filter exception.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2003/08/19 13:11:28 $
 */
public class FilterException extends Exception
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *------------------------------------------------------------------------- */
    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the DescriptorException object
     */
    public FilterException()
    {
        super();
    }

    /**
     *  Constructor for the DescriptorException object
     *
     * @param  s  Description of the Parameter
     */
    public FilterException(String s)
    {
        super(s);
    }

    /*-------------------------------------------------------------------------*
     * public static methods
     *------------------------------------------------------------------------- */
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
