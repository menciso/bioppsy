///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: HasDataFilter.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/02/20 13:12:01 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.filter;

import org.apache.log4j.Category;

/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import joelib.molecule.JOEMol;


/*==========================================================================*
 * INTERFACE DECLARATION
 *==========================================================================  */

/**
 * Molecule process filter for available descriptor entries.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/02/20 13:12:01 $
 */
public class HasDataFilter implements Filter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------  */

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.process.filter.HasDataFilter");

    //~ Instance fields ////////////////////////////////////////////////////////

    private FilterInfo info;

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------  */
    private String attribute;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the DescriptorFilter object
     */
    public HasDataFilter()
    {
    }

    /**
     *  Constructor for the DescriptorFilter object
     *
     * @param  descNamesURL  Description of the Parameter
     */
    public HasDataFilter(String _attribute)
    {
        init(_attribute);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the filterInfo attribute of the DescriptorFilter object
     *
     * @param  _info  The new filterInfo value
     */
    public void setFilterInfo(FilterInfo _info)
    {
        info = _info;
    }

    /*-------------------------------------------------------------------------*
     * public  methods
     *-------------------------------------------------------------------------    */

    /**
     *  Gets the processInfo attribute of the DescriptorFilter object
     *
     * @return    The processInfo value
     */
    public FilterInfo getFilterInfo()
    {
        return info;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean accept(JOEMol mol)
    {
        if (attribute == null)
        {
            logger.warn("No data attribute defined in " +
                this.getClass().getName() + ".");

            return false;
        }

        return mol.hasData(attribute);
    }

    /**
     *  Description of the Method
     *
     * @param  _descNames  Description of the Parameter
     */
    public void init(String _attribute)
    {
        attribute = _attribute;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
