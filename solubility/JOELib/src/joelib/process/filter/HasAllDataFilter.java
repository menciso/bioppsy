///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: HasAllDataFilter.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/02/20 13:12:01 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process.filter;

import wsi.ra.tool.ResourceLoader;

/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;


/*==========================================================================*
 * INTERFACE DECLARATION
 *==========================================================================  */

/**
 * Molecule process filter for available descriptor entries.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/02/20 13:12:01 $
 */
public class HasAllDataFilter implements Filter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------  */

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.process.filter.HasAllDataFilter");

    //~ Instance fields ////////////////////////////////////////////////////////

    private FilterInfo info;

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------  */
    private Vector attributes;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the DescriptorFilter object
     */
    public HasAllDataFilter()
    {
    }

    /**
     *  Constructor for the DescriptorFilter object
     *
     * @param  descNamesURL  Description of the Parameter
     */
    public HasAllDataFilter(String filename)
    {
        Vector tmpInNames = ResourceLoader.readLines(filename);

        if (tmpInNames == null)
        {
            logger.error("File with data names " + filename +
                " could not be found.");

            return;
        }

        init(tmpInNames);
    }

    public HasAllDataFilter(Vector _attributes)
    {
        init(_attributes);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the filterInfo attribute of the DescriptorFilter object
     *
     * @param  _info  The new filterInfo value
     */
    public void setFilterInfo(FilterInfo _info)
    {
        info = _info;
    }

    /*-------------------------------------------------------------------------*
     * public  methods
     *-------------------------------------------------------------------------    */

    /**
     *  Gets the processInfo attribute of the DescriptorFilter object
     *
     * @return    The processInfo value
     */
    public FilterInfo getFilterInfo()
    {
        return info;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean accept(JOEMol mol)
    {
        if ((attributes == null) || (attributes.size() == 0))
        {
            logger.warn("No data attributes defined in " +
                this.getClass().getName() + ".");

            return false;
        }

        int s = attributes.size();

        for (int i = 0; i < s; i++)
        {
            if (!mol.hasData((String) attributes.get(i)))
            {
                return false;
            }
        }

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  _descNames  Description of the Parameter
     */
    public void init(Vector _attributes)
    {
        attributes = _attributes;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
