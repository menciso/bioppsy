///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ProcessPipe.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.18 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.process.filter.Filter;

import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/**
 * Processing pipe for appending multiple processes combined with
 * {@link Filter} functionality.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.18 $, $Date: 2004/07/25 20:43:24 $
 * @see ProcessPipeEntry
 * @see Filter
 */
public class ProcessPipe implements JOEProcess
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.process.ProcessPipe");

    //~ Instance fields ////////////////////////////////////////////////////////

    private ProcessInfo info;
    private Vector processes;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the SimpleReader object
     *
     * @param  _in            Description of the Parameter
     * @param  _inTypeString  Description of the Parameter
     */
    public ProcessPipe()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initializing " + this.getClass().getName());
        }

        processes = new Vector(20);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the processInfo attribute of the ProcessPipe object
     *
     * @param  _info  The new processInfo value
     */
    public void setProcessInfo(ProcessInfo _info)
    {
        info = _info;
    }

    /**
     *  Gets the processInfo attribute of the ProcessPipe object
     *
     * @return    The processInfo value
     */
    public ProcessInfo getProcessInfo()
    {
        return info;
    }

    public JOEProperty[] acceptedProperties()
    {
        return null;
    }

    public void addProcess(JOEProcess process)
    {
        addProcess(process, null, null);
    }

    public void addProcess(JOEProcess process, Hashtable properties)
    {
        addProcess(process, null, properties);
    }

    public void addProcess(JOEProcess process, Filter filter)
    {
        addProcess(process, filter, null);
    }

    public void addProcess(JOEProcess process, Filter filter,
        Hashtable properties)
    {
        ProcessPipeEntry entry;

        entry = new ProcessPipeEntry(process, filter, properties);
        processes.add(entry);
    }

    public boolean clear()
    {
        return true;
    }

    /**
     * Process single molecule entry.
     *
     * @param  mol  Description of the Parameter
     * @return      <tt>true</tt> if this molecule was sucesfully processed, or
     *               <tt>false</tt> if the molecule was skipped, or an error occurred.
     */
    public boolean process(JOEMol mol, Map properties)
        throws JOEProcessException
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            return false;
        }

        ProcessPipeEntry entry;
        boolean allProcessed = true;

        for (int i = 0; i < processes.size(); i++)
        {
            entry = (ProcessPipeEntry) processes.get(i);

            if ((entry.filter == null) || entry.filter.accept(mol))
            {
                //        System.out.println("start process:::");
                if (!entry.process.process(mol, entry.properties))
                {
                    allProcessed = false;

                    break;
                }
            }
            else if ((entry.filter != null) || !entry.filter.accept(mol))
            {
                return false;
            }
        }

        return allProcessed;
    }

    public boolean removeProcess(JOEProcess process)
    {
        return processes.remove(process);
    }

    public Object removeProcess(int index)
    {
        return processes.remove(index);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
