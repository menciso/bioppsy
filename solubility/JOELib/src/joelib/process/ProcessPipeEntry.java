///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ProcessPipeEntry.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process;

import java.util.Hashtable;

import joelib.process.filter.Filter;


/**
 * Processing pipe entry.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/25 20:43:24 $
 * @see JOEProcess
 * @see ProcessPipe
 * @see Filter
 */
public class ProcessPipeEntry
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    protected Filter filter;
    protected Hashtable properties;

    /**
     *  Description of the Field
     */
    protected JOEProcess process;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the ProcessPipeEntry object
     *
     * @param  _process  Description of the Parameter
     * @param  _filter   Description of the Parameter
     */
    public ProcessPipeEntry(JOEProcess _process, Filter _filter,
        Hashtable _properties)
    {
        process = _process;
        filter = _filter;
        properties = _properties;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Two entries are equal, if they contain the same <tt>JOEProcess</tt>
     * object.
     *
     * @param  obj  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof ProcessPipeEntry)
        {
            if (((ProcessPipeEntry) obj).process == this.process)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (obj instanceof JOEProcess)
        {
            if (((JOEProcess) obj) == this.process)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
