///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEProcess.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.14 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.process;

import java.util.Map;

import joelib.molecule.JOEMol;

import joelib.util.PropertyAcceptor;


/**
 *  Interface definition for calling molecule processes.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.14 $, $Date: 2004/07/25 20:43:24 $
 * @see joelib.process.ProcessPipe
 */
public interface JOEProcess extends PropertyAcceptor
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public void setProcessInfo(ProcessInfo _info);

    public ProcessInfo getProcessInfo();

    //    public JOEProperty[] acceptedProperties();
    public boolean clear();

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean process(JOEMol mol, Map properties)
        throws JOEProcessException;
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
