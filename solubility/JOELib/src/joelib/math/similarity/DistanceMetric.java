///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DistanceMetric.java,v $
//  Purpose:  Interface to have a fast method to native descriptor values.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2004/07/25 20:43:23 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.math.similarity;


/**
 * Interface for getting different similarity metric implementation for
 * miscelangelous descriptor result classes (e.g. atom pair descriptors).
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/07/25 20:43:23 $
 * @see joelib.desc.DistanceMetricValue
 * @see joelib.math.similarity.DistanceMetricHelper
 */
public interface DistanceMetric
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public double getDistance(Object source, Object target);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
