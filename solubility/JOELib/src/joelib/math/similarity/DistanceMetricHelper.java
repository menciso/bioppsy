///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DistanceMetricHelper.java,v $
//  Purpose:  Interface to have a fast method to native descriptor values.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:23 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.math.similarity;

import joelib.desc.DescriptorException;


/**
 * Helper class to load distance metric representation classes using reflection.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/07/25 20:43:23 $
 * @see joelib.desc.DistanceMetricValue
 * @see joelib.math.similarity.DistanceMetric
 */
public class DistanceMetricHelper
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public static DistanceMetric getDistanceMetric(String represention)
        throws DescriptorException
    {
        // try to load distance metric representation class
        DistanceMetric dMetric = null;

        try
        {
            // works only for construtor without arguments
            dMetric = (DistanceMetric) Class.forName(represention).newInstance();

            // for metrics with arguments
            //      Class        cls          = Class.forName(resultRepr);
            //      Constructor  constructor[]  = cls.getDeclaredConstructors();
            //      for (int i = 0; i < constructor.length; i++)
            //      {
            //        Class[]  params  = constructor[i].getParameterTypes();
            //        if (params.length == 1)
            //        {
            //          Object[]  inputs  = {descInfo};
            //          descResult = (DescResult) constructor[i].newInstance(inputs);
            //        }
            //      }
        }
         catch (ClassNotFoundException ex)
        {
            throw new DescriptorException(represention + " not found.");
        }
         catch (InstantiationException ex)
        {
            throw new DescriptorException(represention +
                " can not be instantiated.");
        }
         catch (IllegalAccessException ex)
        {
            throw new DescriptorException(represention + " can't be accessed.");
        }

        //        catch (InvocationTargetException ex)
        //        {
        //            ex.printStackTrace();
        //            throw new DescriptorException("InvocationTargetException.");
        //        }
        if (dMetric == null)
        {
            throw new DescriptorException("Metric class " + represention +
                " does'nt exist.");
        }
        else
        {
            return dMetric;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
