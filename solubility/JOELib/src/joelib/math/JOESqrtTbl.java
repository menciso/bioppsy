///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOESqrtTbl.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2003/08/19 13:11:27 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.math;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Sqrt table.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2003/08/19 13:11:27 $
 */
public class JOESqrtTbl implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private member variables
     *------------------------------------------------------------------------- */
    private double[] _tbl;

    /*-------------------------------------------------------------------------*
     * private member variables
     *------------------------------------------------------------------------- */
    private double _incr;

    /*-------------------------------------------------------------------------*
     * private member variables
     *------------------------------------------------------------------------- */
    private double _max;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the JOESqrtTbl object
     */
    public JOESqrtTbl()
    {
    }

    /**
     *  Constructor for the JOESqrtTbl object
     *
     * @param  max   Description of the Parameter
     * @param  incr  Description of the Parameter
     */
    public JOESqrtTbl(double max, double incr)
    {
        init(max, incr);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     */
    public void finalize()
    {
        if (_tbl != null)
        {
            _tbl = null;
        }
    }

    /**
     *  Description of the Method
     *
     * @param  max   Description of the Parameter
     * @param  incr  Description of the Parameter
     */
    public void init(double max, double incr)
    {
        int i;
        double r;
        _max = max * max;
        _incr = incr;

        //array size needs to be large enough to account for fp error
        _tbl = new double[(int) ((_max / _incr) + 10)];

        for (r = (_incr / 2.0f), i = 0; r <= _max; r += _incr, i++)
        {
            _tbl[i] = Math.sqrt(r);
        }

        _incr = 1 / _incr;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Method
     *
     * @param  d2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public double sqrt(double d2)
    {
        return ((d2 < _max) ? _tbl[(int) (d2 * _incr)] : Math.sqrt(d2));
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
