///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: XYZVector.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/02/20 13:11:57 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.math;

import org.apache.log4j.Category;

/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import joelib.util.JOERandom;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Vector to represent x,y,z coordinates.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/02/20 13:11:57 $
 */
public class XYZVector implements Cloneable, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public static member variables
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Field
     */
    public final static double RAD_TO_DEG = 180.0f / Math.PI;

    /**
     *  Description of the Field
     */
    public final static double DEG_TO_RAD = Math.PI / 180.0f;

    /*-------------------------------------------------------------------------*
     * private static member variables
     *------------------------------------------------------------------------- */

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.math.XYZVector");

    /*-------------------------------------------------------------------------*
     * private member variables
     *------------------------------------------------------------------------- */

    //  The global constant XYZVectors

    /**
     *  Description of the Field
     */
    public final static XYZVector vZero = new XYZVector(0.0f, 0.0f, 0.0f);

    /**
     *  Description of the Field
     */
    public final static XYZVector vX = new XYZVector(1.0f, 0.0f, 0.0f);

    /**
     *  Description of the Field
     */
    public final static XYZVector vY = new XYZVector(0.0f, 1.0f, 0.0f);

    /**
     *  Description of the Field
     */
    public final static XYZVector vZ = new XYZVector(0.0f, 0.0f, 1.0f);

    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Field
     */
    public double _vx;

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Field
     */
    public double _vy;

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Field
     */
    public double _vz;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the XYZVector object
     */
    public XYZVector()
    {
        this(0.0f, 0.0f, 0.0f);
    }

    /**
     *  Constructor for the XYZVector object
     *
     * @param  x  Description of the Parameter
     * @param  y  Description of the Parameter
     * @param  z  Description of the Parameter
     */
    public XYZVector(final double x, final double y, final double z)
    {
        _vx = x;
        _vy = y;
        _vz = z;
    }

    /**
     *  Constructor for the XYZVector object
     *
     * @param  v  Description of the Parameter
     */
    public XYZVector(final XYZVector v)
    {
        //  Copy Constructor
        _vx = v._vx;
        _vy = v._vy;
        _vz = v._vz;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Copy this vector to the vector <tt>v</tt>.
     *
     * @param  v  The new to value
     * @return    Description of the Return Value
     */
    public XYZVector setTo(XYZVector v)
    {
        v._vx = _vx;
        v._vy = _vy;
        v._vz = _vz;

        return v;
    }

    /**
     *  Sets the x attribute of the XYZVector object
     *
     * @param  x  The new x value
     */
    public void setX(final double x)
    {
        _vx = x;
    }

    /**
     *  Sets the y attribute of the XYZVector object
     *
     * @param  y  The new y value
     */
    public void setY(final double y)
    {
        _vy = y;
    }

    /**
     *  Sets the z attribute of the XYZVector object
     *
     * @param  z  The new z value
     */
    public void setZ(final double z)
    {
        _vz = z;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public Object clone()
    {
        return this.get(new XYZVector());
    }

    //  Comparison

    /**
     *  Description of the Method
     *
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public boolean equals(final XYZVector v2)
    {
        return equals(this, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static boolean equals(final XYZVector v1, final XYZVector v2)
    {
        if ((v1._vx == v2._vx) && (v1._vy == v2._vy) && (v1._vz == v2._vz))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //  Destructor

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  to  Description of the Parameter
     * @return     Description of the Return Value
     */
    public XYZVector get(XYZVector to)
    {
        to._vx = _vx;
        to._vy = _vy;
        to._vy = _vz;

        return to;
    }

    /**
     *  Description of the Method
     *
     * @param  c  Description of the Parameter
     */
    public void get(double[] c)
    {
        if (c.length > 3)
        {
            logger.warn("Only elements 0-2 in double array are used.");
        }

        c[0] = _vx;
        c[1] = _vy;
        c[2] = _vz;
    }

    /**
     *  Description of the Method
     *
     * @param  c     Description of the Parameter
     * @param  cidx  Description of the Parameter
     */
    public void get(double[] c, int cidx)
    {
        if ((c.length - 1) < cidx)
        {
            logger.error("Atom coordinate-array length (" + c.length +
                ") smaller than requestd position (" + cidx + ")");
            System.exit(1);
        }

        c[cidx] = _vx;
        c[cidx + 1] = _vy;
        c[cidx + 2] = _vz;
    }

    /**
     *  Description of the Method
     *
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector mul(final XYZVector v1, final XYZVector v2)
    {
        return mul(new XYZVector(), v1, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  v   Description of the Parameter
     * @param  v1  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector mul(XYZVector vv, final XYZVector v,
        final XYZVector v1)
    {
        vv._vx = v1._vx * v._vx;
        vv._vy = v1._vy * v._vy;
        vv._vz = v1._vz * v._vz;

        return vv;
    }

    // create a vector orthogonal to me

    /**
     *  Description of the Method
     *
     * @param  res  Description of the Parameter
     */
    public final void createOrthoXYZVector(XYZVector res)
    {
        XYZVector cO = new XYZVector();

        if ((this.x() == 0.0) && (this.y() == 0.0))
        {
            if (this.z() == 0.0)
            {
                logger.error("makeorthovec zero vector");
                System.exit(0);
            }

            cO.setX(1.0);
        }
        else
        {
            cO.setZ(1.0);
        }

        XYZVector.cross(res, cO, this);
        res.normalize();
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @return     Description of the Return Value
     */
    public final double distSq(final XYZVector vv)
    {
        return (((_vx - vv.x()) * (_vx - vv.x())) +
        ((_vy - vv.y()) * (_vy - vv.y())) + ((_vz - vv.z()) * (_vz - vv.z())));
    }

    /**
     *  Description of the Method
     *
     * @param  c  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector div(final int c)
    {
        return div(new XYZVector(), this, c);
    }

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     * @param  c  Description of the Parameter
     * @return    Description of the Return Value
     */
    public static XYZVector div(final XYZVector v, final int c)
    {
        return div(new XYZVector(), v, c);
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  v   Description of the Parameter
     * @param  c   Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector div(XYZVector vv, final XYZVector v, final double c)
    {
        vv._vx = v._vx / c;
        vv._vy = v._vy / c;
        vv._vz = v._vz / c;

        return vv;
    }

    // vector and matrix ops

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     * @param  m  Description of the Parameter
     * @return    Description of the Return Value
     */
    public static XYZVector mul(final XYZVector v, final Matrix3x3 m)
    {
        return mul(new XYZVector(), v, m);
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  v   Description of the Parameter
     * @param  m   Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector mul(XYZVector vv, final XYZVector v,
        final Matrix3x3 m)
    {
        vv._vx = (v._vx * m.ele[0][0]) + (v._vy * m.ele[0][1]) +
            (v._vz * m.ele[0][2]);
        vv._vy = (v._vx * m.ele[1][0]) + (v._vy * m.ele[1][1]) +
            (v._vz * m.ele[1][2]);
        vv._vz = (v._vx * m.ele[2][0]) + (v._vy * m.ele[2][1]) +
            (v._vz * m.ele[2][2]);

        return vv;
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  m   Description of the Parameter
     * @param  v   Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector mul(XYZVector vv, final Matrix3x3 m,
        final XYZVector v)
    {
        vv._vx = (v._vx * m.ele[0][0]) + (v._vy * m.ele[0][1]) +
            (v._vz * m.ele[0][2]);
        vv._vy = (v._vx * m.ele[1][0]) + (v._vy * m.ele[1][1]) +
            (v._vz * m.ele[1][2]);
        vv._vz = (v._vx * m.ele[2][0]) + (v._vy * m.ele[2][1]) +
            (v._vz * m.ele[2][2]);

        return vv;
    }

    //  Immediate Sum, Difference, Scalar Product

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector adding(final XYZVector v)
    {
        _vx += v._vx;
        _vy += v._vy;
        _vz += v._vz;

        return this;
    }

    /**
     *  Description of the Method
     *
     * @param  f  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector adding(final double[] f)
    {
        _vx += f[0];
        _vy += f[1];
        _vz += f[2];

        return this;
    }

    /**
     *  Description of the Method
     *
     * @param  c  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector diving(final double c)
    {
        _vx /= c;
        _vy /= c;
        _vz /= c;

        return this;
    }

    //  XYZVector Length

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public final double length()
    {
        double l;

        l = Math.sqrt((_vx * _vx) + (_vy * _vy) + (_vz * _vz));

        return l;
    }

    //  XYZVector Length Squared

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public final double length_2()
    {
        double l;

        l = (_vx * _vx) + (_vy * _vy) + (_vz * _vz);

        return l;
    }

    /*-------------------------------------------------------------------------*
     * public static methods
     *------------------------------------------------------------------------- */
    public static double angle(final XYZVector v1, final XYZVector v2)
    {
        return Math.acos(dot(v1, v2) / (Math.sqrt(v1.length() * v2.length()))) * (180 / (Math.PI));
    }

    // calculate angle between vectors

    /**
     *  Description of the Method
     *
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     * @param  c  Description of the Parameter
     * @param  d  Description of the Parameter
     * @return    Description of the Return Value
     */
    public static double calcTorsionAngle(XYZVector a, XYZVector b,
        XYZVector c, XYZVector d)
    {
        double torsion;
        XYZVector b1 = new XYZVector();
        XYZVector b2 = new XYZVector();
        XYZVector b3 = new XYZVector();
        XYZVector c1 = new XYZVector();
        XYZVector c2 = new XYZVector();
        XYZVector c3 = new XYZVector();

        XYZVector.sub(b1, a, b);
        XYZVector.sub(b2, b, c);
        XYZVector.sub(b3, c, d);

        XYZVector.cross(c1, b1, b2);
        XYZVector.cross(c2, b2, b3);
        XYZVector.cross(c3, c1, c2);

        if ((c1.length() * c2.length()) < 0.001)
        {
            torsion = 0.0;
        }
        else
        {
            torsion = XYZVector.xyzVectorAngle(c1, c2);

            if (dot(b2, c3) > 0.0)
            {
                torsion *= -1.0;
            }
        }

        return torsion;
    }

    /**
     *  Description of the Method
     *
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector cross(final XYZVector v1, final XYZVector v2)
    {
        return cross(new XYZVector(), v1, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector cross(XYZVector vv, final XYZVector v1,
        final XYZVector v2)
    {
        vv._vx = (v1._vy * v2._vz) - (v1._vz * v2._vy);
        vv._vy = (-v1._vx * v2._vz) + (v1._vz * v2._vx);
        vv._vz = (v1._vx * v2._vy) - (v1._vy * v2._vx);

        return vv;
    }

    //  Dot Product

    /**
     *  Description of the Method
     *
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static double dot(final XYZVector v1, final XYZVector v2)
    {
        double d;

        d = (v1._vx * v2._vx) + (v1._vy * v2._vy) + (v1._vz * v2._vz);

        return d;
    }

    //  Cross Product
    public XYZVector cross(final XYZVector v2)
    {
        return cross(new XYZVector(), this, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  c  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector muling(final double c)
    {
        _vx *= c;
        _vy *= c;
        _vz *= c;

        return this;
    }

    /**
     *  Description of the Method
     *
     * @param  m  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector muling(final Matrix3x3 m)
    {
        XYZVector vv = new XYZVector();

        vv.setX((_vx * m.get(0, 0)) + (_vy * m.get(0, 1)) +
            (_vz * m.get(0, 2)));
        vv.setY((_vx * m.get(1, 0)) + (_vy * m.get(1, 1)) +
            (_vz * m.get(1, 2)));
        vv.setZ((_vx * m.get(2, 0)) + (_vy * m.get(2, 1)) +
            (_vz * m.get(2, 2)));
        _vx = vv.x();
        _vy = vv.y();
        _vz = vv.z();

        return this;
    }

    //  Member Functions

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public XYZVector normalize()
    {
        double l = length();

        if (l == 0)
        {
            return this;
        }

        _vx = _vx / l;
        _vy = _vy / l;
        _vz = _vz / l;

        return this;
    }

    /**
     *  Description of the Method
     *
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public boolean notEquals(final XYZVector v2)
    {
        return notEquals(this, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static boolean notEquals(final XYZVector v1, final XYZVector v2)
    {
        if ((v1._vx != v2._vx) || (v1._vy != v2._vy) || (v1._vz != v2._vz))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //  Sum, Difference, Scalar Product

    /**
     *  Description of the Method
     *
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public XYZVector add(final XYZVector v2)
    {
        return add(new XYZVector(), this, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector add(final XYZVector v1, final XYZVector v2)
    {
        return add(new XYZVector(), v1, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector add(XYZVector vv, final XYZVector v1,
        final XYZVector v2)
    {
        vv._vx = v1._vx + v2._vx;
        vv._vy = v1._vy + v2._vy;
        vv._vz = v1._vz + v2._vz;

        return vv;
    }

    // create a random unit vector in R3

    /**
     *  Description of the Method
     */
    public void randomUnitXYZVector()
    {
        randomUnitXYZVector(null);
    }

    // create a random unit vector
    // if seed is nonNegative then use this as the seed,
    // otherwise do not seed

    /**
     *  Description of the Method
     *
     * @param  oeRandP  Description of the Parameter
     */
    public void randomUnitXYZVector(JOERandom oeRandP)
    {
        boolean doFree = false;

        if (oeRandP == null)
        {
            doFree = true;
            oeRandP = new JOERandom(1234);
            oeRandP.timeSeed();
        }

        // make sure to sample in the unit sphere
        double f1 = 0.0;

        // make sure to sample in the unit sphere
        double f2 = 0.0;

        // make sure to sample in the unit sphere
        double f3 = 0.0;
        boolean b = false;

        while (!b)
        {
            f1 = oeRandP.nextFloat();
            f2 = oeRandP.nextFloat();
            f3 = oeRandP.nextFloat();

            if (b = ((f1 * f1) + (f2 * f2) + (f3 * f3)) <= 1.0)
            {
                if ((oeRandP.nextInt() % 2) == 0)
                {
                    f1 *= -1.0;
                }

                if ((oeRandP.nextInt() % 2) == 0)
                {
                    f2 *= -1.0;
                }

                if ((oeRandP.nextInt() % 2) == 0)
                {
                    f3 *= -1.0;
                }
            }
        }

        this.set(f1, f2, f3);
        this.normalize();

        if (doFree)
        {
            oeRandP = null;
        }
    }

    //  Assignment

    /**
     *  Description of the Method
     *
     * @param  x  Description of the Parameter
     * @param  y  Description of the Parameter
     * @param  z  Description of the Parameter
     */
    public void set(final double x, final double y, final double z)
    {
        _vx = x;
        _vy = y;
        _vz = z;
    }

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector set(final XYZVector v)
    {
        if (this == v)
        {
            return (this);
        }

        _vx = v._vx;
        _vy = v._vy;
        _vz = v._vz;

        return this;
    }

    /**
     *  Description of the Method
     *
     * @param  c  Description of the Parameter
     */
    public void set(final double[] c)
    {
        if (c.length > 3)
        {
            logger.warn("Only the entries from 0-2 are used in the array.");
        }

        _vx = c[0];
        _vy = c[1];
        _vz = c[2];
    }

    /**
     *  Description of the Method
     *
     * @param  c     Description of the Parameter
     * @param  cidx  Description of the Parameter
     */
    public void set(final double[] c, int cidx)
    {
        if (cidx > (c.length - 3))
        {
            logger.error("Index " + cidx +
                " exceeds coordinate array with length " + c.length);
            _vx = 0.0;
            _vy = 0.0;
            _vz = 0.0;

            return;
        }

        _vx = c[cidx];
        _vy = c[cidx + 1];
        _vz = c[cidx + 2];
    }

    /**
     *  Description of the Method
     *
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public XYZVector sub(final XYZVector v2)
    {
        return sub(new XYZVector(), this, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector sub(final XYZVector v1, final XYZVector v2)
    {
        return sub(new XYZVector(), v1, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector sub(XYZVector vv, final XYZVector v1,
        final XYZVector v2)
    {
        vv._vx = v1._vx - v2._vx;
        vv._vy = v1._vy - v2._vy;
        vv._vz = v1._vz - v2._vz;

        return vv;
    }

    /**
     *  Description of the Method
     *
     * @param  c  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector sub(final int c)
    {
        return sub(new XYZVector(), this, c);
    }

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     * @param  c  Description of the Parameter
     * @return    Description of the Return Value
     */
    public static XYZVector sub(final XYZVector v, final int c)
    {
        return sub(new XYZVector(), v, c);
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  v   Description of the Parameter
     * @param  c   Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector sub(XYZVector vv, final XYZVector v, final int c)
    {
        vv._vx = v._vx - c;
        vv._vy = v._vy - c;
        vv._vz = v._vz - c;

        return vv;
    }

    /**
     *  Description of the Method
     *
     * @param  c  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector mul(final int c)
    {
        return mul(new XYZVector(), this, c);
    }

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     * @param  c  Description of the Parameter
     * @return    Description of the Return Value
     */
    public static XYZVector mul(final XYZVector v, final int c)
    {
        return mul(new XYZVector(), v, c);
    }

    /**
     *  Description of the Method
     *
     * @param  vv  Description of the Parameter
     * @param  v   Description of the Parameter
     * @param  c   Description of the Parameter
     * @return     Description of the Return Value
     */
    public static XYZVector mul(XYZVector vv, final XYZVector v, final double c)
    {
        vv._vx = c * v._vx;
        vv._vy = c * v._vy;
        vv._vz = c * v._vz;

        return vv;
    }

    /**
     *  Description of the Method
     *
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public XYZVector mul(final XYZVector v2)
    {
        return mul(new XYZVector(), this, v2);
    }

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector subing(final XYZVector v)
    {
        _vx -= v._vx;
        _vy -= v._vy;
        _vz -= v._vz;

        return this;
    }

    /**
     *  Description of the Method
     *
     * @param  f  Description of the Parameter
     * @return    Description of the Return Value
     */
    public XYZVector subing(final double[] f)
    {
        _vx -= f[0];
        _vy -= f[1];
        _vz -= f[2];

        return this;
    }

    //  Output
    //  public ObjectOutputStream out ( ObjectOutputStream co, final XYZVector v)
    //  {
    //    co.write(v._vx);
    //    co.write(v._vy);
    //    co.write(v._vz);
    //    return co ;
    //  }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String toString()
    {
        return toString(this);
    }

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     * @return    Description of the Return Value
     */
    public String toString(XYZVector v)
    {
        return new String("< " + _vx + ", " + v._vy + ", " + v._vz + " >");
    }

    //  Access Functions to get
    //    x-coordinate, y-coordinate or
    //    z-coordinate of the vector

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public final double x()
    {
        return _vx;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public final double y()
    {
        return _vy;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public final double z()
    {
        return _vz;
    }

    //  Normalization, Make it a unit XYZVector

    /**
     *  Description of the Method
     *
     * @param  v1  Description of the Parameter
     * @param  v2  Description of the Parameter
     * @return     Description of the Return Value
     */
    public static double xyzVectorAngle(final XYZVector v1, final XYZVector v2)
    {
        double mag;
        double dp;

        mag = v1.length() * v2.length();
        dp = dot(v1, v2) / mag;

        if (dp < -0.999999)
        {
            dp = -0.9999999;
        }

        if (dp > 0.9999999)
        {
            dp = 0.9999999;
        }

        if (dp > 1.0)
        {
            dp = 1.0;
        }

        return ((RAD_TO_DEG * Math.acos(dp)));
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
