///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: External.java,v $
//  Purpose:  Interface definition for calling external programs from JOELib.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:43:16 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.ext;

import joelib.process.JOEProcess;


/**
 *  Interface definition for calling external programs from JOELib.
 *
 * A simple example for calling external programs is the {@link Title2Data} class.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:43:16 $
 * @see Title2Data
 */
public interface External extends JOEProcess
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public String getDescriptionFile();

    public void setExternalInfo(ExternalInfo _info);

    public ExternalInfo getExternalInfo();

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */

    //    public boolean process(JOEMol mol, Hashtable properties);
    public boolean isThisOSsupported();
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
