///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SimpleExternalProcess.java,v $
//  Purpose:  Calls corina to create 3D structures.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:43:16 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
///////////////////////////////////////////////////////////////////////////////
package joelib.ext;

import java.util.Map;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcessException;
import joelib.process.ProcessInfo;

import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/**
 * Simple external process representation.
 *
 * A simple example for calling external programs is the {@link Title2Data} class.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:43:16 $
 * @see Title2Data
 */
public class SimpleExternalProcess implements External
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.ext.SimpleExternalProcess");

    //~ Instance fields ////////////////////////////////////////////////////////

    private ExternalInfo info;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the Corina object
     */
    public SimpleExternalProcess()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the descriptionFile attribute of the Corina object
     *
     * @return    The descriptionFile value
     */
    public String getDescriptionFile()
    {
        return info.getDescriptionFile();
    }

    /**
     *  Sets the externalInfo attribute of the Corina object
     *
     * @param  _info  The new externalInfo value
     */
    public void setExternalInfo(ExternalInfo _info)
    {
        info = _info;
    }

    /**
     *  Gets the externalInfo attribute of the Corina object
     *
     * @return    The externalInfo value
     */
    public ExternalInfo getExternalInfo()
    {
        return info;
    }

    /**
     *  Sets the processInfo attribute of the ProcessPipe object
     *
     * @param  _info  The new processInfo value
     */
    public void setProcessInfo(ProcessInfo _info)
    {
        info.setName(_info.getName());
        info.setRepresentation(_info.getRepresentation());
        info.setDescriptionFile(_info.getDescriptionFile());
    }

    /**
     *  Gets the processInfo attribute of the ProcessPipe object
     *
     * @return    The processInfo value
     */
    public ProcessInfo getProcessInfo()
    {
        return (ProcessInfo) info;
    }

    /**
     *  Gets the thisOSsupported attribute of the Corina object
     *
     * @return    The thisOSsupported value
     */
    public boolean isThisOSsupported()
    {
        if (ExternalHelper.getOperationSystemName().equals(ExternalHelper.OS_LINUX) ||
                ExternalHelper.getOperationSystemName().equals(ExternalHelper.OS_WINDOWS) ||
                ExternalHelper.getOperationSystemName().equals(ExternalHelper.OS_SOLARIS))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public JOEProperty[] acceptedProperties()
    {
        return null;
    }

    public boolean clear()
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean process(JOEMol mol, Map properties)
        throws JOEProcessException
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error(
                "Empty property definition for process or missing property entry.");

            return false;
        }

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
