///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Title2Data.java,v $
//  Purpose:  Calls corina to create 3D structures.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.14 $
//            $Date: 2004/03/15 23:16:16 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.ext;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEGenericData;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;

import joelib.molecule.JOEMol;

import joelib.process.JOEProcessException;

import joelib.util.JHM;
import joelib.util.iterator.GenericDataIterator;


/**
 * Simple example for calling external programs.
 *
 * This class gets the title from a molecule and stores it as descriptor value.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.14 $, $Date: 2004/03/15 23:16:16 $
 * @see External
 */
public class Title2Data extends SimpleExternalProcess
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.ext.Title2Data");

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the Corina object
     */
    public Title2Data()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the thisOSsupported attribute of the Corina object
     *
     * @return    The thisOSsupported value
     */
    public boolean isThisOSsupported()
    {
        if (ExternalHelper.getOperationSystemName().equals(ExternalHelper.OS_LINUX) /*||
            ExternalHelper.getOperationSystemName().equals( ExternalHelper.OS_WINDOWS )*/)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean process(JOEMol mol, Hashtable properties)
        throws JOEProcessException
    {
        super.process(mol, properties);

        // generate executable command line
        Vector argsVector = getExternalInfo().getArguments();

        if ((argsVector == null) || (argsVector.size() == 0))
        {
            logger.error("External " + this.getClass().getName() +
                " not properly defined. See " + getDescriptionFile());

            return false;
        }

        String[] args = new String[argsVector.size() + 1];
        args[0] = getExternalInfo().getExecutable(ExternalHelper.getOperationSystemName());

        for (int i = 0; i < (args.length - 1); i++)
        {
            args[i + 1] = (String) argsVector.get(i);
        }

        // get molecule string
        String molString = toMolString(mol);

        if (molString == null)
        {
            logger.error("Molecule not writeable");

            return false;
        }

        // save old pairdata
        Vector data = new Vector(20);
        GenericDataIterator gdit = mol.genericDataIterator();
        JOEGenericData genericData;

        while (gdit.hasNext())
        {
            genericData = gdit.nextGenericData();

            if ((genericData.getDataType() == JOEDataType.JOE_PAIR_DATA) ||
                    (genericData.getDataType() == JOEDataType.JOE_COMMENT_DATA))
            {
                data.add(genericData);
            }
        }

        // execute title2data
        Process process;
        StringBuffer buffer = new StringBuffer(1000);

        try
        {
            Runtime runtime = Runtime.getRuntime();
            logger.info("Starting: " + args[0] + " " + args[1]);
            process = runtime.exec(args);

            // set input pipe
            BufferedReader in = new BufferedReader(new InputStreamReader(
                        process.getInputStream()));

            // set output pipe
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                        process.getOutputStream()));
            out.write(molString, 0, molString.length());

            //System.out.println(molString);
            out.close();

            // open java output, cpp input pipe
            //      StreamOut output = new StreamOut(process.getOutputStream(), molString);
            //      // kick them off
            //      output.start();
            // wait for extern process termination
            //      System.out.println("wait");
            process.waitFor();

            //      System.out.println("waited");
            // get input pipe data
            String nextLine = null;

            while ((nextLine = in.readLine()) != null)
            {
                buffer.append(nextLine + JHM.eol);

                //        System.out.println("title2data: "+nextLine);
            }
        }
         catch (Exception e)
        {
            logger.error("Could not start executable: " + args[0]);
            e.printStackTrace();

            return false;
        }

        // getting new molecule
        JOEMol tmpMol = new JOEMol(mol);

        // create backup
        IOType inType = mol.getInputType();
        IOType outType = mol.getOutputType();
        mol.clear();

        ByteArrayInputStream sReader = new ByteArrayInputStream(buffer.toString()
                                                                      .getBytes());

        // get molecule loader
        MoleculeFileType loader = null;

        try
        {
            loader = JOEFileFormat.getMolReader(sReader,
                    IOTypeHolder.instance().getIOType("SDF"));
        }
         catch (IOException ex)
        {
            ex.printStackTrace();

            return false;
        }
         catch (MoleculeIOException ex)
        {
            ex.printStackTrace();

            return false;
        }

        if (!loader.readable())
        {
            // should never happen
            logger.error(inType.getRepresentation() + " is not readable.");
            logger.error("You're invited to write one !;-)");

            return false;
        }

        // load molecules and restore old data
        mol.setInputType(inType);
        mol.setOutputType(outType);

        boolean success = true;

        try
        {
            success = loader.read(mol);

            if (!success)
            {
                mol.set(tmpMol);

                return false;
            }
        }
         catch (IOException ex)
        {
            ex.printStackTrace();
            mol.set(tmpMol);

            return false;
        }
         catch (MoleculeIOException ex)
        {
            ex.printStackTrace();

            return false;
        }

        if (mol.empty())
        {
            logger.error("No molecule after " + this.getClass().getName() +
                " execution loaded.");
            mol.set(tmpMol);

            return false;
        }

        // restore old descriptor data
        boolean overwriteProp = true;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);

            if (overwriteProp)
            {
                String attribute = genericData.getAttribute();

                if (!mol.hasData(attribute))
                {
                    mol.addData((JOEGenericData) data.get(i));
                }
            }
            else
            {
                mol.addData((JOEGenericData) data.get(i));
            }
        }

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private String toMolString(JOEMol mol)
    {
        // write without descriptor properties if possible
        // should be faster.
        return mol.toString(IOTypeHolder.instance().getIOType("SDF"), false);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
