///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MolFileChooser.java,v $
//  Purpose:  Connection to the tools of the Chemical Development Kit (CDK).
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:18 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.util;

import java.util.Enumeration;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Category;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;


/**
 *
 * @author     wegnerj
 * @license GPL
 */
public class MolFileChooser
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.gui.util.MolFileChooser");
    private static MolFileChooser instance;

    //~ Instance fields ////////////////////////////////////////////////////////

    private JFileChooser loadChooser;
    private JFileChooser saveChooser;
    private String defaultFileFilter = null;

    //~ Constructors ///////////////////////////////////////////////////////////

    //	private String defaultFileFilter = "SDF";

    /**
     *  Constructor for the CDKTools.
     *
     */
    private MolFileChooser()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        loadChooser = new JFileChooser();
        saveChooser = new JFileChooser();
        init();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public static synchronized MolFileChooser instance()
    {
        if (instance == null)
        {
            instance = new MolFileChooser();
        }

        return instance;
    }

    public JFileChooser getLoadFileChooser()
    {
        return loadChooser;
    }

    public JFileChooser getSaveFileChooser()
    {
        return saveChooser;
    }

    public void init()
    {
        MolFileFilter filter = null;
        IOType ioType;
        FileFilter defaultLoadFilter = loadChooser.getFileFilter();
        FileFilter defaultSaveFilter = saveChooser.getFileFilter();

        for (Enumeration e = IOTypeHolder.instance().getFileTypes();
                e.hasMoreElements();)
        {
            ioType = (IOType) e.nextElement();

            MoleculeFileType mfType = null;

            try
            {
                mfType = JOEFileFormat.getMoleculeFileType(ioType);
            }
             catch (MoleculeIOException ex)
            {
                logger.error(ex.getMessage());
            }

            if (mfType != null)
            {
                if (mfType.readable())
                {
                    filter = new MolFileFilter(ioType, mfType, true, false);
                    loadChooser.setFileFilter(filter);

                    if (defaultFileFilter != null)
                    {
                        if (ioType.equals(IOTypeHolder.instance().getIOType(defaultFileFilter)))
                        {
                            defaultLoadFilter = filter;
                        }
                    }
                }

                if (mfType.writeable())
                {
                    filter = new MolFileFilter(ioType, mfType, false, true);
                    saveChooser.setFileFilter(filter);

                    if (defaultFileFilter != null)
                    {
                        if (ioType.equals(IOTypeHolder.instance().getIOType(defaultFileFilter)))
                        {
                            defaultSaveFilter = filter;
                        }
                    }
                }
            }
        }

        loadChooser.setFileFilter(defaultLoadFilter);
        saveChooser.setFileFilter(defaultSaveFilter);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
