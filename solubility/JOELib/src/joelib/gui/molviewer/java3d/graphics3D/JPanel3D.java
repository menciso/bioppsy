///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JPanel3D.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/02/20 13:11:50 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.graphics3D;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.awt.BorderLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javax.media.j3d.Canvas3D;
import javax.media.j3d.GraphicsConfigTemplate3D;

import javax.swing.JPanel;

import joelib.gui.molviewer.java3d.util.MolViewerEventListener;

import joelib.molecule.JOEMol;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * JPanel for Java3D molecule viewer.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/02/20 13:11:50 $
 */
public class JPanel3D extends JPanel
{
    //~ Instance fields ////////////////////////////////////////////////////////

    Canvas3D moleculeCanvas;
    MolecularScene mScene;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *Constructor for the Panel3D object
     */
    public JPanel3D()
    {
        GraphicsConfigTemplate3D tmpl = new GraphicsConfigTemplate3D();
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = env.getDefaultScreenDevice();
        GraphicsConfiguration config = device.getBestConfiguration(tmpl);

        setLayout(new BorderLayout());

        moleculeCanvas = new Canvas3D(config);
        mScene = new MolecularScene(moleculeCanvas);
        add(moleculeCanvas, BorderLayout.CENTER);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Sets the renderStyle attribute of the Panel3D object
     *
     * @param style  The new renderStyle value
     */
    public void setRenderStyle(int style)
    {
        mScene.setRenderStyle(style);
    }

    public void addMolViewerEventListener(MolViewerEventListener l)
    {
        mScene.addMolViewerEventListener(l);
    }

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Adds a feature to the Molecule attribute of the Panel3D object
     *
     * @param m  The feature to be added to the Molecule attribute
     */
    public void addMolecule(JOEMol mol)
    {
        moleculeCanvas.stopRenderer();
        mScene.addMolecule(mol);
        moleculeCanvas.startRenderer();
    }

    /**
     * Sets the fast attribute of the JPanel3D object
     */

    //    public void setFast()
    //    {
    //        mScene.setFast();
    //    }

    /**
     * Sets the fast attribute of the JPanel3D object
     *
     * Sets the nice attribute of the JPanel3D object
     *
     */

    //    public void setNice()
    //    {
    //        mScene.setNice();
    //    }

    /**
     * Sets the fast attribute of the JPanel3D object
     *
     * Sets the nice attribute of the JPanel3D object
     *
     * Description of the Method
     *
     */
    public void clear()
    {
        mScene.clear();
    }

    /**
     * Remove an answer listener from our list of interested listeners
     */
    public void removeMolViewerEventListener(MolViewerEventListener l)
    {
        mScene.removeMolViewerEventListener(l);
    }

    public void removeMolecule(JOEMol mol)
    {
        moleculeCanvas.stopRenderer();
        mScene.removeMolecule(mol);
        moleculeCanvas.startRenderer();
    }

    public void useAtomPropertyColoring(String atomPropertyName)
    {
        mScene.useAtomPropertyColoring(atomPropertyName);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
