///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MolecularScene.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/02/20 13:11:50 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.graphics3D;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

//import com.sun.j3d.utils.ui.MouseZoom;
//import com.sun.j3d.utils.ui.MouseTranslate;
import javax.media.j3d.*;

import javax.vecmath.*;

/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import com.sun.j3d.utils.behaviors.mouse.*;

import joelib.gui.molviewer.java3d.molecule.*;
import joelib.gui.molviewer.java3d.util.MolViewerEvent;
import joelib.gui.molviewer.java3d.util.MolViewerEventListener;

import joelib.molecule.*;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Description of the Class
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/02/20 13:11:50 $
 */
public class MolecularScene
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /** This field holds a list of registered ActionListeners.
     *  Vector is internally synchronized to prevent race conditions
     * @serial
     */
    protected Vector listeners = new Vector();
    BoundingSphere bounds = null;
    BranchGroup fastGroup;
    BranchGroup localeRoot = null;
    BranchGroup niceGroup;
    BranchGroup root = null;
    Canvas3D canvas;
    Hashtable fastNodes;
    Hashtable molecules = new Hashtable();
    Hashtable niceNodes;
    LinearFog fog = null;
    Locale locale;
    PickHighlightBehavior pickBehFast;
    PickHighlightBehavior pickBehNice;
    RenderTable rTable = RenderTable.getTable();
    Switch sceneSwitch;
    TransformGroup rootTrans = null;
    TransformGroup sceneOffset;
    TransformGroup sceneTrans;
    Vector others = new Vector();
    View view;
    VirtualUniverse universe;
    boolean isFast = false;
    boolean isWire = false;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *Constructor for the MolecularScene object
     *
     * @param c  Description of the Parameter
     */
    public MolecularScene(Canvas3D c)
    {
        this.canvas = c;

        niceNodes = new Hashtable();
        fastNodes = new Hashtable();

        createUniverse();

        initSceneGraph();

        initLighting();

        // Attach the branch graph to the universe, via the Locale.
        // The scene graph is now live!
        root.compile();
        localeRoot.addChild(root);
        locale.addBranchGraph(localeRoot);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Sets the fast attribute of the MolecularScene object
     */
    public void setFast()
    {
        if (!isWire)
        {
            sceneSwitch.setWhichChild(1);
            isFast = true;
        }
    }

    /**
     * Gets the fast attribute of the MolecularScene object
     *
     * @return   The fast value
     */
    public boolean isFast()
    {
        return isFast;
    }

    public Enumeration getMolecules()
    {
        return molecules.elements();
    }

    /**
     * Sets the nice attribute of the MolecularScene object
     */
    public void setNice()
    {
        if (!isWire)
        {
            sceneSwitch.setWhichChild(0);
            isFast = false;
        }
    }

    /**
     * Sets the renderStyle attribute of the MolecularScene object
     *
     * @param style  The new renderStyle value
     */
    public void setRenderStyle(int style)
    {
        canvas.stopRenderer();

        if (style == RenderStyle.WIRE)
        {
            setFast();
            isWire = true;
        }
        else
        {
            isWire = false;
            rTable.setStyle(style);

            if (isFast)
            {
                setNice();
            }
        }

        canvas.startRenderer();

        //if(pickBehNice!=null)pickBehNice.clearHighlight();
        //if(pickBehFast!=null)pickBehFast.clearHighlight();
    }

    /**
     * Adds a feature to the Molecule attribute of the MolecularScene object
     *
     * @param m  The feature to be added to the Molecule attribute
     */
    public void addMolecule(JOEMol mol)
    {
        ViewerMolecule m = new ViewerMolecule(mol);
        molecules.put(mol, m);

        detachRoot();
        centerMolecule(m);
        setBounds(m);

        MoleculeNode node = new MoleculeNode(m);
        niceNodes.put(m, node);

        node.compile();
        niceGroup.addChild(node);

        node = new MoleculeNode(m, RenderStyle.WIRE);
        fastNodes.put(m, node);
        node.compile();
        fastGroup.addChild(node);
        root.compile();
        attachRoot();
    }

    // remove everything

    /**
     * Description of the Method
     */
    public void clear()
    {
        detachRoot();

        // find the child
        Enumeration e = niceNodes.keys();

        while (e.hasMoreElements())
        {
            ViewerMolecule m = (ViewerMolecule) e.nextElement();

            //System.out.println("clear "+m.getName());
            removeMolNode(m, niceNodes, niceGroup);
            removeMolNode(m, fastNodes, fastGroup);

            //removeMolecule(m);
        }

        niceNodes = null;
        fastNodes = null;
        molecules = null;
        RenderTable.getTable().clear();

        //		System.out.println("nice group children: "+niceGroup.numChildren());
        //		System.out.println("fast group children: "+fastGroup.numChildren());
        //		System.out.println("local root children: "+localeRoot.numChildren());
        //		System.out.println("root children: "+root.numChildren());
        niceNodes = new Hashtable();
        fastNodes = new Hashtable();
        molecules = new Hashtable();
        root.compile();
        attachRoot();

        if (pickBehNice != null)
        {
            pickBehNice.clearHighlight();
        }

        if (pickBehFast != null)
        {
            pickBehFast.clearHighlight();
        }
    }

    /**
     * Description of the Method
     *
     * @param m  Description of the Parameter
     */
    public void removeMolecule(JOEMol mol)
    {
        ViewerMolecule m = (ViewerMolecule) molecules.remove(mol);

        removeMolecule(m);
    }

    public void removeMolecule(ViewerMolecule m)
    {
        //        Node n = (Node) niceNodes.get(m);
        if (m == null)
        {
            System.out.println("Could not find node for molecule: " + m);

            return;
        }

        detachRoot();
        removeMolNode(m, niceNodes, niceGroup);
        removeMolNode(m, fastNodes, fastGroup);
        root.compile();
        attachRoot();
    }

    public void useAtomPropertyColoring(String atomPropertyName)
    {
        RenderTable.getTable().atomPropertyName = atomPropertyName;

        //		ViewerMolecule viewerMol;
        //		for (Enumeration e = molecules.elements(); e.hasMoreElements();)
        //		{
        //			viewerMol=(ViewerMolecule)e.nextElement();
        //			System.out.println("SET: "+viewerMol.getName()+" "+atomPropertyName);
        //			viewerMol.getAtomPropertyColoring().useAtomPropertyColoring(viewerMol.getJOEMol(), atomPropertyName);
        //		}
    }

    /**
     * Register an action listener to be notified when a button is pressed
     */
    protected void addMolViewerEventListener(MolViewerEventListener l)
    {
        listeners.addElement(l);
    }

    /**
     * Send an event to all registered listeners
     */
    protected void fireEvent(MolViewerEvent e)
    {
        // Make a copy of the list and fire the events using that copy.
        // This means that listeners can be added or removed from the original
        // list in response to this event.  We ought to be able to just use an
        // enumeration for the vector, but that doesn't copy the list internally.
        Vector list = (Vector) listeners.clone();

        for (int i = 0; i < list.size(); i++)
        {
            MolViewerEventListener listener = (MolViewerEventListener) list.elementAt(i);

            switch (e.getType())
            {
            case MolViewerEvent.REPLACE_MOLECULE:
                listener.centralDisplayChange(e);

                break;

            case MolViewerEvent.ATOM_PICKED:
                listener.atomPicked(e);

                break;

            case MolViewerEvent.BOND_PICKED:
                listener.bondPicked(e);

                break;
            }
        }
    }

    /**
     * Remove an answer listener from our list of interested listeners
     */
    protected void removeMolViewerEventListener(MolViewerEventListener l)
    {
        listeners.removeElement(l);
    }

    void setBounds(JOEMol mol)
    {
        ViewerMolecule m = (ViewerMolecule) molecules.get(mol);

        setBounds(m);
    }

    void setBounds(ViewerMolecule m)
    {
        float x = m.getXmax() - m.getXmin();
        float y = m.getYmax() - m.getYmin();
        float ex = Math.max(x, y);

        //      System.out.println("ex: "+ex+" "+m.getZmin()+" "+m.getZmax());
        float backPlane = (m.getZmax() > 150.0f) ? (m.getZmax() + 50.0f) : 150.0f;
        view.setBackClipDistance(backPlane);

        fog.setFrontDistance(0.5f * (m.getZmin() + m.getZmax()));
        fog.setBackDistance(backPlane);

        Transform3D t = new Transform3D();
        t.set(new Vector3f(0.0f, 0.0f, (m.getZmin() - (2.0f * ex))));

        //t.setScale(15.0f/ex);
        sceneOffset.setTransform(t);
    }

    // Can be removed from code

    /**
     * Adds a feature to the Node attribute of the MolecularScene object
     *
     * @param node  The feature to be added to the Node attribute
     */
    void addNode(Node node)
    {
        BranchGroup b = new BranchGroup();
        b.addChild(node);
        detachRoot();
        others.addElement(node);
        niceGroup.addChild(b);
        root.compile();
        attachRoot();
    }

    /**
     * Description of the Method
     */
    void attachRoot()
    {
        localeRoot.addChild(root);
    }

    void centerMolecule(JOEMol mol)
    {
        ViewerMolecule m = (ViewerMolecule) molecules.get(mol);

        centerMolecule(m);
    }

    /**
     * Description of the Method
     *
     * @param m  Description of the Parameter
     */
    void centerMolecule(ViewerMolecule m)
    {
        m.findBB();

        float cenX = 0.5f * (m.getXmin() + m.getXmax());
        float cenY = 0.5f * (m.getYmin() + m.getYmax());
        float cenZ = 0.5f * (m.getZmin() + m.getZmax());
        ViewerAtoms av = m.getMyAtoms();
        int nAtoms = av.size();

        for (int i = 0; i < nAtoms; i++)
        {
            ViewerAtom a = av.getAtom(i);
            a.setX(a.getX() - cenX);
            a.setY(a.getY() - cenY);
            a.setZ(a.getZ() - cenZ);
        }

        m.findBB();
    }

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Description of the Method
     */
    void createUniverse()
    {
        // Establish a virtual universe, with a single hi-res Locale
        universe = new VirtualUniverse();
        locale = new Locale(universe);

        // Create a PhysicalBody and Physical Environment object
        PhysicalBody body = new PhysicalBody();
        PhysicalEnvironment environment = new PhysicalEnvironment();

        // Create a View and attach the Canvas3D and the physical
        // body and environment to the view.
        view = new View();
        view.addCanvas3D(canvas);
        view.setPhysicalBody(body);
        view.setPhysicalEnvironment(environment);
        view.setBackClipDistance(500.0);

        //System.out.println("Clip front/back: "+view.getFrontClipDistance()+"/"+
        //			 view.getBackClipDistance());
        // Create a branch group node for the view platform
        root = new BranchGroup();
        localeRoot = new BranchGroup();
        localeRoot.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        localeRoot.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        localeRoot.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        root.setCapability(BranchGroup.ALLOW_DETACH);
        root.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        root.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        root.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);

        ViewPlatform vp = new ViewPlatform();
        rootTrans = new TransformGroup();

        bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 1000.0);

        rootTrans.addChild(vp);

        BoundingLeaf boundingLeaf = new BoundingLeaf(bounds);
        rootTrans.addChild(boundingLeaf);
        localeRoot.addChild(rootTrans);
        view.attachViewPlatform(vp);
    }

    /**
     * Description of the Method
     */
    void detachRoot()
    {
        root.detach();
    }

    /**
     * Description of the Method
     */
    void initLighting()
    {
        Color3f dlColor = new Color3f(1.0f, 1.0f, 1.0f);

        Vector3f lDirect1 = new Vector3f(1.0f, -1.0f, -1.0f);
        Vector3f lDirect2 = new Vector3f(-1.0f, 1.0f, 1.0f);

        DirectionalLight lgt1 = new DirectionalLight(dlColor, lDirect1);
        lgt1.setInfluencingBounds(bounds);

        DirectionalLight lgt2 = new DirectionalLight(dlColor, lDirect2);
        lgt2.setInfluencingBounds(bounds);

        Color3f alColor = new Color3f(0.65f, 0.65f, 0.65f);
        AmbientLight aLgt = new AmbientLight(alColor);
        aLgt.setInfluencingBounds(bounds);

        root.addChild(aLgt);
        root.addChild(lgt1);
        root.addChild(lgt2);

        // Try some fog for depth cueing
        fog = new LinearFog();
        fog.setCapability(LinearFog.ALLOW_DISTANCE_WRITE);
        fog.setColor(new Color3f(0.0f, 0.0f, 0.0f));
        fog.setFrontDistance(70.0);
        fog.setBackDistance(500.0);

        //fog.setInfluencingBounds(bounds);
        //fog.addScope(sceneTrans);
        //sceneTrans.addChild(fog);
    }

    /**
     * Description of the Method
     */
    void initSceneGraph()
    {
        Transform3D t = new Transform3D();
        t.set(new Vector3f(0.0f, 0.0f, -100.0f));
        sceneOffset = new TransformGroup(t);
        sceneOffset.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        sceneOffset.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

        sceneTrans = new TransformGroup();
        sceneTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        sceneTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

        sceneSwitch = new Switch(Switch.CHILD_MASK);
        sceneSwitch.setCapability(Switch.ALLOW_SWITCH_READ);
        sceneSwitch.setCapability(Switch.ALLOW_SWITCH_WRITE);

        fastGroup = new BranchGroup();
        niceGroup = new BranchGroup();
        fastGroup.setCapability(Group.ALLOW_CHILDREN_EXTEND);
        fastGroup.setCapability(Group.ALLOW_CHILDREN_READ);
        fastGroup.setCapability(Group.ALLOW_CHILDREN_WRITE);

        niceGroup.setCapability(Group.ALLOW_CHILDREN_EXTEND);
        niceGroup.setCapability(Group.ALLOW_CHILDREN_READ);
        niceGroup.setCapability(Group.ALLOW_CHILDREN_WRITE);

        sceneSwitch.addChild(niceGroup);
        sceneSwitch.addChild(fastGroup);
        sceneSwitch.setWhichChild(0);

        // Create the drag behavior node
        MouseFastRotate behavior = new MouseFastRotate(sceneTrans, this);
        behavior.setSchedulingBounds(bounds);
        sceneTrans.addChild(behavior);

        // Create the zoom behavior node
        MouseZoom behavior2 = new MouseZoom(sceneTrans);
        behavior2.setSchedulingBounds(bounds);
        sceneTrans.addChild(behavior2);

        // Create the zoom behavior node
        MouseTranslate behavior3 = new MouseTranslate(sceneTrans);
        behavior3.setSchedulingBounds(bounds);
        sceneTrans.addChild(behavior3);

        // Now create the simple picking behavior
        //pickBehFast = new PickHighlightBehavior(this, canvas, fastGroup, bounds);
        pickBehNice = new PickHighlightBehavior(this, canvas, niceGroup, bounds);

        sceneTrans.addChild(sceneSwitch);

        sceneOffset.addChild(sceneTrans);
        rootTrans.addChild(sceneOffset);
    }

    /**
     * Description of the Method
     *
     * @param m      Description of the Parameter
     * @param nodes  Description of the Parameter
     * @param group  Description of the Parameter
     */
    void removeMolNode(ViewerMolecule m, Hashtable nodes, BranchGroup group)
    {
        Node n = (Node) nodes.get(m);
        int nc = group.numChildren();

        for (int i = 0; i < nc; i++)
        {
            if (group.getChild(i) == n)
            {
                //System.out.println("remove "+m.getName());
                group.removeChild(i);
                nodes.remove(m);

                return;
            }
        }

        m.clear();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
