///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeNode.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/02/20 13:11:50 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.graphics3D;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.Vector;

import javax.media.j3d.BranchGroup;
import javax.media.j3d.TransformGroup;

import joelib.gui.molviewer.java3d.molecule.ViewerAtoms;
import joelib.gui.molviewer.java3d.molecule.ViewerBonds;
import joelib.gui.molviewer.java3d.molecule.ViewerMolecule;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Description of the Class
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/02/20 13:11:50 $
 */
public class MoleculeNode extends BranchGroup implements RenderStyle
{
    //~ Instance fields ////////////////////////////////////////////////////////

    BranchGroup atomGroup;
    BranchGroup bondGroup;
    TransformGroup moleculeTrans;
    Vector atoms;
    Vector bonds;

    // Maybe also others
    ViewerMolecule myMolecule;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *Constructor for the MoleculeNode object
     */
    public MoleculeNode()
    {
        super();
        setCapability(BranchGroup.ALLOW_DETACH);
        buildRoot();
    }

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *Constructor for the MoleculeNode object
     *
     * @param m  Description of the Parameter
     */
    protected MoleculeNode(ViewerMolecule m)
    {
        this(m, RenderStyle.CPK);
    }

    /**
     *Constructor for the MoleculeNode object
     *
     * @param m      Description of the Parameter
     * @param style  Description of the Parameter
     */
    protected MoleculeNode(ViewerMolecule m, int style)
    {
        this();
        myMolecule = m;

        if (style != RenderStyle.WIRE)
        {
            atoms = new Vector();
            bonds = new Vector();

            ViewerAtoms v = m.getMyAtoms();
            int s = v.size();

            for (int i = 0; i < s; i++)
            {
                AtomNode a = new AtomNode(v.getAtom(i));
                addAtomNode(a);
            }

            ViewerBonds b = m.getMyBonds();
            s = b.size();

            for (int i = 0; i < s; i++)
            {
                BondNode bn = new BondNode(b.getBond(i));
                addBondNode(bn);
            }
        }
        else
        {
            ViewerBonds b = m.getMyBonds();
            int s = b.size();

            for (int i = 0; i < s; i++)
            {
                bondGroup.addChild(BondNode.createWire(b.getBond(i)));
            }
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Sets the style attribute of the MoleculeNode object
     *
     * @param style  The new style value
     */
    public void setStyle(int style)
    {
    }

    /**
     * Adds a feature to the AtomNode attribute of the MoleculeNode object
     *
     * @param a  The feature to be added to the AtomNode attribute
     */
    public void addAtomNode(AtomNode a)
    {
        atomGroup.addChild(a);
        atoms.addElement(a);
    }

    /**
     * Adds a feature to the BondNode attribute of the MoleculeNode object
     *
     * @param b  The feature to be added to the BondNode attribute
     */
    public void addBondNode(BondNode b)
    {
        bondGroup.addChild(b);
        bonds.addElement(b);
    }

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Description of the Method
     */
    void buildRoot()
    {
        //moleculeRoot = new BranchGroup();
        //Transform3D t = new Transform3D();
        //t.set(new Vector3f(0.0f, 0.0f, 20.0f));
        // This will allow to transform individual molecules
        moleculeTrans = new TransformGroup();
        moleculeTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

        //BoundingSphere bounds =
        // new BoundingSphere(new Point3d(0.0,0.0,0.0), 500.0);
        // Create the behavior node
        //        DragBehavior behavior = new DragBehavior(moleculeTrans);
        //        behavior.setSchedulingBounds(bounds);
        //        moleculeTrans.addChild(behavior);
        //moleculeRoot.addChild(moleculeTrans);
        addChild(moleculeTrans);

        // currently things are sorted by atoms and bonds
        atomGroup = new BranchGroup();
        bondGroup = new BranchGroup();
        moleculeTrans.addChild(atomGroup);
        moleculeTrans.addChild(bondGroup);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
