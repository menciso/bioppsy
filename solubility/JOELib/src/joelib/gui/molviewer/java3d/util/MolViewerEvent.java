///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MolViewerEvent.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2004/02/20 13:11:51 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.util;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.EventObject;

import joelib.molecule.*;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Description of the Class
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/02/20 13:11:51 $
 */
public class MolViewerEvent extends EventObject
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Description of the Field
     */
    public final static int REPLACE_MOLECULE = 0;

    /**
     * Description of the Field
     */
    public final static int ATOM_PICKED = 1;

    /**
     * Description of the Field
     */
    public final static int BOND_PICKED = 2;

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     * Description of the Field
     */
    protected Object param = null;

    /**
     * Description of the Field
     */
    protected int eventType;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *Constructor for the MolViewerEvent object
     *
     * @param source  Description of the Parameter
     * @param what    Description of the Parameter
     * @param param   new JOEMol molecule
     */
    public MolViewerEvent(Object source, int what, JOEMol param)
    {
        this(source, what);
        this.param = param;
    }

    public MolViewerEvent(Object source, int what, JOEAtom param)
    {
        this(source, what);
        this.param = param;
    }

    public MolViewerEvent(Object source, int what, JOEBond param)
    {
        this(source, what);
        this.param = param;
    }

    /**
     *Constructor for the MolViewerEvent object
     *
     * @param source  Description of the Parameter
     * @param what    Description of the Parameter
     */
    public MolViewerEvent(Object source, int what)
    {
        super(source);
        eventType = what;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the param attribute of the MolViewerEvent object
     *
     * @return   The param value
     */
    public Object getParam()
    {
        return param;
    }

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Gets the type attribute of the MolViewerEvent object
     *
     * @return   The type value
     */
    public int getType()
    {
        return eventType;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
