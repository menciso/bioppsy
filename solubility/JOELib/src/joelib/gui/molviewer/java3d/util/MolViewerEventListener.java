///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MolViewerEventListener.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.3 $
//            $Date: 2003/08/19 13:11:26 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.util;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.EventListener;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Description of the Interface
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.3 $, $Date: 2003/08/19 13:11:26 $
 */
public interface MolViewerEventListener extends EventListener
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public void atomPicked(MolViewerEvent event);

    public void bondPicked(MolViewerEvent event);

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */
    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Description of the Method
     *
     * @param event  Description of the Parameter
     */
    public void centralDisplayChange(MolViewerEvent event);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
