///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: CentralDisplayAdapter.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2003/08/19 13:11:26 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.util;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import joelib.gui.molviewer.java3d.graphics3D.*;
import joelib.gui.molviewer.java3d.molecule.ViewerMolecule;

import joelib.molecule.*;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Description of the Class
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2003/08/19 13:11:26 $
 */
public class CentralDisplayAdapter extends MolViewerEventAdapter
{
    //~ Instance fields ////////////////////////////////////////////////////////

    JPanel3D myPanel;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *Constructor for the CentralDisplayAdapter object
     *
     * @param panel  Description of the Parameter
     */
    public CentralDisplayAdapter(JPanel3D panel)
    {
        myPanel = panel;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Description of the Method
     *
     * @param event  Description of the Parameter
     */
    public void centralDisplayChange(MolViewerEvent event)
    {
        if (event.getType() == MolViewerEvent.REPLACE_MOLECULE)
        {
            myPanel.clear();
            myPanel.addMolecule((JOEMol) event.getParam());
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
