///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MolViewerEventAdapter.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/02/20 13:11:51 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.util;

import org.apache.log4j.Category;

/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import joelib.data.JOEElementTable;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Description of the Class
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/02/20 13:11:51 $
 */
public class MolViewerEventAdapter implements MolViewerEventListener
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.gui.molviewer.java3d.util.MolViewerEventAdapter");

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *Constructor for the MolViewerEventAdapter object
     */
    public MolViewerEventAdapter()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void atomPicked(MolViewerEvent event)
    {
        if (logger.isDebugEnabled())
        {
            StringBuffer sb = new StringBuffer(100);
            JOEAtom atom = (JOEAtom) event.getParam();
            sb.append("atom idx:" + atom.getIdx());
            sb.append(", atomic number:" + atom.getAtomicNum());
            sb.append(", element symbol:" +
                JOEElementTable.instance().getSymbol(atom.getAtomicNum()));
            sb.append(", aromatic flag:" + atom.isAromatic());
            sb.append(", atom vector:" + atom.getVector());
            sb.append(", hybridisation:" + atom.getHyb());
            sb.append(", implicit valence:" + atom.getImplicitValence());
            sb.append(", charge:" + atom.getFormalCharge());
            sb.append(", partial charge:" + atom.getPartialCharge());
            sb.append(", valence:" + atom.getValence());
            sb.append(", ext Electrons:" +
                JOEElementTable.instance().getExteriorElectrons(atom.getAtomicNum()));
            sb.append(", pauling electronegativity:" + atom.getENPauling());
            sb.append(", free electrons:" + atom.getFreeElectrons());
            logger.debug(sb.toString());
        }

        //System.out.println("Atom: " + event.getParam());
    }

    public void bondPicked(MolViewerEvent event)
    {
        if (logger.isDebugEnabled())
        {
            StringBuffer sb = new StringBuffer(100);
            JOEBond bond = (JOEBond) event.getParam();
            sb.append("  atom #");
            sb.append(bond.getBeginAtomIdx());
            sb.append(" is attached to atom #");
            sb.append(bond.getEndAtomIdx());
            sb.append(" with bond of order ");
            sb.append(bond.getBO());
            sb.append(" which is ");

            if (bond.isAromatic())
            {
                sb.append("an aromatic bond.");
            }
            else
            {
                sb.append("a non-aromatic bond.");
            }

            logger.debug(sb.toString());
        }
    }

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Description of the Method
     *
     * @param event  Description of the Parameter
     */
    public void centralDisplayChange(MolViewerEvent event)
    {
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
