///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Angle.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2003/08/19 13:11:26 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.math.geometry;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * A class that provides mathematical definition of geometric angle
 *
 * @author    Zhidong Xie (zxie@tripos.com)
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2003/08/19 13:11:26 $
 */
public class Angle
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     * error/warning message, or other comment
     */
    protected String comment = null;

    /**
     * value of the angle
     */
    protected double value = 0.0;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     * default constructor: value is 0.0;
     */
    public Angle()
    {
    }

    /**
     * full constructor
     *
     * @param inDegree  boolean flag indicating value in degree unit
     * @param value     Description of the Parameter
     */
    public Angle(double value, boolean inDegree)
    {
        this.value = (inDegree) ? (value / 180.0 * Math.PI) : value;
    }

    /**
     * constructor
     *
     * @param value  Description of the Parameter
     */
    public Angle(double value)
    {
        this(value, false);
    }

    /**
     * copy constructor
     *
     * @param angle  Description of the Parameter
     */
    public Angle(Angle angle)
    {
        value = angle.value;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Set comment
     *
     * @param comment
     */
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    /**
     * Return comment
     *
     * @return   The comment value
     */
    public String getComment()
    {
        return comment;
    }

    /**
     * Set value of the angle
     *
     * @param value     angle's value to be set
     * @param inDegree  true if value is in degree unit, false if in rad unit
     */
    public void setValue(double value, boolean inDegree)
    {
        this.value = (inDegree) ? (value / 180.0 * Math.PI) : value;
    }

    /**
     * Set value of the angle
     *
     * @param value  angle's value to be set, in rad unit
     */
    public void setValue(double value)
    {
        this.setValue(value, false);
    }

    /**
     * Return angle value in degree unit
     *
     * @return   Description of the Return Value
     */
    public double degreeValue()
    {
        return ((180.0 * value) / Math.PI);
    }

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Return angle value in rad unit
     *
     * @return   Description of the Return Value
     */
    public double radValue()
    {
        return value;
    }

    /**
     * Return string representation of angle
     *
     * @return   Description of the Return Value
     */
    public String toString()
    {
        return Float.toString((float) degreeValue());
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
