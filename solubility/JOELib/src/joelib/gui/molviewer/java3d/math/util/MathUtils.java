///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MathUtils.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2003/08/19 13:11:26 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.math.util;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * This class provides some math utility methods.
 *
 * @author    Mike Brusati
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2003/08/19 13:11:26 $
 */
public class MathUtils
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */
    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Perform a division of the input integers, and round to the next integer
     * if the divisor is not a even multiple of the dividend.
     *
     * @param dividend  the number to be divided
     * @param divisor   the number by which to divide
     * @return          the result of the division, with possible rounding
     */
    public static int divideAndRound(int dividend, int divisor)
    {
        if (divisor == 0)
        {
            return 0;
        }

        return ((dividend % divisor) == 0) ? (dividend / divisor)
                                           : ((dividend / divisor) + 1);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
