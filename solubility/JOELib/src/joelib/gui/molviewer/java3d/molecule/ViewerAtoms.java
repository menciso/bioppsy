///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ViewerAtoms.java,v $
//  Purpose:  Molecule class for Java3D viewer.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.3 $
//            $Date: 2003/08/19 13:11:26 $
//            $Author: wegner $
//  Original Author: Jason Plurad (jplurad@tripos.com),
//                   Mike Brusati (brusati@tripos.com)
//                   Zhidong Xie (zxie@tripos.com)
//  Original Version: ftp.tripos.com/pub/java3d/
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.molecule;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.Vector;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Atom vector class for Java3D viewer.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.3 $, $Date: 2003/08/19 13:11:26 $
 */
public class ViewerAtoms extends Vector
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     * Default constructor
     */
    public ViewerAtoms()
    {
        super();
    }

    /**
     * Capacity constructor
     *
     * @param cap  initial capacity of vector
     */
    public ViewerAtoms(int cap)
    {
        super(cap, cap);
    }

    /**
     * Capacity and increment constructor
     *
     * @param cap  initial capacity of vector
     * @param inc  increment factor
     */
    public ViewerAtoms(int cap, int inc)
    {
        super(cap, inc);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Returns atom at specified index
     *
     * @param i                                   index of atom
     * @return                                    The atom value
     * @exception ArrayIndexOutOfBoundsException  if index >= capacity()
     */
    public final synchronized ViewerAtom getAtom(int i)
        throws ArrayIndexOutOfBoundsException
    {
        return (ViewerAtom) elementAt(i);
    }

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Append an atom to the end of vector
     *
     * @param a  atom to be appended
     */
    public final synchronized void append(ViewerAtom a)
    {
        addElement(a);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
