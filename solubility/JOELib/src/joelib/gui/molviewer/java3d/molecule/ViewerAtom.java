///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ViewerAtom.java,v $
//  Purpose:  Molecule class for Java3D viewer.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/02/20 13:11:51 $
//            $Author: wegner $
//  Original Author: Jason Plurad (jplurad@tripos.com),
//                   Mike Brusati (brusati@tripos.com)
//                   Zhidong Xie (zxie@tripos.com)
//  Original Version: ftp.tripos.com/pub/java3d/
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d.molecule;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.LinkedList;

import joelib.data.JOEElementTable;

import joelib.molecule.JOEAtom;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Atom class for Java3D viewer.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/02/20 13:11:51 $
 */
public class ViewerAtom
{
    //~ Instance fields ////////////////////////////////////////////////////////

    public LinkedList shapes = new LinkedList();

    /**
     * Description of the Field
     */
    protected JOEAtom atom;

    /**
     * Flag indicating if atom label should be displayed
     */
    protected boolean display = true;

    /**
     * Flag indicating if atom is highlighted
     */
    protected boolean highlight = false;

    /**
     * Flag indicating if atom is selcted
     */
    protected boolean select = false;

    /**
     * Transformed atom coordinates
     */
    protected float tx;

    /**
     * Transformed atom coordinates
     */
    protected float ty;

    /**
     * Transformed atom coordinates
     */
    protected float tz;
    private ViewerMolecule parent;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *Constructor for the ViewerAtom object
     *
     * @param _atom  Description of the Parameter
     */
    public ViewerAtom(ViewerMolecule _parent, JOEAtom _atom)
    {
        atom = _atom;
        parent = _parent;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Return true if the atom is a carbon atom
     *
     * @return   The carbon value
     */
    public boolean isCarbon()
    {
        return atom.isCarbon();
    }

    /**
     * Returns the atomic charge
     *
     * @return   The charge value
     */
    public int getCharge()
    {
        return atom.getFormalCharge();
    }

    /**
     * Set the state to indicate whether the atom is to be formally displayed
     *
     * @param displayOrNot  highlight state of the atom
     */
    public void setDisplay(boolean displayOrNot)
    {
        display = displayOrNot;
    }

    /**
     * Set the state to indicate whether the atom is hightlighted
     *
     * @param highlightOrNot  highlight state of the atom
     */
    public void setHighlight(boolean highlightOrNot)
    {
        highlight = highlightOrNot;
    }

    /**
     * Returns true if the atom is highlighted by user
     *
     * @return   The highlighted value
     */
    public boolean isHighlighted()
    {
        return highlight;
    }

    /**
     * Return true if the atom is a hydrogen atom
     *
     * @return   The hydrogen value
     */
    public boolean isHydrogen()
    {
        return atom.isHydrogen();
    }

    /**
     * Compares this atom with another
     *
     * @return   <tt>true</tt> if atoms are equal, else <tt>false</tt>
     */

    //  public boolean equals( Atom a ) {
    //    if ( a == null )
    //      return false;
    //
    //    return super.equals( a );
    //  }

    /**
     * Compares this atom with another
     *
     * Returns String representation of Atom
     *
     * @return   <tt>true</tt> if atoms are equal, else <tt>false</tt>
     */

    //  public String toString() {
    //    return ( id + " " + name + " " + x + " " + y + " " + z + " " +
    //	         tx + " " + ty + " " + tz + " " + charge
    //                 );
    //  }

    /**
     * Compares this atom with another
     *
     * Returns String representation of Atom
     *
     * Returns id of the atom
     *
     * @return   <tt>true</tt> if atoms are equal, else <tt>false</tt>
     */
    public int getId()
    {
        return atom.getIdx() - 1;
    }

    public JOEAtom getJOEAtom()
    {
        return atom;
    }

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Returns the elemental name
     *
     * @return   The name value
     */
    public String getName()
    {
        return JOEElementTable.instance().getSymbol(atom.getAtomicNum());
    }

    public ViewerMolecule getParent()
    {
        return parent;
    }

    /**
     * Set the state to indicate whether the atom is selected
     *
     * @param selectOrNot  select state of the atom
     */
    public void setSelect(boolean selectOrNot)
    {
        select = selectOrNot;
    }

    /**
     * Gets the colorFromType attribute of the ViewerAtom object
     *
     * @return   The colorFromType value
     */

    //    public Color getColorFromType()
    //    {
    //        int atomNum = JOEElementTable.instance().getAtomicNum(this.getName());
    //        
    //        Color color =this.parent.getAtomPropertyColoring().getAtomColor(atom);
    //        
    //        //Color color = JOEElementTable.instance().getColor(atomNum);
    //
    //        return color;
    //    }

    /**
     * Returns true if the atom is selected by user
     *
     * @return   The selected value
     */
    public boolean isSelected()
    {
        return select;
    }

    /**
     * Set the transformed x coordinate of the atom
     *
     * @param tx  transformed X-coordinate of the atom
     */
    public void setTx(float tx)
    {
        this.tx = tx;
    }

    /**
     * Returns the transformed x coordinate of the atom
     *
     * @return   The tx value
     */
    public float getTx()
    {
        return tx;
    }

    /**
     * Set the transformed y coordinate of the atom
     *
     * @param ty  transformed Y-coordinate of the atom
     */
    public void setTy(float ty)
    {
        this.ty = ty;
    }

    /**
     * Returns the transformed y coordinate of the atom
     *
     * @return   The ty value
     */
    public float getTy()
    {
        return ty;
    }

    /**
     * Returns SYBYL force field atom type
     *
     * @return   The type value
     */
    public String getType()
    {
        return JOEElementTable.instance().getSymbol(atom.getAtomicNum());
    }

    /**
     * Set the transformed z coordinate of the atom
     *
     * @param tz  transformed Z-coordinate of the atom
     */
    public void setTz(float tz)
    {
        this.tz = tz;
    }

    /**
     * Returns the transformed z coordinate of the atom
     *
     * @return   The tz value
     */
    public float getTz()
    {
        return tz;
    }

    /**
     * Set the atom id
     *
     * @param x   The new x value
     */

    //  public void  setId( int id ) { this.id = id; }

    /**
     * Set the atom id
     *
     * Set the x coordinate of the atom
     *
     * @param x   The new x value
     */
    public void setX(float x)
    {
        atom.setVector((double) x, atom.getY(), atom.getZ());
    }

    /**
     * Returns the x coordinate of the atom
     *
     * @return   The x value
     */
    public float getX()
    {
        return (float) atom.getX();
    }

    /**
     * Set the y coordinate of the atom
     *
     * @param y  Y-coordinate of the atom
     */
    public void setY(float y)
    {
        atom.setVector(atom.getX(), (double) y, atom.getZ());
    }

    /**
     * Returns the y coordinate of the atom
     *
     * @return   The y value
     */
    public float getY()
    {
        return (float) atom.getY();
    }

    /**
     * Set the z coordinate of the atom
     *
     * @param z  Z-coordinate of the atom
     */
    public void setZ(float z)
    {
        atom.setVector(atom.getX(), atom.getY(), (double) z);
    }

    /**
     * Returns the z coordinate of the atom
     *
     * @return   The z value
     */
    public float getZ()
    {
        return (float) atom.getZ();
    }

    /**
     * Return true if the atom need to be display
     *
     * @return   Description of the Return Value
     */
    public boolean needDisplay()
    {
        return display;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
