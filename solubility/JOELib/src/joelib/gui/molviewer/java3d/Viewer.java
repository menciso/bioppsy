///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Viewer.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/02/20 13:11:49 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.molviewer.java3d;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.UIManager;

import org.apache.log4j.Category;

import joelib.gui.molviewer.java3d.util.Java3DHelper;

import joelib.molecule.JOEMol;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Description of the Class
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/02/20 13:11:49 $
 */
public class Viewer
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.gui.molviewer.java3d.Viewer");

    /**
     * Description of the Field
     */
    public static Viewer viewer;

    //~ Instance fields ////////////////////////////////////////////////////////

    boolean packFrame = false;
    private ViewerFrame frame;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *Construct the application
     */
    public Viewer()
    {
        this(null, null);
    }

    public Viewer(String inputFile)
    {
        this(null, inputFile);
    }

    /**
     *Constructor for the Viewer object
     *
     * @param type       Description of the Parameter
     * @param inputFile  Description of the Parameter
     */
    public Viewer(String type, String inputFile)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        frame = new ViewerFrame(type, inputFile);

        //Validate frames that have preset sizes
        //Pack frames that have useful preferred size info, e.g. from their layout
        if (packFrame)
        {
            frame.pack();
        }
        else
        {
            frame.validate();
        }

        //Center the window
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = frame.getSize();

        if (frameSize.height > screenSize.height)
        {
            frameSize.height = screenSize.height;
        }

        if (frameSize.width > screenSize.width)
        {
            frameSize.width = screenSize.width;
        }

        frame.setLocation((screenSize.width - frameSize.width) / 2,
            (screenSize.height - frameSize.height) / 2);
        frame.setVisible(true);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *------------------------------------------------------------------------- */

    /**
     * Description of the Method
     *
     * @return   Description of the Return Value
     */
    public static synchronized Viewer instance()
    {
        if (viewer == null)
        {
            viewer = new Viewer();
        }

        viewer.frame.setVisible(true);

        return viewer;
    }

    /**
     * Adds a feature to the Molecule attribute of the Viewer object
     *
     * @param mol  The feature to be added to the Molecule attribute
     */
    public void addMolecule(JOEMol mol)
    {
        frame.addMolecule(mol);
    }

    /**
     *Main method
     *
     * @param args  The command line arguments
     */
    public static void main(String[] args)
    {
        if (Java3DHelper.configOK())
        {
            try
            {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
             catch (Exception e)
            {
                e.printStackTrace();
            }

            if (args.length == 2)
            {
                new Viewer(args[0], args[1]);
            }
            else if (args.length == 1)
            {
                new Viewer(args[0]);
            }
            else
            {
                new Viewer();
            }
        }
        else
        {
            logger.error("Java3D can't be properly configured.");
            logger.error(
                "The graphics board is too old or the driver isn't properly configured.");
            System.exit(0);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
