///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: StatisticPanel.java,v $
//  Purpose:  JOELib Test GUI.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:18 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.test;

import java.awt.BorderLayout;

import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.apache.log4j.Category;

import joelib.test.Convert;
import joelib.test.DescriptorStatistic;


/**
 * DOCUMENT ME!
 *
 * @author $author$
 * @version $Revision: 1.7 $
 */
public class StatisticPanel extends javax.swing.JPanel
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.gui.test.StatisticPanel");

    //~ Instance fields ////////////////////////////////////////////////////////

    private JPanel options;
    private JTextArea description;

    //~ Constructors ///////////////////////////////////////////////////////////

    public StatisticPanel()
    {
        super();
        initComponents();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void startStatistic(String inputFile, String outputFile)
    {
        if ((inputFile == null) || (inputFile.trim().length() == 0))
        {
            logger.error("No input file defined.");

            return;
        }

        Vector argsV = new Vector();

        if ((inputFile != null) && (inputFile.trim().length() != 0))
        {
            argsV.add(inputFile);
        }

        if ((outputFile != null) && (outputFile.trim().length() != 0))
        {
            argsV.add(outputFile);
        }

        String[] args = new String[argsV.size()];

        for (int i = 0; i < argsV.size(); i++)
        {
            args[i] = (String) argsV.get(i);
        }

        DescriptorStatistic convert = new DescriptorStatistic();

        int status = convert.parseCommandLine(args);

        if (status == Convert.CONTINUE)
        {
            convert.test();
        }
        else if (status == Convert.STOP_USAGE)
        {
            convert.usage();

            //System.exit(1);
        }
//        else if (status == Convert.STOP)
//        {
//            //System.exit(0);
//        }
    }

    private void initComponents()
    {
        options = new JPanel();
        description = new JTextArea();

        setLayout(new java.awt.BorderLayout());
        options.setLayout(new java.awt.GridLayout(5, 0));

        description.setText(
            "Calculates the statistic for a molecule descriptor file\n\n" +
            "Command line version can be used with:\n" +
            "Windows: statistic.bat\n" +
            "Linux (or Windows with Cygwin): sh statistic.sh");
        description.setEditable(false);
        description.setToolTipText("Statistic application description");
        add(description, BorderLayout.NORTH);

        add(options, BorderLayout.CENTER);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
