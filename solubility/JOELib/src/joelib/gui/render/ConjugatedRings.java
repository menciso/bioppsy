///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: ConjugatedRings.java,v $
//Purpose:  Renderer for a 2D layout.
//Language: Java
//Compiler: JDK 1.4
//Authors:  Joerg K. Wegner
//Version:  $Revision: 1.4 $
//			$Date: 2004/02/20 13:11:51 $
//			$Author: wegner $
//
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 2 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.render;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.util.JHM;


/**
 * Holding special conjugated ring informations for multiple rings.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/02/20 13:11:51 $
 */
public class ConjugatedRings
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.gui.render.ConjugatedRings");

    //~ Instance fields ////////////////////////////////////////////////////////

    public JOEMol molecule;
    public ConjugatedRing[] cRings;

    //~ Constructors ///////////////////////////////////////////////////////////

    public ConjugatedRings(JOEMol mol, String entries)
    {
        if (!parseCRing(mol, entries))
        {
            logger.error(
                "ring-option;ring-option;... line could not be parsed.");
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Parse entries which start with a non-digit.
     *
     * @param optionEntry
     * @return
     */
    public boolean parseCRing(JOEMol mol, String cRingsS)
    {
        molecule = mol;

        Vector entries = new Vector();
        ConjugatedRing cRing;
        Vector cRingV = new Vector();
        JHM.tokenize(entries, cRingsS, ";");

        String entry;
        boolean store2to = false;

        for (int i = 0; i < entries.size(); i++)
        {
            entry = (String) entries.get(i);
            cRing = new ConjugatedRing();

            if (!cRing.parseCRing(entry))
            {
                return false;
            }

            cRingV.add(cRing);
        }

        cRings = new ConjugatedRing[cRingV.size()];

        for (int i = 0; i < cRingV.size(); i++)
        {
            cRings[i] = (ConjugatedRing) cRingV.get(i);
        }

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
