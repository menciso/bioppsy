///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: RenderingAtoms.java,v $
//Purpose:  Renderer for a 2D layout.
//Language: Java
//Compiler: JDK 1.4
//Authors:  Joerg K. Wegner
//Version:  $Revision: 1.4 $
//			$Date: 2004/07/25 20:43:17 $
//			$Author: wegner $
//Original Author: steinbeck gzelter, egonw
//Original Version: Copyright (C) 1997-2003
//                  The Chemistry Development Kit (CDK) project
//
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
// All we ask is that proper credit is given for our work, which includes
// - but is not limited to - adding the above copyright notice to the beginning
// of your source code files, and to any copyright notice that you may distribute
// with programs based on this work.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.render;

import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;
import joelib.molecule.JOEMolVector;
import joelib.molecule.fragmentation.ContiguousFragments;

import joelib.ring.JOERing;

import joelib.util.JHM;


/**
 * Rendering atoms.
 *
 * @author     steinbeck
 * @author     wegnerj
 * @license    LGPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/07/25 20:43:17 $
 */
public class RenderingAtoms implements java.io.Serializable, Cloneable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.gui.render.RenderingAtoms");

    //~ Instance fields ////////////////////////////////////////////////////////

    Hashtable aMap = new Hashtable();
    Hashtable fraMap = new Hashtable();
    Vector bonds = new Vector();
    Vector frAtoms = new Vector();
    Vector molFragments = new Vector();
    Vector molecules = new Vector();
    Vector rings = new Vector();

    //~ Constructors ///////////////////////////////////////////////////////////

    public RenderingAtoms()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public JOEAtom getAtom(JOEAtom renderAtom)
    {
        RenderAtom ra = (RenderAtom) fraMap.get(renderAtom);

        return ra.atom;
    }

    /**
     * Gets analogue rendering atom for the original atom of the original molecule.
     */
    public RenderAtom getRenderAtom(JOEAtom atom)
    {
        return (RenderAtom) aMap.get(atom);
    }

    public RenderAtom getRenderAtom(int i)
    {
        return (RenderAtom) frAtoms.get(i);
    }

    /**
     * Gets rendering atom.
     *
     * @param i
     * @return
     */
    public JOEAtom getRenderAtomAtom(int i)
    {
        RenderAtom ra = (RenderAtom) frAtoms.get(i);

        return ra.frAtom;
    }

    /**
     * @return
     */
    public int getRenderAtomCount()
    {
        return frAtoms.size();
    }

    public void setRenderAtomLabel(int i, String label)
    {
        RenderAtom ra = (RenderAtom) frAtoms.get(i);
        ra.fraLabel = label;
    }

    public String getRenderAtomLabel(int i)
    {
        RenderAtom ra = (RenderAtom) frAtoms.get(i);

        return ra.fraLabel;
    }

    public void setRenderAtomLabels(JOEMol molecule, String labels,
        String delimiter, String labelDelim)
    {
        if (labels != null)
        {
            Vector lV = new Vector();
            JHM.tokenize(lV, labels, delimiter);

            String s;
            Vector lEntry = new Vector();
            int index;
            String label;
            RenderAtom renderAtom;

            for (int i = 0; i < lV.size(); i++)
            {
                s = (String) lV.get(i);
                index = s.indexOf(labelDelim);

                if (index != -1)
                {
                    Integer integer = new Integer(s.substring(0, index));
                    label = s.substring(index + 1);
                    renderAtom = this.getRenderAtom(molecule.getAtom(
                                integer.intValue()));
                    renderAtom.fraLabel = label;
                }
            }
        }
    }

    public int getRenderAtomNumber(JOEAtom renderAtom)
    {
        RenderAtom ra;

        for (int i = 0; i < frAtoms.size(); i++)
        {
            ra = (RenderAtom) frAtoms.get(i);

            if (ra.frAtom == renderAtom)
            {
                return i;
            }
        }

        return -1;
    }

    public JOEAtom[] getRenderAtoms()
    {
        JOEAtom[] tmp = new JOEAtom[frAtoms.size()];
        RenderAtom ra;

        for (int i = 0; i < frAtoms.size(); i++)
        {
            ra = (RenderAtom) frAtoms.get(i);
            tmp[i] = ra.frAtom;
        }

        return tmp;
    }

    /**
     * @return
     */
    public Vector getRenderBonds()
    {
        return bonds;
    }

    public Vector getRenderFragments()
    {
        return molFragments;
    }

    public JOERing[] getRenderRings()
    {
        JOERing[] tmp = new JOERing[rings.size()];

        for (int i = 0; i < rings.size(); i++)
        {
            tmp[i] = (JOERing) rings.get(i);
        }

        return tmp;
    }

    public void add(JOEMol mol)
    {
        molecules.add(mol);

        Vector origAtomIdx = new Vector();
        ContiguousFragments fragments = new ContiguousFragments();
        JOEMolVector frags = fragments.getFragmentation(mol, false, origAtomIdx);

        //JOEMolVector frags = new JOEMolVector();
        //frags.addMol(mol);
        JOEMol tmpMol;
        Vector tmpRings;
        JOEBond bond;
        JOEAtom atom;

        RenderAtom ra;
        int[] orgIdx;

        for (int i = 0; i < frags.getSize(); i++)
        {
            tmpMol = frags.getMol(i);
            molFragments.add(tmpMol);
            orgIdx = (int[]) origAtomIdx.get(i);

            //System.out.println("frag "+i+" has "+tmpMol.numAtoms()+" atoms");
            for (int j = 1; j <= tmpMol.numAtoms(); j++)
            {
                atom = tmpMol.getAtom(j);

                //atoms.add(atom);
                //atomLabels.add(null);
                if (logger.isDebugEnabled())
                {
                    logger.debug("fragmented atom " + j + " " +
                        atom.getVector());
                }

                //			if(atom.getBonds().size()!=0)connectedAtoms.add(atom);
                ra = new RenderAtom();
                ra.mol = mol;
                ra.frMol = tmpMol;
                ra.atom = mol.getAtom(orgIdx[j - 1]);
                ra.frAtom = atom;
                ra.fraLabel = null;
                frAtoms.add(ra);

                fraMap.put(ra.frAtom, ra);
                aMap.put(ra.atom, ra);
            }

            tmpRings = tmpMol.getSSSR();

            for (int j = 0; j < tmpRings.size(); j++)
            {
                rings.add(tmpRings.get(j));

                if (logger.isDebugEnabled())
                {
                    logger.debug("ring " + tmpRings.get(j) + " in fragment " +
                        i);
                }
            }

            for (int j = 0; j < tmpMol.numBonds(); j++)
            {
                bond = tmpMol.getBond(j);
                bonds.add(bond);

                //if (bond.isDown() || bond.isUp() || bond.isWedge() ||bond.isHash())
                //									{
                //logger.info("bond "+bond.getBeginAtomIdx()+bond+bond.getEndAtomIdx()+" has stereo flags" +bond.getFlags());
                //									}
            }
        }
    }

    public boolean hasRenderAtomLabel(int i)
    {
        RenderAtom ra = (RenderAtom) frAtoms.get(i);

        if ((ra.fraLabel != null) && (ra.fraLabel.length() != 0))
        {
            return true;
        }

        return false;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
