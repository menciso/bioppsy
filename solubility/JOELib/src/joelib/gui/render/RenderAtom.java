///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: RenderAtom.java,v $
//Purpose:  Renderer for a 2D layout.
//Language: Java
//Compiler: JDK 1.4
//Authors:  Joerg K. Wegner
//Version:  $Revision: 1.3 $
//			$Date: 2003/10/27 10:10:20 $
//			$Author: wegner $
//
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 2 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.render;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;


/**
 * Holding original atom and render atom.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.3 $, $Date: 2003/10/27 10:10:20 $
 */
public class RenderAtom
{
    //~ Instance fields ////////////////////////////////////////////////////////

    public JOEAtom atom;

    /**
     * Fragment render atom.
     */
    public JOEAtom frAtom;

    /**
     * Fragment render molecule.
     */
    public JOEMol frMol;
    public JOEMol mol;
    public String fraLabel;
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
