///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: Arrows.java,v $
//Purpose:  Renderer for a 2D layout.
//Language: Java
//Compiler: JDK 1.4
//Authors:  Joerg K. Wegner
//Version:  $Revision: 1.4 $
//			$Date: 2004/02/20 13:11:51 $
//			$Author: wegner $
//
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 2 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.gui.render;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;

import joelib.util.JHM;


/**
 * Holding multiple arrow informations.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/02/20 13:11:51 $
 */
public class Arrows
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.gui.render.Arrows");

    //~ Instance fields ////////////////////////////////////////////////////////

    public JOEMol molecule;
    public Arrow[] arrows;

    //~ Constructors ///////////////////////////////////////////////////////////

    public Arrows(JOEMol mol, String entries)
    {
        if (!parseFromToAtoms(mol, entries))
        {
            logger.error(
                "from-option-to;from-option-to;... line could not be parsed.");
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Parse entries which start with a non-digit.
     *
     * @param optionEntry
     * @return
     */
    public boolean parseFromToAtoms(JOEMol mol, String fromto)
    {
        molecule = mol;

        Vector entries = new Vector();
        Arrow arrow;
        Vector arrowsV = new Vector();
        JHM.tokenize(entries, fromto, ";");

        String entry;
        boolean store2to = false;

        for (int i = 0; i < entries.size(); i++)
        {
            entry = (String) entries.get(i);
            arrow = new Arrow();

            if (!arrow.parseFromToAtom(entry))
            {
                return false;
            }

            arrowsV.add(arrow);
        }

        arrows = new Arrow[arrowsV.size()];

        for (int i = 0; i < arrowsV.size(); i++)
        {
            arrows[i] = (Arrow) arrowsV.get(i);
        }

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
