///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DescriptorInfo.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.17 $
//            $Date: 2004/07/29 10:16:48 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import joelib.util.types.BasicFactoryInfo;


/**
 * Informations for a descriptor.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.17 $, $Date: 2004/07/29 10:16:48 $
 */
public class DescriptorInfo extends BasicFactoryInfo
{
    //~ Static fields/initializers /////////////////////////////////////////////

    public final static String TYPE_UNKNOWN = "unknown";
    public final static String TYPE_NO_COORDINATES = "no_coordinates";
    public final static String TYPE_TOPOLOGICAL = "topological";
    public final static String TYPE_GEOMETRICAL = "geometrical";
    public final static String TYPE_ENERGETIC = "energegetic";
    public final static int REQUIRED_DIMENSION_UNKNOWN = -1;
    public final static int REQUIRED_DIMENSION_NO_COORDINATES = 0;
    public final static int REQUIRED_DIMENSION_TOPOLOGICAL = 2;
    public final static int REQUIRED_DIMENSION_GEOMETRICAL = 3;
    public final static int REQUIRED_DIMENSION_ENERGETIC = 4;

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     * Base path to description file.
     */
    protected String descriptionFile;

    /**
     * Initialization class for this descriptor.
     */
    protected String initialization;

    /**
     * Result class for this descriptor.
     */
    protected String result;

    /**
     * Descriptor type, e.g. structural, topological, geometrical, energegetic.
     *
     * @see #TYPE_UNKNOWN
     * @see #TYPE_NO_COORDINATES
     * @see #TYPE_TOPOLOGICAL
     * @see #TYPE_GEOMETRICAL
     * @see #TYPE_ENERGETIC
     */
    protected String type;

    /**
     * Required descriptor coordinates dimensions: 0, 2 or 3.
     *
     * @see #REQUIRED_DIMENSION_UNKNOWN
     * @see #REQUIRED_DIMENSION_NO_COORDINATES
     * @see #REQUIRED_DIMENSION_TOPOLOGICAL
     * @see #REQUIRED_DIMENSION_GEOMETRICAL
     * @see #REQUIRED_DIMENSION_ENERGETIC
     */
    protected int requiredDimension;

    //~ Constructors ///////////////////////////////////////////////////////////


    /**
     * Initializes a descriptor information.
     *
     * @param _name            descriptor name
     * @param _type            descriptor type, e.g. no_coordinates, topological, geometrical, energegetic
     * @param _requiredDimension   descriptor coordinate dimensions: 0, 2 or 3
     * @param _representation  descriptor representation
     * @param _descriptionFile base path to description file
     * @param _initialization  initialization class for this descriptor
     * @param _result          result class for this descriptor
     * @deprecated the required dimension is strongly connected to the descriptor type,
     * so it would be better to use the internal resolving used in
     * {@link #DescriptorInfo(String, String, String, String, String, String)}
     *
     * @see #TYPE_UNKNOWN
     * @see #TYPE_NO_COORDINATES
     * @see #TYPE_TOPOLOGICAL
     * @see #TYPE_GEOMETRICAL
     * @see #TYPE_ENERGETIC
     */
    public DescriptorInfo(String _name, String _type, String _representation,
        int _requiredDimension, String _descriptionFile,
        String _initialization, String _result)
    {
        super(_name, _representation, _descriptionFile);

        name = _name;
        representation = _representation;
        type = _type;
        requiredDimension = _requiredDimension;
        initialization = _initialization;
        descriptionFile = _descriptionFile;
        result = _result;
    }

    /**
     * Initializes a descriptor information.
     *
     * @param _name            descriptor name
     * @param _type            descriptor type, e.g. no_coordinates, topological, geometrical, energegetic
     * @param _representation  descriptor representation
     * @param _descriptionFile base path to description file
     * @param _initialization  initialization class for this descriptor
     * @param _result          result class for this descriptor
     *
     * @see #TYPE_UNKNOWN
     * @see #TYPE_NO_COORDINATES
     * @see #TYPE_TOPOLOGICAL
     * @see #TYPE_GEOMETRICAL
     * @see #TYPE_ENERGETIC
     */
    public DescriptorInfo(String _name, String _type, String _representation,
        String _descriptionFile, String _initialization, String _result)
    {
        super(_name, _representation, _descriptionFile);

        name = _name;
        representation = _representation;
        type = _type;
        requiredDimension = getDimensionFromType(_type);
        initialization = _initialization;
        descriptionFile = _descriptionFile;
        result = _result;
    }

    /**
     * Initializes a descriptor information.
     *
     * @param _name            descriptor name
     * @param _type            descriptor type, e.g. no_coordinates, topological, geometrical, energegetic
     * @param _representation  descriptor representation
     * @param _descriptionFile base path to description file
     * @param _initialization  initialization class for this descriptor
     * @param _result          result class for this descriptor
     *
     * @see #TYPE_UNKNOWN
     * @see #TYPE_NO_COORDINATES
     * @see #TYPE_TOPOLOGICAL
     * @see #TYPE_GEOMETRICAL
     * @see #TYPE_ENERGETIC
     */
    public DescriptorInfo(String _name, int _requiredDimension,
        String _representation, String _descriptionFile,
        String _initialization, String _result)
    {
        super(_name, _representation, _descriptionFile);

        name = _name;
        representation = _representation;
        type = getTypeFromDimension(_requiredDimension);
        requiredDimension = _requiredDimension;
        initialization = _initialization;
        descriptionFile = _descriptionFile;
        result = _result;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets required descriptor dimension.
     *
     * @return the required dimension
     * @see #REQUIRED_DIMENSION_UNKNOWN
     * @see #REQUIRED_DIMENSION_NO_COORDINATES
     * @see #REQUIRED_DIMENSION_TOPOLOGICAL
     * @see #REQUIRED_DIMENSION_GEOMETRICAL
     * @see #REQUIRED_DIMENSION_ENERGETIC
     * @see #TYPE_UNKNOWN
     * @see #TYPE_NO_COORDINATES
     * @see #TYPE_TOPOLOGICAL
     * @see #TYPE_GEOMETRICAL
     * @see #TYPE_ENERGETIC
     */
    public static int getDimensionFromType(String _type)
    {
        if (_type.equals(TYPE_NO_COORDINATES))
        {
            return REQUIRED_DIMENSION_NO_COORDINATES;
        }
        else if (_type.equals(TYPE_TOPOLOGICAL))
        {
            return REQUIRED_DIMENSION_TOPOLOGICAL;
        }
        else if (_type.equals(TYPE_GEOMETRICAL))
        {
            return REQUIRED_DIMENSION_GEOMETRICAL;
        }
        else if (_type.equals(TYPE_ENERGETIC))
        {
            return REQUIRED_DIMENSION_ENERGETIC;
        }

        return REQUIRED_DIMENSION_UNKNOWN;
    }

    /*-------------------------------------------------------------------------*
     * public static methods
     *------------------------------------------------------------------------- */

    /**
     * Gets descriptor type, e.g. no_coordinates, topological, geometrical, energegetic.
     *
     * @return the descriptor type
     * @see #TYPE_UNKNOWN
     * @see #TYPE_NO_COORDINATES
     * @see #TYPE_TOPOLOGICAL
     * @see #TYPE_GEOMETRICAL
     * @see #TYPE_ENERGETIC
     * @see #REQUIRED_DIMENSION_UNKNOWN
     * @see #REQUIRED_DIMENSION_NO_COORDINATES
     * @see #REQUIRED_DIMENSION_TOPOLOGICAL
     * @see #REQUIRED_DIMENSION_GEOMETRICAL
     * @see #REQUIRED_DIMENSION_ENERGETIC
     */
    public static String getTypeFromDimension(int requiredDimension)
    {
        switch (requiredDimension)
        {
        case REQUIRED_DIMENSION_NO_COORDINATES:
            return TYPE_NO_COORDINATES;

        case REQUIRED_DIMENSION_TOPOLOGICAL:
            return TYPE_TOPOLOGICAL;

        case REQUIRED_DIMENSION_GEOMETRICAL:
            return TYPE_GEOMETRICAL;

        case REQUIRED_DIMENSION_ENERGETIC:
            return TYPE_ENERGETIC;

        default:
            return TYPE_UNKNOWN;
        }
    }

    /**
     * Gets descriptor name.
     *
     * @return the decsriptor name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets the descriptor result class representation name.
     *
     * @return the descriptor result class representation name
     */
    public String getResult()
    {
        return result;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     * Gets descriptor type, e.g. no_coordinates, topological, geometrical, energegetic.
     *
     * @return the descriptor type
     * @see #TYPE_UNKNOWN
     * @see #TYPE_NO_COORDINATES
     * @see #TYPE_TOPOLOGICAL
     * @see #TYPE_GEOMETRICAL
     * @see #TYPE_ENERGETIC
     */
    public String getType()
    {
        return type;
    }

    /**
     * Gets descriptor type dimensions.
     *
     * @return the descriptor type dimensions
     * @see #REQUIRED_DIMENSION_UNKNOWN
     * @see #REQUIRED_DIMENSION_NO_COORDINATES
     * @see #REQUIRED_DIMENSION_TOPOLOGICAL
     * @see #REQUIRED_DIMENSION_GEOMETRICAL
     * @see #REQUIRED_DIMENSION_ENERGETIC
     */
    public int getTypeDimension()
    {
        return requiredDimension;
    }

    /**
     * Gets the descriptor informations.
     *
     * @return the descriptor informations
     */
    public String toString()
    {
        StringBuffer sb = new StringBuffer(100);

        sb.append("<name:");
        sb.append(name);
        sb.append(", type:");
        sb.append(type);
        sb.append(", requiredDimension:");
        sb.append(requiredDimension);
        sb.append(", representation class:");
        sb.append(representation);
        sb.append(", description:");
        sb.append(descriptionFile);
        sb.append(", result class:");
        sb.append(result);
        sb.append(">");

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
