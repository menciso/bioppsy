///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomPropertyDescriptors.java,v $
//  Purpose:  Descriptor helper class.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/08/27 09:30:43 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.util;

import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEPairData;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorFactory;
import joelib.desc.DescriptorHelper;
import joelib.desc.result.APropDoubleArrResult;

import joelib.molecule.JOEMol;
import joelib.molecule.types.AtomProperties;


/**
 *  Example for loading molecules and get atom properties.
 *
 * @author     wegnerj
 */
public class AtomPropertyDescriptors
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.desc.util.AtomPropertyDescriptors");
    private static final Vector atomProperties = DescriptorHelper.instance()
                                                                 .getAtomPropDescs();
    public static final double[] RDF_SMOOTHINGFACTORS = new double[]
        {
            5.0, 25.0, 100.0, 200.0
        };
    public static final String[] DEFAULT_DESC_NAMES = 
    {
        "Burden_modified_eigenvalues", "Auto_correlation",
        "Global_topological_charge_index", "RDF"
    };

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescDescription[] descDescription;
    private String[] descNames;
    private Descriptor[] descriptor;
    private double[] rdfSmoothings;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Set descriptors which accept the 'ATOM_PROPERTY' initializer property.
     */
    public void setDescriptors2Calculate(String[] _descNames)
        throws DescriptorException
    {
        descNames = new String[_descNames.length];

        for (int i = 0; i < _descNames.length; i++)
        {
            descNames[i] = _descNames[i];
        }

        descriptor = new Descriptor[descNames.length];
        descDescription = new DescDescription[descNames.length];

        //get descriptor base
        for (int i = 0; i < descriptor.length; i++)
        {
            descriptor[i] = DescriptorFactory.getDescriptor(descNames[i]);

            if (descriptor[i] == null)
            {
                throw new DescriptorException("Descriptor " + descNames[i] +
                    " can't be loaded.");
            }
        }
    }

    public void setRDFSmoothings(double[] smoothings)
    {
        rdfSmoothings = new double[smoothings.length];

        for (int i = 0; i < smoothings.length; i++)
        {
            rdfSmoothings[i] = smoothings[i];
        }
    }

    /**
    *  A unit test for JUnit
    *
    * @param  molURL   Description of the Parameter
    * @param  inType   Description of the Parameter
    * @param  outType  Description of the Parameter
    */
    public void calculate(JOEMol mol) throws DescriptorException
    {
        // strip salt !!!
        mol.stripSalts();
        
        // delete hydrogens
        //mol.deleteHydrogens();
        //AtomProperties ap;
        //for (int j = 0; j < atomProperties.size(); j++)
        //{
        //	ap=(AtomProperties)DescriptorHelper.descFromMol(mol,(String)atomProperties.get(j),false);
        // 		for (int index = 1;
        //		index <= mol.numAtoms();
        //		index++)
        //		{
        //			System.out.println("AP: "+mol.getTitle()+" "+atomProperties.get(j)+" "+ap.getDoubleValue(index));
        //		}
        //}
        
        
        DescResult result;
        Hashtable[] stringInits = new Hashtable[atomProperties.size()];

        // initialize atom properties
        for (int i = 0; i < atomProperties.size(); i++)
        {
            stringInits[i] = new Hashtable();
            stringInits[i].put("ATOM_PROPERTY", atomProperties.get(i));
        }

        // start calculations
        int rdfSmooth = 0;
        boolean isRDF = false;
        String descBaseName;

        // calculate descriptors and
        // store them as native descriptor values
        for (int i = 0; i < descriptor.length; i++)
        {
            //System.out.print(descNames[i]);
            if (descriptor[i].getDescInfo().getName().equals("RDF"))
            {
                rdfSmooth = rdfSmoothings.length;
                isRDF = true;
            }
            else
            {
                rdfSmooth = 1;
                isRDF = false;
            }

            for (int s = 0; s < rdfSmooth; s++)
            {
                for (int k = 0; k < stringInits.length; k++)
                {
                    if (isRDF)
                    {
                        stringInits[k].put("SMOOTHING_FACTOR",
                            new Double(rdfSmoothings[s]));
                    }

                    // calculate atom properties
                    descriptor[i].clear();
                    result = descriptor[i].calculate(mol, stringInits[k]);

                    // has something weird happen
                    if (result == null)
                    {
                        logger.error("Descriptor " + descNames[i] + ":" +
                            atomProperties.get(k) + " was not calculated for " +
                            mol.getTitle());

                        continue;
                    }
                    
                    // add descriptor data to molecule
                    JOEPairData dp = new JOEPairData();

                    //          dp.setAttribute( descriptor[i].getDescInfo().getName()+"_"+k );
                    if (descriptor[i].getDescInfo().getName().equals("RDF"))
                    {
                        dp.setAttribute(descriptor[i].getDescInfo().getName() +
                            "_B" + rdfSmoothings[s] + ":" +
                            atomProperties.get(k));
                    }
                    else
                    {
                        dp.setAttribute(descriptor[i].getDescInfo().getName() +
                            ":" + atomProperties.get(k));
                    }

                    dp.setValue(result);
                    mol.addData(dp, true);

                    if (result instanceof APropDoubleArrResult)
                    {
                        if (isRDF)
                        {
                            descBaseName = descriptor[i].getDescInfo().getName() +
                                "_B" + rdfSmoothings[s];
                        }
                        else
                        {
                            descBaseName = descriptor[i].getDescInfo().getName();
                        }

                        ((APropDoubleArrResult) result).writeSingleResults(mol,
                            descBaseName, true, false, null, null, true);
                    }
                }
            }
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
