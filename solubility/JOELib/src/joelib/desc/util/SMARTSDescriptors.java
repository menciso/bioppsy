///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SMARTSDescriptors.java,v $
//  Purpose:  Descriptor helper class.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/08/27 09:30:43 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.util;

import java.util.Vector;

import org.apache.log4j.Category;

//import jcompchem.joelib.desc.types.MACCS;
import joelib.data.JOEPairData;

import joelib.desc.DescriptorException;
import joelib.desc.result.BitResult;
import joelib.desc.result.BooleanResult;
import joelib.desc.result.IntResult;
import joelib.desc.types.SSKey3DS;

import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;


/**
 * Example for loading molecules and get atom properties.
 *
 * @author     wegnerj
 */
public class SMARTSDescriptors
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.desc.util.SMARTSDescriptors");

    //~ Instance fields ////////////////////////////////////////////////////////

    //private MACCS maccsFingerprint = new MACCS();
    private SSKey3DS ssFingerprint = new SSKey3DS();
    private JOESmartsPattern[] smarts;
    private String[] smartsDescriptions;
    private String[] smartsStrings;

    //~ Constructors ///////////////////////////////////////////////////////////

    public SMARTSDescriptors()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void setSMARTS2Calculate(String[] _smartsStrings)
        throws DescriptorException
    {
        setSMARTS2Calculate(_smartsStrings, null);
    }

    public void setSMARTS2Calculate(String[] _smartsStrings,
        String[] _descriptions) throws DescriptorException
    {
        smartsStrings = new String[_smartsStrings.length];
        smarts = new JOESmartsPattern[_smartsStrings.length];

        if (_descriptions != null)
        {
            smartsDescriptions = new String[_descriptions.length];
        }

        JOESmartsPattern single = null;

        for (int i = 0; i < _smartsStrings.length; i++)
        {
            smartsStrings[i] = _smartsStrings[i];
            single = new JOESmartsPattern();
            smarts[i] = single;

            if (_descriptions != null)
            {
                smartsDescriptions[i] = _descriptions[i];
            }
            else
            {
                smartsDescriptions = null;
            }

            if (smartsStrings[i] != null)
            {
                if (!smarts[i].init(smartsStrings[i]))
                {
                    throw new DescriptorException("Invalid SMARTS pattern: " +
                        smartsStrings[i]);
                }
            }
        }
    }

    /**
    *  A unit test for JUnit
    *
    * @param  molURL   Description of the Parameter
    * @param  inType   Description of the Parameter
    * @param  outType  Description of the Parameter
    */
    public void calculate(JOEMol mol, boolean countSMARTS)
        throws DescriptorException
    {
        StringBuffer descName;
        Vector matchList;

        // calculate descriptors and
        // store them as native descriptor values
        for (int i = 0; i < smarts.length; i++)
        {
            if (smartsStrings[i] != null)
            {
                descName = new StringBuffer(100);

                if ((smartsDescriptions != null) &&
                        (smartsDescriptions[i] != null))
                {
                    descName.append(smartsDescriptions[i]);

                    //System.out.println("print "+smartsDescriptions[i]);
                }
                else
                {
                    descName.append("SMARTS_");

                    if (countSMARTS)
                    {
                        descName.append("c");
                    }
                    else
                    {
                        descName.append("b");
                    }

                    descName.append('_');
                    descName.append(smartsStrings[i]);
                }

                //System.out.println("match: "+smartsStrings[i]+"="+smarts[i].match(mol));
                // match SMARTS pattern
                if (!smarts[i].match(mol))
                {
                    matchList = null;
                }
                else
                {
                    matchList = smarts[i].getUMapList();
                }

                // add descriptor data to molecule
                JOEPairData dp = new JOEPairData();
                dp.setAttribute(descName.toString());
              	//System.out.println("matchList.size(): "+matchList.size());

                if (countSMARTS)
                {
                    IntResult result = new IntResult();

                    if (matchList != null)
                    {
                    	//System.out.println("matchList.size(): "+matchList.size());
                        result.setInt(matchList.size());
                    }
                    else
                    {
                        result.setInt(0);
                    }

                    dp.setValue(result);
                }
                else
                {
                    BooleanResult result = new BooleanResult();

                    if ((matchList != null) && (matchList.size() != 0))
                    {
                        result.value = true;
                    }
                    else
                    {
                        result.value = false;
                    }

                    dp.setValue(result);
                }

                //System.out.println("add : "+smartsStrings[i]+" = "+dp);
                mol.addData(dp, true);
            }
        }
    }

    /**
    * MACCS keys.
    *
    */

    //    public void calculateMACCS(JOEMol mol) throws DescriptorException
    //    {
    //        //MACCS_fingerprint
    //        JOEPairData dp = null;
    //        BitResult fingerprint = (BitResult) maccsFingerprint.calculate(mol);
    //
    //        for (int i = 1; i <= fingerprint.maxBitSize; i++)
    //        {
    //            BooleanResult result = new BooleanResult();
    //            result.value = fingerprint.value.get(i);
    //            dp = new JOEPairData();
    //            dp.setAttribute("SMARTS_MACCS_" + i);
    //            dp.setValue(result);
    //            mol.addData(dp, true);
    //        }
    //    }

    /**
    * Pharmacophore fingerprint.
    *
    * @author     wegnerj
    * @cite gxsb00
    */
    public void calculateSSKeys(JOEMol mol) throws DescriptorException
    {
        //Pharmacophore_fingerprint_1
        JOEPairData dp = null;
        BitResult fingerprint = (BitResult) ssFingerprint.calculate(mol);

        for (int i = 1; i <= fingerprint.maxBitSize; i++)
        {
            BooleanResult result = new BooleanResult();
            result.value = fingerprint.value.get(i);
            dp = new JOEPairData();
            dp.setAttribute("SMARTS_SSKey_" + i);
            dp.setValue(result);
            mol.addData(dp, true);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
