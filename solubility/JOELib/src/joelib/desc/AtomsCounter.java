///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomsCounter.java,v $
//  Purpose:  Zagreb Group Index 1 - Calculator.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.14 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import java.util.Map;

import org.apache.log4j.Category;

import joelib.data.JOEKernel;

import joelib.desc.result.IntResult;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;
import joelib.util.iterator.AtomIterator;


/**
 * Abstract descriptor class for counting the number of atoms.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.14 $, $Date: 2004/07/25 20:42:59 $
 */
public abstract class AtomsCounter implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.AtomsCounter");
    public final static String ATOM_NUMBERS = "ATOM_NUMBERS";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(ATOM_NUMBERS, "int[]", "Atom numbers to count.",
                true),
        };

    //~ Instance fields ////////////////////////////////////////////////////////

    public DescriptorInfo descInfo;
    private int[] atomicNumbers;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initializes descriptor class for counting the number of atoms.
     */
    public AtomsCounter()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the default atoms to calculate.
     *
     * @return the default atoms to calculate
     */
    public abstract int[] getDefaultAtoms();

    /**
     * Gets the descriptor informations for this descriptor.
     *
     * @return   the descriptor information
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     * Gets the descriptor description.
     *
     * @return   the descriptor description
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    /**
     * Return the accepted properties.
     *
     * @return the accepted properties
     */
    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     *
     * @see #ATOM_NUMBERS
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     *
     * @see #ATOM_NUMBERS
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        if (!(descResult instanceof IntResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                IntResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        // find all atoms of the given list in the molecule
        int counter = 0;
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        int atomicNum;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atomicNum = atom.getAtomicNum();

            for (int i = 0; i < atomicNumbers.length; i++)
            {
                if (atomicNum == atomicNumbers[i])
                {
                    counter++;

                    break;
                }
            }
        }

        IntResult result = (IntResult) descResult;
        result.setInt(counter);
        result.addCMLProperty(JOEKernel.instance().getKernelID());

        return result;
    }

    /**
     * Clear descriptor calculation method for a new molecule.
     */
    public void clear()
    {
        atomicNumbers = getDefaultAtoms();

        if (atomicNumbers == null)
        {
            logger.error("No atomic numbers defined in " +
                AtomsCounter.class.getName());
        }
    }

    /**
     * Initialize descriptor calculation method for all following molecules.
     *
     * @param initData  initialization properties
     * @return <tt>true</tt> if the initialization was successfull
     *
     * @see #ATOM_NUMBERS
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        int[] atoms = (int[]) JOEPropertyHelper.getProperty(this, ATOM_NUMBERS,
                properties);

        if (atoms == null)
        {
            atomicNumbers = getDefaultAtoms();

            if (atomicNumbers == null)
            {
                logger.error("No atomic numbers defined in " +
                    AtomsCounter.class.getName());

                return false;
            }
        }
        else
        {
            atomicNumbers = atoms;
        }

        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
