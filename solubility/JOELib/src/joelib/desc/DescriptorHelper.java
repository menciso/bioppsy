///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DescriptorHelper.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.39 $
//            $Date: 2004/08/27 09:30:42 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import wsi.ra.tool.PropertyHolder;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEGenericData;
import joelib.data.JOEPairData;

import joelib.molecule.JOEMol;

import joelib.util.JOEHelper;
import joelib.util.iterator.GenericDataIterator;


/**
 * Some methods to faciliate the work with descriptors.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.39 $, $Date: 2004/08/27 09:30:42 $
 */
public final class DescriptorHelper
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.desc.DescriptorHelper");
    private static DescriptorHelper instance;
    private static final int DEFAULT_DESCRIPTOR_NUMBER = 50;

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable descHolder;
    private PropertyHolder propertyHolder;
    private boolean addDescriptorIfNotExist = true;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initializes the descriptor helper factory.
     */
    private DescriptorHelper()
    {
        propertyHolder = PropertyHolder.instance();

        // initialize descriptor holder
        descHolder = new Hashtable(DEFAULT_DESCRIPTOR_NUMBER);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Getting descriptor values from a molecule and try to calculate a descriptor
     * value if it is not already available. This method should NOT be used for getting
     * common {@link JOEGenericData} elements of molecules.
     *
     * <p>
     * Missing descriptor values can be calculated by using
     * {@link DescriptorHelper#descFromMol(JOEMol, String)}.<br>
     * Example:
     * <blockquote><pre>
     * DescResult result=null;
     * try
     * {
     *         result=DescriptorHelper.instance().descFromMol(mol, descriptorName);
     * }
     * catch (DescriptorException ex)
     * {
     *         // descriptor can not be calculated
     * }
     * </pre></blockquote>
     * Notice the difference between {@link JOEGenericData} and
     * {@link DescResult}. {@link DescResult} values can be added to
     * molecules by using {@link JOEPairData}
     * <blockquote><pre>
     * JOEPairData dp = new JOEPairData();
     * dp.setAttribute(descriptorName);
     * dp.setValue(result);
     * mol.addData(dp);
     * </pre></blockquote>
     *
     * @param  mol           The molecule
     * @param  descName      The name of the descriptor
     * @param  addIfNotExist If <tt>true</tt> the descriptor value will be added to the molecule
     * @return     The descriptor result
     * @see #descFromMol(JOEMol, String)
     * @see #descFromMol(JOEMol, String, DescResult)
     * @see JOEMol#getData(JOEDataType)
     * @see JOEMol#getData(String)
     * @see JOEMol#genericDataIterator()
     * @see JOEMol#addData(JOEGenericData)
     * @see JOEMol#addData(JOEGenericData, boolean)
     * @see JOEDataType
     * @see JOEPairData
     * @see DescResult
     * @see joelib.molecule.GenericDataHolder#getData(String, boolean)
     */
    public static synchronized DescResult descFromMol(JOEMol mol,
        String descName, boolean addIfNotExist) throws DescriptorException
    {
        // it's more efficient to solve this in the calculation calling method
        DescResult result = null;

        //ResultFactory.instance().getDescResult(descName);
        return descFromMol(mol, descName, result, addIfNotExist);
    }

    /**
     * Getting descriptor values from a molecule and try to calculate a descriptor
     * value if it is not already available. This method should NOT be used for getting
     * common {@link JOEGenericData} elements of molecules.
     *
     * <p>
     * Missing descriptor values can be calculated by using
     * {@link DescriptorHelper#descFromMol(JOEMol, String)}.<br>
     * Example:
     * <blockquote><pre>
     * DescResult result=null;
     * try
     * {
     *         result=DescriptorHelper.instance().descFromMol(mol, descriptorName);
     * }
     * catch (DescriptorException ex)
     * {
     *         // descriptor can not be calculated
     * }
     * </pre></blockquote>
     * Notice the difference between {@link JOEGenericData} and
     * {@link DescResult}. {@link DescResult} values can be added to
     * molecules by using {@link JOEPairData}
     * <blockquote><pre>
     * JOEPairData dp = new JOEPairData();
     * dp.setAttribute(descriptorName);
     * dp.setValue(result);
     * mol.addData(dp);
     * </pre></blockquote>
     *
     * @param  mol           The molecule
     * @param  descName      The name of the descriptor
     * @param  descResult    The descriptor result
     * @param  addIfNotExist If <tt>true</tt> the descriptor value will be added to the molecule
     * @return     The descriptor result
     * @see #descFromMol(JOEMol, String)
     * @see #descFromMol(JOEMol, String, DescResult)
     * @see JOEMol#getData(JOEDataType)
     * @see JOEMol#getData(String)
     * @see JOEMol#genericDataIterator()
     * @see JOEMol#addData(JOEGenericData)
     * @see JOEMol#addData(JOEGenericData, boolean)
     * @see JOEDataType
     * @see JOEPairData
     * @see DescResult
     * @see joelib.molecule.GenericDataHolder#getData(String, boolean)
     */
    public static synchronized DescResult descFromMol(JOEMol mol,
        String descName, DescResult descResult, boolean addIfNotExist)
        throws DescriptorException
    {
        // get descriptor or calculate if not already available
        //    System.out.println(mol);
        //    System.out.println(descName+": "+mol.hasData(descName));
        if (mol.hasData(descName))
        {
            // get calculated descriptor
            JOEPairData data = (JOEPairData) mol.getData(descName, true);

            //      System.out.println("has '"+descName+"' "+data);
            //type checking by class name
            //      if(data.getValue().getClass().getName().equals(descResult.getClass().getName()))
            //      {
            // not really correct
            // more correct would be:
            // ((DescResult) data.getValue()).clone(descResult);
            Object obj = data.getValue();

            if (!JOEHelper.hasInterface(obj, "DescResult"))
            {
                logger.error("Result '" + descName +
                    "' must implement joelib.desc.DescResult. " +
                    obj.getClass().getName() + " does not.");

                return null;
            }
            else
            {
                descResult = (DescResult) data.getValue();
            }

            //		System.out.println(obj.getClass().getName()+":"+obj);
            //		System.out.println("descResult="+descResult);
            //		System.out.println("data="+data);
            //		System.out.println(data.getValue().getClass().getName()+" compared to ");
            //		System.out.println(descResult.getClass().getName());
            //      }
        }
        else
        {
            // calculate distance matrix
            Descriptor descriptor;

            try
            {
                descriptor = DescriptorFactory.getDescriptor(descName);

                if (descriptor == null)
                {
                    logger.error(
                        "Can not find calculation class for descriptor: '" +
                        descName + "'.");

                    return null;
                }

                if (descResult == null)
                {
                    descResult = ResultFactory.instance().getDescResult(descName);
                }

                descResult = descriptor.calculate(mol, descResult);

                if (descResult == null)
                {
                    logger.error("Descriptor '" + descName +
                        "' can not be calculated.");

                    return null;
                }

                // add calculated descriptor to molecule (if it do not exist)
                if (addIfNotExist)
                {
                    JOEPairData dp = new JOEPairData();
                    dp.setAttribute(descName);
                    dp.setValue(descResult);
                    mol.addData(dp);
                }
            }
             catch (DescriptorException ex)
            {
                // more user friendly
                //              ex.printStackTrace();
                //              return null;
                // more developer like
                throw ex;
            }
        }

        return descResult;
    }

    /**
     * Faciliates the generation of a descriptor information.
     *
     * @param name             the name of the descriptor
     * @param descClass        the descriptor calculation class
     * @param type             the descriptor type
     * @param dimension        the descriptor dimension, which is needed for calculation
     * @param initialization   the initialization representation (DEPRECATED)
     * @param result           the name of the result class
     * @return DescriptorInfo  the descriptor information
     * @todo remove initialization
     */
    public static DescriptorInfo generateDescInfo(String name, Class descClass,
        String type, String initialization, String result)
    {
        String representation = descClass.getName();
        int index = representation.lastIndexOf(".");
        String docs = "docs/desc/" + representation.substring(index + 1);

        return new DescriptorInfo(name, type, representation, docs,
            initialization, result);
    }

    /**
     * Gets the instance of the descriptor helper factory.
     *
     * @return the instance of the descriptor helper factory
     */
    public static synchronized DescriptorHelper instance()
    {
        if (instance == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Getting " +
                    DescriptorHelper.class.getClass().getName() + " instance.");
            }

            instance = new DescriptorHelper();
            instance.loadInfos();
        }

        return instance;
    }

    /**
     * @param addDescriptorIfNotExist
     */
    public void setAddingPolicy(boolean addDescriptorIfNotExist)
    {
        this.addDescriptorIfNotExist = addDescriptorIfNotExist;
    }

    /**
     * @return
     */
    public boolean getAddingPolicy()
    {
        return addDescriptorIfNotExist;
    }

    /**
     * Gets the names of all available {@link joelib.molecule.types.AtomProperties} descriptors.
     *
     * @return the names of all available {@link joelib.molecule.types.AtomProperties} descriptors
     */
    public Vector getAtomPropDescs()
    {
        Enumeration e = descHolder.keys();
        String name;
        Vector descs = new Vector(20);

        while (e.hasMoreElements())
        {
            name = (String) e.nextElement();

            if (isAtomPropDesc(name))
            {
                descs.add(name);
            }
        }

        return descs;
    }

    /**
     * Checks if this is a {@link joelib.molecule.types.AtomProperties} descriptor.
     *
     * @param name the name of the descriptor to check
     * @return <tt>true</tt> if this is an atom property descriptor
     */
    public boolean isAtomPropDesc(String name)
	{
    	DescResult result = null;
    	try
			{
    			result = ResultFactory.instance().getDescResult(name);
    		}
    		catch (DescriptorException ex)
			{
    			logger.warn(ex.toString());
    		}

    		if (result instanceof joelib.molecule.types.AtomProperties)
    		//if (JOEHelper.hasInterface(result, "AtomProperties"))
    		{
    			return true;
    		}
    		
    		return false;
    }
    
    
    /**
     * Gets the names of all available {@link joelib.molecule.types.BondProperties} descriptors.
     *
     * @return the names of all available {@link joelib.molecule.types.BondProperties} descriptors
     */
    public Vector getBondPropDescs()
    {
        Enumeration e = descHolder.keys();
        String name;
        Vector descs = new Vector(20);

        while (e.hasMoreElements())
        {
            name = (String) e.nextElement();

            if (isBondPropDesc(name))
            {
                descs.add(name);
            }
        }

        return descs;
    }
    
    /**
     * Checks if this is a {@link joelib.molecule.types.BondProperties} descriptor.
     *
     * @param name the name of the descriptor to check
     * @return <tt>true</tt> if this is an bond property descriptor
     */
    public boolean isBondPropDesc(String name)
	{
    	DescResult result = null;
    	try
		{
    		result = ResultFactory.instance().getDescResult(name);
    	}
    	catch (DescriptorException ex)
		{
    		logger.warn(ex.toString());
    	}

    	if (result instanceof joelib.molecule.types.BondProperties)
    	//if (JOEHelper.hasInterface(result, "BondProperties"))
    	{
    		return true;
    	}
    	
    	return false;
    }

    /**
    * Returns a descriptor information for the given descriptor name.
    *
    * @param  name  the name of the descriptor
    * @return       the descriptor information
    */
    public DescriptorInfo getDescInfo(String name)
    {
        if (name == null)
        {
            return null;
        }

        return (DescriptorInfo) descHolder.get(name);
    }

    /**
     * Gets the names of all available {@link joelib.desc.NativeValue} descriptors.
     *
     * @return the names of all available {@link joelib.desc.NativeValue} descriptors
     */
    public Vector getNativeDescs()
    {
        Enumeration e = descHolder.keys();
        String name;
        Vector descs = new Vector(20);

        while (e.hasMoreElements())
        {
            name = (String) e.nextElement();

            if (isNativeDesc(name))
            {
                descs.add(name);
            }
        }

        return descs;
    }

    /**
     * Checks if this is a {@link joelib.desc.NativeValue} descriptor.
     *
     * @param name the name of the descriptor to check
     * @return <tt>true</tt> if this is a native value descriptor
     */
    public boolean isNativeDesc(String name)
	{
    	DescResult result = null;
    	try
		{
    		result = ResultFactory.instance().getDescResult(name);
    	}
    	catch (DescriptorException ex)
		{
    		logger.warn(ex.toString());
    	}

    	if (result instanceof joelib.desc.NativeValue)
    	//if (JOEHelper.hasInterface(result, "NativeValue"))
    	{
    		return true;
    	}
    	
    	return false;
    }
    
    
    /**
     * Getting descriptor values from a molecule and try to calculate a descriptor
     * value if it is not already available. This method should NOT be used for getting
     * common {@link JOEGenericData} elements of molecules. Not existing descriptor
     * values will be added to the molecule, if the
     * <tt>joelib.descriptor.addIfNotExist</tt> property in the {@link PropertyHolder} is
     * <tt>true</tt>.
     *
     * <p>
     * Missing descriptor values can be calculated by using
     * {@link DescriptorHelper#descFromMol(JOEMol, String)}.<br>
     * Example:
     * <blockquote><pre>
     * DescResult result=null;
     * try
     * {
     *         result=DescriptorHelper.instance().descFromMol(mol, descriptorName);
     * }
     * catch (DescriptorException ex)
     * {
     *         // descriptor can not be calculated
     * }
     * </pre></blockquote>
     * Notice the difference between {@link JOEGenericData} and
     * {@link DescResult}. {@link DescResult} values can be added to
     * molecules by using {@link JOEPairData}
     * <blockquote><pre>
     * JOEPairData dp = new JOEPairData();
     * dp.setAttribute(descriptorName);
     * dp.setValue(result);
     * mol.addData(dp);
     * </pre></blockquote>
     *
     * @param  mol        The molecule
     * @param  descName   The name of the descriptor
     * @return     The descriptor result
     * @see #descFromMol(JOEMol, String, DescResult)
     * @see #descFromMol(JOEMol, String, DescResult, boolean)
     * @see JOEMol#getData(JOEDataType)
     * @see JOEMol#getData(String)
     * @see JOEMol#genericDataIterator()
     * @see JOEMol#addData(JOEGenericData)
     * @see JOEMol#addData(JOEGenericData, boolean)
     * @see JOEDataType
     * @see JOEPairData
     * @see DescResult
     * @see joelib.molecule.GenericDataHolder#getData(String, boolean)
     * @see PropertyHolder
     */
    public synchronized DescResult descFromMol(JOEMol mol, String descName)
        throws DescriptorException
    {
        // it's more efficient to solve this in the calculation calling method
        DescResult result = null;

        //ResultFactory.instance().getDescResult(descName);
        return descFromMol(mol, descName, result, addDescriptorIfNotExist);
    }

    /**
     * Getting descriptor values from a molecule and try to calculate a descriptor
     * value if it is not already available. This method should NOT be used for getting
     * common {@link JOEGenericData} elements of molecules. Not existing descriptor
     * values will be added to the molecule, if the
     * <tt>joelib.descriptor.addIfNotExist</tt> property in the {@link PropertyHolder} is
     * <tt>true</tt>.
     *
     * <p>
     * Missing descriptor values can be calculated by using
     * {@link DescriptorHelper#descFromMol(JOEMol, String)}.<br>
     * Example:
     * <blockquote><pre>
     * DescResult result=null;
     * try
     * {
     *         result=DescriptorHelper.instance().descFromMol(mol, descriptorName);
     * }
     * catch (DescriptorException ex)
     * {
     *         // descriptor can not be calculated
     * }
     * </pre></blockquote>
     * Notice the difference between {@link JOEGenericData} and
     * {@link DescResult}. {@link DescResult} values can be added to
     * molecules by using {@link JOEPairData}
     * <blockquote><pre>
     * JOEPairData dp = new JOEPairData();
     * dp.setAttribute(descriptorName);
     * dp.setValue(result);
     * mol.addData(dp);
     * </pre></blockquote>
     *
     * @param  mol        The molecule
     * @param  descName   The name of the descriptor
     * @param  descResult The descriptor result, where the result should be stored
     * @return     The descriptor result
     * @see #descFromMol(JOEMol, String)
     * @see #descFromMol(JOEMol, String, DescResult, boolean)
     * @see JOEMol#getData(JOEDataType)
     * @see JOEMol#getData(String)
     * @see JOEMol#genericDataIterator()
     * @see JOEMol#addData(JOEGenericData)
     * @see JOEMol#addData(JOEGenericData, boolean)
     * @see JOEDataType
     * @see JOEPairData
     * @see DescResult
     * @see joelib.molecule.GenericDataHolder#getData(String, boolean)
     * @see PropertyHolder
     */
    public synchronized DescResult descFromMol(JOEMol mol, String descName,
        DescResult descResult) throws DescriptorException
    {
        return descFromMol(mol, descName, descResult, addDescriptorIfNotExist);
    }

    /**
     * Returns a {@link Enumeration} of all available descriptor names
     * that could be calculated by using JOELib.
     *
     * @return  The {@link Enumeration} of all available descriptor names
     * @see Descriptor
     */
    public Enumeration descriptors()
    {
        return descHolder.keys();
    }

    /**
     * Returns all descriptor values in the molecule as {@link JOEGenericData} elements.
     *
     * @param  mol        The molecule
     * @return     All descriptors in this molecule as {@link JOEGenericData} elements
     * @see JOEMol#genericDataIterator()
     * @see JOEGenericData
     */
    public synchronized Vector molDescriptors(JOEMol mol)
    {
        Vector descriptors = new Vector(mol.dataSize());

        // List of all available descriptors in molecule
        JOEGenericData genericData;
        GenericDataIterator gdit = mol.genericDataIterator();

        while (gdit.hasNext())
        {
            genericData = gdit.nextGenericData();

            if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
            {
                descriptors.add((JOEPairData) genericData);
            }
        }

        return descriptors;
    }

    /**
     * Gets the number of descriptors that are available.
     *
     * @return the number of descriptors that are available
     */
    public int numberOfDescs()
    {
        return descHolder.size();
    }

    private synchronized DescriptorInfo loadDescInfo(String representation)
        throws DescriptorException
    {
        Descriptor descBase;

        // try to load Descriptor representation class
        try
        {
            descBase = (Descriptor) Class.forName(representation).newInstance();
        }
         catch (ClassNotFoundException ex)
        {
            throw new DescriptorException(representation + " not found.");
        }
         catch (InstantiationException ex)
        {
            throw new DescriptorException(representation +
                " can not be instantiated.");
        }
         catch (IllegalAccessException ex)
        {
            throw new DescriptorException(representation +
                " can't be accessed.");
        }

        if (descBase == null)
        {
            throw new DescriptorException("Descriptor class " + representation +
                " does'nt exist.");
        }

        DescDescription description = descBase.getDescription();
        boolean descriptionMissing = false;
        StringBuffer sb = new StringBuffer(20);

        //        if(!description.hasXMLDescription())
        //        {
        //          sb.append("XML ");
        //          descriptionMissing=true;
        //        }
        //if (!description.hasTextDescription())
        //{
        //	sb.append("TXT ");
        //	descriptionMissing = true;
        //}
        if (!description.hasHTMLDescription())
        {
            sb.append("HTML ");
            descriptionMissing = true;
        }

        if (descriptionMissing)
        {
            sb.append(" description is missing for ");
            sb.append(representation);
            sb.append(" in ");
            sb.append(description.getDescriptionFile());

            logger.warn(sb.toString());
        }

        return descBase.getDescInfo();
    }

    /**
     * Load all descriptor informations from the property file.
     *
     * @return if the informations were loaded successfully
     */
    private synchronized boolean loadInfos()
    {
        String representation;
        Properties prop = propertyHolder.getProperties();
        DescriptorInfo descInfo;

        int i = 0;
        String descriptor_i;
        boolean allInfosLoaded = true;

        while (true)
        {
            i++;

            //System.out.println("load "+i);
            descriptor_i = "joelib.descriptor." + i;
            representation = prop.getProperty(descriptor_i + ".representation");

            if (representation == null)
            {
                logger.info("" + descHolder.size() +
                    " descriptor informations loaded.");

                break;
            }

            descInfo = null;

            try
            {
                descInfo = loadDescInfo(representation);
            }
             catch (DescriptorException ex)
            {
                logger.warn(ex.toString());
                logger.warn("Error in descriptor entry #" + i + ": " +
                    representation);
                allInfosLoaded = false;
            }

            if (descInfo != null)
            {
                if (descHolder.contains(descInfo.getName()))
                {
                    logger.warn("Descriptor entry #" + i + ": " +
                        representation + " is skipped, " +
                        "because name for this descriptor (" +
                        descInfo.getName() + ") exists already.");
                }
                else
                {
                    descHolder.put(descInfo.getName(), descInfo);
                }
            }
            else
            {
                logger.warn("Descriptor info #" + i + " is empty.");
            }
        }

        String flagString = propertyHolder.getProperties().getProperty("joelib.descriptor.addIfNotExist");
        addDescriptorIfNotExist = Boolean.valueOf(flagString).booleanValue();

        return allInfosLoaded;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
