///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: NumberFormatDescResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/03/15 23:16:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import wsi.ra.text.DecimalFormatter;

import joelib.io.IOType;


/**
 * Interface definition for descriptor results which use number formats.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/03/15 23:16:14 $
 */
public interface NumberFormatDescResult
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public String toString(IOType ioType, DecimalFormatter format);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
