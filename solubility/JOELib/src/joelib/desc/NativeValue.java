///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: NativeValue.java,v $
//  Purpose:  Interface to have a fast method to native descriptor values.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;


/**
 * Interface to have a fast method to getting native descriptor values. This is a better
 * method than checking class names and forces the developer to implement fast
 * accesing methods without class casting methods.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:42:59 $
 */
public interface NativeValue
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public void setDoubleNV(double value);

    public double getDoubleNV();

    public boolean isDoubleNV();

    public void setIntNV(int value);

    public int getIntNV();

    public boolean isIntNV();

    public void setStringNV(String value);

    public String getStringNV();
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
