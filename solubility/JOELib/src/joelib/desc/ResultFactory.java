///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ResultFactory.java,v $
//  Purpose:  Factory class to get loader/writer classes.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.32 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import wsi.ra.tool.PropertyHolder;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Category;

import joelib.data.JOEGenericData;
import joelib.data.JOEGlobalDataBase;
import joelib.data.JOEPairData;

import joelib.desc.result.StringResult;

import joelib.io.types.cml.ResultCMLProperties;

import joelib.molecule.JOEMol;

import joelib.util.JOEHelper;
import joelib.util.types.StringPattern;
import joelib.util.types.StringString;


/**
 *  Factory class to get descriptor results and faciliate the parsing of descriptor entries.
 * The definition file can be defined in the
 * <tt>joelib.data.ResultFactory.resourceFile</tt> property in the {@link wsi.ra.tool.PropertyHolder}.
 * The {@link wsi.ra.tool.ResourceLoader} loads the <tt>joelib.properties</tt> file for default.
 *
 * <p>
 * Let's have a look at a <tt>knownResults.txt</tt> example file:
 * <blockquote><pre>
 * $JOELIB_RESULT$ joelib.desc.result.IntResult
 * #
 * # PETRA descriptors
 * #
 * E_CHARGE
 * E_DELTAHF
 * E_HASH
 * E_POLARIZABILITY
 * #
 * # Molconn Z 350
 * #
 * id
 * nvx
 * nrings
 * ncirc
 * nelem
 * $REGEXP$ nas\p{Upper}\p{Lower}*
 * $REGEXP$ nd\d
 * $REGEXP$ ne\d+
 * </pre></blockquote>
 * This means all descriptors of the type nd1, nd2, nd3, ..., nd9 are {@link joelib.desc.result.IntResult}
 * descriptors. Also the E_CHARGE, E_DELTAHF, E_HASH, E_POLARIZABILITY, id, nvx, ... descriptors.<br>
 * For a detailed description for regular expressions patterns have a look at the
 * {@link java.util.regex.Pattern} class.
 *
 * <p>
 * Speed optimization (for external descriptors):
 * <ul>
 * <li> Use as much explicit descriptor name entries as possible. They will be stored in a look up
 * table ({@link java.util.Hashtable})  with fast access predicates.
 * <li> Use regular expressions only if there can be a lot of descriptors described with these expressions.
 * Still, there will be all regular expressions checked, until the first one, which matches the given descriptor,
 * will be found. But this will be always much more expensier than getting descriptor result
 * (representation classes for descriptor values) directly from the {@link java.util.Hashtable}.
 * </ul>
 * External descriptors will be descriptors, which are known in JOELib, but can not be calculated. All
 * internal descriptor result classes will be always stored explicitly.
 *
 * <p>
 * Default:<br>
 * joelib.desc.ResultFactory.resourceFile=<a href="http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/joelib/joelib/src/joelib/data/plain/knownResults.txt?rev=HEAD&content-type=text/vnd.viewcvs-markup">joelib/data/plain/knownResults.txt</a>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.32 $, $Date: 2004/07/25 20:42:59 $
 * @see wsi.ra.tool.PropertyHolder
 * @see wsi.ra.tool.ResourceLoader
 */
public class ResultFactory extends JOEGlobalDataBase
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.desc.ResultFactory");
    private final static String DEFAULT_RESOURCE = "joelib/data/plain/knownResults.txt";
    private final static String IDENTIFIER = "$JOELIB_RESULT$";
    private final static String REGEXP = "$REGEXP$";
    private static ResultFactory instance;

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable representation;
    private String actRep;
    private Vector regExp;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the ResultFactory.
     */
    private ResultFactory()
    {
        initialized = false;

        Properties prop = PropertyHolder.instance().getProperties();
        resourceFile = prop.getProperty(this.getClass().getName() +
                ".resourceFile", DEFAULT_RESOURCE);

        representation = new Hashtable(100, 50);
        regExp = new Vector(30);
        actRep = null;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public static synchronized ResultFactory instance()
    {
        if (instance == null)
        {
            instance = new ResultFactory();
        }

        return instance;
    }

    /**
     *  Gets the descResult attribute of the ResultFactory class
     *
     * @param descName                 Description of the Parameter
     * @return                         The descResult value
     * @exception DescriptorException  Description of the Exception
     */
    public DescResult getDescResult(String descName) throws DescriptorException
    {
        if (!initialized)
        {
            init();
        }

        String resultRepr = null;

        // try to load Descriptor representation class
        DescResult descResult = null;

        // exist calculateable descriptor ?
        DescriptorInfo descInfo = DescriptorHelper.instance().getDescInfo(descName);

        // if no calculateable descriptor exists look in known results
        if (descInfo == null)
        {
            resultRepr = (String) representation.get(descName);

            if (resultRepr == null)
            {
                // o.k., now there is no result type defined for this descriptor
                // let's try to guess one
                resultRepr = guessDescResult(descName);

                if (resultRepr == null)
                {
                    return null;
                }
            }

            /*                  DescriptorInfo(String _name,
                              String dataType,
                              int _typeDimension,
                              String _representation,
                              String _descriptionFile,
                              String _initialization,
                              String _result)*/
            descInfo = new DescriptorInfo(descName,
                    DescriptorInfo.TYPE_UNKNOWN, "", "", null, resultRepr);

            //          throw new DescriptorException("Descriptor '"+name+"' is not defined");
        }
        else
        {
            resultRepr = descInfo.getResult();
        }

        try
        {
            // works only for construtor without arguments
            descResult = (DescResult) Class.forName(resultRepr).newInstance();

            // for descriptor with arguments
            //      Class        cls          = Class.forName(resultRepr);
            //      Constructor  constructor[]  = cls.getDeclaredConstructors();
            //      for (int i = 0; i < constructor.length; i++)
            //      {
            //        Class[]  params  = constructor[i].getParameterTypes();
            //        if (params.length == 1)
            //        {
            //          Object[]  inputs  = {descInfo};
            //          descResult = (DescResult) constructor[i].newInstance(inputs);
            //        }
            //      }
        }
         catch (ClassNotFoundException ex)
        {
            throw new DescriptorException(descInfo.getResult() + " not found.");
        }
         catch (InstantiationException ex)
        {
            throw new DescriptorException(descInfo.getResult() +
                " can not be instantiated.");
        }
         catch (IllegalAccessException ex)
        {
            throw new DescriptorException(descInfo.getResult() +
                " can't be accessed.");
        }

        //        catch (InvocationTargetException ex)
        //        {
        //            ex.printStackTrace();
        //            throw new DescriptorException("InvocationTargetException.");
        //        }
        if (descResult == null)
        {
            throw new DescriptorException("DescResult class " + resultRepr +
                " does'nt exist.");
        }
        else
        {
            return descResult;
        }
    }

    /**
     *  Description of the Method
     *
     * @param buffer  Description of the Parameter
     */
    public void parseLine(String buffer)
    {
        // skip command lines
        String trimmed = buffer.trim();

        if (!trimmed.equals("") && (buffer.charAt(0) != '#'))
        {
            if (trimmed.charAt(0) == '$')
            {
                int index;

                if ((index = trimmed.indexOf(IDENTIFIER)) != -1)
                {
                    actRep = trimmed.substring(index + 1 + IDENTIFIER.length())
                                    .trim();
                }
                else if ((index = trimmed.indexOf(REGEXP)) != -1)
                {
                    String tmp = trimmed.substring(index + 1 + REGEXP.length())
                                        .trim();
                    regExp.add(new StringPattern(actRep, Pattern.compile(tmp)));
                }
            }
            else if (actRep != null)
            {
                //            	System.out.println(buffer.trim()+" is of type "+actRep);
                representation.put(trimmed, actRep);
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param mol       Description of the Parameter
     * @param descName  Description of the Parameter
     * @param data      Description of the Parameter
     * @return          Description of the Return Value
     */
    public JOEPairData parsePairData(JOEMol mol, String descName,
        JOEGenericData data)
    {
        DescResult result;

        try
        {
            result = getDescResult(descName);
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            return null;
        }

        if (result == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("No data result type found for '" + descName +
                    "'. Supposed String value.");
            }

            return null;
        }

        // check if descriptor has already been calculated
        // and if it's not a unparsed descriptor entry in 
        // the StringResult
        // If the StringResult descriptor contains CML element attributes
        // copy these attributes
        JOEPairData pairData = (JOEPairData) data;

        if (pairData.getValue() instanceof DescResult &&
                ((pairData.getValue() instanceof StringResult) == false))
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Descriptor '" + descName +
                    "' is (parsed) PairData");
            }

            return pairData;
        }
        else
        {
            // StringReult descriptor
            // convert StringReult descriptor to Int, Double, ...
            // descriptor, if data type is known
            if ((pairData.getValue() instanceof StringResult))
            {
                StringResult sResult = ((StringResult) pairData.getValue());

                if (logger.isDebugEnabled())
                {
                    logger.debug("Descriptor '" + descName +
                        "'is (unparsed) StringResult");
                    logger.debug("Descriptor '" + descName +
                        "'will forced to be " + result.getClass().getName());
                    logger.debug(descName + "=" + sResult.value);
                }

                pairData.setValue(sResult.value);

                try
                {
                    //System.out.println("INPUT:"+IOTypeHolder.instance().getIOType("CML")+" data:"+ pairData);
                    if (!result.fromPairData(mol.getInputType(), pairData))
                    {
                        logger.error("Descriptor '" + descName +
                            "' could not be parsed.");
                    }

                    //result.fromPairData(IOTypeHolder.instance().getIOType("CML"), pairData);
                    // copy CML element attributes
                    if (JOEHelper.hasInterface(result, "ResultCMLProperties") &&
                            JOEHelper.hasInterface(sResult,
                                "ResultCMLProperties"))
                    {
                        Enumeration enum = sResult.getCMLProperties();

                        if (enum != null)
                        {
                            if (logger.isDebugEnabled())
                            {
                                logger.debug("Copy CML attribute properties");
                            }

                            StringString ss;
                            ResultCMLProperties cmlProp = ((ResultCMLProperties) result);

                            while (enum.hasMoreElements())
                            {
                                ss = (StringString) enum.nextElement();
                                cmlProp.addCMLProperty(ss);
                            }
                        }

                        //						else
                        //						{
                        //							if (logger.isDebugEnabled())
                        //								logger.debug("No CML attribute properties defined");
                        //						}
                    }

                    //					else
                    //					{
                    //						if (logger.isDebugEnabled())
                    //							logger.debug("No CML attribute property acceptor");
                    //					}
                }
                 catch (Exception ex)
                {
                    logger.error("Parsing error: " + ex.getMessage());

                    //ex.printStackTrace();
                    logger.error(" at descriptor '" + descName +
                        "' in molecule: " + mol.getTitle());

                    return null;
                }
            }

            // String
            else
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Descriptor '" + descName +
                        "'is (unparsed) String");
                    logger.debug("Descriptor '" + descName +
                        "'will forced to be " + result.getClass().getName());
                    logger.debug(descName + "=" + pairData);
                }

                try
                {
                    //System.out.println("INPUT:"+IOTypeHolder.instance().getIOType("CML")+" data:"+ pairData);
                    //System.out.println("Forceed to be:"+result.getClass().getName());
                    if (!result.fromPairData(mol.getInputType(), pairData))
                    {
                        logger.error("Descriptor '" + descName +
                            "' could not be parsed.");
                    }

                    //result.fromPairData(IOTypeHolder.instance().getIOType("CML"), pairData);
                }
                 catch (NumberFormatException ex)
                {
                    logger.error(ex.toString());
                    ex.printStackTrace();
                    logger.error("At descriptor '" + descName +
                        "' in molecule: " + mol.getTitle());

                    return null;
                }
            }

            // replace old data
            mol.deleteData(descName);
            mol.addData((JOEPairData) result);
        }

        return (JOEPairData) result;
    }

    /**
     * Get descriptor result ype by using regular expressions.
     *
     * @param descName
     * @return String
     */
    protected String guessDescResult(String descName)
    {
        StringPattern reg;
        Matcher m;

        for (int i = 0; i < regExp.size(); i++)
        {
            reg = (StringPattern) regExp.get(i);
            m = reg.p.matcher(descName);

            //System.out.println(descName+" matches "+reg.p.pattern()+"="+m.matches());
            if (m.matches())
            {
                ///System.out.println(reg.s+" guessed from "+descName+" with "+reg.p.pattern());
                return reg.s;
            }
        }

        return null;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
