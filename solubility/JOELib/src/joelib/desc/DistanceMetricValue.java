///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DistanceMetricValue.java,v $
//  Purpose:  Interface to have a fast method to native descriptor values.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;


/**
 * Interface to have a fast method to getting similarity metric values.
 * The <tt>target</tt> object must be checked. All used metrics must
 * implement the {@link joelib.math.similarity.DistanceMetric} class.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/07/25 20:42:59 $
 * @see joelib.math.similarity.DistanceMetric
 * @see joelib.math.similarity.DistanceMetricHelper
 */
public interface DistanceMetricValue
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public double getDistance(Object target);

    public void loadDefaultMetric() throws DescriptorException;

    public void loadMetric(String represenation) throws DescriptorException;
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
