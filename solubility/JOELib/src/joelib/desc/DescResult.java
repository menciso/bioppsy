///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DescResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.16 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import joelib.data.JOEPairData;

import joelib.io.IOType;


/**
 * Interface definition for descriptor results.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.16 $, $Date: 2004/07/25 20:42:59 $
 */
public interface DescResult
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public Object clone();

    public String formatDescription(IOType ioType);

    public boolean fromPairData(IOType ioType, JOEPairData pairData)
        throws NumberFormatException;

    public boolean fromString(IOType ioType, String sValue)
        throws NumberFormatException;

    public boolean init(String descName);

    public String toString(IOType ioType);

    //  public int defNumCharacters(); // 25
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
