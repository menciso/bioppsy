///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BinaryValue.java,v $
//  Purpose:  Interface to have a fast method to binary descriptor values.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import joelib.util.JOEBitVec;


/**
 * Interface to have a fast method to getting binary (bit string) descriptor values. This
 * is a better
 * method than checking class names and forces the developer to implement fast
 * accesing methods without class casting methods.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:42:59 $
 */
public interface BinaryValue
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public JOEBitVec getBinaryValue();

    // that's a problem inBitResult, because the number of max bits is not set !?
    // use simple the highest set bit ???
    // solve that problem later !;-)
    //public void setBinaryValue(JOEBitVec _value);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
