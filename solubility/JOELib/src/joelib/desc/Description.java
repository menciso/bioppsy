///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Description.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import java.util.Enumeration;
import java.util.Hashtable;


/**
 * Description for a descriptor.
 * Link to the {@link #TEXT}, {@link #HTML} and {@link #XML} description for this descriptor.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:42:59 $
 */
public final class Description
{
    //~ Static fields/initializers /////////////////////////////////////////////

    public final static Description UNDEFINED = new Description(0, "UNDEFINED");

    /**
     *  Description of the Field
     */
    public final static Description TEXT = new Description(1, "TEXT");

    /**
     *  Description of the Field
     */
    public final static Description HTML = new Description(2, "HTML");

    /**
     *  Description of the Field
     */
    public final static Description XML = new Description(3, "XML");

    //public static final int    minType         = 0;

    /**
     *  Description of the Field
     */
    public final static int maxType = 3;
    private static Hashtable descHolder;
    private static String name;

    //~ Instance fields ////////////////////////////////////////////////////////

    private int value;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the Description object
     *
     * @param  _value           Description of the Parameter
     * @param  _name            Description of the Parameter
     * @param  _representation  Description of the Parameter
     */
    private Description(int _value, String _name)
    {
        if (descHolder == null)
        {
            descHolder = new Hashtable(maxType + 1);
        }

        value = _value;
        name = _name;

        if (descHolder.get(name) == null)
        {
            descHolder.put(name, this);
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the name attribute of the Description class
     *
     * @param  type  Description of the Parameter
     * @return       The name value
     */
    public static String getName(Description type)
    {
        //descHolder.keys()
        for (Enumeration e = descHolder.elements(); e.hasMoreElements();)
        {
            Description t = (Description) e.nextElement();

            if (t.value == type.value)
            {
                return t.name;
            }
        }

        return Description.UNDEFINED.name;
    }

    /**
     *  Gets the name attribute of the Description class
     *
     * @param  _value  Description of the Parameter
     * @return         The name value
     */
    public static String getName(int _value)
    {
        return getType(_value).name;
    }

    /**
    *  Gets the type attribute of the Description class
    *
    * @param  _value  Description of the Parameter
    * @return         The type value
    */
    public static Description getType(int _value)
    {
        //descHolder.keys()
        for (Enumeration e = descHolder.elements(); e.hasMoreElements();)
        {
            Description t = (Description) e.nextElement();

            if (t.value == _value)
            {
                return t;
            }
        }

        return Description.UNDEFINED;
    }

    /**
     *  Gets the type attribute of the Description class
     *
     * @param  _name  Description of the Parameter
     * @return        The type value
     */
    public static Description getType(String _name)
    {
        if (descHolder.get(_name) != null)
        {
            return (Description) descHolder.get(name);
        }
        else
        {
            return Description.UNDEFINED;
        }
    }

    /**
     *  Gets the value attribute of the Description class
     *
     * @param  type  Description of the Parameter
     * @return       The value value
     */
    public static int getValue(Description type)
    {
        //descHolder.keys()
        for (Enumeration e = descHolder.elements(); e.hasMoreElements();)
        {
            Description t = (Description) e.nextElement();

            if (t.value == type.value)
            {
                return t.value;
            }
        }

        return Description.UNDEFINED.value;
    }

    /**
     *  Gets the valueFromName attribute of the Description class
     *
     * @param  _name  Description of the Parameter
     * @return        The valueFromName value
     */
    public static int getValueFromName(String _name)
    {
        return getType(_name).value;
    }

    /**
     *  Gets the name attribute of the Description object
     *
     * @return    The name value
     */
    public String getName()
    {
        return name;
    }

    /**
     *  Gets the value attribute of the Description object
     *
     * @return    The value value
     */
    public int getValue()
    {
        return value;
    }

    /**
     *  Description of the Method
     *
     * @param  is  Description of the Parameter
     * @return     Description of the Return Value
     */
    public boolean equals(Description is)
    {
        if (is instanceof Description && (is != null))
        {
            if (is.value == value)
            {
                return true;
            }
        }

        return false;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
