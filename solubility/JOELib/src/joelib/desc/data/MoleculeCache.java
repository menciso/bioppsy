///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeCache.java,v $
//  Purpose:  Molecule caching interface for data mining classes.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Nikolas H. Fechner
//  Version:  $Revision: 1.2 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.data;

import java.util.Hashtable;

import joelib.io.IOType;

import joelib.molecule.JOEMol;

import joelib.process.types.DescStatistic;


/**
 * Molecule caching interface for data mining classes.
 *
 * @author Nikolas H. Fechner
 *
 * @license    GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2004/07/25 20:42:59 $
 */
public interface MoleculeCache
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public Hashtable getBinning(int _bins);

    public Hashtable getBinning(int _bins, boolean forceCalculation);

    public String[] getDescContainsNaN();

    public double[] getDescFromMolByIdentifier(String _moleculeIdentifier);

    public double[] getDescFromMolByIndex(int position);

    public double[] getDescFromMolByName(String _moleculeName);

    public String[] getDescNames();

    public double[] getDescValues(String _descriptorName);

    public double[][] getDescValues(String[] _descriptorNames);

    public double[][] getDescValues(String[] _descriptorNames, int[] ifMolID,
        int[] ifNotMolID);

    public double[][] getMatrix();

    public void setMolIdentifier(String _moleculeIdentifier);

    public String[] getMolNames();

    public boolean setMoleculeDescriptors(JOEMol mol, int moleculeEntry);

    public DescStatistic getStatistic();

    public boolean calcVarianceNorm(DescStatistic _statistic);

    public MoleculeCache clone(MoleculeCache target);

    public boolean existsMatrixFileFor(String fileName);

    public boolean fromFileFor(String fileName);

    public boolean loadMatrix(IOType _inType, String _inFile)
        throws Exception;

    public boolean loadMatrix(IOType _inType, String _inFile, boolean useCaching)
        throws Exception;

    public int numberOfDescriptors();

    public int numberOfMolecules();

    public String toString();

    public void writeMatrixFileFor(String _inFile);

    // BAD !!! Do never use this !!!
    // Only historical for autocorrelation and RDF !!!
    //public double[] getDescFromMolByIdentifier(String _moleculeIdentifier, boolean replaceNaN);
    //public double[] getDescFromMolByIndex(int position, boolean replaceNaN);
    //public double[] getDescFromMolByName(String _moleculeName, boolean replaceNaN);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
