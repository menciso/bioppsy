///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SimpleStringAtomProperty.java,v $
//  Purpose:  Graph potentials.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.1 $
//            $Date: 2004/08/27 09:30:42 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import java.util.Map;

import org.apache.log4j.Category;

import joelib.data.JOEKernel;

import joelib.desc.result.AtomStringResult;

import joelib.io.types.cml.ResultCMLProperties;
import joelib.molecule.JOEMol;

import joelib.util.JOEProperty;


/**
 * Descriptor class for calculating an atom property which are String values.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.1 $, $Date: 2004/08/27 09:30:42 $
 * @cite wy96
 *
 * @see joelib.desc.result.AtomDoubleResult
 */
public abstract class SimpleStringAtomProperty implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.SimpleStringAtomProperty");

    //~ Instance fields ////////////////////////////////////////////////////////

    public DescriptorInfo descInfo;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initializes descriptor class for calculating an atom property which are double values.
     */
    public SimpleStringAtomProperty()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the double atom property values.
     *
     * @param the molecule
     * @return the double atom property values
     */
    public abstract String[] getStringAtomProperties(JOEMol mol, ResultCMLProperties result);

    /**
     * Gets the descriptor informations for this descriptor.
     *
     * @return   the descriptor information
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     * Gets the descriptor description.
     *
     * @return   the descriptor description
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return null;
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
    	AtomStringResult result = null;

        // check if the result type is correct
        if (!(descResult instanceof AtomStringResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                AtomStringResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (AtomStringResult) descResult;
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        String[] atomProperties = getStringAtomProperties(mol, (ResultCMLProperties)result);
        result.setStringArray(atomProperties);
        result.addCMLProperty(JOEKernel.instance().getKernelID());

        return result;
    }

    /**
     * Clear descriptor calculation method for a new molecule.
     */
    public void clear()
    {
    }

    /**
     * Initialize descriptor calculation method for all following molecules.
     *
     * @param initData  initialization properties
     * @return <tt>true</tt> if the initialization was successfull
     */
    public boolean initialize(Map properties)
    {
        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
