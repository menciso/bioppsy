///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DescriptorFactory.java,v $
//  Purpose:  Factory class to get loader/writer classes.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.20 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import java.util.Map;

import org.apache.log4j.Category;


/**
 * Factory class to get descriptor calculation classes.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.20 $, $Date: 2004/07/25 20:42:59 $
 */
public class DescriptorFactory
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.desc.DescriptorFactory");
    private static DescriptorFactory instance;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initialize descriptor helper factory class.
     */
    private DescriptorFactory()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets a descriptor calculation class instance.
     *
     * @param name the name of the descriptor calculation class
     * @return the descriptor calculation instance
     * @throws DescriptorException the descriptor exception
     */
    public static Descriptor getDescriptor(String name)
        throws DescriptorException
    {
        return getDescriptor(name, null);
    }

    /**
         * Gets a descriptor calculation class instance.
         *
         * @param name the name of the descriptor calculation class
         * @param properties the properties to initialize the descriptor calculation class
         * @return the descriptor calculation instance
         * @throws DescriptorException the descriptor exception
     */
    public static Descriptor getDescriptor(String descName, Map properties)
        throws DescriptorException
    {
        // try to load Descriptor representation class
        Descriptor descBase = null;

        DescriptorInfo descInfo = DescriptorHelper.instance().getDescInfo(descName);

        if (descInfo == null)
        {
            return null;

            //          throw new DescriptorException("Descriptor '"+descName+"' is not defined");
        }

        try
        {
            descBase = (Descriptor) Class.forName(descInfo.getRepresentation())
                                         .newInstance();
        }
         catch (ClassNotFoundException ex)
        {
            throw new DescriptorException(descInfo.getRepresentation() +
                " not found.");
        }
         catch (InstantiationException ex)
        {
            throw new DescriptorException(descInfo.getRepresentation() +
                " can not be instantiated.");
        }
         catch (IllegalAccessException ex)
        {
            throw new DescriptorException(descInfo.getRepresentation() +
                " can't be accessed.");
        }

        if (descBase == null)
        {
            throw new DescriptorException("Descriptor class " +
                descInfo.getRepresentation() + " does'nt exist.");
        }
        else
        {
            //          descBase.setDescInfo(descInfo);
            if (properties != null)
            {
                descBase.initialize(properties);
            }

            return descBase;
        }
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/

    /**
     * Gets the instance for the descriptor factory helper class.
     *
     * @return the instance for the descriptor factory helper class
     */
    public static synchronized DescriptorFactory instance()
    {
        if (instance == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Getting " + DescriptorFactory.class.getName() +
                    " instance.");
            }

            instance = new DescriptorFactory();
        }

        return instance;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
