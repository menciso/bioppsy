///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SMARTSCounter.java,v $
//  Purpose:  Zagreb Group Index 1 - Calculator.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.16 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEKernel;

import joelib.desc.result.IntResult;

import joelib.molecule.JOEMol;

import joelib.smarts.JOESmartsPattern;

import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/**
 * Representation for a simple SMARTS counter descriptor.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.16 $, $Date: 2004/07/25 20:42:59 $
 * @see JOESmartsPattern
 * @cite smarts
 */
public abstract class SMARTSCounter implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.SMARTSCounter");
    public final static String SMARTS = "SMARTS";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(SMARTS, "java.lang.String",
                "SMARTS pattern to count.", true),
        };

    //~ Instance fields ////////////////////////////////////////////////////////

    public DescriptorInfo descInfo;
    private JOESmartsPattern smarts;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initializes descriptor class for calculating the number of SMARTS patterns.
     */
    public SMARTSCounter()
    {
        smarts = new JOESmartsPattern();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the default number of SMARTS pattern in the molecule.
     *
     * @param the molecule
     * @return the default number of SMARTS pattern in the molecule
     */
    public abstract String getDefaultSMARTS();

    /**
            * Gets the descriptor informations for this descriptor.
            *
            * @return   the descriptor information
            */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     * Gets the descriptor description.
     *
     * @return   the descriptor description
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     *
     * @see #SMARTS
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     *
     * @see #SMARTS
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        if (!(descResult instanceof IntResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                IntResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());
        }

        if (mol.empty())
        {
            IntResult result = (IntResult) descResult;
            result.setInt(0);
            logger.warn("Empty molecule '" + mol.getTitle() +
                "'. Set descriptor to 0.");

            return result;
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        // match the SMARTS pattern and return matching count
        smarts.match(mol);

        Vector matchList = smarts.getUMapList();

        IntResult result = (IntResult) descResult;
        result.setInt(matchList.size());
        result.addCMLProperty(JOEKernel.instance().getKernelID());

        return result;
    }

    /**
     * Clear descriptor calculation method for a new molecule.
     */
    public void clear()
    {
        if (!smarts.init(getDefaultSMARTS()))
        {
            logger.error("Invalid SMARTS pattern '" + getDefaultSMARTS() +
                "' defined in " + SMARTSCounter.class.getName());
        }
    }

    /**
     * Initialize descriptor calculation method for all following molecules.
     *
     * @param initData  initialization properties
     * @return <tt>true</tt> if the initialization was successfull
     *
     * @see #SMARTS
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        String smartsPattern = (String) JOEPropertyHelper.getProperty(this,
                SMARTS, properties);

        if (smartsPattern == null)
        {
            if (!smarts.init(getDefaultSMARTS()))
            {
                logger.error("Invalid SMARTS pattern '" + getDefaultSMARTS() +
                    "' defined in " + SMARTSCounter.class.getName());

                return false;
            }
        }
        else
        {
            if (!smarts.init(smartsPattern))
            {
                logger.error("Invalid SMARTS pattern '" + smartsPattern +
                    "' defined in " + SMARTSCounter.class.getName());
            }
        }

        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
