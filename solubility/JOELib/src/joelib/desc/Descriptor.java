///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Descriptor.java,v $
//  Purpose:  Descriptor base class.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.16 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import java.util.Map;

import joelib.molecule.JOEMol;

import joelib.util.PropertyAcceptor;


/**
 * Interface for defining descriptors, which can be calculated by using JOELib.
 *
 * For 'knowing' descriptor values which can not be calculated, have a look at the
 * {@link joelib.desc.ResultFactory}  object.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.16 $, $Date: 2004/07/25 20:42:59 $
 */
public interface Descriptor extends PropertyAcceptor
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the descriptor informations for this descriptor.
     *
     * @return   the descriptor information
     */
    public DescriptorInfo getDescInfo();

    //    public JOEProperty[] acceptedProperties();
    //    public void setDescInfo(DescriptorInfo _descInfo);

    /**
     * Gets the descriptor description.
     *
     * @return   the descriptor description
     */
    public DescDescription getDescription();

    /**
     * Calculate descriptor for this molecule.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException;

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException;

    /**
     * Calculate descriptor for this molecule.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, Map initData)
        throws DescriptorException;

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult, Map initData)
        throws DescriptorException;

    /**
     * Clear descriptor calculation method for a new molecule.
     */
    public void clear();

    /**
     * Initialize descriptor calculation method for all following molecules.
     *
     * @param initData  initialization properties
     * @return <tt>true</tt> if the initialization was successfull
     */
    public boolean initialize(Map initData);

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor();
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
