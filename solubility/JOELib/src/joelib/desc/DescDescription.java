///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DescDescription.java,v $
//  Purpose:  Descriptor helper methods.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:42:59 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc;

import wsi.ra.tool.ResourceLoader;

import java.net.URL;


/**
 * Access class to the descriptor descripton files.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:42:59 $
 */
public class DescDescription
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private String descriptionFile;
    private String htmlDescription;
    private String textDescription;
    private String xmlDescription;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initialize the access class to the descriptor descripton files.
     *
     * @param  _descriptionFile  Description of the Parameter
     */
    public DescDescription(String _descriptionFile)
    {
        descriptionFile = _descriptionFile;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Sets base file to the descriptor description files.
     *
     * @param  _descFile  base file to the descriptor description files
     */
    public void setDescriptionFile(String _descFile)
    {
        descriptionFile = _descFile;
    }

    /**
     * Gets base file to the descriptor description files.
     *
     * @return    base file to the descriptor description files
     */
    public String getDescriptionFile()
    {
        return descriptionFile;
    }

    /**
     * Returns <tt>true</tt> if a <b>text</b> description file for the descriptor exists.
     *
     * @return    <tt>true</tt> if a <b>text</b> description file for the descriptor exists
     */
    public final boolean hasTextDescription()
    {
        URL location = this.getClass().getClassLoader().getSystemResource(descriptionFile +
                ".txt");

        if (location == null)
        {
            // try again for web start applications
            location = this.getClass().getClassLoader().getResource(descriptionFile +
                    ".txt");
        }

        return (location != null);
    }

    /**
     * Gets the <b>text</b> description for the descriptor.
     *
     * @return    the <b>text</b> description for the descriptor
     */
    public String getTextDescription()
    {
        if (textDescription != null)
        {
            return textDescription;
        }

        //    System.out.println("load: "+descriptionFile + ".txt");
        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(descriptionFile +
                ".txt");

        if (bytes != null)
        {
            textDescription = new String(bytes);

            return textDescription;
        }
        else
        {
            return null;
        }
    }

    /**
     * Returns <tt>true</tt> if a <b>HTML</b> description file for the descriptor exists.
     *
     * @return    <tt>true</tt> if a <b>HTML</b> description file for the descriptor exists
     */
    public final boolean hasHTMLDescription()
    {
        URL location = this.getClass().getClassLoader().getSystemResource(descriptionFile +
                ".html");

        if (location == null)
        {
            // try again for web start applications
            location = this.getClass().getClassLoader().getResource(descriptionFile +
                    ".html");
        }

        if (location == null)
        {
            location = this.getClass().getClassLoader().getSystemResource(descriptionFile +
                    ".htm");
        }

        if (location == null)
        {
            // try again for web start applications
            location = this.getClass().getClassLoader().getResource(descriptionFile +
                    ".htm");
        }

        return (location != null);
    }

    /**
     * Gets the <b>HTML</b> description for the descriptor.
     *
     * @return    the <b>HTML</b> description for the descriptor
     */
    public String getHTMLDescription()
    {
        if (htmlDescription != null)
        {
            return htmlDescription;
        }

        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(descriptionFile +
                ".html");

        if (bytes == null)
        {
            bytes = ResourceLoader.instance().getBytesFromResourceLocation(descriptionFile +
                    ".htm");
        }

        if (bytes != null)
        {
            htmlDescription = new String(bytes);

            return htmlDescription;
        }
        else
        {
            return null;
        }
    }

    /**
     * Returns <tt>true</tt> if a <b>XML</b> description file for the descriptor exists.
     *
     * @return    <tt>true</tt> if a <b>XML</b> description file for the descriptor exists
     */
    public final boolean hasXMLDescription()
    {
        URL location = this.getClass().getClassLoader().getSystemResource(descriptionFile +
                ".xml");

        if (location == null)
        {
            // try again for web start applications
            location = this.getClass().getClassLoader().getResource(descriptionFile +
                    ".xml");
        }

        return (location != null);
    }

    /**
     * Gets the <b>XML</b> description for the descriptor.
     *
     * @return    the <b>XML</b> description for the descriptor
     */
    public String getXMLDescription()
    {
        if (xmlDescription != null)
        {
            return xmlDescription;
        }

        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(descriptionFile +
                ".xml");

        if (bytes != null)
        {
            xmlDescription = new String(bytes);

            return xmlDescription;
        }
        else
        {
            return null;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
