///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomType.java,v $
//  Purpose:  Atom mass.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.1 $
//            $Date: 2004/08/27 09:30:42 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleStringAtomProperty;

import joelib.io.types.cml.ResultCMLProperties;
import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;
import joelib.util.types.StringString;


/**
 * Atom type (JOELib internal).
 *
 * This atom property stores the JOELib internal atom type, which can be used via the
 * look-up table in {@link joelib.data.JOETypeTable} to export molecules to other
 * formats, like Synyl MOL2, MM2, Tinker, etc.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.1 $, $Date: 2004/08/27 09:30:42 $
 */
public class AtomType extends SimpleStringAtomProperty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.AtomType");
    public static final String DESC_KEY = "Atom_type";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the AtomType object
     */
    public AtomType()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.AtomStringResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public String[] getStringAtomProperties(JOEMol mol, ResultCMLProperties cmlProps)
    {
        // get mass for all atoms
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        String[] types = new String[mol.numAtoms()];
        int i = 0;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            types[i++] = atom.getType();
        }
        
        // not required, because we use here the kernel indentifier instead
        //cmlProps.addCMLProperty(new StringString("dictRef","joelib:atomType"));

        return types;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
