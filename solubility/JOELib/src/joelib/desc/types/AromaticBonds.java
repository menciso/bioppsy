///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AromaticBonds.java,v $
//  Purpose:  Number of aromatic bonds.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleIntDesc;

import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.iterator.BondIterator;


/**
 * Number of aromatic bonds.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:14 $
 */
public class AromaticBonds extends SimpleIntDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.AromaticBonds");
    public static final String DESC_KEY = "Number_of_aromatic_bonds";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape1 object
     */
    public AromaticBonds()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.IntResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the doubleValue attribute of the KierShape1 object
     *
     * @param mol  Description of the Parameter
     * @return     The doubleValue value
     */
    public int getIntValue(JOEMol mol)
    {
        int aromaticBonds = 0;

        BondIterator bit = mol.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            //Molecule graph must be H-Atom depleted
            bond = bit.nextBond();

            if (!bond.getBeginAtom().isHydrogen() &&
                    !bond.getEndAtom().isHydrogen())
            {
                if (bond.isAromatic())
                {
                    aromaticBonds++;
                }
            }
        }

        return aromaticBonds;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
