///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: KierShape1.java,v $
//  Purpose:  Calculates the Kier Shape for paths with length one.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Jan Bruecker, Joerg K. Wegner
//  Version:  $Revision: 1.26 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;


/**
 *  Calculates the Kier Shape for paths with length one.
 *
 * @author    Jan Bruecker
 * @author    wegner
 * @license GPL
 * @cvsversion    $Revision: 1.26 $, $Date: 2004/07/25 20:43:14 $
 * @cite tc00kiershape
 */
public class KierShape1 extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.KierShape1");
    public static final String DESC_KEY = "Kier_shape_1";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape1 object
     */
    public KierShape1()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the doubleValue attribute of the KierShape1 object
     *
     * @param mol  Description of the Parameter
     * @return     The doubleValue value
     */
    public double getDoubleValue(JOEMol mol)
    {
        double kier;

        double paths = 0;
        double atoms = 0;
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();

        //    NbrAtomIterator   nait;
        while (ait.hasNext())
        {
            //Molecule graph must be H-Atom depleted
            atom = ait.nextAtom();

            if (!atom.isHydrogen())
            {
                atoms++;
            }
        }

        BondIterator bit = mol.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            //Molecule graph must be H-Atom depleted
            bond = bit.nextBond();

            if (!bond.getBeginAtom().isHydrogen() &&
                    !bond.getEndAtom().isHydrogen())
            {
                paths++;
            }
        }

        //    System.out.println("Nodes: " + atoms + "\nPaths: " + paths);
        if (paths > 0)
        {
            kier = ((atoms * ((atoms - 1) * (atoms - 1))) / (paths * paths));
        }
        else
        {
            return 0.0;
        }

        return kier;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
