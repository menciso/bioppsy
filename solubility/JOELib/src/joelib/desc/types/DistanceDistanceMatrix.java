///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DistanceDistanceMatrix.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import java.util.Map;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.DoubleMatrixResult;
import joelib.desc.result.IntMatrixResult;

import joelib.molecule.JOEMol;

import joelib.util.JOEProperty;


/**
 *  Calculates the Distance Matrix (shortest paths from each atom to each atom) of a molecule
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/07/25 20:43:14 $
 */
public class DistanceDistanceMatrix implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.DistanceDistanceMatrix");
    public static final String DESC_KEY = "Distance_distance_matrix";

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DistanceMatrix object
     */
    public DistanceDistanceMatrix()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_GEOMETRICAL, null,
                "joelib.desc.result.DoubleMatrixResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @param  _descInfo  The new descInfo value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     * @return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        if (!(descResult instanceof DoubleMatrixResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                DoubleMatrixResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        int atoms = mol.numAtoms();

        // get geometrical distance matrix or calculate if not already available
        DescResult tmpResult = null;
        String geomDistanceMatrixKey = "Geometrical_distance_matrix";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol,
                    geomDistanceMatrixKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error(
                "Can not calculate distance matrix for distance distance matrix.");

            return null;
        }

        if (!(tmpResult instanceof DoubleMatrixResult))
        {
            logger.error("Needed descriptor '" + geomDistanceMatrixKey +
                "' should be of type " + DoubleMatrixResult.class.getName() +
                ". Distance distance matrix can not be calculated.");

            return null;
        }

        DoubleMatrixResult geomDistResult = (DoubleMatrixResult) tmpResult;
        double[][] geometrical = geomDistResult.value;

        // get topological distance matrix or calculate if not already available
        //DescResult tmpResult=null;
        String distanceMatrixKey = "Distance_matrix";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol,
                    distanceMatrixKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error(
                "Can not calculate distance matrix for distance distance matrix.");

            return null;
        }

        if (!(tmpResult instanceof IntMatrixResult))
        {
            logger.error("Needed descriptor '" + distanceMatrixKey +
                "' should be of type " + IntMatrixResult.class.getName() +
                ". Distance distance matrix can not be calculated.");

            return null;
        }

        IntMatrixResult distResult = (IntMatrixResult) tmpResult;
        int[][] topological = distResult.value;

        double[][] matrix = new double[mol.numAtoms()][mol.numAtoms()];
        double dist;

        for (int i = 1; i < mol.numAtoms(); i++)
        {
            for (int k = 0; k < i; k++)
            {
                dist = geometrical[i][k] / topological[i][k];
                matrix[i][k] = dist;
                matrix[k][i] = dist;
            }
        }

        DoubleMatrixResult result = (DoubleMatrixResult) descResult;
        result.value = matrix;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     */
    public boolean initialize(Map properties)
    {
        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
