///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: WeightedBurdenEigenvalues.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import jmat.data.Matrix;

import jmat.data.matrixDecompositions.EigenvalueDecomposition;

import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.APropDoubleArrResult;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;
import joelib.molecule.types.AtomProperties;

import joelib.util.JOEHelper;
import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;


/**
 * @author     wegnerj
 */
public class WeightedBurdenEigenvalues implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.WeightedBurdenEigenvalues");
    public static final String DESC_KEY = "Weighted_burden_eigenvalues";
    public final static String ATOM_PROPERTY1 = "ATOM_PROPERTY1";
    public final static String ATOM_PROPERTY2 = "ATOM_PROPERTY2";
    public final static String ATOM_PROPERTY3 = "ATOM_PROPERTY3";
    public final static String ATOM_PROPERTY4 = "ATOM_PROPERTY4";
    public final static String ATOM_PROPERTY5 = "ATOM_PROPERTY5";
    public final static String ATOM_PROPERTY6 = "ATOM_PROPERTY6";
    public final static String ATOM_PROPERTY1_WEIGHT = "ATOM_PROPERTY1_WEIGHT";
    public final static String ATOM_PROPERTY2_WEIGHT = "ATOM_PROPERTY2_WEIGHT";
    public final static String ATOM_PROPERTY3_WEIGHT = "ATOM_PROPERTY3_WEIGHT";
    public final static String ATOM_PROPERTY4_WEIGHT = "ATOM_PROPERTY4_WEIGHT";
    public final static String ATOM_PROPERTY5_WEIGHT = "ATOM_PROPERTY5_WEIGHT";
    public final static String ATOM_PROPERTY6_WEIGHT = "ATOM_PROPERTY6_WEIGHT";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(ATOM_PROPERTY1, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY2, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY3, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY4, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY5, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY6, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY1_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY2_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY3_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY4_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY5_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY6_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
        };

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;
    private String[] propertyNames;
    private double[] weights;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the BCUT object
     */
    public WeightedBurdenEigenvalues()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.APropDoubleArrResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public static double[] getBurdenEigenvalues(JOEMol mol,
        String[] propertyName, double[] weights)
    {
        if ((propertyName == null) || (mol == null))
        {
            return null;
        }

        // remove hydrogens
        // could be a problem if the atom properties can not be calculated by JOELib
        // In this case use: mol.clone(true);
        JOEMol hDepletedMol = (JOEMol) mol.clone();
        hDepletedMol.deleteHydrogens();

        int propSize = propertyName.length;
        DescResult tmpPropResult;
        AtomProperties[] properties = new AtomProperties[propSize];

        for (int i = 0; i < propSize; i++)
        {
            // get atom properties or calculate if not already available
            try
            {
                tmpPropResult = DescriptorHelper.instance().descFromMol(hDepletedMol,
                        propertyName[i]);
            }
             catch (DescriptorException ex)
            {
                logger.error(ex.toString());

                return null;
            }

            if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
            {
                properties[i] = (AtomProperties) tmpPropResult;
            }
            else
            {
                logger.error("Property '" + propertyName[i] +
                    "' must be an atom type to calculate the burden.");

                return null;
            }
        }

        // calculate burden
        double[] burdenValues = new double[hDepletedMol.numAtoms()];
        Matrix burden = new Matrix(hDepletedMol.numAtoms(),
                hDepletedMol.numAtoms(), 0.001);
        JOEAtom atom;
        int atomIndex;

        AtomIterator ait = hDepletedMol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atomIndex = atom.getIdx();

            double x = 0;

            BondIterator bit = atom.bondIterator();
            JOEBond bond;

            while (bit.hasNext())
            {
                bond = bit.nextBond();

                if (bond.isAromatic())
                {
                    x = 1.5;
                }
                else
                {
                    x = (double) bond.getBO();
                }

                x *= 0.1;

                JOEAtom nbrAtom = bond.getNbrAtom(atom);

                if ((atom.getValence() == 1) || (nbrAtom.getValence() == 1))
                {
                    x += 0.01;
                }

                burden.set(atomIndex - 1, nbrAtom.getIdx() - 1, x);
            }

            // set weighted atom properties
            double weightedProperty = 0.0;

            for (int i = 0; i < propSize; i++)
            {
                //weightedProperty += properties[i].getDoubleValue(atomIndex)*weights[i];
                weightedProperty += (scale(properties[i],
                    properties[i].getDoubleValue(atomIndex),
                    hDepletedMol.numAtoms()) * weights[i]);

                //weightedProperty += scale2(properties[i], properties[i].getDoubleValue(atomIndex), hDepletedMol.numAtoms())*weights[i];
            }

            burden.set(atomIndex - 1, atomIndex - 1, weightedProperty);
        }

        //     System.out.println("burdenMatrix: " + burden.toString());
        EigenvalueDecomposition eigDecomp = burden.eig();
        Matrix eigenvalueMatrix = eigDecomp.getD_Real();

        for (int i = 0; i < hDepletedMol.numAtoms(); i++)
        {
            burdenValues[i] = eigenvalueMatrix.get(i, i);
        }

        return burdenValues;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @return            The description value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     *  Gets the description attribute of the Descriptor object
     *
     * @return            The description value
     * @return            The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        APropDoubleArrResult result = null;

        // check if the result type is correct
        if (!(descResult instanceof APropDoubleArrResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                APropDoubleArrResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (APropDoubleArrResult) descResult;
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        double[] burdenValues = getBurdenEigenvalues(mol, propertyNames, weights);

        //    Arrays.sort(burdenValues);
        //    double burden_sort[] = new double[burdenValues.length];
        //    for (int i = 0; i < burdenValues.length; i++)
        //    {
        //    burden_sort[i] = burdenValues[burdenValues.length-i-1];
        //    }
        // save result
        result.value = burdenValues;

        // set atom property name(s)
        int size = propertyNames.length;
        StringBuffer sb = new StringBuffer(size * 20);

        for (int i = 0; i < size; i++)
        {
            sb.append(propertyNames[i]);
            sb.append(':');
            sb.append(weights[i]);
            sb.append('_');
        }

        result.atomProperty = sb.toString();
        sb = null;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        String property;
        Double weight;
        Vector tmpProperties = new Vector(6);
        Vector tmpWeight = new Vector(6);

        for (int i = 1; i <= 6; i++)
        {
            property = (String) JOEPropertyHelper.getProperty(this,
                    "ATOM_PROPERTY" + i, properties);

            if ((property == null) || property.equals("NOT_SET"))
            {
                break;
            }
            else
            {
                tmpProperties.add(property);
            }

            weight = (Double) JOEPropertyHelper.getProperty(this,
                    "ATOM_PROPERTY" + i + "_WEIGHT", properties);

            if (weight == null)
            {
                break;
            }
            else
            {
                tmpWeight.add(weight);
            }
        }

        propertyNames = new String[tmpProperties.size()];
        weights = new double[tmpProperties.size()];

        for (int i = 0; i < tmpProperties.size(); i++)
        {
            propertyNames[i] = (String) tmpProperties.get(i);
            weights[i] = ((Double) tmpWeight.get(i)).doubleValue();
        }

        return true;
    }

    public static double scale(AtomProperties props, double val, int numberAtoms)
    {
        double sqsum = 0.0;
        double result = 0.0;

        for (int i = 1; i <= numberAtoms; i++)
        {
            sqsum += (props.getDoubleValue(i) * props.getDoubleValue(i));

            //        System.out.println(" props.getDoubleValue(i):"+ props.getDoubleValue(i));
        }

        if (sqsum != 0.0)
        {
            result = val / sqsum;

            return result;
        }
        else
        {
            logger.error("Error: Sqsum = 0; DIV by 0!!");

            return 0;
        }
    }

    public static double scale2(AtomProperties props, double val,
        int numberAtoms)
    {
        double max = -Double.MAX_VALUE;
        double result;

        for (int i = 1; i >= numberAtoms; i++)
        {
            if (max < props.getDoubleValue(i))
            {
                max = props.getDoubleValue(i);
            }
        }

        result = val / max;

        return result;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
