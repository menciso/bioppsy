///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TopologicalRadius.java,v $
//  Purpose:  Calculates the topological radius.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleIntDesc;
import joelib.desc.result.IntMatrixResult;

import joelib.molecule.JOEMol;


/**
 * Calculates the topological radius.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/25 20:43:15 $
 */
public class TopologicalRadius extends SimpleIntDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.TopologicalRadius");
    public static final String DESC_KEY = "Topological_radius";

    //~ Constructors ///////////////////////////////////////////////////////////

    public TopologicalRadius()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.IntResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the defaultAtoms attribute of the NumberOfC object
     *
     * @return   The defaultAtoms value
     */
    public int getIntValue(JOEMol mol)
    {
        if (mol.empty())
        {
            logger.warn("Empty molecule '" + mol.getTitle() + "'. " +
                this.DESC_KEY + " was set to 0.");

            return 0;
        }

        // get distance matrix or calculate if not already available
        DescResult tmpResult = null;
        String distanceMatrixKey = "Distance_matrix";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol,
                    distanceMatrixKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate distance matrix for " + DESC_KEY +
                ".");

            return 0;
        }

        if (!(tmpResult instanceof IntMatrixResult))
        {
            logger.error("Needed descriptor '" + distanceMatrixKey +
                "' should be of type " + IntMatrixResult.class.getName() +
                ". " + DESC_KEY + " can not be calculated.");

            return 0;
        }

        IntMatrixResult distResult = (IntMatrixResult) tmpResult;
        int[][] distances = distResult.value;

        int topologicalRadius = Integer.MAX_VALUE;

        for (int i = 0; i < distances.length; i++)
        {
            for (int ii = 0; ii < i; ii++)
            {
                if (topologicalRadius > distances[i][ii])
                {
                    topologicalRadius = distances[i][ii];
                }
            }
        }

        return topologicalRadius;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
