///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ConjugatedTopologicalDistance.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2004/04/13 14:09:21 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDynamicAtomProperty;
import joelib.desc.result.DynamicArrayResult;
import joelib.desc.result.IntMatrixResult;

import joelib.molecule.JOEMol;
import joelib.molecule.types.AtomProperties;

import joelib.util.JOEHelper;


/**
 * Conjugated topological distance.
 *
 * @author     wegnerj
 * @license GPL
 * @cite wz03
 * @cite wfz04a
 * @cite wfz04b
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/04/13 14:09:21 $
 */
public class ConjugatedTopologicalDistance extends SimpleDynamicAtomProperty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
    *  Obtain a suitable logger.
    */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.ConjugatedTopologicalDistance");
    public static final String DESC_KEY = "Conjugated_topological_distance";

    //~ Instance fields ////////////////////////////////////////////////////////

    private double influenceOfDistance;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
    *  Constructor for the KierShape1 object
    */
    public ConjugatedTopologicalDistance()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.AtomDynamicResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Object getAtomPropertiesArray(JOEMol molOriginal)
    {
        // get atom properties or calculate if not already available
        DescResult tmpPropResult;

        // remove hydrogens
        JOEMol mol = (JOEMol) molOriginal.clone();
        mol.deleteHydrogens();

        try
        {
            tmpPropResult = DescriptorHelper.instance().descFromMol(mol,
                    AtomInConjEnvironment.DESC_KEY);
        }
         catch (DescriptorException e)
        {
            return null;
        }

        AtomProperties atomProperties;

        if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
        {
            atomProperties = (AtomProperties) tmpPropResult;
        }
        else
        {
            // should never happen
            logger.error("Property '" + AtomInConjEnvironment.DESC_KEY +
                "' must be an atom type to calculate the " + DESC_KEY + ".");

            return null;
        }

        // get distance matrix or calculate if not already available
        DescResult tmpResult = null;
        String distanceMatrixKey = APropertyDistanceMatrix.DESC_KEY;

        //"Distance_matrix";
        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol,
                    distanceMatrixKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate distance matrix for " + DESC_KEY +
                ".");

            return null;
        }

        if (!(tmpResult instanceof IntMatrixResult))
        {
            logger.error("Needed descriptor '" + distanceMatrixKey +
                "' should be of type " + IntMatrixResult.class.getName() +
                ". " + DESC_KEY + " can not be calculated.");

            return null;
        }

        IntMatrixResult distResult = (IntMatrixResult) tmpResult;
        int[][] distances = distResult.value;

        // get electrotopological state index
        int s = mol.numAtoms();
        int[] cdist = (int[]) DynamicArrayResult.getNewArray(DynamicArrayResult.INT,
                s);

        int i_1;
        int tmp = 0;

        for (int i = 0; i < s; i++)
        {
            i_1 = i + 1;

            try
            {
                tmp = ((AtomProperties) atomProperties).getIntValue(i_1);
            }
             catch (Exception ex)
            {
                // zip Exception ???
                // i don't get it ... this should NEVER be happen
                ex.printStackTrace();
            }

            cdist[i] = 1;

            for (int j = 0; j < s; j++)
            {
                if ((distances[i][j] == 0) ||
                        (distances[i][j] == Integer.MAX_VALUE))
                {
                    distances[i][j] = 1;
                }

                if (tmp == 1)
                {
                    if (cdist[i] < distances[i][j])
                    {
                        cdist[i] = distances[i][j];
                    }
                }
            }

            if (logger.isDebugEnabled())
            {
                logger.debug("conjugated[" + i_1 + "]=" + tmp + " cdist[" +
                    i_1 + "]=" + cdist[i]);
            }
        }

        // save result
        return cdist;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
