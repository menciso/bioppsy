///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: HBA1.java,v $
//  Purpose:  Number of Hydrogen Bond Acceptors (HBA).
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SMARTSCounter;


/**
 * Number of Hydrogen Bond Acceptors (HBA).
 * This number is often used for drug-like and lead-like filters.
 *
 * @author     wegnerj
 * @license GPL
 * @cite gwb98
 * @cite lldf01
 * @cite odtl01
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:14 $
 * @see joelib.desc.types.HBD1
 * @see joelib.desc.types.HBD2
 * @see joelib.desc.types.HBD2
 * @see joelib.process.filter.RuleOf5Filter
 * @see joelib.desc.types.AtomInDonor
 * @see joelib.desc.types.AtomInAcceptor
 * @see joelib.desc.types.AtomInDonAcc
 */
public class HBA1 extends SMARTSCounter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.HBA1");
    public final static String DEFAULT = 
            "[$([!#6;+0]);!$([F,Cl,Br,I]);!$([o,s,nX3]);!$([Nv5,Pv5,Sv4,Sv6])]";
    public static final String DESC_KEY = "Number_of_HBA_1";

    //~ Constructors ///////////////////////////////////////////////////////////

    public HBA1()
    {
        super();

        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName() +
                "with SMARTS pattern: " + DEFAULT);
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, "joelib.desc.StringInit",
                "joelib.desc.result.IntResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public String getDefaultSMARTS()
    {
        return DEFAULT;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
