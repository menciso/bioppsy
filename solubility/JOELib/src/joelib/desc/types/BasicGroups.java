///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BasicGroups.java,v $
//  Purpose:  Number of basic groups.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SMARTSCounter;


/**
 * Number of basic groups.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:14 $
 */
public class BasicGroups extends SMARTSCounter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.BasicGroups");
    public final static String DEFAULT = 
            "[$([NH2]-[CX4]),$([NH](-[CX4])-[CX4]),$(N(-[CX4])(-[CX4])-[CX4]),$([*;+;!$(*~[*;-])])$(N=C-N),$(N-C=N)]";
    public static final String DESC_KEY = "Number_of_basic_groups";

    //~ Constructors ///////////////////////////////////////////////////////////

    public BasicGroups()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName() +
                "with SMARTS pattern: " + DEFAULT);
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, "joelib.desc.StringInit",
                "joelib.desc.result.IntResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public String getDefaultSMARTS()
    {
        return DEFAULT;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
