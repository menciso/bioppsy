///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: NumberOfHal.java,v $
//  Purpose:  Number of halogen atoms.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;

import joelib.desc.AtomsCounter;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;


/**
 * Number of halogen atoms.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:43:14 $
 */
public class NumberOfHal extends AtomsCounter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.NumberOfHal");
    private static int[] DEFAULT = new int[]
        {
            JOEElementTable.instance().getAtomicNum("F"),
            JOEElementTable.instance().getAtomicNum("Cl"),
            JOEElementTable.instance().getAtomicNum("Br"),
            JOEElementTable.instance().getAtomicNum("I"),
            JOEElementTable.instance().getAtomicNum("At")
        };
    public static final String DESC_KEY = "Number_of_halogen_atoms";

    //~ Constructors ///////////////////////////////////////////////////////////

    public NumberOfHal()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.IntResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the defaultAtoms attribute of the NumberOfC object
     *
     * @return   The defaultAtoms value
     */
    public int[] getDefaultAtoms()
    {
        return DEFAULT;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
