///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GlobalTopologicalChargeIndex.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import jmat.data.Matrix;

import java.util.Map;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.APropDoubleResult;
import joelib.desc.result.IntMatrixResult;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;
import joelib.molecule.types.AtomProperties;

import joelib.util.JOEHelper;
import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;
import joelib.util.iterator.NbrAtomIterator;


/**
 *  Calculates the Topological Charge Index.
 *
 * @author     Gregor Wernet
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/07/25 20:43:14 $
 */
public class GlobalTopologicalChargeIndex implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.GlobalTopologicalChargeIndex");
    public final static String ATOM_PROPERTY = "ATOM_PROPERTY";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(ATOM_PROPERTY, "java.lang.String",
                "Atom property to use.", true, "Atom_valence"),
        };
    public static final String DESC_KEY = "Global_topological_charge_index";

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;
    private String propertyName;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the GlobalTopologicalChargeIndex object
     */
    public GlobalTopologicalChargeIndex()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.APropDoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @return            The description value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     *  Gets the description attribute of the Descriptor object
     *
     * @return            The description value
     * @return            The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @param  molOriginal              Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol molOriginal, DescResult descResult,
        Map properties) throws DescriptorException
    {
        APropDoubleResult result = null;

        // check if the result type is correct
        if (!(descResult instanceof APropDoubleResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                APropDoubleResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (APropDoubleResult) descResult;
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        if (molOriginal.empty())
        {
            result.value = Double.NaN;
            logger.warn("Empty molecule '" + molOriginal.getTitle() + "'. " +
                DESC_KEY + " was set to " + result.value);
            result.atomProperty = propertyName;

            return result;
        }

        JOEMol mol = (JOEMol) molOriginal.clone(true, new String[]{propertyName});
        mol.deleteHydrogens();

        // get atom properties or calculate if not already available
        DescResult tmpPropResult;
        tmpPropResult = DescriptorHelper.instance().descFromMol(mol,
                propertyName);

        AtomProperties atomProperties;

        if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
        {
            atomProperties = (AtomProperties) tmpPropResult;
        }
        else
        {
            logger.error("Property '" + propertyName +
                "' must be an atom type to calculate the " + DESC_KEY + ".");

            return null;
        }

        DescResult tmpResult = null;
        String distanceMatrixKey = "Distance_matrix";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol,
                    distanceMatrixKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate distance matrix for " + DESC_KEY +
                ".");

            return null;
        }

        if (!(tmpResult instanceof IntMatrixResult))
        {
            logger.error("Needed descriptor '" + distanceMatrixKey +
                "' should be of type " + IntMatrixResult.class.getName() +
                ". " + DESC_KEY + " can not be calculated.");

            return null;
        }

        IntMatrixResult distResult = (IntMatrixResult) tmpResult;
        int[][] distances = distResult.value;

        Matrix adjacent = new Matrix(mol.numAtoms(), mol.numAtoms());

        JOEAtom atom1 = new JOEAtom();
        JOEAtom atom2 = new JOEAtom();

        for (int i = 1; i <= mol.numAtoms(); i++)
        {
            atom1 = mol.getAtom(i);

            NbrAtomIterator nbit = atom1.nbrAtomIterator();

            while (nbit.hasNext())
            {
                atom2 = nbit.nextNbrAtom();
                adjacent.set(atom1.getIdx() - 1, atom2.getIdx() - 1, 1.0);
            }
        }

        double[][] d_dist = new double[distances.length][distances[0].length];
        double[][] d_rs_dist = new double[distances.length][distances[0].length];

        for (int i = 0; i < distances.length; i++)
        {
            for (int j = 0; j < distances[i].length; j++)
            {
                d_dist[i][j] = (double) distances[i][j];

                double x = (double) distances[i][j];

                if (x != 0.0)
                {
                    d_rs_dist[i][j] = 1 / (x * x);
                }
                else
                {
                    d_rs_dist[i][j] = 0.0;
                }
            }
        }

        Matrix distance = new Matrix(d_dist);
        Matrix rs_distance = new Matrix(d_rs_dist);

        //System.out.println("Die rs_distance-Matrix:");
        //System.out.print(rs_distance.toString());
        Matrix galvez = adjacent.times(rs_distance);

        //System.out.println("Die Galvez-Matrix:");
        //System.out.print(galvez.toString());
        JOEAtom atom3 = new JOEAtom();
        Matrix charge_term = new Matrix(mol.numAtoms(), mol.numAtoms());

        for (int i = 0; i < mol.numAtoms(); i++)
        {
            for (int j = 0; j < mol.numAtoms(); j++)
            {
                charge_term.set(i, j, (galvez.get(i, j) - galvez.get(j, i)));

                if (i == j)
                {
                    atom3 = mol.getAtom(i + 1);
                    charge_term.set(i, j, atomProperties.getDoubleValue(i + 1));

                    //charge_term.set(i,j,(double) atom3.getValence());
                }
            }
        }

        //System.out.println("Die charge_term-Matrix:");
        //System.out.print(charge_term.toString());
        double[] tci_gk = new double[mol.numAtoms()];

        for (int k = 0; k < mol.numAtoms(); k++)
        {
            for (int i = 0; i < mol.numAtoms(); i++)
            {
                for (int j = 0; j < mol.numAtoms(); j++)
                {
                    double abs_ij = charge_term.get(i, j);

                    if (abs_ij < 0.0)
                    {
                        abs_ij *= -1.0;
                    }

                    double delta = 0.0;

                    if (k == (int) distance.get(i, j))
                    {
                        delta = 1.0;
                    }

                    tci_gk[k] += (0.5 * (abs_ij * delta));

                    //          System.out.println("In G"+k+"(i="+i+",j="+j+"): "+ (0.5*(abs_ij * delta)));
                }
            }

            //System.out.println("G----->G"+k+": "+tci_gk[k]);
        }

        double[] tci_jk = new double[mol.numAtoms()];

        for (int i = 0; i < tci_jk.length; i++)
        {
            tci_jk[i] = tci_gk[i] / (mol.numAtoms() - 1);
        }

        double tci = 0.0;

        for (int i = 0; i < tci_jk.length; i++)
        {
            tci += tci_jk[i];
        }

        // save result
        result.value = tci;
        result.atomProperty = propertyName;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        String property = (String) JOEPropertyHelper.getProperty(this,
                ATOM_PROPERTY, properties);

        if (property == null)
        {
            // should never happen
            propertyName = "Atom_valence";
        }
        else
        {
            propertyName = property;
        }

        return true;
    }

    /**
    * Test the implementation of this descriptor.
    *
    * @return <tt>true</tt> if the implementation is correct
    */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
