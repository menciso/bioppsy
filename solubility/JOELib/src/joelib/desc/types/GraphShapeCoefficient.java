///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GraphShapeCoefficient.java,v $
//  Purpose:  Calculates the graph shape coefficient.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;
import joelib.desc.result.IntResult;

import joelib.molecule.JOEMol;


/**
 * Calculates the graph shape coefficient.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/25 20:43:14 $
 */
public class GraphShapeCoefficient extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.GraphShapeCoefficient");
    public static final String DESC_KEY = "Graph_shape_coefficient";

    //~ Constructors ///////////////////////////////////////////////////////////

    public GraphShapeCoefficient()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the defaultAtoms attribute of the NumberOfC object
     *
     * @return   The defaultAtoms value
     */
    public double getDoubleValue(JOEMol mol)
    {
        // get topological diameter or calculate if not already available
        DescResult tmpResult = null;
        String diameterKey = "Topological_diameter";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol, diameterKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate " + diameterKey + " for " +
                DESC_KEY + ".");

            return 0;
        }

        if (!(tmpResult instanceof IntResult))
        {
            logger.error("Needed descriptor '" + diameterKey +
                "' should be of type " + IntResult.class.getName() + ". " +
                DESC_KEY + " can not be calculated.");

            return 0;
        }

        IntResult diameterResult = (IntResult) tmpResult;
        double diameter = diameterResult.getDoubleNV();

        // get topological radius or calculate if not already available
        //DescResult tmpResult=null;
        String radiusKey = "Topological_radius";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol, radiusKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate " + radiusKey + " for " + DESC_KEY +
                ".");

            return 0;
        }

        if (!(tmpResult instanceof IntResult))
        {
            logger.error("Needed descriptor '" + radiusKey +
                "' should be of type " + IntResult.class.getName() + ". " +
                DESC_KEY + " can not be calculated.");

            return 0;
        }

        IntResult radiusResult = (IntResult) tmpResult;
        double radius = radiusResult.getDoubleNV();

        double shape = (diameter - radius) / radius;

        return shape;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
