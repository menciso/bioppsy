///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: KierShape2.java,v $
//  Purpose:  Calculates the Kier Shape for paths with length two.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.NbrAtomIterator;


/**
 *  Calculates the Kier Shape for paths with length two.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:14 $
 */
public class KierShape2 extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.KierShape2");
    public static final String DESC_KEY = "Kier_shape_2";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape2 object
     */
    public KierShape2()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the doubleValue attribute of the KierShape2 object
     *
     * @param mol  Description of the Parameter
     * @return     The doubleValue value
     */
    public double getDoubleValue(JOEMol mol)
    {
        double nodes = 0;
        AtomIterator ait = mol.atomIterator();
        NbrAtomIterator nbrait;
        NbrAtomIterator nbrait2;
        JOEAtom node;
        JOEAtom nbrNode;
        JOEAtom nbrNode2;
        double p = 0;
        double kier;

        while (ait.hasNext())
        {
            node = ait.nextAtom();

            //Iteration over all nodes, "node" is the current node of the Iteration
            if (!node.isHydrogen())
            {
                //Graph should be Hydrogens depleted
                nodes++;
                nbrait = node.nbrAtomIterator();

                //Iteration over all NeighborAtoms of the current node
                while (nbrait.hasNext())
                {
                    nbrNode = nbrait.nextNbrAtom();
                    nbrait2 = nbrNode.nbrAtomIterator();

                    while (nbrait2.hasNext())
                    {
                        //Iteration over all Neighbor Atoms form current NeighborAtom
                        nbrNode2 = nbrait2.nextNbrAtom();

                        if ((!nbrNode2.isHydrogen()) &&
                                (nbrNode2.getIdx() != node.getIdx()))
                        {
                            p++;
                        }
                    }
                }
            }
        }

        p = (p / 2);

        //each path has been counted twice, so divide by two
        if (p > 0)
        {
            kier = (((nodes - 1) * ((nodes - 2) * (nodes - 2))) / (p * p));
        }
        else
        {
            return 0.0;
        }

        //System.out.println("Kier2 paths: " +p +"\nNodes: " +nodes);
        return kier;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
