///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GraphPotentials.java,v $
//  Purpose:  Graph potentials.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.24 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import jmat.data.Matrix;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleAtomProperty;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;


/**
 * External rotational symmetry or graph potentials.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.24 $, $Date: 2004/07/25 20:43:14 $
 * @cite wy96
 */
public class GraphPotentials extends SimpleDoubleAtomProperty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.GraphPotentials");
    public static final String DESC_KEY = "Graph_potentials";

    //~ Constructors ///////////////////////////////////////////////////////////

    public GraphPotentials()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.AtomDoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public double[] getDoubleAtomProperties(JOEMol mol)
    {
        // get graph potentials for all atoms
        double[] grPot = graphPotentials(mol);

        return grPot;
    }

    /**
     *  Calculate the Graph Potentials of a molecule based on V.E. Rozenblit, A.B.
     *  Golender Logical and Combinatorial Algorithms for Drug Design for an
     *  example see:<br>
     *  W.P. Walters, S. H. Yalkowsky, 'ESCHER-A Computer Program for the
     *  Determination of External Rotational Symmetry Numbers from Molecular
     *  Topology', J. Chem. Inf. Comput. Sci., 1996, 36(5), 1015-1017
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public static double[] graphPotentials(JOEMol mol)
    {
        Matrix g = gMatrix(mol);

        //    System.out.println("G-matrix"+MathHelper.matrixToString(g));
        Matrix inverseG = g.inverse();

        //    System.out.println("inverse G-matrix"+MathHelper.matrixToString(inverseG));
        Matrix c = cMatrix(mol);

        //    System.out.println("C-matrix"+MathHelper.matrixToString(c));
        Matrix h = inverseG.times(c);

        int nAtoms = mol.numAtoms();
        double[] graphPotentials = new double[nAtoms];

        for (int i = 0; i < nAtoms; i++)
        {
            graphPotentials[i] = h.get(i, 0);

            //      System.out.println("H("+i+"):"+h.get(i,0) );
        }

        return graphPotentials;
    }

    /**
     *  Construct the matrix C, which is simply a column vector consisting of the
     *  valence for each atom
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private static Matrix cMatrix(JOEMol mol)
    {
        Matrix c = new Matrix(mol.numAtoms(), 1);
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        int i = 0;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            c.set(i, 0, atom.getValence());
            i++;
        }

        //      ByteArrayOutputStream baos = new ByteArrayOutputStream(10000);
        //      PrintWriter pw = new PrintWriter(baos);
        //      c.print(pw, 5,2);
        //      System.out.println("TEST"+baos.toString());
        return c;
    }

    /**
     *  Construct the matrix G, which puts each atoms valence+1 on the diagonal
     *  and and -1 on the off diagonal if two atoms are connected.
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private static Matrix gMatrix(JOEMol mol)
    {
        JOEAtom atom1;
        JOEAtom atom2;
        Matrix g = new Matrix(mol.numAtoms(), mol.numAtoms());
        AtomIterator ait1 = mol.atomIterator();
        AtomIterator ait2 = mol.atomIterator();
        int i;
        int j;

        i = 0;

        double value;

        while (ait1.hasNext())
        {
            atom1 = ait1.nextAtom();
            ait2.reset();
            j = 0;

            while (ait2.hasNext())
            {
                atom2 = ait2.nextAtom();

                if (atom1 == atom2)
                {
                    value = (atom1.getValence() + 1);
                    value += ((double) atom1.getAtomicNum() / 10.0);
                    value += ((double) atom1.getHyb() / 100.0);
                    g.set(i, j, value);
                }
                else
                {
                    if (atom1.isConnected(atom2))
                    {
                        g.set(i, j, -1.0);
                    }
                    else
                    {
                        g.set(i, j, 0.0);
                    }
                }

                j++;
            }

            i++;
        }

        return g;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
