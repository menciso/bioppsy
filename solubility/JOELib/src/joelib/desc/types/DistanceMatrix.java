///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DistanceMatrix.java,v $
//  Purpose:  Distance matrix.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Category;

import joelib.algo.BFS;
import joelib.algo.BFSResult;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorFactory;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.IntMatrixResult;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.JOEProperty;


/**
 *  Calculates the Distance Matrix (shortest paths from each atom to each atom) of a molecule
 *
 * @author     Jan Bruecker
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/25 20:43:14 $
 */
public class DistanceMatrix implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.DistanceMatrix");
    public static final String DESC_KEY = "Distance_matrix";

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DistanceMatrix object
     */
    public DistanceMatrix()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.IntMatrixResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @param  _descInfo  The new descInfo value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     * @return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        if (mol.empty())
        {
            logger.error("Empty molecule '" + mol.getTitle() + "'.");

            return null;
        }

        if (!(descResult instanceof IntMatrixResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                IntMatrixResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        int atoms = mol.numAtoms();
        Descriptor[] bfs = new Descriptor[atoms];

        try
        {
            for (int i = 0; i < atoms; i++)
            {
                bfs[i] = DescriptorFactory.getDescriptor("Breadth_first_search");

                // System.out.println("Loaded descriptor:\n" + bfs[i].getDescInfo());
            }
        }
         catch (DescriptorException ex)
        {
            ex.printStackTrace();

            return null;
        }

        DescResult[] resultArr = new DescResult[atoms];
        Hashtable[] init = new Hashtable[atoms];
        int[][] matrix = new int[atoms][atoms];
        BFSResult[] resultBFS = new BFSResult[atoms];

        // initialize BFS
        for (int i = 1; i < (atoms + 1); i++)
        {
            JOEAtom startAtom = mol.getAtom(i);
            init[i - 1] = new Hashtable();
            init[i - 1].put(BFS.STARTING_ATOM, startAtom);
        }

        try
        {
            for (int j = 0; j < atoms; j++)
            {
                resultArr[j] = bfs[j].calculate(mol, init[j]);

                // has something weird happen
                if (resultArr[j] == null)
                {
                    logger.error("Distance matrix can't be calculated");

                    return null;
                }
            }
        }
         catch (DescriptorException ex)
        {
            ex.printStackTrace();
        }

        for (int l = 0; l < atoms; l++)
        {
            resultBFS[l] = (BFSResult) resultArr[l];
        }

        for (int p = 0; p < atoms; p++)
        {
            for (int k = 0; k < atoms; k++)
            {
                matrix[p][k] = resultBFS[p].traverse[k];
            }
        }

        IntMatrixResult result = (IntMatrixResult) descResult;
        result.value = matrix;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     */
    public boolean initialize(Map properties)
    {
        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
