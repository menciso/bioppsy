///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: IntrinsicState.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDynamicAtomProperty;
import joelib.desc.result.DynamicArrayResult;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;


/**
 *  Atom valences.
 *
 * @author     wegnerj
 */
public class IntrinsicState extends SimpleDynamicAtomProperty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.IntrinsicState");
    public static final String DESC_KEY = "Intrinsic_state";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape1 object
     */
    public IntrinsicState()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.AtomDynamicResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Object getAtomPropertiesArray(JOEMol mol)
    {
        // get partial charges for all atoms
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        double[] istate = (double[]) DynamicArrayResult.getNewArray(DynamicArrayResult.DOUBLE,
                mol.numAtoms());
        int i = 0;
        double period;
        double valEl;
        double sigEl;
        JOEElementTable elements = JOEElementTable.instance();
        int atomID;
        double t;
        Vector bonds;
        int freeEl;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atomID = atom.getAtomicNum();

            period = elements.getPeriod(atomID);
            bonds = atom.getBonds();
            valEl = 0.0;
            sigEl = (double) bonds.size();

            for (int j = 0; j < bonds.size(); j++)
            {
                valEl += (double) ((JOEBond) bonds.get(j)).getBO();
            }

            //System.out.print(mol.getTitle()+" "+atom.getIdx());
            freeEl = atom.getFreeElectrons();

            //System.out.println("-"+atom.getIdx());
            valEl += (double) freeEl;

            //            			System.out.println("=============");
            //            			System.out.println("atomIndex=" + atom.getIdx());
            //            			System.out.println("period=" + period);
            //            			System.out.println("freeEl=" + freeEl);
            //            			System.out.println("valEl=" + valEl);
            //            			System.out.println("sigEl=" + sigEl);
            t = 2 / period;
            istate[i++] = ((t * t * valEl) + 1) / sigEl;

            //            			System.out.println("istate=" + istate[i - 1]);
        }

        return istate;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
