///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GeometricalRadius.java,v $
//  Purpose:  Calculates the geometrical radius.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;
import joelib.desc.result.DoubleMatrixResult;

import joelib.molecule.JOEMol;


/**
 * Calculates the geometrical radius.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/25 20:43:14 $
 */
public class GeometricalRadius extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.GeometricalRadius");
    public static final String DESC_KEY = "Geometrical_radius";

    //~ Constructors ///////////////////////////////////////////////////////////

    public GeometricalRadius()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_GEOMETRICAL, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the defaultAtoms attribute of the NumberOfC object
     *
     * @return   The defaultAtoms value
     */
    public double getDoubleValue(JOEMol mol)
    {
        if (mol.empty())
        {
            logger.warn("Empty molecule '" + mol.getTitle() + "'. " +
                this.DESC_KEY + " was set to 0.");

            return 0.0;
        }

        // get distance matrix or calculate if not already available
        DescResult tmpResult = null;
        String geomDistanceMatrixKey = GeomDistanceMatrix.DESC_KEY;

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol,
                    geomDistanceMatrixKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate distance matrix for " + DESC_KEY +
                ".");

            return 0;
        }

        if (!(tmpResult instanceof DoubleMatrixResult))
        {
            logger.error("Needed descriptor '" + geomDistanceMatrixKey +
                "' should be of type " + DoubleMatrixResult.class.getName() +
                ". " + DESC_KEY + " can not be calculated.");

            return 0;
        }

        DoubleMatrixResult distResult = (DoubleMatrixResult) tmpResult;
        double[][] distances = distResult.value;

        double geometricalRadius = Double.MAX_VALUE;

        for (int i = 0; i < distances.length; i++)
        {
            for (int ii = 0; ii < i; ii++)
            {
                if (geometricalRadius > distances[i][ii])
                {
                    geometricalRadius = distances[i][ii];
                }
            }
        }

        return geometricalRadius;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
