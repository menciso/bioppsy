///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomValence.java,v $
//  Purpose:  Atom valence.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDynamicAtomProperty;
import joelib.desc.result.DynamicArrayResult;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;


/**
 * Atom valence.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:14 $
 */
public class AtomValence extends SimpleDynamicAtomProperty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.AtomValence");
    public static final String DESC_KEY = "Atom_valence";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape1 object
     */
    public AtomValence()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.AtomDynamicResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Object getAtomPropertiesArray(JOEMol mol)
    {
        // get partial charges for all atoms
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        int[] valences = (int[]) DynamicArrayResult.getNewArray(DynamicArrayResult.INT,
                mol.numAtoms());
        int i = 0;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            valences[i++] = atom.getValence();
        }

        return valences;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
