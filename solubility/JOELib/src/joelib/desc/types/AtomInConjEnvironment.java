///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomInConjEnvironment.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDynamicAtomProperty;
import joelib.desc.result.DynamicArrayResult;

import joelib.molecule.JOEMol;

import joelib.smarts.Patty;


/**
 * Is this atom part of a conjugated system.
 *
 * @author     wegnerj
 * @license GPL
 * @cite wz03
 * @cite wfz04a
 * @cite wfz04b
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/07/25 20:43:14 $
 */
public class AtomInConjEnvironment extends SimpleDynamicAtomProperty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.AtomInConjEnvironment");
    public static final String DESC_KEY = "Atom_in_conjugated_environment";
    private static Patty patty = new Patty();

    /**
     * Assigned identifier for conjugated atoms.
     */
    private static String assignment = "c";

    static
    {
        // all aromatic atoms
        patty.addRule("a", assignment);

        // all butadien analogues
        patty.addRule("*=,#*-,=*=,#*", assignment);

        // alpha, beta unsaturated, with pi effects
        patty.addRule("[N,P,O,S]=,#*-[*;!H0]", assignment);

        // alpha, beta unsaturated, with sigma effects
        patty.addRule("*=,#*-[F,Cl,Br,I]", assignment);

        // alpha, beta unsaturated, with pi effects (tautomer forms)
        patty.addRule("*=,#*-[N,P,O,S;!H0]", assignment);
    }

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape1 object
     */
    public AtomInConjEnvironment()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.AtomDynamicResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Object getAtomPropertiesArray(JOEMol mol)
    {
        // get electrotopological state index
        int s = mol.numAtoms();
        boolean[] conjugated = (boolean[]) DynamicArrayResult.getNewArray(DynamicArrayResult.BOOLEAN,
                s);

        int[] assignment = patty.assignTypes2All(mol);

        for (int i = 0; i < assignment.length; i++)
        {
            //			System.out.println("assignment["+i+"]:"+assignment[i]);
            if (assignment[i] != -1)
            {
                conjugated[i] = true;
            }
        }

        //		for (int i = 0; i < s; i++)
        //		{
        //			System.out.println(
        //				"conjugated["
        //					+ (i+1)
        //					+ "]="
        //					+ conjugated[i]);
        //		}
        // save result
        return conjugated;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
