///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: WeightedBurdenModifiedEigenvalues.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import java.util.Arrays;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.APropDoubleArrResult;

import joelib.molecule.JOEMol;

import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/**
 * @author     wegnerj
 */
public class WeightedBurdenModifiedEigenvalues implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.BurdenModifiedEigenvalues");
    public static final String DESC_KEY = "Weighted_burden_modified_eigenvalues";
    public final static String ATOM_PROPERTY1 = "ATOM_PROPERTY1";
    public final static String ATOM_PROPERTY2 = "ATOM_PROPERTY2";
    public final static String ATOM_PROPERTY3 = "ATOM_PROPERTY3";
    public final static String ATOM_PROPERTY4 = "ATOM_PROPERTY4";
    public final static String ATOM_PROPERTY5 = "ATOM_PROPERTY5";
    public final static String ATOM_PROPERTY6 = "ATOM_PROPERTY6";
    public final static String ATOM_PROPERTY1_WEIGHT = "ATOM_PROPERTY1_WEIGHT";
    public final static String ATOM_PROPERTY2_WEIGHT = "ATOM_PROPERTY2_WEIGHT";
    public final static String ATOM_PROPERTY3_WEIGHT = "ATOM_PROPERTY3_WEIGHT";
    public final static String ATOM_PROPERTY4_WEIGHT = "ATOM_PROPERTY4_WEIGHT";
    public final static String ATOM_PROPERTY5_WEIGHT = "ATOM_PROPERTY5_WEIGHT";
    public final static String ATOM_PROPERTY6_WEIGHT = "ATOM_PROPERTY6_WEIGHT";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(ATOM_PROPERTY1, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY2, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY3, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY4, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY5, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY6, "java.lang.String",
                "SMARTS pattern to count.", true, "NOT_SET"),
            new JOEProperty(ATOM_PROPERTY1_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY2_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY3_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY4_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY5_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
            new JOEProperty(ATOM_PROPERTY6_WEIGHT, "java.lang.Double",
                "SMARTS pattern to count.", true, new Double(0.0)),
        };

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;
    private String[] propertyNames;
    private double[] weights;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the BCUT object
     */
    public WeightedBurdenModifiedEigenvalues()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.APropDoubleArrResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @return            The description value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     *  Gets the description attribute of the Descriptor object
     *
     * @return            The description value
     * @return            The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        APropDoubleArrResult result = null;

        // check if the result type is correct
        if (!(descResult instanceof APropDoubleArrResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                APropDoubleArrResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (APropDoubleArrResult) descResult;
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        double[] bcutValues = WeightedBurdenEigenvalues.getBurdenEigenvalues(mol,
                propertyNames, weights);

        for (int i = 0; i < bcutValues.length; i++)
        {
            bcutValues[i] = Math.abs(bcutValues[i]);
        }

        Arrays.sort(bcutValues);

        double[] bcut_sort = new double[bcutValues.length];

        for (int i = 0; i < bcutValues.length; i++)
        {
            bcut_sort[i] = bcutValues[bcutValues.length - i - 1];
        }

        // save result
        result.value = bcut_sort;

        // set atom property name(s)
        int size = propertyNames.length;
        StringBuffer sb = new StringBuffer(size * 20);

        for (int i = 0; i < size; i++)
        {
            sb.append(propertyNames[i]);
            sb.append(':');
            sb.append(weights[i]);
            sb.append('_');
        }

        result.atomProperty = sb.toString();

        //if(result.atomProperty.trim().length()==0)	result.atomProperty = "?";
        sb = null;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        String property;
        Double weight;
        Vector tmpProperties = new Vector(6);
        Vector tmpWeight = new Vector(6);

        for (int i = 1; i <= 6; i++)
        {
            property = (String) JOEPropertyHelper.getProperty(this,
                    "ATOM_PROPERTY" + i, properties);

            //	System.out.println(""+i+"'"+property+"'");
            if ((property == null) || property.equals("NOT_SET"))
            {
                break;
            }
            else
            {
                //	System.out.println("add "+i+property);
                tmpProperties.add(property);
            }

            weight = (Double) JOEPropertyHelper.getProperty(this,
                    "ATOM_PROPERTY" + i + "_WEIGHT", properties);

            //	System.out.println(""+i+weight);
            if (weight == null)
            {
                break;
            }
            else
            {
                //	       System.out.println("add weight "+i+" "+weight);
                tmpWeight.add(weight);
            }
        }

        propertyNames = new String[tmpProperties.size()];
        weights = new double[tmpProperties.size()];

        //    System.out.println("s1:"+tmpProperties.size()+" s2:"+tmpWeight.size());
        for (int i = 0; i < tmpProperties.size(); i++)
        {
            propertyNames[i] = (String) tmpProperties.get(i);

            //      System.out.println(""+i+" is set "+tmpWeight.get(i));
            Double d = (Double) tmpWeight.get(i);

            //      System.out.println("Double: "+d);
            weights[i] = d.doubleValue();
        }

        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
