///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: TopologicalAtomPair.java,v $
//  Purpose:  Atom pair descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/08/27 09:30:42 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types.atompair;

import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.IntMatrixResult;

import joelib.molecule.JOEMol;
import joelib.molecule.types.AtomProperties;

import joelib.util.JOEHelper;
import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/**
 * Calculates topological atom type pair (depends on atom properties used).
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/08/27 09:30:42 $
 */
public class TopologicalAtomPair implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.atompair.TopologicalAtomPair");
    public static final String DESC_KEY = "Topological_atom_pair";
    public final static String ATOM_PROPERTIES = "ATOM_PROPERTIES";

    // define default atom properties:
    //////////////////////////////////
    //	Electronegativity_pauling
    //	Electrogeometrical_state_index
    //	Conjugated_topological_distance
    //	Graph_potentials
    //	Conjugated_electrotopological_state_index
    //	Atom_mass
    //	Atom_van_der_waals_volume
    //	Atom_in_conjugated_environment
    //	Atom_valence
    //	Intrinsic_state
    //	Electrotopological_state_index
    //	Electron_affinity
    //	Gasteiger_Marsili
    //	public final static String DEFAULT_ATOM_PROPERTIES[] = new String[] { "Atom_valence", "Gasteiger_Marsili", "Intrinsic_state"};
    //public final static String DEFAULT_ATOM_PROPERTIES[] =	new String[] { "Atom_in_acceptor" };
    public final static String[] DEFAULT_ATOM_PROPERTIES = new String[]
        {
            "Atom_valence"
        };
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(ATOM_PROPERTIES, "[Ljava.lang.String;",
                "Atom properties to define atom pair atom types.", true,
                DEFAULT_ATOM_PROPERTIES),
        };

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;

    // store informations for caching properties
    private JOEMol molCache;
    private JOEMol molCacheDeprotonated;
    private AtomProperties[] atomPropertiesCache;
    private String[] atomPropertyNames;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape1 object
     */
    public TopologicalAtomPair()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.types.atompair.AtomPairResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @return            The description value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     *  Gets the description attribute of the Descriptor object
     *
     * @return            The description value
     * @return            The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol molOriginal, DescResult descResult,
        Map properties) throws DescriptorException
    {
        AtomPairResult result = null;

        // check if the result type is correct
        if (!(descResult instanceof AtomPairResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                AtomPairResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (AtomPairResult) descResult;
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        // TODO : implement synchronized molecule cache (singleton class) with
        // TODO : protonated and deprotonated molecules !!!
        JOEMol mol = null;
        AtomProperties[] atomProperties;

        if (molCache != molOriginal)
        {
            // remove hydrogens
            mol = (JOEMol) molOriginal.clone();
            mol.deleteHydrogens();

            // get atom properties or calculate if not already available
            DescResult tmpPropResult;
            atomProperties = new AtomProperties[atomPropertyNames.length];

            for (int i = 0; i < atomPropertyNames.length; i++)
            {
                tmpPropResult = DescriptorHelper.instance().descFromMol(mol,
                        atomPropertyNames[i]);

                if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
                {
                    atomProperties[i] = (AtomProperties) tmpPropResult;
                }
                else
                {
                    // should never happen
                    logger.error("Property '" + atomPropertyNames[i] +
                        "' must be an atom type to calculate the " + DESC_KEY +
                        ".");

                    return null;
                }
            }
        }
        else
        {
            mol = this.molCacheDeprotonated;
            atomProperties = atomPropertiesCache;
        }

        // get distance matrix or calculate if not already available
        DescResult tmpResult = null;
        String distanceMatrixKey = "Distance_matrix";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol,
                    distanceMatrixKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate distance matrix for " + DESC_KEY +
                ".");

            return null;
        }

        if (!(tmpResult instanceof IntMatrixResult))
        {
            logger.error("Needed descriptor '" + distanceMatrixKey +
                "' should be of type " + IntMatrixResult.class.getName() +
                ". " + DESC_KEY + " can not be calculated.");

            return null;
        }

        IntMatrixResult distResult = (IntMatrixResult) tmpResult;
        int[][] distances = distResult.value;

        // calculate atom pair descriptor !!!
        AtomPair atomPair = null;
        Hashtable atomPairs = new Hashtable();
        int[] ia;

        for (int i = 0; i < distances.length; i++)
        {
            // visit only one triangle of the symmetric distance matrix
            for (int j = i; j < distances.length; j++)
            {
                atomPair = new AtomPair(atomProperties, mol.getAtom(i + 1),
                        mol.getAtom(j + 1), distances[i][j]);

                //				System.out.println(atomPair+" has hash code: "+atomPair.hashCode()+" stored:"+atomPairs.containsKey(atomPair));
                //				System.out.println(atomPairs);
                //				System.out.println("-----------------------------------");
                if (atomPairs.containsKey(atomPair))
                {
                    // increment atom pair counter
                    ia = (int[]) atomPairs.get(atomPair);
                    ia[0]++;
                }
                else
                {
                    // add new atom pair counter
                    atomPairs.put(atomPair, new int[]{1});

                    //					System.out.println("put "+atomPair);
                }
            }
        }

        // store informations
        result.atomPairs = atomPairs;
        result.atomPropertyNames = atomPropertyNames;

        // cache molecules and atom properties
        molCache = molOriginal;
        molCacheDeprotonated = mol;
        atomPropertiesCache = atomProperties;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
        atomPropertyNames = null;
        molCache = null;
        atomPropertiesCache = null;
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        String[] property = (String[]) JOEPropertyHelper.getProperty(this,
                ATOM_PROPERTIES, properties);

        if (property == null)
        {
            System.out.println("USING DEFAULT_ATOM_PROPERTIES");
            atomPropertyNames = DEFAULT_ATOM_PROPERTIES;
        }
        else
        {
       		atomPropertyNames = property;
        }
        
        return true;
    }

    /**
    * Test the implementation of this descriptor.
    *
    * @return <tt>true</tt> if the implementation is correct
    */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
