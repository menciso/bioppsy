///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomPairResult.java,v $
//  Purpose:  Atom pair descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types.atompair;

import java.io.LineNumberReader;
import java.io.StringReader;

import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DistanceMetricValue;

import joelib.io.IOType;

import joelib.math.similarity.DistanceMetric;
import joelib.math.similarity.DistanceMetricHelper;

import joelib.util.JHM;


/**
 * Atom pair descriptor (depends on atom properties used).
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/07/25 20:43:15 $
 */
public class AtomPairResult extends JOEPairData implements Cloneable,
    DescResult, DistanceMetricValue
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.atompair.AtomPairResult");
    private final static String DEFAULT_DISTANCE_METRIC_NAME = "BasicAPDistanceMetric";

    //	private final static String basicFormat =
    //		"notDefined";
    private final static String lineFormat = "numberOfAtomPairs p\n" +
        "numberOfAtomProperties n\n" + "<atom property 1>\n" + "...\n" +
        "<atom property n>\n" + "<atom property 1 of atom pair 1_a>\n" +
        "...\n" + "<atom property n of atom pair 1_a>\n" +
        "<distance of atom pair 1>\n" + "<atom property 1 of atom pair 1_b>\n" +
        "...\n" + "<atom property n of atom pair 1_b>\n" +
        "<occurence of atom pair 1>\n" + "...\n" + "...\n" +
        "<atom property 1 of atom pair p_a>\n" + "...\n" +
        "<atom property n of atom pair p_a>\n" + "<distance of atom pair 1>\n" +
        "<atom property 1 of atom pair p_b>\n" + "...\n" +
        "<atom property n of atom pair p_b>\n" + "<occurence of atom pair p>";

    //~ Instance fields ////////////////////////////////////////////////////////

    public Hashtable atomPairs;
    public String[] atomPropertyNames;
    private DistanceMetric metric;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    /**
     *  Constructor for the IntResult object
     */
    public AtomPairResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public double getDistance(Object target)
    {
        if (metric == null)
        {
            try
            {
                loadDefaultMetric();
            }
             catch (DescriptorException e)
            {
                logger.error(e.getMessage());

                return Double.NaN;
            }
        }

        return metric.getDistance(this, target);
    }

    public AtomPairResult clone(AtomPairResult _target)
    {
        _target.atomPairs = (Hashtable) atomPairs.clone();
        _target.atomPropertyNames = new String[atomPropertyNames.length];
        System.arraycopy(atomPropertyNames, 0, _target.atomPropertyNames, 0,
            atomPropertyNames.length);
        _target.metric = metric;

        return _target;
    }

    public Object clone()
    {
        AtomPairResult newObj = new AtomPairResult();

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        //		if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        //		{
        return lineFormat;

        //		}
        //		else
        //		{
        //			return basicFormat;
        //		}
    }

    /**
     *  Description of the Method
     *
     * @param  pairData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param  sValue  Description of the Parameter
     * @return         Description of the Return Value
     */
    public boolean fromString(IOType ioType, String sValue)
    {
        StringReader sr = new StringReader(sValue);
        LineNumberReader lnr = new LineNumberReader(sr);

        int numberOfAtomPairs = 0;
        int numberOfAtomProperties = 0;

        // get number of atom pairs and number of atom properties type
        try
        {
            numberOfAtomPairs = Integer.parseInt(lnr.readLine());
            numberOfAtomProperties = Integer.parseInt(lnr.readLine());

            atomPropertyNames = new String[numberOfAtomProperties];

            for (int i = 0; i < numberOfAtomProperties; i++)
            {
                atomPropertyNames[i] = lnr.readLine();
            }
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            return false;
        }

        atomPairs = new Hashtable(numberOfAtomPairs);

        AtomPair tmp;
        int occurence;

        for (int i = 0; i < numberOfAtomPairs; i++)
        {
            tmp = AtomPair.fromString(lnr, numberOfAtomProperties);

            if (tmp == null)
            {
                logger.error("Atom pair was not parsed successfully.");

                return false;
            }
            else
            {
                try
                {
                    occurence = Integer.parseInt(lnr.readLine());
                }
                 catch (Exception ex)
                {
                    logger.error(ex.getMessage());

                    return false;
                }

                atomPairs.put(tmp, new int[]{occurence});
            }

            //System.out.println("PARSED AP:"+tmp+" occured "+occurence+" times");
        }

        //		if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        //		{
        //		}
        //		else
        //		{
        //		}
        //System.out.println("PARSED: "+this.toString()+"///////////");
        return true;
    }

    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    public void loadDefaultMetric() throws DescriptorException
    {
        loadMetric(DEFAULT_DISTANCE_METRIC_NAME);
    }

    public void loadMetric(String represenation) throws DescriptorException
    {
        metric = DistanceMetricHelper.getDistanceMetric(represenation);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        StringBuffer sb = new StringBuffer();

        if (atomPairs == null)
        {
            logger.error("Contains no data");

            return null;
        }

        // store number of atom properties to faciliate reading and
        // parsing atom pair descriptors
        // we have n atom pair descriptors
        sb.append(atomPairs.size());
        sb.append(JHM.eol);

        // each atom pair with n atom properties
        sb.append(atomPropertyNames.length);
        sb.append(JHM.eol);

        for (int i = 0; i < this.atomPropertyNames.length; i++)
        {
            sb.append(atomPropertyNames[i]);
            sb.append(JHM.eol);
        }

        //		if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        //		{
        AtomPair key;

        for (Enumeration e = atomPairs.keys(); e.hasMoreElements();)
        {
            key = (AtomPair) e.nextElement();
            sb.append(key.toString(ioType));
            sb.append(JHM.eol);
            sb.append((((int[]) atomPairs.get(key))[0]));

            if (e.hasMoreElements())
            {
                sb.append(JHM.eol);
            }
        }

        //		}
        //		else
        //		{
        //			AtomPair key;
        //			for (Enumeration e = atomPairs.keys(); e.hasMoreElements();)
        //			{
        //				key = (AtomPair) e.nextElement();
        //				sb.append(key.toString(ioType));
        //				sb.append(' ');
        //				sb.append((((int[]) atomPairs.get(key))[0]));
        //				if (e.hasMoreElements())
        //				{
        //					sb.append(JHM.eol);
        //				}
        //			}
        //		}
        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
