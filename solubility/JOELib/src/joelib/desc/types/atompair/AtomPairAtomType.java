///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomPairAtomType.java,v $
//  Purpose:  Atom pair descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2004/07/25 20:43:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types.atompair;

import java.io.LineNumberReader;

import joelib.data.JOEElementTable;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;

import joelib.util.JHM;


/**
 * Atom type for pair descriptor (depends on atom properties used).
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/07/25 20:43:15 $
 */
public class AtomPairAtomType
{
    //~ Instance fields ////////////////////////////////////////////////////////

    public double[] atomProperties;
    public int atomicNumber;
    private int hash = 0;

    //~ Constructors ///////////////////////////////////////////////////////////

    public AtomPairAtomType(int _atomicNumber, double[] _atomProperties)
    {
        atomicNumber = _atomicNumber;

        atomProperties = new double[_atomProperties.length];
        System.arraycopy(_atomProperties, 0, atomProperties, 0,
            _atomProperties.length);
    }

    protected AtomPairAtomType()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public boolean equals(Object obj)
    {
        if (obj instanceof AtomPairAtomType)
        {
            return equals((AtomPairAtomType) obj);
        }
        else
        {
            return false;
        }
    }

    public boolean equals(AtomPairAtomType type)
    {
        if (atomicNumber != type.atomicNumber)
        {
            return false;
        }

        if (atomProperties.length != type.atomProperties.length)
        {
            return false;
        }

        for (int i = 0; i < atomProperties.length; i++)
        {
            if (atomProperties[i] != type.atomProperties[i])
            {
                return false;
            }
        }

        return true;
    }

    public static AtomPairAtomType fromString(LineNumberReader lnr,
        int numAtomProperties)
    {
        AtomPairAtomType type = new AtomPairAtomType();

        type.atomProperties = new double[numAtomProperties];

        try
        {
            type.atomicNumber = JOEElementTable.instance().getAtomicNum(lnr.readLine());

            for (int i = 0; i < numAtomProperties; i++)
            {
                type.atomProperties[i] = Double.parseDouble(lnr.readLine());
            }
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            return null;
        }

        //System.out.println("PARSED TYPE \""+type+"\"");
        return type;
    }

    public synchronized int hashCode()
    {
        if (hash != 0)
        {
            return hash;
        }

        int hashCode = atomicNumber;

        long bits;
        int tmpI;

        for (int i = 0; i < atomProperties.length; i++)
        {
            // similar code of Double.hashcode() !!!
            // without need to get a Double instance
            bits = Double.doubleToLongBits(atomProperties[i]);

            // unsigned right shift operator
            tmpI = (int) (bits ^ (bits >>> 32));

            hashCode = (31 * hashCode) + tmpI;
        }

        hash = hashCode;

        return hashCode;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */
    public synchronized int reHash()
    {
        hash = 0;

        return hashCode();
    }

    public String toString()
    {
        return toString(IOTypeHolder.instance().getIOType("UNDEFINED"));
    }

    public String toString(IOType ioType)
    {
        StringBuffer sb = new StringBuffer(100);
        int len = atomProperties.length;
        int len_1 = len - 1;

        //		if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        //		{
        sb.append(JOEElementTable.instance().getSymbol(atomicNumber));
        sb.append(JHM.eol);

        for (int i = 0; i < len; i++)
        {
            sb.append(atomProperties[i]);

            if (i < len_1)
            {
                sb.append(JHM.eol);
            }
        }

        //		}
        //		else
        //		{
        //			sb.append(JOEElementTable.instance().getSymbol(atomicNumber));
        //			sb.append('_');
        //			for (int i = 0; i < len; i++)
        //			{
        //				sb.append(atomProperties[i]);
        //				if (i < len_1)
        //					sb.append('_');
        //			}
        //		}
        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
