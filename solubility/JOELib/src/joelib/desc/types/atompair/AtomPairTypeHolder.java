///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomPairTypeHolder.java,v $
//  Purpose:  Atom pair descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2004/07/25 20:43:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types.atompair;

import java.util.Hashtable;

import joelib.molecule.JOEAtom;
import joelib.molecule.types.AtomProperties;


/**
 * Atom type singleton class to cache the types (depends on atom properties used).
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2004/07/25 20:43:15 $
 */
public class AtomPairTypeHolder
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private static AtomPairTypeHolder instance;

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable types = new Hashtable();

    //~ Constructors ///////////////////////////////////////////////////////////

    private AtomPairTypeHolder()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public static synchronized AtomPairTypeHolder instance()
    {
        if (instance == null)
        {
            instance = new AtomPairTypeHolder();
        }

        return instance;
    }

    public AtomPairAtomType getAPType(AtomProperties[] properties, JOEAtom atom)
    {
        double[] values = new double[properties.length];

        for (int i = 0; i < properties.length; i++)
        {
            values[i] = properties[i].getDoubleValue(atom.getIdx());
        }

        AtomPairAtomType type = new AtomPairAtomType(atom.getAtomicNum(), values);

        if (types.containsKey(type))
        {
            return (AtomPairAtomType) types.get(type);
        }
        else
        {
            types.put(type, type);
        }

        return type;
    }

    public AtomPairAtomType addReturn(AtomPairAtomType type)
    {
        if (type == null)
        {
            return null;
        }

        if (types.containsKey(type))
        {
            return (AtomPairAtomType) types.get(type);
        }
        else
        {
            types.put(type, type);
        }

        return type;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
