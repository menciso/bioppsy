///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ZagrebIndex2.java,v $
//  Purpose:  Calculates the Zagreb Group Index 2.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.iterator.BondIterator;


/**
 *  Calculates the Zagreb Group Index 2.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:15 $
 */
public class ZagrebIndex2 extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.ZagrebIndex2");
    public static final String DESC_KEY = "Zagreb_group_index_2";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the ZagrebIndex2 object
     */
    public ZagrebIndex2()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public double getDoubleValue(JOEMol mol)
    {
        double counter = 0;
        double atomDegree1;
        double atomDegree2;
        JOEAtom node1;
        JOEAtom node2;
        JOEBond bond;
        BondIterator bit = mol.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            node1 = bond.getBeginAtom();
            node2 = bond.getEndAtom();

            if ((!node1.isHydrogen()) && (!node2.isHydrogen())) //Graph must be H Atom depleted
            {
                atomDegree1 = node1.getBonds().size();
                atomDegree2 = node2.getBonds().size();
                counter += (atomDegree1 * atomDegree2);
            }
        }

        return counter;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
