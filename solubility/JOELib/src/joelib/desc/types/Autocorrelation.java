///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Autocorrelation.java,v $
//  Purpose:  Moreau-Broto autocorrelation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.14 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import java.util.Map;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.APropDoubleArrResult;
import joelib.desc.result.IntMatrixResult;

import joelib.molecule.JOEMol;
import joelib.molecule.types.AtomProperties;

import joelib.util.JOEHelper;
import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/**
 * Moreau-Broto autocorrelation.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.14 $, $Date: 2004/07/25 20:43:14 $
 * @cite bmv84
 */
public class Autocorrelation implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.Autocorrelation");
    public final static String ATOM_PROPERTY = "ATOM_PROPERTY";
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            new JOEProperty(ATOM_PROPERTY, "java.lang.String",
                "Atom property to use.", true, "Gasteiger_Marsili"),
        };
    public static final String DESC_KEY = "Auto_correlation";

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;
    private String propertyName;

    //~ Constructors ///////////////////////////////////////////////////////////

    public Autocorrelation()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.APropDoubleArrResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @param  _descInfo  The new descInfo value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     * @return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol molOriginal, DescResult descResult,
        Map properties) throws DescriptorException
    {
        APropDoubleArrResult result = null;

        // check if the result type is correct
        if (!(descResult instanceof APropDoubleArrResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                APropDoubleArrResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (APropDoubleArrResult) descResult;
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        if (molOriginal.empty())
        {
            result.value = new double[1];
            result.atomProperty = propertyName;
            logger.warn("Empty molecule '" + molOriginal.getTitle() + "'. " +
                DESC_KEY + " was set to ac[0]=0.");

            return result;
        }

        JOEMol mol = (JOEMol) molOriginal.clone(true, new String[]{propertyName});
        mol.deleteHydrogens();

        //System.out.print("propertyName: "+propertyName);
        // get distance matrix or calculate if not already available
        DescResult tmpResult;
        String distanceMatrixKey = "Distance_matrix";
        tmpResult = DescriptorHelper.instance().descFromMol(mol,
                distanceMatrixKey);

        if (!(tmpResult instanceof IntMatrixResult))
        {
            logger.error("Needed descriptor '" + distanceMatrixKey +
                "' should be of type " + IntMatrixResult.class.getName() +
                ". " + DESC_KEY + " can not be calculated.");

            return null;
        }

        IntMatrixResult distResult = (IntMatrixResult) tmpResult;
        int[][] distances = distResult.value;

        // get maximum distance value
        int maxDistance = -Integer.MAX_VALUE;

        for (int i = 0; i < distances.length; i++)
        {
            for (int ii = 0; ii < i; ii++)
            {
                if (maxDistance < distances[i][ii])
                {
                    maxDistance = distances[i][ii];
                }
            }
        }

        // get atom properties or calculate if not already available
        DescResult tmpPropResult;
        tmpPropResult = DescriptorHelper.instance().descFromMol(mol,
                propertyName);

        AtomProperties atomProperties;

        if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
        {
            atomProperties = (AtomProperties) tmpPropResult;
        }
        else
        {
            logger.error("Property '" + propertyName +
                "' must be an atom type to calculate the " + DESC_KEY + ".");

            return null;
        }

        // calculate autocorrelation
        //		System.out.println("maxDistance:"+maxDistance);
        if (maxDistance < 0)
        {
            logger.warn("Possibly invalid molecule or only one atom in " +
                mol.getTitle());
            maxDistance = 0;

            //return null;
        }

        if (maxDistance == Integer.MAX_VALUE)
        {
            logger.warn("Some atoms which where never visited by BFS in " +
                mol.getTitle());

            //maxDistance=0;
            return null;
        }

        double[] acValues = new double[maxDistance + 1];
        double value;

        //     System.out.println("atoms:"+mol.numAtoms()+" distances:"+distances.length);
        for (int i = 0; i < distances.length; i++)
        {
            for (int ii = 0; ii <= i; ii++)
            {
                value = atomProperties.getDoubleValue(i + 1) * atomProperties.getDoubleValue(ii +
                        1);
                acValues[distances[i][ii]] += value;
                acValues[distances[ii][i]] += value;
            }

            //System.out.print(" "+atomProperties.getDoubleValue(i + 1));
        }

        //System.out.println("");
        //		for (int i = 0; i < acValues.length; i++) {
        //			System.out.print(" "+acValues[i]);
        //		}
        //		System.out.println("");
        // save result
        result.value = acValues;
        result.atomProperty = propertyName;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        String property = (String) JOEPropertyHelper.getProperty(this,
                ATOM_PROPERTY, properties);

        if (property == null)
        {
            propertyName = "Gasteiger_Marsili";
        }
        else
        {
            propertyName = property;
        }

        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
