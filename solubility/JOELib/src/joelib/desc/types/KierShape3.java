///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: KierShape3.java,v $
//  Purpose:  Calculates the Kier Shape for paths with length three.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.NbrAtomIterator;


/**
 *  Calculates the Kier Shape for paths with length three.
 *
 * @author    Jan Bruecker
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:14 $
 */
public class KierShape3 extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.KierShape3");
    public static final String DESC_KEY = "Kier_shape_3";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape3 object
     */
    public KierShape3()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the doubleValue attribute of the KierShape3 object
     *
     * @param mol  Description of the Parameter
     * @return     The doubleValue value
     */
    public double getDoubleValue(JOEMol mol)
    {
        double nodes = 0;
        double paths = 0;
        AtomIterator ait = mol.atomIterator();
        NbrAtomIterator nbrait;
        NbrAtomIterator nbrait2;
        NbrAtomIterator nbrait3;
        JOEAtom atom;
        JOEAtom nbrAtom;
        JOEAtom nbrAtom2;
        JOEAtom nbrAtom3;
        double kier;

        while (ait.hasNext())
        {
            //Iterates over all nodes
            atom = ait.nextAtom();

            //"node" is the current node of the Iteration
            if (!atom.isHydrogen())
            {
                //Graph should be H-Atom depleted
                nodes++;
                nbrait = atom.nbrAtomIterator();

                while (nbrait.hasNext())
                {
                    //Iterates over all edges of the current "node"
                    nbrAtom = nbrait.nextNbrAtom();

                    if (!nbrAtom.isHydrogen())
                    {
                        nbrait2 = nbrAtom.nbrAtomIterator();

                        while (nbrait2.hasNext())
                        {
                            nbrAtom2 = nbrait2.nextNbrAtom();

                            if ((!nbrAtom2.isHydrogen()) &&
                                    (nbrAtom2.getIdx() != atom.getIdx()))
                            {
                                nbrait3 = nbrAtom2.nbrAtomIterator();

                                while (nbrait3.hasNext())
                                {
                                    nbrAtom3 = nbrait3.nextNbrAtom();

                                    if ((!nbrAtom3.isHydrogen()) &&
                                            (nbrAtom3.getIdx() != nbrAtom.getIdx()))
                                    {
                                        paths++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        paths = paths / 2;

        //each path has been counted twice, so divide by two
        //System.out.println("Kier 3 paths: " +paths +"\n Knoten: " +nodes);
        if (paths > 0)
        {
            if ((nodes % 2) == 0)
            {
                kier = (((nodes - 3) * ((nodes - 2) * (nodes - 2))) / (paths * paths));
            }
            else
            {
                kier = (((nodes - 1) * ((nodes - 3) * (nodes - 3))) / (paths * paths));
            }
        }
        else
        {
            return 0.0;
        }

        return kier;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
