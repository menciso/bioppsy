///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GeometricalShapeCoefficient.java,v $
//  Purpose:  Calculates the geometrical shape coefficient.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;
import joelib.desc.result.DoubleResult;

import joelib.molecule.JOEMol;


/**
 * Calculates the geometrical shape coefficient.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:14 $
 */
public class GeometricalShapeCoefficient extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.GeometricalShapeCoefficient");
    public static final String DESC_KEY = "Geometrical_shape_coefficient";

    //~ Constructors ///////////////////////////////////////////////////////////

    public GeometricalShapeCoefficient()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_GEOMETRICAL, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the defaultAtoms attribute of the NumberOfC object
     *
     * @return   The defaultAtoms value
     */
    public double getDoubleValue(JOEMol mol)
    {
        // get topological diameter or calculate if not already available
        DescResult tmpResult = null;
        String diameterKey = "Geometrical_diameter";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol, diameterKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error(
                "Can not calculate geometrical diameter for geometrical shape coefficient.");

            return 0;
        }

        if (!(tmpResult instanceof DoubleResult))
        {
            logger.error("Needed descriptor '" + diameterKey +
                "' should be of type " + DoubleResult.class.getName() +
                ". Geometrical shape coefficient can not be calculated.");

            return 0;
        }

        DoubleResult diameterResult = (DoubleResult) tmpResult;
        double diameter = (double) diameterResult.value;

        // get topological radius or calculate if not already available
        //DescResult tmpResult=null;
        String radiusKey = "Geometrical_radius";

        try
        {
            tmpResult = DescriptorHelper.instance().descFromMol(mol, radiusKey);
        }
         catch (DescriptorException ex)
        {
            logger.error(ex.toString());
            logger.error("Can not calculate geometrical radius for " +
                DESC_KEY + ".");

            return 0;
        }

        if (!(tmpResult instanceof DoubleResult))
        {
            logger.error("Needed descriptor '" + radiusKey +
                "' should be of type " + DoubleResult.class.getName() + ". " +
                DESC_KEY + " can not be calculated.");

            return 0;
        }

        DoubleResult radiusResult = (DoubleResult) tmpResult;
        double radius = (double) radiusResult.value;

        double shape = (diameter - radius) / radius;

        return shape;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
