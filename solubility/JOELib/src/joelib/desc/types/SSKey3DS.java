///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SSKey3DS.java,v $
//  Purpose:  Pharmacophore fingerprint.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorFactory;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.BitResult;
import joelib.desc.result.DoubleResult;
import joelib.desc.result.IntResult;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.ring.JOERing;

import joelib.smarts.JOESmartsPattern;

import joelib.util.JOEBitVec;
import joelib.util.JOEProperty;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;


/**
 * Pharmacophore fingerprint.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:43:14 $
 * @cite gxsb00
 */
public class SSKey3DS implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.SSKey3DS");
    private static Hashtable smartsPatterns = null;
    public static final String DESC_KEY = "Pharmacophore_fingerprint_1";

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;
    private String aromaticBondsName = "Number_of_aromatic_bonds";
    private String fracRotBondsName = "Fraction_of_rotatable_bonds";
    private String hbaDescriptorName = "Number_of_HBA_1";
    private String hbdDescriptorName = "Number_of_HBD_2";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape2 object
     */
    public SSKey3DS()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.BitResult");

        if (smartsPatterns == null)
        {
            smartsPatterns = getFingerprintPatterns();
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @param  _descInfo  The new descInfo value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     * @return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        if (!(descResult instanceof BitResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                BitResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());
        }

        if (mol.empty())
        {
            JOEBitVec fp = new JOEBitVec(54);
            BitResult result = (BitResult) descResult;
            result.value = fp;
            result.maxBitSize = 54;
            logger.warn("Empty molecule '" + mol.getTitle() +
                "'. No bits were set.");

            return result;
        }

        Descriptor hbd = null;
        Descriptor hba = null;
        Descriptor fracRotBonds = null;
        Descriptor aromaticBonds = null;

        try
        {
            hbd = DescriptorFactory.getDescriptor(hbdDescriptorName);
            hba = DescriptorFactory.getDescriptor(hbaDescriptorName);
            fracRotBonds = DescriptorFactory.getDescriptor(fracRotBondsName);
            aromaticBonds = DescriptorFactory.getDescriptor(aromaticBondsName);
        }
         catch (DescriptorException ex)
        {
            ex.printStackTrace();

            return null;
        }

        IntResult hbdResult = null;
        IntResult hbaResult = null;
        IntResult aroResult = null;
        DoubleResult frbResult = null;

        try
        {
            //calculate hydrogen bond donors
            hbdResult = (IntResult) hbd.calculate(mol);

            if (hbdResult == null)
            {
                logger.error("Hydrogen bond donors " + hbdDescriptorName +
                    " can't be calculated.");

                return null;
            }

            //calculate hydrogen bond acceptors
            hbaResult = (IntResult) hba.calculate(mol);

            if (hbaResult == null)
            {
                logger.error("Hydrogen bond acceptors " + hbaDescriptorName +
                    " can't be calculated.");

                return null;
            }

            //calculate fraction of rotatable bonds
            frbResult = (DoubleResult) fracRotBonds.calculate(mol);

            if (frbResult == null)
            {
                logger.error("Fraction of rotatable bonds " + fracRotBondsName +
                    " can't be calculated.");

                return null;
            }

            //calculate number of aromatic bonds
            aroResult = (IntResult) aromaticBonds.calculate(mol);

            if (aroResult == null)
            {
                logger.error("Number of aromatic bonds " + aromaticBondsName +
                    " can't be calculated.");

                return null;
            }
        }
         catch (DescriptorException ex)
        {
            ex.printStackTrace();
        }

        JOEBitVec fp = new JOEBitVec(54);

        /////////////////////////////
        // FRB
        /////////////////////////////
        if (frbResult.value > 0.0)
        {
            //1:  Fraction of rotatable bonds: 0.0 < x <= 0.1 (0)
            fp.set(0);
        }

        if (frbResult.value > 0.1)
        {
            //2:  Fraction of rotatable bonds: 0.1 < x <= 0.2
            fp.set(1);
        }

        if (frbResult.value > 0.2)
        {
            //3:  Fraction of rotatable bonds: 0.2 < x <= 0.3
            fp.set(2);
        }

        if (frbResult.value > 0.3)
        {
            //4:  Fraction of rotatable bonds: 0.3 < x <= 0.4
            fp.set(3);
        }

        if (frbResult.value > 0.4)
        {
            //5:  Fraction of rotatable bonds:       x > 0.4
            fp.set(4);
        }

        ////////////////////////////
        // ARB
        ////////////////////////////
        if (aroResult.getInt() >= 2)
        {
            //6:  Aromatic bonds in molecule:  2 - 7  (0-1)
            fp.set(5);
        }

        if (aroResult.getInt() >= 8)
        {
            //7:  Aromatic bonds in molecule:  8 -15
            fp.set(6);
        }

        if (aroResult.getInt() >= 16)
        {
            //8:  Aromatic bonds in molecule:  16-19
            fp.set(7);
        }

        if (aroResult.getInt() >= 20)
        {
            //9:  Aromatic bonds in molecule:  20-25
            fp.set(8);
        }

        if (aroResult.getInt() >= 26)
        {
            //10: Aromatic bonds in molecule:  26-31
            fp.set(9);
        }

        if (aroResult.getInt() >= 32)
        {
            //11: Aromatic bonds in molecule:  32-37
            fp.set(10);
        }

        if (aroResult.getInt() >= 38)
        {
            //12: Aromatic bonds in molecule:  >= 38
            fp.set(11);
        }

        //////////////////////////////
        // SSKeys
        //////////////////////////////
        if (hasHeteroCycle(mol))
        {
            //13: heterocycle
            fp.set(12);
        }

        if (hasSMARTSPattern(mol, "13"))
        {
            //14: aromatic OH
            fp.set(13);
        }

        if (hasSMARTSPattern(mol, "14"))
        {
            //15: aliphatic OH
            fp.set(14);
        }

        if (hasSMARTSPattern(mol, "15"))
        {
            //16: aliphatic secondary amine
            fp.set(15);
        }

        if (hasSMARTSPattern(mol, "16"))
        {
            //17: aliphatic tertiary amine
            fp.set(16);
        }

        if (hasSMARTSPattern(mol, "17"))
        {
            //18: phenyl ring
            fp.set(17);
        }

        if (hasNRing(mol))
        {
            //19: Nitrogen-containing ring
            fp.set(18);
        }

        if (hasSMARTSPattern(mol, "19"))
        {
            //20: -SO2
            fp.set(19);
        }

        if (hasSMARTSPattern(mol, "20"))
        {
            //21: -SO
            fp.set(20);
        }

        if (hasSMARTSPattern(mol, "21"))
        {
            //22: ester
            fp.set(21);
        }

        if (hasSMARTSPattern(mol, "22"))
        {
            //23: amide
            fp.set(22);
        }

        if (hasNonAromatic5Ring(mol))
        {
            //24: 5-membered non-aromatic ring
            fp.set(23);
        }

        if (hasAromatic5Ring(mol))
        {
            //25: 5-membered aromatic ring
            fp.set(24);
        }

        if (hasRingGreater9(mol))
        {
            //26: 9-membered or larger (fused) ring
            fp.set(25);
        }

        if (hasSMARTSPattern(mol, "26"))
        {
            //27: fused ring system
            fp.set(26);
        }

        if (hasSMARTSPattern(mol, "27"))
        {
            //28: fused aromatic ring system
            fp.set(27);
        }

        if (hasSMARTSPattern(mol, "28"))
        {
            //29: -OSO
            fp.set(28);
        }

        if (hasHalogen(mol))
        {
            //30: halogen atom
            fp.set(29);
        }

        if (hasSMARTSPattern(mol, "30"))
        {
            //31: Nitrogen attached to alpha-carbon of aromatic system
            fp.set(30);
        }

        if (hasSMARTSPattern(mol, "31"))
        {
            //32: -NO2
            fp.set(31);
        }

        if (hasSMARTSPattern(mol, "32"))
        {
            //33: rings separated by 2-3 non-ring atoms
            fp.set(32);
        }

        if (hasSMARTSPattern(mol, "33"))
        {
            //34: rings separated by 4-5 non-ring atoms
            fp.set(33);
        }

        if (hasSMARTSPattern(mol, "34"))
        {
            //35: NN
            fp.set(34);
        }

        if (hasSMARTSPattern(mol, "35"))
        {
            //36: C attached to 3 carbons and a hetero atom
            fp.set(35);
        }

        if (hasSMARTSPattern(mol, "36"))
        {
            //37: oxygen separated by 2 atoms
            fp.set(36);
        }

        if (hasSMARTSPattern(mol, "37"))
        {
            //38: methyl attached to hetero atom
            fp.set(37);
        }

        if (hasDoubleBond(mol))
        {
            //39: double bond
            fp.set(38);
        }

        if (hasSMARTSPattern(mol, "39"))
        {
            //40: Non-H atom linked to 3 heteroatoms
            fp.set(39);
        }

        if (hasSMARTSPattern(mol, "40"))
        {
            //41: Quaternary atom
            fp.set(40);
        }

        if (hasSMARTSPattern(mol, "41"))
        {
            //42: 2 methylenes separated by 2 atoms
            fp.set(41);
        }

        if (hasSMARTSPattern(mol, "42"))
        {
            //43: non-ring oxygen attached to aromatic system
            fp.set(42);
        }

        if (hasSMARTSPattern(mol, "43"))
        {
            //44: 2 non-C,H atoms separated by 2 atoms
            fp.set(43);
        }

        ///////////////////////////////
        // HBA
        ///////////////////////////////
        if (hbaResult.getInt() >= 1)
        {
            //45: HBA=1 (0)
            fp.set(44);
        }

        if (hbaResult.getInt() >= 2)
        {
            //46: HBA=2
            fp.set(45);
        }

        if (hbaResult.getInt() >= 3)
        {
            //47: HBA=3
            fp.set(46);
        }

        if (hbaResult.getInt() >= 4)
        {
            //48: HBA=4
            fp.set(47);
        }

        if (hbaResult.getInt() >= 5)
        {
            //49: HBA=5
            fp.set(48);
        }

        if (hbaResult.getInt() >= 6)
        {
            //50: HBA=6
            fp.set(49);
        }

        if (hbaResult.getInt() >= 7)
        {
            //51: HBA=7
            fp.set(50);
        }

        if (hbaResult.getInt() >= 8)
        {
            //52: HBA=8
            fp.set(51);
        }

        if (hbaResult.getInt() >= 9)
        {
            //53: HBA=9
            fp.set(52);
        }

        if (hbaResult.getInt() >= 10)
        {
            //54: HBA>=10
            fp.set(53);
        }

        BitResult result = (BitResult) descResult;
        result.value = fp;
        result.maxBitSize = 54;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        return true;
    }

    /**
    * Test the implementation of this descriptor.
    *
    * @return <tt>true</tt> if the implementation is correct
    */
    public boolean testDescriptor()
    {
        return true;
    }

    private static Hashtable getFingerprintPatterns()
    {
        Hashtable smarts = new Hashtable();

        initSingleSMARTS(smarts, "13", "[OX2;H1;$(O-a)]");
        initSingleSMARTS(smarts, "14", "[OX2;H1;$(O-C)]");
        initSingleSMARTS(smarts, "15", "[NX3;H1;$(N(~C)~C)]");
        initSingleSMARTS(smarts, "16", "[NQ3;H0;$(N(~C)(~C)~C)]");
        initSingleSMARTS(smarts, "17", "[Cc1ccccc1]");
        initSingleSMARTS(smarts, "19", "[#16Q4;$(S(=O)(=O))]-*");
        initSingleSMARTS(smarts, "20", "[#16Q2;$(S~O);!$(S(~O)~O)]-*");
        initSingleSMARTS(smarts, "21", "C([OQ2])=O");
        initSingleSMARTS(smarts, "22", "C([NX3])=O");
        initSingleSMARTS(smarts, "26", "[!a;R2]");
        initSingleSMARTS(smarts, "27", "[a;R2]");
        initSingleSMARTS(smarts, "28", "[#16Q2;$(S(~O)~O)]-*");
        initSingleSMARTS(smarts, "30", "cC~N");
        initSingleSMARTS(smarts, "31", "[#7Q3;$(N(~O)~O)]-*");
        initSingleSMARTS(smarts, "32",
            "[$([*;R][*;!R][*;!R][*;R]),$([*;R][*;!R][*;!R][*;!R][*;R])]");
        initSingleSMARTS(smarts, "33",
            "[$([*;R][*;!R][*;!R][*;!R][*;!R][*;R]),$([*;R][*;!R][*;!R][*;!R][*;!R][*;!R][*;R])]");
        initSingleSMARTS(smarts, "34", "[NN]");
        initSingleSMARTS(smarts, "35", "C([#6])([#6])[#6][*;!C;!H]");
        initSingleSMARTS(smarts, "36", "[#8]**[#8]");
        initSingleSMARTS(smarts, "37", "[C;H3][*;!C;!H]");
        initSingleSMARTS(smarts, "39",
            "[*X3;!H]([*;!$([#6]);!H])([*;!$([#6]);!H])[*;!$([#6]);!H]");
        initSingleSMARTS(smarts, "40", "[*X4]");
        initSingleSMARTS(smarts, "41", "*=C~*~*~C=*");
        initSingleSMARTS(smarts, "42", "[O;R0]~a");
        initSingleSMARTS(smarts, "43", "[!#6;!H]~*~*~[!#6;!H]");

        return smarts;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private boolean hasAromatic5Ring(JOEMol mol)
    {
        Vector sssRings = mol.getSSSR();
        JOERing ring;

        for (int i = 0; i < sssRings.size(); i++)
        {
            ring = (JOERing) sssRings.get(i);

            if (ring.size() == 5)
            {
                if (ring.isAromatic())
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private boolean hasDoubleBond(JOEMol mol)
    {
        BondIterator bit = mol.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.getBondOrder() == 2)
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    private boolean hasHalogen(JOEMol mol)
    {
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        int atomicNum;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atomicNum = atom.getAtomicNum();

            if (atom.isHalogen())
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private boolean hasHeteroCycle(JOEMol mol)
    {
        Vector sssRings = mol.getSSSR();
        JOERing ring;

        for (int i = 0; i < sssRings.size(); i++)
        {
            ring = (JOERing) sssRings.get(i);

            if (ring.isHetero())
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private boolean hasNRing(JOEMol mol)
    {
        Vector sssRings = mol.getSSSR();
        JOERing ring;
        int[] atomIDs;

        for (int i = 0; i < sssRings.size(); i++)
        {
            ring = (JOERing) sssRings.get(i);
            atomIDs = ring.getAtoms();

            for (int j = 0; j < atomIDs.length; j++)
            {
                if ((mol.getAtom(atomIDs[j])).getAtomicNum() == 7)
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private boolean hasNonAromatic5Ring(JOEMol mol)
    {
        Vector sssRings = mol.getSSSR();
        JOERing ring;

        for (int i = 0; i < sssRings.size(); i++)
        {
            ring = (JOERing) sssRings.get(i);

            if (ring.size() == 5)
            {
                if (!ring.isAromatic())
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    private boolean hasRingGreater9(JOEMol mol)
    {
        Vector sssRings = mol.getSSSR();
        JOERing ring;

        for (int i = 0; i < sssRings.size(); i++)
        {
            ring = (JOERing) sssRings.get(i);

            if (ring.size() > 9)
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @param  mol           Description of the Parameter
     * @param  smartPattern  Description of the Parameter
     * @return               Description of the Return Value
     */
    private boolean hasSMARTSPattern(JOEMol mol, String id)
    {
        JOESmartsPattern smarts = (JOESmartsPattern) smartsPatterns.get(id);

        if (smarts == null)
        {
            logger.error("ID '" + id + "' is missing in " +
                SSKey3DS.class.getName());

            return false;
        }

        smarts.match(mol);

        if (smarts.numMatches() > 0)
        {
            return true;
        }

        return false;
    }

    private static void initSingleSMARTS(Hashtable table, String id,
        String smartPattern)
    {
        JOESmartsPattern smarts = new JOESmartsPattern();

        if (!smarts.init(smartPattern))
        {
            logger.error("Invalid SMARTS pattern (id:" + id + ") '" +
                smartPattern + "' defined in " + SSKey3DS.class.getName());

            return;
        }

        table.put(id, smarts);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
