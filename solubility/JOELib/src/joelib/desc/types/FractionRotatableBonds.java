///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: FractionRotatableBonds.java,v $
//  Purpose:  Fraction of rotatable bonds.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;

import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.iterator.BondIterator;


/**
 * Fraction of rotatable bonds.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:14 $
 */
public class FractionRotatableBonds extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.FractionRotatableBonds");
    public static final String DESC_KEY = "Fraction_of_rotatable_bonds";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape2 object
     */
    public FractionRotatableBonds()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the defaultAtoms attribute of the NumberOfC object
     *
     * @return   The defaultAtoms value
     */
    public double getDoubleValue(JOEMol mol)
    {
        int heavyBonds = 0;
        int rotatableBonds = 0;

        BondIterator bit = mol.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            //Molecule graph must be H-Atom depleted
            bond = bit.nextBond();

            if (!bond.getBeginAtom().isHydrogen() &&
                    !bond.getEndAtom().isHydrogen())
            {
                heavyBonds++;

                if (bond.isRotor())
                {
                    rotatableBonds++;
                }
            }
        }

        if (heavyBonds == 0)
        {
            return 0.0;
        }
        else
        {
            return (double) rotatableBonds / (double) heavyBonds;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
