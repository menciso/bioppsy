///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ZagrebIndex1.java,v $
//  Purpose:  Calculates the Zagreb Group Index 1.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.9 $
//            $Date: 2004/07/25 20:43:15 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleDesc;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;


/**
 *  Calculates the Zagreb Group Index 1.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/07/25 20:43:15 $
 */
public class ZagrebIndex1 extends SimpleDoubleDesc
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.ZagrebIndex1");
    public static final String DESC_KEY = "Zagreb_group_index_1";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the ZagrebIndex1 object
     */
    public ZagrebIndex1()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the doubleValue attribute of the ZagrebIndex1 object
     *
     * @param mol  Description of the Parameter
     * @return     The doubleValue value
     */
    public double getDoubleValue(JOEMol mol)
    {
        double counter = 0;
        double atomDegree;
        JOEAtom node;
        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            node = ait.nextAtom();

            if (!node.isHydrogen())
            {
                atomDegree = node.getBonds().size();
                counter += (atomDegree * atomDegree);
            }
        }

        return counter;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
