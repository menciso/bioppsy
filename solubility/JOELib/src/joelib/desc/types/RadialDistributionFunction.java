///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: RadialDistributionFunction.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import wsi.ra.tool.PropertyHolder;

import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.APropDoubleArrResult;
import joelib.desc.result.DoubleMatrixResult;

import joelib.molecule.JOEMol;
import joelib.molecule.types.AtomProperties;

import joelib.util.JOEHelper;
import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;


/**
 * Radial Basis Function (RDF).
 *
 * @author     wegnerj
 */
public class RadialDistributionFunction implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.RadialDistributionFunction");
    public final static String ATOM_PROPERTY = "ATOM_PROPERTY";
    public final static String MIN_SPHERICAL_VOLUME = "MIN_SPHERICAL_VOLUME";
    public final static String MAX_SPHERICAL_VOLUME = "MAX_SPHERICAL_VOLUME";
    public final static String SPHERICAL_VOLUME_RESOLUTION = "SPHERICAL_VOLUME_RESOLUTION";
    public final static String SMOOTHING_FACTOR = "SMOOTHING_FACTOR";
    public final static String REMOVE_HYDROGENS = "REMOVE_HYDROGENS";
    public final static JOEProperty ATOM_PROPERTY_PROPERTY = new JOEProperty(ATOM_PROPERTY,
            "java.lang.String", "Atom property to use.", true,
            "Gasteiger_Marsili");
    public final static JOEProperty MIN_SPHERICAL_VOLUME_PROPERTY = new JOEProperty(MIN_SPHERICAL_VOLUME,
            "java.lang.Double", "Minimum spherical volume radius.", true,
            new Double(0.2));
    public final static JOEProperty MAX_SPHERICAL_VOLUME_PROPERTY = new JOEProperty(MAX_SPHERICAL_VOLUME,
            "java.lang.Double", "Maximum spherical volume radius.", true,
            new Double(10.0));
    public final static JOEProperty SPHERICAL_VOLUME_RESOLUTION_PROPERTY = new JOEProperty(SPHERICAL_VOLUME_RESOLUTION,
            "java.lang.Double",
            "Resolution to use for the spherical volume radius.", true,
            new Double(0.2));
    public final static JOEProperty SMOOTHING_FACTOR_PROPERTY = new JOEProperty(SMOOTHING_FACTOR,
            "java.lang.Double",
            "Smoothing parameter for the interatomic distances.", true,
            new Double(0.2));
    public final static JOEProperty REMOVE_HYDROGENS_PROPERTY = new JOEProperty(REMOVE_HYDROGENS,
            "java.lang.Boolean",
            "Remove hydrogens before calculating radial basis methodName.",
            true, Boolean.TRUE);
    private final static JOEProperty[] ACCEPTED_PROPERTIES = new JOEProperty[]
        {
            ATOM_PROPERTY_PROPERTY, MIN_SPHERICAL_VOLUME_PROPERTY,
            MAX_SPHERICAL_VOLUME_PROPERTY, SPHERICAL_VOLUME_RESOLUTION_PROPERTY,
            SMOOTHING_FACTOR_PROPERTY, REMOVE_HYDROGENS_PROPERTY
        };

    //public static final String DESC_KEY = "Radial_distribution_function";
    public static final String DESC_KEY = "RDF";

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;
    private String propertyName;
    private boolean removeHydrogens = true;
    private double maxSphericalVolume = 10.0;
    private double minSphericalVolume = 0.2;
    private double smoothingFactorB = 25;
    private double sphericalVolumeResolution = 0.2;

    //~ Constructors ///////////////////////////////////////////////////////////

    public RadialDistributionFunction()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.APropDoubleArrResult");

        PropertyHolder pHolder = PropertyHolder.instance();
        Properties prop = pHolder.getProperties();
        String className = this.getClass().getName();
        String valueS;
        double valueD;

        valueD = PropertyHolder.getDouble(prop,
                className + ".minSphericalVolume", 0.1, 50, 0.2);

        if (!Double.isNaN(valueD))
        {
            minSphericalVolume = valueD;
            MIN_SPHERICAL_VOLUME_PROPERTY.setDefaultProperty(new Double(
                    minSphericalVolume));

            if (logger.isDebugEnabled())
            {
                logger.debug("Set minSphericalVolume=" + minSphericalVolume);
            }
        }

        valueD = PropertyHolder.getDouble(prop,
                className + ".maxSphericalVolume", minSphericalVolume, 100, 10);

        if (!Double.isNaN(valueD))
        {
            maxSphericalVolume = valueD;
            MAX_SPHERICAL_VOLUME_PROPERTY.setDefaultProperty(new Double(
                    maxSphericalVolume));

            if (logger.isDebugEnabled())
            {
                logger.debug("Set maxSphericalVolume=" + maxSphericalVolume);
            }
        }

        valueD = PropertyHolder.getDouble(prop,
                className + ".sphericalVolumeResolution", 0.01, 0.5, 0.2);

        if (!Double.isNaN(valueD))
        {
            sphericalVolumeResolution = valueD;
            SPHERICAL_VOLUME_RESOLUTION_PROPERTY.setDefaultProperty(new Double(
                    sphericalVolumeResolution));

            if (logger.isDebugEnabled())
            {
                logger.debug("Set sphericalVolumeResolution=" +
                    sphericalVolumeResolution);
            }
        }

        valueD = PropertyHolder.getDouble(prop, className + ".smoothingFactor",
                1.0, 100000.0, 25.0);

        if (!Double.isNaN(valueD))
        {
            smoothingFactorB = valueD;
            SMOOTHING_FACTOR_PROPERTY.setDefaultProperty(new Double(
                    smoothingFactorB));

            if (logger.isDebugEnabled())
            {
                logger.debug("Set smoothingFactor=" + smoothingFactorB);
            }
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     * @param  _descInfo  The new descInfo value
     */

    //  public void setDescInfo(DescriptorInfo _descInfo)
    //  {
    //    descInfo = _descInfo;
    //  }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     * @return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return ACCEPTED_PROPERTIES;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @param  initData                 Description of the Parameter
     * @param  descResult               Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol molOriginal, DescResult descResult,
        Map properties) throws DescriptorException
    {
        APropDoubleArrResult result = null;

        // check if the result type is correct
        if (!(descResult instanceof APropDoubleArrResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                APropDoubleArrResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());

            return null;
        }

        // initialize result type, if not already initialized
        else
        {
            result = (APropDoubleArrResult) descResult;
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        if (molOriginal.empty())
        {
            result.value = new double[1];
            result.atomProperty = propertyName;
            logger.warn("Empty molecule '" + molOriginal.getTitle() + "'. " +
                DESC_KEY + " was set to ac[0]=0.");

            return result;
        }

        JOEMol mol = null;

        if (removeHydrogens)
        {
            mol = (JOEMol) molOriginal.clone(true, new String[]{propertyName});
            mol.deleteHydrogens();
        }
        else
        {
            mol = molOriginal;
        }

        //System.out.print("propertyName: "+propertyName);
        // get distance matrix or calculate if not already available
        DescResult tmpResult;
        String distanceMatrixKey = "Geometrical_distance_matrix";
        tmpResult = DescriptorHelper.instance().descFromMol(mol,
                distanceMatrixKey);

        if (!(tmpResult instanceof DoubleMatrixResult))
        {
            logger.error("Needed descriptor '" + distanceMatrixKey +
                "' should be of type " + DoubleMatrixResult.class.getName() +
                ". " + DESC_KEY + " can not be calculated.");

            return null;
        }

        DoubleMatrixResult distResult = (DoubleMatrixResult) tmpResult;
        double[][] distances = distResult.value;

        //		double maxDistance = -Double.MAX_VALUE;
        //		for (int i = 0; i < distances.length; i++)
        //		{
        //			for (int ii = 0; ii < i; ii++)
        //			{
        //				if (maxDistance < distances[i][ii])
        //					maxDistance = distances[i][ii];
        //			}
        //		}
        //		System.out.println("Max distance: "+maxDistance);
        // get number of intervals
        int intervals = (int) ((maxSphericalVolume - minSphericalVolume) / sphericalVolumeResolution);

        // get atom properties or calculate if not already available
        DescResult tmpPropResult;
        tmpPropResult = DescriptorHelper.instance().descFromMol(mol,
                propertyName);

        //System.out.println("Use atom property: "+propertyName);
        AtomProperties atomProperties;

        if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
        {
            atomProperties = (AtomProperties) tmpPropResult;
        }
        else
        {
            logger.error("Property '" + propertyName +
                "' must be an atom type to calculate the " + DESC_KEY + ".");

            return null;
        }

        // calculate radial basis methodName (RDF)
        int atoms = mol.numAtoms();
        int atoms_1 = mol.numAtoms() - 1;
        double[] rdfValues = new double[intervals + 1];
        double aPropWeights;
        double eTerm;
        double scalingFactor = 0.0;
        int index = 0;
        double tmpRDF = 0.0;

        //     System.out.println("atoms:"+mol.numAtoms()+" distances:"+distances.length);
        for (double r = minSphericalVolume; r <= maxSphericalVolume;
                r += sphericalVolumeResolution)
        {
            tmpRDF = 0.0;

            for (int i = 0; i < atoms_1; i++)
            {
                for (int j = i + 1; j < atoms; j++)
                {
                    aPropWeights = atomProperties.getDoubleValue(i + 1) * atomProperties.getDoubleValue(j +
                            1);

                    eTerm = (r - distances[i][j]);
                    eTerm *= eTerm;
                    tmpRDF += (aPropWeights * Math.exp(-smoothingFactorB * eTerm));
                }
            }

            rdfValues[index] = tmpRDF;

            //			scalingFactor += tmpRDF * tmpRDF;
            index++;
        }

        // this SCALING makes sense for Kohonen networks, but is not
        // necessary before we know what we want to do with these
        // descriptors
        //		scalingFactor = 1 / Math.sqrt(scalingFactor);
        //		for (int i = 0; i < rdfValues.length; i++)
        //		{
        //			rdfValues[i] *= scalingFactor;
        //		}
        //System.out.println("");
        //		for (int i = 0; i < rdfValues; i++) {
        //			System.out.print(" "+rdfValues[i]);
        //		}
        //		System.out.println("");
        // save result
        //System.out.println("Calculated "+propertyName+":RDF[0]="+rdfValues[0]+" use[0]:"+atomProperties.getDoubleValue(1));
        result.value = rdfValues;
        result.atomProperty = propertyName;

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  initData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        String propertyS = (String) JOEPropertyHelper.getProperty(this,
                ATOM_PROPERTY, properties);

        if (propertyS == null)
        {
            propertyName = "Gasteiger_Marsili";
        }
        else
        {
            propertyName = propertyS;
        }

        //System.out.println("propertyName:"+propertyName);
        Double propertyD = (Double) JOEPropertyHelper.getProperty(this,
                MIN_SPHERICAL_VOLUME, properties);

        if (propertyD != null)
        {
            minSphericalVolume = propertyD.doubleValue();
        }

        propertyD = (Double) JOEPropertyHelper.getProperty(this,
                MAX_SPHERICAL_VOLUME, properties);

        if (propertyD != null)
        {
            maxSphericalVolume = propertyD.doubleValue();
        }

        propertyD = (Double) JOEPropertyHelper.getProperty(this,
                SPHERICAL_VOLUME_RESOLUTION, properties);

        if (propertyD != null)
        {
            sphericalVolumeResolution = propertyD.doubleValue();
        }

        propertyD = (Double) JOEPropertyHelper.getProperty(this,
                SMOOTHING_FACTOR, properties);

        if (propertyD != null)
        {
            smoothingFactorB = propertyD.doubleValue();
        }

        //System.out.println("Use smoothing ="+smoothingFactorB);
        Boolean propertyB = (Boolean) JOEPropertyHelper.getProperty(this,
                REMOVE_HYDROGENS, properties);

        if (propertyD != null)
        {
            removeHydrogens = propertyB.booleanValue();
        }

        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
