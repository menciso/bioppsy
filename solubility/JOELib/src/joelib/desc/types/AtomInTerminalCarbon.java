///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomInTerminalCarbon.java,v $
//  Purpose:  Calculates a descriptor.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/07/25 20:43:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//  Copyright (c) ALTANA PHARMA AG, Konstanz, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import org.apache.log4j.Category;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDynamicAtomProperty;
import joelib.desc.result.DynamicArrayResult;

import joelib.molecule.JOEMol;

import joelib.smarts.Patty;


/**
 * Is this atom a terminal carbon atom.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/07/25 20:43:14 $
 */
public class AtomInTerminalCarbon extends SimpleDynamicAtomProperty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.AtomInTerminalCarbon");
    public static final String DESC_KEY = "Atom_in_terminal_carbon";
    private static Patty patty = new Patty();

    /**
     * Assigned identifier for conjugated atoms.
     */
    private static String assignment = "c";

    static
    {
        // all methyl atoms
        patty.addRule("[CH3]", assignment);

        // all methenyl atoms
        patty.addRule("[CD1H2]", assignment);

        // all ethinyl atoms
        patty.addRule("[CD1H1]", assignment);
    }

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape1 object
     */
    public AtomInTerminalCarbon()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.AtomDynamicResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Object getAtomPropertiesArray(JOEMol mol)
    {
        // get electrotopological state index
        int s = mol.numAtoms();
        boolean[] terminalC = (boolean[]) DynamicArrayResult.getNewArray(DynamicArrayResult.BOOLEAN,
                s);

        int[] assignment = patty.assignTypes2All(mol);

        for (int i = 0; i < assignment.length; i++)
        {
            //			System.out.println("assignment["+i+"]:"+assignment[i]);
            if (assignment[i] != -1)
            {
                terminalC[i] = true;
            }
        }

        //		for (int i = 0; i < s; i++)
        //		{
        //			System.out.println(
        //				"conjugated["
        //					+ (i+1)
        //					+ "]="
        //					+ conjugated[i]);
        //		}
        // save result
        return terminalC;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
