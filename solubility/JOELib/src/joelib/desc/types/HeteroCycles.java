///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: HeteroCycles.java,v $
//  Purpose:  Number of hetero cycles.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/06/30 09:17:49 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.types;

import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.desc.DescDescription;
import joelib.desc.DescResult;
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.ResultFactory;
import joelib.desc.result.IntResult;

import joelib.molecule.JOEMol;

import joelib.ring.JOERing;

import joelib.util.JOEProperty;


/**
 * Number of hetero cycles.
 *
 * @author    wegner
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/06/30 09:17:49 $
 */
public class HeteroCycles implements Descriptor
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.types.HeteroCycles");
    public static final String DESC_KEY = "Number_of_heterocycles";

    //~ Instance fields ////////////////////////////////////////////////////////

    private DescriptorInfo descInfo;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the KierShape1 object
     */
    public HeteroCycles()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.IntResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     *@return    Description of the Return Value
     */
    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     *  Sets the descriptionFile attribute of the Descriptor object
     *
     *@param  _descInfo  The new descInfo value
     */

    //    public void setDescInfo(DescriptorInfo _descInfo) {
    //        descInfo = _descInfo;
    //    }

    /**
     *  Gets the description attribute of the Descriptor object
     *
     *@return    The description value
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return null;
    }

    /**
     *  Description of the Method
     *
     *@param  mol                      Description of the Parameter
     *@return                          Description of the Return Value
     *@exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     *  Description of the Method
     *
     *@param  mol                      Description of the Parameter
     *@param  initData                 Description of the Parameter
     *@return                          Description of the Return Value
     *@exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     *  Description of the Method
     *
     *@param  mol                      Description of the Parameter
     *@param  descResult               Description of the Parameter
     *@return                          Description of the Return Value
     *@exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     *  Description of the Method
     *
     *@param  mol                      Description of the Parameter
     *@param  initData                 Description of the Parameter
     *@param  descResult               Description of the Parameter
     *@return                          Description of the Return Value
     *@exception  DescriptorException  Description of the Exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        if (!(descResult instanceof IntResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                IntResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());
        }

        int heteroCycles = 0;

        Vector sssRings = mol.getSSSR();
        JOERing ring;

        for (int i = 0; i < sssRings.size(); i++)
        {
            ring = (JOERing) sssRings.get(i);

            if (ring.isHetero())
            {
                heteroCycles++;
            }
        }

        IntResult result = (IntResult) descResult;
        result.setInt(heteroCycles);

        return result;
    }

    /**
     *  Description of the Method
     */
    public void clear()
    {
    }

    /**
     *  Description of the Method
     *
     *@param  initData  Description of the Parameter
     *@return           Description of the Return Value
     */
    public boolean initialize(Map properties)
    {
        return true;
    }

    /**
    * Test the implementation of this descriptor.
    *
    * @return <tt>true</tt> if the implementation is correct
    */
    public boolean testDescriptor()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
