///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: IntBitArrayResult.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.17 $
//            $Date: 2004/07/25 20:43:00 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.BinaryValue;
import joelib.desc.DescResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.types.ChemicalMarkupLanguage;

import joelib.util.ArrayHelper;
import joelib.util.JOEBitVec;
import joelib.util.LineArrayHelper;
import joelib.util.types.StringString;


/**
 *  Atom representation.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.17 $, $Date: 2004/07/25 20:43:00 $
 */
public class IntBitArrayResult extends JOEPairData implements DescResult,
    BinaryValue, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.result.IntBitArrayResult");
    private final static String basicFormat = "<bit_pos_1,...bit_pos_n>\n" +
        "where bit_pos_n is the position of a set bit in the bit string.";

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public JOEBitVec value;
    private Hashtable cmlProperties;

    //~ Constructors ///////////////////////////////////////////////////////////

    //  public  int        maxPossibleBit;

    /**
     *  Constructor for the IntResult object
     */
    public IntBitArrayResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the double attribute of the DoubleResult object
     *
     * @return    The double value
     */

    //  public JOEBitVec getBits()
    //  {
    //    return value;
    //  }
    public JOEBitVec getBinaryValue()
    {
        return value;
    }

    /**
     *  Sets the double attribute of the DoubleResult object
     *
     * @param  bits  The new bits value
     */
    public void setBits(JOEBitVec bits)
    {
        value = bits;
    }

    public Enumeration getCMLProperties()
    {
        if (cmlProperties == null)
        {
            return null;
        }

        return cmlProperties.elements();
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------   */
    public void addCMLProperty(StringString property)
    {
        if (cmlProperties == null)
        {
            cmlProperties = new Hashtable();
        }

        cmlProperties.put(property.s1, property);
    }

    public IntBitArrayResult clone(IntBitArrayResult _target)
    {
        _target.value.clear();
        _target.value.or(this.value);
        _target.cmlProperties = this.cmlProperties;

        return _target;
    }

    public Object clone()
    {
        IntBitArrayResult newObj = new IntBitArrayResult();

        newObj.value = new JOEBitVec();

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        return basicFormat;
    }

    /**
     *  Description of the Method
     *
     * @param  pairData  Description of the Parameter
     * @param  ioType    Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param  sValue  Description of the Parameter
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public boolean fromString(IOType ioType, String sValue)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            Vector baVector;
            baVector = LineArrayHelper.booleanArrayFromString(sValue);

            boolean[] array = (boolean[]) baVector.get(0);

            //            maxBitSize=array.length;
            value.fromBoolArray(array);
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            String arrayDelimiter = null;
            String arraySize = null;

            if (cmlProperties != null)
            {
                StringString ss;
                ss = (StringString) cmlProperties.get("delimiter");

                if (ss != null)
                {
                    arrayDelimiter = ss.s2;
                }

                ss = (StringString) cmlProperties.get("size");

                if (ss != null)
                {
                    arraySize = ss.s2;
                }
            }

            if (arrayDelimiter == null)
            {
                arrayDelimiter = ChemicalMarkupLanguage.getDefaultDelimiter() +
                    " \t\r\n";
            }

            //			if (arraySize == null)
            //			{
            //				logger.error("Number of size is missing in array.");
            //				return false;
            //			}
            //			else
            //			{
            //				int size = Integer.parseInt(arraySize);
            boolean[] array = ArrayHelper.booleanArrayFromTrueFalseString(sValue,
                    arrayDelimiter);
            value = new JOEBitVec();
            value.fromBoolArray(array);

            //			}
            if (arraySize != null)
            {
                int size = Integer.parseInt(arraySize);

                if (size != array.length)
                {
                    logger.warn("Actual array size=" + array.length +
                        ", expected size=" + size);
                }
            }
        }
        else
        {
            int[] array = ArrayHelper.intArrayFromSimpleString(sValue, " _");
            value = new JOEBitVec();
            value.fromIntArray(array);
        }

        //    maxPossibleBit=Integer.parseInt(PropertyHolder.instance().getProperty(this,"maxPossibleBit"));
        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  _descName  Description of the Parameter
     * @return            Description of the Return Value
     */
    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        StringBuffer sb = new StringBuffer();

        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            //boolean[] array = value.toBoolArray();
            //LineArrayHelper.toString(sb, array).toString();
            int[] array = value.toIntArray();
            LineArrayHelper.toString(sb, array).toString();
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            String delimiter = null;

            if (cmlProperties != null)
            {
                StringString tmp = (StringString) cmlProperties.get("delimiter");

                if (tmp != null)
                {
                    delimiter = tmp.s2;
                }
            }

            if (delimiter == null)
            {
                delimiter = ChemicalMarkupLanguage.getDefaultDelimiter();
            }

            ArrayHelper.toTrueFalseString(sb, value.toBoolArray(), delimiter)
                       .toString();
        }
        else
        {
            int[] array = value.toIntArray();
            ArrayHelper.toSimpleString(sb, array, "_");
        }

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
