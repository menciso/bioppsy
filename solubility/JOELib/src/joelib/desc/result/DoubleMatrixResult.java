///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DoubleMatrixResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.23 $
//            $Date: 2004/03/15 23:16:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import wsi.ra.text.DecimalFormatHelper;
import wsi.ra.text.DecimalFormatter;

/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.NumberFormatDescResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.types.ChemicalMarkupLanguage;
import joelib.io.types.cml.ResultCMLProperties;

import joelib.util.LineMatrixHelper;
import joelib.util.MatrixHelper;
import joelib.util.types.StringString;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================  */

/**
 * Double matrix results of variable size.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.23 $, $Date: 2004/03/15 23:16:14 $
 */
public class DoubleMatrixResult extends JOEPairData implements Cloneable,
    DescResult, ResultCMLProperties, NumberFormatDescResult,
    java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.result.DoubleMatrixResult");
    private final static String basicFormat =
        "nLines nColumns<<e00,...,e0(nLines-1)>...<e(nColumns-1)0,...,e(nColumns-1)(nLines-1)>>\n" +
        "with nLines, eX0,...,eX(nLines-1) of type 32-bit integer" +
        "with nColumns, e0X,...,e(nColumns-1)X of type 32-bit integer";
    private final static String lineFormat = "nLines nColumns\n" + "e00\n" +
        "e01\n" + "...\n" + "e0(nLines-1)\n" + "n10\n" + "e11\n" + "...\n" +
        "e1(nLines-1)\n" + "...\n" + "e(nColumns-1)(nLines-1)\n" +
        "<empty line>\n" +
        "with nLines, eX0,...,eX(nLines-1) of type 32-bit integer" +
        "with nColumns, e0X,...,e(nColumns-1)X of type 32-bit integer";

    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------  */

    /**
     *  Description of the Field
     */
    public double[][] value;
    private Hashtable cmlProperties;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the IntMatrixResult object
     */
    public DoubleMatrixResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Enumeration getCMLProperties()
    {
        if (cmlProperties == null)
        {
            return null;
        }

        return cmlProperties.elements();
    }

    public void addCMLProperty(StringString property)
    {
        if (cmlProperties == null)
        {
            cmlProperties = new Hashtable();
        }

        cmlProperties.put(property.s1, property);
    }

    public DoubleMatrixResult clone(DoubleMatrixResult _target)
    {
        int s = this.value.length;

        for (int i = 0; i < s; i++)
        {
            System.arraycopy(this.value[i], 0, _target.value[i], 0,
                value[i].length);
        }

        _target.cmlProperties = this.cmlProperties;

        return _target;
    }

    public Object clone()
    {
        DoubleMatrixResult newObj = new DoubleMatrixResult();

        newObj.value = new double[this.value.length][this.value[0].length];

        if (cmlProperties != null)
        {
            newObj.cmlProperties = (Hashtable) cmlProperties.clone();
        }

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            return lineFormat;
        }
        else
        {
            return basicFormat;
        }
    }

    /**
     *  Description of the Method
     *
     * @param  pairData  Description of the Parameter
     * @param  ioType    Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param  sValue  Description of the Parameter
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public boolean fromString(IOType ioType, String sValue)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            value = LineMatrixHelper.doubleMatrixFromString(sValue);
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            if (cmlProperties == null)
            {
                logger.error("CML properties are missing");

                return false;
            }

            String matrixDelimiter = ((StringString) cmlProperties.get(
                    "delimiter")).s2;
            String matrixRows = ((StringString) cmlProperties.get("rows")).s2;
            String matrixColumns = ((StringString) cmlProperties.get("columns")).s2;

            if (matrixDelimiter == null)
            {
                matrixDelimiter = ChemicalMarkupLanguage.getDefaultDelimiter() +
                    " \t\r\n";
            }

            if (matrixRows == null)
            {
                logger.error("Number of rows is missing in matrix.");

                return false;
            }
            else if (matrixColumns == null)
            {
                logger.error("Number of columns is missing in matrix.");

                return false;
            }
            else
            {
                int rows = Integer.parseInt(matrixRows);
                int columns = Integer.parseInt(matrixColumns);
                value = MatrixHelper.doubleMatrixFromSimpleString(sValue, rows,
                        columns, matrixDelimiter);
            }
        }
        else
        {
            value = MatrixHelper.instance().doubleMatrixFromString(sValue);
        }

        return true;
    }

    /**
     *  Constructor for the IntMatrixResult object
     *
     * @param  descInfo  Description of the Parameter
     */

    //  public IntMatrixResult(DescriptorInfo descInfo)
    //  {
    //    dataType = JOEDataType.JOE_PAIR_DATA;
    //    this.setAttribute(descInfo.getName());
    //
    //    this._value = this;
    //        dataType = JOEDataType.getNewDataType( descInfo.getName() );
    //  }

    /*-------------------------------------------------------------------------*
     * public static methods
     *-------------------------------------------------------------------------  */
    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        return toString(ioType, DecimalFormatHelper.instance());
    }

    public String toString(IOType ioType, DecimalFormatter format)
    {
        StringBuffer sb = new StringBuffer();

        if ((ioType != null) &&
                ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            LineMatrixHelper.toString(sb, value, format).toString();
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            String delimiter = null;

            if (cmlProperties != null)
            {
                StringString tmp = (StringString) cmlProperties.get("delimiter");

                if (tmp != null)
                {
                    delimiter = tmp.s2;
                }
            }

            if (delimiter == null)
            {
                delimiter = ChemicalMarkupLanguage.getDefaultDelimiter();
            }

            //MatrixHelper.instance().toSimpleString(sb, value, delimiter, format).toString();
            MatrixHelper.toTranspRectString(sb, value, delimiter, format)
                        .toString();
        }
        else
        {
            MatrixHelper.instance().toString(sb, value, format).toString();
        }

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
