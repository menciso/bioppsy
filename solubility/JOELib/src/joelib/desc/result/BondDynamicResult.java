///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BondDynamicResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/03/15 23:16:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import joelib.molecule.types.BondProperties;


/**
 * Dynamic results of bond properties.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/03/15 23:16:14 $
 */
public class BondDynamicResult extends DynamicArrayResult
    implements BondProperties, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    public static final String BOND_PROPERTY = "bond_property";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DoubleResult object
     */
    public BondDynamicResult()
    {
        super();
        setPropertyType(BOND_PROPERTY);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the doubleValue attribute of the AtomDynamicResult object
     *
     * @param  idx  The new doubleValue value
     * @param  _value   The new doubleValue value
     */
    public void setDoubleValue(int idx, double _value)
    {
        if (array instanceof double[])
        {
            ((double[]) array)[idx - 1] = _value;
        }
        else if (array instanceof int[])
        {
            ((int[]) array)[idx - 1] = (int) _value;
        }
        else if (array instanceof boolean[])
        {
            ((boolean[]) array)[idx - 1] = ((_value == 1) ? true : false);
        }
    }

    /**
     *  Gets the doubleValue attribute of the AtomDynamicResult object
     *
     * @param  idx  Description of the Parameter
     * @return          The doubleValue value
     */
    public double getDoubleValue(int idx)
    {
        if (array instanceof double[])
        {
            return ((double[]) array)[idx - 1];
        }
        else if (array instanceof int[])
        {
            return (double) ((int[]) array)[idx - 1];
        }
        else if (array instanceof boolean[])
        {
            return ((((boolean[]) array)[idx - 1] == true) ? 1.0 : 0.0);
        }

        return Double.NaN;
    }

    /**
     *  Sets the intValue attribute of the AtomDynamicResult object
     *
     * @param  idx  The new intValue value
     * @param  _value   The new intValue value
     */
    public void setIntValue(int idx, int _value)
    {
        if (array instanceof double[])
        {
            ((double[]) array)[idx - 1] = (double) _value;
        }
        else if (array instanceof int[])
        {
            ((int[]) array)[idx - 1] = _value;
        }
        else if (array instanceof boolean[])
        {
            ((boolean[]) array)[idx - 1] = ((_value == 1) ? true : false);
        }
    }

    /**
     *  Constructor for the DoubleResult object
     *
     * @param  descInfo  Description of the Parameter
     */

    //  public BondDynamicResult(DescriptorInfo descInfo)
    //  {
    //    super(descInfo);
    //  }

    /*-------------------------------------------------------------------------*
     * public static methods
     *-------------------------------------------------------------------------   */

    /**
     *  Gets the intValue attribute of the AtomDynamicResult object.
     *
     * @param  idx  Description of the Parameter
     * @return          The intValue value
     */
    public int getIntValue(int idx)
    {
        if (array instanceof double[])
        {
            return (int) ((double[]) array)[idx - 1];
        }
        else if (array instanceof int[])
        {
            return ((int[]) array)[idx - 1];
        }
        else if (array instanceof boolean[])
        {
            return ((((boolean[]) array)[idx - 1] == true) ? 1 : 0);
        }

        return Integer.MAX_VALUE;
    }

    /**
     *  Sets the stringValue attribute of the AtomDynamicResult object
     *
     * @param  idx  The new stringValue value
     * @param  _value   The new stringValue value
     */
    public void setStringValue(int idx, String _value)
    {
        if (array instanceof double[])
        {
            ((double[]) array)[idx - 1] = Double.parseDouble(_value);
        }
        else if (array instanceof int[])
        {
            ((int[]) array)[idx - 1] = Integer.parseInt(_value);
        }
        else if (array instanceof boolean[])
        {
            ((boolean[]) array)[idx - 1] = ((_value.equals("1") ||
                _value.equals("true")) ? true : false);
        }
    }

    /**
     *  Gets the stringValue attribute of the AtomDynamicResult object
     *
     * @param  idx  Description of the Parameter
     * @return          The stringValue value
     */
    public String getStringValue(int idx)
    {
        if (array instanceof double[])
        {
            return Double.toString(((double[]) array)[idx - 1]);
        }
        else if (array instanceof int[])
        {
            return Integer.toString(((int[]) array)[idx - 1]);
        }
        else if (array instanceof boolean[])
        {
            return ((((boolean[]) array)[idx - 1] == true) ? "1" : "0");
        }

        return null;
    }

    /**
     *  Sets the value attribute of the AtomDynamicResult object
     *
     * @param  idx  The new value value
     * @param  _value   The new value value
     */
    public void setValue(int idx, Object _value)
    {
        if (array instanceof double[])
        {
            ((double[]) array)[idx - 1] = ((Double) _value).doubleValue();
        }
        else if (array instanceof int[])
        {
            ((int[]) array)[idx - 1] = ((Integer) _value).intValue();
        }
        else if (array instanceof boolean[])
        {
            ((boolean[]) array)[idx - 1] = ((Boolean) _value).booleanValue();
        }
    }

    /**
     *  Gets the value attribute of the AtomDynamicResult object
     *
     * @param  idx  Description of the Parameter
     * @return          The value value
     */
    public Object getValue(int idx)
    {
        if (array instanceof double[])
        {
            return new Double(((double[]) array)[idx - 1]);
        }
        else if (array instanceof int[])
        {
            return new Integer(((int[]) array)[idx - 1]);
        }
        else if (array instanceof boolean[])
        {
            return new Boolean(((boolean[]) array)[idx - 1]);
        }

        return null;
    }

    public BondDynamicResult clone(BondDynamicResult _target, int size)
    {
        //_target.propertyType = this.propertyType;
        //_target.dataDescription = this.dataDescription;
        //_target.dataUnit = this.dataDescription;
        //System.arraycopy(this.array, 0, _target.array, 0, size);
        //return _target;
        return (BondDynamicResult) super.clone(_target, size);
    }

    public Object clone()
    {
        BondDynamicResult newObj = new BondDynamicResult();

        int size = -1;

        if (array instanceof double[])
        {
            size = ((double[]) this.array).length;
            newObj.array = new double[size];
        }
        else if (array instanceof int[])
        {
            size = ((int[]) this.array).length;
            newObj.array = new int[size];
        }
        else if (array instanceof boolean[])
        {
            size = ((boolean[]) this.array).length;
            newObj.array = new boolean[size];
        }

        return clone(newObj, size);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
