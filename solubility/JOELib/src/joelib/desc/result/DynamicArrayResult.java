///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DynamicArrayResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.19 $
//            $Date: 2004/08/27 09:30:42 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import wsi.ra.text.DecimalFormatHelper;
import wsi.ra.text.DecimalFormatter;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.NumberFormatDescResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.types.cml.ResultCMLProperties;

import joelib.util.ArrayHelper;
import joelib.util.JHM;
import joelib.util.LineArrayHelper;
import joelib.util.types.StringString;


/**
 * Dynamic array results of variable size.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.19 $, $Date: 2004/08/27 09:30:42 $
 */
public class DynamicArrayResult extends JOEPairData implements Cloneable,
    DescResult, ResultCMLProperties, NumberFormatDescResult,
    java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.result.DynamicArrayResult");

    /**
     *  Description of the Field
     */
    public final static String INT = "int";

    /**
     *  Description of the Field
     */
    public final static String DOUBLE = "double";

    /**
     *  Description of the Field
     */
    public final static String BOOLEAN = "boolean";

    private final static String basicFormat = "propertyType\n" +
        "dataDescription\n" + "dataUnit\n" + "dataType\n" +
        "n<e0,...e(n-1)>\n" + "with propertyType  of type String" +
        "with dataDescription of type String" + "with dataUnit of type String" +
        "with dataType = <" + INT + " ," + DOUBLE + " ," + BOOLEAN + ">" +
        "with n of type 32-bit integer" +
        "with e0,...,e(n-1) of type 32-bit integer or 64-bit floating point value IEEE 754 or boolean value <0,1>";
    private final static String lineFormat = "propertyType\n" +
        "dataDescription\n" + "dataUnit\n" + "dataType\n" + "n\n" + "e0\n" +
        "...\n" + "e(n-1)>\n" + "with propertyType  of type String" +
        "with dataDescription of type String" + "with dataUnit of type String" +
        "with dataType = <" + INT + " ," + DOUBLE + " ," + BOOLEAN + ">" +
        "with n of type 32-bit integer" +
        "with e0,...,e(n-1) of type 32-bit integer or 64-bit floating point value IEEE 754 or boolean value <0,1>";

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    protected Object array;

    /**
     *  Description of the Field
     */
    protected String dataDescription;

    /**
     *  Description of the Field
     */
    protected String dataUnit;

    /**
     *  Description of the Field
     */
    protected String propertyType;
    private Hashtable cmlProperties;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the IntArrayResult object
     */
    public DynamicArrayResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
        propertyType = dataDescription = dataUnit = "?";
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the array attribute of the DynamicArrayResult object
     *
     * @param  newArray  The new array value
     */
    public void setArray(Object newArray)
    {
        array = newArray;
    }

    /**
     *  Gets the array attribute of the DynamicArrayResult object
     *
     * @return    The array value
     */
    public Object getArray()
    {
        return array;
    }

    /**
     *  Gets the booleanArray attribute of the DynamicArrayResult object
     *
     * @return    The booleanArray value
     */
    public boolean[] getBooleanArray()
    {
        if (array instanceof boolean[])
        {
            return (boolean[]) array;
        }
        else
        {
            return null;
        }
    }

    public Enumeration getCMLProperties()
    {
        if (cmlProperties == null)
        {
            return null;
        }

        return cmlProperties.elements();
    }

    /**
     *  Sets the dataDescription attribute of the DynamicArrayResult object
     *
     * @param  _desc  The new dataDescription value
     */
    public void setDataDescription(String _desc)
    {
        if (_desc.trim().equals(""))
        {
            dataDescription = "?";
        }
        else
        {
            dataDescription = _desc;
        }
    }

    /**
     *  Gets the dataDescription attribute of the DynamicArrayResult object
     *
     * @return    The dataDescription value
     */
    public String getDataDescription()
    {
        return dataDescription;
    }

    /**
     *  Sets the dataUnit attribute of the DynamicArrayResult object
     *
     * @param  _unit  The new dataUnit value
     */
    public void setDataUnit(String _unit)
    {
        if (_unit.trim().equals(""))
        {
            dataUnit = "?";
        }
        else
        {
            dataUnit = _unit;
        }

        System.out.println("_unit:" + _unit + "'???");
    }

    /**
     *  Gets the dataUnit attribute of the DynamicArrayResult object
     *
     * @return    The dataUnit value
     */
    public String getDataUnit()
    {
        return dataUnit;
    }

    /**
     *  Gets the doubleArray attribute of the DynamicArrayResult object
     *
     * @return    The doubleArray value
     */
    public double[] getDoubleArray()
    {
        if (array instanceof double[])
        {
            return (double[]) array;
        }
        else
        {
            return null;
        }
    }

    /**
     *  Gets the intArray attribute of the DynamicArrayResult object
     *
     * @return    The intArray value
     */
    public int[] getIntArray()
    {
        if (array instanceof int[])
        {
            return (int[]) array;
        }
        else
        {
            return null;
        }
    }

    /**
     *  Gets the newArray attribute of the DynamicArrayResult class
     *
     * @param  type  Description of the Parameter
     * @param  size  Description of the Parameter
     * @return       The newArray value
     */
    public static Object getNewArray(String type, int size)
    {
        Object newArray = null;

        if (type.equals(INT))
        {
            newArray = new int[size];
        }
        else if (type.equals(DOUBLE))
        {
            newArray = new double[size];
        }
        else if (type.equals(BOOLEAN))
        {
            newArray = new boolean[size];
        }

        return newArray;
    }

    /**
     *  Sets the propertyType attribute of the DynamicArrayResult object
     *
     * @param  _pType  The new propertyType value
     */
    public void setPropertyType(String _pType)
    {
        if (_pType.trim().equals(""))
        {
            propertyType = "?";
        }
        else
        {
            propertyType = _pType;
        }
    }

    /**
     *  Gets the propertyType attribute of the DynamicArrayResult object
     *
     * @return    The propertyType value
     */
    public String getPropertyType()
    {
        return propertyType;
    }

    public void addCMLProperty(StringString property)
    {
        if (cmlProperties == null)
        {
            cmlProperties = new Hashtable(3);
        }

        cmlProperties.put(property.s1, property);
    }

    public DynamicArrayResult clone(DynamicArrayResult _target, int size)
    {
        _target.propertyType = this.propertyType;
        _target.dataDescription = this.dataDescription;
        _target.dataUnit = this.dataDescription;
        System.arraycopy(this.array, 0, _target.array, 0, size);

        return _target;
    }

    public Object clone()
    {
        DynamicArrayResult newObj = new DynamicArrayResult();

        int size = -1;

        if (array instanceof double[])
        {
            size = ((double[]) this.array).length;
            newObj.array = new double[size];
        }
        else if (array instanceof int[])
        {
            size = ((int[]) this.array).length;
            newObj.array = new int[size];
        }
        else if (array instanceof boolean[])
        {
            size = ((boolean[]) this.array).length;
            newObj.array = new boolean[size];
        }

        return clone(newObj, size);
    }

    /**
     *  Description of the Method
     *
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            return lineFormat;
        }
        else
        {
            return basicFormat;
        }
    }

    /**
     *  Description of the Method
     *
     * @param  pairData  Description of the Parameter
     * @param  ioType    Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param  ioType  Description of the Parameter
     * @param  sValue  Description of the Parameter
     * @return         Description of the Return Value
     */
    public boolean fromString(IOType ioType, String sValue)
    {
        return fromString(ioType, sValue, null);
    }

    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        return toString(ioType, null, DecimalFormatHelper.instance());
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType, DecimalFormatter format)
    {
        return toString(ioType, null, format);
    }

    /**
     *  Description of the Method
     *
     * @param  sValue      Description of the Parameter
     * @param  ioType      Description of the Parameter
     * @param  _arrayType  Description of the Parameter
     * @return             Description of the Return Value
     */
    private boolean fromString(IOType ioType, String sValue, String _arrayType)
    {
        StringTokenizer st = new StringTokenizer(sValue, " \t\n\r");

        //		StringReader sr = new StringReader(sValue);
        //		LineNumberReader lnr = new LineNumberReader(sr);
        //    String            line;
        String arrayType;

        // get property type, data description and data unit
        try
        {
            //			propertyType = lnr.readLine();
            //			dataDescription = lnr.readLine();
            //			dataUnit = lnr.readLine();
            propertyType = st.nextToken();
            dataDescription = st.nextToken();
            dataUnit = st.nextToken();
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            return false;
        }

        // get array data type
        if (_arrayType == null)
        {
            try
            {
                //				arrayType = lnr.readLine();
                arrayType = st.nextToken();
            }
             catch (Exception ex)
            {
                ex.printStackTrace();

                return false;
            }
        }
        else
        {
            arrayType = _arrayType;
        }

        Vector vector;

        if (logger.isDebugEnabled())
        {
            logger.debug("propertyType: " + propertyType);
            logger.debug("dataDescription: " + dataDescription);
            logger.debug("dataUnit: " + dataUnit);
        }

        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            if (arrayType.equals(INT))
            {
                vector = LineArrayHelper.intArrayFromString(st, -1);
            }
            else if (arrayType.equals(DOUBLE))
            {
                vector = LineArrayHelper.doubleArrayFromString(st, -1);
            }
            else if (arrayType.equals(BOOLEAN))
            {
                vector = LineArrayHelper.booleanArrayFromString(st, -1);
            }
            else
            {
                return false;
            }
        }
        else
        {
            String sArray = st.nextToken();

            //			int index = sValue.indexOf("\n");
            //			String sArray = sValue.substring(index).trim();
            //System.out.println("DYN-ARRAY:"+sArray);
            if (arrayType.equals(INT))
            {
                vector = ArrayHelper.instance().intArrayFromString(sArray);
            }
            else if (arrayType.equals(DOUBLE))
            {
                vector = ArrayHelper.instance().doubleArrayFromString(sArray);
            }
            else if (arrayType.equals(BOOLEAN))
            {
                vector = ArrayHelper.instance().booleanArrayFromString(sArray);
            }
            else
            {
                return false;
            }
        }

        if (vector == null)
        {
            return false;
        }

        array = vector.get(0);

        return true;
    }

    /**
     *  Write this array to <tt>String</tt> .
     *
     * @param  ioType     Description of the Parameter
     * @param  arrayType  a user defined array type should be written or <tt>null
     *      </tt>for the standard type
     * @return            Description of the Return Value
     */
    private String toString(IOType ioType, String arrayType,
        DecimalFormatter format)
    {
        StringBuffer sb = new StringBuffer();

        // write property type, data description and data unit
        sb.append(propertyType);
        sb.append(JHM.eol);
        sb.append(dataDescription);
        sb.append(JHM.eol);
        sb.append(dataUnit);
        sb.append(JHM.eol);

        // write array data type
        if (arrayType == null)
        {
            if (array instanceof double[])
            {
                sb.append(DOUBLE);
                sb.append(JHM.eol);
            }
            else if (array instanceof int[])
            {
                sb.append(INT);
                sb.append(JHM.eol);
            }
            else if (array instanceof boolean[])
            {
                sb.append(BOOLEAN);
                sb.append(JHM.eol);
            }
        }
        else
        {
            sb.append(arrayType);
            sb.append(JHM.eol);
        }

        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            if (array instanceof double[])
            {
                LineArrayHelper.toString(sb, (double[]) array, format);
            }
            else if (array instanceof int[])
            {
                LineArrayHelper.toString(sb, (int[]) array);
            }
            else if (array instanceof boolean[])
            {
                LineArrayHelper.toString(sb, (boolean[]) array);
            }
        }
        else
        {
            ArrayHelper ah = ArrayHelper.instance();

            if (array instanceof double[])
            {
                ah.toString(sb, (double[]) array, format);
            }
            else if (array instanceof int[])
            {
                ah.toString(sb, (int[]) array);
            }
            else if (array instanceof boolean[])
            {
                ah.toString(sb, (boolean[]) array);
            }
        }

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
