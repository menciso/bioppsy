///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomDoubleResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/03/15 23:16:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import joelib.molecule.types.AtomProperties;


/**
 * Double results of atom properties.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/03/15 23:16:14 $
 */
public class AtomDoubleResult extends DoubleArrayResult
    implements AtomProperties, java.io.Serializable
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DoubleResult object
     */
    public AtomDoubleResult()
    {
        super();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void setDoubleValue(int atomIdx, double _value)
    {
        value[atomIdx - 1] = _value;
    }

    public double getDoubleValue(int atomIdx)
    {
        return value[atomIdx - 1];
    }

    public void setIntValue(int atomIdx, int _value)
    {
        value[atomIdx - 1] = (double) _value;
    }

    /**
     *  Constructor for the DoubleResult object
     *
     * @param  descInfo  Description of the Parameter
     */

    //  public AtomDoubleResult(DescriptorInfo descInfo)
    //  {
    //    super(descInfo);
    //  }

    /*-------------------------------------------------------------------------*
     * public  methods
     *-------------------------------------------------------------------------  */
    public int getIntValue(int atomIdx)
    {
        return (int) value[atomIdx - 1];
    }

    public void setStringValue(int atomIdx, String _value)
    {
        value[atomIdx - 1] = Double.parseDouble(_value);
    }

    public String getStringValue(int atomIdx)
    {
        return Double.toString(value[atomIdx - 1]);
    }

    public void setValue(int atomIdx, Object _value)
    {
        value[atomIdx - 1] = ((Double) _value).doubleValue();
    }

    public Object getValue(int atomIdx)
    {
        return new Double(value[atomIdx - 1]);
    }

    public AtomDoubleResult clone(AtomDoubleResult _target)
    {
        //System.arraycopy(this.value,0,_target.value, 0, value.length);
        //return _target;
        return (AtomDoubleResult) super.clone(_target);
    }

    public Object clone()
    {
        AtomDoubleResult newObj = new AtomDoubleResult();

        newObj.value = new double[this.value.length];

        return clone(newObj);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
