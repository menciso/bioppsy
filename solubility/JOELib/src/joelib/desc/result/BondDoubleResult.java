///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BondDoubleResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2003/08/19 13:11:25 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;


/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import joelib.molecule.types.BondProperties;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================  */

/**
 * Double results of bond properties.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2003/08/19 13:11:25 $
 */
public class BondDoubleResult extends DoubleArrayResult
    implements BondProperties, java.io.Serializable
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------  */
    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the DoubleResult object
     */
    public BondDoubleResult()
    {
        super();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void setDoubleValue(int bondIdx, double _value)
    {
        value[bondIdx - 1] = _value;
    }

    public double getDoubleValue(int bondIdx)
    {
        return value[bondIdx - 1];
    }

    public void setIntValue(int bondIdx, int _value)
    {
        value[bondIdx - 1] = (double) _value;
    }

    /**
     *  Constructor for the DoubleResult object
     *
     * @param  descInfo  Description of the Parameter
     */

    //  public BondDoubleResult(DescriptorInfo descInfo)
    //  {
    //    super(descInfo);
    //  }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */
    public int getIntValue(int bondIdx)
    {
        return (int) value[bondIdx - 1];
    }

    public void setStringValue(int bondIdx, String _value)
    {
        value[bondIdx - 1] = Double.parseDouble(_value);
    }

    public String getStringValue(int bondIdx)
    {
        return Double.toString(value[bondIdx - 1]);
    }

    public void setValue(int bondIdx, Object _value)
    {
        value[bondIdx - 1] = ((Double) _value).doubleValue();
    }

    public Object getValue(int bondIdx)
    {
        return new Double(value[bondIdx - 1]);
    }

    public BondDoubleResult clone(BondDoubleResult _target)
    {
        //System.arraycopy(this.value,0,_target.value, 0, value.length);
        //return _target;
        return (BondDoubleResult) super.clone(_target);
    }

    public Object clone()
    {
        BondDoubleResult newObj = new BondDoubleResult();

        newObj.value = new double[this.value.length];

        return clone(newObj);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
