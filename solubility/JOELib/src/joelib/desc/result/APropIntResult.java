///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: APropIntResult.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/02/20 13:11:45 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;


/*==========================================================================*
 * IMPORTS
 *==========================================================================    */
import java.io.LineNumberReader;
import java.io.StringReader;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.NativeValue;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;

import joelib.util.JHM;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================    */

/**
 *  Atom representation.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/02/20 13:11:45 $
 */
public class APropIntResult extends JOEPairData implements Cloneable,
    DescResult, NativeValue, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private final static String basicFormat = "<atom_property>\n" +
        "32-bit integer";
    private final static String lineFormat = "<atom_property>\n" +
        "32-bit integer";

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public String atomProperty;

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------    */

    /**
     *  Description of the Field
     */
    public int value;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------    */

    /**
     *  Constructor for the IntResult object
     */
    public APropIntResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Sets the doubleNV attribute of the APropIntResult object
     *
     * @param _value  The new doubleNV value
     */
    public void setDoubleNV(double _value)
    {
        value = (int) _value;
    }

    /**
     * Gets the doubleNV attribute of the APropIntResult object
     *
     * @return   The doubleNV value
     */
    public double getDoubleNV()
    {
        return (double) value;
    }

    /**
     * Gets the doubleNV attribute of the APropIntResult object
     *
     * @return   The doubleNV value
     */
    public boolean isDoubleNV()
    {
        return false;
    }

    /**
     *  Sets the double attribute of the DoubleResult object
     *
     * @param _v  The new double value
     */
    public void setInt(int _v)
    {
        value = _v;
    }

    /**
     *  Gets the double attribute of the DoubleResult object
     *
     * @return   The double value
     */
    public int getInt()
    {
        return value;
    }

    /**
     * Sets the intNV attribute of the APropIntResult object
     *
     * @param _value  The new intNV value
     */
    public void setIntNV(int _value)
    {
        value = _value;
    }

    /**
     * Gets the intNV attribute of the APropIntResult object
     *
     * @return   The intNV value
     */
    public int getIntNV()
    {
        return value;
    }

    /**
     * Gets the intNV attribute of the APropIntResult object
     *
     * @return   The intNV value
     */
    public boolean isIntNV()
    {
        return true;
    }

    /**
     * Sets the stringNV attribute of the APropIntResult object
     *
     * @param _value  The new stringNV value
     */
    public void setStringNV(String _value)
    {
        value = Integer.parseInt(_value);
    }

    /**
     * Gets the stringNV attribute of the APropIntResult object
     *
     * @return   The stringNV value
     */
    public String getStringNV()
    {
        return Integer.toString(value);
    }

    public APropIntResult clone(APropIntResult _target)
    {
        _target.atomProperty = this.atomProperty;
        _target.value = this.value;

        return _target;
    }

    public Object clone()
    {
        APropIntResult newObj = new APropIntResult();

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            return lineFormat;
        }
        else
        {
            return basicFormat;
        }
    }

    /**
     *  Description of the Method
     *
     * @param pairData  Description of the Parameter
     * @param ioType    Description of the Parameter
     * @return          Description of the Return Value
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param sValue  Description of the Parameter
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public boolean fromString(IOType ioType, String sValue)
    {
        StringReader sr = new StringReader(sValue);
        LineNumberReader lnr = new LineNumberReader(sr);

        String tmp = null;

        // get property type
        if (ioType.equals(IOTypeHolder.instance().getIOType("FLAT")) == false)
        {
            try
            {
                atomProperty = lnr.readLine();
            }
             catch (Exception ex)
            {
                ex.printStackTrace();

                return false;
            }
        }
        else
        {
            int index = sValue.indexOf("\n");
            tmp = sValue.substring(index).trim();
        }

        if (ioType.equals(IOTypeHolder.instance().getIOType("UNDEFINED")))
        {
            int index = sValue.indexOf("\n");
            tmp = sValue.substring(index).trim();
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("FLAT")) == false)
        {
            int index = sValue.indexOf("\n");
            tmp = sValue.substring(index).trim();
        }

        value = Integer.parseInt(tmp);

        return true;
    }

    /**
     *  Constructor for the DoubleResult object
     *
     * @param _descName  Description of the Parameter
     * @return           Description of the Return Value
     */

    //  public IntResult(DescriptorInfo descInfo)
    //  {
    //    dataType = JOEDataType.JOE_PAIR_DATA;
    //      this.setAttribute(descInfo.getName());
    //
    //    this._value = this;
    //  }

    /*-------------------------------------------------------------------------*
     * public static methods
     *-------------------------------------------------------------------------    */
    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        StringBuffer sb = new StringBuffer();

        if (ioType.equals(IOTypeHolder.instance().getIOType("FLAT")))
        {
            sb.append(value);
        }
        else
        {
            sb.append(atomProperty);
            sb.append(JHM.eol);
            sb.append(value);
        }

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
