///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BitResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.18 $
//            $Date: 2004/07/25 20:43:00 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;


/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.BinaryValue;
import joelib.desc.DescResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.types.ChemicalMarkupLanguage;
import joelib.io.types.cml.ResultCMLProperties;

import joelib.util.ArrayHelper;
import joelib.util.JOEBitVec;
import joelib.util.LineArrayHelper;
import joelib.util.types.StringString;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================  */

/**
 *  Atom representation.
 *
 * @author     wegnerj
 *     30. Januar 2002
 */
public class BitResult extends JOEPairData implements Cloneable, DescResult,
    BinaryValue, ResultCMLProperties, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.result.BitResult");
    private final static String basicFormat = "n<bit_1,...bit_n>\n" +
        "with n\n" + "and bit_0,...,bit_n of type <0,1>";
    private final static String lineFormat = "n\n" + "bit_1\n" + "...\n" +
        "bit_n\n" + "with n\n" + "and bit_0,...,bit_n of type <0,1>";

    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------  */

    /**
     *  Description of the Field
     */
    public JOEBitVec value;
    public int maxBitSize;
    private Hashtable cmlProperties;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the IntResult object
     */
    public BitResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;

        value = new JOEBitVec();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public JOEBitVec getBinaryValue()
    {
        return value;
    }

    /**
     *  Sets the double attribute of the DoubleResult object
     *
     * @param  _v  The new double value
     */
    public void setBits(JOEBitVec bits, int _maxBitSize)
    {
        value = bits;
        maxBitSize = _maxBitSize;
    }

    public Enumeration getCMLProperties()
    {
        if (cmlProperties == null)
        {
            return null;
        }

        return cmlProperties.elements();
    }

    /**
     *  Gets the double attribute of the DoubleResult object
     *
     * @return    The double value
     */

    //  public JOEBitVec getBits()
    //  {
    //    return value;
    //  }
    public int getMaxBitSize()
    {
        return maxBitSize;
    }

    public void addCMLProperty(StringString property)
    {
        if (cmlProperties == null)
        {
            cmlProperties = new Hashtable();
        }

        cmlProperties.put(property.s1, property);
    }

    public BitResult clone(BitResult _target)
    {
        _target.maxBitSize = this.maxBitSize;
        _target.value.clear();
        _target.value.or(this.value);
        _target.cmlProperties = this.cmlProperties;

        return _target;
    }

    public Object clone()
    {
        BitResult newObj = new BitResult();

        newObj.value = new JOEBitVec();

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param  ioType  Description of the Parameter
     * @return         Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            return lineFormat;
        }
        else
        {
            return basicFormat;
        }
    }

    /**
     *  Description of the Method
     *
     * @param  pairData  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param  sValue  Description of the Parameter
     * @return         Description of the Return Value
     */
    public boolean fromString(IOType ioType, String sValue)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            Vector baVector;
            baVector = LineArrayHelper.booleanArrayFromString(sValue);

            boolean[] array = (boolean[]) baVector.get(0);
            maxBitSize = array.length;
            value.fromBoolArray(array);
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            String arrayDelimiter = null;
            String arraySize = null;

            if (cmlProperties != null)
            {
                StringString ss;
                ss = (StringString) cmlProperties.get("delimiter");

                if (ss != null)
                {
                    arrayDelimiter = ss.s2;
                }

                ss = (StringString) cmlProperties.get("size");

                if (ss != null)
                {
                    arraySize = ss.s2;
                }
            }

            if (arrayDelimiter == null)
            {
                arrayDelimiter = ChemicalMarkupLanguage.getDefaultDelimiter() +
                    " \t\r\n";
            }

            //			if(arraySize==null)
            //			{
            //				logger.error("Number of size is missing in array.");
            //				return false;
            //			}
            //			else{
            //				int size=Integer.parseInt(arraySize);
            boolean[] array = ArrayHelper.booleanArrayFromTrueFalseString(sValue,
                    arrayDelimiter);
            maxBitSize = array.length;
            value.fromBoolArray(array);

            //			}
            if (arraySize != null)
            {
                int size = Integer.parseInt(arraySize);

                if (size != array.length)
                {
                    logger.warn("Actual array size=" + array.length +
                        ", expected size=" + size);
                }
            }
        }
        else
        {
            Vector baVector;
            baVector = ArrayHelper.booleanArrayFromString(sValue, " _", -1);

            boolean[] array = (boolean[]) baVector.get(0);
            maxBitSize = array.length;
            value.fromBoolArray(array);
        }

        return true;
    }

    /**
     *  Constructor for the DoubleResult object
     *
     * @param  descInfo  Description of the Parameter
     */

    //  public IntResult(DescriptorInfo descInfo)
    //  {
    //    dataType = JOEDataType.JOE_PAIR_DATA;
    //      this.setAttribute(descInfo.getName());
    //
    //    this._value = this;
    //  }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */
    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        StringBuffer sb = new StringBuffer();

        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            boolean[] array = value.toBoolArr(maxBitSize);
            LineArrayHelper.toString(sb, array).toString();
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            String delimiter = null;

            if (cmlProperties != null)
            {
                delimiter = ((StringString) cmlProperties.get("delimiter")).s2;
            }

            if (delimiter == null)
            {
                delimiter = ChemicalMarkupLanguage.getDefaultDelimiter();
            }

            ArrayHelper.toTrueFalseString(sb, value.toBoolArray(), delimiter)
                       .toString();
        }
        else
        {
            boolean[] array = value.toBoolArr(maxBitSize);
            ArrayHelper.toString(sb, array, "_", true).toString();
        }

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
