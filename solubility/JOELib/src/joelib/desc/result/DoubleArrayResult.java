///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DoubleArrayResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.21 $
//            $Date: 2004/07/25 20:43:00 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import wsi.ra.text.DecimalFormatHelper;
import wsi.ra.text.DecimalFormatter;

/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.NumberFormatDescResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.types.ChemicalMarkupLanguage;
import joelib.io.types.cml.ResultCMLProperties;

import joelib.util.ArrayHelper;
import joelib.util.LineArrayHelper;
import joelib.util.types.StringString;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================  */

/**
 * Double array results of variable size.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.21 $, $Date: 2004/07/25 20:43:00 $
 */
public class DoubleArrayResult extends JOEPairData implements Cloneable,
    DescResult, ResultCMLProperties, NumberFormatDescResult,
    java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.result.DoubleArrayResult");
    private final static String basicFormat = "n<e0,...e(n-1)>\n" +
        "with n of type 32-bit integer" +
        "with e0,...,e(n-1) of type 64-bit floating point value IEEE 754";
    private final static String lineFormat = "n\n" + "e0\n" + "...\n" +
        "e(n-1)>\n" + 
        //            "<empty line>\n" +
        "with n of type 32-bit integer" +
        "with e0,...,e(n-1) of type 64-bit floating point value IEEE 754";

    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------  */
    protected double[] value;
    private Hashtable cmlProperties;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the IntArrayResult object
     */
    public DoubleArrayResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Enumeration getCMLProperties()
    {
        if (cmlProperties == null)
        {
            return null;
        }

        return cmlProperties.elements();
    }

    /**
     *  Sets the double attribute of the IntArrayResult object
     *
     * @param _iarray  The new double value
     */
    public void setDoubleArray(double[] _darray)
    {
        value = _darray;
    }

    /**
     *  Gets the double attribute of the IntArrayResult object
     *
     * @return   The double value
     */
    public double[] getDoubleArray()
    {
        return (double[]) value;
    }

    public void addCMLProperty(StringString property)
    {
        if (cmlProperties == null)
        {
            cmlProperties = new Hashtable();
        }

        cmlProperties.put(property.s1, property);
    }

    public DoubleArrayResult clone(DoubleArrayResult _target)
    {
        System.arraycopy(this.value, 0, _target.value, 0, value.length);
        _target.cmlProperties = this.cmlProperties;

        return _target;
    }

    public Object clone()
    {
        DoubleArrayResult newObj = new DoubleArrayResult();

        newObj.value = new double[this.value.length];

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            return lineFormat;
        }
        else
        {
            return basicFormat;
        }
    }

    /**
     *  Description of the Method
     *
     * @param pairData  Description of the Parameter
     * @param ioType    Description of the Parameter
     * @return          Description of the Return Value
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param sValue  Description of the Parameter
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public boolean fromString(IOType ioType, String sValue)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            Vector daVector;
            daVector = LineArrayHelper.doubleArrayFromString(sValue);
            value = (double[]) daVector.get(0);
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            String arrayDelimiter = null;
            String arraySize = null;

            if (cmlProperties != null)
            {
                StringString ss;
                ss = (StringString) cmlProperties.get("delimiter");

                if (ss != null)
                {
                    arrayDelimiter = ss.s2;
                }

                ss = (StringString) cmlProperties.get("size");

                if (ss != null)
                {
                    arraySize = ss.s2;
                }
            }

            if (arrayDelimiter == null)
            {
                arrayDelimiter = ChemicalMarkupLanguage.getDefaultDelimiter() +
                    " \t\r\n";
            }

            //			if (arraySize == null)
            //			{
            //				logger.error("Number of size is missing in array.");
            //				return false;
            //			}
            //			else
            //			{
            value = ArrayHelper.doubleArrayFromSimpleString(sValue,
                    arrayDelimiter);

            //			}
            if (arraySize != null)
            {
                int size = Integer.parseInt(arraySize);

                if (size != value.length)
                {
                    logger.warn("Actual array size=" + value.length +
                        ", expected size=" + size);
                }
            }
        }
        else
        {
            Vector daVector;
            daVector = ArrayHelper.instance().doubleArrayFromString(sValue);
            value = (double[]) daVector.get(0);
        }

        return true;
    }

    /**
     *  Constructor for the IntArrayResult object
     *
     * @param descInfo  Description of the Parameter
     */

    //    public DoubleArrayResult(DescriptorInfo descInfo)
    //    {
    //        dataType = JOEDataType.JOE_PAIR_DATA;
    //        this.setAttribute(descInfo.getName());
    //
    //        this._value = this;
    //        dataType = JOEDataType.getNewDataType( descInfo.getName() );
    //    }

    /*-------------------------------------------------------------------------*
     * public static methods
     *-------------------------------------------------------------------------  */
    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        return toString(ioType, DecimalFormatHelper.instance());
    }

    public String toString(IOType ioType, DecimalFormatter format)
    {
        StringBuffer sb = new StringBuffer();

        //sb.append("ioType:"+ioType);
        //Vector v =null;
        //v.add(new Object());
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")))
        {
            LineArrayHelper.toString(sb, value, format).toString();
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            String delimiter = null;

            if (cmlProperties != null)
            {
                StringString tmp = (StringString) cmlProperties.get("delimiter");

                if (tmp != null)
                {
                    delimiter = tmp.s2;
                }
            }

            if (delimiter == null)
            {
                delimiter = ChemicalMarkupLanguage.getDefaultDelimiter();
            }

            ArrayHelper.toSimpleString(sb, value, delimiter, format);
        }
        else
        {
            ArrayHelper.instance().toString(sb, value, format).toString();
        }

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
