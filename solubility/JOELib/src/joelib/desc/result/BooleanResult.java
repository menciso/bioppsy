///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BooleanResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/02/20 13:11:45 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;


/*==========================================================================*
 * IMPORTS
 *==========================================================================   */
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.NativeValue;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.types.cml.ResultCMLProperties;

import joelib.util.types.StringString;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================   */

/**
 * Integer result.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/02/20 13:11:45 $
 */
public class BooleanResult extends JOEPairData implements Cloneable, DescResult,
    NativeValue, ResultCMLProperties, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.result.BooleanResult");

    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------   */

    /**
     *  Description of the Field
     */
    public boolean value;
    private Hashtable cmlProperties;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------   */

    /**
     *  Constructor for the IntResult object
     */
    public BooleanResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the double attribute of the DoubleResult object
     *
     * @param _v  The new double value
     */
    public void setBoolean(boolean _v)
    {
        value = _v;
    }

    /**
     *  Gets the double attribute of the DoubleResult object
     *
     * @return   The double value
     */
    public boolean getBoolean()
    {
        return value;
    }

    public Enumeration getCMLProperties()
    {
        if (cmlProperties == null)
        {
            return null;
        }

        return cmlProperties.elements();
    }

    /**
     * Sets the doubleNV attribute of the IntResult object
     *
     * @param _value  The new doubleNV value
     */
    public void setDoubleNV(double _value)
    {
        if (_value == 0.0)
        {
            value = false;
        }
        else
        {
            value = true;
        }
    }

    /**
     * Gets the doubleNV attribute of the IntResult object
     *
     * @return   The doubleNV value
     */
    public double getDoubleNV()
    {
        if (value)
        {
            return 1.0;
        }
        else
        {
            return 0.0;
        }
    }

    /**
     * Gets the doubleNV attribute of the IntResult object
     *
     * @return   The doubleNV value
     */
    public boolean isDoubleNV()
    {
        return false;
    }

    /**
     * Sets the intNV attribute of the IntResult object
     *
     * @param _value  The new intNV value
     */
    public void setIntNV(int _value)
    {
        if (_value == 0)
        {
            value = false;
        }
        else
        {
            value = true;
        }
    }

    /**
     * Gets the intNV attribute of the IntResult object
     *
     * @return   The intNV value
     */
    public int getIntNV()
    {
        if (value)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    /**
     * Gets the intNV attribute of the IntResult object
     *
     * @return   The intNV value
     */
    public boolean isIntNV()
    {
        return true;
    }

    /**
     * Sets the stringNV attribute of the IntResult object
     *
     * @param _value  The new stringNV value
     */
    public void setStringNV(String _value)
    {
        if (_value.equals("0") || _value.equals("0.0") ||
                _value.equalsIgnoreCase("FALSE"))
        {
            value = false;
        }
        else
        {
            value = true;
        }
    }

    /**
     * Gets the stringNV attribute of the IntResult object
     *
     * @return   The stringNV value
     */
    public String getStringNV()
    {
        if (value)
        {
            return "1";
        }
        else
        {
            return "0";
        }
    }

    public void addCMLProperty(StringString property)
    {
        if (cmlProperties == null)
        {
            cmlProperties = new Hashtable();
        }

        cmlProperties.put(property.s1, property);
    }

    public BooleanResult clone(BooleanResult _target)
    {
        _target.value = value;
        _target.cmlProperties = this.cmlProperties;

        return _target;
    }

    public Object clone()
    {
        BooleanResult newObj = new BooleanResult();

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")) ||
                ioType.equals(IOTypeHolder.instance().getIOType("FLAT")))
        {
            return "1 or 0";
        }
        else
        {
            return "true or false";
        }
    }

    /**
     *  Description of the Method
     *
     * @param pairData                   Description of the Parameter
     * @param ioType                     Description of the Parameter
     * @return                           Description of the Return Value
     * @exception NumberFormatException  Description of the Exception
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
        throws NumberFormatException
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param sValue                     Description of the Parameter
     * @param ioType                     Description of the Parameter
     * @return                           Description of the Return Value
     * @exception NumberFormatException  Description of the Exception
     */
    public boolean fromString(IOType ioType, String sValue)
        throws NumberFormatException
    {
        //System.out.println("BOOLEAN::: "+ioType+" "+sValue);
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")) ||
                ioType.equals(IOTypeHolder.instance().getIOType("FLAT")))
        {
            if (sValue.equals("1"))
            {
                value = true;
            }
            else if (sValue.equals("0"))
            {
                value = false;
            }
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            try
            {
                value = ((sValue != null) && sValue.equalsIgnoreCase("true"));

                //System.out.println("PARSED:"+value);
            }
             catch (NumberFormatException ex)
            {
                logger.error(ex.toString());
                throw ex;

                //			return false;
            }
        }
        else
        {
            try
            {
                value = ((sValue != null) && sValue.equalsIgnoreCase("true"));
            }
             catch (NumberFormatException ex)
            {
                logger.error(ex.toString());
                throw ex;

                //			return false;
            }
        }

        return true;
    }

    /*-------------------------------------------------------------------------*
     * public static methods
     *-------------------------------------------------------------------------   */
    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        if (ioType.equals(IOTypeHolder.instance().getIOType("SDF")) ||
                ioType.equals(IOTypeHolder.instance().getIOType("FLAT")))
        {
            if (value)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
        else if (ioType.equals(IOTypeHolder.instance().getIOType("CML")))
        {
            return Boolean.toString(value);
        }
        else
        {
            return Boolean.toString(value);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
