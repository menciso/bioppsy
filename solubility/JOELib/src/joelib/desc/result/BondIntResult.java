///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BondIntResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2004/03/15 23:16:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import joelib.molecule.types.BondProperties;


/**
 * Integer results of atom properties.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/03/15 23:16:14 $
 */
public class BondIntResult extends IntArrayResult implements BondProperties,
    java.io.Serializable
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DoubleResult object
     */
    public BondIntResult()
    {
        super();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void setDoubleValue(int bondIdx, double _value)
    {
        value[bondIdx - 1] = (int) _value;
    }

    public double getDoubleValue(int bondIdx)
    {
        return (double) value[bondIdx - 1];
    }

    public void setIntValue(int bondIdx, int _value)
    {
        value[bondIdx - 1] = (int) _value;
    }

    public int getIntValue(int bondIdx)
    {
        return (int) value[bondIdx - 1];
    }

    public void setStringValue(int bondIdx, String _value)
    {
        value[bondIdx - 1] = Integer.parseInt(_value);
    }

    public String getStringValue(int bondIdx)
    {
        return Integer.toString(value[bondIdx - 1]);
    }

    public void setValue(int bondIdx, Object _value)
    {
        value[bondIdx - 1] = ((Integer) _value).intValue();
    }

    public Object getValue(int bondIdx)
    {
        return new Integer(value[bondIdx - 1]);
    }

    public BondIntResult clone(BondIntResult _target)
    {
        //System.arraycopy(this.value, 0, _target.value, 0, value.length);
        //return _target;
        return (BondIntResult) super.clone(_target);
    }

    public Object clone()
    {
        BondIntResult newObj = new BondIntResult();

        newObj.value = new int[this.value.length];

        return clone(newObj);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
