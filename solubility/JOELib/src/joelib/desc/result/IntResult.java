///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: IntResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.21 $
//            $Date: 2004/08/27 09:30:42 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import wsi.ra.text.DecimalFormatHelper;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.NativeValue;

import joelib.io.IOType;
import joelib.io.types.cml.ResultCMLProperties;

import joelib.util.types.StringString;


/**
 * Integer result.
 * E.g. when normalizing the data, the integers will be stored as double values AND
 * can be also loaded as double values !!!
 * If you use <tt>setDoubleNV</tt> the values will be later stored as double values,
 * when converting to String values !
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.21 $, $Date: 2004/08/27 09:30:42 $
 */
public class IntResult extends JOEPairData implements Cloneable, DescResult,
    NativeValue, ResultCMLProperties, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.result.IntResult");

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable cmlProperties;
    private boolean storeAsDouble = false;

    /**
     *  Description of the Field
     */
    private double value;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the IntResult object
     */
    public IntResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Enumeration getCMLProperties()
    {
        if (cmlProperties == null)
        {
            return null;
        }

        return cmlProperties.elements();
    }

    /**
     * Sets the doubleNV attribute of the IntResult object
     *
     * @param _value  The new doubleNV value
     */
    public void setDoubleNV(double _value)
    {
        storeAsDouble = true;
        value = _value;
    }

    /**
     * Gets the doubleNV attribute of the IntResult object
     *
     * @return   The doubleNV value
     */
    public double getDoubleNV()
    {
        return value;
    }

    /**
     * Gets the doubleNV attribute of the IntResult object
     *
     * @return   The doubleNV value
     */
    public boolean isDoubleNV()
    {
        if (storeAsDouble)
        {
            return true;
        }

        return false;
    }

    /**
     *  Sets the double attribute of the DoubleResult object
     *
     * @param _v  The new double value
     */
    public void setInt(int _v)
    {
        storeAsDouble = false;
        value = _v;
    }

    /**
     *  Gets the double attribute of the DoubleResult object
     *
     * @return   The double value
     */
    public int getInt()
    {
        return (int) Math.round(value);
    }

    /**
     * Sets the intNV attribute of the IntResult object
     *
     * @param _value  The new intNV value
     */
    public void setIntNV(int _value)
    {
        storeAsDouble = false;
        value = _value;
    }

    /**
     * Gets the intNV attribute of the IntResult object
     *
     * @return   The intNV value
     */
    public int getIntNV()
    {
        return (int) Math.round(value);
    }

    /**
     * Gets the intNV attribute of the IntResult object
     *
     * @return   The intNV value
     */
    public boolean isIntNV()
    {
        return true;
    }

    /**
     * Sets the stringNV attribute of the IntResult object
     *
     * @param _value  The new stringNV value
     */
    public void setStringNV(String _value)
    {
        try
        {
            value = Integer.parseInt(_value);
            storeAsDouble = false;
        }
         catch (NumberFormatException ex)
        {
            try
            {
                // o.k. let's try to resolve double values
                value = (int) Double.parseDouble(_value);
                storeAsDouble = true;
            }
             catch (NumberFormatException ex2)
            {
                // show exception if this fails 
                logger.error(ex2.toString());

                //throw ex;
                //			return false;
            }
        }
    }

    /**
     * Gets the stringNV attribute of the IntResult object
     *
     * @return   The stringNV value
     */
    public String getStringNV()
    {
        return Integer.toString((int) Math.round(value));
    }

    public void addCMLProperty(StringString property)
    {
        if (cmlProperties == null)
        {
            cmlProperties = new Hashtable(3);
        }

        cmlProperties.put(property.s1, property);
    }

    public IntResult clone(IntResult _target)
    {
        _target.value = value;
        _target.storeAsDouble = storeAsDouble;
        _target.cmlProperties = this.cmlProperties;

        return _target;
    }

    public Object clone()
    {
        IntResult newObj = new IntResult();

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        return "32-bit integer value. Internal stored as double to allow e.g. normalizations.";
    }

    /**
     *  Description of the Method
     *
     * @param pairData                   Description of the Parameter
     * @param ioType                     Description of the Parameter
     * @return                           Description of the Return Value
     * @exception NumberFormatException  Description of the Exception
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
        throws NumberFormatException
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param sValue                     Description of the Parameter
     * @param ioType                     Description of the Parameter
     * @return                           Description of the Return Value
     * @exception NumberFormatException  Description of the Exception
     */
    public boolean fromString(IOType ioType, String sValue)
        throws NumberFormatException
    {
        try
        {
            value = Integer.parseInt(sValue);
            storeAsDouble = false;
        }
         catch (NumberFormatException ex)
        {
            try
            {
                // o.k. let's try to resolve double values
                value = (int) Double.parseDouble(sValue);
                storeAsDouble = true;
            }
             catch (NumberFormatException ex2)
            {
                // show exception if this fails 
                logger.error(ex2.toString());
                throw ex;

                //			return false;
            }
        }

        return true;
    }

    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        if (storeAsDouble)
        {
            return DecimalFormatHelper.instance().format(value);
        }

        return Integer.toString((int) Math.round(value));
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
