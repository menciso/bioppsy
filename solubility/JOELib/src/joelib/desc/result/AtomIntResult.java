///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomIntResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/08/27 09:30:42 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import joelib.molecule.types.AtomProperties;


/**
 * Integer results of atom properties.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/08/27 09:30:42 $
 */
public class AtomIntResult extends IntArrayResult implements AtomProperties,
    java.io.Serializable
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DoubleResult object
     */
    public AtomIntResult()
    {
        super();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Sets the doubleValue attribute of the AtomIntResult object
     *
     * @param atomIdx  The new doubleValue value
     * @param _value   The new doubleValue value
     */
    public void setDoubleValue(int atomIdx, double _value)
    {
        value[atomIdx - 1] = (int) _value;
    }

    /**
     * Gets the doubleValue attribute of the AtomIntResult object
     *
     * @param atomIdx  Description of the Parameter
     * @return         The doubleValue value
     */
    public double getDoubleValue(int atomIdx)
    {
        return (double) value[atomIdx - 1];
    }

    /**
     * Sets the intValue attribute of the AtomIntResult object
     *
     * @param atomIdx  The new intValue value
     * @param _value   The new intValue value
     */
    public void setIntValue(int atomIdx, int _value)
    {
        value[atomIdx - 1] = _value;
    }

    /**
     * Gets the intValue attribute of the AtomIntResult object
     *
     * @param atomIdx  Description of the Parameter
     * @return         The intValue value
     */
    public int getIntValue(int atomIdx)
    {
        return value[atomIdx - 1];
    }

    /**
     * Sets the stringValue attribute of the AtomIntResult object
     *
     * @param atomIdx  The new stringValue value
     * @param _value   The new stringValue value
     */
    public void setStringValue(int atomIdx, String _value)
    {
        value[atomIdx - 1] = Integer.parseInt(_value);
    }

    /**
     * Gets the stringValue attribute of the AtomIntResult object
     *
     * @param atomIdx  Description of the Parameter
     * @return         The stringValue value
     */
    public String getStringValue(int atomIdx)
    {
        return Integer.toString(value[atomIdx - 1]);
    }

    /**
     * Sets the value attribute of the AtomIntResult object
     *
     * @param atomIdx  The new value value
     * @param _value   The new value value
     */
    public void setValue(int atomIdx, Object _value) //throws DataFormatException
    {
        //        if (_value instanceof Integer)
        //        {
        value[atomIdx - 1] = ((Integer) _value).intValue();

        //        }
        //        else
        //        {
        //          throw new DataFormatException();
        //        }
    }

    /**
     * Gets the value attribute of the AtomIntResult object
     *
     * @param atomIdx  Description of the Parameter
     * @return         The value value
     */
    public Object getValue(int atomIdx)
    {
        return new Integer(value[atomIdx - 1]);
    }

    public AtomIntResult clone(AtomIntResult _target)
    {
        //System.arraycopy(this.value, 0, _target.value, 0, value.length);
        //return _target;
        return (AtomIntResult) super.clone(_target);
    }

    public Object clone()
    {
        AtomIntResult newObj = new AtomIntResult();

        newObj.value = new int[this.value.length];

        return clone(newObj);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
