///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DoubleResult.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.22 $
//            $Date: 2004/03/15 23:16:14 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.desc.result;

import wsi.ra.text.DecimalFormatHelper;
import wsi.ra.text.DecimalFormatter;

import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.NativeValue;
import joelib.desc.NumberFormatDescResult;

import joelib.io.IOType;
import joelib.io.types.cml.ResultCMLProperties;

import joelib.util.types.StringString;


/**
 * Double results.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.22 $, $Date: 2004/03/15 23:16:14 $
 */
public class DoubleResult extends JOEPairData implements Cloneable, DescResult,
    NativeValue, ResultCMLProperties, NumberFormatDescResult,
    java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.desc.result.DoubleResult");

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public double value;
    private Hashtable cmlProperties;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the DoubleResult object
     */
    public DoubleResult()
    {
        dataType = JOEDataType.JOE_PAIR_DATA;
        this.setAttribute("DescriptorResult");

        this._value = this;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Enumeration getCMLProperties()
    {
        if (cmlProperties == null)
        {
            return null;
        }

        return cmlProperties.elements();
    }

    /**
     *  Sets the double attribute of the DoubleResult object
     *
     * @param _v  The new double value
     */
    public void setDouble(double _v)
    {
        value = _v;
    }

    /**
     *  Gets the double attribute of the DoubleResult object
     *
     * @return   The double value
     */
    public double getDouble()
    {
        return value;
    }

    /**
     * Sets the doubleNV attribute of the DoubleResult object
     *
     * @param _value  The new doubleNV value
     */
    public void setDoubleNV(double _value)
    {
        value = _value;
    }

    /**
     * Gets the doubleNV attribute of the DoubleResult object
     *
     * @return   The doubleNV value
     */
    public double getDoubleNV()
    {
        return value;
    }

    /**
     * Gets the doubleNV attribute of the DoubleResult object
     *
     * @return   The doubleNV value
     */
    public boolean isDoubleNV()
    {
        return true;
    }

    /**
     * Sets the intNV attribute of the DoubleResult object
     *
     * @param _value  The new intNV value
     */
    public void setIntNV(int _value)
    {
        value = (double) _value;
    }

    /**
     * Gets the intNV attribute of the DoubleResult object
     *
     * @return   The intNV value
     */
    public int getIntNV()
    {
        return (int) value;
    }

    /**
     * Gets the intNV attribute of the DoubleResult object
     *
     * @return   The intNV value
     */
    public boolean isIntNV()
    {
        return false;
    }

    /**
     * Sets the stringNV attribute of the DoubleResult object
     *
     * @param _value  The new stringNV value
     */
    public void setStringNV(String _value)
    {
        value = Double.parseDouble(_value);
    }

    /**
     * Gets the stringNV attribute of the DoubleResult object
     *
     * @return   The stringNV value
     */
    public String getStringNV()
    {
        return DecimalFormatHelper.instance().format(value);
    }

    public void addCMLProperty(StringString property)
    {
        if (cmlProperties == null)
        {
            cmlProperties = new Hashtable(3);
        }

        cmlProperties.put(property.s1, property);
    }

    public DoubleResult clone(DoubleResult _target)
    {
        _target.value = value;
        _target.cmlProperties = this.cmlProperties;

        return _target;
    }

    public Object clone()
    {
        DoubleResult newObj = new DoubleResult();

        return clone(newObj);
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String formatDescription(IOType ioType)
    {
        return "64-bit floating point value IEEE 754";
    }

    /**
     *  Description of the Method
     *
     * @param pairData                   Description of the Parameter
     * @param ioType                     Description of the Parameter
     * @return                           Description of the Return Value
     * @exception NumberFormatException  Description of the Exception
     */
    public boolean fromPairData(IOType ioType, JOEPairData pairData)
        throws NumberFormatException
    {
        this.setAttribute(pairData.getAttribute());

        Object value = pairData.getValue();
        boolean success = false;

        if ((value != null) && (value instanceof String))
        {
            success = fromString(ioType, (String) value);
        }

        return success;
    }

    /**
     *  Description of the Method
     *
     * @param sValue                     Description of the Parameter
     * @param ioType                     Description of the Parameter
     * @return                           Description of the Return Value
     * @exception NumberFormatException  Description of the Exception
     */
    public boolean fromString(IOType ioType, String sValue)
        throws NumberFormatException
    {
        try
        {
            value = Double.parseDouble(sValue);
        }
         catch (NumberFormatException ex)
        {
            // 'Inf' are infinity values also !;-)
            if (ex.toString().lastIndexOf("Inf") != -1)
            {
                // to catch MOE descriptors correctly
                value = Double.POSITIVE_INFINITY;
            }
            else if ((ex.toString().lastIndexOf("NaN") != -1) ||
                    (ex.toString().lastIndexOf("-Na") != -1))
            {
                // to catch MOE descriptors correctly
                value = Double.NaN;
            }
            else if (ex.toString().lastIndexOf("-In") != -1)
            {
                // to catch MOE descriptors correctly
                value = Double.NEGATIVE_INFINITY;
            }
            else
            {
                logger.error(ex.toString());
                throw ex;
            }

            //System.out.println("double result value set to "+value);
        }

        return true;
    }

    /**
     *  Constructor for the DoubleResult object
     *
     * @param _descName  Description of the Parameter
     * @return           Description of the Return Value
     */
    public boolean init(String _descName)
    {
        this.setAttribute(_descName);

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType)
    {
        return toString(ioType, DecimalFormatHelper.instance());
    }

    /**
     *  Description of the Method
     *
     * @param ioType  Description of the Parameter
     * @return        Description of the Return Value
     */
    public String toString(IOType ioType, DecimalFormatter format)
    {
        return format.format(value);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
