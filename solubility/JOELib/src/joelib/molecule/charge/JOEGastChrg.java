///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEGastChrg.java,v $
//  Purpose:  Calculates the Gasteiger charge.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.16 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule.charge;

import java.util.Vector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.JOEHelper;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;


/**
 * Partial charge calculation.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.16 $, $Date: 2004/07/25 20:43:24 $
 * @cite gm78
 */
public class JOEGastChrg implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private final static double MX_GASTEIGER_DENOM = GasteigerState.MX_GASTEIGER_DENOM;
    private final static double MX_GASTEIGER_DAMP = GasteigerState.MX_GASTEIGER_DAMP;
    private final static int MX_GASTEIGER_ITERS = GasteigerState.MX_GASTEIGER_ITERS;

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  {@link java.util.Vector} of <tt>GasteigerState</tt>
     */
    private Vector _gsv;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEGastChrg object
     */
    public JOEGastChrg()
    {
        _gsv = new Vector();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean assignPartialCharges(JOEMol mol)
    {
        //InitialPartialCharges(mol);
        JOEAtom atom;
        GasteigerState gasteigerState;

        gSVResize(mol.numAtoms() + 1);

        double[] a = new double[1];
        double[] b = new double[1];
        double[] c = new double[1];
        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (!gasteigerSigmaChi(atom, a, b, c))
            {
                return (false);
            }

            gasteigerState = ((GasteigerState) _gsv.get(atom.getIdx()));

            // set a,b and c values
            // and the denominator !
            gasteigerState.setValues(a[0], b[0], c[0], atom.getPartialCharge());
        }

        double alpha;

        double charge;

        double denom;
        int j;
        int iter;
        JOEBond bond;
        JOEAtom src;
        JOEAtom dst;

        alpha = 1.0;

        for (iter = 0; iter < MX_GASTEIGER_ITERS; iter++)
        {
            alpha *= MX_GASTEIGER_DAMP;

            for (j = 1; j < _gsv.size(); j++)
            {
                gasteigerState = ((GasteigerState) _gsv.get(j));
                charge = gasteigerState.q;
                gasteigerState.chi = (((gasteigerState.c * charge) +
                    gasteigerState.b) * charge) + gasteigerState.a;
            }

            BondIterator bit = mol.bondIterator();

            while (bit.hasNext())
            {
                bond = bit.nextBond();

                src = bond.getBeginAtom();
                dst = bond.getEndAtom();

                gasteigerState = ((GasteigerState) _gsv.get(src.getIdx()));

                GasteigerState gasteigerState2 = ((GasteigerState) _gsv.get(dst.getIdx()));

                if (gasteigerState.chi >= gasteigerState2.chi)
                {
                    if (dst.isHydrogen())
                    {
                        denom = MX_GASTEIGER_DENOM;
                    }
                    else
                    {
                        denom = gasteigerState2.denom;
                    }
                }
                else
                {
                    if (src.isHydrogen())
                    {
                        denom = MX_GASTEIGER_DENOM;
                    }
                    else
                    {
                        denom = gasteigerState.denom;
                    }
                }

                charge = (gasteigerState.chi - gasteigerState2.chi) / denom;
                gasteigerState.q -= (alpha * charge);
                gasteigerState2.q += (alpha * charge);
            }
        }

        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            gasteigerState = ((GasteigerState) _gsv.get(atom.getIdx()));
            atom.setPartialCharge(gasteigerState.q);
        }

        return (true);
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }

    /**
     *  Description of the Method
     *
     * @param  size  Description of the Parameter
     */
    public void gSVResize(int size)
    {
        //GasteigerState gs;
        //    for (int i = 0; i < _gsv.size(); i++)
        //    {
        //      Object  obj  = _gsv.get(i);
        //      obj = null;
        //    }
        _gsv.clear();

        for (int j = 0; j < size; j++)
        {
            _gsv.add(new GasteigerState());
        }
    }

    /**
     * @param  a     of type <tt>double[0]</tt> .
     * @param  b     of type <tt>double[0]</tt> .
     * @param  c     of type <tt>double[0]</tt> .
     * @param  atom  Description of the Parameter
     * @return       Description of the Return Value
     */
    private boolean gasteigerSigmaChi(JOEAtom atom, double[] a, double[] b,
        double[] c)
    {
        int count;
        double[] val = new double[]{0.0, 0.0, 0.0};

        switch (atom.getAtomicNum())
        {
        case 1:

            //H
            val[0] = 0.37f;
            val[1] = 7.17f;
            val[2] = 12.85f;

            break;

        case 6:

            //C
            if (atom.getHyb() == 3)
            {
                val[0] = 0.68f;
                val[1] = 7.98f;
                val[2] = 19.04f;
            }

            if (atom.getHyb() == 2)
            {
                val[0] = 0.98f;
                val[1] = 8.79f;
                val[2] = 19.62f;
            }

            if (atom.getHyb() == 1)
            {
                val[0] = 1.67f;
                val[1] = 10.39f;
                val[2] = 20.57f;
            }

            break;

        case 7:

            //N
            if (atom.getHyb() == 3)
            {
                if ((atom.getValence() == 4) || (atom.getFormalCharge() != 0))
                {
                    val[0] = 0.0f;
                    val[1] = 0.0f;
                    val[2] = 23.72f;
                }
                else
                {
                    val[0] = 2.08f;
                    val[1] = 11.54f;
                    val[2] = 23.72f;
                }
            }

            if (atom.getHyb() == 2)
            {
                if (JOEHelper.EQ(atom.getType(), "Npl") ||
                        JOEHelper.EQ(atom.getType(), "Nam"))
                {
                    val[0] = 2.46f;
                    val[1] = 12.32f;
                    val[2] = 24.86f;
                }
                else
                {
                    val[0] = 2.57f;
                    val[1] = 12.87f;
                    val[2] = 24.87f;
                }
            }

            if (atom.getHyb() == 1)
            {
                val[0] = 3.71f;
                val[1] = 15.68f;
                val[2] = 27.11f;
            }

            break;

        case 8:

            //O
            if (atom.getHyb() == 3)
            {
                val[0] = 2.65f;
                val[1] = 14.18f;
                val[2] = 28.49f;
            }

            if (atom.getHyb() == 2)
            {
                val[0] = 3.75f;
                val[1] = 17.07f;
                val[2] = 31.33f;
            }

            break;

        case 9:

            //F
            val[0] = 3.12f;
            val[1] = 14.66f;
            val[2] = 30.82f;

            break;

        case 15:

            //P
            val[0] = 1.62f;
            val[1] = 8.90f;
            val[2] = 18.10f;

            break;

        case 16:

            //S
            count = atom.countFreeOxygens();

            if ((count == 0) || (count == 1))
            {
                val[0] = 2.39f;
                val[1] = 10.14f;
                val[2] = 20.65f;
            }

            if (count > 1)
            {
                val[0] = 2.39f;
                val[1] = 12.00f;
                val[2] = 24.00f;
            }

            /*S2? if (count == 0) {val[0] = 2.72;val[1] = 10.88;val[2] = 21.69;} */
            break;

        case 17:

            //Cl
            val[0] = 2.66f;
            val[1] = 11.00f;
            val[2] = 22.04f;

            break;

        case 35:

            //Br
            val[0] = 2.77f;
            val[1] = 10.08f;
            val[2] = 19.71f;

            break;

        case 53:

            //I
            val[0] = 2.90f;
            val[1] = 9.90f;
            val[2] = 18.82f;

            break;

        case 13:

            //Al
            val[0] = 1.06f;
            val[1] = 5.47f;
            val[2] = 11.65f;

            break;
        }

        if (val[2] != 0.0)
        {
            a[0] = val[1];
            b[0] = (val[2] - val[0]) / 2;
            c[0] = ((val[2] + val[0]) / 2) - val[1];
        }
        else
        {
            return (false);
        }

        return (true);
    }

    /**
     *  Description of the Method
     *
     * @param  mol  Description of the Parameter
     */
    private void initialPartialCharges(JOEMol mol)
    {
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (atom.isCarboxylOxygen())
            {
                atom.setPartialCharge(-0.500f);
            }
            else if (atom.isPhosphateOxygen() && (atom.getHvyValence() == 1))
            {
                atom.setPartialCharge(-0.666f);
            }
            else if (atom.isSulfateOxygen())
            {
                atom.setPartialCharge(-0.500f);
            }
            else
            {
                atom.setPartialCharge((double) atom.getFormalCharge());
            }
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
