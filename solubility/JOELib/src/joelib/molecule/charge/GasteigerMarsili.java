///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GasteigerMarsili.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.14 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule.charge;

import org.apache.log4j.Category;

import joelib.data.JOEPhModel;

import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.SimpleDoubleAtomProperty;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;


/**
 * Partial charge calculation.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.14 $, $Date: 2004/07/25 20:43:24 $
 * @cite gm78
 */
public class GasteigerMarsili extends SimpleDoubleAtomProperty
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.molecule.charge.GasteigerMarsili");
    public static final String DESC_KEY = "Gasteiger_Marsili";

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the partial charge calculation.
     */
    public GasteigerMarsili()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.AtomDoubleResult");
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public double[] getDoubleAtomProperties(JOEMol mol)
    {
        JOEMol useMol = null;

        if (mol.automaticPartialCharge())
        {
            // are we allowed to assign and overwrite partial charges ?
            useMol = mol;
        }
        else
        {
            // if not then clone molecule, because it contains
            // probably partial charges from another vendor
            useMol = (JOEMol) mol.clone();

            //	seed partial charges are set in the atom typing procedure
            JOEPhModel.instance().assignSeedPartialCharge(useMol);

            JOEGastChrg gc = new JOEGastChrg();
            gc.assignPartialCharges(useMol);
        }

        // get partial charges for all atoms
        JOEAtom atom;
        AtomIterator ait = useMol.atomIterator();
        double[] pCharges = new double[useMol.numAtoms()];
        int i = 0;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            pCharges[i++] = atom.getPartialCharge();
        }

        return pCharges;
    }

    /**
     * @return
     */
    private Object getParent()
    {
        // TODO Auto-generated method stub
        return null;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
