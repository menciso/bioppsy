///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOERTree.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2003/08/19 13:11:27 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule;


/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.util.Vector;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Atom tree.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2003/08/19 13:11:27 $
 */
public class JOERTree implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private member variables
     *------------------------------------------------------------------------- */
    private JOEAtom _atom;
    private JOERTree _prv;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the atom tree.
     *
     * @param  atom  atom
     * @param  prv   previous atom tree
     */
    public JOERTree(JOEAtom atom, JOERTree prv)
    {
        _atom = atom;
        _prv = prv;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     * Gets the atom index of this atom tree node.
     *
     * @return    The atom index
     */
    public int getAtomIdx()
    {
        return _atom.getIdx();
    }

    /**
     *  Desctructor
     */
    public void finalize()
    {
    }

    /**
     * Adds this atom to the given <tt>path</tt>.
     *
     * @param  path  {@link java.util.Vector} of <tt>JOEAtom</tt>
     */
    public void pathToRoot(Vector path)
    {
        path.add(_atom);

        if (_prv != null)
        {
            _prv.pathToRoot(path);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
