///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MolGenerationHelper.java,v $
//  Purpose:  Counts the number of descriptors and molecules in a molecule file.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule.generation;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.iterator.NbrAtomIterator;
import joelib.util.types.IntInt;


/**
 * Helper class for molecule generation.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:24 $
 */
public class MolGenerationHelper
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.molecule.generation.MolGenerationHelper");

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Create molecule by adding R-groups to the given base molecule.
     * For the fragments rudimentary 3D coordinates will be calculated,
     * if the base molecule contains already 2D or 3D coordinates.
     *
     * @param baseMolecule basis molecule
     * @param connections where i1 is the placeholder atom and i2 is the R-group
     * @param rGroups the fragments to add to the base molecule
     * @return JOEMol the generated molecule
     */
    public JOEMol createNewMolecule(JOEMol baseMolecule, IntInt[] connections,
        JOEMol[] rGroups)
    {
        JOEMol mol = (JOEMol) baseMolecule.clone();

        int size = rGroups.length;
        StringBuffer sb = new StringBuffer();
        JOEMol fragment;

        for (int m = 0; m < size; m++)
        {
            fragment = rGroups[m];
            sb.append(fragment.getTitle());

            //System.out.print(rGroups[m] + " ");
            if (m < (size - 1))
            {
                sb.append(',');
            }
        }

        sb.append('-');
        sb.append(mol.getTitle());
        mol.setTitle(sb.toString());

        JOEAtom[] placeHolderAtoms = new JOEAtom[connections.length];

        for (int i = 0; i < connections.length; i++)
        {
            IntInt ii = connections[i];
            fragment = rGroups[ii.i2 - 1];
            placeHolderAtoms[i] = mol.getAtom(ii.i1);

            //System.out.println("connect fragment " + ii.i2 + " to (place holder) atom " + ii.i1+" "+placeHolderAtoms[i]);
        }

        boolean createCoordinates = mol.has2D() || mol.has3D();

        // do ring search for coordinate generation
        if (createCoordinates)
        {
            mol.getSSSR();
        }

        JOEBond bond;

        //		JOEAtom atom;
        XYZVector v = null;

        for (int i = 0; i != rGroups.length; i++)
        {
            IntInt ii = connections[i];
            fragment = rGroups[ii.i2 - 1];

            JOEAtom placeHolderAtom = placeHolderAtoms[i];

            //			int placeHolderIdx = placeHolderAtom.getIdx();
            JOEAtom nbr;
            int ind_nbr;
            int frag_indx;
            JOEAtom frag_next;
            JOEAtom frag1;

            if (placeHolderAtom.getBonds().size() != 0)
            {
                // get bond to place holder atom
                // The atom has only one bond so this is simple
                bond = (JOEBond) placeHolderAtom.getBonds().get(0);

                // get neighbour atom of the place holder atom
                nbr = bond.getNbrAtom(placeHolderAtom);

                mol.beginModify();

                // delete bond to place holder atom
                mol.deleteBond(bond);

                // delete atom to place holder atom
                mol.deleteAtom(placeHolderAtom);

                //System.out.println("delete atom "+placeHolderAtom.getIdx()+" "+placeHolderAtom+" "+placeHolderAtom.getVector());
                mol.endModify();

                frag1 = (JOEAtom) fragment.getAtom(1).clone();

                // create new bond vector
                if (createCoordinates)
                {
                    v = new XYZVector();

                    double bondlen = JOEElementTable.instance().getCovalentRad(nbr.getAtomicNum()) +
                        JOEElementTable.instance().getCovalentRad(frag1.getAtomicNum());
                    nbr.getNewBondVector(v, bondlen);
                }

                mol.beginModify();

                // set new atom coordinates
                // and add first Rgroup atom
                // with a single bond
                if (createCoordinates)
                {
                    frag1.setVector(v.x(), v.y(), v.z());
                }

                ind_nbr = nbr.getIdx();
                mol.addAtom(frag1);
                frag_indx = mol.numAtoms();
                mol.addBond(ind_nbr, frag_indx, 1);

                // add all other atoms
                if (fragment.numAtoms() > 1)
                {
                    int firstConnectIdx = frag_indx;

                    // add atoms
                    for (int k = 2; k <= fragment.numAtoms(); k++)
                    {
                        frag_next = (JOEAtom) fragment.getAtom(k).clone();
                        mol.addAtom(frag_next);
                    }

                    int begin;
                    int end;

                    for (int k = 0; k < fragment.numBonds(); k++)
                    {
                        bond = fragment.getBond(k);
                        begin = bond.getBeginAtomIdx();
                        end = bond.getEndAtomIdx();

                        // use relative atom indices to first fragment atom
                        // use original bond order
                        mol.addBond(firstConnectIdx - 1 + begin,
                            firstConnectIdx - 1 + end, bond.getBO());
                    }

                    // create coordinates for the rest of the fragment atoms
                    if (createCoordinates)
                    {
                        double bondlen;
                        int generated = 0;

                        // repeat iteration until all fragment atoms has
                        // assigned coordinates (important for non-linear groups)
                        while (generated <= (fragment.numAtoms() - 2))
                        {
                            for (int k = 1; k < fragment.numAtoms(); k++)
                            {
                                frag_next = mol.getAtom(firstConnectIdx + k);

                                //System.out.println("fragment atom "+(k+1)+" "+frag_next.getX());
                                if ((frag_next.getX() == 0.0) &&
                                        (frag_next.getY() == 0.0) &&
                                        (frag_next.getZ() == 0.0))
                                {
                                    NbrAtomIterator nait = frag_next.nbrAtomIterator();

                                    while (nait.hasNext())
                                    {
                                        nbr = nait.nextNbrAtom();

                                        //System.out.println("fragment atom "+(nbr.getIdx()-firstConnectIdx)+" "+nbr.getX());
                                        if ((nbr.getX() != 0.0) &&
                                                (nbr.getY() != 0.0))
                                        {
                                            v = new XYZVector();

                                            // now we have a complete molecule
                                            // and can use corrected bonds
                                            bondlen = JOEElementTable.instance()
                                                                     .correctedBondRad(nbr.getAtomicNum(),
                                                    nbr.getHyb()) +
                                                JOEElementTable.instance()
                                                               .correctedBondRad(frag_next.getAtomicNum(),
                                                    frag_next.getHyb());
                                            nbr.getNewBondVector(v, bondlen);
                                            frag_next.setVector(v.x(), v.y(),
                                                v.z());
                                            generated++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                mol.endModify();
            }
            else
            {
                logger.error("Atom " + placeHolderAtom.getIdx() +
                    " has no bond to other atoms.");

                return null;
            }
        }

        return mol;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
