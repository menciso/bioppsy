///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEResidue.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/02/20 13:11:59 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule;


/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
import java.util.Vector;

import joelib.util.iterator.AtomIterator;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================*/

/**
 * Residue informations.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/02/20 13:11:59 $
 */
public class JOEResidue implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    protected String _chain;
    protected String _resname;
    protected Vector _atomid; // String
    protected Vector _atoms; // JOEAtom
    protected Vector _hetatm; // boolean
    protected Vector _sernum; // int
    protected int _chainnum;

    /*-------------------------------------------------------------------------*
     * protected member variables
     *-------------------------------------------------------------------------*/
    protected int _idx;
    protected int _resnum;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------*/
    public JOEResidue()
    {
        _chainnum = 0;
        _resnum = 0;
        _resname = "";
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void setAtomID(JOEAtom atom, String id)
    {
        _atomid.set(getIndex(atom), id);
    }

    public String getAtomID(JOEAtom atom)
    {
        return (String) (_atomid.get(getIndex(atom)));
    }

    public void setChain(String chain)
    {
        _chain = chain;
    }

    public String getChain()
    {
        return (_chain);
    }

    public void setChainNum(int chainnum)
    {
        _chainnum = chainnum;
    }

    public int getChainNum()
    {
        return (_chainnum);
    }

    public void setHetAtom(JOEAtom atom, boolean hetatm)
    {
        boolean[] btmp;
        btmp = (boolean[]) _hetatm.get(getIndex(atom));
        btmp[0] = hetatm;
    }

    public boolean isHetAtom(JOEAtom atom)
    {
        return ((boolean[]) _hetatm.get(getIndex(atom)))[0];
    }

    public void setIdx(int idx)
    {
        _idx = idx;
    }

    public int getIdx()
    {
        return (_idx);
    }

    public void setName(String resname)
    {
        _resname = resname;
    }

    public String getName()
    {
        return (_resname);
    }

    public void setNum(int resnum)
    {
        _resnum = resnum;
    }

    public int getNum()
    {
        return (_resnum);
    }

    public void setSerialNum(JOEAtom atom, int sernum)
    {
        int[] itmp;
        itmp = (int[]) _sernum.get(getIndex(atom));
        itmp[0] = sernum;
    }

    public int getSerialNum(JOEAtom atom)
    {
        return ((int[]) _sernum.get(getIndex(atom)))[0];
    }

    /**
     * Gets an iterator over all atoms in this residue.
     *
     * @return   the atom iterator for this residue
     */
    public AtomIterator atomIterator()
    {
        return new AtomIterator(_atoms);
    }

    public void clear()
    {
        for (int i = 0; i < _atoms.size(); i++)
        {
            ((JOEAtom) _atoms.get(i)).setResidue(null);
        }

        _chainnum = 0;
        _resnum = 0;
        _resname = "";

        _atoms.clear();
        _atomid.clear();
        _hetatm.clear();
        _sernum.clear();
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/

    //copy residue information
    public Object clone()
    {
        return (new JOEResidue());
    }

    public JOEResidue clone(JOEResidue to)
    {
        return clone(this, to);
    }

    public static JOEResidue clone(JOEResidue src, JOEResidue to)
    {
        to._chainnum = src._chainnum;
        to._resnum = src._resnum;
        to._resname = src._resname;
        to._atomid = src._atomid;
        to._hetatm = src._hetatm;
        to._sernum = src._sernum;

        return to;
    }

    public void addAtom(JOEAtom atom)
    {
        if (atom != null)
        {
            atom.setResidue(this);

            _atoms.add(atom);
            _atomid.add("");
            _hetatm.add(new boolean[]{false});
            _sernum.add(new int[]{0});
        }
    }

    public void finalize()
    {
        JOEAtom atom;

        for (int i = 0; i < _atoms.size(); i++)
        {
            atom = (JOEAtom) _atoms.get(i);
            atom.setResidue(null);
        }

        _atoms.clear();
    }

    //  public void insertAtom(JOEAtom atom)
    //  {
    //    if (atom != null)
    //    {
    //        atom.setResidue(this);
    //
    //        _atoms.add(atom);
    //        _atomid.ensureCapacity(_atoms.size());
    //        _hetatm.ensureCapacity(_atoms.size());
    //        _sernum.ensureCapacity(_atoms.size());
    //   _atomid.add("");
    //   _hetatm.add(new boolean[]{false});
    //   _sernum.add(new int[]{0});
    //    }
    //  }
    public void removeAtom(JOEAtom atom)
    {
        if (atom != null)
        {
            int idx = getIndex(atom);

            if (idx >= 0)
            {
                atom.setResidue(null);

                _atoms.remove(idx);
                _atomid.remove(idx);
                _hetatm.remove(idx);
                _sernum.remove(idx);
            }
        }
    }

    protected int getIndex(JOEAtom atom)
    {
        return _atoms.indexOf(atom);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
