///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BondProperties.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2003/08/19 13:11:27 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule.types;


/*==========================================================================*
 * IMPORTS
 *==========================================================================*/
/*==========================================================================*
 * INTERFACE DECLARATION
 *==========================================================================*/

/**
 * Interface to access bond properties.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2003/08/19 13:11:27 $
 */
public interface BondProperties
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public void setDoubleValue(int bondIdx, double value);

    public double getDoubleValue(int bondIdx);

    public void setIntValue(int bondIdx, int value);

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/
    public int getIntValue(int bondIdx);

    public void setStringValue(int bondIdx, String value);

    public String getStringValue(int bondIdx);

    public void setValue(int bondIdx, Object value);

    public Object getValue(int bondIdx);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
