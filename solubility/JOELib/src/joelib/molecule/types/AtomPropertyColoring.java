///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomPropertyColoring.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.5 $
//            $Date: 2004/02/20 13:12:01 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule.types;

import wsi.ra.tool.PropertyHolder;

/*==========================================================================*
 * IMPORTS
 *========================================================================== */
import java.awt.Color;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.JOEHelper;


/*==========================================================================*
 * CLASS DECLARATION
 *========================================================================== */

/**
 * Some methods to faciliate the work with descriptors.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.5 $, $Date: 2004/02/20 13:12:01 $
 */
public final class AtomPropertyColoring
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------*/

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.molecule.types.AtomPropertyColoring");

    //~ Instance fields ////////////////////////////////////////////////////////

    // variables for property coloring
    private AtomProperties data = null;
    private Color maxColor = new Color(1.0f, 0.0f, 0.0f);
    private Color minColor = new Color(0.0f, 0.0f, 1.0f);
    private JOEElementTable etab = JOEElementTable.instance();
    private JOEMol mol4coloring;

    /*-------------------------------------------------------------------------*
     * private  member variables
     *------------------------------------------------------------------------- */
    private PropertyHolder propertyHolder;
    private String atomProperty2Use = "Gasteiger_Marsili";
    private boolean dataInitialised = false;
    private boolean usePropertyColoring = false;
    private double maxDataValue;
    private double minDataValue;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private member variables
     *------------------------------------------------------------------------- */
    /*-------------------------------------------------------------------------*
     * constructor
     *------------------------------------------------------------------------- */

    /**
     *  Constructor for the DescriptorHelper object
     *
     * @param  _value           Description of the Parameter
     * @param  _name            Description of the Parameter
     * @param  _representation  Description of the Parameter
     */
    public AtomPropertyColoring()
    {
        propertyHolder = PropertyHolder.instance();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public Color getAtomColor(JOEAtom atom)
    {
        if (atom == null)
        {
            return null;
        }

        if ((mol4coloring != null) && (mol4coloring != atom.getParent()))
        {
            logger.warn("Coloring (" + mol4coloring.getTitle() +
                ") should be newly initialized for molecule " +
                atom.getParent().getTitle());
        }

        if (usePropertyColoring)
        {
            double delta = maxDataValue - minDataValue;

            //System.out.println("data:"+data);
            float val = (float) ((data.getDoubleValue(atom.getIdx()) -
                minDataValue) / delta);
            float r = (((maxColor.getRed() - minColor.getRed()) * val) +
                minColor.getRed()) / 255.0f;
            float g = (((maxColor.getGreen() - minColor.getGreen()) * val) +
                minColor.getGreen()) / 255.0f;
            float b = (((maxColor.getBlue() - minColor.getBlue()) * val) +
                minColor.getBlue()) / 255.0f;

            //        System.out.println(""+atom.getIdx()+": "+data.getDoubleValue(atom.getIdx())+": "+val);
            //        System.out.println("rgb:"+r+" "+g+" "+b);
            return new Color(r, g, b);
        }
        else
        {
            int atomNum = atom.getAtomicNum();

            return etab.getColor(atomNum);
        }
    }

    public JOEMol getMoleculeForColoring()
    {
        return mol4coloring;
    }

    public void useAtomPropertyColoring(JOEMol mol, String property)
    {
        if ((property == null) || (mol == null))
        {
            usePlainColoring();

            return;
        }

        atomProperty2Use = property;

        mol4coloring = mol;

        DescResult result = null;

        try
        {
            result = DescriptorHelper.instance().descFromMol(mol, property);
        }
         catch (DescriptorException e)
        {
            logger.error(e.toString());
            logger.error("Use plain atom coloring.");
            usePropertyColoring = false;
        }

        if (result == null)
        {
            logger.error("Can't get atom property " + property +
                " for atom coloring in " + this.getClass().getName());
        }

        //      System.out.println(""+JOEHelper.hasInterface(genericData, "joelib.molecule.types.AtomProperties"));
        //      System.out.println(""+(genericData instanceof AtomDynamicResult));
        //      System.out.println(""+(genericData instanceof JOEPairData));
        if (JOEHelper.hasInterface(result, "AtomProperties"))
        {
            dataInitialised = true;
            data = (AtomProperties) result;
            minDataValue = Double.MAX_VALUE;
            maxDataValue = -Double.MAX_VALUE;

            double value;

            for (int i = 1; i <= mol.numAtoms(); i++)
            {
                value = data.getDoubleValue(i);

                if (value > maxDataValue)
                {
                    maxDataValue = value;
                }

                if (value < minDataValue)
                {
                    minDataValue = value;
                }

                //          System.out.println(""+i+": "+value);
            }

            usePropertyColoring = true;
        }
        else
        {
            logger.error("Data for atom coloring has wrong format in " +
                this.getClass().getName());
        }
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */
    public void usePlainColoring()
    {
        usePropertyColoring = false;
    }

    /*-------------------------------------------------------------------------*
     * private methods
     *------------------------------------------------------------------------- */
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
