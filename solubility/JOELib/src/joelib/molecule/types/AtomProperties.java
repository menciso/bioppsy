///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomProperties.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.8 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule.types;

import java.util.zip.DataFormatException;


/**
 * Interface to access atom properties.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:24 $
 */
public interface AtomProperties
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Sets the doubleValue attribute of the AtomProperties object
     *
     * @param atomIdx  The new doubleValue value
     * @param value    The new doubleValue value
     */
    public void setDoubleValue(int atomIdx, double value);

    /**
     * Gets the doubleValue attribute of the AtomProperties object
     *
     * @param atomIdx  Description of the Parameter
     * @return         The doubleValue value
     */
    public double getDoubleValue(int atomIdx);

    /**
     * Sets the intValue attribute of the AtomProperties object
     *
     * @param atomIdx  The new intValue value
     * @param value    The new intValue value
     */
    public void setIntValue(int atomIdx, int value);

    /**
     * Gets the intValue attribute of the AtomProperties object
     *
     * @param atomIdx                  Description of the Parameter
     * @return                         The intValue value
     * @exception DataFormatException  if this is not an <tt>int</tt> atom property
     */
    public int getIntValue(int atomIdx) throws DataFormatException;

    /**
     * Sets the stringValue attribute of the AtomProperties object
     *
     * @param atomIdx  The new stringValue value
     * @param value    The new stringValue value
     */
    public void setStringValue(int atomIdx, String value);

    /**
     * Gets the stringValue attribute of the AtomProperties object
     *
     * @param atomIdx  Description of the Parameter
     * @return         The stringValue value
     */
    public String getStringValue(int atomIdx);

    /**
     * Sets the value attribute of the AtomProperties object
     *
     * @param atomIdx  The new value value
     * @param value    The new value value
     */
    public void setValue(int atomIdx, Object value);

    /**
     * Gets the value attribute of the AtomProperties object
     *
     * @param atomIdx  Description of the Parameter
     * @return         The value value
     */
    public Object getValue(int atomIdx);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
