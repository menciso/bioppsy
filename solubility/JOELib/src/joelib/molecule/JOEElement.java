///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEElement.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2003/08/19 13:11:27 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule;


/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import java.awt.Color;

import java.util.StringTokenizer;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================  */

/**
 * Element entry in periodic table.
 *
 * @author    wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2003/08/19 13:11:27 $
 */
public class JOEElement implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private Color _rgb;
    private String _exteriorElectrons;
    private String _symbol;
    private byte _group;
    private byte _period;
    private double _Rbo;
    private double _Rcov;
    private double _Rvdw;
    private double _eAffinity;
    private double _enAllredRochow;
    private double _enPauling;
    private double _enSanderson;
    private double _mass;
    private int _maxbonds;

    /*-------------------------------------------------------------------------*
     * private member variables
     *-------------------------------------------------------------------------  */
    private int _num;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the JOEElement object
     */
    public JOEElement()
    {
    }

    /**
     *  Constructor for the JOEElement object
     *
     * @param num                Description of the Parameter
     * @param sym                Description of the Parameter
     * @param rcov               Description of the Parameter
     * @param rbo                Description of the Parameter
     * @param rvdw               Description of the Parameter
     * @param maxbo              Description of the Parameter
     * @param mass               Description of the Parameter
     * @param rgb                Description of the Parameter
     * @param exteriorElectrons  Description of the Parameter
     * @param period             Description of the Parameter
     * @param group              Description of the Parameter
     * @param enAllredRochow     Description of the Parameter
     * @param enPauling          Description of the Parameter
     * @param eAffinity          Description of the Parameter
     */
    public JOEElement(int num, String sym, double rcov, double rbo,
        double rvdw, int maxbo, Color rgb, double mass,
        String exteriorElectrons, byte period, byte group,
        double enAllredRochow, double enPauling, double enSanderson,
        double eAffinity)
    {
        _num = num;
        _symbol = sym;
        _Rcov = rcov;
        _Rbo = rbo;
        _Rvdw = rvdw;
        _maxbonds = maxbo;
        _rgb = rgb;
        _mass = mass;
        _exteriorElectrons = exteriorElectrons;
        _period = period;
        _group = group;
        _enAllredRochow = enAllredRochow;
        _enPauling = enPauling;
        _eAffinity = eAffinity;
        _enSanderson = enSanderson;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the atom electronegativity after Allred and Rochow.
     *
     * @return   The allredRochowEN value
     */
    public final double getAllredRochowEN()
    {
        return _enAllredRochow;
    }

    /**
     *  Gets the atomicNum attribute of the JOEElement object
     *
     * @return   The atomicNum value
     */
    public final int getAtomicNum()
    {
        return (_num);
    }

    /**
     *  Gets the boRad attribute of the JOEElement object
     *
     * @return   The boRad value
     */
    public final double getBoRad()
    {
        return (_Rbo);
    }

    /**
     * Gets the color attribute of the JOEElement object
     *
     * @return   The color value
     */
    public final Color getColor()
    {
        return (_rgb);
    }

    /**
     *  Gets the covalentRad attribute of the JOEElement object
     *
     * @return   The covalentRad value
     */
    public final double getCovalentRad()
    {
        return (_Rcov);
    }

    /**
     * Gets the electronAffinity attribute of the JOEElement object
     *
     * @return   The electronAffinity value
     */
    public final double getElectronAffinity()
    {
        return _eAffinity;
    }

    /**
     * Gets the exteriorElectrons attribute of the JOEElement object
     *
     * @return   The exteriorElectrons value
     */
    public final int getExteriorElectrons()
    {
        StringTokenizer el = new StringTokenizer(_exteriorElectrons, "spdf");

        int extEl = 0;
        String nextEl;

        while (el.hasMoreTokens())
        {
            nextEl = el.nextToken();
            extEl += Integer.parseInt(nextEl);
        }

        return extEl;
    }

    /**
     * Gets the group attribute of the JOEElement object
     *
     * @return   The group value
     */
    public final int getGroup()
    {
        return (_group);
    }

    /**
     *  Gets the mass attribute of the JOEElement object
     *
     * @return   The mass value
     */
    public final double getMass()
    {
        return (_mass);
    }

    /**
     *  Gets the maxBonds attribute of the JOEElement object
     *
     * @return   The maxBonds value
     */
    public final int getMaxBonds()
    {
        return (_maxbonds);
    }

    /**
     * Gets the atom electronegativity after Pauling.
     *
     * @return   The paulingEN value
     */
    public final double getPaulingEN()
    {
        return _enPauling;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */

    /**
     * Gets the period attribute of the JOEElement object
     *
     * @return   The period value
     */
    public final int getPeriod()
    {
        return (_period);
    }

    /**
     * Gets the atom electronegativity after Pauling.
     *
     * @return   The paulingEN value
     */
    public final double getSandersonEN()
    {
        return _enSanderson;
    }

    /**
     *  Gets the symbol attribute of the JOEElement object
     *
     * @return   The symbol value
     */
    public final String getSymbol()
    {
        return (_symbol);
    }

    /**
     *  Gets the vdwRad attribute of the JOEElement object
     *
     * @return   The vdwRad value
     */
    public final double getVdwRad()
    {
        return (_Rvdw);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
