///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: GenericDataHolder.java,v $
//  Purpose:  Generic data holder for molecules.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.23 $
//            $Date: 2004/02/20 13:11:58 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule;


/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEGenericData;
import joelib.data.JOEPairData;

import joelib.desc.ResultFactory;

import joelib.util.BitSet14;
import joelib.util.iterator.GenericDataIterator;


/*==========================================================================*
 * CLASS DECLARATION
 *==========================================================================  */

/**
 * Holder class of generic data objects for one molecule.
 *
 * @author    wegnerj
 * @license   GPL
 * @cvsversion    $Revision: 1.23 $, $Date: 2004/02/20 13:11:58 $
 */
public class GenericDataHolder implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
             *  Obtain a suitable logger.
             */
    private static Category logger = Category.getInstance(
            "joelib.molecule.GenericDataHolder");

    //~ Instance fields ////////////////////////////////////////////////////////

    JOEMol parent;
    private Hashtable pairData;

    /*-------------------------------------------------------------------------*
     * protected member variables
     *-------------------------------------------------------------------------  */
    private Vector data;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * constructor
     *-------------------------------------------------------------------------  */

    /**
     *  Constructor for the GenericDataHolder object
     *
     * @param _parent  Description of the Parameter
     */
    public GenericDataHolder(JOEMol _parent, int _dataInit, int _pairDataInit)
    {
        parent = _parent;

        data = new Vector(_dataInit);
        pairData = new Hashtable(_pairDataInit);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the data attribute of the GenericDataHolder object
     *
     * @param dt  Description of the Parameter
     * @return    The data value
     */
    public JOEGenericData getData(JOEDataType dt)
    {
        if ((data.size() == 0) && (pairData.size() == 0))
        {
            return null;
        }

        JOEGenericData genericData;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);

            if (genericData.getAttribute().equals(dt.toString()))
            {
                JOEPairData newData = ResultFactory.instance().parsePairData(parent,
                        dt.toString(), genericData);

                if (newData != null)
                {
                    data.set(i, newData);

                    return newData;
                }

                return genericData;
            }
        }

        genericData = (JOEGenericData) pairData.get(dt.toString());

        if (genericData != null)
        {
            JOEPairData newData = ResultFactory.instance().parsePairData(parent,
                    dt.toString(), genericData);

            if (newData != null)
            {
                pairData.put(dt.toString(), newData);

                return newData;
            }

            return genericData;
        }
        else
        {
            return null;
        }
    }

    /**
     * Gets the data attribute of the GenericDataHolder object
     *
     * @param s      Description of the Parameter
     * @param parse  Description of the Parameter
     * @return       <tt>null</tt> if this data don't exist
     */
    public JOEGenericData getData(String s, boolean parse)
    {
        if (s == null)
        {
            logger.warn("Data name should be not null.");

            return null;
        }

        if ((data.size() == 0) && (pairData.size() == 0))
        {
            return null;
        }

        JOEGenericData genericData;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);

            if (genericData.getAttribute().equals(s))
            {
                if (parse)
                {
                    JOEPairData newData = ResultFactory.instance()
                                                       .parsePairData(parent,
                            s, genericData);

                    if (newData != null)
                    {
                        data.set(i, newData);

                        return newData;
                    }
                }

                return genericData;
            }
        }

        genericData = (JOEGenericData) pairData.get(s);

        //        System.out.println(((JOEPairData)genericData).getValue().getClass().getName()+": "+genericData);
        if (genericData != null)
        {
            //            System.out.println("unparsed data:"+genericData);
            if (parse)
            {
                JOEPairData newData = ResultFactory.instance().parsePairData(parent,
                        s, genericData);

                if (newData != null)
                {
                    pairData.put(s, newData);

                    //                  System.out.println("parsed data:"+newData);
                    return newData;
                }
            }

            return genericData;
        }
        else
        {
            return null;
        }
    }

    /**
     * Adds a feature to the Data attribute of the GenericDataHolder object
     *
     * @param d          The feature to be added to the Data attribute
     * @param overwrite  The feature to be added to the Data attribute
     */
    public void addData(JOEGenericData d, boolean overwrite)
    {
        if (d.getDataType() == JOEDataType.JOE_PAIR_DATA)
        {
            if (overwrite)
            {
                pairData.put(d.getAttribute(), d);
            }
            else
            {
                // add only if it not already exists
                if (!pairData.containsKey(d.getAttribute()))
                {
                    pairData.put(d.getAttribute(), d);
                }
            }
        }
        else
        {
            if (!overwrite)
            {
                data.add(d);
            }
            else
            {
                JOEGenericData genericData;

                for (int i = 0; i < data.size(); i++)
                {
                    genericData = (JOEGenericData) data.get(i);

                    if (genericData.equals(d))
                    {
                        data.set(i, d);

                        return;
                    }
                }
            }
        }
    }

    /**
     * Description of the Method
     */
    public void clear()
    {
        data.clear();
        pairData.clear();
    }

    /**
     * Description of the Method
     *
     * @param s  Description of the Parameter
     * @return   Description of the Return Value
     */
    public boolean deleteData(String s)
    {
        if ((data.size() == 0) && (pairData.size() == 0))
        {
            return false;
        }

        JOEGenericData genericData;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);

            if (genericData.getAttribute().equals(s))
            {
                data.remove(i);
            }
        }

        genericData = (JOEGenericData) pairData.remove(s);

        if (genericData != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Description of the Method
     *
     * @param dt  Description of the Parameter
     */
    public void deleteData(JOEDataType dt)
    {
        if ((data.size() == 0) && (pairData.size() == 0))
        {
            return;
        }

        JOEGenericData genericData;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);

            if (genericData.getAttribute().equals(dt))
            {
                data.remove(i);
            }
        }

        genericData = (JOEGenericData) pairData.remove(dt.toString());

        if (genericData != null)
        {
            return;
        }
        else
        {
            return;
        }
    }

    /**
     * Description of the Method
     *
     * @param gd  Description of the Parameter
     */
    public void deleteData(JOEGenericData gd)
    {
        if ((data.size() == 0) && (pairData.size() == 0))
        {
            return;
        }

        JOEGenericData genericData;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);

            if (genericData.equals(gd))
            {
                data.remove(i);
            }
        }

        genericData = (JOEGenericData) pairData.get(gd.getAttribute());

        if (genericData == gd)
        {
            genericData = (JOEGenericData) pairData.remove(gd.getAttribute());
        }

        if (genericData != null)
        {
            return;
        }
        else
        {
            return;
        }
    }

    /**
     * Description of the Method
     *
     * @param vg  Description of the Parameter
     */
    public void deleteData(Vector vg)
    {
        BitSet14 notFound = new BitSet14(vg.size());

        // to vg.size because toIndex is EXCLUSIVE
        notFound.set(0, vg.size(), true);

        // remove vector entries
        Vector vdata = new Vector();
        boolean del;
        JOEGenericData genericData;
        JOEGenericData genericData2;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);
            del = false;

            for (int ii = 0; ii < vg.size(); ii++)
            {
                genericData2 = (JOEGenericData) vg.get(ii);

                if (genericData.equals(genericData2))
                {
                    del = true;
                    notFound.set(ii, false);
                    vdata.add(genericData);

                    break;
                }
            }
        }

        data.clear();
        data.ensureCapacity(vdata.size());

        for (int i = 0; i < vdata.size(); i++)
        {
            data.add(vdata.get(i));
        }

        //System.out.println("remove hash table entries");
        // remove hash table entries
        int index = 0;
        int oldIndex = -2;

        while (((index = notFound.nextSetBit(index)) != -1) &&
                (index != oldIndex))
        {
            //System.out.println("index "+index);
            genericData = (JOEGenericData) vg.get(index);
            pairData.remove(genericData.getAttribute());
            oldIndex = index;
        }
    }

    /**
     * Description of the Method
     */
    public void finalize()
    {
        data.clear();
        pairData.clear();
    }

    /**
     *  Gets the data attribute of the <tt>JOEMol</tt> object
     *
     * @return   The data value
     */

    //    public Vector getData()
    //    {
    //        return (data);
    //    }
    public GenericDataIterator genericDataIterator()
    {
        return new GenericDataIterator(data, pairData);
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */

    /**
     * Description of the Method
     *
     * @param s  Description of the Parameter
     * @return   Description of the Return Value
     */
    public boolean hasData(String s)
    {
        if (s == null)
        {
            return false;
        }

        if ((data.size() == 0) && (pairData.size() == 0))
        {
            return false;
        }

        JOEGenericData genericData;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);

            if (genericData.getAttribute().equals(s))
            {
                return true;
            }
        }

        if (pairData == null)
        {
            return false;
        }

        genericData = (JOEGenericData) pairData.get(s);

        if (genericData != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Description of the Method
     *
     * @param dt  Description of the Parameter
     * @return    Description of the Return Value
     */
    public boolean hasData(JOEDataType dt)
    {
        if ((data.size() == 0) && (pairData.size() == 0))
        {
            return false;
        }

        JOEGenericData genericData;

        for (int i = 0; i < data.size(); i++)
        {
            genericData = (JOEGenericData) data.get(i);

            //System.out.println(dt.toString()+" "+genericData.getAttribute()+" = "+genericData.getAttribute().equals(dt));
            if (genericData.getDataType().equals(dt))
            {
                return true;
            }
        }

        genericData = (JOEGenericData) pairData.get(dt.toString());

        if (genericData != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Description of the Method
     *
     * @return   Description of the Return Value
     */
    public int size()
    {
        return (data.size() + pairData.size());
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
