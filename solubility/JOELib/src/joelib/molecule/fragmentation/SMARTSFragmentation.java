///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SMARTSFragmentation.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule.fragmentation;

import wsi.ra.tool.ResourceLoader;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;
import joelib.molecule.JOEMolVector;

import joelib.smarts.JOESmartsPattern;

import joelib.util.JHM;


/**
 * Fragmentation implementation based on SMARTS patterns for a molecule.
 *
 * @author     wegnerj
 */
public class SMARTSFragmentation
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.molecule.fragmentation.SMARTSFragmentation");

    //~ Instance fields ////////////////////////////////////////////////////////

    private ContiguousFragments contigFragmenter = new ContiguousFragments();
    private Vector description;
    private Vector smarts;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Fragments a molecule and single atoms occuring in the molecule are allowed.
     */
    public JOEMolVector getFragmentation(JOEMol mol)
    {
        return getFragmentation(mol, false, null);
    }

    /**
     * Fragments the molecule using SMARTS rules.
     *
     * @param molOriginal
     * @param skipSingleAtoms
     * @return JOEMolVector
     */
    public JOEMolVector getFragmentation(JOEMol molOriginal,
        boolean skipSingleAtoms, Vector origAtomIdx)
    {
        if (smarts == null)
        {
            logger.error("No SMARTS pattern defined.");

            return null;
        }

        if ((molOriginal == null) || molOriginal.empty())
        {
            logger.warn(
                "Molecule not defined or empty. It can not be fragmented.");

            return null;
        }

        JOEMol mol = (JOEMol) molOriginal.clone();

        JOESmartsPattern pSMARTS;
        String _description;
        Vector matchList;
        int[] iaTmp;

        for (int i = 0; i < smarts.size(); i++)
        {
            pSMARTS = (JOESmartsPattern) smarts.get(i);
            _description = (String) description.get(i);

            if ((mol == null) || mol.empty())
            {
                logger.error("No molecular structure available for " +
                    mol.getTitle());

                continue;
            }
            else
            {
                // find substructures
                pSMARTS.match(mol);
                matchList = pSMARTS.getMapList();

                for (int nn = 0; nn < matchList.size(); nn++)
                {
                    iaTmp = (int[]) matchList.get(nn);

                    // that's the easiest way to generate fragments
                    // rings will be opened !;-)
                    mol.deleteBond(mol.getBond(iaTmp[0], iaTmp[1]));
                }
            }
        }

        // skip non-connected atoms will be applied in the
        // contigous fragmenter
        return contigFragmenter.getFragmentation(mol, skipSingleAtoms,
            origAtomIdx);
    }

    public boolean setPattySMARTS(String pattySMARTSfile)
    {
        Vector pattyLines = ResourceLoader.readLines(pattySMARTSfile);

        return setPattySMARTS(pattyLines);
    }

    public boolean setPattySMARTS(Vector pattySMARTS)
    {
        boolean successfull = true;
        smarts = new Vector(pattySMARTS.size());
        description = new Vector(pattySMARTS.size());

        String line;
        String smartsPattern;
        Vector lineV = new Vector();

        for (int i = 0; i < pattySMARTS.size(); i++)
        {
            line = (String) pattySMARTS.get(i);

            //System.out.println("------------------------------");
            JHM.tokenize(lineV, line, " \t\r\n");

            // parse, initialize and generate SMARTS pattern
            // to allow fast pattern matching
            smartsPattern = (String) lineV.get(0);

            JOESmartsPattern parsedSmarts = new JOESmartsPattern();

            if (!parsedSmarts.init(smartsPattern))
            {
                logger.error("Invalid SMARTS pattern: " + smartsPattern);
                successfull = false;

                continue;
            }

            if (parsedSmarts.numAtoms() < 2)
            {
                logger.error("SMARTS pattern " + smartsPattern +
                    " must have at least two atoms to allow fragmentation.");
                successfull = false;

                continue;
            }

            // store smarts pattern and description
            smarts.add(parsedSmarts);

            if (lineV.size() < 2)
            {
                logger.warn("No description for SMARTS pattern: " +
                    smartsPattern);
                description.add("");
            }
            else
            {
                description.add(lineV.get(1));
            }
        }

        return successfull;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
