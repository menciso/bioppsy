///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ContiguousFragments.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule.fragmentation;

import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;
import joelib.molecule.JOEMolVector;

import joelib.util.iterator.BondIterator;


/**
 * Fragmentation implementation for contiguous fragments in a molecule.
 *
 * @author     wegnerj
 */
public class ContiguousFragments
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.molecule.fragmentation.ContiguousFragments");

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Fragments a molecule and single atoms occuring in the molecule are allowed.
     */
    public JOEMolVector getFragmentation(JOEMol mol)
    {
        return getFragmentation(mol, false, null);
    }

    /**
     * Returns all contiguous fragments of this molecule as single {@link joelib.molecule.JOEMol} objects.
     *
     * @param mol
     * @param skipSingleAtoms
     * @return JOEMolVector
     */
    public JOEMolVector getFragmentation(JOEMol mol, boolean skipSingleAtoms,
        Vector origAtomIdx)
    {
        if ((mol == null) || mol.empty())
        {
            logger.warn(
                "Molecule not defined or empty. It can not be fragmented.");

            return null;
        }

        Vector fragmentsIdx = new Vector();
        mol.contiguousFragments(fragmentsIdx);

        JOEMolVector fragments = new JOEMolVector();

        int[] fragmentAtomIdx;

        //System.out.println("" + fragmentsIdx.size() + " contiguous fragments in molecule '" + mol.getTitle() );
        JOEMol newMol;
        int[] mappedAtomIdx = new int[mol.numAtoms() + 1];
        Hashtable bonds = new Hashtable(mol.numBonds());
        JOEBond bond = null;
        JOEAtom atom;
        BondIterator bit;

        for (int i = 0; i < fragmentsIdx.size(); i++)
        {
            fragmentAtomIdx = (int[]) fragmentsIdx.get(i);

            if (origAtomIdx != null)
            {
                origAtomIdx.add(fragmentAtomIdx);
            }

            //System.out.println ("fragment " + i + " has "+fragmentAtomIdx.length+" atoms");
            // create new molecule
            newMol = new JOEMol(mol.getInputType(), mol.getOutputType());

            //System.out.println("has virtual bond "+newMol.hasData(JOEDataType.JOE_VIRTUAL_BOND_DATA));
            newMol.beginModify();

            if (fragmentAtomIdx.length == 1)
            {
                // skip non-connected atoms
                if (skipSingleAtoms)
                {
                    continue;
                }

                //System.out.println("atom: "+fragmentAtomIdx[0]);
                newMol.reserveAtoms(1);
                newMol.addAtom(mol.getAtom(fragmentAtomIdx[0]));
            }
            else
            {
                // store mapping positions
                for (int j = 0; j < fragmentAtomIdx.length; j++)
                {
                    //System.out.println("map "+fragmentAtomIdx[j]+" to "+(j+1));
                    mappedAtomIdx[fragmentAtomIdx[j]] = j + 1;
                }

                // create atoms and bonds for new molecule
                newMol.reserveAtoms(fragmentAtomIdx.length);

                for (int j = 0; j < fragmentAtomIdx.length; j++)
                {
                    //System.out.println("atom: "+fragmentAtomIdx[j]);
                    atom = mol.getAtom(fragmentAtomIdx[j]);

                    //System.out.println("add atom: "+fragmentAtomIdx[j]);
                    newMol.addAtom(atom);

                    bit = atom.bondIterator();

                    //System.out.println("Has bonds: "+bit.hasNext());
                    while (bit.hasNext())
                    {
                        bond = bit.nextBond();

                        // ensure to store bonds only once
                        if (!bonds.containsKey(bond))
                        {
                            //System.out.println("old bond: "+bond.getBeginAtomIdx()+bond.toString()+bond.getEndAtomIdx());
                            //System.out.println("bond: "+mappedAtomIdx[bond.getBeginAtomIdx()]+bond.toString()+mappedAtomIdx[bond.getEndAtomIdx()]);
                            bonds.put(bond, "");
                            newMol.addBond(mappedAtomIdx[bond.getBeginAtomIdx()],
                                mappedAtomIdx[bond.getEndAtomIdx()],
                                bond.getBO(), bond.getFlags());
                        }
                    }
                }
            }

            newMol.endModify();

            // add new molecule
            fragments.addMol(newMol);
        }

        //System.out.println("Finished contigous fragmentation");
        return fragments;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
