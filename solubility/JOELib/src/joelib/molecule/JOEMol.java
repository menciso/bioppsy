///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEMol.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.80 $
//            $Date: 2004/07/25 20:43:23 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEElementTable;
import joelib.data.JOEGenericData;
import joelib.data.JOEPairData;
import joelib.data.JOEPhModel;
import joelib.data.JOEVirtualBond;

import joelib.desc.DescResult;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.types.GraphPotentials;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;
import joelib.io.PropertyWriter;

import joelib.math.Matrix3x3;
import joelib.math.XYZVector;

import joelib.ring.JOERing;
import joelib.ring.JOERingData;
import joelib.ring.JOERingSearch;

import joelib.smiles.JOEMol2Smi;

import joelib.sort.ArraySizeComparator;
import joelib.sort.QuickInsertSort;

import joelib.util.JHM;
import joelib.util.JOEBitVec;
import joelib.util.JOEHelper;
import joelib.util.database.AbstractDatabase;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;
import joelib.util.iterator.ConformerIterator;
import joelib.util.iterator.GenericDataIterator;
import joelib.util.iterator.NativeValueIterator;
import joelib.util.iterator.NbrAtomIterator;
import joelib.util.iterator.ResidueIterator;
import joelib.util.iterator.RingIterator;
import joelib.util.types.AtomHCount;
import joelib.util.types.AtomZPos;


/**
 * Molecule representation.
 *
 * <p>
 * There are different possibilities to build a molecule.
 * <ol>
 * <li>Using SMILES notation to build a molecule.<br>
 * <blockquote><pre>
 * JOEMol mol=new JOEMol();
 * String smiles="c1cc(OH)cc1";
 * if (!JOESmilesParser.smiToMol(mol, smiles, setTitle.toString()))
 * {
 *   System.err.println("SMILES entry \"" + smiles + "\" could not be loaded.");
 * }
 * System.out.println(mol.toString());
 * </pre></blockquote>
 * </li>
 *
 * <li>Using plain atoms and bonds to build a molecule.<br>
 * <blockquote><pre>
 * JOEMol mol = new JOEMol();
 * // start molecule modification
 * mol.beginModify();
 * mol.reserveAtoms(2);
 *
 * // build carbon atom
 * JOEAtom C = new JOEAtom();
 * atomC.setAtomicNum(6);
 *
 * // build molecule
 * for (int i=0; i&lt;2; i++) {
 *   mol.addAtom(atom);
 * }
 *
 * // add double bond
 * mol.addBond(1,2, 2);
 *
 * //end molecule modification and store all
 * //coordinates in coordinate array
 * mol.endModify();
 * </pre></blockquote>
 * </li>
 * 2D coordinates can be generated using {@link joelib.util.cdk.CDKTools} which uses
 * the structure layout module from the <a href="http://cdk.sourceforge.net/">CDK</a>.<br>
 * 3D coordinates can be
 * generated using an external processing module ({@link joelib.ext.External}), if available,
 * for 3D generation programs, like
 * <a href="http://www2.chemie.uni-erlangen.de/software/corina/" target="_top">Corina</a>.
 *
 * <p>
 * For speed optimization of loading descriptor molecule files have a
 * look at the {@link joelib.desc.ResultFactory}.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.80 $, $Date: 2004/07/25 20:43:23 $
 * @cite clr98complexity
 * @cite zup89c
 * @cite fig96
 * @cite gm78
 * @cite smilesFormat
 * @see joelib.smiles.JOESmilesParser
 * @see joelib.util.cdk.CDKTools
 * @see joelib.ext.External
 * @see joelib.ext.Title2Data
 */
public class JOEMol implements Cloneable, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.molecule.JOEMol");

    /**
     *  Molecule flag: SSSR calculated.
     */
    public final static int JOE_SSSR_MOL = (1 << 1);

    /**
     *  Molecule flag: rings perceived.
     */
    public final static int JOE_RINGFLAGS_MOL = (1 << 2);

    /**
     *  Molecule flag: aromaticity calculated.
     */
    public final static int JOE_AROMATIC_MOL = (1 << 3);

    /**
     *  Molecule flag: atom types perceived.
     */
    public final static int JOE_ATOMTYPES_MOL = (1 << 4);

    /**
     *  Molecule flag: atom chirality flags perceived.
     */
    public final static int JOE_CHIRALITY_MOL = (1 << 5);

    /**
     *  Molecule flag: partitial charges perceived.
     */
    public final static int JOE_PCHARGE_MOL = (1 << 6);

    /**
     *  Molecule flag: atom hybridisation perceived.
     */
    public final static int JOE_HYBRID_MOL = (1 << 8);

    /**
     *  Molecule flag: implicit valences perceived.
     */
    public final static int JOE_IMPVAL_MOL = (1 << 9);

    /**
     *  Molecule flag: kekulization perceived.
     */
    public final static int JOE_KEKULE_MOL = (1 << 10);

    /**
     *  Molecule flag: closure bonds perceived.
     */
    public final static int JOE_CLOSURE_MOL = (1 << 11);

    /**
     *  Molecule flag: H atoms added molecule.
     */
    public final static int JOE_H_ADDED_MOL = (1 << 12);

    /**
     *  Molecule flag: PH value perceived.
     */
    public final static int JOE_PH_CORRECTED_MOL = (1 << 13);

    /**
     *  Molecule flag: aromaticity corrected molecule.
     */
    public final static int JOE_AROM_CORRECTED_MOL = (1 << 14);

    /**
     *  Molecule flag: chain molecule.
     */
    public final static int JOE_CHAINS_MOL = (1 << 15);

    /**
     *  Molecule flag: current conformer.
     */
    public final static int JOE_CURRENT_CONFORMER = (1 << 16);

    //-1 not necessary because int in Java 32 bit value;

    /*-------------------------------------------------------------------------*
     * private static member variables
     *------------------------------------------------------------------------- */
    private final static int JOE_ATOM_INCREMENT = 100;

    //	private final static int JOE_BOND_INCREMENT = 100;
    private final static IOType DEFAULT_IO_TYPE = IOTypeHolder.instance()
                                                              .getIOType("SDF");

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Holds additional molecule data, e.g. atom, bond or moelcule properties.
     */
    protected GenericDataHolder genericData;

    /**
     * Molecule compressed.
     */

    //	protected boolean _compressed;

    /**
     *  Molecule input type.
     */
    protected IOType _itype;

    /**
     *  Molecule output type.
     */
    protected IOType _otype;

    /**
     *  Energy of this molecule.
     */
    protected String _partialChargeVendor;

    /**
     *  Title for this molecule.
     */
    protected String _title;

    /**
     *  Atoms of this molecule.
     *
     * @see    joelib.molecule.JOEAtom
     */
    protected Vector _atom;

    /**
     *  Bonds of this molecule.
     *
     * @see    joelib.molecule.JOEBond
     */
    protected Vector _bond;

    /**
     *  Description of the Field
     */
    protected Vector _pose;

    /**
     * Residues for this molecule.
     */
    protected Vector _residue;

    /**
     *  Of type double[].
     */
    protected Vector _vconf;

    /**
     *  Coordinate array of the atom positions.
     */
    protected double[] _c;

    /**
     *  Description of the Field
     */
    protected double[] _xyz_pose;

    /**
     * Automatic formal charge calculation.
     */
    protected boolean _autoFormalCharge;

    /**
     * Automatic partial charge calculation.
     */
    protected boolean _autoPartialCharge;

    /**
     *  Energy of this molecule.
     */
    protected double _energy;

    /**
     *  Description of the Field
     */
    protected int _access;

    /**
     *  Description of the Field
     */
    protected int _cur_pose_idx;

    /*-------------------------------------------------------------------------*
     * protected member variables
     *------------------------------------------------------------------------- */

    /**
     *  Molecule flags.
     *
     * @see    joelib.molecule.JOEMol#JOE_SSSR_MOL
     * @see    joelib.molecule.JOEMol#JOE_RINGFLAGS_MOL
     * @see    joelib.molecule.JOEMol#JOE_AROMATIC_MOL
     * @see    joelib.molecule.JOEMol#JOE_ATOMTYPES_MOL
     * @see    joelib.molecule.JOEMol#JOE_CHIRALITY_MOL
     * @see    joelib.molecule.JOEMol#JOE_PCHARGE_MOL
     * @see    joelib.molecule.JOEMol#JOE_HYBRID_MOL
     * @see    joelib.molecule.JOEMol#JOE_IMPVAL_MOL
     * @see    joelib.molecule.JOEMol#JOE_KEKULE_MOL
     * @see    joelib.molecule.JOEMol#JOE_CLOSURE_MOL
     * @see    joelib.molecule.JOEMol#JOE_H_ADDED_MOL
     * @see    joelib.molecule.JOEMol#JOE_PH_CORRECTED_MOL
     * @see    joelib.molecule.JOEMol#JOE_AROM_CORRECTED_MOL
     * @see    joelib.molecule.JOEMol#JOE_CHAINS_MOL
     * @see    joelib.molecule.JOEMol#JOE_CURRENT_CONFORMER
     */
    protected int _flags;

    /**
     *  Modification counter.
     *
     * @see #getMod()
     */
    protected int _mod;

    /**
     *  Number of atoms in this molecule.
     */
    protected int _natoms;

    /**
     *  Number of bonds in this molecule.
     */
    protected int _nbonds;
    private int hash = 0;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the <tt>JOEMol</tt>. The 'Structured Data File'
     *  (SDF) molecule data format is used for default.
     *
     * @see #JOEMol(IOType, IOType)
     * @see #JOEMol(JOEMol)
     * @see #JOEMol(JOEMol, boolean)
     * @see #JOEMol(JOEMol, boolean, String[])
     * @cite mdlMolFormat
     */
    public JOEMol()
    {
        this(DEFAULT_IO_TYPE, DEFAULT_IO_TYPE);
    }

    /**
     *  Constructor for the <tt>JOEMol</tt>.
     *
     * @param  itype  input type for this molecule
     * @param  otype  output type for this molecule
     *
     * @see #JOEMol()
     * @see #JOEMol(JOEMol)
     * @see #JOEMol(JOEMol, boolean)
     * @see #JOEMol(JOEMol, boolean, String[])
     */
    public JOEMol(IOType itype, IOType otype)
    {
        _natoms = _nbonds = 0;
        _mod = 0;
        _access = 0;
        _energy = 0.0f;
        _itype = itype;
        _otype = otype;
        _atom = new Vector(20, JOE_ATOM_INCREMENT);
        _bond = new Vector(20);
        genericData = new GenericDataHolder(this, 5, 250);
        _title = "";
        _c = null;
        _flags = 0;
        _vconf = new Vector();
        _xyz_pose = null;
        _pose = new Vector();
        _residue = new Vector();
        _cur_pose_idx = 0;
        _autoPartialCharge = true;
        _autoFormalCharge = true;

        //		_compressed = false;
    }

    /**
     * Constructor for the <tt>JOEMol</tt>.
     *
     * @param  source  The source molecule
     * @param  cloneDesc  clones the PairData descriptors if <tt>true</tt>
     * @param  descriptors  the descriptors to clone. If <tt>null</tt> all descriptors are cloned
     *
     * @see #JOEMol()
     * @see #JOEMol(IOType, IOType)
     * @see #JOEMol(JOEMol)
     * @see #JOEMol(JOEMol, boolean)
     */
    public JOEMol(final JOEMol source, boolean cloneDesc, String[] descriptors)
    {
        _atom = new Vector(20, JOE_ATOM_INCREMENT);
        _bond = new Vector(20);
        _vconf = new Vector();
        _pose = new Vector();
        _residue = new Vector();
        genericData = new GenericDataHolder(this, 5, 250);
        set(source, cloneDesc, descriptors);
    }

    /**
     * Constructor for the <tt>JOEMol</tt>.
     *
     * @param  source  The source molecule
     * @param  cloneDesc  clones the PairData descriptors if <tt>true</tt>
     *
     * @see #JOEMol()
     * @see #JOEMol(IOType, IOType)
     * @see #JOEMol(JOEMol)
     * @see #JOEMol(JOEMol, boolean)
     * @see #JOEMol(JOEMol, boolean, String[])
     */
    public JOEMol(final JOEMol source, boolean cloneDesc)
    {
        this(source, cloneDesc, null);
    }

    /**
     * Clones the molecule without data elements.
     *
     * @param  source  The source molecule
     *
     * @see #JOEMol()
     * @see #JOEMol(IOType, IOType)
     * @see #JOEMol(JOEMol, boolean)
     * @see #JOEMol(JOEMol, boolean, String[])
     */
    public JOEMol(final JOEMol source)
    {
        this(source, false, null);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the add attribute of the <tt>JOEMol</tt> object
     *
     * @param  source  The new add value
     * @return         Description of the Return Value
     */
    public JOEMol setAdd(final JOEMol source)
    {
        JOEMol src = source;
        JOEAtom atom;
        JOEBond bond;
        AtomIterator ait = src.atomIterator();
        BondIterator bit = src.bondIterator();

        beginModify();

        int prevatms = numAtoms();

        _title += ("_" + src.getTitle());

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            addAtom(atom);
        }

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            addBond(bond);
            addBond(bond.getBeginAtomIdx() + prevatms,
                bond.getEndAtomIdx() + prevatms, bond.getBO());
        }

        endModify();

        return this;
    }

    /**
     *  Sets the aromaticCorrected attribute of the <tt>JOEMol</tt> object
     */
    public void setAromaticCorrected()
    {
        setFlag(JOE_AROM_CORRECTED_MOL);
    }

    /**
     *  Sets the aromaticPerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setAromaticPerceived()
    {
        setFlag(JOE_AROMATIC_MOL);
    }

    /**
     *  Gets an atom of the <tt>JOEMol</tt> object Atom index must be between <tt>
     *  1</tt> to <tt>numAtoms()</tt> .
     *
     * @param  idx  Description of the Parameter
     * @return      The atom value
     */
    public JOEAtom getAtom(int idx)
    {
        if ((idx < 1) || (idx > numAtoms()))
        {
            //oeAssert(false);
            //      throw new Exception("Requested Atom Out of Range");
            logger.error("Requested atom (" + idx + ") out of range (1-" +
                numAtoms() + ").");

            return null;
        }

        //        System.out.println("get: "+(idx - 1)+" "+_atom.get(idx - 1));
        return (JOEAtom) _atom.get(idx - 1);
    }

    /**
     *  Sets the atomTypesPerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setAtomTypesPerceived()
    {
        setFlag(JOE_ATOMTYPES_MOL);
    }

    /**
     *  Sets the flag if the automatic calculation of the formal charge of the
     *  atoms is allowed. This is for example used in the PH value correction
     *  method.
     *
     * @param  val  <tt>true</tt> if the calculation of the formal charge is
     *      allowed.
     */
    public void setAutomaticFormalCharge(boolean val)
    {
        _autoFormalCharge = val;
    }

    /**
     *  Sets the automaticPartialCharge attribute of the <tt>JOEMol</tt> object
     *
     * @param  val  The new automaticPartialCharge value
     */
    public void setAutomaticPartialCharge(boolean val)
    {
        _autoPartialCharge = val;
    }

    /**
     *  Gets a bond of the <tt>JOEMol</tt> object. Bond index must be between <tt>
     *  0</tt> to <tt>(numBonds()-1)</tt> .
     *
     * @param  idx  Description of the Parameter
     * @return      The bond value
     */
    public JOEBond getBond(int idx)
    {
        if ((idx < 0) || (idx >= numBonds()))
        {
            //        throw new Exception("Requested Bond Out of Range");
            logger.error("Requested bond (" + idx + ") out of range (0-" +
                (numBonds() - 1) + ").");

            return null;
        }

        return (JOEBond) _bond.get(idx);
    }

    /**
     *  Gets the bond attribute of the <tt>JOEMol</tt> object Atom index must be
     *  between <tt>1</tt> to <tt>numAtoms()</tt> .
     *
     * @param  bgn  atom index of the start atom
     * @param  end  atom index of the end atom
     * @return      The bond value
     */
    public JOEBond getBond(int bgn, int end)
    {
        return getBond(getAtom(bgn), getAtom(end));
    }

    /**
     *  Gets the bond attribute of the <tt>JOEMol</tt> object
     *
     * @param  bgn  Description of the Parameter
     * @param  end  Description of the Parameter
     * @return      The bond value
     */
    public JOEBond getBond(JOEAtom bgn, JOEAtom end)
    {
        JOEAtom nbr;
        NbrAtomIterator nait = bgn.nbrAtomIterator();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();

            if (nbr == end)
            {
                return nait.actualBond();
            }
        }

        //oeAssert(false); //should never get here
        return null;
    }

    /**
     *  Sets the chainsPerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setChainsPerceived()
    {
        setFlag(JOE_CHAINS_MOL);
    }

    /**
     *  Gets the chiral attribute of the JOEMol object
     *
     * @return    The chiral value
     */
    public boolean isChiral()
    {
        JOEAtom atom;
        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if ((atom.isCarbon() || atom.isNitrogen()) &&
                    (atom.getHvyValence() > 2) && atom.isChiral())
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Sets the chiralityPerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setChiralityPerceived()
    {
        setFlag(JOE_CHIRALITY_MOL);
    }

    /**
     *  Sets the closureBondsPerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setClosureBondsPerceived()
    {
        setFlag(JOE_CLOSURE_MOL);
    }

    /**
     *  Sets the conformer attribute of the JOEMol object
     *
     * @param  i  The new conformer value
     */
    public void setConformer(int i)
    {
        _c = (double[]) _vconf.get(i);
    }

    /**
     *  Gets the conformer attribute of the JOEMol object
     *
     * @param  i  Description of the Parameter
     * @return    The conformer value
     */
    public double[] getConformer(int i)
    {
        return (double[]) _vconf.get(i);
    }

    /**
     *  Sets the conformers attribute of the JOEMol object
     *
     * @param  v  The new conformers value (v is of type double[])
     */
    public void setConformers(Vector v)
    {
        //double[] fa;
        //		for (int i = 0; i < _vconf.size(); i++)
        //		{
        //			fa = (double[]) _vconf.get(i);
        //			fa = null;
        //		}
        _vconf.clear();

        //        _vconf = v;
        _vconf.ensureCapacity(v.size());

        for (int i = 0; i < v.size(); i++)
        {
            _vconf.add(v.get(i));
        }

        _c = (_vconf.size() == 0) ? null : (double[]) _vconf.get(0);
    }

    /**
     * Returns a {@link java.util.Vector} of all conformer coordinates (<tt>double[]</tt> values).
     *
     * @return    The conformers coordinates
     */
    public Vector getConformers()
    {
        return _vconf;
    }

    /**
     * Gets the coordinate array of this molecule.
     *
     * @return    The coordinates array
     */
    public double[] getCoordinates()
    {
        return (_c);
    }

    /**
     *  Sets the correctedForPH attribute of the <tt>JOEMol</tt> object
     */
    public void setCorrectedForPH()
    {
        setFlag(JOE_PH_CORRECTED_MOL);
    }

    /**
     *  Gets the correctedForPH attribute of the JOEMol object
     *
     * @return    The correctedForPH value
     */
    public boolean isCorrectedForPH()
    {
        return (hasFlag(JOE_PH_CORRECTED_MOL));
    }

    /**
     * Returns the first data entry of the given data type.
     * If this data type don't exists in this molecule, <tt>null</tt>
     * is returned.
     *
     * <p>
     * There exist a lot of default data types which where defined in
             * {@link JOEDataType}. These data types are used for caching ring
             * searches and storing special data types like comments or virtual bonds.
             * Furthermore there exist the most important data type {@link JOEPairData}
             * for storing descriptor values. Read the {@link JOEDataType} description
             * for details.
     *
     * @param  dt       data type
     * @return     <tt>null</tt> if no element of this data type exists
     * @see #getData(String)
     * @see #getData(String, boolean)
     * @see #genericDataIterator()
     * @see #addData(JOEGenericData)
     * @see #addData(JOEGenericData, boolean)
     */
    public JOEGenericData getData(JOEDataType dt)
    {
        return genericData.getData(dt);
    }

    /**
     * Returns the data entry with the given name, if multiple data entries
     * exists only the first one is taken.
     * If the data element is a {@link JOEPairData} element with a unparsed
     * String value, the descriptor value will be automatically parsed from
     * a String value to the representing result ({@link DescResult}) class.
     * If no data element with this name exists in this molecule, <tt>null</tt>
     * is returned.
     *
     * <p>
     * There exist a lot of default data types which where defined in
             * {@link JOEDataType}. These data types are used for caching ring
             * searches and storing special data types like comments or virtual bonds.
             * Furthermore there exist the most important data type {@link JOEPairData}
             * for storing descriptor values. Read the {@link JOEDataType} description
             * for details.
     *
     * <p>
     * Missing descriptor values can be calculated by using
     * {@link DescriptorHelper#descFromMol(JOEMol, String)}.<br>
     * Example:
     * <blockquote><pre>
     * DescResult result=null;
     * try
     * {
     *         result=DescriptorHelper.instance().descFromMol(mol, descriptorName);
     * }
     * catch (DescriptorException ex)
     * {
     *         // descriptor can not be calculated
     * }
     * </pre></blockquote>
     * Notice the difference between {@link JOEGenericData} and
     * {@link DescResult}. {@link DescResult} values can be added to
     * molecules by using {@link JOEPairData}
     * <blockquote><pre>
     * JOEPairData dp = new JOEPairData();
     * dp.setAttribute(descriptorName);
     * dp.setValue(result);
     * mol.addData(dp);
     * </pre></blockquote>
     *
     * @param  s      The data element (descriptor) name
     * @return     <tt>null</tt> if no element with this name exists
     * @see #getData(JOEDataType)
     * @see #getData(String, boolean)
     * @see #genericDataIterator()
     * @see #addData(JOEGenericData)
     * @see #addData(JOEGenericData, boolean)
     * @see JOEDataType
     * @see JOEPairData
     * @see DescResult
     * @see GenericDataHolder#getData(String, boolean)
     * @see DescriptorHelper#descFromMol(JOEMol, String)
     * @see DescriptorHelper#descFromMol(JOEMol, String, DescResult)
     * @see DescriptorHelper#descFromMol(JOEMol, String, DescResult, boolean)
     */
    public JOEGenericData getData(String s)
    {
        return genericData.getData(s, true);
    }

    /**
     * Returns the data entry with the given name, if multiple data entries
     * exists only the first one is taken.
     * If the data element is a {@link JOEPairData} the <tt>parse</tt> flag
     * can be used to parse this data elements.
     * If no data element with this name exists in this molecule, <tt>null</tt>
     * is returned.
     *
     * <p>
     * There exist a lot of default data types which where defined in
             * {@link JOEDataType}. These data types are used for caching ring
             * searches and storing special data types like comments or virtual bonds.
             * Furthermore there exist the most important data type {@link JOEPairData}
             * for storing descriptor values. Read the {@link JOEDataType} description
             * for details.
     *
     * <p>
     * Missing descriptor values can be calculated by using
     * {@link DescriptorHelper#descFromMol(JOEMol, String)}.<br>
     * Example:
     * <blockquote><pre>
     * DescResult result=null;
     * try
     * {
     *         result=DescriptorHelper.instance().descFromMol(mol, descriptorName);
     * }
     * catch (DescriptorException ex)
     * {
     *         // descriptor can not be calculated
     * }
     * </pre></blockquote>
     * Notice the difference between {@link JOEGenericData} and
     * {@link DescResult}. {@link DescResult} values can be added to
     * molecules by using {@link JOEPairData}
     * <blockquote><pre>
     * JOEPairData dp = new JOEPairData();
     * dp.setAttribute(descriptorName);
     * dp.setValue(result);
     * mol.addData(dp);
     * </pre></blockquote>
     *
     * @param  s       The data element (descriptor) name
     * @param  parse   Parse data element
     * @return     <tt>null</tt> if no element with this name exists
     * @see #getData(JOEDataType)
     * @see #getData(String)
     * @see #genericDataIterator()
     * @see #addData(JOEGenericData)
     * @see #addData(JOEGenericData, boolean)
     * @see JOEDataType
     * @see JOEPairData
     * @see DescResult
     * @see GenericDataHolder#getData(String, boolean)
     * @see DescriptorHelper#descFromMol(JOEMol, String)
     * @see DescriptorHelper#descFromMol(JOEMol, String, DescResult)
     * @see DescriptorHelper#descFromMol(JOEMol, String, DescResult, boolean)
     */
    public JOEGenericData getData(String s, boolean parse)
    {
        return genericData.getData(s, parse);
    }

    /**
     *  Sets the energy attribute of the <tt>JOEMol</tt> object
     *
     * @param  energy  The new energy value
     */
    public void setEnergy(double energy)
    {
        _energy = energy;
    }

    /**
     *  Gets the energy attribute of the <tt>JOEMol</tt> object
     *
     * @return    The energy value
     */
    public double getEnergy()
    {
        return (_energy);
    }

    /**
     * Gets mass given by isotopes (or most abundant isotope as necessary).
     *
     * @return    The molWt value
     */
    public double getExactMass()
    {
        double molwt = 0.0f;
        JOEAtom atom;
        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            molwt += atom.getExactMass();
        }

        return molwt;
    }

    /**
     *  Gets the firstAtom attribute of the <tt>JOEMol</tt> object
     *
     * @return    The firstAtom value
     */
    public JOEAtom getFirstAtom()
    {
        return ((_atom.size() == 0) ? null : (JOEAtom) _atom.get(0));
    }

    /**
     *  Gets the flags attribute of the <tt>JOEMol</tt> object
     *
     * @return    The flags value
     * @see       joelib.molecule.JOEMol#JOE_SSSR_MOL
     * @see       joelib.molecule.JOEMol#JOE_RINGFLAGS_MOL
     * @see       joelib.molecule.JOEMol#JOE_AROMATIC_MOL
     * @see       joelib.molecule.JOEMol#JOE_ATOMTYPES_MOL
     * @see       joelib.molecule.JOEMol#JOE_CHIRALITY_MOL
     * @see       joelib.molecule.JOEMol#JOE_PCHARGE_MOL
     * @see       joelib.molecule.JOEMol#JOE_HYBRID_MOL
     * @see       joelib.molecule.JOEMol#JOE_IMPVAL_MOL
     * @see       joelib.molecule.JOEMol#JOE_KEKULE_MOL
     * @see       joelib.molecule.JOEMol#JOE_CLOSURE_MOL
     * @see       joelib.molecule.JOEMol#JOE_H_ADDED_MOL
     * @see       joelib.molecule.JOEMol#JOE_PH_CORRECTED_MOL
     * @see       joelib.molecule.JOEMol#JOE_AROM_CORRECTED_MOL
     * @see       joelib.molecule.JOEMol#JOE_CHAINS_MOL
     * @see       joelib.molecule.JOEMol#JOE_CURRENT_CONFORMER
     */
    public int getFlags()
    {
        return (_flags);
    }

    //calculates the graph theoretical distance for every atom
    //and puts it into gtd

    /**
     *  Gets the gTDVector attribute of the <tt>JOEMol</tt> object
     *
     * @param  gtd  Description of the Parameter
     * @return      The gTDVector value
     */
    public boolean getGTDVector(int[] gtd)
    {
        //gtd.clear();
        //gtd.setSize(numAtoms());
        if (gtd.length != numAtoms())
        {
            logger.error("gtd must have length of #atoms: " + numAtoms());
        }

        int gtdcount;

        int natom;
        JOEBitVec used = new JOEBitVec();
        JOEBitVec curr = new JOEBitVec();
        JOEBitVec next = new JOEBitVec();
        JOEAtom atom;
        JOEAtom atom1;
        JOEBond bond;
        AtomIterator ait = this.atomIterator();

        next.clear();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            gtdcount = 0;
            used.clear();
            curr.clear();
            used.setBitOn(atom.getIdx());
            curr.setBitOn(atom.getIdx());

            while (!curr.isEmpty())
            {
                next.clear();

                //          for(natom=curr.nextSetBit(0); natom>=0; natom=curr.nextSetBit(natom+1))
                for (natom = curr.nextBit(-1); natom != curr.endBit();
                        natom = curr.nextBit(natom))
                {
                    atom1 = getAtom(natom);

                    BondIterator bit = atom1.bondIterator();

                    while (bit.hasNext())
                    {
                        bond = bit.nextBond();

                        if (!used.bitIsOn(bond.getNbrAtomIdx(atom1)) &&
                                !curr.bitIsOn(bond.getNbrAtomIdx(atom1)))
                        {
                            if (!(bond.getNbrAtom(atom1)).isHydrogen())
                            {
                                next.setBitOn(bond.getNbrAtomIdx(atom1));
                            }
                        }
                    }
                }

                used.set(next);
                curr.set(next);
                gtdcount++;
            }

            gtd[atom.getIdx() - 1] = gtdcount;
        }

        return true;
    }

    /**
     *  Sets the hybridizationPerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setHybridizationPerceived()
    {
        setFlag(JOE_HYBRID_MOL);
    }

    /**
     *  Sets the hydrogensAdded attribute of the <tt>JOEMol</tt> object
     */
    public void setHydrogensAdded()
    {
        setFlag(JOE_H_ADDED_MOL);
    }

    /**
     *  Sets the implicitValencePerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setImplicitValencePerceived()
    {
        setFlag(JOE_IMPVAL_MOL);
    }

    /**
     *  Sets the inputType attribute of the <tt>JOEMol</tt> object
     *
     * @param  type  The new inputType value
     */
    public void setInputType(IOType type)
    {
        _itype = type;
    }

    /**
     *  Sets the kekulePerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setKekulePerceived()
    {
        setFlag(JOE_KEKULE_MOL);
    }

    /**
     *  Gets the mod attribute of the <tt>JOEMol</tt> object
     *
     * @return    The mod value
     */
    public int getMod()
    {
        return (_mod);
    }

    /**
     *  Gets the title attribute of the <tt>JOEMol</tt> object.
     *
     * @return    The title value
     */
    public final String getTitle()
    {
        return _title;
    }

    /**
     *  Gets the inputType attribute of the <tt>JOEMol</tt> object
     *
     * @return    The inputType value
     */
    public IOType getInputType()
    {
        return (_itype);
    }

    /**
     * Gets standard molar mass given by IUPAC atomic masses.
     *
     * @return    The molWt value
     */
    public double getMolWt()
    {
        double molwt = 0.0f;
        JOEAtom atom;
        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            molwt += atom.getAtomicMass();
        }

        return molwt;
    }

    /**
     *  Sets the outputType attribute of the <tt>JOEMol</tt> object
     *
     * @param  type  The new outputType value
     */
    public void setOutputType(IOType type)
    {
        _otype = type;
    }

    /**
     *  Gets the outputType attribute of the <tt>JOEMol</tt> object
     *
     * @return    The outputType value
     */
    public IOType getOutputType()
    {
        return (_otype);
    }

    public void setPartialChargeVendor(String vendor)
    {
        _partialChargeVendor = vendor;
    }

    public String getPartialChargeVendor()
    {
        if (this.automaticPartialCharge())
        {
            return "joelib:partialCharge";
        }
        else
        {
            return _partialChargeVendor;
        }
    }

    /**
     *  Sets the partialChargesPerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setPartialChargesPerceived()
    {
        setFlag(JOE_PCHARGE_MOL);
    }

    /**
     *  Gets the residue attribute of the JOEMol object
     *
     * @param  idx  Description of the Parameter
     * @return      The residue value
     */
    public JOEResidue getResidue(int idx)
    {
        if ((idx < 0) || (idx >= numResidues()))
        {
            throw new IndexOutOfBoundsException("Residue with index " + idx +
                " does'nt exist.");

            //return null;
        }

        return ((JOEResidue) _residue.get(idx));
    }

    /**
     *  Sets the ringAtomsAndBondsPerceived attribute of the <tt>JOEMol</tt>
     *  object
     */
    public void setRingAtomsAndBondsPerceived()
    {
        setFlag(JOE_RINGFLAGS_MOL);
    }

    /**
     * Gets iterator for the Smallest Set of Smallest Rings (SSSR).
     *
     * <blockquote><pre>
     * RingIterator rit = mol.getRingIterator();
     * JOERing ring;
     * while(rit.hasNext())
     * {
     *   ring = rit.nextRing();
     *
     * }
     * </pre></blockquote>
     *
     * @return     The ring iterator
     * @see #getSSSR()
     * @see #findSSSR()
     * @see #atomIterator()
     * @see #bondIterator()
     * @see #conformerIterator()
     * @see #genericDataIterator
     * @see #nativeValueIterator()
     * @see RingIterator
     * @cite fig96
     */
    public RingIterator getRingIterator()
    {
        return new RingIterator(getSSSR());
    }

    /**
     * Gets the Smallest Set of Smallest Rings (SSSR).
     *
     * @return    {@link java.util.Vector} of <tt>JOERing</tt>
     *
     * @see #getRingIterator()
     * @see #findSSSR()
     * @cite fig96
     */
    public synchronized Vector getSSSR()
    {
        if (!hasSSSRPerceived())
        {
            findSSSR();
        }

        JOERingData rd = (JOERingData) getData(JOEDataType.JOE_RING_DATA);

        return (rd.getData());
    }

    /**
     *  Sets the sSSRPerceived attribute of the <tt>JOEMol</tt> object
     */
    public void setSSSRPerceived()
    {
        setFlag(JOE_SSSR_MOL);
    }

    /**
     *  Sets the title attribute of the <tt>JOEMol</tt> object
     *
     * @param  title  The new title value
     */
    public void setTitle(String title)
    {
        _title = title;
    }

    /**
     *  Sets the torsion attribute of the <tt>JOEMol</tt> object
     *
     * @param  a    The new torsion value
     * @param  b    The new torsion value
     * @param  c    The new torsion value
     * @param  d    The new torsion value
     * @param  ang  The new torsion value
     */
    public void setTorsion(JOEAtom a, JOEAtom b, JOEAtom c, JOEAtom d,
        double ang)
    {
        int[] tor = new int[4];
        Vector atoms = new Vector();

        // of type int[1]
        int[] itmp;

        tor[0] = a.getCIdx();
        tor[1] = b.getCIdx();
        tor[2] = c.getCIdx();
        tor[3] = d.getCIdx();

        findChildren(atoms, b.getIdx(), c.getIdx());

        int j;

        for (j = 0; j < atoms.size(); j++)
        {
            itmp = (int[]) atoms.get(j);
            itmp[0] = (itmp[0] - 1) * 3;
        }

        double v1x;

        double v1y;

        double v1z;

        double v2x;

        double v2y;

        double v2z;

        double v3x;

        double v3y;

        double v3z;
        double c1x;
        double c1y;
        double c1z;
        double c2x;
        double c2y;
        double c2z;
        double c3x;
        double c3y;
        double c3z;
        double c1mag;
        double c2mag;
        double radang;
        double costheta;
        double[] m = new double[9];
        double x;
        double y;
        double z;
        double mag;
        double rotang;
        double sn;
        double cs;
        double t;
        double tx;
        double ty;
        double tz;

        //calculate the torsion angle
        v1x = (double) (_c[tor[0]] - _c[tor[1]]);
        v2x = (double) (_c[tor[1]] - _c[tor[2]]);
        v1y = (double) (_c[tor[0] + 1] - _c[tor[1] + 1]);
        v2y = (double) (_c[tor[1] + 1] - _c[tor[2] + 1]);
        v1z = (double) (_c[tor[0] + 2] - _c[tor[1] + 2]);
        v2z = (double) (_c[tor[1] + 2] - _c[tor[2] + 2]);
        v3x = (double) (_c[tor[2]] - _c[tor[3]]);
        v3y = (double) (_c[tor[2] + 1] - _c[tor[3] + 1]);
        v3z = (double) (_c[tor[2] + 2] - _c[tor[3] + 2]);

        c1x = (v1y * v2z) - (v1z * v2y);
        c2x = (v2y * v3z) - (v2z * v3y);
        c1y = (-v1x * v2z) + (v1z * v2x);
        c2y = (-v2x * v3z) + (v2z * v3x);
        c1z = (v1x * v2y) - (v1y * v2x);
        c2z = (v2x * v3y) - (v2y * v3x);
        c3x = (c1y * c2z) - (c1z * c2y);
        c3y = (-c1x * c2z) + (c1z * c2x);
        c3z = (c1x * c2y) - (c1y * c2x);

        c1mag = (c1x * c1x) + (c1y * c1y) + (c1z * c1z);
        c2mag = (c2x * c2x) + (c2y * c2y) + (c2z * c2z);

        if ((c1mag * c2mag) < 0.01f)
        {
            costheta = 1.0f;
        }

        //avoid div by zero error
        else
        {
            costheta = ((c1x * c2x) + (c1y * c2y) + (c1z * c2z)) / (double) (Math.sqrt(c1mag * c2mag));
        }

        if (costheta < -0.999999f)
        {
            costheta = -0.999999f;
        }

        if (costheta > 0.999999f)
        {
            costheta = 0.999999f;
        }

        if (((v2x * c3x) + (v2y * c3y) + (v2z * c3z)) > 0.0f)
        {
            radang = (double) -Math.acos(costheta);
        }
        else
        {
            radang = (double) Math.acos(costheta);
        }

        //
        // now we have the torsion angle (radang) - set up the rot matrix
        //
        //find the difference between current and requested
        rotang = ang - radang;

        sn = (double) Math.sin(rotang);
        cs = (double) Math.cos(rotang);
        t = 1 - cs;

        //normalize the rotation vector
        mag = (double) Math.sqrt((v2x * v2x) + (v2y * v2y) + (v2z * v2z));
        x = v2x / mag;
        y = v2y / mag;
        z = v2z / mag;

        //set up the rotation matrix
        m[0] = (t * x * x) + cs;
        m[1] = (t * x * y) + (sn * z);
        m[2] = (t * x * z) - (sn * y);
        m[3] = (t * x * y) - (sn * z);
        m[4] = (t * y * y) + cs;
        m[5] = (t * y * z) + (sn * x);
        m[6] = (t * x * z) + (sn * y);
        m[7] = (t * y * z) - (sn * x);
        m[8] = (t * z * z) + cs;

        //
        //now the matrix is set - time to rotate the atoms
        //
        tx = _c[tor[1]];
        ty = _c[tor[1] + 1];
        tz = _c[tor[1] + 2];

        for (int i = 0; i < atoms.size(); i++)
        {
            j = ((int[]) atoms.get(i))[0];
            _c[j] -= tx;
            _c[j + 1] -= ty;
            _c[j + 2] -= tz;
            x = (_c[j] * m[0]) + (_c[j + 1] * m[1]) + (_c[j + 2] * m[2]);
            y = (_c[j] * m[3]) + (_c[j + 1] * m[4]) + (_c[j + 2] * m[5]);
            z = (_c[j] * m[6]) + (_c[j + 1] * m[7]) + (_c[j + 2] * m[8]);
            _c[j] = x;
            _c[j + 1] = y;
            _c[j + 2] = z;
            _c[j] += tx;
            _c[j + 1] += ty;
            _c[j + 2] += tz;
        }
    }

    /**
     *  Gets the torsion attribute of the <tt>JOEMol</tt> object
     *
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     * @param  c  Description of the Parameter
     * @param  d  Description of the Parameter
     * @return    The torsion value
     */
    public double getTorsion(int a, int b, int c, int d)
    {
        return (XYZVector.calcTorsionAngle(((JOEAtom) _atom.get(a - 1)).getVector(),
            ((JOEAtom) _atom.get(b - 1)).getVector(),
            ((JOEAtom) _atom.get(c - 1)).getVector(),
            ((JOEAtom) _atom.get(d - 1)).getVector()));
    }

    /**
     *  Gets the torsion attribute of the <tt>JOEMol</tt> object
     *
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     * @param  c  Description of the Parameter
     * @param  d  Description of the Parameter
     * @return    The torsion value
     */
    public double getTorsion(JOEAtom a, JOEAtom b, JOEAtom c, JOEAtom d)
    {
        return (XYZVector.calcTorsionAngle(a.getVector(), b.getVector(),
            c.getVector(), d.getVector()));
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     * Adds (cloned) atom to the <tt>JOEMol</tt>.
     * Also checks for any bonds that should be made to the new atom.
     * Atoms starts with the index 1, which is a difference to bonds
     * which starts with the bond index 0.
     *
     * <p>
     * It's recommended calling the <tt>beginModify()</tt> method before the
     * first atom will be added and calling the <tt>endModify()</tt> method
     * after the last atom has been added. This causes, that the coordinates
     * of the atoms will also be stored in a coordinate array in the molecule.
     *
     * @param  atom  The <tt>JOEAtom</tt> to add, which will be deep cloned.
     * @return       <tt>true</tt> if successfull
     *
     * @see #addBond(int, int, int)
     * @see #addBond(int, int, int, int)
     * @see #addBond(int, int, int, int, int)
     * @see #addBond(JOEBond)
     * @see #beginModify()
     * @see #endModify()
     * @see #endModify(boolean)
     */
    public synchronized boolean addAtom(JOEAtom atom)
    {
        beginModify();

        JOEAtom joeatom;

        // = createAtom();
        joeatom = (JOEAtom) atom.clone();
        joeatom.setIdx(_natoms + 1);

        //System.out.println("add idx:"+joeatom.getIdx());
        //System.out.println("add cidx:"+joeatom.getCIdx());
        joeatom.setParent(this);

        //    if (_atom.size()==0 || _natoms+1 >= _atom.size())
        //    {
        //      // resize atom vector and fill new elements (automatically) with null
        //      _atom.setSize(_natoms+JOE_ATOM_INCREMENT);
        //      //for (int j =(_natoms+1); j<_atom.size();j++) _atom.set(j, null);
        //    }
        //    _atom.set(_natoms, oeatom);
        _atom.add(joeatom);

        if (logger.isDebugEnabled())
        {
            logger.debug("Add atom " + _atom.get(_natoms) + " with index " +
                joeatom.getIdx());
        }

        _natoms++;

        if (hasData(JOEDataType.JOE_VIRTUAL_BOND_DATA))
        {
            /*add bonds that have been queued        */
            JOEVirtualBond vb;

            GenericDataIterator gdit = genericDataIterator();
            JOEGenericData genericData;
            Vector verase = new Vector();

            //JOEGenericData
            while (gdit.hasNext())
            {
                genericData = gdit.nextGenericData();

                if (genericData.getDataType().equals(JOEDataType.JOE_VIRTUAL_BOND_DATA))
                {
                    vb = (JOEVirtualBond) genericData;

                    if ((vb.getBgn() > _natoms) || (vb.getEnd() > _natoms))
                    {
                        continue;
                    }

                    if ((joeatom.getIdx() == vb.getBgn()) ||
                            (joeatom.getIdx() == vb.getEnd()))
                    {
                        addBond(vb.getBgn(), vb.getEnd(), vb.getOrder(),
                            vb.getStereo());
                        verase.add(genericData);
                    }
                }
            }

            if (verase.size() != 0)
            {
                deleteData(verase);
            }
        }

        //System.out.println("end modify");
        endModify();

        return true;
    }

    /**
     * Adds a bond to the <tt>JOEMol</tt>.
     * Bonds starts with the index 0, which is a difference to atoms
     * which starts with the atom index 1.
     *
     * @param  first      The start atom index (atoms begins with index 1)
     * @param  second     The end atom index (atoms begins with index 1)
     * @param  order      The bond order
     * @return         <tt>true</tt> if successfull
     *
     * @see #addAtom(JOEAtom)
     * @see #addBond(int, int, int, int)
     * @see #addBond(int, int, int, int, int)
     * @see #addBond(JOEBond)
     * @see #connectTheDots()
     */
    public boolean addBond(int first, int second, int order)
    {
        return addBond(first, second, order, 0, -1);
    }

    /**
     * Adds a bond to the <tt>JOEMol</tt>.
     * Bonds starts with the index 0, which is a difference to atoms
     * which starts with the atom index 1.
     *
     * @param  first      The start atom index (atoms begins with index 1)
     * @param  second     The end atom index (atoms begins with index 1)
     * @param  order      The bond order
     * @param  flags     The stereo flag
     * @return            <tt>true</tt> if successfull
     *
     * @see #addBond(int, int, int)
     * @see #addBond(int, int, int, int, int)
     * @see #addBond(JOEBond)
     * @see #connectTheDots()
     */
    public boolean addBond(int first, int second, int order, int flags)
    {
        return addBond(first, second, order, flags, -1);
    }

    /**
     * Adds a bond to the <tt>JOEMol</tt>.
     * Bonds starts with the index 0, which is a difference to atoms
     * which starts with the atom index 1.
     *
     * @param  first      The start atom index (atoms begins with index 1)
     * @param  second     The end atom index (atoms begins with index 1)
     * @param  order      The bond order
     * @param  stereo     The stereo flag
     * @param  insertpos  The position at which the bond should be inserted
     * @return            <tt>true</tt> if successfull
     *
     * @see #addAtom(JOEAtom)
     * @see #addBond(int, int, int)
     * @see #addBond(int, int, int, int)
     * @see #addBond(JOEBond)
     * @see #connectTheDots()
     */
    public synchronized boolean addBond(int first, int second, int order,
        int stereo, int insertpos)
    {
        beginModify();

        if (logger.isDebugEnabled())
        {
            logger.debug("addBond:" + first + " " + second + " " + order + " " +
                stereo + " " + insertpos);
        }

        if (first == second)
        {
            logger.error("'Loop-Bond' is not allowed for atom " + first);

            return false;
        }

        if ((first <= numAtoms()) && (second <= numAtoms()))
        {
            //atoms exist
            JOEBond bond = createBond();

            if (bond == null)
            {
                endModify();

                return false;
            }

            JOEAtom bgn;

            JOEAtom end;
            bgn = getAtom(first);
            end = getAtom(second);

            if ((bgn == null) || (end == null))
            {
                //        throw new Exception("Unable to add bond - invalid atom index");
                logger.error("Unable to add bond - invalid atom index");

                return false;
            }

            bond.set(_nbonds, bgn, end, order, stereo);
            bond.setParent(this);

            //set aromatic flags if it has the appropriate order
            if (order == JOEBond.JOE_AROMATIC_BOND_ORDER)
            {
                bond.setAromatic();
                bgn.setAromatic();
                end.setAromatic();
            }

            _bond.add(bond);
            _nbonds++;

            if (insertpos == -1)
            {
                //                System.out.println("addBond2Atom: "+bgn.getIdx()+" "+end.getIdx());
                bgn.addBond(bond);
                end.addBond(bond);
            }
            else
            {
                if (insertpos >= bgn.getValence())
                {
                    bgn.addBond(bond);

                    //                    System.out.println(""+bgn.getIdx());
                }
                else
                {
                    //need to insert the bond for the connectivity order to be preserved
                    //otherwise stereochemistry gets screwed up
                    NbrAtomIterator nait = bgn.nbrAtomIterator();
                    nait.setActualIndex(insertpos);

                    //                    int counter = 0;
                    //                    while (counter != insertpos)
                    //                    {
                    //                        counter++;
                    //                        nait.nextNbrAtom();
                    //                    }
                    //                    System.out.println("AtomBonds:"+bgn.getValence());
                    bgn.insertBond(nait, bond);

                    //                    System.out.println("insert("+insertpos+") "+bgn.getIdx());
                    //                    System.out.println("AtomBonds:"+bgn.getValence());
                }

                end.addBond(bond);

                //                System.out.println(" "+end.getIdx());
            }
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Add virtual bond");
            }

            //at least one atom doesn't exist yet - add to bond_q
            addData(new JOEVirtualBond(first, second, order, stereo));
        }

        endModify();

        //  BondIterator bit = this.bondIterator();
        //  JOEBond bond;
        //  StringBuffer sb=new StringBuffer(1000);
        //  while(bit.hasNext())
        //  {
        //    bond = bit.nextBond();
        //    sb.append("BOND:");
        //    sb.append(bond.getIdx());
        //    sb.append(' ');
        //    sb.append(bond.getBeginAtomIdx());
        //    sb.append(' ');
        //    sb.append(bond.getEndAtomIdx());
        //  }
        //  System.out.println(sb.toString());
        return true;
    }

    /**
     * Adds (cloned) bond to the <tt>JOEMol</tt>.
     * Bonds starts with the index 0, which is a difference to atoms
     * which starts with the atom index 1.
     *
     * @param  bond  The <tt>JOEBond</tt> to add, which will be deep cloned.
     * @return       Description of the Return Value
     *
     * @see #addAtom(JOEAtom)
     * @see #addBond(int, int, int)
     * @see #addBond(int, int, int, int)
     * @see #addBond(int, int, int, int, int)
     * @see #connectTheDots()
     */
    public synchronized boolean addBond(JOEBond bond)
    {
        return (addBond(bond.getBeginAtomIdx(), bond.getEndAtomIdx(),
            bond.getBO(), bond.getFlags()));
    }

    /**
     * Adds conformer coordinates to this molecule.
     *
     * @param  f  The conformer coordinates
     */
    public synchronized void addConformer(double[] f)
    {
        _vconf.add(f);
    }

    /**
     *  Adds a <tt>JOEGenericData</tt> object to this molecule but don't overwrite
     *  existing data elements with the same name, if they exists already.
     *
     * <p>
     * There exist a lot of default data types which where defined in
             * {@link JOEDataType}. These data types are used for caching ring
             * searches and storing special data types like comments or virtual bonds.
             * Furthermore there exist the most important data type {@link JOEPairData}
             * for storing descriptor values. Read the {@link JOEDataType} description
             * for details.
     *
     * <p>
     * Missing descriptor values can be calculated by using
     * {@link DescriptorHelper#descFromMol(JOEMol, String)}.<br>
     * Example:
     * <blockquote><pre>
     * DescResult result=null;
     * try
     * {
     *         result=DescriptorHelper.instance().descFromMol(mol, descriptorName);
     * }
     * catch (DescriptorException ex)
     * {
     *         // descriptor can not be calculated
     * }
     * </pre></blockquote>
     * Notice the difference between {@link JOEGenericData} and
     * {@link DescResult}. {@link DescResult} values can be added to
     * molecules by using {@link JOEPairData}
     * <blockquote><pre>
     * JOEPairData dp = new JOEPairData();
     * dp.setAttribute(descriptorName);
     * dp.setValue(result);
     * mol.addData(dp);
     * </pre></blockquote>
     *
     * @param  d  The new data value
     * @see #addData(JOEGenericData, boolean)
     * @see #getData(JOEDataType)
     * @see #getData(String)
     * @see #getData(String, boolean)
     * @see #genericDataIterator()
     * @see JOEDataType
     * @see JOEPairData
     * @see DescriptorHelper#descFromMol(JOEMol, String)
     * @see DescriptorHelper#descFromMol(JOEMol, String, DescResult)
     * @see DescriptorHelper#descFromMol(JOEMol, String, DescResult, boolean)
     */
    public synchronized void addData(JOEGenericData d)
    {
        if (genericData.hasData(d.getAttribute()))
        {
            logger.warn("" + d.getAttribute() +
                " exists already. New data entry was not stored.");
        }

        genericData.addData(d, false);
    }

    /**
     *  Adds a <tt>JOEGenericData</tt> object to this molecule.
     *
     * <p>
     * There exist a lot of default data types which where defined in
             * {@link JOEDataType}. These data types are used for caching ring
             * searches and storing special data types like comments or virtual bonds.
             * Furthermore there exist the most important data type {@link JOEPairData}
             * for storing descriptor values. Read the {@link JOEDataType} description
             * for details.
     *
     * <p>
     * Missing descriptor values can be calculated by using
     * {@link DescriptorHelper#descFromMol(JOEMol, String)}.<br>
     * Example:
     * <blockquote><pre>
     * DescResult result=null;
     * try
     * {
     *         result=DescriptorHelper.instance().descFromMol(mol, descriptorName);
     * }
     * catch (DescriptorException ex)
     * {
     *         // descriptor can not be calculated
     * }
     * </pre></blockquote>
     * Notice the difference between {@link JOEGenericData} and
     * {@link DescResult}. {@link DescResult} values can be added to
     * molecules by using {@link JOEPairData}
     * <blockquote><pre>
     * JOEPairData dp = new JOEPairData();
     * dp.setAttribute(descriptorName);
     * dp.setValue(result);
     * mol.addData(dp);
     * </pre></blockquote>
     *
     * @param  d  The new data value
     * @param  overwrite  Overwrite already existing data element, if <tt>true</tt>
     * @see #addData(JOEGenericData)
     * @see #getData(JOEDataType)
     * @see #getData(String)
     * @see #getData(String, boolean)
     * @see #genericDataIterator()
     * @see JOEDataType
     * @see JOEPairData
     * @see DescriptorHelper#descFromMol(JOEMol, String)
     * @see DescriptorHelper#descFromMol(JOEMol, String, DescResult)
     * @see DescriptorHelper#descFromMol(JOEMol, String, DescResult, boolean)
     */
    public synchronized void addData(JOEGenericData d, boolean overwrite)
    {
        genericData.addData(d, overwrite);
    }

    /**
     * Adds hydrogens atoms to this molecule.
     * The pH value will be corrected.
     *
     * @return    <tt>true</tt> if successfull
     * @see JOEPhModel
     */
    public synchronized boolean addHydrogens()
    {
        return addHydrogens(false, true);
    }

    /**
     * Adds hydrogens atoms to this molecule.
     * All hydrogens in neighbourhood to N,O,P or S are treated as polar.
     * The pH value will be corrected.
     *
     * @param  polaronly  Add only polar hydrogens, if <tt>true</tt>
     * @return            <tt>true</tt> if successfull
     * @see JOEPhModel
     */
    public synchronized boolean addHydrogens(boolean polaronly)
    {
        return addHydrogens(polaronly, true);
    }

    /**
     * Adds hydrogens atoms to this molecule.
     * All hydrogens in neighbourhood to N,O,P or S are treated as polar.
     * The pH value correction mode can be choosen.
     * Dont't use (experimental) coordinate vector for added hydrogens.
     *
     * @param  polaronly     Add only polar hydrogens, if <tt>true</tt>
     * @param  correctForPH  Corrects molecule for pH if <tt>true</tt>
     * @return               <tt>true</tt> if successfull
     * @see JOEPhModel
     */
    public synchronized boolean addHydrogens(boolean polaronly,
        boolean correctForPH)
    {
        return addHydrogens(polaronly, correctForPH, true);
    }

    /**
     * Adds hydrogens atoms to this molecule.
     * All hydrogens in neighbourhood to N,O,P or S are treated as polar.
     * The pH value correction mode can be choosen.
     * The (experimental) coordinate vector for added hydrogens mode can be choosen.
     *
     * <p>
     * If useCoordV is <tt>true</tt> the coordinate vector for added H atoms will be used.
     * This will be slower, because <tt>endModify()</tt> will be called after
     * every added hydrogen atom. If useCoordV is <tt>false</tt> the coordinate vector
     * for added H atoms will be ignored, which will be faster.

     *
     * @param  polaronly     Add only polar hydrogens, if <tt>true</tt>
     * @param  correctForPH  Corrects molecule for pH if <tt>true</tt>
     * @param  useCoordV     The coordinate vector for added hydrogens will be used if <tt>true</tt>
     * @return               <tt>true</tt> if successfull
     * @see JOEPhModel
     * @see #endModify()
     * @see #endModify(boolean)
     */
    public synchronized boolean addHydrogens(boolean polaronly,
        boolean correctForPH, boolean useCoordV)
    {
        if (!isCorrectedForPH() && correctForPH)
        {
            correctForPH();
        }

        if (hasHydrogensAdded())
        {
            return true;
        }

        setHydrogensAdded();

        //count up number of hydrogens to add
        JOEAtom atom;

        //count up number of hydrogens to add
        JOEAtom h;
        int hcount;
        int count = 0;
        Vector vhadd = new Vector();

        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (polaronly &&
                    !(atom.isNitrogen() || atom.isOxygen() || atom.isSulfur() ||
                    atom.isPhosphorus()))
            {
                continue;
            }

            hcount = atom.getImplicitValence() - atom.getValence();

            if (hcount < 0)
            {
                hcount = 0;
            }

            if (hcount != 0)
            {
                vhadd.add(new AtomHCount(atom, hcount));
                count += hcount;
            }
        }

        if (count == 0)
        {
            return true;
        }

        boolean hasCoords = hasNonZeroCoords();

        //realloc memory in coordinate arrays for new hydrogens
        double[] tmpf = null;
        double[] iterf;
        int size = _vconf.size();

        for (int j = 0; j < size; j++)
        {
            iterf = (double[]) _vconf.get(j);
            tmpf = new double[(numAtoms() + count) * 3];

            //System.out.println("j"+j+" _c"+_c+" length"+_c.length+" (numAtoms() + count) * 3:"+((numAtoms() + count) * 3));
            //System.out.println("numAtoms():"+numAtoms()+" count:"+ count+" iterf.length:"+iterf.length+" tmpf.length:"+tmpf.length);
            if (hasCoords)
            {
                System.arraycopy(iterf, 0, tmpf, 0, iterf.length);
            }

            _vconf.set(j, tmpf);
            iterf = null;
        }

        //Just delete the pose coordinate array.  It will automatically
        //be reallocated when SetPose(int) is called.
        if (_xyz_pose != null)
        {
            _xyz_pose = null;
        }

        if (useCoordV == false)
        {
            beginModify();

            //incrementMod();
        }

        int m;

        XYZVector v = new XYZVector();
        double hbrad = JOEElementTable.instance().correctedBondRad(1, 0);

        for (int k = 0; k < vhadd.size(); k++)
        {
            AtomHCount atomHCount = (AtomHCount) vhadd.get(k);
            atom = atomHCount.atom;

            double bondlen = hbrad +
                JOEElementTable.instance().correctedBondRad(atom.getAtomicNum(),
                    atom.getHyb());

            for (m = 0; m < atomHCount.hCount; m++)
            {
                //                System.out.println("numConformers():"+numConformers()+"atomHCount.hCount:"+atomHCount.hCount);
                //remove this line
                if (useCoordV == true)
                {
                    atom.getNewBondVector(v, bondlen);
                }

                // bug in this code
                // the memory allocated from endModify for _c is to small
                // the solution would be to allocate (numAtoms() + count) * 3
                // eventually over a global _hcount member variable
                // but lets think about it ...
                //                for (n = 0; n < numConformers(); n++)
                //                {
                //                    setConformer(n);
                //                    if (hasCoords)
                //                    {
                //                        atom.getNewBondVector(v, bondlen);
                //                        System.out.println("n"+n+" _c"+_c+" length"+_c.length+" numAtoms()*3:"+(numAtoms()*3));
                //                       // _c[(numAtoms()) * 3] = v.x();
                //                       // _c[(numAtoms()) * 3 + 1] = v.y();
                //                       // _c[(numAtoms()) * 3 + 2] = v.z();
                //                    }
                //                    else
                //                    {
                //                      v.set(0.0,0.0,0.0);
                //                       // _c[(numAtoms()) * 3] = 0.0;
                //                       // _c[(numAtoms()) * 3 + 1] = 0.0;
                //                       // _c[(numAtoms()) * 3 + 2] = 0.0;
                //                    }
                //                }
                h = newAtom();
                h.setType("H");
                h.setAtomicNum(1);
                h.setVector(v.x(), v.y(), v.z());
                addBond(atom.getIdx(), h.getIdx(), 1);

                //h.setCoordPtr(_c);
            }
        }

        if (useCoordV == false)
        {
            endModify();
        }

        setConformer(0);

        //reset atom type and partial charge flags
        _flags &= (~(JOE_PCHARGE_MOL | JOE_ATOMTYPES_MOL));

        return true;
    }

    /**
     * Adds hydrogens atoms to the given atom.
     * The pH value will not be corrected.
     *
     * @param  atom  The atom to which the hydogens should be added
     * @return               <tt>true</tt> if successfull
     */
    public synchronized boolean addHydrogens(JOEAtom atom)
    {
        JOEAtom h;

        //count up number of hydrogens to add
        int hcount;

        //count up number of hydrogens to add
        int count = 0;
        Vector vhadd = new Vector();

        hcount = atom.getImplicitValence() - atom.getValence();

        if (hcount < 0)
        {
            hcount = 0;
        }

        if (hcount != 0)
        {
            vhadd.add(new AtomHCount(atom, hcount));
            count += hcount;
        }

        if (count == 0)
        {
            return true;
        }

        //realloc memory in coordinate arrays for new hydroges
        double[] tmpf;
        double[] iter;

        for (int j = 0; j < _vconf.size(); j++)
        {
            iter = (double[]) _vconf.get(j);
            tmpf = new double[((numAtoms() + count) * 3) + 10];
            System.arraycopy(iter, 0, tmpf, 0, numAtoms() * 3);
            _vconf.set(j, tmpf);
            iter = null;
        }

        //Just delete the pose coordinate array.  It will automatically
        //be reallocated when SetPose(int) is called.
        if (_xyz_pose != null)
        {
            _xyz_pose = null;
        }

        incrementMod();

        int m;

        int n;
        XYZVector v = new XYZVector();
        AtomHCount atomHCount;
        double hbrad = JOEElementTable.instance().correctedBondRad(1, 0);

        for (int k = 0; k < vhadd.size(); k++)
        {
            atomHCount = (AtomHCount) vhadd.get(k);
            atom = atomHCount.atom;

            double bondlen = hbrad +
                JOEElementTable.instance().correctedBondRad(atom.getAtomicNum(),
                    atom.getHyb());

            for (m = 0; m < atomHCount.hCount; m++)
            {
                for (n = 0; n < numConformers(); n++)
                {
                    setConformer(n);
                    atom.getNewBondVector(v, bondlen);
                    _c[(numAtoms()) * 3] = v.x();
                    _c[((numAtoms()) * 3) + 1] = v.y();
                    _c[((numAtoms()) * 3) + 2] = v.z();
                }

                h = newAtom();
                h.setType("H");
                h.setAtomicNum(1);
                addBond(atom.getIdx(), h.getIdx(), 1);
                h.setCoordPtr(_c);
            }
        }

        decrementMod();
        setConformer(0);

        //reset atom type and partial charge flags
        //_flags &= (~(JOE_PCHARGE_MOL|JOE_ATOMTYPES_MOL));
        return true;
    }

    /**
     * Add polar hydrogens to molecule.
     * All hydrogens in neighbourhood to N,O,P or S are treated as polar.
     * The pH value will be corrected.
     *
     * @return    Description of the Return Value
     * @see JOEPhModel
     */
    public synchronized boolean addPolarHydrogens()
    {
        return addHydrogens(true);
    }

    /**
     * Adds residue information to this molecule.
     *
     * @param  residue  The residue information
     * @return          <tt>true</tt> if successfull
     */
    public synchronized boolean addResidue(JOEResidue residue)
    {
        beginModify();

        JOEResidue oeresidue = (JOEResidue) residue.clone();
        oeresidue.setIdx(_residue.size());
        _residue.add(oeresidue);

        endModify();

        return true;
    }

    /**
     * Aligns atom a1 on p1 and atom a2 along p1->p2 vector.
     *
     * @param  a1  first atom
     * @param  a2  second atom
     * @param  p1  start point
     * @param  p2  end point
     */
    public synchronized void align(JOEAtom a1, JOEAtom a2, XYZVector p1,
        XYZVector p2)
    {
        Vector children = new Vector();

        // of type int[1]
        //find which atoms to rotate
        findChildren(children, a1.getIdx(), a2.getIdx());
        children.add(new int[]{a2.getIdx()});

        //find the rotation vector and angle
        XYZVector v1 = new XYZVector();
        XYZVector v2 = new XYZVector();
        XYZVector v3 = new XYZVector();
        v1 = p2.sub(p1);
        v2 = a2.getVector().sub(a1.getVector());
        XYZVector.cross(v3, v1, v2);

        double angle = XYZVector.xyzVectorAngle(v1, v2);

        //find the rotation matrix
        Matrix3x3 m = new Matrix3x3();
        m.rotAboutAxisByAngle(v3, angle);

        //rotate atoms
        XYZVector v;
        JOEAtom atom;
        int[] itmp;

        for (int i = 0; i < children.size(); i++)
        {
            itmp = (int[]) children.get(i);
            atom = getAtom(itmp[0]);
            v = atom.getVector();
            v.subing(a1.getVector());
            v.muling(m);

            //rotate the point
            v.adding(p1);

            //translate the vector
            atom.setVector(v);
        }

        //set a1 = p1
        a1.setVector(p1);
    }

    /**
     * Gets an iterator over all atoms in this molecule.
     *
     * Possibility one:
     * <blockquote><pre>
     * AtomIterator ait = mol.atomIterator();
     * JOEAtom atom;
     * while (ait.hasNext())
     * {
     *   atom = ait.nextAtom();
     *
     * }
     * </pre></blockquote>
     *
     * Atoms starts with the index 1, which is a difference to bonds
     * which starts with the bond index 0.
     * Possibility two:
     * <blockquote><pre>
     * int atoms = mol.numAtoms();
     * JOEAtom atom;
     * for (int i=1; i&lt;=atoms; i++)
     * {
     *   atom=mol.getAtom(i);
     *
     * }
     * </pre></blockquote>
     *
     * @return    the atom iterator for this molecule
     * @see #bondIterator()
     * @see #conformerIterator()
     * @see #getRingIterator()
     * @see #genericDataIterator
     * @see #nativeValueIterator()
     */
    public AtomIterator atomIterator()
    {
        return new AtomIterator(_atom);
    }

    /**
     *  Gets the flag if the automatic calculation of the formal charge of the
     *  atoms is allowed. This is for example used in the PH value correction
     *  method.
     *
     * @return    <tt>true</tt> if the calculation of the formal charge is
     *      allowed.
     */
    public boolean automaticFormalCharge()
    {
        return (_autoFormalCharge);
    }

    /**
     *  Gets the flag if the automatic calculation of the partial charge of the
     *  atoms is allowed.
     *
     * @return    <tt>true</tt> if the calculation of the partial charge is
     *      allowed.
     * @see joelib.molecule.charge.JOEGastChrg
     * @cite gm78
     */
    public boolean automaticPartialCharge()
    {
        return (_autoPartialCharge);
    }

    /**
     * Begins modification of atoms and increase modification counter.
     *
     * The modification will be only processed if <tt>getMod()</tt> is 0!!!
     * If you have called beginModify/endModify twice you can not expect
     * that these changes are already available correctly.
     * This fits especially for deleted and added atoms, because endModify
     * updates the atomId's, writes the atom coordinates to the rotamer
     * arrays and checks the aromaticity.
     *
     * @see #endModify()
     * @see #endModify(boolean)
     * @see #decrementMod()
     * @see #incrementMod()
     * @see #getMod()
     */
    public synchronized void beginModify()
    {
        //        if(logger.isDebugEnabled())logger.debug("beginModify");
        //suck coordinates from _c into _v for each atom
        if ((_mod == 0) && !empty())
        {
            JOEAtom atom;
            AtomIterator ait = this.atomIterator();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();
                atom.setVector();
                atom.clearCoordPtr();
            }

            //			ConformerIterator cit = conformerIterator();
            //			double[] conformer;
            //
            //			while (cit.hasNext())
            //			{
            //				conformer = cit.nextConformer();
            //				conformer = null;
            //			}
            _c = null;
            _vconf.clear();

            //        deletePoses();
            //Destroy rotamer list if necessary
            //        if (getData("RotamerList")!=null) {
            //            JOERotamerList tmp = getData("RotamerList");
            //            tmp=null;
            //            deleteData(oeRotamerList);
            //          }
        }

        _mod++;
    }

    /**
     *  Gets an iterator over all bonds.
     *
     * Possibility one:
     * <blockquote><pre>
     * BondIterator bit = bondIterator();
     * JOEBond bond;
     * while (bit.hasNext())
     * {
     *   bond = bit.nextBond();
     *
     * }
     * </pre></blockquote>
     *
     * Bonds starts with the index 0, which is a difference to atoms
     * which starts with the atom index 1.
     *
     * Possibility two:
     * <blockquote><pre>
     * int bonds = mol.numBonds();
     * JOEBond bond;
     * for (int i=0; i&lt;bonds; i++)
     * {
     *   bond=mol.getBond(i);
     *
     * }
     * </pre></blockquote>
     *
     * @return    the bond iterator
     * @see #atomIterator()
     * @see #conformerIterator()
     * @see #getRingIterator()
     * @see #genericDataIterator
     * @see #nativeValueIterator()
     */
    public BondIterator bondIterator()
    {
        return new BondIterator(_bond);
    }

    /**
     * Centers molecule.
     *
     * @see #center(int)
     */
    public synchronized void center()
    {
        int j;
        int size;
        double[] c;
        double x;
        double y;
        double z;
        double fsize;

        size = numAtoms();
        fsize = -1.0f / (double) numAtoms();

        double[] fa;

        for (int i = 0; i < _vconf.size(); i++)
        {
            fa = (double[]) _vconf.get(i);
            c = fa;
            x = y = z = 0.0f;

            for (j = 0; j < size; j++)
            {
                x += c[j * 3];
                y += c[(j * 3) + 1];
                z += c[(j * 3) + 2];
            }

            x *= fsize;
            y *= fsize;
            z *= fsize;

            for (j = 0; j < size; j++)
            {
                c[j * 3] += x;
                c[(j * 3) + 1] += y;
                c[(j * 3) + 2] += z;
            }
        }
    }

    /**
     * Centers conformer.
     *
     * @param  nconf  number of the conformer
     * @return        the center of the conformer
             * @see #center()
     */
    public synchronized XYZVector center(int nconf)
    {
        setConformer(nconf);

        JOEAtom atom = null;
        AtomIterator ait = this.atomIterator();
        double x = 0.0f;
        double y = 0.0f;
        double z = 0.0f;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            x += atom.x();
            y += atom.y();
            z += atom.z();
        }

        double natoms = (double) numAtoms();
        x /= natoms;
        y /= natoms;
        z /= natoms;

        XYZVector vtmp = new XYZVector();
        XYZVector v = new XYZVector(x, y, z);

        ait.reset();

        while (ait.hasNext())
        {
            XYZVector.sub(vtmp, atom.getVector(), v);
            atom.setVector(vtmp);
        }

        return v;
    }

    /**
     * Clears molecule.
     *
     * @return    <tt>true</tt> if successfull
     */
    public synchronized boolean clear()
    {
        //		JOEAtom atom;
        //		JOEBond bond;
        //    AtomIterator       ait        = this.atomIterator();
        //    BondIterator       bit        = this.bondIterator();
        //    while (ait.hasNext())
        //    {
        //      atom = ait.nextAtom();
        //      destroyAtom(atom);
        //      atom = null;
        //    }
        _atom.clear();

        //    while (bit.hasNext())
        //    {
        //      bond = bit.nextBond();
        //      destroyBond(bond);
        //      bond = null;
        //    }
        _bond.clear();

        _natoms = _nbonds = 0;

        //Delete residues
        //    int                ii;
        //    Object             obj;
        //    for (ii = 0; ii < _residue.size(); ii++)
        //    {
        //      obj = _residue.get(ii);
        //      obj = null;
        //    }
        _residue.clear();

        //clear out the multiconformer data
        //    ConformerIterator  cit        = conformerIterator();
        //double[] conformer;
        //    while (cit.hasNext())
        //    {
        //      conformer = cit.nextConformer();
        //      cit.remove();
        //    }
        _vconf.clear();

        //
        //    //Clear out the pose data
        //    deletePoses();
        genericData.clear();

        _c = null;
        _flags = 0;
        _mod = 0;

        return true;
    }

    /**
     *  Clones molecule without additional data (e.g. descriptor data).
     *
     * @return    the cloned molecule
     */
    public synchronized Object clone()
    {
        return (new JOEMol(this));
    }

    /**
     * Clones this molecule.
     *
     * @param  cloneDesc  clones the PairData descriptors if <tt>true</tt>
     * @return            the cloned molecule
     */
    public synchronized Object clone(boolean cloneDesc)
    {
        return (new JOEMol(this, cloneDesc));
    }

    /**
     * Clones this molecule.
     *
     * @param  cloneDesc  clones the PairData descriptors if <tt>true</tt>
     * @param  cloneDesc  clones the PairData descriptors if <tt>true</tt>
     * @return            the cloned molecule
     */
    public synchronized Object clone(boolean cloneDesc, String[] descriptors)
    {
        return (new JOEMol(this, cloneDesc, descriptors));
    }

    // public void connectTheDotsSort();
    //  public  void beginAccess()
    //  {
    //    if (_access == 0) unCompress();
    //    _access++;
    //  }
    //
    //  public  void endAccess()
    //  {
    //    _access--;
    //    if (_access == 0) compress();
    //  }

    /**
     * Gets an iterator over all conformers.
     *
     * <blockquote><pre>
     * ConformerIterator cit = mol.conformerIterator();
     * double conformer[];
     * while (cit.hasNext())
     * {
     *   conformer = cit.nextConformer();
     *
     * }
     * </pre></blockquote>
     *
     * @return    the conformer iterator
     * @see #atomIterator()
     * @see #bondIterator()
     * @see #getRingIterator()
     * @see #genericDataIterator
     * @see #nativeValueIterator()
     */
    public ConformerIterator conformerIterator()
    {
        return new ConformerIterator(_vconf);
    }

    /**
     * Returns the number of conformers.
     *
     * @return    number of conformers
     */
    public final int conformerNum()
    {
        return _vconf.size();
    }

    /**
     *  Use inter-atomic distances to identify bonds.
         * <p>
         * For assigning atom types using a geometry-based algorithm have a look at {@cite ml91} and the
         * structure based expert rules in {@link joelib.data.JOEAtomTyper}.
     *
     * @cite ml91
     */
    public synchronized void connectTheDots()
    {
        if (empty())
        {
            return;
        }

        if (!has3D())
        {
            return; // not useful on 2D structures
        }

        int j;
        int k;
        int max;
        JOEAtom atom;
        JOEAtom nbr;

        Vector zsortedAtoms = new Vector();

        Vector rad = new Vector();

        Vector zsorted = new Vector();
        double[] c = new double[numAtoms() * 3];
        rad.setSize(_natoms);

        AtomIterator ait = this.atomIterator();
        j = 0;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            (atom.getVector()).get(c, j * 3);

            AtomZPos entry = new AtomZPos(atom, atom.getVector().z());
            zsortedAtoms.add(entry);
            j++;
        }

        //???????
        //    bool SortAtomZ(const pair<OEAtom*,float> &a, const pair<OEAtom*,float> &b)
        //    {
        //      return (a.second < b.second);
        //    }
        QuickInsertSort qisort = new QuickInsertSort();
        qisort.sort(zsortedAtoms, new AtomZPosComparator());

        max = zsortedAtoms.size();

        for (j = 0; j < max; j++)
        {
            atom = ((AtomZPos) zsortedAtoms.get(j)).atom;

            double fvalue = JOEElementTable.instance().correctedBondRad(atom.getAtomicNum(),
                    3);
            fvalue *= 1.10f;
            rad.set(j, new double[]{fvalue});
            zsorted.add(new int[]{atom.getIdx() - 1});
        }

        int[] itmp;

        //		double ftmp[];
        int idx1;
        int idx2;
        double cutoff;
        double zd;
        double tmpf;
        double d2;

        for (j = 0; j < max; j++)
        {
            itmp = (int[]) zsorted.get(j);
            idx1 = itmp[0];

            for (k = j + 1; k < max; k++)
            {
                itmp = (int[]) zsorted.get(k);
                idx2 = itmp[0];

                cutoff = ((double[]) rad.get(j))[0] +
                    ((double[]) rad.get(k))[0];
                cutoff *= cutoff;

                tmpf = c[(idx1 * 3) + 2] - c[(idx2 * 3) + 2];
                zd = tmpf * tmpf;

                //if (zd > cutoff)
                if (zd > 25.0)
                {
                    // bigger than max cutoff
                    break;
                }

                tmpf = c[idx1 * 3] - c[idx2 * 3];
                d2 = tmpf * tmpf;
                tmpf = c[(idx1 * 3) + 1] - c[(idx2 * 3) + 1];
                d2 += (tmpf * tmpf);
                d2 += zd;

                if (d2 > cutoff)
                {
                    continue;
                }

                if (d2 < 0.4)
                {
                    continue;
                }

                atom = getAtom(idx1 + 1);
                nbr = getAtom(idx2 + 1);

                if (atom.isConnected(nbr))
                {
                    continue;
                }

                if (atom.isHydrogen() && nbr.isHydrogen())
                {
                    continue;
                }

                addBond(idx1 + 1, idx2 + 1, 1);
            }
        }

        // If between BeginModify and EndModify, coord pointers are NULL
        // setup molecule to handle current coordinates
        //        if (_c == null)
        //        {
        //            //		   _c = c;
        //            //		   for (atom = BeginAtom(i);atom;atom = NextAtom(i))
        //            //			   atom->SetCoordPtr(&_c);
        //            //		   _vconf.push_back(c);
        //            //		   unset = true;
        //        }
        // Cleanup -- delete long bonds that exceed max valence
        JOEBond maxbond;

        // Cleanup -- delete long bonds that exceed max valence
        JOEBond bond;
        double maxlength;
        Vector bonds;

        for (int i = 1; i <= numAtoms(); i++)
        {
            atom = this.getAtom(i);

            while ((atom.BOSum() > (JOEElementTable.instance().getMaxBonds(atom.getAtomicNum()))) ||
                    (atom.smallestBondAngle() < 45.0))
            {
                bonds = atom.getBonds();
                maxbond = (JOEBond) bonds.get(0);
                maxlength = maxbond.getLength();

                for (int m = 0; m < bonds.size(); m++)
                {
                    bond = (JOEBond) bonds.get(m);

                    if (bond.getLength() > maxlength)
                    {
                        maxbond = bond;
                        maxlength = bond.getLength();
                    }
                }

                deleteBond(maxbond);
            }
        }

        //	   if (unset)
        //	   {
        //		   _c = null;
        //		   for (atom = BeginAtom(i);atom;atom = NextAtom(i))
        //			   atom->ClearCoordPtr();
        //		   _vconf.resize(_vconf.size()-1);
        //	   }
        c = null;
    }

    /**
     * Each Vector contains the atom numbers of a contiguous fragment.
     * The integer arrays are sorted by size from largest to smallest.
     * <br>
     * Example:
     * <blockquote><pre>
     * Vector fragments=new Vector();
     * mol.contiguousFragments(fragments);
     * int fragmentAtomIdx[];
     * System.out.println(""+fragments.size()+" contiguous fragments in molecule '"+
     *                    mol.getTitle()+"' (salt ?).");
     * for (int i = 0; i &lt; fragments.size(); i++)
     * {
     *   fragmentAtomIdx=(int[])fragments.get(i);
     *   System.out.print("Atoms of fragment "+i+":");
     *          for (int j = 0; j &lt; fragmentAtomIdx.length; j++)
     *   {
     *     System.out.print(fragmentAtomIdx[j]);
     *     System.out.print(' ');
     *   }
     *   System.out.println();
     * }
     * </pre></blockquote>
     *
     * @param  cfl  a Vector that stores the integer arrays for the contiguous fragments atom numbers
     * @see #stripSalts()
     */
    public synchronized void contiguousFragments(Vector cfl)
    {
        int j;
        AtomIterator ait = atomIterator();
        JOEBitVec used;
        JOEBitVec curr;
        JOEBitVec next;
        JOEBitVec frag;
        Vector tmp = null;

        //		Vector cflTmp = new Vector();
        used = new JOEBitVec(numAtoms() + 1);
        curr = new JOEBitVec(numAtoms() + 1);
        next = new JOEBitVec(numAtoms() + 1);
        frag = new JOEBitVec(numAtoms() + 1);

        JOEAtom atom;
        JOEBond bond;

        while (used.countBits() < numAtoms())
        {
            curr.clear();
            frag.clear();
            ait.reset();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                if (!used.bitIsOn(atom.getIdx()))
                {
                    //            System.out.println("setb1 "+atom.getIdx());
                    curr.setBitOn(atom.getIdx());

                    break;
                }
            }

            frag.orSet(curr);

            while (!curr.isEmpty())
            {
                next.clear();

                //          for(j=curr.nextSetBit(0); j>=0; j=curr.nextSetBit(j+1))
                for (j = curr.nextBit(-1); j != curr.endBit();
                        j = curr.nextBit(j))
                {
                    atom = getAtom(j);

                    BondIterator bit = atom.bondIterator();

                    while (bit.hasNext())
                    {
                        bond = bit.nextBond();

                        //                  System.out.println("idx"+bond.getNbrAtomIdx(atom));
                        if (!used.bitIsOn(bond.getNbrAtomIdx(atom)))
                        {
                            //                    System.out.println("setb2 "+bond.getNbrAtomIdx(atom));
                            next.setBitOn(bond.getNbrAtomIdx(atom));
                        }
                    }
                }

                used.orSet(curr);
                used.orSet(next);
                frag.orSet(next);
                curr.set(next);
            }

            tmp = new Vector();
            tmp.clear();

            //frag.toVectorWithIntArray(tmp);
            //cflTmp.add(tmp);
            cfl.add(frag.toIntArray());
        }

        QuickInsertSort sorting = new QuickInsertSort();

        //VectorSizeComparator vSizeComparator = new VectorSizeComparator();
        //sorting.sort(cflTmp, vSizeComparator);
        ArraySizeComparator arraySizeComparator = new ArraySizeComparator();
        sorting.sort(cfl, arraySizeComparator);
    }

    /**
     * Copies given conformer to this molecule.
     *
     * @param  src    the conformer
     * @param  idx  the index of the conformer
     */
    public void copyConformer(double[] src, int idx)
    {
        //    assert _vconf.size()!=0 : idx < _vconf.size();
        if ((_vconf.size() == 0) || (idx >= _vconf.size()))
        {
            logger.error("Conformer array _vconf is not defined or to small.");

            return;
        }

        double[] dtmp = (double[]) _vconf.get(idx);
        System.arraycopy(src, 0, dtmp, 0, 3 * numAtoms());
    }

    /**
     * Copies given conformer to this molecule.
     *
     * @param  src    the conformer
     * @param  idx  the index of the conformer
     */
    public void copyConformer(float[] src, int idx)
    {
        //    assert _vconf.size()!=0 : idx < _vconf.size();
        if ((_vconf.size() == 0) || (idx >= _vconf.size()))
        {
            logger.error("_vconf is not defined or to small.");

            return;
        }

        double[] dtmp = (double[]) _vconf.get(idx);
        int i;

        for (i = 0; i < numAtoms(); i++)
        {
            dtmp[i * 3] = (double) src[i * 3];
            dtmp[(i * 3) + 1] = (double) src[(i * 3) + 1];
            dtmp[(i * 3) + 2] = (double) src[(i * 3) + 2];
        }
    }

    /**
     * Corrects pH value of the molecule.
     *
     * @return    <tt>true</tt> if successfull
     * @see JOEPhModel
     */
    public synchronized boolean correctForPH()
    {
        if (isCorrectedForPH())
        {
            return true;
        }

        JOEPhModel.instance().correctForPH(this);

        return true;
    }

    /**
     * Creates only a new atom.
     *
     * @return    the created atom
     * @see #createBond()
     * @see #addAtom(JOEAtom)
     * @see #newAtom()
     */
    public JOEAtom createAtom()
    {
        return new JOEAtom();
    }

    /**
     * Creates only a new bond.
     *
     * @return    the created bond
     * @see #createAtom()
     */
    public JOEBond createBond()
    {
        return new JOEBond();
    }

    /**
     * Returns the number of data elements in this molecule.
     *
     * @return    number of data elements in this molecule
     */
    public int dataSize()
    {
        return genericData.size();
    }

    /**
     * Decrease modification counter.
     *
     * @see #incrementMod()
     * @see #getMod()
     * @see #beginModify()
     * @see #endModify()
     * @see #endModify(boolean)
     */
    public void decrementMod()
    {
        _mod--;
    }

    /**
     * Delete atom from molecule.
     *
     * @param  atom  The atom to delete
     * @return       <tt>true</tt> if successfull
     */
    public synchronized boolean deleteAtom(JOEAtom atom)
    {
        if (atom.isHydrogen())
        {
            //System.out.println("delete hydrogen atom");
            return deleteHydrogen(atom);
        }

        beginModify();

        //don't need to do anything with coordinates b/c
        //beginModify() blows away coordinates
        //System.out.println("delete normal atom");
        //find bonds to delete
        Vector vdb = new Vector();
        NbrAtomIterator nait = atom.nbrAtomIterator();

        JOEAtom tmpAtom;

        while (nait.hasNext())
        {
            tmpAtom = nait.nextNbrAtom();
            vdb.add(nait.actualBond());
        }

        for (int j = 0; j < vdb.size(); j++)
        {
            //System.out.println("delete bond");
            deleteBond((JOEBond) vdb.get(j));
        }

        //delete bonds
        _atom.remove(atom.getIdx() - 1);
        destroyAtom(atom);
        _natoms--;

        //reset all the indices to the atoms
        int idx = 1;
        JOEAtom tatom;
        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            tatom = ait.nextAtom();

            //System.out.println("old idx:"+tatom.getIdx()+" set idx:"+idx+" atom "+ ((Object)tatom));
            tatom.setIdx(idx);
            idx++;
        }

        endModify();

        return true;
    }

    /**
     * Delete the given <tt>JOEBond</tt> from this molecule.
     *
     * @param  bond  The bond to delete
     * @return       <tt>true</tt> if successfull
     */
    public boolean deleteBond(JOEBond bond)
    {
        // nothing to do !
        if (bond == null)
        {
            return true;
        }

        // start deleting bond
        beginModify();

        if (bond.getBeginAtom() == null)
        {
            logger.warn("Begin atom does not exist.");
        }
        else
        {
            (bond.getBeginAtom()).deleteBond(bond);
        }

        if (bond.getEndAtom() == null)
        {
            logger.warn("End atom does not exist.");
        }
        else
        {
            (bond.getEndAtom()).deleteBond(bond);
        }

        _bond.remove(bond.getIdx());

        destroyBond(bond);

        JOEBond tbond;
        BondIterator bit = this.bondIterator();
        int j = 0;

        while (bit.hasNext())
        {
            tbond = bit.nextBond();
            tbond.setIdx(j);
            j++;
        }

        _nbonds--;
        endModify();

        return true;
    }

    /**
     * Delete conformer from molecule.
     *
     * @param  idx  The conformer number
     */
    public void deleteConformer(int idx)
    {
        if ((idx < 0) || (idx >= _vconf.size()))
        {
            return;
        }

        //		double daTmp[] = (double[]) _vconf.get(idx);
        //		daTmp = null;
        _vconf.remove(idx);
    }

    /**
     *  Description of the Method
     *
     * @param  s  Description of the Parameter
     * @return    Description of the Return Value
     */
    public boolean deleteData(String s)
    {
        return genericData.deleteData(s);
    }

    /**
     *  Deletes all data elements os the given <tt>JOEDataType</tt> .
     *
     * @param  dt  type of the elements to delete
     */
    public void deleteData(JOEDataType dt)
    {
        genericData.deleteData(dt);
    }

    /**
     *  Delete all data elements which are <tt>equal</tt> to the given <tt>
     *  JOEGenericData</tt> element.
     *
     * @param  gd  the element to delete
     */
    public void deleteData(JOEGenericData gd)
    {
        genericData.deleteData(gd);
    }

    /**
     *  Description of the Method
     *
     * @param  vg  Description of the Parameter
     */
    public void deleteData(Vector vg)
    {
        genericData.deleteData(vg);
    }

    /**
     * Deletes hydrogen atom.
     *
     * <p>
     * <blockquote><pre>
     * mol.beginModify();
     * deleteHydrogen(atom );
     * mol.endModify();
     * </pre></blockquote>
     *
     * @param  atom  Hydrogen atom to delete
     * @return       <tt>true</tt> if successfull
     */
    public boolean deleteHydrogen(JOEAtom atom)
    {
        //find bonds to delete
        //JOEAtom nbr;
        Vector vdb = new Vector();
        NbrAtomIterator nait = atom.nbrAtomIterator();

        //if(nait.hasNext())vdb.add(nait.actualBond());
        while (nait.hasNext())
        {
            //nbr =
            nait.nextNbrAtom();
            vdb.add(nait.actualBond());
        }

        //if(vdb.size()==0) return true;
        incrementMod();

        for (int j = 0; j < vdb.size(); j++)
        {
            deleteBond((JOEBond) vdb.get(j));
        }

        //delete bonds
        decrementMod();

        int idx = 0;

        if (atom.getIdx() != (int) numAtoms())
        {
            idx = atom.getCIdx();

            int size = numAtoms() - atom.getIdx();

            //Update conformer coordinates
            for (int k = 0; k < _vconf.size(); k++)
            {
                //System.out.println("size:::"+size+" length:"+((double[]) _vconf.get(k)).length);
                System.arraycopy((double[]) _vconf.get(k), idx + 3,
                    (double[]) _vconf.get(k), idx, 3 * size);
            }

            //Update pose coordinates if necessary
            if (_xyz_pose != null)
            {
                System.arraycopy(_xyz_pose, idx + 3, _xyz_pose, idx, 3 * size);
            }
        }

        //System.out.println("_atom.size():"+_atom.size()+" idx:"+atom.getIdx());
        _atom.remove(atom.getIdx() - 1);
        destroyAtom(atom);
        _natoms--;

        //reset all the indices to the atoms
        JOEAtom tmp;
        AtomIterator ait = this.atomIterator();
        idx = 1;

        while (ait.hasNext())
        {
            tmp = ait.nextAtom();
            tmp.setIdx(idx);
            idx++;
        }

        return true;
    }

    /**
     * Delete all hydrogen atoms from molecule.
     *
     * <p>
             * <blockquote><pre>
             * mol.beginModify();
             * deleteHydrogens();
             * mol.endModify();
             * </pre></blockquote>
             *
     * @return    <tt>true</tt> if successfull
     */
    public synchronized boolean deleteHydrogens()
    {
        JOEAtom atom = null;
        JOEAtom nbr = null;
        Vector delatoms = new Vector();
        Vector va = new Vector();
        AtomIterator ait = this.atomIterator();

        //if(logger.isDebugEnabled())logger.debug("Start removing H atoms"+this.numAtoms());
        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (atom.isHydrogen())
            {
                delatoms.add(atom);
            }
        }

        if (delatoms.size() == 0)
        {
            return true;
        }

        //if(logger.isDebugEnabled())logger.debug("Count H's");

        /* decide whether these flags need to be reset
         *_flags &= (~(JOE_ATOMTYPES_MOL));
         *_flags &= (~(JOE_HYBRID_MOL));
         *_flags &= (~(JOE_PCHARGE_MOL));
         *_flags &= (~(JOE_IMPVAL_MOL));
         */

        //find bonds to delete
        Vector vdb = new Vector();

        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            //      System.out.println("atom element symbol:" + JOEElementTable.instance().getSymbol(atom.getAtomicNum()));
            if (!atom.isHydrogen())
            {
                NbrAtomIterator nait = atom.nbrAtomIterator();

                while (nait.hasNext())
                {
                    nbr = nait.nextNbrAtom();

                    //          System.out.println("nbr element symbol:" + JOEElementTable.instance().getSymbol(nbr.getAtomicNum()));
                    //          System.out.println("is H: "+nbr.isHydrogen());
                    if (nbr.isHydrogen())
                    {
                        vdb.add(nait.actualBond());
                    }
                }
            }
        }

        incrementMod();

        for (int j = 0; j < vdb.size(); j++)
        {
            deleteBond((JOEBond) vdb.get(j));
        }

        //delete bonds
        decrementMod();

        int idx1 = 0;

        int idx2 = 0;
        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (!atom.isHydrogen())
            {
                //Update conformer coordinates
                for (int k = 0; k < _vconf.size(); k++)
                {
                    System.arraycopy((double[]) _vconf.get(k), idx1 * 3,
                        (double[]) _vconf.get(k), idx2 * 3, 3);
                }

                //        //Update pose coordinates if necessary
                //        if (_xyz_pose != null)
                //        {
                //          System.arraycopy(_xyz_pose, idx1 * 3, _xyz_pose, idx2 * 3, 3);
                //        }
                idx2++;
                va.add(atom);
            }

            idx1++;
        }

        for (int i = 0; i < delatoms.size(); i++)
        {
            destroyAtom((JOEAtom) delatoms.get(i));
            _natoms--;
        }

        //_atom = va;
        _atom.clear();

        for (int i = 0; i < va.size(); i++)
        {
            _atom.add(va.get(i));
        }

        //_atom.resize(_atom.size()+1);
        //_atom[_atom.size()-1] = null;
        _natoms = (short) va.size();

        //reset all the indices to the atoms
        ait.reset();
        idx1 = 1;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atom.setIdx(idx1);

            //System.out.println("RESET to:"+idx1);
            idx1++;
        }

        if (logger.isDebugEnabled())
        {
            //check atom neighbours
            ait.reset();

            int max = this.numAtoms();
            NbrAtomIterator nbrIterator;

            //logger.info("H atoms removed "+max);
            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                //System.out.print(" "+atom.getIdx());
                if (nbr.getIdx() > max)
                {
                    logger.error("Atom " + atom.getIdx() +
                        " index should not be greater " + max + ".");
                }

                nbrIterator = atom.nbrAtomIterator();

                while (nbrIterator.hasNext())
                {
                    nbr = nbrIterator.nextNbrAtom();

                    //System.out.println("JOEMol:Atom ("+atom.getIdx()+") neighbour index: "+nbr.getIdx());
                    if (nbr.getIdx() > max)
                    {
                        logger.error("Atom " + nbr.getIdx() +
                            " should not exist(" + max + ").");
                    }
                }
            }

            //System.out.println("");
        }

        return true;
    }

    /**
     * Delete all hydrogen atoms from given atom.
     *
     * <p>
     * <blockquote><pre>
     * mol.beginModify();
     * deleteHydrogens(atom);
     * mol.endModify();
     * </pre></blockquote>
     *
     * @param  atom  Atom from which hydrogen atoms should be deleted
     * @return    <tt>true</tt> if successfull
     */
    public synchronized boolean deleteHydrogens(JOEAtom atom)
    {
        JOEAtom nbr;
        JOEBitVec bvd = new JOEBitVec();
        Vector delatoms = new Vector();

        NbrAtomIterator nait = atom.nbrAtomIterator();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();

            if (nbr.isHydrogen())
            {
                delatoms.add(nbr);
                bvd.set(nbr.getIdx());
            }
        }

        if (delatoms.size() == 0)
        {
            return true;
        }

        incrementMod();

        int idx1 = 0;

        int idx2 = 0;

        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            nbr = ait.nextAtom();

            if (!bvd.get(nbr.getIdx()))
            {
                //Update conformer coordinates
                for (int k = 0; k < _vconf.size(); k++)
                {
                    System.arraycopy((double[]) _vconf.get(k), idx1 * 3,
                        (double[]) _vconf.get(k), idx2 * 3, 3);
                }

                //Update pose coordinates if necessary
                if (_xyz_pose != null)
                {
                    System.arraycopy(_xyz_pose, idx1 * 3, _xyz_pose, idx2 * 3, 3);
                }

                idx2++;
            }

            idx1++;
        }

        for (int i = 0; i < delatoms.size(); i++)
        {
            deleteAtom((JOEAtom) delatoms.get(i));
        }

        decrementMod();

        return true;
    }

    /**
     * Delete all non polar hydrogens from molecule.
     * All hydrogens in neighbourhood to N,O,P or S are treated as polar.
     *
     * @return    <tt>true</tt> if successfull
     */
    public synchronized boolean deleteNonPolarHydrogens()
    {
        JOEAtom atom;
        Vector delatoms = new Vector();
        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (atom.isNonPolarHydrogen())
            {
                delatoms.add(atom);
            }
        }

        if (delatoms.size() == 0)
        {
            return (true);
        }

        incrementMod();

        /*int idx1,idx2;
        *vector<double*>::iterator j;
        *for (idx1=0,idx2=0,atom = BeginAtom(i);atom;atom = NextAtom(i),idx1++)
        *if (!atom->IsHydrogen())
        *{
        *for (j = _vconf.begin();j != _vconf.end();j++)
        *memcpy((char*)&((*j)[idx2*3]),(char*)&((*j)[idx1*3]),sizeof(double)*3);
        *idx2++;
        *}
        */
        for (int i = 0; i < delatoms.size(); i++)
        {
            deleteAtom((JOEAtom) delatoms.get(i));
        }

        decrementMod();

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  residue  Description of the Parameter
     * @return          Description of the Return Value
     */
    public synchronized boolean deleteResidue(JOEResidue residue)
    {
        int idx = residue.getIdx();

        for (int i = idx; i < _residue.size(); i++)
        {
            ((JOEResidue) _residue.get(i)).setIdx(i - 1);
        }

        _residue.remove(idx);

        residue = null;

        return (true);
    }

    /**
     * Destroys atom.
     *
     * @param  atom  the atom
     */
    public void destroyAtom(JOEAtom atom)
    {
        //atom = null;
    }

    /**
     * Destroys all bond.
     *
     * @param  bond  the bond
     */
    public void destroyBond(JOEBond bond)
    {
        bond = null;
    }

    /**
     * Returns <tt>true</tt> if this molecule contains no atoms.
     *
     * @return   <tt>true</tt> if this molecule contains no atoms
     */
    public boolean empty()
    {
        return (_natoms == 0);
    }

    /**
     * Ends modification of atoms and decrease modification counter.
     * All flags will be deleted.
     *
     * The modification will be  only processed if <tt>getMod()</tt> is 0!!!
     * If you have called beginModify/endModify twice you can not expect
     * that these changes are already available correctly.
     * This fits especially for deleted and added atoms, because endModify
     * updates the atomId's, writes the atom coordinates to the rotamer
     * arrays and checks the aromaticity.
     *
     * @see #endModify(boolean)
     * @see #beginModify()
     * @see #decrementMod()
     * @see #incrementMod()
     * @see #getMod()
     */
    public synchronized void endModify()
    {
        endModify(true);
    }

    /**
     * Ends modification of atoms and decrease modification counter.
     *
     * The modification will be  only processed if <tt>getMod()</tt> is 0!!!
     * If you have called beginModify/endModify twice you can not expect
     * that these changes are already available correctly.
     * This fits especially for deleted and added atoms, because endModify
     * updates the atomId's, writes the atom coordinates to the rotamer
     * arrays and checks the aromaticity.
     *
     * @param  nukePerceivedData  if <tt>true</tt> all flags will be deleted
     * @see #endModify()
     * @see #beginModify()
     * @see #decrementMod()
     * @see #incrementMod()
     * @see #getMod()
     */
    public synchronized void endModify(boolean nukePerceivedData)
    {
        //System.out.println("end modification counter "+_mod);
        //        if(logger.isDebugEnabled())logger.debug("endModify");
        if (_mod == 0)
        {
            //oeAssert(false);
            //        throw new Exception("_mod is negative - EndModify() called too many times");
            logger.error("_mod is negative - EndModify() called too many times");

            return;
        }

        _mod--;

        if (_mod != 0)
        {
            return;
        }

        if (nukePerceivedData)
        {
            _flags = 0;
        }

        _c = null;

        /*
         *leave generic data alone for now - just nuke it on clear()
         *if (hasData("Comment")) { Object tmp=getData("Comment"); tmp=null;);
         *_vdata.clear();
         */
        if (empty())
        {
            return;
        }

        //if atoms present convert coords into array
        double[] c = new double[(numAtoms() * 3)];
        _c = c;

        int idx = 0;
        JOEAtom atom;
        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atom.setIdx(idx + 1);
            (atom.getVector()).get(_c, idx * 3);
            atom.setCoordPtr(_c);
            idx++;
        }

        _vconf.add(c);

        //        System.out.println("NBONDS2"+numBonds());
        //kekulize structure
        setAromaticPerceived();
        kekulize();
        unsetAromaticPerceived();

        //        System.out.println("NBONDS3"+numBonds());
        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atom.unsetAromatic();
        }

        BondIterator bit = this.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            bond.unsetAromatic();
        }

        //        System.out.println("NBONDS4"+numBonds());
        //unsetImplicitValencePerceived();
        //System.out.println("ENDMOD: coordinates: "+_c.length);
        // reset hashcode
        reHash();
    }

    /**
     * Checks if two molecules are equal, ignoring descriptor values.
     *
     * This method uses the full equality check if the atom and bonds.
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof JOEMol)
        {
            return equals((JOEMol) obj);
        }
        else
        {
            return false;
        }
    }

    /**
     * Checks if two molecules are equal, ignoring descriptor values.
     *
     * This method uses the full equality check if the atom and bonds.
     */
    public boolean equals(JOEMol type)
    {
        if (this.numAtoms() != type.numAtoms())
        {
            return false;
        }

        if (this.numBonds() != type.numBonds())
        {
            return false;
        }

        JOEAtom aS;
        JOEAtom aT;
        JOEBond bS;
        JOEBond bT;
        AtomIterator aitS = this.atomIterator();
        AtomIterator aitT = type.atomIterator();
        BondIterator bitS = this.bondIterator();
        BondIterator bitT = type.bondIterator();

        while (aitS.hasNext() && aitT.hasNext())
        {
            aS = aitS.nextAtom();
            aT = aitT.nextAtom();
            aS.equals(aT);
        }

        while (bitS.hasNext() && bitT.hasNext())
        {
            bS = bitS.nextBond();
            bT = bitT.nextBond();
            bS.equals(bT);
        }

        return true;
    }

    /**
     * Returns <tt>true</tt> if this bond exists.
     *
     * @param  bgn  atom index of the start atom
     * @param  end  atom index of the end atom
     * @return <tt>true</tt> if this bond exists
     */
    public boolean existsBond(int bgn, int end)
    {
        if (this.numAtoms() < Math.max(bgn, end))
        {
            return false;
        }

        if (this.getBond(bgn, end) != null)
        {
            return true;
        }

        return false;
    }

    /**
     * Destructor for this molecule.
     */
    public void finalize()
    {
        JOEAtom atom;
        JOEBond bond;

        //JOEResidue residue;
        BondIterator bit = this.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            destroyBond(bond);
        }

        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            destroyAtom(atom);
        }

        //		ResidueIterator rsit = this.residueIterator();
        //		while (rsit.hasNext())
        //		{
        //			residue = rsit.nextResidue();
        //			residue = null;
        //		}
        //clear out the multiconformer data
        //		double fa[];
        //		int size = _vconf.size();
        //		for (int k = 0; k < size; k++)
        //		{
        //			fa = (double[]) _vconf.get(k);
        //			fa = null;
        //		}
        _vconf.clear();

        //Deallocate the pose coordinate array if necessary
        if (_xyz_pose != null)
        {
            _xyz_pose = null;
        }

        genericData.finalize();
        _atom = null;
        _bond = null;
        _vconf = null;
        _pose = null;
        _residue = null;
    }

    /**
     *  Locates all atoms for which there exists a path to <tt>second</tt> without
     *  going through <tt>first</tt> children does not include <tt>second</tt> .
     *
     * @param  children  Description of the Parameter
     * @param  first     Description of the Parameter
     * @param  second    Description of the Parameter
     */
    public void findChildren(Vector children, int first, int second)
    {
        int i;
        JOEBitVec used = new JOEBitVec();
        JOEBitVec curr = new JOEBitVec();
        JOEBitVec next = new JOEBitVec();

        used.setBitOn(first);
        used.setBitOn(second);
        curr.setBitOn(second);

        JOEAtom atom;
        JOEBond bond;

        while (!curr.isEmpty())
        {
            next.clear();

            //        for(i=curr.nextSetBit(0); i>=0; i=curr.nextSetBit(i+1))
            for (i = curr.nextBit(-1); i != curr.endBit();
                    i = curr.nextBit(i))
            {
                atom = getAtom(i);

                BondIterator bit = atom.bondIterator();

                while (bit.hasNext())
                {
                    bond = bit.nextBond();

                    if (!used.bitIsOn(bond.getNbrAtomIdx(atom)))
                    {
                        next.setBitOn(bond.getNbrAtomIdx(atom));
                    }
                }
            }

            used.orSet(next);
            curr.set(next);
        }

        used.setBitOff(first);
        used.setBitOff(second);
        used.toVectorWithIntArray(children);
    }

    /**
     *  locates all atoms for which there exists a path to 'second' without going
     *  through 'first' children does not include 'second'
     *
     * @param  children  Description of the Parameter
     * @param  bgn       Description of the Parameter
     * @param  end       Description of the Parameter
     */
    public void findChildren(Vector children, JOEAtom bgn, JOEAtom end)
    {
        JOEBitVec used = new JOEBitVec();
        JOEBitVec curr = new JOEBitVec();
        JOEBitVec next = new JOEBitVec();

        used.set(bgn.getIdx());
        used.set(end.getIdx());
        curr.set(end.getIdx());
        children.clear();

        int i;
        JOEAtom atom;
        JOEAtom nbr;

        for (;;)
        {
            next.clear();

            //        for(i=curr.nextSetBit(0); i>=0; i=curr.nextSetBit(i+1))
            for (i = curr.nextBit(-1); i != curr.endBit();
                    i = curr.nextBit(i))
            {
                atom = getAtom(i);

                NbrAtomIterator nait = atom.nbrAtomIterator();

                while (nait.hasNext())
                {
                    nbr = nait.nextNbrAtom();

                    if (!used.get(nbr.getIdx()))
                    {
                        children.add(nbr);
                        next.set(nbr.getIdx());
                        used.set(nbr.getIdx());
                    }
                }
            }

            if (next.size() == 0)
            {
                break;
            }

            curr.set(next);
        }
    }

    /**
     *  Description of the Method
     */
    public synchronized void findChiralCenters()
    {
        if (hasChiralityPerceived())
        {
            return;
        }

        setChiralityPerceived();

        //do quick test to see if there are any possible chiral centers
        boolean mayHaveChiralCenter = false;
        JOEAtom atom = null;
        JOEAtom nbr = null;

        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if ((atom.getHyb() == 3) && (atom.getHvyValence() >= 3))
            {
                mayHaveChiralCenter = true;

                break;
            }
        }

        if (!mayHaveChiralCenter)
        {
            return;
        }

        JOEBond bond;
        BondIterator bit = this.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isWedge() || bond.isHash())
            {
                (bond.getBeginAtom()).setChiral();
            }
        }

        double[] gp = GraphPotentials.graphPotentials(this);

        //System.out.println("O.K.");
        boolean ischiral;
        Vector tlist = new Vector();
        double[] ftmp;

        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            //        System.out.println("ait");
            if ((atom.getHyb() == 3) && (atom.getHvyValence() >= 3) &&
                    !atom.isChiral())
            {
                //            System.out.println("if");
                tlist.clear();
                ischiral = true;

                NbrAtomIterator nait = atom.nbrAtomIterator();

                while (nait.hasNext())
                {
                    //                System.out.println("nait");
                    nbr = nait.nextNbrAtom();

                    for (int k = 0; k < tlist.size(); k++)
                    {
                        //                    System.out.println("k:"+k);
                        ftmp = (double[]) tlist.get(k);

                        if (Math.abs(gp[nbr.getIdx() - 1] - ftmp[0]) < 0.001)
                        {
                            ischiral = false;
                        }
                    }

                    if (ischiral)
                    {
                        tlist.add(new double[]{gp[nbr.getIdx() - 1]});
                    }
                    else
                    {
                        break;
                    }
                }

                if (ischiral)
                {
                    atom.setChiral();
                }
            }
        }

        //System.out.println("FINI");
    }

    /**
     *  each vector<int> contains the atom numbers of a contig fragment the
     *  vectors are sorted by size from largest to smallest
     *
     * @param  lf  Description of the Parameter
     */
    public synchronized void findLargestFragment(JOEBitVec lf)
    {
        int j;
        JOEAtom atom;
        JOEBond bond;

        AtomIterator ait = this.atomIterator();
        JOEBitVec used = new JOEBitVec();
        JOEBitVec curr = new JOEBitVec();
        JOEBitVec next = new JOEBitVec();
        JOEBitVec frag = new JOEBitVec();

        lf.clear();

        while (used.countBits() < numAtoms())
        {
            curr.clear();
            frag.clear();
            ait.reset();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                if (!used.bitIsOn(atom.getIdx()))
                {
                    curr.setBitOn(atom.getIdx());

                    break;
                }
            }

            frag.orSet(curr);

            while (!curr.isEmpty())
            {
                next.clear();

                //            for(j=curr.nextSetBit(0); j>=0; j=curr.nextSetBit(j+1))
                for (j = curr.nextBit(-1); j != curr.endBit();
                        j = curr.nextBit(j))
                {
                    atom = getAtom(j);

                    BondIterator bit = atom.bondIterator();

                    while (bit.hasNext())
                    {
                        bond = bit.nextBond();

                        if (!used.bitIsOn(bond.getNbrAtomIdx(atom)))
                        {
                            next.setBitOn(bond.getNbrAtomIdx(atom));
                        }
                    }
                }

                used.orSet(curr);
                used.orSet(next);
                frag.orSet(next);
                curr.set(next);
            }

            if ((lf.size() == 0) || (lf.countBits() < frag.countBits()))
            {
                lf.set(frag);
            }
        }
    }

    /**
     *  Description of the Method
     */
    public synchronized void findRingAtomsAndBonds()
    {
        if (hasFlag(JOE_RINGFLAGS_MOL))
        {
            return;
        }

        setFlag(JOE_RINGFLAGS_MOL);

        JOEBitVec avisit = new JOEBitVec(numAtoms() + 1);
        JOEBitVec bvisit = new JOEBitVec(numAtoms() + 1);

        Vector path = new Vector();

        // of type int[1]
        path.setSize(numAtoms() + 1);

        for (int i = 0; i < path.size(); i++)
        {
            path.set(i, new int[]{Integer.MAX_VALUE});
        }

        for (int i = 1; i <= numAtoms(); i++)
        {
            if (!avisit.get(i))
            {
                JOERingSearch.findRings(this, path, avisit, bvisit, i, 0);
            }
        }
    }

    /**
     * Finds the Smallest Set of Smallest Rings (SSSR).
     *
     * @see #getSSSR()
     * @see #getSSSRIterator(Vector)
     * @todo Find correct reference: Frerejacque, Bull. Soc. Chim. Fr., 5, 1008 (1939)
     * @cite fig96
     */
    public synchronized void findSSSR()
    {
        if (hasSSSRPerceived())
        {
            return;
        }

        setSSSRPerceived();

        JOERing ring;

        //get frerejaque taking int account multiple possible spanning graphs
        int frj = JHM.determineFRJ(this);

        if (logger.isDebugEnabled())
        {
            logger.debug("frerejaque:" + frj);
        }

        if (frj != 0)
        {
            Vector vr = new Vector();

            // of type JOERing
            findRingAtomsAndBonds();

            Vector cbonds = new Vector();

            // of type JOEBond
            JOEBond bond;

            //restrict search for rings around closure bonds
            BondIterator bit = this.bondIterator();

            while (bit.hasNext())
            {
                bond = bit.nextBond();

                if (bond.isClosure())
                {
                    cbonds.add(bond);
                }
            }

            if (cbonds.size() != 0)
            {
                JOERingSearch rs = new JOERingSearch();

                //search for all rings about closures
                for (int i = 0; i < cbonds.size(); i++)
                {
                    rs.addRingFromClosure(this, (JOEBond) cbonds.get(i), 0);
                }

                //sort ring sizes from smallest to largest
                rs.sortRings();

                //full ring set - reduce to SSSR set
                rs.removeRedundant(frj);

                //store the SSSR set
                RingIterator rit = rs.getRingIterator();
                JOERing rtmp;

                while (rit.hasNext())
                {
                    rtmp = rit.nextRing();

                    ring = new JOERing(rtmp._path, numAtoms() + 1);

                    ring.setParent(this);
                    vr.add(ring);
                }

                //rs.WriteRings(); //for debugging only
            }

            //      System.out.println("JOE_RING_DATA "+getData(JOEDataType.JOE_RING_DATA)+" vr "+vr);
            if (!hasData(JOEDataType.JOE_RING_DATA))
            {
                addData(new JOERingData());
            }

            JOERingData rd = (JOERingData) getData(JOEDataType.JOE_RING_DATA);
            rd.setData(vr);

            //System.out.println("JOE_RING_DATA5 "+getData(JOEDataType.JOE_RING_DATA)+" vr "+vr);
        }
        else
        {
            if (!hasData(JOEDataType.JOE_RING_DATA))
            {
                addData(new JOERingData());
            }
        }
    }

    /**
     * Gets an iterator over the generic data elements of this molecule.
     *
     * There exist a lot of default data types which where defined in
             * {@link JOEDataType}. These data types are used for caching ring
             * searches and storing special data types like comments or virtual bonds.
             * Furthermore there exist the most important data type {@link JOEPairData}
             * for storing descriptor values. Read the {@link JOEDataType} description
             * for details.
             *
     * <p>
     * <ol>
     * <li>For getting all data elements the simple data iterator can be used.<br>
     * <blockquote><pre>
     * GenericDataIterator gdit = mol.genericDataIterator();
     * JOEGenericData genericData;
     * while(gdit.hasNext())
     * {
     *   genericData = gdit.nextGenericData();
     *
     * }
     * </pre></blockquote>
     * </li>
     *
     * <li>For getting all descriptor data elements a slightly modified iterator can be used.<br>
     * <blockquote><pre>
     * GenericDataIterator gdit = mol.genericDataIterator();
     * JOEGenericData genericData;
     * while(gdit.hasNext())
     * {
     *   genericData = gdit.nextGenericData();
     *   if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
     *   {
     *           JOEPairData pairData = (JOEPairData)genericData;
     *
     *   }
     * }
     * </pre></blockquote>
     * </li>
     *
     * <li>For getting all native value descriptors the simple data iterator can be used, also.
     * Because this is a little bit tricky, the {@link #nativeValueIterator()} would be
     * the recommended method for getting these values. Though the complex access
     * will be presented, because it's really instructive for understanding the
     * data access under JOELib.<br>
     * <blockquote><pre>
     * GenericDataIterator gdit = mol.genericDataIterator();
     * JOEGenericData genericData;
     * while(gdit.hasNext())
     * {
     *   genericData = gdit.nextGenericData();
     *   if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
     *   {
     *           JOEPairData pairData = (JOEPairData)genericData;
     *           // data must be parsed to check data type
     *           genericData = mol.getData(pairData.getAttribute(), true);
     *           pairData = (JOEPairData)genericData;
     *             if(JOEHelper.hasInterface(pairData, "NativeValue"))
     *             {
     *        double tmpDbl=((NativeValue) pairData).getDoubleNV();
     *
     *             }
     *   }
     * }
     * </pre></blockquote>
     * </li>
     * </ol>
     *
     * @return    the generic data iterator
     * @see #atomIterator()
     * @see #bondIterator()
     * @see #conformerIterator()
     * @see #getRingIterator()
     * @see #nativeValueIterator()
     * @see #getData(String)
     * @see #getData(String, boolean)
     * @see #addData(JOEGenericData)
     * @see #addData(JOEGenericData, boolean)
     * @see DescriptorHelper#molDescriptors(JOEMol)
     * @see JOEDataType
     * @see JOEGenericData
     */
    public GenericDataIterator genericDataIterator()
    {
        return genericData.genericDataIterator();
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean has2D()
    {
        boolean hasX;
        boolean hasY;
        boolean hasZ;
        JOEAtom atom;
        AtomIterator ait = this.atomIterator();

        hasX = hasY = false;
        hasZ = false;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (!hasX && (atom.x() != 0.0))
            {
                hasX = true;
            }

            if (!hasY && (atom.y() != 0.0))
            {
                hasY = true;
            }

            if (!hasZ && (atom.z() != 0.0))
            {
                hasZ = true;
            }

            if (hasX && hasY && !hasZ)
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean has3D()
    {
        boolean hasX;
        boolean hasY;
        boolean hasZ;
        JOEAtom atom;
        AtomIterator ait = this.atomIterator();

        hasX = hasY = hasZ = false;

        if (this._c == null)
        {
            return false;
        }

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (!hasX && (atom.x() != 0.0))
            {
                hasX = true;
            }

            if (!hasY && (atom.y() != 0.0))
            {
                hasY = true;
            }

            if (!hasZ && (atom.z() != 0.0))
            {
                hasZ = true;
            }

            if (hasX && hasY && hasZ)
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasAromaticCorrected()
    {
        return (hasFlag(JOE_AROM_CORRECTED_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasAromaticPerceived()
    {
        return (hasFlag(JOE_AROMATIC_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasAtomTypesPerceived()
    {
        return (hasFlag(JOE_ATOMTYPES_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasChainsPerceived()
    {
        return (hasFlag(JOE_CHAINS_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasChiralityPerceived()
    {
        return (hasFlag(JOE_CHIRALITY_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasClosureBondsPerceived()
    {
        return (hasFlag(JOE_CLOSURE_MOL));
    }

    /**
     *  Description of the Method
     *
     * @param  s  Description of the Parameter
     * @return    <tt>true</tt> if the generic attribute/value pair exists
     */
    public boolean hasData(String s)
    {
        return genericData.hasData(s);
    }

    /**
     *  Returns <tt>true</tt> if one ore more entries of the given <tt>JOEDataType
     *  </tt> exists.
     *
     * @param  dt  Description of the Parameter
     * @return     <tt>true</tt> if the generic attribute/value pair exists
     */
    public boolean hasData(JOEDataType dt)
    {
        return genericData.hasData(dt);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasHybridizationPerceived()
    {
        return (hasFlag(JOE_HYBRID_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasHydrogensAdded()
    {
        return (hasFlag(JOE_H_ADDED_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasImplicitValencePerceived()
    {
        return (hasFlag(JOE_IMPVAL_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasKekulePerceived()
    {
        return (hasFlag(JOE_KEKULE_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasNonZeroCoords()
    {
        JOEAtom atom;
        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (atom.getVector().notEquals(XYZVector.vZero))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasPartialChargesPerceived()
    {
        return (hasFlag(JOE_PCHARGE_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasRingAtomsAndBondsPerceived()
    {
        return (hasFlag(JOE_RINGFLAGS_MOL));
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean hasSSSRPerceived()
    {
        return (hasFlag(JOE_SSSR_MOL));
    }

    /**
     * Calculates the hashcode of a molecule using the methods <tt>AbstractDatabase.getHashcode</tt>
     * and <tt>AbstractDatabase.getSMILESHashcode</tt>.
     */
    public synchronized int hashCode()
    {
        if (hash != 0)
        {
            return hash;
        }

        // hashcode without cis/trans and chirality informations
        int hashCode = AbstractDatabase.getHashcode(this);

        // hashcode WITH cis/trans and chirality informations
        hashCode = (31 * hashCode) + AbstractDatabase.getSMILESHashcode(this);

        hash = hashCode;

        return hashCode;
    }

    /**
     * Increase modification counter.
     *
     * @see #decrementMod()
     * @see #getMod()
     * @see #beginModify()
     * @see #endModify()
     * @see #endModify(boolean)
     */
    public void incrementMod()
    {
        _mod++;
    }

    /**
     *  Description of the Method
     *
     * @param  atom  Description of the Parameter
     * @return       Description of the Return Value
     */
    public boolean insertAtom(JOEAtom atom)
    {
        beginModify();

        JOEAtom oeatom = createAtom();
        oeatom = atom;
        oeatom.setIdx(_natoms + 1);
        oeatom.setParent(this);

        //		if (_atom.size() == 0 || _natoms + 1 >= _atom.size())
        //		{
        //			_atom.setSize(_natoms + JOE_ATOM_INCREMENT);
        //
        //			//for (int j =(_natoms+1); j<_atom.size();j++) _atom.put(j) = null;
        //		}
        //_atom.set(_natoms, oeatom);
        _atom.add(oeatom);
        _natoms++;

        if (hasData(JOEDataType.JOE_VIRTUAL_BOND_DATA))
        {
            //add bonds that have been queued
            JOEVirtualBond vb;

            GenericDataIterator gdit = genericDataIterator();
            JOEGenericData genericData;
            Vector verase = new Vector();

            //JOEGenericData
            while (gdit.hasNext())
            {
                genericData = gdit.nextGenericData();

                if (genericData.getDataType().equals(JOEDataType.JOE_VIRTUAL_BOND_DATA))
                {
                    vb = (JOEVirtualBond) genericData;

                    if ((vb.getBgn() > _natoms) || (vb.getEnd() > _natoms))
                    {
                        continue;
                    }

                    if ((oeatom.getIdx() == vb.getBgn()) ||
                            (oeatom.getIdx() == vb.getEnd()))
                    {
                        addBond(vb.getBgn(), vb.getEnd(), vb.getOrder());
                        verase.add(genericData);
                    }
                }
            }

            if (verase.size() != 0)
            {
                deleteData(verase);
            }
        }

        endModify();

        return true;
    }

    /**
     *  Gets the compressed attribute of the <tt>JOEMol</tt> object
     *
     * @return    The compressed value
     */

    //	public boolean isCompressed()
    //	{
    //		return _compressed;
    //	}
    //  public void jacobi(double a[4][4], double d[], double v[4][4]);

    /**
     * Kekulizes the molecule.
     *
     * @return    <tt>true</tt> if successfull
     */
    public boolean kekulize()
    {
        if (numAtoms() > 255)
        {
            return false;
        }

        JOEBond bond;
        BondIterator bit = this.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isKSingle())
            {
                bond.setBO(1);
            }
            else if (bond.isKDouble())
            {
                bond.setBO(2);
            }
            else if (bond.isKTriple())
            {
                bond.setBO(3);
            }
        }

        return true;
    }

    /**
     *  Gets an iterator over native descriptor values (<tt>int</tt> or <tt>double</tt>)
     * of this molecule.
     *
     * <blockquote><pre>
     * NativeValueIterator nativeIt = mol.nativeValueIterator();
     * double value;
     * String descName;
     * while (nativeIt.hasNext())
     * {
     *   value = nativeIt.nextDouble();
     *   descName = nativeIt.actualName();
     *
     * }
     * </pre></blockquote>
     *
     * @return    The native value iterator
     * @see #atomIterator()
     * @see #bondIterator()
     * @see #conformerIterator()
     * @see #getRingIterator()
     * @see #genericDataIterator
     */
    public NativeValueIterator nativeValueIterator()
    {
        return new NativeValueIterator(this, genericData.genericDataIterator());
    }

    /**
     * Creates a new atom and adds it to molecule.
     *
     * @return    the new atom
     * @see #addAtom(JOEAtom)
     * @see #createAtom()
     */
    public JOEAtom newAtom()
    {
        beginModify();

        JOEAtom oeatom = createAtom();
        oeatom.setIdx(_natoms + 1);

        //System.out.println("create idx:"+oeatom.getIdx());
        //System.out.println("create cidx:"+oeatom.getCIdx());
        oeatom.setParent(this);

        //if (_atom.size() == 0 || _natoms + 1 >= _atom.size())
        //{
        //	//_atom.setSize(_natoms + JOE_ATOM_INCREMENT);
        //	_atom.ensureCapacity(_natoms + JOE_ATOM_INCREMENT);
        //	//JOEAtom atom;
        //	//for (j = 0, j+=(_natoms+1); j <= _atom.size(); j++) _atom.put(j) = null;
        //}
        //if ((_atom.size() - 1) >= _natoms)
        //{
        //	_atom.set(_natoms, oeatom);
        //}
        //else
        //{
        //	_atom.add(oeatom);
        //}
        _atom.add(oeatom);

        if (logger.isDebugEnabled())
        {
            logger.debug("Created new atom " + _atom.get(_natoms) +
                " with index " + oeatom.getIdx());
        }

        _natoms++;

        if (hasData(JOEDataType.JOE_VIRTUAL_BOND_DATA))
        {
            /*add bonds that have been queued        */
            JOEVirtualBond vb;

            GenericDataIterator gdit = genericDataIterator();
            JOEGenericData genericData;
            Vector verase = new Vector();

            // JOEGenericData
            while (gdit.hasNext())
            {
                genericData = gdit.nextGenericData();

                if (genericData.getDataType().equals(JOEDataType.JOE_VIRTUAL_BOND_DATA))
                {
                    vb = (JOEVirtualBond) genericData;

                    if ((vb.getBgn() > _natoms) || (vb.getEnd() > _natoms))
                    {
                        continue;
                    }

                    if ((oeatom.getIdx() == vb.getBgn()) ||
                            (oeatom.getIdx() == vb.getEnd()))
                    {
                        addBond(vb.getBgn(), vb.getEnd(), vb.getOrder());
                        verase.add(genericData);
                    }
                }
            }

            if (verase.size() != 0)
            {
                deleteData(verase);
            }
        }

        endModify();

        return oeatom;
    }

    /**
     * Returns and adds new residue informations for this molecule.
     *
     * @return    The residue informations.
     */
    public JOEResidue newResidue()
    {
        JOEResidue oeresidue = new JOEResidue();
        oeresidue.setIdx(_residue.size());
        _residue.add(oeresidue);

        return oeresidue;
    }

    //  public boolean compress()
    //  {
    //    int size = 0;
    //    byte buf[] = new byte[100000];
    //
    //    if (!_compressed && numAtoms() < 256)
    //    {
    //      writeBinary(buf, size, this);
    //
    //      if (size)
    //      {
    //        _compressed = true;
    //		    JOECompressData cd = new JOECompressData();
    // 		    cd.setData(buf,size);
    //        clear();
    //		    setData(cd);
    //      }
    //      else  _compressed = false;
    //    }
    //
    //    return _compressed;
    //  }
    //  public  boolean unCompress()
    //  {
    //    int size = 0;
    //    char buf[];
    //    char data[];
    //
    //    if (_compressed)
    //    {
    //  	  JOECompressData cd = (JOECompressData)getData(oeCompressData);
    //      data = cd.getData();
    //
    //      if (data != null)
    //      {
    //  		  size = cd.getSize();
    //        if (size > 0)
    //        {
    //          _compressed = false;
    //          buf = new byte[size];
    //          System.arraycopy(data,0,buf,0,size);
    //          clear();
    //          readBinary(buf,this,size);
    //          buf = null;
    //          return true;
    //        }
    //      }
    //    }
    //
    //    return false;
    //  }

    /**
     * Returns the number of atoms.
     *
     * @return    The number of atoms
     */
    public int numAtoms()
    {
        return (_natoms);
    }

    /**
     * Returns the number of bonds.
     *
     * @return    The number of bonds
     */
    public int numBonds()
    {
        return (_nbonds);
    }

    /**
     * Returns the number of conformers of this molecule.
     *
     * @return    The number of conformers of this molecule
     */
    public int numConformers()
    {
        return _vconf.size();
    }

    /**
     * Returns the number of heavy atoms.
     * That is the number of all atoms except hydrogen atoms.
     *
     * @return    The number of heavy atoms
     */
    public int numHvyAtoms()
    {
        JOEAtom atom;
        AtomIterator ait = this.atomIterator();
        int count = 0;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (!atom.isHydrogen())
            {
                count++;
            }
        }

        return count;
    }

    /**
     * Returns the number of residues.
     *
     * @return    The number of residues
     */
    public int numResidues()
    {
        return (_residue.size());
    }

    /**
     * Returns the number of rotatable bonds.
     *
     * @return    The number of rotatable bonds
     */
    public int numRotors()
    {
        JOEBond bond;
        BondIterator bit = this.bondIterator();
        int count = 0;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isRotor())
            {
                count++;
            }
        }

        return count;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean perceiveKekuleBonds()
    {
        if (hasKekulePerceived())
        {
            return true;
        }

        setKekulePerceived();

        JOEBond bond;

        //initialize kekule bonds
        boolean done = true;
        boolean badResonanceForm = false;
        boolean[] varo = new boolean[numAtoms() + 1];

        for (int i = 0; i < (numAtoms() + 1); i++)
        {
            varo[i] = false;
        }

        BondIterator bit = this.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            switch (bond.getBO())
            {
            case 2:
                bond.setKDouble();

                break;

            case 3:
                bond.setKTriple();

                break;

            case 5:
                bond.setKSingle();

                if (bond.isInRing())
                {
                    varo[bond.getBeginAtomIdx()] = true;
                    varo[bond.getEndAtomIdx()] = true;
                    done = false;
                }
                else
                {
                    badResonanceForm = true;
                }

                break;

            default:
                bond.setKSingle();

                break;
            }
        }

        if (badResonanceForm)
        {
            JHM.correctBadResonanceForm(this);
        }

        if (done)
        {
            return true;
        }

        //set the maximum valence for each aromatic atom
        JOEAtom atom;

        //set the maximum valence for each aromatic atom
        JOEAtom nbr;
        int[] maxv = new int[numAtoms() + 1];

        AtomIterator ait = this.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (varo[atom.getIdx()])
            {
                switch (atom.getAtomicNum())
                {
                case 6:
                    maxv[atom.getIdx()] = 4;

                    break;

                case 8:
                case 16:
                case 34:
                case 52:
                    maxv[atom.getIdx()] = 2;

                    break;

                case 7:
                case 15:
                case 33:
                    maxv[atom.getIdx()] = 3;

                    break;
                }

                //correct valence for formal charges
                if (atom.isCarbon())
                {
                    maxv[atom.getIdx()] -= Math.abs(atom.getFormalCharge());
                }
                else
                {
                    maxv[atom.getIdx()] += atom.getFormalCharge();
                }

                if (atom.isNitrogen() || atom.isSulfur())
                {
                    NbrAtomIterator nait = atom.nbrAtomIterator();

                    while (nait.hasNext())
                    {
                        nbr = nait.nextNbrAtom();

                        if (nbr.isOxygen() && (nait.actualBond().getBO() == 2))
                        {
                            maxv[atom.getIdx()] += 2;
                        }
                    }
                }
            }
        }

        boolean result = true;
        boolean[] used = new boolean[numAtoms() + 1];
        Vector va = new Vector();
        Vector curr = new Vector();
        Vector next = new Vector();
        ait.reset();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (varo[atom.getIdx()] && !used[atom.getIdx()])
            {
                va.clear();
                va.add(atom);
                curr.clear();
                curr.add(atom);
                used[atom.getIdx()] = true;

                while (curr.size() != 0)
                {
                    next.clear();

                    for (int k = 0; k < curr.size(); k++)
                    {
                        JOEAtom tmp = (JOEAtom) curr.get(k);
                        NbrAtomIterator nait = tmp.nbrAtomIterator();

                        while (nait.hasNext())
                        {
                            nbr = nait.nextNbrAtom();

                            if (varo[nbr.getIdx()] && !used[nbr.getIdx()])
                            {
                                used[nbr.getIdx()] = true;
                                next.add(nbr);
                                va.add(nbr);
                            }
                        }
                    }

                    // c++: curr = next;
                    curr.clear();

                    for (int nn = 0; nn < next.size(); nn++)
                    {
                        curr.add(next.get(nn));
                    }
                }

                //try it first without protonating aromatic nitrogens
                if (!JHM.expandKekule(this, va, new AtomIterator(va), maxv,
                            false) &&
                        !JHM.expandKekule(this, va, new AtomIterator(va), maxv,
                            true))
                {
                    result = false;
                }
            }
        }

        if (!result)
        {
            if (numAtoms() < 256)
            {
                StringBuffer smilesSB = new StringBuffer(1000);
                JOEMol2Smi m2s = new JOEMol2Smi();
                m2s.correctAromaticAmineCharge(this);
                m2s.createSmiString(this, smilesSB);

                String smiles = smilesSB.toString();

                // NEVER use the this.toString(IOTypeHolder.instance().getIOType("SMILES"))
                // here.
                //
                // THIS causes a endless recursive loop, if
                // the canonical-flag is set to true.
                // The SMILES writer clones and tries to kekulize, we know there
                // is an error, so the SMILES writer clones and tries to kekulize,
                // we know there is an error, so ... DEADLOOP
                //
                //String smiles=this.toString(IOTypeHolder.instance().getIOType("SMILES"));
                logger.warn("Kekulization error = " +
                    smiles.substring(0, smiles.length() - 1));
            }
            else
            {
                logger.warn("Kekulization error = " + getTitle());

                //exit(0);
            }
        }

        return result;
    }

    public synchronized int reHash()
    {
        hash = 0;

        return hashCode();
    }

    /**
     * @param  v  of type <tt>JOEAtom</tt>
     */
    public void renumberAtoms(Vector v)
    {
        if (empty())
        {
            return;
        }

        Vector va = (Vector) v.clone();

        //    va = v;
        JOEAtom atom;
        AtomIterator ait = new AtomIterator(va);

        if ((va.size() != 0) && (va.size() < numAtoms()))
        {
            //make sure all atoms are represented in the vector
            JOEBitVec bv = new JOEBitVec();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                //bv |= atom.getIdx();
                bv.set(atom.getIdx());
            }

            ait = this.atomIterator();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();

                if (!bv.get(atom.getIdx()))
                {
                    va.add(atom);
                }
            }
        }

        int j;

        int k;
        double[] c;
        double[] ctmp = new double[numAtoms() * 3];

        for (j = 0; j < numConformers(); j++)
        {
            c = getConformer(j);
            ait = new AtomIterator(va);
            k = 0;

            while (ait.hasNext())
            {
                atom = ait.nextAtom();
                System.arraycopy(c, atom.getCIdx(), ctmp, k * 3, 3);
                k++;
            }

            System.arraycopy(ctmp, 0, c, 0, 3 * numAtoms());
        }

        ait.reset();
        k = 1;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atom.setIdx(k);
            k++;
        }

        ctmp = null;
        _atom = va;

        //set the atom vector
    }

    /**
     * Reserves a initial capacity of atoms.
     *
     * @param  natoms  The number of atoms to reserve
     */
    public void reserveAtoms(int natoms)
    {
        if ((natoms != 0) && (_mod != 0))
        {
            _atom.ensureCapacity(natoms);
        }
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public ResidueIterator residueIterator()
    {
        return new ResidueIterator(_residue);
    }

    /**
     * @param  u  Description of the Parameter
     */
    public void rotate(final double[][] u)
    {
        int i;
        int j;
        int k;
        double[] m = new double[9];

        for (k = 0, i = 0; i < 3; i++)
        {
            for (j = 0; j < 3; j++)
            {
                m[k++] = u[i][j];
            }
        }

        for (i = 0; i < numConformers(); i++)
        {
            rotate(m, i);
        }
    }

    /**
     * @param  m  of size 9.
     */
    public void rotate(final double[] m)
    {
        for (int i = 0; i < numConformers(); i++)
        {
            rotate(m, i);
        }
    }

    /**
     * @param  m      of size 9.
     * @param  nconf  Description of the Parameter
     */
    public void rotate(final double[] m, int nconf)
    {
        int i;
        int size;
        double x;
        double y;
        double z;
        double[] c = (nconf == JOE_CURRENT_CONFORMER) ? _c : getConformer(nconf);

        size = numAtoms();

        for (i = 0; i < size; i++)
        {
            x = c[i * 3];
            y = c[(i * 3) + 1];
            z = c[(i * 3) + 2];
            c[i * 3] = (m[0] * x) + (m[1] * y) + (m[2] * z);
            c[(i * 3) + 1] = (m[3] * x) + (m[4] * y) + (m[5] * z);
            c[(i * 3) + 2] = (m[6] * x) + (m[7] * y) + (m[8] * z);
        }
    }

    //public boolean isOuterBond( JOEBond b)
    //  {
    //    Vector sssRings = getSSSR();
    //
    //    JOERing ring=null;
    //    JOEAtom atom;
    //    boolean ringFound=false;
    //    JOEBond bond;
    //    for (int i = 0; i < sssRings.size(); i++)
    //    {
    //      ring = (JOERing) sssRings.get(i);
    //      BondIterator bit = this.bondIterator();
    //      while(bit.hasNext())
    //      {
    //        bond = bit.nextBond();
    //        if(ring.isMember(bond))
    //        {
    //          ringFound=true;
    //        }
    //      }
    //      if(ringFound)break;
    //    }
    //
    //    if(ringFound)
    //    {
    //      XYZVector c=new XYZVector();
    //      XYZVector n=new XYZVector();
    //      XYZVector n1=new XYZVector();
    //      ring.findCenterAndNormal(c, n, n1);
    //    //    double x1;// a1_x
    //    //    double y1;// a1_y
    //    //    double x2;// a2_x
    //    //    double y2;// a2_y
    //    //    x1 = tx2  - tx1;
    //    //    y1 = ty2  - ty1;
    //    //    x2 = r.tx - tx1;
    //    //    y2 = r.ty - ty1;
    //    //
    //    //    return ( (x1 * y2) - (y1 * x2) >= 0);   // right side
    //    }
    //    return false;
    //
    //  }
    //  public final int numPoses() {return ((_pose.size()==0)?0:_pose.size());}
    //
    //  /**
    //   * Returns the number of the current pose.
    //   * @return The number of the current pose.  If no poses are present 0 is returned.
    //   */
    //  public  int currentPoseIndex()
    //  {
    //    if (_pose.size()==0) return 0;
    //    return _cur_pose_idx;
    //  }
    //  /**
    //   *  Deletes all pose information for the OEMol.
    //   */
    //  public void deletePoses()
    //  {
    //    //If there are no poses don't do anything
    //    if (_pose.size()==0) return;
    //
    //    //If the atom coordinate array is pointing to the pose
    //    //array change it to point to the 1st conformer
    //    if (_c == _xyz_pose && _c != null) {
    //        if (_vconf.size()!=0) _c = _vconf[0];
    //        else _c = null;
    //      }
    //
    //    //Free the pose coordinate array
    //    if (_xyz_pose!=null) _xyz_pose = null;
    //
    //    _pose.clear();
    //
    //    _cur_pose_idx=0;
    //
    //  }
    //
    //  /**
    //   *  Deletes all pose information for the OEMol.
    //   *
    //   *  Deletes all pose information for the OEMol. Deletes all pose information
    //   *  for the OEMol. Deletes all pose information for the OEMol. Deletes all
    //   *  pose information for the OEMol. Deletes all pose information for the
    //   *  OEMol. Deletes a specified pose.
    //   *
    //   */
    //  public void deletePose( int i)
    //  {
    //    //Check that a valid pose is being deleted.
    //    if (i >= numPoses()) return;
    //
    //    //If this is the last pose just call DeletePoses.
    //    if (numPoses() == 1) {deletePoses(); return;}
    //
    //    //Delete the pose
    //    _pose.remove(i);
    //
    //    return;
    //  }
    //
    //  /**
    //   *  Deletes all pose information for the <tt>JOEMol</tt>.
    //   */
    //  public void addPose(JOEPose pose)
    //  {
    //    //Check that the pose references a valid conformer
    //    if (pose.conformerNum() >= (int) numConformers()) {
    //        throw new Exception("WARNING! Pose does not reference a valid conformer");
    //        logger.error("WARNING! OEMol::AddPose(OEPose)  "+
    //                           "Pose references invalid conformer");
    //        return;
    //      }
    //    _pose.put(pose);
    //  }
    //
    //  /**
    //   *  Deletes all pose information for the <tt>JOEMol</tt>.
    //   */
    //  public void setPoses(Vector poses)
    //  {
    //    //Check that all the poses reference valid conformers
    //    int i;
    //    for (i=0 ; i<poses.size() ; i++)
    //    {
    //      if (((JOEPose)poses.get(i)).conformerNum() >= (int) numConformers())
    //      {
    //        throw new Exception("WARNING! Poses do not reference valid conformers");
    //        logger.error("Pose references invalid conformer");
    //        return;
    //      }
    //    }
    //
    //    //Set the poses
    //    _pose = poses;
    //
    //    //If the atom coordinate array was looking at poses have it look at the first new pose
    //    if (_pose.size()!=0) {
    //        if (_c == _xyz_pose) setPose(0);
    //      }
    //    else if (_vconf.size()!=0) setConformer(0);
    //    else _c = null;
    //  }
    //
    //  /**
    //   *  Deletes all pose information for the <tt>JOEMol</tt>.
    //   *
    //   */
    //  public void setPose( int i)
    //  {
    //    //check that the pose is valid
    //    if (i >= numPoses())
    //    {
    //      throw new Exception("WARNING! Invalid pose specified");
    //      logger.error("Invalid pose specified!");
    //      return;
    //    }
    //
    //    //Check that the pose references a valid conformer
    //    if (((JOEPose)_pose.get(i)).conformerNum() >= (int) numConformers())
    //    {
    //      throw new Exception("WARNING! Pose references invalid conformer");
    //      logger.error("Pose references invalid conformer");
    //      return;
    //    }
    //
    //    //Make sure the pose coordinate array has memory
    //    if (_xyz_pose==null) _xyz_pose = new double [3*numAtoms()];
    //
    //    //Generate coordinates for the pose
    //    int ii;
    //    double xyz_conf[] = getConformer( ((JOEPose)_pose.get(i)).conformerNum() );
    //    for (ii=0 ; ii<3*numAtoms() ; ii++) _xyz_pose[ii] = xyz_conf[ii];
    //    ((JOEPose)_pose.get(i)).coordTrans().transform(_xyz_pose, numAtoms());
    //
    //    //Point the atom coordinate pointer to the coordinates of the pose
    //    _c = _xyz_pose;
    //
    //    //Remember current pose
    //    _cur_pose_idx = i;
    //
    //    return;
    //  }
    //
    //  /**
    //   *  Deletes all pose information for the <tt>JOEMol</tt>.
    //   *
    //   */
    //  public void getPoseCoordinates( int i, double xyz[])
    //  {
    //    //check that the pose is valid
    //    if (i >= numPoses())
    //    {
    //      throw new Exception("WARNING! Invalid pose specified");
    //      logger.error("Invalid pose specified!");
    //      return;
    //    }
    //
    //    //Check that the pose references a valid conformer
    //    if ( ((JOEPose)_pose.get(i)).conformerNum() >= (int) numConformers())
    //    {
    //      throw new Exception("WARNING! Pose references invalid conformer");
    //      logger.error("Pose references invalid conformer");
    //      return;
    //    }
    //
    //    //Check that xyz is not NULL
    //    if (xyz==null) return;
    //
    //    //Generate coordinates for the pose
    //    int ii;
    //    double xyz_conf[] = getConformer(((JOEPose)_pose.get(i)).conformerNum());
    //    for (ii=0 ; ii<3*numAtoms() ; ii++) xyz[ii] = xyz_conf[ii];
    //    ((JOEPose)_pose.get(i)).coordTrans().transform(xyz, numAtoms());
    //
    //    return;
    //  }
    //
    //  /**
    //   *  Deletes all pose information for the <tt>JOEMol</tt>.
    //   */
    //  public JOEPose getPose( int i)
    //  {
    ///*
    //    //check that the pose is valid
    //    if (i >= numPoses()) {
    //        throw new Exception("WARNING! Invalid pose specified");
    //        logger.error("Invalid pose specified!");
    //        JOEPose pose;
    //        return pose;
    //      }
    //*/
    //    return _pose[i];
    //  }
    //
    //  /**
    //   *  Deletes all pose information for the <tt>JOEMol</tt>.
    //   */
    //  public void changePosesToConformers()
    //  {
    //    //If there aren't any poses don't do anything
    //    if (_pose.size()==0) return;
    //
    //    //Generate the coordinates of all the poses
    //    int i;
    //    double[][] dconf = new double[numPoses()][];
    //
    //    for (i=0 ; i<numPoses() ; i++) {
    //        dconf[i] = new double [3*numAtoms()];
    //        getPoseCoordinates(i,dconf[i]);
    //      }
    //
    //    //Now that we have the coordinates clear the pose info for JOEMol
    //    deletePoses();
    //
    //    //Assign the pose coordinates to the conformers
    //    setConformers(dconf);
    //
    //    return;
    //  }
    //misc bond functions
    //  public void assignResidueBonds(JOEBitVec &);

    /**
     *  Clones molecule without additional data (e.g. descriptor data).
     *
     * @param  source  Description of the Parameter
     * @return         Description of the Return Value
     */
    public JOEMol set(final JOEMol source)
    {
        return set(source, false, null);
    }

    /**
     *  Clones molecule.
     * Missing descriptor entries, defined in <tt>descriptors</tt> will be
     * ignored.
     *
     * @param  source     Description of the Parameter
     * @param  cloneDesc  clones the PairData descriptors if <tt>true</tt>
     * @param  descriptors  descriptors to clone. If <tt>null</tt> all descriptors are cloned
     * @return            The new molecule
     */
    public JOEMol set(final JOEMol source, boolean cloneDesc,
        String[] descriptors)
    {
        return set(source, cloneDesc, descriptors, false);
    }

    /**
     *  Clones molecule.
     *
     * @param  source     Description of the Parameter
     * @param  cloneDesc  clones the PairData descriptors if <tt>true</tt>
     * @param  descriptors  descriptors to clone. If <tt>null</tt> all descriptors are cloned
     * @param  addDescIfNotExist  Missing descriptor entries, defined in <tt>descriptors</tt>
     *          will be automatically added to the molecule if <tt>true</tt>
     * @return            The new molecule
     */
    public JOEMol set(final JOEMol source, boolean cloneDesc,
        String[] descriptors, boolean addDescIfNotExist)
    {
        JOEMol src = source;
        JOEAtom atom;
        JOEBond bond;
        AtomIterator ait = source.atomIterator();
        BondIterator bit = source.bondIterator();

        clear();
        beginModify();

        _atom.ensureCapacity(src.numAtoms());
        _bond.ensureCapacity(src.numBonds());

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            addAtom(atom);
        }

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            addBond(bond);
        }

        _itype = src.getInputType();
        _otype = src.getOutputType();

        this._title = src.getTitle();
        this._energy = src.getEnergy();

        endModify();

        //Copy Residue information
        int numRes = src.numResidues();

        if (numRes > 0)
        {
            int k;
            JOEResidue src_res = null;
            JOEResidue res = null;
            JOEAtom src_atom = null;
            atom = null;

            for (k = 0; k < numRes; k++)
            {
                res = newResidue();
                src_res = src.getResidue(k);
                res.setName(src_res.getName());
                res.setNum(src_res.getNum());
                res.setChain(src_res.getChain());
                res.setChainNum(src_res.getChainNum());

                AtomIterator ait2 = src_res.atomIterator();

                while (ait2.hasNext())
                {
                    src_atom = ait2.nextAtom();
                    atom = getAtom(src_atom.getIdx());
                    res.addAtom(atom);
                    res.setAtomID(atom, src_res.getAtomID(src_atom));
                    res.setHetAtom(atom, src_res.isHetAtom(src_atom));
                    res.setSerialNum(atom, src_res.getSerialNum(src_atom));
                }
            }
        }

        //Copy conformer information
        if (src.numConformers() > 1)
        {
            int k;
            int l;
            Vector conf = new Vector();
            double[] xyz = null;

            for (k = 0; k < src.numConformers(); k++)
            {
                xyz = new double[3 * src.numAtoms()];

                for (l = 0; l < (int) (3 * src.numAtoms()); l++)
                {
                    xyz[l] = src.getConformer(k)[l];
                }

                conf.add(xyz);
            }

            setConformers(conf);
        }

        //    //Copy rotamer list
        //    JOERotamerList rml = (JOERotamerList) src.getData(oeRotamerList);
        //    //if (rml!=null) {System.out.println("DEBUG : OEMol assignment operator.  Source HAS RotamerList");}
        //    //else  {System.out.println("DEBUG : OEMol assignment operator.  Source does NOT have RotamerList");}
        //    if (rml!=null && rml.numAtoms() == src.numAtoms()) {
        //        //Destroy old rotamer list if necessary
        //        if ((JOERotamerList)getData(oeRotamerList)) {
        //            deleteData(oeRotamerList);
        //          }
        //
        //        //Set base coordinates
        //        JOERotamerList cp_rml = new JOERotamerList();
        //        int k,l;
        //        Vector bc;
        //        double[] c  = null;
        //        double[] cc = null;
        //        for (k=0 ; k<rml.numBaseCoordinateSets() ; k++) {
        //            c = new double [3*rml.numAtoms()];
        //            cc = rml.getBaseCoordinateSet(k);
        //            for (l=0 ; l<3*rml.numAtoms() ; l++) c[l] = cc[l];
        //            bc.put(c);
        //          }
        //        if (rml.numBaseCoordinateSets()) cp_rml.setBaseCoordinateSets(bc,rml.numAtoms());
        //
        //        //Set reference array
        //        char[] ref = new char [rml.numRotors()*4];
        //        rml.getReferenceArray(ref);
        //        cp_rml.setup( this, ref, rml.numRotors() );
        //        delete [] ref;
        //
        //        //Set Rotamers
        //        char[] rotamers = new char [(rml.numRotors()+1)*rml.numRotamers()];
        //
        //        RotamerIterator rit = rotamerIterator();
        //        int idx=0;
        //        char[] _rotamers;
        //
        //        while(rit.hasNext())
        //        {
        //          _rotamers = rit.nextRotamer();
        //          //memcpy(&rotamers[idx], (const char*)*kk, sizeof( char)*(rml.numRotors()+1));
        //          //idx += sizeof( char)*(rml.numRotors()+1);
        //          System.arraycopy(kk, 0, rotamers, idx, rml.numRotors()+1);
        //          idx += rml.numRotors()+1;
        //        }
        //
        //        cp_rml.addRotamers( rotamers, rml.numRotamers());
        //        delete [] rotamers;
        //        setData(cp_rml);
        //      }
        //Copy pose information
        //    _pose = src._pose.clone();
        //    if (_pose.size()!=0) setPose(src.currentPoseIndex());
        // copy simple pair data entries
        if (cloneDesc)
        {
            // clone all descriptor values
            if (descriptors == null)
            {
                GenericDataIterator gdit = src.genericDataIterator();
                JOEPairData pairData;
                JOEGenericData genericData;

                while (gdit.hasNext())
                {
                    genericData = gdit.nextGenericData();

                    if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                    {
                        pairData = (JOEPairData) genericData;

                        JOEPairData dp = new JOEPairData();
                        dp.setAttribute(genericData.getAttribute());

                        //          dp.setValue(((JOEPairData) genericData).toString(getInputType()));
                        Object obj = pairData.getValue();

                        if (JOEHelper.hasInterface(obj, "DescResult"))
                        {
                            dp.setValue(((DescResult) obj).clone());
                        }
                        else
                        {
                            dp.setValue(((JOEPairData) genericData).toString(
                                    getInputType()));
                        }

                        this.addData(dp);
                    }
                }
            }

            // clone only given descriptor values
            else
            {
                int s = descriptors.length;
                JOEPairData pairData;
                JOEGenericData genericData;

                for (int i = 0; i < s; i++)
                {
                    genericData = src.getData(descriptors[i]);

                    // descriptor to clone
                    if (genericData == null)
                    {
                        if (addDescIfNotExist)
                        {
                            // If  not available we'll try to calculate this descriptor
                            try
                            {
                                DescriptorHelper.descFromMol(this,
                                    descriptors[i], true);
                            }
                             catch (DescriptorException de)
                            {
                                // that's a serious problem the user should know
                                logger.error("Descriptor '" + descriptors[i] +
                                    "' was not cloned, because it is not available" +
                                    " and could not be calculated in" +
                                    getTitle() + ".");

                                continue;
                            }

                            genericData = src.getData(descriptors[i]);
                        }
                        else
                        {
                            // be quiet, if descriptors to clone are not available.
                            // Often, the descriptors to clone are only defined
                            // for speeding up the cloning process in some
                            // descriptor calculation methods.
                            //							logger.warn(
                            //								"Descriptor '"
                            //									+ descriptors[i]
                            //									+ "' was not cloned, because it is not available"
                            //									+ " in"
                            //									+ getTitle()
                            //									+".");
                            continue;
                        }
                    }

                    if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                    {
                        pairData = (JOEPairData) genericData;

                        JOEPairData dp = new JOEPairData();
                        dp.setAttribute(genericData.getAttribute());

                        //          dp.setValue(((JOEPairData) genericData).toString(getInputType()));
                        Object obj = pairData.getValue();

                        if (JOEHelper.hasInterface(obj, "DescResult"))
                        {
                            dp.setValue(((DescResult) obj).clone());
                        }
                        else
                        {
                            dp.setValue(((JOEPairData) genericData).toString(
                                    getInputType()));
                        }

                        this.addData(dp);
                    }
                }
            }
        }

        return this;
    }

    /**
     *  Description of the Method
     */
    public void sortBonds()
    {
        //  bool CompareBonds(const OEBond *a,const OEBond *b)
        //  {
        //    if (a->GetBeginAtomIdx() == b->GetBeginAtomIdx())
        //      return(a->GetEndAtomIdx() < b->GetEndAtomIdx());
        //    return(a->GetBeginAtomIdx() < b->GetBeginAtomIdx());
        //  }
        //    sort(_bond.begin(),_bond.end(),CompareBonds);
        QuickInsertSort sorting = new QuickInsertSort();
        BondComparator bondComparator = new BondComparator();
        sorting.sort(_bond, bondComparator);
    }

    /**
     * Deletes all atoms except for the largest contiguous fragment.
     *
     * @return    <tt>true</tt> if all smaller contiguous fragments were deleted
     * @see #contiguousFragments(Vector)
     */
    public boolean stripSalts()
    {
        Vector cfl = new Vector();

        //int vector
        int[] max;

        //vector of int vector
        //System.out.println("contig");
        contiguousFragments(cfl);

        //System.out.println("strip1");
        if ((cfl.size() == 0) || (cfl.size() == 1))
        {
            return (false);
        }

        //System.out.println("strip2");
        max = (int[]) cfl.get(0);

        for (int i = 0; i < cfl.size(); i++)
        {
            if (max.length < ((int[]) cfl.get(i)).length)
            {
                max = (int[]) cfl.get(i);
            }
        }

        Vector delatoms = new Vector();

        // atom vector
        for (int i = 0; i < cfl.size(); i++)
        {
            if (cfl.get(i) != max)
            {
                for (int j = 0; j < ((int[]) cfl.get(i)).length; j++)
                {
                    delatoms.add(getAtom(((int[]) cfl.get(i))[j]));

                    //System.out.println("remove " + i + ": atom " + j);
                }
            }
        }

        if (delatoms.size() != 0)
        {
            int tmpflags = _flags & (~(JOE_SSSR_MOL));
            beginModify();

            for (int k = 0; k < delatoms.size(); k++)
            {
                //System.out.println("strip: delete atom " + ((JOEAtom) delatoms.get(k)).getIdx()+" atom "+delatoms.get(k));
                deleteAtom((JOEAtom) delatoms.get(k));
            }

            endModify();
            _flags = tmpflags;
        }

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  conf  Description of the Parameter
     * @param  rmat  Description of the Parameter
     */
    public void toInertialFrame(int conf, double[] rmat)
    {
        int i;
        int count = 0;
        double x;
        double y;
        double z;
        double[] center = new double[3];
        double[][] m = new double[3][3];

        //???????
        //for (i = 0;i < 3;i++) memset(&m[i],'\0',sizeof(double)*3);
        //memset(center,'\0',sizeof(double)*3);
        setConformer(conf);

        JOEAtom atom = null;
        AtomIterator ait = this.atomIterator();

        //find center of mass
        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (!atom.isHydrogen())
            {
                center[0] += atom.x();
                center[1] += atom.y();
                center[2] += atom.z();
                count++;
            }
        }

        center[0] /= (double) count;
        center[1] /= (double) count;
        center[2] /= (double) count;

        //calculate inertial tensor
        ait.reset();

        while (ait.hasNext())
        {
            if (!atom.isHydrogen())
            {
                x = atom.x() - center[0];
                y = atom.y() - center[1];
                z = atom.z() - center[2];

                m[0][0] += ((y * y) + (z * z));
                m[0][1] -= (x * y);
                m[0][2] -= (x * z);
                m[1][0] -= (x * y);
                m[1][1] += ((x * x) + (z * z));
                m[1][2] -= (y * z);
                m[2][0] -= (x * z);
                m[2][1] -= (y * z);
                m[2][2] += ((x * x) + (y * y));
            }
        }

        /* find rotation matrix for moment of inertia   */

        // oe_make_rmat(m,rmat);

        /*
         *double v[3][3];
         *jacobi3x3(m,v);
         *XYZVector v1,v2,v3,r1,r2;
         *r1.Set(v[0][0],v[1][0],v[2][0]);
         *r2.Set(v[0][1],v[1][1],v[2][1]);
         *v3 = cross(r1,r2); v3 = v3.normalize();
         *v2 = cross(v3,r1); v2 = v2.normalize();
         *v1 = cross(v2,v3); v1 = v1.normalize();
         *double rmat[9];
         *rmat[0] = v1.x(); rmat[1] = v1.y(); rmat[2] = v1.z();
         *rmat[3] = v2.x(); rmat[4] = v2.y(); rmat[5] = v2.z();
         *rmat[6] = v3.x(); rmat[7] = v3.y(); rmat[8] = v3.z();
         */
        /* rotate all coordinates   */
        double[] c = getConformer(conf);

        for (i = 0; i < numAtoms(); i++)
        {
            x = c[i * 3] - center[0];
            y = c[(i * 3) + 1] - center[1];
            z = c[(i * 3) + 2] - center[2];
            c[i * 3] = (x * rmat[0]) + (y * rmat[1]) + (z * rmat[2]);
            c[(i * 3) + 1] = (x * rmat[3]) + (y * rmat[4]) + (z * rmat[5]);
            c[(i * 3) + 2] = (x * rmat[6]) + (y * rmat[7]) + (z * rmat[8]);
        }
    }

    /**
     *  Description of the Method
     */
    public void toInertialFrame()
    {
        double[] m = new double[9];

        for (int i = 0; i < numConformers(); i++)
        {
            toInertialFrame(i, m);
        }
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String toString()
    {
        return toString(getOutputType(), true);
    }

    /**
     *  Description of the Method
     *
     * @param  type  Description of the Parameter
     * @return       Description of the Return Value
     */
    public String toString(IOType type)
    {
        return toString(type, true);
    }

    /**
     *  Description of the Method
     *
     * @param  writeDescriptors  Description of the Parameter
     * @return                   Description of the Return Value
     */
    public String toString(boolean writeDescriptors)
    {
        return toString(getOutputType(), writeDescriptors);
    }

    /**
     *  Description of the Method
     *
     * @param  type              Description of the Parameter
     * @param  writeDescriptors  Description of the Parameter
     * @return                   Description of the Return Value
     */
    public String toString(IOType type, boolean writeDescriptors)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(10000);

        if (type == null)
        {
            logger.warn("Output type not defined, using default '" +
                DEFAULT_IO_TYPE.getName() + "'.");
            type = DEFAULT_IO_TYPE;
        }

        MoleculeFileType writer = null;

        try
        {
            writer = JOEFileFormat.getMolWriter(baos, type);

            if (!writer.writeable())
            {
                logger.warn(type.getRepresentation() + " is not writeable.");

                return null;

                //                System.exit(1);
            }

            if (!writeDescriptors &&
                    JOEHelper.hasInterface(writer, "PropertyWriter"))
            {
                ((PropertyWriter) writer).write(this, null, false, null);
            }
            else
            {
                writer.write(this, null);
            }
        }
         catch (IOException ex)
        {
            return null;

            //            return new String("No molecule output possible.");
        }
         catch (MoleculeIOException ex)
        {
            return null;

            //            return new String("No molecule output possible.");
        }
         catch (Exception ex)
        {
            ex.printStackTrace();

            return null;

            //            System.exit(1);
        }

        return baos.toString();
    }

    /**
     *  Description of the Method
     *
     * @param  v  Description of the Parameter
     */
    public void translate(final XYZVector v)
    {
        for (int i = 0; i < numConformers(); i++)
        {
            translate(v, i);
        }
    }

    /**
     *  Description of the Method
     *
     * @param  v      Description of the Parameter
     * @param  nconf  Description of the Parameter
     */
    public void translate(final XYZVector v, int nconf)
    {
        int i;
        int size;
        double x;
        double y;
        double z;
        double[] c = (nconf == JOE_CURRENT_CONFORMER) ? _c : getConformer(nconf);

        x = v.x();
        y = v.y();
        z = v.z();
        size = numAtoms();

        for (i = 0; i < size; i++)
        {
            c[i * 3] += x;
            c[(i * 3) + 1] += y;
            c[(i * 3) + 2] += z;
        }
    }

    /**
     *  Description of the Method
     */
    public void unsetAromaticPerceived()
    {
        _flags &= (~(JOE_AROMATIC_MOL));
    }

    /**
     *  Description of the Method
     */
    public void unsetImplicitValencePerceived()
    {
        _flags &= (~(JOE_IMPVAL_MOL));
    }

    /**
     *  Description of the Method
     */
    public void unsetPartialChargesPerceived()
    {
        _flags &= (~(JOE_PCHARGE_MOL));
    }

    /**
     *  Sets a molecule flag for this <tt>JOEMol</tt> object.
     *
     * @param  flag  The new flag value
     */
    protected void setFlag(int flag)
    {
        _flags |= flag;
    }

    /**
     * Gets iterator for the Smallest Set of Smallest Rings (SSSR).
     *
     * @param  rv  Description of the Parameter
     * @return     The sSSRIterator value
     * @see #findSSSR()
     * @cite fig96
     */
    protected RingIterator getSSSRIterator(Vector rv)
    {
        return new RingIterator(rv);
    }

    /**
     *  Has this molecules set flags.
     *
     * @param  flag  molecule flag
     * @return       <tt>true</tt> if the flag was set
     */
    protected boolean hasFlag(int flag)
    {
        return (((_flags & flag) != 0) ? true : false);
    }

    /**
     *  Description of the Method
     *
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     * @return    Description of the Return Value
     */
    private boolean sortAtomZ(final AtomZPos a, final AtomZPos b)
    {
        return (a.z < b.z);
    }

    /**
     *  Description of the Method Description of the Method Description of the
     *  Method Description of the Method Description of the Method
     *
     * @param  a  Description of the Parameter
     * @param  b  Description of the Parameter
     * @return    Description of the Return Value
     * @return    Description of the Return Value
     */
    private boolean sortVVInt(final Vector a, final Vector b)
    {
        return (a.size() > b.size());
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
