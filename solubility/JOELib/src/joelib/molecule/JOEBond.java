///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEBond.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.26 $
//            $Date: 2004/07/25 20:43:23 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule;

import java.util.Vector;

import joelib.data.JOEAromaticTyper;
import joelib.data.JOEElementTable;

import joelib.math.XYZVector;

import joelib.util.JOEBitVec;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;
import joelib.util.iterator.NbrAtomIterator;


/**
 *  Bond representation.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.26 $, $Date: 2004/07/25 20:43:23 $
 */
public class JOEBond implements Cloneable, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Bond flag: is aromatic bond
     */
    public final static int JOE_AROMATIC_BOND = (1 << 1);

    /**
     *  Bond flag: is wedge bond
     */
    public final static int JOE_WEDGE_BOND = (1 << 2);

    /**
     *  Bond flag: is hash bond
     */
    public final static int JOE_HASH_BOND = (1 << 3);

    /**
     *  Bond flag: is ring bond
     */
    public final static int JOE_RING_BOND = (1 << 4);

    /**
     *  Bond flag: is torup bond
     */
    public final static int JOE_TORUP_BOND = (1 << 5);

    /**
     *  Bond flag: is tordown bond
     */
    public final static int JOE_TORDOWN_BOND = (1 << 6);

    /**
     *  Bond flag: is single bond
     */
    public final static int JOE_KSINGLE_BOND = (1 << 7);

    /**
     *  Bond flag: is double bond
     */
    public final static int JOE_KDOUBLE_BOND = (1 << 8);

    /**
     *  Bond flag: is triple bond
     */
    public final static int JOE_KTRIPLE_BOND = (1 << 9);

    /**
     *  Bond flag: is closure bond
     */
    public final static int JOE_CLOSURE_BOND = (1 << 10);

    /**
     *  Bond order: aromatic bond
     */
    public final static int JOE_AROMATIC_BOND_ORDER = 5;

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     * Begin atom
     */
    protected JOEAtom _bgn;

    /**
     * End atom
     */
    protected JOEAtom _end;

    /**
     * Parent molecule
     */
    protected JOEMol _parent;

    /**
     * Bond order
     */
    protected char _order;

    /**
     * Bond flags
     */
    protected int _flags;

    /**
     * Bond index
     */
    protected int _idx;
    private int hash = 0;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the <tt>JOEBond</tt> object
     */
    public JOEBond()
    {
        //not necessary
        //clear();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Clone the <tt>JOEBond</tt> <tt>src</tt> to the <tt>JOEBond</tt> <tt>to
     *  </tt>.
     *
     * @param  src  the source <tt>JOEBond</tt>
     * @param  to   the destination <tt>JOEBond</tt>
     * @return      the destination <tt>JOEBond</tt>
     */
    public static Object clone(JOEBond src, JOEBond to)
    {
        // not necessary
        //to.clear();
        to._idx = src._idx;
        to._order = src._order;
        to._flags = src._flags;
        to._bgn = src._bgn;
        to._end = src._end;

        return to;
    }

    /**
     * Gets the bond order for this bond.
     *
     * Please remember that you can check the aromaticity also via the
     * aromaticity flag, which is the better way.
     *
     * The JOEBond.JOE_AROMATIC_BOND_ORDER which can be assigned to the bond
     * order is only needed for some awkward import/export methods.
     *
     * Please remember that the aromaticity typer JOEAromaticTyper.assignAromaticFlags(JOEMol)
     * assign ONLY aromaticity flags and NOT the internal aromatic bond order JOEBond.JOE_AROMATIC_BOND_ORDER.
     *
     * @return    The bond order
     */
    public final int getBO()
    {
        return ((int) _order);
    }

    /**
     *  Gets the beginAtomIdx attribute of the <tt>JOEBond</tt> object
     *
     * @return    The beginAtomIdx value
     */
    public final int getBeginAtomIdx()
    {
        return (_bgn.getIdx());
    }

    /**
     * Gets the bond order for this bond.
     *
     * Please remember that you can check the aromaticity also via the
     * aromaticity flag, which is the better way.
     *
     * The JOEBond.JOE_AROMATIC_BOND_ORDER which can be assigned to the bond
     * order is only needed for some awkward import/export methods.
     *
     * Please remember that the aromaticity typer JOEAromaticTyper.assignAromaticFlags(JOEMol)
     * assign ONLY aromaticity flags and NOT the internal aromatic bond order JOEBond.JOE_AROMATIC_BOND_ORDER.
     *
     * @return    The bond order
     */
    public final int getBondOrder()
    {
        return ((int) _order);
    }

    /**
     *  Gets the endAtomIdx attribute of the <tt>JOEBond</tt> object
     *
     * @return    The endAtomIdx value
     */
    public final int getEndAtomIdx()
    {
        return (_end.getIdx());
    }

    /**
     *  Gets the flags attribute of the <tt>JOEBond</tt> object
     *
     * @return    The flags value
     */
    public final int getFlags()
    {
        return (_flags);
    }

    /**
     *  Gets the idx attribute of the <tt>JOEBond</tt> object
     *
     * @return    The idx value
     */
    public final int getIdx()
    {
        return (_idx);
    }

    /**
     *  Gets the length of the <tt>JOEBond</tt> object.
     *
     * @return    The euclidian distance between start and end atom
     */
    public final double getLength()
    {
        double d2 = 0.0;
        double xx;
        double yy;
        double zz;
        JOEAtom begin = getBeginAtom();
        JOEAtom end = getEndAtom();

        xx = begin.getX() - end.getX();
        yy = begin.getY() - end.getY();
        zz = begin.getZ() - end.getZ();
        d2 = (xx * xx) + (yy * yy) + (zz * zz);

        return Math.sqrt(d2);
    }

    /**
     *  Gets the amide attribute of the <tt>JOEBond</tt> object
     *
     * @return    The amide value
     */
    public boolean isAmide()
    {
        JOEAtom a1;
        JOEAtom a2;
        a1 = a2 = null;

        if ((_bgn.getAtomicNum() == 6) && (_end.getAtomicNum() == 7))
        {
            a1 = _bgn;
            a2 = _end;
        }

        if ((_bgn.getAtomicNum() == 7) && (_end.getAtomicNum() == 6))
        {
            a1 = _end;
            a2 = _bgn;
        }

        if ((a1 == null) || (a2 == null))
        {
            return false;
        }

        if (getBO() != 1)
        {
            return false;
        }

        JOEBond bond;
        BondIterator bit = a1.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isCarbonyl())
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Sets the aromatic flag for this bond.
     *
     * This flag does not change the bond order.
     * Please remember that the aromaticity typer JOEAromaticTyper.assignAromaticFlags(JOEMol)
     * assign ONLY aromaticity flags and NOT the internal aromatic bond order JOEBond.JOE_AROMATIC_BOND_ORDER.
     */
    public void setAromatic()
    {
        setFlag(JOE_AROMATIC_BOND);
    }

    /**
     * Gets the aromatic state of this bond.
     *
     * Please remember that you can check the aromaticity also via the
     * bond order of JOEBond.JOE_AROMATIC_BOND_ORDER. But the aromaticity flag
     * is the better way.
     *
     * Please remember that the aromaticity typer JOEAromaticTyper.assignAromaticFlags(JOEMol)
     * assign ONLY aromaticity flags and NOT the internal aromatic bond order JOEBond.JOE_AROMATIC_BOND_ORDER.
     *
     * @return    <tt>true</tt> if this bond is aromatic
     */
    public boolean isAromatic()
    {
        if (hasFlag(JOE_AROMATIC_BOND))
        {
            return true;
        }

        if (!(getParent()).hasAromaticPerceived())
        {
            JOEAromaticTyper.instance().assignAromaticFlags(getParent());

            if (hasFlag(JOE_AROMATIC_BOND))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Sets the bond order for this bond.
     *
     * This causes no change in the aromaticity flag for this bond.
     *
     * Please remember that the aromaticity typer JOEAromaticTyper.assignAromaticFlags(JOEMol)
     * assign ONLY aromaticity flags and NOT the internal aromatic bond order JOEBond.JOE_AROMATIC_BOND_ORDER.
     *
     * @param  order  the new bond order
     */
    public void setBO(int order)
    {
        _order = (char) order;

        if (order == 5)
        {
            setAromatic();

            if (_bgn != null)
            {
                _bgn.setAromatic();
            }

            if (_end != null)
            {
                _end.setAromatic();
            }
        }
        else
        {
            unsetAromatic();
        }
    }

    /**
     *  Sets the begin attribute of the <tt>JOEBond</tt> object
     *
     * @param  begin  The new begin value
     */
    public void setBegin(JOEAtom begin)
    {
        _bgn = begin;
    }

    /**
     *  Gets the beginAtom attribute of the <tt>JOEBond</tt> object
     *
     * @return    The beginAtom value
     */
    public JOEAtom getBeginAtom()
    {
        return (_bgn);
    }

    /**
     *  Gets the carbonyl attribute of the <tt>JOEBond</tt> object
     *
     * @return    The carbonyl value
     */
    public boolean isCarbonyl()
    {
        if (getBO() != 2)
        {
            return false;
        }

        if (((_bgn.getAtomicNum() == 6) && (_end.getAtomicNum() == 8)) ||
                ((_bgn.getAtomicNum() == 8) && (_end.getAtomicNum() == 6)))
        {
            return true;
        }

        return false;
    }

    /**
     *  Sets the closure attribute of the <tt>JOEBond</tt> object
     */
    public void setClosure()
    {
        setFlag(JOE_CLOSURE_BOND);
    }

    /**
     *  Gets the closure attribute of the <tt>JOEBond</tt> object
     *
     * @return    The closure value
     */
    public boolean isClosure()
    {
        JOEMol mol = getParent();

        if (mol == null)
        {
            return false;
        }

        if (mol.hasClosureBondsPerceived())
        {
            return hasFlag(JOE_CLOSURE_BOND);
        }

        mol.setClosureBondsPerceived();

        JOEBond bond;
        JOEAtom atom;
        JOEAtom nbr;
        JOEBitVec uatoms = new JOEBitVec(mol.numAtoms() + 1);
        JOEBitVec ubonds = new JOEBitVec(mol.numAtoms() + 1);

        Vector curr = new Vector();

        // of type JOEAtom
        Vector next = new Vector();

        // of type JOEAtom
        AtomIterator ait = mol.atomIterator();

        while (uatoms.countBits() < mol.numAtoms())
        {
            if (curr.size() == 0)
            {
                ait.reset();

                while (ait.hasNext())
                {
                    atom = ait.nextAtom();

                    if (!uatoms.get(atom.getIdx()))
                    {
                        uatoms.set(atom.getIdx());

                        //              System.out.println("set uat#"+ atom.getIdx() );
                        curr.add(atom);

                        break;
                    }
                }
            }

            while (curr.size() != 0)
            {
                for (int i = 0; i < curr.size(); i++)
                {
                    NbrAtomIterator nait = ((JOEAtom) curr.get(i)).nbrAtomIterator();

                    while (nait.hasNext())
                    {
                        nbr = nait.nextNbrAtom();

                        if (!uatoms.get(nbr.getIdx()))
                        {
                            uatoms.set(nbr.getIdx());
                            ubonds.set(nait.actualBond().getIdx());

                            //                          System.out.println("set uat"+ nbr.getIdx()+
                            //                                             "set ubo"+ nait.actualBond().getIdx()  );
                            next.add(nbr);
                        }
                    }
                }

                //c++: curr = next;
                curr.clear();

                for (int nn = 0; nn < next.size(); nn++)
                {
                    curr.add(next.get(nn));
                }

                next.clear();
            }
        }

        BondIterator bit = mol.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (!ubonds.get(bond.getIdx()))
            {
                bond.setClosure();

                //          System.out.println("set closure: "+bond.getIdx());
            }
        }

        return hasFlag(JOE_CLOSURE_BOND);
    }

    /**
     *  Gets the double attribute of the <tt>JOEBond</tt> object
     *
     * @return    The double value
     */
    public boolean isDouble()
    {
        if (hasFlag(JOE_AROMATIC_BOND))
        {
            return false;
        }

        if (!(getParent()).hasAromaticPerceived())
        {
            JOEAromaticTyper.instance().assignAromaticFlags(getParent());
        }

        if ((this.getBondOrder() == 2) && !(hasFlag(JOE_AROMATIC_BOND)))
        {
            return true;
        }

        return false;
    }

    /**
     *  Sets the down attribute of the <tt>JOEBond</tt> object
     */
    public void setDown()
    {
        setFlag(JOE_TORDOWN_BOND);
    }

    /**
     *  Gets the down attribute of the <tt>JOEBond</tt> object
     *
     * @return    The down value
     */
    public boolean isDown()
    {
        return (hasFlag(JOE_TORDOWN_BOND));
    }

    /**
     *  Sets the end attribute of the <tt>JOEBond</tt> object
     *
     * @param  end  The new end value
     */
    public void setEnd(JOEAtom end)
    {
        _end = end;
    }

    /**
     *  Gets the endAtom attribute of the <tt>JOEBond</tt> object
     *
     * @return    The endAtom value
     */
    public JOEAtom getEndAtom()
    {
        return (_end);
    }

    /**
     *  Gets the equibLength attribute of the <tt>JOEBond</tt> object
     *
     * @return    The equibLength value
     */
    public double getEquibLength()
    {
        double length;
        JOEAtom begin;
        JOEAtom end;

        // CorrectedBondRad will always return a # now
        //  if (!correctedBondRad(getBeginAtom(),rad1)) return(0.0);
        //  if (!correctedBondRad(getEndAtom(),rad2))   return(0.0);
        begin = getBeginAtom();
        end = getEndAtom();
        length = JOEElementTable.instance().correctedBondRad(begin.getAtomicNum(),
                begin.getHyb()) +
            JOEElementTable.instance().correctedBondRad(end.getAtomicNum(),
                end.getHyb());

        if (isAromatic())
        {
            length *= 0.93f;
        }
        else if (getBO() == 2)
        {
            length *= 0.91f;
        }
        else if (getBO() == 3)
        {
            length *= 0.87f;
        }

        return length;
    }

    /**
     *  Gets the ester attribute of the <tt>JOEBond</tt> object
     *
     * @return    The ester value
     */
    public boolean isEster()
    {
        JOEAtom a1;
        JOEAtom a2;
        a1 = a2 = null;

        if ((_bgn.getAtomicNum() == 6) && (_end.getAtomicNum() == 8))
        {
            a1 = _bgn;
            a2 = _end;
        }

        if ((_bgn.getAtomicNum() == 8) && (_end.getAtomicNum() == 6))
        {
            a1 = _end;
            a2 = _bgn;
        }

        if ((a1 == null) || (a2 == null))
        {
            return false;
        }

        if (getBO() != 1)
        {
            return false;
        }

        BondIterator bit = a1.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isCarbonyl())
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Gets the hash attribute of the <tt>JOEBond</tt> object
     *
     * @return    The hash value
     */
    public boolean isHash()
    {
        return (hasFlag(JOE_HASH_BOND));
    }

    /**
     *  Sets the idx attribute of the <tt>JOEBond</tt> object
     *
     * @param  idx  The new idx value
     */
    public void setIdx(int idx)
    {
        _idx = idx;
    }

    /**
     *  Sets the inRing attribute of the <tt>JOEBond</tt> object
     */
    public void setInRing()
    {
        setFlag(JOE_RING_BOND);
    }

    /**
     *  Gets the inRing attribute of the <tt>JOEBond</tt> object
     *
     * @return    The inRing value
     */
    public boolean isInRing()
    {
        if (hasFlag(JOE_RING_BOND))
        {
            return true;
        }

        if (!(getParent()).hasRingAtomsAndBondsPerceived())
        {
            (getParent()).findRingAtomsAndBonds();

            if (hasFlag(JOE_RING_BOND))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Assign double-bond-Kekule-flag to this bond.
     */
    public void setKDouble()
    {
        _flags &= (~(JOE_KSINGLE_BOND | JOE_KDOUBLE_BOND | JOE_KTRIPLE_BOND));
        _flags |= JOE_KDOUBLE_BOND;
    }

    /**
     *  Returns <tt>true</tt> if this is a double bond in the Kekule structure of this molecule.
     *
     * @return    <tt>true</tt> if this is a double bond in the Kekule structure of this molecule.
     */
    public boolean isKDouble()
    {
        if ((_flags & JOE_KDOUBLE_BOND) != 0)
        {
            return true;
        }

        if (!(getParent()).hasKekulePerceived())
        {
            getParent().perceiveKekuleBonds();
        }

        return ((_flags & JOE_KDOUBLE_BOND) != 0) ? true : false;
    }

    /**
     *  Assign single to this bond.
     */
    public void setKSingle()
    {
        _flags &= (~(JOE_KSINGLE_BOND | JOE_KDOUBLE_BOND | JOE_KTRIPLE_BOND));
        _flags |= JOE_KSINGLE_BOND;
    }

    /**
     *  Returns <tt>true</tt> if this is a single bond in the Kekule structure of this molecule.
     *
     * @return    <tt>true</tt> if this is a single bond in the Kekule structure of this molecule.
     */
    public boolean isKSingle()
    {
        if ((_flags & JOE_KSINGLE_BOND) != 0)
        {
            return true;
        }

        if (!(getParent()).hasKekulePerceived())
        {
            getParent().perceiveKekuleBonds();
        }

        return ((_flags & JOE_KSINGLE_BOND) != 0) ? true : false;
    }

    /**
     *  Assign triple-bond-Kekule-flag to this bond.
     */
    public void setKTriple()
    {
        _flags &= (~(JOE_KSINGLE_BOND | JOE_KDOUBLE_BOND | JOE_KTRIPLE_BOND));
        _flags |= JOE_KTRIPLE_BOND;
    }

    /**
     *  Returns <tt>true</tt> if this is a triple bond in the Kekule structure of this molecule.
     *
     * @return    <tt>true</tt> if this is a triple bond in the Kekule structure of this molecule.
     */
    public boolean isKTriple()
    {
        if ((_flags & JOE_KTRIPLE_BOND) != 0)
        {
            return true;
        }

        if (!(getParent()).hasKekulePerceived())
        {
            getParent().perceiveKekuleBonds();
        }

        return ((_flags & JOE_KTRIPLE_BOND) != 0) ? true : false;
    }

    /**
     *  Sets the length attribute of the <tt>JOEBond</tt> object
     *
     * @param  fixed   The new length value
     * @param  length  The new length value
     */
    public void setLength(JOEAtom fixed, double length)
    {
        int i;
        JOEMol mol = fixed.getParent();
        XYZVector v1 = new XYZVector();
        XYZVector v2 = new XYZVector();
        XYZVector v3 = new XYZVector();
        XYZVector v4 = new XYZVector();
        Vector children = new Vector();

        // of type int[1]
        int[] tmp;

        int a = fixed.getIdx();
        int b = getNbrAtom(fixed).getIdx();

        mol.findChildren(children, a, b);
        children.add(new int[]{b});

        v1 = getNbrAtom(fixed).getVector();
        v2 = fixed.getVector();
        XYZVector.sub(v3, v1, v2);
        v3.normalize();
        v3.muling(length);
        v3.adding(v2);
        XYZVector.sub(v4, v3, v1);

        for (i = 0; i < children.size(); i++)
        {
            tmp = (int[]) children.get(i);
            v1 = mol.getAtom(tmp[0]).getVector();
            v1.adding(v4);
            mol.getAtom(tmp[0]).setVector(v1);

            /*
             *idx = (tmp[0]-1) * 3;
             *c[idx]   += x;
             *c[idx+1] += y;
             *c[idx+2] += z;
             */
        }
    }

    /**
     *  Gets the nbrAtom attribute of the <tt>JOEBond</tt> object
     *
     * @param  ptr  Description of the Parameter
     * @return      The nbrAtom value
     */
    public JOEAtom getNbrAtom(JOEAtom ptr)
    {
        return ((ptr != _bgn) ? _bgn : _end);
    }

    /**
     *  Gets the nbrAtomIdx attribute of the <tt>JOEBond</tt> object
     *
     * @param  ptr  Description of the Parameter
     * @return      The nbrAtomIdx value
     */
    public int getNbrAtomIdx(JOEAtom ptr)
    {
        return ((ptr != _bgn) ? _bgn.getIdx() : _end.getIdx());
    }

    /**
     *  Sets the parent attribute of the <tt>JOEBond</tt> object
     *
     * @param  ptr  The new parent value
     */
    public void setParent(JOEMol ptr)
    {
        _parent = ptr;
    }

    /**
     *  Gets the parent attribute of the <tt>JOEBond</tt> object
     *
     * @return    The parent value
     */
    public JOEMol getParent()
    {
        return (_parent);
    }

    /**
     *  Gets the primaryAmide attribute of the <tt>JOEBond</tt> object
     *
     * @return    The primaryAmide value
     */
    public boolean isPrimaryAmide()
    {
        JOEAtom a1;
        JOEAtom a2;
        a1 = a2 = null;

        if ((_bgn.getAtomicNum() == 6) && (_end.getAtomicNum() == 7))
        {
            a1 = _bgn;
            a2 = _end;
        }

        if ((_bgn.getAtomicNum() == 7) && (_end.getAtomicNum() == 6))
        {
            a1 = _end;
            a2 = _bgn;
        }

        if ((a1 == null) || (a2 == null))
        {
            return false;
        }

        if (getBO() != 1)
        {
            return false;
        }

        BondIterator bit = a1.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isCarbonyl())
            {
                if (a2.getHvyValence() == 2)
                {
                    return true;
                }
            }
        }

        return false;
    }

    //***property request methods***

    /**
     * Returns <tt>true</tt> if this is a ratatable single bond.
     *
     * @return    The rotor value
     */
    public boolean isRotor()
    {
        return ((_bgn.getHvyValence() > 1) && (_end.getHvyValence() > 1) &&
        (_order == 1) && !isInRing() && (_bgn.getHyb() != 1) &&
        (_end.getHyb() != 1));
    }

    /**
     *  Gets the secondaryAmide attribute of the <tt>JOEBond</tt> object
     *
     * @return    The secondaryAmide value
     */
    public boolean isSecondaryAmide()
    {
        return false;
    }

    /**
     *  Gets the single attribute of the <tt>JOEBond</tt> object
     *
     * @return    The single value
     */
    public boolean isSingle()
    {
        if (hasFlag(JOE_AROMATIC_BOND))
        {
            return false;
        }

        if (!(getParent()).hasAromaticPerceived())
        {
            JOEAromaticTyper.instance().assignAromaticFlags(getParent());
        }

        if ((this.getBondOrder() == 1) && !(hasFlag(JOE_AROMATIC_BOND)))
        {
            return true;
        }

        return false;
    }

    public boolean isTriple()
    {
        if ((this.getBondOrder() == 3))
        {
            return true;
        }

        return false;
    }

    /**
     *  The JUnit setup method
     */
    public void setUp()
    {
        setFlag(JOE_TORUP_BOND);
    }

    /**
     *  Gets the up attribute of the <tt>JOEBond</tt> object
     *
     * @return    The up value
     */
    public boolean isUp()
    {
        return (hasFlag(JOE_TORUP_BOND));
    }

    /**
     *  Gets the wedge attribute of the <tt>JOEBond</tt> object
     *
     * @return    The wedge value
     */
    public boolean isWedge()
    {
        return (hasFlag(JOE_WEDGE_BOND));
    }

    public void clear()
    {
        _idx = 0;
        _order = 0;
        _flags = 0;
        _bgn = null;
        _end = null;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/
    public Object clone()
    {
        return clone(this, new JOEBond());
    }

    /**
     * Checks if two bonds are equal.
     *
     * Compares order, flags and equality of begin and end atom.
     * AND the bond index number AND the full equality check of
     * the begin and end atom.
     *
     * @param type
     * @return
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof JOEBond)
        {
            return equals((JOEBond) obj, true);
        }
        else
        {
            return false;
        }
    }

    /**
     * Checks if two bonds are equal.
     *
     * Compares order, flags and equality of begin and end atom.
     * When <tt>fullComparison</tt> is set to <tt>false</tt> the bond index number
     * is ignored. The atom are compared also using the <tt>fullComparison</tt> flag.
     *
     * @param type
     * @return
     */
    public boolean equals(JOEBond type, boolean fullComparison)
    {
        if (fullComparison)
        {
            if (_idx != type._idx)
            {
                return false;
            }
        }

        if (_order != type._order)
        {
            return false;
        }

        if (_flags != type._flags)
        {
            return false;
        }

        if (!getEndAtom().equals(type.getEndAtom(), fullComparison))
        {
            return false;
        }

        if (!getBeginAtom().equals(type.getBeginAtom(), fullComparison))
        {
            return false;
        }

        return true;
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }

    /**
     * Calculates the hashcode for a bond.
     *
     * Includes order, flags and equality of begin and end atom.
     * Excludes the bond index number. The begin and end atom are included
     * using their hashcode methods.
     *
     * @param type
     * @return
     */
    public synchronized int hashCode()
    {
        if (hash != 0)
        {
            return hash;
        }

        int hashCode = _order;

        hashCode = (31 * hashCode) + _flags;
        hashCode = (31 * hashCode) + getEndAtom().hashCode();
        hashCode = (31 * hashCode) + getBeginAtom().hashCode();

        hash = hashCode;

        return hashCode;
    }

    public synchronized int reHash()
    {
        hash = 0;

        return hashCode();
    }

    /**
     *  Description of the Method
     *
     * @param  idx    Description of the Parameter
     * @param  begin  Description of the Parameter
     * @param  end    Description of the Parameter
     * @param  order  Description of the Parameter
     * @param  flags  Description of the Parameter
     */
    public void set(int idx, JOEAtom begin, JOEAtom end, int order, int flags)
    {
        setIdx(idx);
        setBegin(begin);
        setEnd(end);
        setBO(order);
        setFlag(flags);
    }

    public String toString()
    {
        if (this.isKDouble() || this.isAromatic())
        {
            return "=";
        }
        else if (this.isKTriple())
        {
            return "#"; //return "-";
        }
        else
        {
            return "-";
        }
    }

    /**
     *  Description of the Method
     */
    public void unsetAromatic()
    {
        _flags &= (~(JOE_AROMATIC_BOND));
    }

    /**
     *  Sets the flag attribute of the <tt>JOEBond</tt> object
     *
     * @param  flag  The new flag value
     */
    protected void setFlag(int flag)
    {
        _flags |= flag;
    }

    /**
     *  Description of the Method
     *
     * @param  flag  Description of the Parameter
     * @return       Description of the Return Value
     */
    protected boolean hasFlag(int flag)
    {
        return ((_flags & flag) != 0);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
