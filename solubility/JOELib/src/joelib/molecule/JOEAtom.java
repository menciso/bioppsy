///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEAtom.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.43 $
//            $Date: 2004/08/27 09:30:44 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEAromaticTyper;
import joelib.data.JOEAtomTyper;
import joelib.data.JOEDataType;
import joelib.data.JOEElementTable;
import joelib.data.JOEIsotopeTable;
import joelib.data.JOEPhModel;

import joelib.math.Matrix3x3;
import joelib.math.XYZVector;

import joelib.molecule.charge.JOEGastChrg;

import joelib.ring.JOERing;
import joelib.ring.JOERingData;

import joelib.util.iterator.BondIterator;
import joelib.util.iterator.NbrAtomIterator;
import joelib.util.iterator.RingIterator;
import joelib.util.iterator.VectorIterator;


//import joelib.molecule.chain.*;

/**
 *  Atom representation.
 *
 * @author     wegnerj
 * @license    GPL
 * @cvsversion    $Revision: 1.43 $, $Date: 2004/08/27 09:30:44 $
 */
public class JOEAtom implements Cloneable, java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.molecule.JOEAtom");

    /**
     *  Atom flag: is in a ring of size 4.
     */
    public final static int JOE_4RING_ATOM = (1 << 1);

    /**
     *  Atom flag: is in a ring of size 3.
     */
    public final static int JOE_3RING_ATOM = (1 << 2);

    /**
     *  Atom flag: is aromatic.
     */
    public final static int JOE_AROMATIC_ATOM = (1 << 3);

    /**
     *  Atom flag: is in a ring.
     */
    public final static int JOE_RING_ATOM = (1 << 4);

    /**
     *  Atom flag: is a clockwise stereo atom.
     *
     * @see    joelib.molecule.JOEAtom#JOE_ACSTEREO_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_CHIRAL_ATOM
     */
    public final static int JOE_CSTEREO_ATOM = (1 << 5);

    /**
     *  Atom flag: is a anti-clockwise stereo atom.
     *
     * @see    joelib.molecule.JOEAtom#JOE_CSTEREO_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_CHIRAL_ATOM
     */
    public final static int JOE_ACSTEREO_ATOM = (1 << 6);

    /**
     *  Atom flag: is a donor.
     */
    public final static int JOE_DONOR_ATOM = (1 << 7);

    /**
     *  Atom flag: is a acceptor.
     */
    public final static int JOE_ACCEPTOR_ATOM = (1 << 8);

    /**
     *  Atom flag: is a chiral atom (stereo center).
     *
     * @see    joelib.molecule.JOEAtom#JOE_CSTEREO_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_ACSTEREO_ATOM
     */
    public final static int JOE_CHIRAL_ATOM = (1 << 9);
    public static final int FREE_ELECTRONS_NOT_DEF = Integer.MIN_VALUE;

    /**
     *  1/sqrt(3).
     */
    private final static double ONE_OVER_SQRT3 = 0.577350269f;

    /**
     *  sqrt(2/3).
     */
    private final static double SQRT_TWO_THIRDS = 0.816496581f;

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Molecule which contains this atom.
     */
    protected JOEMol _parent;
    protected JOEResidue _residue;

    /**
     *  Description of the Field
     */
    protected String _type;

    /**
     *  Bonds of this atom.
     */
    protected Vector _bond;

    /**
     *  x,y and z coordinate of the molecule.
     */
    protected XYZVector _v;

    /**
     *  Coordinate array.
     */
    protected double[] _c;

    /**
     *  Partitial charge of this atom.
     */
    protected double _pcharge;

    /**
     *  Atomic number. Element number in the periodic table.
     */
    protected int _atomicnum;

    /**
     *  Index into coordinate array.
     */
    protected int _cidx;

    /**
     *  Formal charge of this atom.
     */
    protected int _fcharge;

    /**
     *  Atom flags.
     *
     * @see    joelib.molecule.JOEAtom#JOE_4RING_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_3RING_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_AROMATIC_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_RING_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_CSTEREO_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_ACSTEREO_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_ACCEPTOR_ATOM
     * @see    joelib.molecule.JOEAtom#JOE_CHIRAL_ATOM
     */
    protected int _flags;

    /**
     *  Description of the Field
     */
    protected int _freeElectrons;

    /**
     *  Hybridisation of the atom.
     */
    protected int _hyb;

    /**
     *  Index number as loaded from molecule file.
     */
    protected int _idx;

    /**
     *  Implicit valence.
     */
    protected int _impval;

    /**
     * Isotope value (0 = most abundant) for this atom.
     */
    protected int _isotope;
    private int hash = 0;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the <tt>JOEAtom</tt> object.
     */
    public JOEAtom()
    {
        _v = new XYZVector();
        _bond = new Vector();
        _freeElectrons = FREE_ELECTRONS_NOT_DEF;
        clear();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Clone the <tt>JOEAtom</tt> <tt>src</tt> to the <tt>JOEAtom</tt> <tt>to
     *  </tt>. Bond info is not copied here as pointers may be invalid.
     *
     * @param  src  the source <tt>JOEAtom</tt>
     * @param  to   the destination <tt>JOEAtom</tt>
     * @return      the destination <tt>JOEAtom</tt>
     */
    public static Object clone(JOEAtom src, JOEAtom to)
    {
        to.clear();

        to._idx = src.getIdx();
        to._hyb = src.getHyb();
        to._atomicnum = src.getAtomicNum();
        to._fcharge = src.getFormalCharge();
        to._isotope = src.getIsotope();

        //to._stereo   = src.getStereo();
        to._type = src.getType();
        to._pcharge = src.getPartialCharge();

        // heavy weight cloning
        // you can use to.setVector( src ); for light weight cloning
        to.setVector(src.getVector().x(), src.getVector().y(),
            src.getVector().z());

        //    System.out.println("add "+src.getVector());
        to._flags = src.getFlag();
        to._residue = null;

        return (to);
    }

    public final double getAtomicMass()
    {
        if (_isotope == 0)
        {
            return JOEElementTable.instance().getMass(_atomicnum);
        }
        else
        {
            return JOEIsotopeTable.instance().getExactMass(_atomicnum, _isotope);
        }
    }

    /**
     * Gets the atomic number of the <tt>JOEAtom</tt> object.
     *
     * @return    the atomic number
     */
    public final int getAtomicNum()
    {
        return ((int) _atomicnum);
    }

    /**
     *  Returns <tt>true</tt> if nitrogen is part of an amide.
     *
     * @return    <tt>true</tt> if nitrogen is part of an amide
     */
    public boolean isAmideNitrogen()
    {
        if (!isNitrogen())
        {
            return false;
        }

        JOEAtom nbratom;
        JOEAtom atom = this;
        JOEBond abbond;
        JOEBond bond;

        BondIterator bit1 = this.bondIterator();

        while (bit1.hasNext())
        {
            bond = bit1.nextBond();
            nbratom = bond.getNbrAtom(atom);

            BondIterator bit2 = nbratom.bondIterator();

            while (bit2.hasNext())
            {
                abbond = bit2.nextBond();

                if ((abbond.getBO() == 2) &&
                        (((abbond.getNbrAtom(nbratom)).getAtomicNum() == 8) ||
                        ((abbond.getNbrAtom(nbratom)).getAtomicNum() == 16)))
                {
                    return (true);
                }
            }
        }

        return (false);
    }

    /**
     *  Gets the antiClockwise attribute of the JOEAtom object
     *
     * @return    The antiClockwise value
     */
    public boolean isAntiClockwise()
    {
        return (hasFlag(JOE_ACSTEREO_ATOM));
    }

    /**
     *  Sets the 'anti clockwise stereo' flag of the <tt>JOEAtom</tt> .
     */
    public void setAntiClockwiseStereo()
    {
        setFlag(JOE_ACSTEREO_ATOM | JOE_CHIRAL_ATOM);
    }

    /**
     *  Sets the aromatic attribute of the <tt>JOEAtom</tt> object
     */
    public void setAromatic()
    {
        setFlag(JOE_AROMATIC_ATOM);
    }

    /**
     *  Returns <tt>true</tt> if this is a aromatic atom.
     *
     * @return    <tt>true</tt> if this is a aromatic atom
     */
    public boolean isAromatic()
    {
        if (hasFlag(JOE_AROMATIC_ATOM))
        {
            return true;
        }

        if (!(getParent()).hasAromaticPerceived())
        {
            JOEAromaticTyper.instance().assignAromaticFlags(getParent());

            if (hasFlag(JOE_AROMATIC_ATOM))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Returns <tt>true</tt> if this is an aromatic N-Oxide atom.
     *
     * @return    <tt>true</tt> if this is an aromatic N-Oxide atom
     */
    public boolean isAromaticNOxide()
    {
        if (!isNitrogen() || !isAromatic())
        {
            return false;
        }

        JOEAtom atom;
        NbrAtomIterator nait1 = nbrAtomIterator();

        while (nait1.hasNext())
        {
            atom = nait1.nextNbrAtom();

            if (atom.isOxygen() && !nait1.actualBond().isInRing() &&
                    (nait1.actualBond().getBO() == 2))
            {
                return (true);
            }
        }

        return (false);
    }

    /**
     *  Sets the atomic number of the <tt>JOEAtom</tt> object. E.g.: carbon atoms
     *  have the atomic number 6, H atoms the atomic number 1.
     *
     * @param  atomicnum  the new atomic number
     */
    public void setAtomicNum(int atomicnum)
    {
        _atomicnum = atomicnum;
    }

    /**
     * Returns <tt>true</tt> if this is a axial atom.
     *
     * @return    <tt>true</tt> if this is a axial atom
     */
    public boolean isAxial()
    {
        double tor;
        JOEAtom a;
        JOEAtom b;
        JOEAtom c;
        NbrAtomIterator nait1 = nbrAtomIterator();

        while (nait1.hasNext())
        {
            a = nait1.nextNbrAtom();

            if ((a.getHyb() == 3) && a.isInRing() &&
                    !nait1.actualBond().isInRing())
            {
                NbrAtomIterator nait2 = a.nbrAtomIterator();

                while (nait2.hasNext())
                {
                    b = nait2.nextNbrAtom();

                    if ((b != this) && b.isInRing() && (b.getHyb() == 3))
                    {
                        NbrAtomIterator nait3 = b.nbrAtomIterator();

                        while (nait3.hasNext())
                        {
                            c = (JOEAtom) nait3.next();

                            if ((c != a) && c.isInRing())
                            {
                                tor = Math.abs((getParent()).getTorsion(this,
                                            a, b, c));

                                return ((tor > 55.0) && (tor < 75.0));
                            }
                        }
                    }
                }
            }
        }

        return (false);
    }

    /**
     *  Gets the bond attribute of the <tt>JOEAtom</tt> object
     *
     * @param  nbr  Description of the Parameter
     * @return      The bond value
     */
    public JOEBond getBond(JOEAtom nbr)
    {
        BondIterator bit = this.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.getNbrAtom(this) == nbr)
            {
                return bond;
            }
        }

        return null;
    }

    /**
     *  Returns a {@link java.util.Vector} for all bonds in this atom.
     *
     * @return    the {@link java.util.Vector} for all bonds in this atom
     */
    public final Vector getBonds()
    {
        return _bond;
    }

    /**
     *  Gets the cIdx attribute of the <tt>JOEAtom</tt> object
     *
     * @return    The cIdx value
     */
    public final int getCIdx()
    {
        return (_cidx);
    }

    /**
     *  Returns <tt>true</tt> if carbon is part of an carboxyl group.
     *
     * @return  <tt>true</tt> if carbon is part of an carboxyl group
     */
    public boolean isCarboxylOxygen()
    {
        if (!isOxygen())
        {
            return false;
        }

        if (getHvyValence() != 1)
        {
            return false;
        }

        BondIterator bit = this.bondIterator();
        JOEBond bond;
        JOEAtom atom = null;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if ((bond.getNbrAtom(this)).isCarbon())
            {
                atom = bond.getNbrAtom(this);

                break;
            }
        }

        if (atom == null)
        {
            return false;
        }

        if (atom.countFreeOxygens() != 2)
        {
            return false;
        }

        //atom is connected to a carbon that has a total
        //of 2 attached free oxygens
        return true;
    }

    /**
     *  Sets the chiral flag of the <tt>JOEAtom</tt> .
     */
    public void setChiral()
    {
        setFlag(JOE_CHIRAL_ATOM);
    }

    /**
     *  Returns <tt>true</tt> if this is a chiral atom.
     *
     * @return    <tt>true</tt> if this is a chiral atom
     */
    public boolean isChiral()
    {
        if (hasFlag(JOE_CHIRAL_ATOM))
        {
            return true;
        }

        if (!(getParent()).hasChiralityPerceived())
        {
            (getParent()).findChiralCenters();

            if (hasFlag(JOE_CHIRAL_ATOM))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Gets the clockwise attribute of the JOEAtom object
     *
     * @return    The clockwise value
     */
    public boolean isClockwise()
    {
        return (hasFlag(JOE_CSTEREO_ATOM));
    }

    /**
     *  Sets the 'clockwise stereo' flag of the <tt>JOEAtom</tt> .
     */
    public void setClockwiseStereo()
    {
        setFlag(JOE_CSTEREO_ATOM | JOE_CHIRAL_ATOM);
    }

    /**
     *  Returns <tt>true</tt> if this atom is connected to the atom <tt>at</tt> .
     *
     * @param  at  Description of the Parameter
     * @return     The connected value
     */
    public boolean isConnected(JOEAtom at)
    {
        BondIterator bit = this.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if ((bond.getBeginAtom() == at) || (bond.getEndAtom() == at))
            {
                //        System.out.println("conn:true");
                return (true);
            }
        }

        /*
                        Vector bonds = getBonds();
                        JOEBond bond;
                        for (int i = 0; i < bonds.length(); i++)
                        {
                                bond = (JOEBond) bonds.get(i);
                                if (bond.getBeginAtom() == at || bond.getEndAtom() == at)
                                        return (true);
                        }
        */

        //System.out.println("conn:false");
        return (false);
    }

    /**
     *  Sets the coordPtr attribute of the <tt>JOEAtom</tt> object
     *
     * @param  c  The new coordPtr value
     */
    public void setCoordPtr(double[] c)
    {
        _c = c;
        _cidx = (getIdx() - 1) * 3;
    }

    /**
     *  Gets the coordinate attribute of the <tt>JOEAtom</tt> object
     *
     * @return    The coordinate value
     */
    public double[] getCoordinate()
    {
        return (_c);
    }

    /**
     *  Gets the coordinateIdx attribute of the <tt>JOEAtom</tt> object.
     *
     * @return    The coordinateIdx value
     */
    public final int getCoordinateIdx()
    {
        return ((int) _cidx);
    }

    /**
     * Gets the atom electronegativity after Allred and Rochow.
     *
     * @return   The allredRochowEN value
     */
    public double getENAllredRochow()
    {
        return JOEElementTable.instance().getAllredRochowEN(this.getAtomicNum());
    }

    /**
     * Gets the atom electronegativity after Pauling.
     *
     * @return   The paulingEN value
     */
    public double getENPauling()
    {
        return JOEElementTable.instance().getPaulingEN(this.getAtomicNum());
    }

    /**
     * Gets the atom electronegativity after Pauling.
     *
     * @return   The paulingEN value
     */
    public double getENSanderson()
    {
        return JOEElementTable.instance().getSandersonEN(this.getAtomicNum());
    }

    /**
     * Gets the electronAffinity attribute of the JOEElement object
     *
     * @return   The electronAffinity value
     */
    public double getElectronAffinity()
    {
        return JOEElementTable.instance().getElectronAffinity(this.getAtomicNum());
    }

    public final double getExactMass()
    {
        return JOEIsotopeTable.instance().getExactMass(_atomicnum, _isotope);
    }

    /**
     *  Gets the formal charge of the <tt>JOEAtom</tt> .
     *
     * @return    the formal charge of this atom
     */
    public final int getFormalCharge()
    {
        //    System.out.println("get charge: "+_fcharge);
        return (_fcharge);
    }

    /**
     *  Gets the index number of the <tt>JOEAtom</tt> object.
     *
     * @return    the index number of this atom
     */
    public final int getIdx()
    {
        return ((int) _idx);
    }

    /**
     *  Returns <tt>true</tt> if electronegative atom of type N, O, F, Cl, Br.
     *
     * @return    <tt>true</tt> if electronegative atom of type N, O, F, Cl, Br.
     */
    public boolean isElectronegative()
    {
        return ((_atomicnum == 7) || (_atomicnum == 8) || (_atomicnum == 9) ||
        (_atomicnum == 17) || (_atomicnum == 35));
    }

    public boolean isElementOfGroup(int periodicGroup)
    {
        if ((periodicGroup == 8) && (_atomicnum == 2))
        {
            return true;
        }

        if (periodicGroup <= 2)
        {
            if ((periodicGroup == 1) && (_atomicnum == 1))
            {
                return true;
            }

            return ((_atomicnum == (2 + periodicGroup)) ||
            (_atomicnum == (10 + periodicGroup)) ||
            (_atomicnum == (18 + periodicGroup)) ||
            (_atomicnum == (36 + periodicGroup)) ||
            (_atomicnum == (54 + periodicGroup)) ||
            (_atomicnum == (86 + periodicGroup)));
        }
        else
        {
            return ((_atomicnum == (2 + periodicGroup)) ||
            (_atomicnum == (10 + periodicGroup)) ||
            (_atomicnum == (28 + periodicGroup)) ||
            (_atomicnum == (46 + periodicGroup)) ||
            (_atomicnum == (78 + periodicGroup)) ||
            (_atomicnum == (110 + periodicGroup)));
        }

        //return false;
    }

    /**
     *  Sets the formal charge of the <tt>JOEAtom</tt> object.
     *
     * @param  fcharge  the new formal charge of this atom
     */
    public void setFormalCharge(int fcharge)
    {
        //		if(fcharge==1)
        //		{
        //			    System.out.println("set charge: "+fcharge);
        //			    Object obj=null;
        //			    obj.toString();
        //		}
        _fcharge = fcharge;
    }

    /**
     * Set the number of free electrons.
     *
     * @param the number of free electrons or <tt>FREE_ELECTRONS_NOT_DEF</tt>
     * @see #FREE_ELECTRONS_NOT_DEF
     */
    public void setFreeElectrons(int freeElectrons)
    {
        _freeElectrons = freeElectrons;
    }

    /**
     *  Returns <tt>true</tt> if this is a halogen atom.
     *
     * @return    <tt>true</tt> if this is a halogen atom.
     */
    public boolean isHalogen()
    {
        return ((_atomicnum == 9) || (_atomicnum == 17) || (_atomicnum == 35) ||
        (_atomicnum == 53) || (_atomicnum == 85));
    }

    /**
     *  Returns <tt>true</tt> if this is a hetero atom.
     *
     * @return    <tt>true</tt> if this is a hetero atom.
     */
    public boolean isHeteroatom()
    {
        return ((_atomicnum == 7) || (_atomicnum == 8) || (_atomicnum == 15) ||
        (_atomicnum == 16) || isHalogen());
    }

    /**
     *  Sets the hybridisation of the <tt>JOEAtom</tt> object.
     *
     * @param  hyb  the new hybridisation value
     */
    public void setHyb(int hyb)
    {
        _hyb = hyb;
    }

    /**
      * Sets hybridization and geometry of this atom.
      *
      * @param  hyb  The hybridisation of this atom
      * @return      <tt>true</tt> if successfull
      */
    public boolean setHybAndGeom(int hyb)
    {
        //if (hyb == getHyb()) return true;
        if ((hyb == 0) && (getHvyValence() > 1))
        {
            return false;
        }

        if ((hyb == 1) && (getHvyValence() > 2))
        {
            return false;
        }

        if ((hyb == 2) && (getHvyValence() > 3))
        {
            return false;
        }

        if ((hyb == 3) && (getHvyValence() > 4))
        {
            return false;
        }

        JOEMol mol = getParent();

        JOEAtom nbr;
        Vector delatm = new Vector();
        NbrAtomIterator nait = nbrAtomIterator();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();

            if (nbr.isHydrogen())
            {
                delatm.add(nbr);
            }
        }

        //delete attached hydrogens
        mol.incrementMod();

        for (int j = 0; j < delatm.size(); j++)
        {
            mol.deleteAtom((JOEAtom) delatm.get(j));
        }

        mol.decrementMod();

        double targetAngle;

        if (hyb == 3)
        {
            targetAngle = 109.5;
        }
        else if (hyb == 2)
        {
            targetAngle = 120.0;
        }
        else if (hyb == 1)
        {
            targetAngle = 180.0;
        }
        else
        {
            targetAngle = 0.0;
        }

        //adjust attached acyclic bond lengths
        double br1;

        //adjust attached acyclic bond lengths
        double br2;
        br1 = JOEElementTable.instance().correctedBondRad(getAtomicNum(), hyb);

        nait.reset();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();

            if (!nait.actualBond().isInRing())
            {
                br2 = JOEElementTable.instance().correctedBondRad(nbr.getAtomicNum(),
                        nbr.getHyb());
                nait.actualBond().setLength(this, br1 + br2);
            }
        }

        if (getValence() > 1)
        {
            double angle;
            Matrix3x3 m = new Matrix3x3();
            XYZVector v1 = new XYZVector();
            XYZVector v2 = new XYZVector();
            XYZVector v3 = new XYZVector();
            XYZVector v4 = new XYZVector();
            XYZVector n = new XYZVector();
            XYZVector s = new XYZVector();
            JOEAtom r1;
            JOEAtom r2;
            JOEAtom r3;
            JOEAtom a1;
            JOEAtom a2;
            JOEAtom a3;
            JOEAtom a4;
            r1 = r2 = r3 = a1 = a2 = a3 = a4 = null;

            //find ring atoms first
            nait.reset();

            while (nait.hasNext())
            {
                nbr = nait.nextNbrAtom();

                if (nait.actualBond().isInRing())
                {
                    if (r1 == null)
                    {
                        r1 = nbr;
                    }
                    else if (r2 == null)
                    {
                        r2 = nbr;
                    }
                    else if (r3 == null)
                    {
                        r3 = nbr;
                    }
                }
            }

            //find non-ring atoms
            nait.reset();

            while (nait.hasNext())
            {
                nbr = nait.nextNbrAtom();

                if (!nait.actualBond().isInRing())
                {
                    if (a1 == null)
                    {
                        a1 = nbr;
                    }
                    else if (a2 == null)
                    {
                        a2 = nbr;
                    }
                    else if (a3 == null)
                    {
                        a3 = nbr;
                    }
                    else if (a4 == null)
                    {
                        a4 = nbr;
                    }
                }
            }

            //adjust geometries of heavy atoms according to hybridization
            if (hyb == 1)
            {
                if (a2 != null)
                {
                    XYZVector.sub(v1, a1.getVector(), getVector());
                    v1.normalize();
                    XYZVector.sub(v2, a2.getVector(), getVector());
                    v2.normalize();
                    XYZVector.cross(n, v1, v2);
                    angle = XYZVector.xyzVectorAngle(v1, v2) - targetAngle;
                    m.rotAboutAxisByAngle(n, -angle);
                    applyRotMatToBond(mol, m, this, a1);
                }
            }
            else if (hyb == 2)
            {
                if ((r1 != null) && (r2 != null) && (a1 != null))
                {
                    XYZVector.sub(v1, r1.getVector(), getVector());
                    v1.normalize();
                    XYZVector.sub(v2, r2.getVector(), getVector());
                    v2.normalize();
                    XYZVector.sub(v3, a1.getVector(), getVector());
                    XYZVector.add(s, v1, v2);
                    s.normalize();
                    s.muling(-1.0f);
                    XYZVector.cross(n, s, v3);
                    angle = XYZVector.xyzVectorAngle(s, v3);
                    m.rotAboutAxisByAngle(n, angle);
                    applyRotMatToBond(mol, m, this, a1);
                }
                else
                {
                    if (a2 != null)
                    {
                        XYZVector.sub(v1, a1.getVector(), getVector());
                        v1.normalize();
                        XYZVector.sub(v2, a2.getVector(), getVector());
                        v2.normalize();
                        XYZVector.cross(n, v1, v2);
                        angle = XYZVector.xyzVectorAngle(v1, v2) - targetAngle;
                        m.rotAboutAxisByAngle(n, -angle);
                        applyRotMatToBond(mol, m, this, a1);
                    }

                    if (a3 != null)
                    {
                        XYZVector.sub(v1, a1.getVector(), getVector());
                        v1.normalize();
                        XYZVector.sub(v2, a2.getVector(), getVector());
                        v2.normalize();
                        XYZVector.sub(v3, a3.getVector(), getVector());
                        XYZVector.add(s, v1, v2);
                        s.normalize();
                        s.muling(-1.0f);
                        XYZVector.cross(n, s, v3);
                        angle = XYZVector.xyzVectorAngle(s, v3);
                        m.rotAboutAxisByAngle(n, angle);
                        applyRotMatToBond(mol, m, this, a3);
                    }
                }
            }
            else if (hyb == 3)
            {
                if ((r1 != null) && (r2 != null) && (r3 != null) &&
                        (a1 != null))
                {
                    XYZVector.sub(v1, r1.getVector(), getVector());
                    v1.normalize();
                    XYZVector.sub(v2, r2.getVector(), getVector());
                    v2.normalize();
                    XYZVector.sub(v3, r3.getVector(), getVector());
                    v3.normalize();
                    XYZVector.sub(v4, a1.getVector(), getVector());
                    XYZVector.add(s, v1, v2);
                    s.adding(v3);
                    s.muling(-1.0f);
                    s.normalize();
                    XYZVector.cross(n, s, v4);
                    angle = XYZVector.xyzVectorAngle(s, v4);
                    m.rotAboutAxisByAngle(n, angle);
                    applyRotMatToBond(mol, m, this, a1);
                }
                else if ((r1 != null) && (r2 != null) && (r3 == null) &&
                        (a1 != null))
                {
                    XYZVector.sub(v1, r1.getVector(), getVector());
                    v1.normalize();
                    XYZVector.sub(v2, r2.getVector(), getVector());
                    v2.normalize();
                    XYZVector.sub(v3, a1.getVector(), getVector());
                    XYZVector.add(s, v1, v2);
                    s.muling(-1.0f);
                    s.normalize();
                    XYZVector.cross(n, v1, v2);
                    n.normalize();
                    s.muling(ONE_OVER_SQRT3);

                    // 1/sqrt(3)
                    n.muling(SQRT_TWO_THIRDS);

                    // sqrt(2/3)
                    s.adding(n);
                    s.normalize();
                    XYZVector.cross(n, s, v3);
                    angle = XYZVector.xyzVectorAngle(s, v3);
                    m.rotAboutAxisByAngle(n, angle);
                    applyRotMatToBond(mol, m, this, a1);

                    if (a2 != null)
                    {
                        XYZVector.sub(v1, r1.getVector(), getVector());
                        v1.normalize();
                        XYZVector.sub(v2, r2.getVector(), getVector());
                        v2.normalize();
                        XYZVector.sub(v3, a1.getVector(), getVector());
                        v3.normalize();
                        XYZVector.sub(v4, a2.getVector(), getVector());
                        XYZVector.add(s, v1, v2);
                        s.adding(v3);
                        s.muling(-1.0f);
                        s.normalize();
                        XYZVector.cross(n, s, v4);
                        angle = XYZVector.xyzVectorAngle(s, v4);
                        m.rotAboutAxisByAngle(n, angle);
                        applyRotMatToBond(mol, m, this, a2);
                    }
                }
                else
                {
                    if (a2 != null)
                    {
                        XYZVector.sub(v1, a1.getVector(), getVector());
                        v1.normalize();
                        XYZVector.sub(v2, a2.getVector(), getVector());
                        v2.normalize();
                        XYZVector.cross(n, v1, v2);
                        angle = XYZVector.xyzVectorAngle(v1, v2) - targetAngle;
                        m.rotAboutAxisByAngle(n, -angle);
                        applyRotMatToBond(mol, m, this, a1);
                    }

                    if (a3 != null)
                    {
                        XYZVector.sub(v1, a1.getVector(), getVector());
                        v1.normalize();
                        XYZVector.sub(v2, a2.getVector(), getVector());
                        v2.normalize();
                        XYZVector.sub(v3, a3.getVector(), getVector());
                        XYZVector.add(s, v1, v2);
                        s.muling(-1.0f);
                        s.normalize();
                        XYZVector.cross(n, v1, v2);
                        n.normalize();
                        s.muling(ONE_OVER_SQRT3);

                        // 1/sqrt(3)
                        n.muling(SQRT_TWO_THIRDS);

                        // sqrt(2/3)
                        s.adding(n);
                        s.normalize();
                        XYZVector.cross(n, s, v3);
                        angle = XYZVector.xyzVectorAngle(s, v3);
                        m.rotAboutAxisByAngle(n, angle);
                        applyRotMatToBond(mol, m, this, a3);
                    }
                }
            }
        }

        //add hydrogens back to atom
        int impval = 1;

        switch (getAtomicNum())
        {
        case 6:

            if (hyb == 3)
            {
                impval = 4;
            }

            if (hyb == 2)
            {
                impval = 3;
            }

            if (hyb == 1)
            {
                impval = 2;
            }

            break;

        case 7:

            if (hyb == 3)
            {
                impval = 3;
            }

            if (hyb == 2)
            {
                impval = 2;
            }

            if (hyb == 1)
            {
                impval = 1;
            }

            break;

        case 8:

            if (hyb == 3)
            {
                impval = 2;
            }

            if (hyb == 2)
            {
                impval = 2;
            }

            if (hyb == 1)
            {
                impval = 2;
            }

        case 16:

            if (hyb == 3)
            {
                impval = 2;
            }

            if (hyb == 2)
            {
                impval = 2;
            }

            if (hyb == 1)
            {
                impval = 0;
            }

            break;

        case 15:

            if (hyb == 3)
            {
                impval = 4;
            }

            if (hyb == 2)
            {
                impval = 3;
            }

            if (hyb == 1)
            {
                impval = 2;
            }

            break;

        default:
            impval = 1;
        }

        int hcount = impval - getHvyValence();

        if (hcount != 0)
        {
            int k;
            XYZVector v = new XYZVector();
            JOEAtom atom;
            double brsum = JOEElementTable.instance().correctedBondRad(1, 0) +
                JOEElementTable.instance().correctedBondRad(getAtomicNum(),
                    getHyb());
            setHyb(hyb);

            mol.beginModify();

            for (k = 0; k < hcount; k++)
            {
                getNewBondVector(v, brsum);
                atom = mol.newAtom();
                atom.setAtomicNum(1);
                atom.setType("H");
                atom.setVector(v);
                mol.addBond(atom.getIdx(), getIdx(), 1);
            }

            mol.endModify();
        }

        return (true);
    }

    /**
     *  Sets the index number of the <tt>JOEAtom</tt> object.
     *
     * @param  idx  the new index number
     */
    public void setIdx(int idx)
    {
        _idx = idx;
        _cidx = (idx - 1) * 3;
    }

    /**
     *  Sets the implicit valence of the <tt>JOEAtom</tt> .
     *
     * @param  val  the new implicit valence
     */
    public void setImplicitValence(int val)
    {
        _impval = val;
    }

    /**
     *  Sets the 'in ring' flag of the <tt>JOEAtom</tt> object.
     */
    public void setInRing()
    {
        setFlag(JOE_RING_ATOM);
    }

    /**
     *  Returns <tt>true</tt> if this is a ring atom.
     *
     * @return    <tt>true</tt> if this is a ring atom
     */
    public boolean isInRing()
    {
        if (hasFlag(JOE_RING_ATOM))
        {
            return (true);
        }

        if (!(getParent()).hasRingAtomsAndBondsPerceived())
        {
            (getParent()).findRingAtomsAndBonds();

            if (hasFlag(JOE_RING_ATOM))
            {
                return (true);
            }
        }

        return (false);
    }

    /**
     *  Returns <tt>true</tt> if this is a atom in a ring of given size.
     *
     * @param  size  size of the ring
     * @return       <tt>true</tt> if this is a atom in a ring of given size
     */
    public boolean isInRingSize(int size)
    {
        if (!(getParent()).hasSSSRPerceived())
        {
            (getParent()).findSSSR();
        }

        Vector rings = ((JOERingData) (getParent()).getData(JOEDataType.JOE_RING_DATA)).getData();

        if (rings == null)
        {
            logger.error("could not get SSSR data.");

            //      throw new Exception("could not get SSSR data.");
        }

        if (!hasFlag(JOE_RING_ATOM))
        {
            return false;
        }

        RingIterator rit = (getParent()).getSSSRIterator(rings);
        JOERing ring;

        while (rit.hasNext())
        {
            ring = rit.nextRing();

            if (ring.isInRing(getIdx()) && (ring.pathSize() == size))
            {
                return (true);
            }
        }

        return (false);
    }

    /**
     * Set isotpe value for this atom.
     *
     * @return byte
     */
    public void setIsotope(int isotopeValue)
    {
        _isotope = isotopeValue;
    }

    /**
     *  Gets the newBondVector attribute of the <tt>JOEAtom</tt> object
     *
     * @param  v       Description of the Parameter
     * @param  length  Description of the Parameter
     * @return         The newBondVector value
     */
    public boolean getNewBondVector(XYZVector v, double length)
    {
        // ***experimental code***
        JOEAtom atom = null;
        v.set(XYZVector.vZero);

        if (getValence() == 0)
        {
            v.set(XYZVector.vX);
            v.muling(length);
            v.adding(getVector());

            return true;
        }

        if (getValence() == 1)
        {
            XYZVector vtmp = new XYZVector();
            XYZVector v1 = new XYZVector();
            XYZVector v2 = new XYZVector();
            NbrAtomIterator nait = nbrAtomIterator();

            if (nait.hasNext())
            {
                atom = nait.nextNbrAtom();
            }

            vtmp = getVector().sub(atom.getVector());

            if ((getHyb() == 2) || (isOxygen() && hasAlphaBetaUnsat()))
            {
                boolean quit = false;
                JOEAtom a1;
                JOEAtom a2;
                v2 = XYZVector.vZero;

                NbrAtomIterator nait1 = nbrAtomIterator();

                while (nait1.hasNext() && !quit)
                {
                    a1 = nait1.nextNbrAtom();

                    NbrAtomIterator nait2 = a1.nbrAtomIterator();

                    while (nait2.hasNext() && !quit)
                    {
                        a2 = nait2.nextNbrAtom();

                        if ((a1 != null) && (a2 != null) && (a2 != this))
                        {
                            XYZVector.sub(v2, a1.getVector(), a2.getVector());
                            quit = true;
                        }
                    }
                }

                if (v2 == XYZVector.vZero)
                {
                    XYZVector.cross(v1, vtmp, XYZVector.vX);
                    XYZVector.cross(v2, vtmp, XYZVector.vY);

                    if (v1.length() < v2.length())
                    {
                        v1.set(v2);
                    }
                }
                else
                {
                    XYZVector.cross(v1, vtmp, v2);
                }

                Matrix3x3 m = new Matrix3x3();
                m.rotAboutAxisByAngle(v1, 60.0);
                XYZVector.mul(v, m, vtmp);
                v.normalize();
            }

            if (getHyb() == 3)
            {
                XYZVector.cross(v1, vtmp, XYZVector.vX);
                XYZVector.cross(v2, vtmp, XYZVector.vY);

                if (v1.length() < v2.length())
                {
                    v1.set(v2);
                }

                Matrix3x3 m = new Matrix3x3();
                m.rotAboutAxisByAngle(v1, 70.5);
                XYZVector.mul(v, m, vtmp);
                v.normalize();
            }

            if (getHyb() == 1)
            {
                v.set(vtmp);
            }

            v.muling(length);
            v.adding(getVector());

            return true;
        }

        if (getValence() == 2)
        {
            XYZVector v1 = new XYZVector();
            XYZVector v2 = new XYZVector();
            XYZVector vsum = new XYZVector();
            XYZVector vnorm = new XYZVector();
            NbrAtomIterator nait = nbrAtomIterator();

            if (!nait.hasNext())
            {
                return false;
            }

            atom = nait.nextNbrAtom();
            XYZVector.sub(v1, getVector(), atom.getVector());

            if (!nait.hasNext())
            {
                return false;
            }

            atom = nait.nextNbrAtom();

            XYZVector.sub(v2, getVector(), atom.getVector());
            v1.normalize();
            v2.normalize();
            XYZVector.add(vsum, v1, v2);
            vsum.normalize();

            if (getHyb() == 2)
            {
                v.set(vsum);
            }

            if (getHyb() == 3)
            {
                XYZVector.cross(vnorm, v2, v1);
                vnorm.normalize();

                vsum.muling(ONE_OVER_SQRT3);
                vnorm.muling(SQRT_TWO_THIRDS);

                XYZVector.add(v, vsum, vnorm);
            }

            v.muling(length);
            v.adding(getVector());

            return true;
        }

        if (getValence() == 3)
        {
            XYZVector vtmp = new XYZVector();
            XYZVector vsum = new XYZVector();
            NbrAtomIterator nait = nbrAtomIterator();

            while (nait.hasNext())
            {
                atom = nait.nextNbrAtom();
                XYZVector.sub(vtmp, getVector(), atom.getVector());
                vtmp.normalize();
                vtmp.diving(3.0);
                vsum.adding(vtmp);
            }

            vsum.normalize();
            v.set(vsum);
            v.muling(length);
            v.adding(getVector());

            return (true);
        }

        return true;
    }

    /**
     *  Gets the nextAtom attribute of the <tt>JOEAtom</tt> object
     *
     * @return    The nextAtom value
     */
    public JOEAtom getNextAtom()
    {
        JOEMol mol = getParent();

        return ((getIdx() == mol.numAtoms()) ? null : mol.getAtom(getIdx() + 1));
    }

    /**
     *  Returns <tt>true</tt> if nitrogen is part of a nitro group.
     *
     * @return    <tt>true</tt> if nitrogen is part of a nitro group
     */
    public boolean isNitroOxygen()
    {
        if (!isOxygen())
        {
            return false;
        }

        if (getHvyValence() != 1)
        {
            return false;
        }

        BondIterator bit = this.bondIterator();
        JOEBond bond;
        JOEAtom atom = null;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if ((bond.getNbrAtom(this)).isNitrogen())
            {
                atom = bond.getNbrAtom(this);

                break;
            }
        }

        if (atom == null)
        {
            return false;
        }

        if (atom.countFreeOxygens() != 2)
        {
            return false;
        }

        //atom is connected to a nitrogen that has a total
        //of 2 attached free oxygens
        return true;
    }

    /**
     *  Returns <tt>true</tt> if this is a nitrogen atom.
     *
     * @return    The nitrogen value
     */
    public boolean isNitrogen()
    {
        return (_atomicnum == 7);
    }

    /**
     *  Returns <tt>true</tt> if this is a non polar hydrogen atom.
     *
     * @return    <tt>true</tt> if this is a non polar hydrogen atom
     */
    public boolean isNonPolarHydrogen()
    {
        if (!isHydrogen())
        {
            return false;
        }

        BondIterator bit = this.bondIterator();
        JOEBond bond;
        JOEAtom atom;

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            atom = bond.getNbrAtom(this);

            if (atom.getAtomicNum() == 6)
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Returns <tt>true</tt> if this atom (first) is with <tt>atom</tt> (fourth)
     *  connected to another two atoms (second, third). E.g. <tt>this</tt> -'other
     *  atom 1'--'other atom 2'-<tt>atom</tt>
     *
     * @param  at  Description of the Parameter
     * @return     if this atom (first) is with <tt>atom</tt> (fourth) connected
     *      to another two atoms (second, third).
     */
    public boolean isOneFour(JOEAtom at)
    {
        JOEAtom atom1 = this;
        JOEAtom atom2 = at;
        JOEBond bond1;
        JOEBond bond2;

        BondIterator bit1 = atom1.bondIterator();
        BondIterator bit2 = atom2.bondIterator();

        while (bit1.hasNext())
        {
            bond1 = bit1.nextBond();
            bit2.reset();

            while (bit2.hasNext())
            {
                bond2 = bit2.nextBond();

                if ((bond1.getNbrAtom(atom1)).isConnected(bond2.getNbrAtom(
                                atom2)))
                {
                    return (true);
                }
            }
        }

        return (false);
    }

    /**
     *  Returns <tt>true</tt> if this atom (first) is with <tt>atom</tt> (third)
     *  connected to another atom (second). E.g. <tt>this</tt> -'other atom'-<tt>
     *  atom</tt>
     *
     * @param  atom  the third atom
     * @return       if this atom (first) is with <tt>atom</tt> (third) connected
     *      to another atom (second)
     */
    public boolean isOneThree(JOEAtom atom)
    {
        JOEAtom atom1 = this;
        JOEAtom atom2 = atom;
        JOEBond bond1;
        JOEBond bond2;

        BondIterator bit1 = atom1.bondIterator();
        BondIterator bit2 = atom2.bondIterator();

        while (bit1.hasNext())
        {
            bond1 = bit1.nextBond();
            bit2.reset();

            while (bit2.hasNext())
            {
                bond2 = bit2.nextBond();

                if (bond1.getNbrAtom(atom1) == bond2.getNbrAtom(atom2))
                {
                    return (true);
                }
            }
        }

        /*
                        Vector bonds1, bonds2;
                        bonds1 = atom1.getBonds();
                        bonds2 = atom2.getBonds();
                        for (int i = 0; i < bonds1.length(); i++)
                        {
                                bond1 = (JOEBond) bonds1.get(i);
                                for (int j = 0; j < bonds2.length(); j++)
                                {
                                        bond2 = (JOEBond) bonds2.get(j);
                                        if (bond1.getNbrAtom(atom1) == bond2.getNbrAtom(atom2))
                                                return (true);
                                }
                        }
        */
        return (false);
    }

    /**
     *  Returns <tt>true</tt> if this is a oxygen atom.
     *
     * @return    The oxygen value
     */
    public boolean isOxygen()
    {
        return (_atomicnum == 8);
    }

    /**
     *  Sets the parent molecule for this <tt>JOEAtom</tt> .
     *
     * @param  ptr  the parent molecule for this <tt>JOEAtom</tt>
     */
    public void setParent(JOEMol ptr)
    {
        _parent = ptr;
    }

    /**
     *  Gets the parent attribute of the <tt>JOEAtom</tt> object
     *
     * @return    The parent value
     */
    public JOEMol getParent()
    {
        return (_parent);
    }

    /**
     *  Sets the partial charge of the <tt>JOEAtom</tt> object.
     *
     * @param  pcharge  the new partial charge value
     */
    public void setPartialCharge(double pcharge)
    {
        //System.out.println("set pcharge at "+this.getIdx()+": "+pcharge+" and overwrite "+_pcharge);
        _pcharge = pcharge;
    }

    /**
     *  Gets the partialCharge attribute of the <tt>JOEAtom</tt> object
     *
     * @return    The partialCharge value
     */
    public double getPartialCharge()
    {
        if (_parent == null)
        {
            return (_pcharge);
        }

        //System.out.println("get pcharge at "+this.getIdx()+": "+_pcharge);
        if (!_parent.automaticPartialCharge())
        {
            return (_pcharge);
        }

        if (!_parent.hasPartialChargesPerceived())
        {
            //seed partial charges are set in the atom typing procedure
            JOEPhModel.instance().assignSeedPartialCharge(getParent());

            JOEGastChrg gc = new JOEGastChrg();
            gc.assignPartialCharges(_parent);
        }

        return (_pcharge);
    }

    /**
     *  Returns <tt>true</tt> if phospor is part of an phosphat.
     *
     * @return  <tt>true</tt> if phospor is part of an phosphat
     */
    public boolean isPhosphateOxygen()
    {
        if (!isOxygen())
        {
            return false;
        }

        if (getHvyValence() != 1)
        {
            return false;
        }

        BondIterator bit = this.bondIterator();
        JOEBond bond;
        JOEAtom atom = null;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if ((bond.getNbrAtom(this)).isPhosphorus())
            {
                atom = bond.getNbrAtom(this);

                break;
            }
        }

        if (atom == null)
        {
            return false;
        }

        if (atom.countFreeOxygens() > 2)
        {
            return true;
        }

        //atom is connected to a carbon that has a total
        //of 2 attached free oxygens
        return false;
    }

    /**
     *  Returns <tt>true</tt> if this is a phospor atom.
     *
     * @return    The phosphorus value
     */
    public boolean isPhosphorus()
    {
        return (_atomicnum == 15);
    }

    /**
     * Returns <tt>true</tt> if this is a polar hydrogen atom.
     * If this atom is hydrogen and it has a N,O,P or S atom as neighbour.
     *
     * @return    <tt>true</tt> if this is a polar hydrogen atom
     */
    public boolean isPolarHydrogen()
    {
        if (!isHydrogen())
        {
            return false;
        }

        BondIterator bit = this.bondIterator();
        JOEBond bond;
        JOEAtom atom;

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            atom = bond.getNbrAtom(this);

            if (atom.getAtomicNum() == 7)
            {
                return true;
            }

            if (atom.getAtomicNum() == 8)
            {
                return true;
            }

            if (atom.getAtomicNum() == 15)
            {
                return true;
            }

            if (atom.getAtomicNum() == 16)
            {
                return true;
            }
        }

        return false;
    }

    public void setResidue(JOEResidue res)
    {
        _residue = res;
    }

    public JOEResidue getResidue()
    {
        if (_residue != null)
        {
            return _residue;
        }
        else if (!getParent().hasChainsPerceived())
        {
            //        JOEChainsParser.instance().perceiveChains( getParent() );
            return _residue;
        }
        else
        {
            return null;
        }
    }

    //public final int        getStereo()         {return((int)_stereo);}

    /**
     *  Gets the valence of the <tt>JOEAtom</tt> object.
     *
     * @return    the atom valence
     */
    public final int getValence()
    {
        return ((_bond.size() == 0) ? 0 : _bond.size());
    }

    /**
     *  Returns <tt>true</tt> if this is a carbon atom.
     *
     * @return    The carbon value
     */
    public boolean isCarbon()
    {
        return (_atomicnum == 6);
    }

    /**
     * Get the number of free electrons.
     * If the free electrons are not defined the count of the implicit
     * and explicit bonds are used for calculating the number of free
     * electrons.
     *
     * @return    the number of free electrons
     * @see #FREE_ELECTRONS_NOT_DEF
     */
    public int getFreeElectrons()
    {
        //System.out.print(" 1_");
        if (!_parent.isCorrectedForPH())
        {
            _parent.correctForPH();
        }

        //System.out.print("2_");
        if (_freeElectrons == FREE_ELECTRONS_NOT_DEF)
        {
            int freeEl = 0;
            int sumBO = 0;
            BondIterator bit = this.bondIterator();
            JOEBond bond;

            while (bit.hasNext())
            {
                bond = bit.nextBond();
                sumBO += bond.getBO();
            }

            //    // very primitive ! Only usable for the first/second period with strong 8 electron rule
            //    int           usedEl  = 2 * (sumBO + (this.getImplicitValence() - this.getValence()));
            //    freeEl = 8 - usedEl;
            int hatoms = this.getImplicitValence() - this.getValence();
            int usedEl = sumBO + hatoms + this.getFormalCharge();

            //      System.out.println("usedEl:"+usedEl);
            //      System.out.println("extEl:"+JOEElementTable.instance().getExteriorElectrons(this.getAtomicNum()));
            freeEl = JOEElementTable.instance().getExteriorElectrons(this.getAtomicNum()) -
                usedEl;

            if (freeEl < 0)
            {
                logger.warn("Atom #" + this.getIdx() + "(" +
                    JOEElementTable.instance().getSymbol(this.getAtomicNum()) +
                    ") in " + this._parent.getTitle() + " has " + +freeEl +
                    " electrons. Check formal charge and valence." + "Charge:" +
                    this.getFormalCharge() + " Valence:" + this.getValence() +
                    " Impl. valence:" + this.getImplicitValence());
            }

            return freeEl;
        }
        else
        {
            return _freeElectrons;
        }
    }

    /**
     *  Returns the number of heteroatoms connected to an atom.
     *
     * @return    the number of connected heteroatoms to this atom
     */
    public int getHeteroValence()
    {
        int count = 0;
        BondIterator bit = this.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (!(bond.getNbrAtom(this)).isHeteroatom())
            {
                count++;
            }
        }

        return count;
    }

    /**
     *  Returns the number of non-hydrogens connected to an atom.
     *
     * @return    the number of heavy atoms
     */
    public int getHvyValence()
    {
        int count = 0;
        BondIterator bit = this.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (!(bond.getNbrAtom(this)).isHydrogen())
            {
                count++;
            }
        }

        return count;
    }

    /**
     *  Gets the hybridisation of the <tt>JOEAtom</tt> .
     *
     * @return    the atom hybridisation
     */
    public int getHyb()
    {
        //hybridization is assigned when atoms are typed
        JOEMol mol = getParent();

        if ((mol != null) && !mol.hasHybridizationPerceived())
        {
            JOEAtomTyper.instance().assignHyb(mol);
        }

        return (_hyb);
    }

    /**
     *  Returns <tt>true</tt> if this is a hydrogen atom.
     *
     * @return    The hydrogen value
     */
    public boolean isHydrogen()
    {
        return (_atomicnum == 1);
    }

    /**
     *  Gets the implicit valence attribute of the <tt>JOEAtom</tt> .
     *
     * @return    the implicit valence of this atom
     */
    public int getImplicitValence()
    {
        JOEMol mol = getParent();

        if ((mol != null) && !mol.hasImplicitValencePerceived())
        {
            JOEAtomTyper.instance().assignImplicitValence(getParent());
        }

        return ((int) _impval);
    }

    /**
     * Isotpe value for this atom.
     *
     * @return byte
     */
    public int getIsotope()
    {
        return (_isotope);
    }

    /**
     *  Returns <tt>true</tt> if sulfur is part of a sulfat.
     *
     * @return    <tt>true</tt> if sulfur is part of a sulfat
     */
    public boolean isSulfateOxygen()
    {
        if (!isOxygen())
        {
            return false;
        }

        if (getHvyValence() != 1)
        {
            return false;
        }

        BondIterator bit = this.bondIterator();
        JOEBond bond;
        JOEAtom atom = null;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if ((bond.getNbrAtom(this)).isSulfur())
            {
                atom = bond.getNbrAtom(this);

                break;
            }
        }

        if (atom == null)
        {
            return false;
        }

        if (atom.countFreeOxygens() < 3)
        {
            return false;
        }

        //atom is connected to a carbon that has a total
        //of 2 attached free oxygens
        return true;
    }

    /**
     *  Returns <tt>true</tt> if this is a sulfur atom.
     *
     * @return    The sulfur value
     */
    public boolean isSulfur()
    {
        return (_atomicnum == 16);
    }

    /**
     *  Sets the type of the <tt>JOEAtom</tt> object.
     *
     * @param  type  the new type
     */
    public void setType(String type)
    {
        _type = new String(type);
    }

    /**
     *  Gets the type of the <tt>JOEAtom</tt>.
     * 
     * This is the JOELib internal atom type, which can be used via the
     * look-up table in {@link joelib.data.JOETypeTable} to export molecules to other
     * formats, like Synyl MOL2, MM2, Tinker, etc.
     *
     * @return    the type of this atom
     */
    public String getType()
    {
        JOEMol mol = getParent();

        if ((mol != null) && !mol.hasAtomTypesPerceived())
        {
            JOEAtomTyper.instance().assignTypes(getParent());
        }

        //System.out.println("type "+dataType);
        return (_type);
    }

    /**
     *  Sets the vector of the <tt>JOEAtom</tt> object. Copies the x,y and z
     *  values from the coordinate array into the <tt>XYZVector</tt> .
     */
    public void setVector()
    {
        //    assert (_c != null) == true;
        if (_c != null)
        {
            _v.set(_c[_cidx], _c[_cidx + 1], _c[_cidx + 2]);
        }
    }

    /**
     *  Sets the new vector values of the <tt>JOEAtom</tt> object to the given
     *  values from <tt></tt> .
     *
     * @param  v  the new vector values
     */
    public void setVector(XYZVector v)
    {
        if (_c == null)
        {
            _v.set(v);

            //            _v = v;
        }
        else
        {
            _c[_cidx] = v.x();
            _c[_cidx + 1] = v.y();
            _c[_cidx + 2] = v.z();
        }
    }

    /**
     *  Sets the vector attribute of the <tt>JOEAtom</tt> object
     *
     * @param  x  The new vector value
     * @param  y  The new vector value
     * @param  z  The new vector value
     */
    public void setVector(double x, double y, double z)
    {
        if (_c == null)
        {
            _v.set(x, y, z);
        }
        else
        {
            _c[_cidx] = x;
            _c[_cidx + 1] = y;
            _c[_cidx + 2] = z;
        }
    }

    /**
     *  Gets the vector attribute of the <tt>JOEAtom</tt> object
     *
     * @return    The vector value
     */
    public XYZVector getVector()
    {
        if (_c == null)
        {
            return (_v);
        }

        _v.set(_c, _cidx);

        return (_v);
    }

    /**
     *  Gets the x coordinate of the <tt>JOEAtom</tt> .
     *
     * @return    the x coordinate
     */
    public double getX()
    {
        return (x());
    }

    /**
     *  Gets the y coordinate of the <tt>JOEAtom</tt> .
     *
     * @return    the y coordinate
     */
    public double getY()
    {
        return (y());
    }

    /**
     *  Gets the z coordinate of the <tt>JOEAtom</tt> .
     *
     * @return    the z coordinate
     */
    public double getZ()
    {
        return (z());
    }

    /**
     *  Sum of the bond orders of the bonds to the atom.
     *
     * @return    sum of the bond orders of the bonds to the atom
     */
    public int BOSum()
    {
        int bo;
        int bosum = 0;
        BondIterator bit = this.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            bo = bond.getBO();
            bosum += ((bo < 4) ? (2 * bo) : 3);
        }

        bosum = bosum >> 1;

        return bosum;
    }

    /**
     * Modifies a hydrogen atom to methyl group with three explicit hydrogens.
     *
     * @return    <tt>true</tt> if successfull
     */
    public boolean HtoMethyl()
    {
        if (!isHydrogen())
        {
            return false;
        }

        JOEMol mol = getParent();

        mol.beginModify();

        setAtomicNum(6);
        setType("C3");
        setHyb(3);

        JOEAtom atom = null;
        JOEBond bond;
        NbrAtomIterator nait = nbrAtomIterator();

        if (nait.hasNext())
        {
            atom = nait.nextNbrAtom();
            bond = nait.actualBond();
        }
        else
        {
            mol.endModify();

            return false;
        }

        double br1;

        double br2;
        br1 = JOEElementTable.instance().correctedBondRad(6, 3);
        br2 = JOEElementTable.instance().correctedBondRad(atom.getAtomicNum(),
                atom.getHyb());
        bond.setLength(atom, br1 + br2);

        JOEAtom hatom;
        br2 = JOEElementTable.instance().correctedBondRad(1, 0);

        XYZVector v = new XYZVector();

        for (int j = 0; j < 3; j++)
        {
            hatom = mol.newAtom();
            hatom.setAtomicNum(1);
            hatom.setType("H");

            getNewBondVector(v, br1 + br2);
            hatom.setVector(v);
            mol.addBond(getIdx(), mol.numAtoms(), 1);
        }

        mol.endModify();

        return true;
    }

    /**
     * Returns the K bond order sum of this atom.
     *
     * Means: implicite valence + sum(BO_i - 1)
     *
     * @return    Description of the Return Value
     */
    public int KBOSum()
    {
        int bosum = 0;
        BondIterator bit = this.bondIterator();
        JOEBond bond;

        bosum = getImplicitValence();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.isKDouble())
            {
                bosum++;
            }
            else if (bond.isKTriple())
            {
                bosum += 2;
            }
        }

        return bosum;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/

    /**
     *  Adds a bond to this atom.
     *
     * @param  bond  the new bond to add
     */
    public void addBond(JOEBond bond)
    {
        _bond.add(bond);
    }

    /**
     *  Returns a <tt>BondIterator</tt> for all bonds in this atom.
     *
     * <blockquote><pre>
     * BondIterator bit = atom.bondIterator();
     * JOEBond bond;
     * while (bit.hasNext())
     * {
     *   bond = bit.nextBond();
     *
     * }
     * </pre></blockquote>
     *
     * @return    the <tt>BondIterator</tt> for all bonds in this atom
     * @see #nbrAtomIterator()
     */
    public BondIterator bondIterator()
    {
        return new BondIterator(_bond);
    }

    /**
     *  Deletes all contained informations in this atom.
     */
    public void clear()
    {
        _c = null;
        _parent = null;
        _cidx = 0;
        _flags = 0;
        _idx = 0;
        _hyb = 0;
        _atomicnum = 0;
        _impval = 0;
        _fcharge = 0;
        _isotope = 0;

        //_stereo   = 0;
        _type = new String();
        _pcharge = 0.0f;
        _bond.clear();

        //    _bond.setSize(4);
        _residue = null;
    }

    /**
     *  Clear coordinate array.
     */
    public void clearCoordPtr()
    {
        _c = null;
        _cidx = 0;
    }

    /**
     *  Clone this <tt>JOEAtom</tt> object.
     *
     * @return    cloned atom
     */
    public Object clone()
    {
        return clone(this, new JOEAtom());
    }

    /**
     *  Count the bonds of the bond order <tt>order</tt> .
     *
     * @param  order  the bond <tt>order</tt> of the bonds to count
     * @return        the number of the counted bonds with the given bond <tt>
     *      order</tt>
     */
    public int countBondsOfOrder(int order)
    {
        BondIterator bit = this.bondIterator();
        JOEBond bond;
        int count = 0;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.getBO() == order)
            {
                count++;
            }
        }

        return count;
    }

    /**
     *  Count the number of free oxygens.
     *
     * @return    the number of free oxygens
     */
    public int countFreeOxygens()
    {
        int count = 0;
        BondIterator bit = this.bondIterator();
        JOEBond bond;
        JOEAtom atom;

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            atom = bond.getNbrAtom(this);

            if (atom.isOxygen() && (atom.getHvyValence() == 1))
            {
                count++;
            }
        }

        return count;
    }

    /**
     *  Decrement the implicit valence.
     */
    public void decrementImplicitValence()
    {
        _impval--;
    }

    /**
     *  Delete a bond from this atom.
     *
     * @param  del_b  the bond to delete
     * @return        <tt>true</tt> if the given bond was deleted succesfully
     */
    public boolean deleteBond(JOEBond del_b)
    {
        BondIterator bit = this.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond == del_b)
            {
                bit.remove();

                return true;
            }
        }

        return false;
    }

    public void deleteResidue()
    {
        if (_residue != null)
        {
            _residue = null;
        }
    }

    /**
     * Checks if two atoms are equal.
     *
     * Compares hybridization, charge, isotope, atom type and flags,
     * AND atom index number, partial charge and position.
     *
     * @param type
     * @return
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof JOEAtom)
        {
            return equals((JOEAtom) obj, true);
        }
        else
        {
            return false;
        }
    }

    /**
     * Checks if two atoms are equal.
     *
     * Compares hybridization, charge, isotope, atom type and flags.
     * When <tt>fullComparison</tt> is set to <tt>false</tt> the parent molecule,
     * atom index number, partial charge and position are ignored.
     *
     * @param type
     * @param fullComparison When set to <tt>false</tt> the parent molecule,
     * atom index number, partial charge and position are ignored.
     * @return
     */
    public boolean equals(JOEAtom type, boolean fullComparison)
    {
        if (_hyb != type.getHyb())
        {
            return false;
        }

        if (_atomicnum != type.getAtomicNum())
        {
            return false;
        }

        if (_fcharge != type.getFormalCharge())
        {
            return false;
        }

        if (_type != type.getType())
        {
            return false;
        }

        if (_flags != type.getFlag())
        {
            return false;
        }

        if (fullComparison)
        {
            if (_parent != type.getParent())
            {
                return false;
            }

            if (_idx != type.getIdx())
            {
                return false;
            }

            if (_pcharge != type.getPartialCharge())
            {
                return false;
            }

            if (getVector().x() != type.getVector().x())
            {
                return false;
            }

            if (getVector().y() != type.getVector().y())
            {
                return false;
            }

            if (getVector().z() != type.getVector().z())
            {
                return false;
            }
        }

        return true;
    }

    /**
     *  Returns the number of explicit hydrogens. Returns the number of H atoms.
     *
     * @return    the number of the explicit hydrogens
     */
    public int explicitHydrogenCount()
    {
        int numH = 0;

        JOEAtom atom;
        NbrAtomIterator nait = nbrAtomIterator();

        while (nait.hasNext())
        {
            atom = nait.nextNbrAtom();

            if (atom.isHydrogen())
            {
                numH++;
            }
        }

        return numH;
    }

    /**
     *  Destructor for the <tt>JOEAtom</tt> object.
     */
    public void finalize()
    {
        if (_residue != null)
        {
            _residue.removeAtom(this);
        }
    }

    /**
     * Returns <tt>true</tt> if this atom is part of an alpha
     * beta unsaturated fragment.
     *
     * <p>
     * Search for pattern of type:<br>
     * $(**=:#*)
     * @return               <tt>true</tt> if this atom is part of an alpha
     *                        beta unsaturated fragment
     */
    public boolean hasAlphaBetaUnsat()
    {
        return hasAlphaBetaUnsat(true);
    }

    /**
     * Returns <tt>true</tt> if this atom is part of an alpha
     * beta unsaturated fragment.
     *
     * <p>
     * Search for pattern of type:<br>
     * $(*[*,!P,!S]=:#*) if <tt>includePandS=false</tt>
     * or<br>
     * $(**=:#*) if <tt>includePandS=true</tt>
     *
     * @param  includePandS  if <tt>true</tt>
     * @return               <tt>true</tt> if this atom is part of an alpha
     *                        beta unsaturated fragment
     */
    public boolean hasAlphaBetaUnsat(boolean includePandS)
    {
        JOEAtom at;
        JOEAtom a2;
        NbrAtomIterator nait1 = nbrAtomIterator();

        while (nait1.hasNext())
        {
            at = nait1.nextNbrAtom();

            if (includePandS || (!at.isPhosphorus() && !at.isSulfur()))
            {
                NbrAtomIterator nait2 = at.nbrAtomIterator();

                while (nait2.hasNext())
                {
                    a2 = nait2.nextNbrAtom();

                    if ((a2 != this) &&
                            ((nait2.actualBond().getBO() == 2) ||
                            (nait2.actualBond().getBO() == 3) ||
                            (nait2.actualBond().getBO() == 5)))
                    {
                        return (true);
                    }
                }
            }
        }

        return (false);
    }

    /**
     * Returns <tt>true</tt> if atom has an aromatic bond.
     *
     * @return  <tt>true</tt> if atom has an aromatic bond
     */
    public boolean hasAromaticBond()
    {
        return (hasBondOfOrder(4));
    }

    /**
     * Returns <tt>true</tt> if atom has bond of given <tt>order</tt>.
     *
     * @param  order  The bond order
     * @return        <tt>true</tt> if atom has bond of given <tt>order</tt>
     */
    public boolean hasBondOfOrder(int order)
    {
        BondIterator bit = this.bondIterator();
        JOEBond bond;

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if (bond.getBO() == order)
            {
                return (true);
            }
        }

        return (false);
    }

    /**
     * Returns <tt>true</tt> if the chirality was specified for this atom.
     *
     * @return  <tt>true</tt> if the chirality was specified for this atom
     */
    public boolean hasChiralitySpecified()
    {
        return (hasFlag(JOE_CSTEREO_ATOM | JOE_ACSTEREO_ATOM));
    }

    /**
     * Returns <tt>true</tt> if atom has a double bond.
     *
     * @return  <tt>true</tt> if atom has a double bond
     */
    public boolean hasDoubleBond()
    {
        return (hasBondOfOrder(2));
    }

    public boolean hasResidue()
    {
        return (_residue != null);
    }

    /**
     * Returns <tt>true</tt> if atom has a single bond.
     *
     * @return  <tt>true</tt> if atom has a single bond
     */
    public boolean hasSingleBond()
    {
        return (hasBondOfOrder(1));
    }

    /**
     * Calculates the hashcode for an atom.
     *
     * Includes hybridization, charge, isotope, atom type and flags.
     * Excludes atom index number, partial charge and position.
     *
     * @param type
     * @return
     */
    public synchronized int hashCode()
    {
        if (hash != 0)
        {
            return hash;
        }

        int hashCode = _atomicnum;

        hashCode = (31 * hashCode) + _hyb;
        hashCode = (31 * hashCode) + _type.hashCode();
        hashCode = (31 * hashCode) + _flags;

        long bits;
        int tmpI;

        // similar code of Double.hashcode() !!!
        // without need to get a Double instance
        bits = Double.doubleToLongBits(_fcharge);

        // unsigned right shift operator
        tmpI = (int) (bits ^ (bits >>> 32));
        hashCode = (31 * hashCode) + tmpI;

        hash = hashCode;

        return hashCode;
    }

    /**
     *  Returns the number of implicit hydrogens. handles H,C,N,S,O,X
     *
     * @return    implicite hydrogen count
     */
    public int implicitHydrogenCount()
    {
        JOEMol mol = getParent();

        if ((mol != null) && !mol.hasImplicitValencePerceived())
        {
            JOEAtomTyper.instance().assignImplicitValence(getParent());
        }

        int impval = _impval - getHvyValence();

        return ((impval > 0) ? impval : 0);
    }

    /**
     *  Increment the implicit valence.
     */
    public void incrementImplicitValence()
    {
        _impval++;
    }

    /**
     *  Insert a bond to this atom.
     *
     * @param  it    Description of the Parameter
     * @param  bond  Description of the Parameter
     */
    public void insertBond(VectorIterator it, JOEBond bond)
    {
        if ((it instanceof NbrAtomIterator) || (it instanceof BondIterator))
        {
            it.insert(bond);
        }
        else
        {
            logger.error(
                "Iterator must be of type BondIterator or NbrAtomIterator");

            //throw new IteratorException("Iterator must be of type BondIterator or NbrAtomIterator");
        }

        //????
        //_bond.set(it,bond);
    }

    /**
     *  Returns the number of rings in which this atom is contained.
     *
     * @return    Description of the Return Value
     */
    public int memberOfRingCount()
    {
        int count = 0;

        if (!(getParent()).hasSSSRPerceived())
        {
            //            System.out.println("11111: "+((JOERingData) (getParent().getData(JOEDataType.JOE_RING_DATA))));
            (getParent()).findSSSR();

            //            System.out.println("22222: "+((JOERingData) (getParent().getData(JOEDataType.JOE_RING_DATA))));
        }

        //        System.out.println("    is ring ");
        if (!hasFlag(JOE_RING_ATOM))
        {
            //                System.out.println(" no ");
            return 0;
        }

        //        System.out.println(" yes ");
        //        System.out.println("JOE_RING_DATA4 "+
        //                           ((JOERingData) (getParent().getData(JOEDataType.JOE_RING_DATA))));
        JOERingData rData = ((JOERingData) (getParent().getData(JOEDataType.JOE_RING_DATA)));

        if (rData == null)
        {
            logger.error("could not get JOERingData data.");
            System.exit(1);

            //      throw new Exception("could not get SSSR data.");
        }

        Vector rings = rData.getData();

        if (rings == null)
        {
            logger.error("could not get SSSR data.");
            System.exit(1);

            //      throw new Exception("could not get SSSR data.");
        }

        if (!hasFlag(JOE_RING_ATOM))
        {
            return 0;
        }

        RingIterator rit = (getParent()).getSSSRIterator(rings);
        JOERing ring;

        while (rit.hasNext())
        {
            ring = rit.nextRing();

            if (ring.isInRing(getIdx()))
            {
                count++;
            }
        }

        return count;
    }

    /**
     *  Returns a <tt>NbrAtomIterator</tt> for all neighbour atoms in this atom.
     *
     * <blockquote><pre>
     * NbrAtomIterator nait = atom.nbrAtomIterator();
     * JOEBond bond;
     * JOEAtom nbrAtom;
     * while (nait.hasNext())
     * {
     *          nbrAtom=nait.nextNbrAtom();
     *   bond = nait.actualBond();
     *
     * }
     * </pre></blockquote>
     *
     * @return    the <tt>NbrAtomIterator</tt> for all neighbour atoms in this atom
     * @see #bondIterator()
     */
    public NbrAtomIterator nbrAtomIterator()
    {
        return new NbrAtomIterator(_bond, this);
    }

    public void newResidue()
    {
        if (_residue == null)
        {
            _residue = new JOEResidue();
        }
    }

    public synchronized int reHash()
    {
        hash = 0;

        return hashCode();
    }

    public double smallestBondAngle()
    {
        JOEAtom b;
        JOEAtom c;
        XYZVector v1;
        XYZVector v2;
        double degrees;
        double minDegrees;

        minDegrees = 360.0;

        JOEBond bond;

        for (int j = 0; j < _bond.size(); j++)
        {
            bond = ((JOEBond) _bond.get(j));
            b = bond.getNbrAtom(this);

            for (int k = j + 1; k < _bond.size(); k++)
            {
                if (k >= _bond.size())
                {
                    break;
                }

                bond = ((JOEBond) _bond.get(k));
                c = bond.getNbrAtom(this);

                v1 = b.getVector().sub(this.getVector());
                v2 = c.getVector().sub(this.getVector());
                degrees = XYZVector.angle(v1, v2);

                if (degrees < minDegrees)
                {
                    minDegrees = degrees;
                }
            }
        }

        return minDegrees;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String toString()
    {
        return JOEElementTable.instance().getSymbol(this.getAtomicNum());
    }

    /**
     *  Clears the aromatic flag of this atom.
     */
    public void unsetAromatic()
    {
        _flags &= (~(JOE_AROMATIC_ATOM));
    }

    /**
     *  Clears the stereo flag of this atom.
     */
    public void unsetStereo()
    {
        _flags &= ~(JOE_ACSTEREO_ATOM);
        _flags &= ~(JOE_CSTEREO_ATOM);
        _flags &= ~(JOE_CHIRAL_ATOM);
    }

    /**
     *  Gets the x coordinate of the <tt>JOEAtom</tt> .
     *
     * @return    the x coordinate
     */
    public double x()
    {
        //  	if(_cidx>_c.length-3)
        //  	{
        //  		logger.error("Index "+_cidx+" exceeds coordinate array with length "+_c.length);
        //    return 0.0;
        //  	}
        if (_c != null)
        {
            return _c[_cidx];
        }
        else
        {
            return _v._vx;
        }
    }

    /**
     *  Gets the y coordinate of the <tt>JOEAtom</tt> .
     *
     * @return    the y coordinate
     */
    public double y()
    {
        //  	if(_cidx>_c.length-3)
        //  	{
        //  		logger.error("Index "+_cidx+" exceeds coordinate array with length "+_c.length);
        //    return 0.0;
        //  	}
        if (_c != null)
        {
            return _c[_cidx + 1];
        }
        else
        {
            return _v._vy;
        }
    }

    /**
     *  Gets the z coordinate of the <tt>JOEAtom</tt> .
     *
     * @return    the z coordinate
     */
    public double z()
    {
        //  	if(_cidx>_c.length-3)
        //  	{
        //  		logger.error("Index "+_cidx+" exceeds coordinate array with length "+_c.length);
        //    return 0.0;
        //  	}
        if (_c != null)
        {
            return _c[_cidx + 2];
        }
        else
        {
            return _v._vz;
        }
    }

    /*-------------------------------------------------------------------------*
      * protected methods
      *-------------------------------------------------------------------------     */

    /**
     *  Gets the flag attribute of the JOEAtom object
     *
     * @return    The flag value
     * @see       joelib.molecule.JOEAtom#JOE_4RING_ATOM
     * @see       joelib.molecule.JOEAtom#JOE_3RING_ATOM
     * @see       joelib.molecule.JOEAtom#JOE_AROMATIC_ATOM
     * @see       joelib.molecule.JOEAtom#JOE_RING_ATOM
     * @see       joelib.molecule.JOEAtom#JOE_CSTEREO_ATOM
     * @see       joelib.molecule.JOEAtom#JOE_ACSTEREO_ATOM
     * @see       joelib.molecule.JOEAtom#JOE_ACCEPTOR_ATOM
     * @see       joelib.molecule.JOEAtom#JOE_CHIRAL_ATOM
     */
    protected final int getFlag()
    {
        return (_flags);
    }

    /**
     *  Sets the flag attribute of the JOEAtom object
     *
     * @param  flag  The new flag value
     * @see          joelib.molecule.JOEAtom#JOE_4RING_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_3RING_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_AROMATIC_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_RING_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_CSTEREO_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_ACSTEREO_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_ACCEPTOR_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_CHIRAL_ATOM
     */
    protected void setFlag(int flag)
    {
        _flags |= flag;
    }

    /**
     *  Description of the Method
     *
     * @param  flag  Description of the Parameter
     * @return       Description of the Return Value
     * @see          joelib.molecule.JOEAtom#JOE_4RING_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_3RING_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_AROMATIC_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_RING_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_CSTEREO_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_ACSTEREO_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_ACCEPTOR_ATOM
     * @see          joelib.molecule.JOEAtom#JOE_CHIRAL_ATOM
     */
    protected boolean hasFlag(int flag)
    {
        return (((_flags & flag) != 0) ? true : false);
    }

    /*-------------------------------------------------------------------------*
     * static public methods
     *-------------------------------------------------------------------------*/

    /**
     *  Apply rotation matrix <tt>m</tt> to the bond between atom <tt>a1</tt> and
     *  <tt>a2</tt> in molecule <tt>mol</tt> .
     *
     * @param  mol  molecule
     * @param  m    rotation matrix
     * @param  a1   atom number one
     * @param  a2   atom number two
     */
    static void applyRotMatToBond(JOEMol mol, Matrix3x3 m, JOEAtom a1,
        JOEAtom a2)
    {
        Vector children = new Vector();

        // of type int[1]
        mol.findChildren(children, a1.getIdx(), a2.getIdx());
        children.add(new int[]{a2.getIdx()});

        XYZVector v;

        for (int i = 0; i < children.size(); i++)
        {
            v = mol.getAtom(((int[]) children.get(i))[0]).getVector();
            v.subing(a1.getVector());
            v.muling(m);
            v.adding(a1.getVector());
            mol.getAtom(((int[]) children.get(i))[0]).setVector(v);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
