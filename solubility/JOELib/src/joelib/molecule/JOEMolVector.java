///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEMolVector.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.19 $
//            $Date: 2004/07/25 20:43:24 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.molecule;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.JOEFileFormat;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;
import joelib.io.SimpleReader;


/**
 * Molecule vector to load single and multiple molecule files.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.19 $, $Date: 2004/07/25 20:43:24 $
 *
 * @see #joelib.io.IOHelper
 */
public class JOEMolVector implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.molecule.JOEMolVector");
    private static final int DEFAULT_SIZE = 100;

    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  {@link java.util.Vector} of <tt>JOEMol</tt>
     */
    Vector _molvec;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Creates an empty molecule vector.
     */
    public JOEMolVector()
    {
        _molvec = new Vector();
    }

    /**
     *  Constructor for the JOEMolVector object
     *
     * @param  ifs                        Description of the Parameter
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public JOEMolVector(InputStream ifs) throws IOException
    {
        read(ifs, IOTypeHolder.instance().getIOType("SDF"),
            IOTypeHolder.instance().getIOType("SDF"), -1);
    }

    /**
     *  Constructor for the JOEMolVector object
     *
     * @param  ifs                        Description of the Parameter
     * @param  nToRead                    Description of the Parameter
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public JOEMolVector(InputStream ifs, int nToRead) throws IOException
    {
        read(ifs, IOTypeHolder.instance().getIOType("SDF"),
            IOTypeHolder.instance().getIOType("SDF"), nToRead);
    }

    /**
     *  Constructor for the JOEMolVector object
     *
     * @param  ifs                        Description of the Parameter
     * @param  in_type                    Description of the Parameter
     * @param  out_type                   Description of the Parameter
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public JOEMolVector(InputStream ifs, final IOType in_type,
        final IOType out_type) throws IOException
    {
        read(ifs, in_type, out_type, -1);
    }

    /**
     *  Constructor for the JOEMolVector object
     *
     * @param  ifs                        Description of the Parameter
     * @param  in_type                    Description of the Parameter
     * @param  out_type                   Description of the Parameter
     * @param  nToRead                    Description of the Parameter
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public JOEMolVector(InputStream ifs, final IOType in_type,
        final IOType out_type, int nToRead)
        throws IOException, MoleculeIOException
    {
        read(ifs, in_type, out_type, nToRead);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Sets the molecule at position <tt>i</tt> of this molecule vector.
     *
     * @param  i    The new mol value
     * @param  mol  The new mol value
     */
    public Object setMol(int i, JOEMol mol)
    {
        return _molvec.set(i, mol);
    }

    /**
     *  Get a specific molecule from a OEMolVector. Index starts at zero.
     *
     * @param  i  Description of the Parameter
     * @return    The mol value
     */
    public JOEMol getMol(int i)
    {
        if (_molvec == null)
        {
            return null;
        }

        if ((i >= 0) && (i < _molvec.size()))
        {
            return (JOEMol) _molvec.get(i);
        }
        else
        {
            logger.error("Index " + i +
                " out of range in JOEMolVector.getMol ");

            return (null);
        }
    }

    /**
     *  Gets the size attribute of the JOEMolVector object
     *
     * @return    The size value
     */
    public int getSize()
    {
        if (_molvec == null)
        {
            return -1;
        }

        return (_molvec.size());
    }

    /**
     * @return    {@link java.util.Vector} of <tt>JOEMol</tt>
     */
    public Vector getVector()
    {
        return (_molvec);
    }

    /**
     * Add molecule to this molecule vector.
     *
     * @param  i    The new mol value
     * @param  mol  The new mol value
     */
    public void addMol(JOEMol mol)
    {
        _molvec.add(mol);
    }

    /**
     *  Functions for dealing with groups of molecules. MolVec will read either
     *  all molecules from a file or a set of conformers.
     */
    public void finalize()
    {
        //        for (int i = 0; i < _molvec.size(); i++)
        //        {
        //            Object obj = _molvec.get(i);
        //            obj = null;
        //        }
        _molvec.clear();
    }

    /**
     *  Read all molecules from a file into a <code>Vector</code> of <code>JOEMol</code>. Input and output
     *  types default to SDF.
     *
     * @param  ifs                        Description of the Parameter
     * @param  in_type                    Description of the Parameter
     * @param  out_type                   Description of the Parameter
     * @param  nToRead                    Description of the Parameter
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public void read(InputStream ifs, final IOType in_type,
        final IOType out_type, int nToRead) throws IOException
    {
        if (nToRead == -1)
        {
            _molvec = new Vector(DEFAULT_SIZE);
        }
        else
        {
            _molvec = new Vector(nToRead);
        }

        int nRead = 0;
        SimpleReader reader = null;

        //        try
        //        {
        reader = new SimpleReader(ifs, in_type);

        //        }
        //        catch (IOException ex)
        //        {
        //            throw ex;
        //        }
        JOEMol mol;

        while (true)
        {
            // load only nToRead molecules
            if (nRead == nToRead)
            {
                break;
            }

            // create empty molecule
            mol = new JOEMol(in_type, out_type);

            // load next molecule
            try
            {
                if (!reader.readNext(mol))
                {
                    break;
                }
            }
             catch (IOException ex)
            {
                throw ex;
            }
             catch (MoleculeIOException mex)
            {
                logger.warn("Skipping molecule entry. " + mex.getMessage());
            }

            // increment molecule counter and load next molecule
            nRead++;
            _molvec.add(mol);
        }
    }

    /**
     *  Write a OEMolVector to a file. Output type defaults to SDF.
     *
     * @param  ofs                        Description of the Parameter
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public void write(OutputStream ofs) throws IOException, MoleculeIOException
    {
        IOType type = IOTypeHolder.instance().getIOType("SDF");
        write(ofs, type);
    }

    /**
     *  Write a OEMolVector to a file. Output type defaults to SDF.
     *
     * @param  ofs                        Description of the Parameter
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public void write(OutputStream ofs, IOType type)
        throws IOException, MoleculeIOException
    {
        MoleculeFileType writer = null;

        writer = JOEFileFormat.getMolWriter(ofs, type);

        if (!writer.writeable())
        {
            logger.warn(type.getRepresentation() + " is not writeable.");

            return;
        }

        JOEMol mol;

        for (int i = 0; i < _molvec.size(); i++)
        {
            mol = (JOEMol) _molvec.get(i);

            //System.out.println("Store "+mol);
            writer.write(mol, mol.getTitle());
        }
    }

    //  public boolean readConfs(InputStream ifs, final IOType in_type, final IOType out_type)
    //  {
    //  JOEMol mol;
    //  JOEFileFormat ff;
    //  String title,master;
    //
    //  _molvec.resize(0);
    //
    //  int i = 1;
    //  while (1)
    //    {
    //      mol = new OEMol;
    //      mol.setInputType(in_type);
    //      mol.setOutputType(out_type);
    //      streampos sp = ifs.tellg();
    //      ff.readMolecule(ifs,*mol);
    //      if (mol.numAtoms() == 0)
    //        {
    //          mol=null;
    //	  return(false);
    //        }
    //
    //      title = mol.getTitle();
    //      if (i == 1)
    //        {
    //          master = title;
    //          _molvec.put(mol);
    //        }
    //      else
    //        {
    //          if (title == master)
    //	    _molvec.put(mol);
    //          else
    //            {
    //              ifs.seekg(sp);
    //              mol = mol;
    //              break;
    //            }
    //        }
    //      i++;
    //    }
    //  return(true);
    //  }
    //
    //  public boolean readConfs(InputStream ifs)
    //
    //  {
    //    boolean retval = readConfs(ifs, IOType.SDF, IOType.SDF);
    //    return retval;
    //  }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
