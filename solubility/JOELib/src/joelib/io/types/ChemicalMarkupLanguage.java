///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ChemicalMarkupLanguage.java,v $
//  Purpose:  Reader/Writer for CML files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.33 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//  Original Author: steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu, egonw@sci.kun.nl
//  Original Version: Chemical Development Kit,  http://sourceforge.net/projects/cdk
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import joelib.io.MoleculeFileType;
import joelib.io.PropertyWriter;
import joelib.io.types.cml.CDOInterface;
import joelib.io.types.cml.CMLErrorHandler;
import joelib.io.types.cml.CMLHandler;
import joelib.io.types.cml.CMLMoleculeWriter;
import joelib.io.types.cml.CMLResolver;
import joelib.io.types.cml.CMLWriterProperties;
import joelib.io.types.cml.MoleculeArray;
import joelib.io.types.cml.MoleculeAttributeArray;
import joelib.io.types.cml.MoleculeFileCDO;
import joelib.io.types.cml.MoleculeHuge;
import joelib.io.types.cml.MoleculeLarge;
import joelib.molecule.JOEMol;
import joelib.molecule.JOEMolVector;
import joelib.util.JHM;

import org.apache.log4j.Category;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import wsi.ra.tool.PropertyHolder;


/**
 * Reader/Writer for Chemical Markup Language (CML) files.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.33 $, $Date: 2004/07/25 20:43:19 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public class ChemicalMarkupLanguage implements MoleculeFileType, PropertyWriter,
    CMLWriterProperties
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.ChemicalMarkupLanguage");
    private final static String description = new String(
            "Chemical Markup Language (CML)");
    private final static String[] extensions = new String[]{"cml"};
    public static final int OUTPUT_HUGE = 0;
    public static final int OUTPUT_LARGE = 1;
    public static final int OUTPUT_ARRAY = 2;
    public static final int OUTPUT_ATTRIBUTE_ARRAY = 3;
    public static final String OUTPUT_HUGE_S = "huge";
    public static final String OUTPUT_LARGE_S = "large";
    public static final String OUTPUT_ARRAY_S = "array";
    public static final String OUTPUT_ATTRIBUTE_ARRAY_S = "attributearray";
    public static final float CML_VERSION_1 = 1.0f;
    public static final float CML_VERSION_2 = 2.0f;
    public static final String DEFAULT_DELIMITER = " ";
    private static String DEFAULT_NAMESPACE = "cml";
    private static String DEFAULT_XML_DECLARATION = "http://www.xml-cml.org/schema/cml2/core";
    private static float cmlDefaultVersion = CML_VERSION_2;
    private static String defaultDelimiter = DEFAULT_DELIMITER;

    //~ Instance fields ////////////////////////////////////////////////////////

    private CMLMoleculeWriter cmlOutputWriter = null;

    //    private ContentHandler handler;
    //	private EntityResolver resolver;
    private InputStreamReader isr;
    private JOEMolVector molVector = new JOEMolVector();
    private LineNumberReader lnr;
    private MoleculeFileCDO cdo;
    private Pattern endPattern = Pattern.compile("</.*:molecule.*>");
    private Pattern startPattern = Pattern.compile("<.*:molecule.*>");
    private PrintStream ps;
    private String dtdResourceDir;
    private String namespace = DEFAULT_NAMESPACE;
    private String xmlDeclaration = DEFAULT_XML_DECLARATION;
    private XMLReader parser;
    private boolean done;
    private boolean forceFormalCharge = false;
    private boolean fragment;
    private boolean impliciteHydrogens = false;
    private boolean moleculeReaded = false;
    private boolean partialCharge = false;
    private boolean storeChemistryKernelInfo = true;
    private boolean symmetryInformations = false;
    private boolean useNamespace = true;
    private boolean useSlowerMemorySavingPreparser = false;
    private int cmlOutputType = OUTPUT_HUGE;
    private int molReadedIndex;

    //~ Methods ////////////////////////////////////////////////////////////////

    public float getCMLversion()
    {
        return cmlDefaultVersion;
    }

    public static String getDefaultDelimiter()
    {
        return defaultDelimiter;
    }

    public String getNamespace()
    {
        return this.namespace;
    }

    public String getXMLDeclaration()
    {
        return xmlDeclaration;
    }

    public void closeReader() throws IOException
    {
    }

    public void closeWriter() throws IOException
    {
        ps.println("\n</list>");
        ps.close();
    }

    public boolean forceWriteFormalCharge()
    {
        return forceFormalCharge;
    }

    /**
     *  Description of the Method
     *
     * @param iStream          Description of the Parameter
     * @exception IOException  Description of the Exception
     */
    public void initReader(InputStream iStream) throws IOException
    {
        initProperties();

        isr = new InputStreamReader(iStream);

        if (useSlowerMemorySavingPreparser)
        {
            lnr = new LineNumberReader(isr);
        }

        initSAXparser(true);

        moleculeReaded = false;
        molReadedIndex = 0;
        cdo.setMoleculeSetOfMolecules(molVector);

        //cdo.setMoleculeCallback(this);
    }

    /**
     *  Description of the Method
     *
     * @param os               Description of the Parameter
     * @exception IOException  Description of the Exception
     */
    public void initWriter(OutputStream os) throws IOException
    {
        initProperties();

        //        ps = new OutputStreamWriter(os);
        ps = new PrintStream(os);

        done = false;
        fragment = false;

        switch (cmlOutputType)
        {
        case OUTPUT_HUGE:
            cmlOutputWriter = new MoleculeHuge(ps, this);

            break;

        case OUTPUT_LARGE:
            cmlOutputWriter = new MoleculeLarge(ps, this);

            break;

        case OUTPUT_ARRAY:
            cmlOutputWriter = new MoleculeArray(ps, this);

            break;

        case OUTPUT_ATTRIBUTE_ARRAY:
            cmlOutputWriter = new MoleculeAttributeArray(ps, this);

            break;
        }

        //System.out.println(cmlOutputWriter.getClass().getName());
        ps.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");

        //		ps.println("<?xml version=\""+cmlDefaultVersion+"\" encoding=\"ISO-8859-1\"?>");
        ps.println("<!DOCTYPE molecule SYSTEM \"cml.dtd\" []>");
        ps.println("<list>");
    }

    /**
     * Description of the Method
     *
     * @return   Description of the Return Value
     */
    public String inputDescription()
    {
        return description;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public String[] inputFileExtensions()
    {
        return extensions;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public String outputDescription()
    {
        return description;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public String[] outputFileExtensions()
    {
        return extensions;
    }

    /**
     *  Reads an molecule entry as (unparsed) <tt>String</tt> representation.
     *
     * @return                  <tt>null</tt> if the reader contains no more
     *      relevant data. Otherwise the <tt>String</tt> representation of the
     *      whole molecule entry is returned.
     * @exception  IOException  typical IOException
     */
    public String read() throws IOException
    {
        StringBuffer molecule = null;
        String line;

        while ((line = lnr.readLine()) != null)
        {
            Matcher endMatcher = endPattern.matcher(line);
            Matcher startMatcher = startPattern.matcher(line);

            if (endMatcher.matches())
            {
                //System.out.println(line+" ::: "+matcher.matches()+" "+matcher.end());
                molecule.append(line.substring(0, endMatcher.end()));

                break;
            }
            else if (startMatcher.matches())
            {
                //System.out.println(line+" ::: "+matcher.matches()+" "+matcher.start());
                molecule = new StringBuffer(10000);
                molecule.append(line.substring(startMatcher.start()));
                molecule.append(JHM.eol);
            }
            else
            {
                if (molecule != null)
                {
                    molecule.append(line);
                    molecule.append(JHM.eol);
                }
            }
        }

        if (line == null)
        {
            return null;
        }
        else
        {
            //System.out.println("##################\n"+molecule);
            return molecule.toString();
        }
    }

    /**
     *  Description of the Method
     *
     * @param mol              Description of the Parameter
     * @return                 Description of the Return Value
     * @exception IOException  Description of the Exception
     */
    public synchronized boolean read(JOEMol mol) throws IOException
    {
        return read(mol, null);
    }

    /**
     * Loads an molecule in MDL SD-MOL format and sets the title.
     * If <tt>title</tt> is <tt>null</tt> the title line in
     * the molecule file is used.
     *
     * @param mol              Description of the Parameter
     * @param title            Description of the Parameter
     * @return                 Description of the Return Value
     * @exception IOException  Description of the Exception
     */
    public synchronized boolean read(JOEMol mol, String title)
        throws IOException
    {
        if (useSlowerMemorySavingPreparser)
        {
            String molecule = read();

            if (molecule == null)
            {
                return false;
            }

            initSAXparser(false);

            StringReader stringReader = new StringReader(molecule);
            cdo.setMolecule(mol);
            startSAXparser(stringReader);

            return true;
        }
        else
        {
            if (!moleculeReaded)
            {
                cdo.setMolecule(mol);
                startSAXparser(isr);

                // and send signal for finishing outer loop
                // in the next round!!!
                moleculeReaded = true;
                molReadedIndex = 0;
            }

            if ((molReadedIndex < this.molVector.getSize()))
            {
                mol.set(this.molVector.getMol(molReadedIndex));
                molReadedIndex++;

                return true;
            }
        }

        return false;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public boolean readable()
    {
        return true;
    }

    public boolean skipReaderEntry() throws IOException
    {
        return true;
    }

    public boolean storeChemistryKernelInfo()
    {
        return storeChemistryKernelInfo;
    }

    public boolean useNamespace()
    {
        return this.useNamespace;
    }

    /**
     *  Description of the Method
     *
     * @param mol              Description of the Parameter
     * @return                 Description of the Return Value
     * @exception IOException  Description of the Exception
     */
    public boolean write(JOEMol mol) throws IOException
    {
        return write(mol, "Undefined");
    }

    /**
     *  Description of the Method
     *
     * @param mol              Description of the Parameter
     * @param title            Description of the Parameter
     * @return                 Description of the Return Value
     * @exception IOException  Description of the Exception
     */
    public boolean write(JOEMol mol, String title) throws IOException
    {
        return write(mol, title, true, null);
    }

    /**
     *  Writes a molecule with his <tt>JOEPairData</tt> .
     *
     * @param  mol              the molecule with additional data
     * @param  title            the molecule title or <tt>null</tt> if the title
     *      from the molecule should be used
     * @param  writePairData    if <tt>true</tt> then the additional molecule data
     *      is written
     * @param  attribs2write    Description of the Parameter
     * @return                  <tt>true</tt> if the molecule and the data has
     *      been succesfully written.
     * @exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol, String title, boolean writePairData,
        Vector attribs2write) throws IOException
    {
        //        if (!done)
        //        {
        //            if (!fragment)
        //            {
        //		ps.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
        //		ps.println("<?xml version=\""+cmlDefaultVersion+"\" encoding=\"ISO-8859-1\"?>");
        //		ps.println("<!DOCTYPE molecule SYSTEM \"cml.dtd\" []>");
        //            }
        //            if (object instanceof SetOfMolecules)
        //            {
        //                write((SetOfMolecules) object);
        //            }
        //            else if (object instanceof Molecule)
        //            {
        //                write((Molecule) object);
        cmlOutputWriter.writeMolecule(mol, writePairData, attribs2write);

        //            }
        //            else
        //            {
        //                throw new UnsupportedChemObjectException("Only supported are SetOfMolecules and Molecule.");
        //            }
        //            if (!fragment)
        //            {
        //                done = true;
        //            }
        //        }
        //        else
        //        {
        //        }
        return true;
    }

    public boolean writeImpliciteHydrogens()
    {
        return impliciteHydrogens;
    }

    public boolean writePartialCharge()
    {
        return partialCharge;
    }

    public boolean writeSymmetryInformations()
    {
        return symmetryInformations;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public boolean writeable()
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @exception  IOException  Description of the Exception
     */
    private void initProperties()
    {
        String value;

        value = PropertyHolder.instance().getProperty(this, "output");

        if (value == null)
        {
            cmlOutputType = OUTPUT_HUGE;
        }
        else if (value.equalsIgnoreCase(OUTPUT_HUGE_S))
        {
            cmlOutputType = OUTPUT_HUGE;
        }
        else if (value.equalsIgnoreCase(OUTPUT_LARGE_S))
        {
            cmlOutputType = OUTPUT_LARGE;
        }
        else if (value.equalsIgnoreCase(OUTPUT_ARRAY_S))
        {
            cmlOutputType = OUTPUT_ARRAY;
        }
        else if (value.equalsIgnoreCase(OUTPUT_ATTRIBUTE_ARRAY_S))
        {
            cmlOutputType = OUTPUT_ATTRIBUTE_ARRAY;
        }
        else
        {
            logger.error("Use output type :" + OUTPUT_HUGE_S + ", " +
                OUTPUT_LARGE_S + " and " + OUTPUT_ARRAY_S);
            cmlOutputType = OUTPUT_HUGE;
        }

        //System.out.println("output: '"+value+"' "+cmlOutputType);
        value = PropertyHolder.instance().getProperty(this,
                "output.force.formalCharge");

        if (((value != null) && value.equalsIgnoreCase("true")))
        {
            forceFormalCharge = true;
        }
        else
        {
            forceFormalCharge = false;
        }

        value = PropertyHolder.instance().getProperty(this,
                "output.partialCharge");

        if (((value != null) && value.equalsIgnoreCase("true")))
        {
            partialCharge = true;
        }
        else
        {
            partialCharge = false;
        }

        value = PropertyHolder.instance().getProperty(this,
                "output.hydrogenCount");

        if (((value != null) && value.equalsIgnoreCase("true")))
        {
            impliciteHydrogens = true;
        }
        else
        {
            impliciteHydrogens = false;
        }

        value = PropertyHolder.instance().getProperty(this,
                "output.symmetryInformations");

        if (((value != null) && value.equalsIgnoreCase("true")))
        {
            symmetryInformations = true;
        }
        else
        {
            symmetryInformations = false;
        }

        value = PropertyHolder.instance().getProperty(this,
                "output.useNamespace");

        if (((value != null) && value.equalsIgnoreCase("true")))
        {
            useNamespace = true;
        }
        else
        {
            useNamespace = false;
        }

        value = PropertyHolder.instance().getProperty(this,
                "output.storeChemistryKernelInfo");

        if (((value != null) && value.equalsIgnoreCase("true")))
        {
            storeChemistryKernelInfo = true;
        }
        else
        {
            storeChemistryKernelInfo = false;
        }

        value = PropertyHolder.instance().getProperty(this,
                "useSlowerMemorySavingPreparser");

        if (((value != null) && value.equalsIgnoreCase("true")))
        {
            useSlowerMemorySavingPreparser = true;
        }
        else
        {
            useSlowerMemorySavingPreparser = false;
        }

        //logger.info("useSlowerMemorySavingPreparser: "+useSlowerMemorySavingPreparser);
        dtdResourceDir = PropertyHolder.instance().getProperty(this,
                "DTD.resourceDir");

        double dTmp = PropertyHolder.instance().getDouble(this,
                "output.defaultVersion", 0);

        if (!Double.isNaN(dTmp))
        {
            cmlDefaultVersion = (float) dTmp;
        }

        value = PropertyHolder.instance().getProperty(this, "defaultDelimiter");

        if (value != null)
        {
            defaultDelimiter = value;
        }

        value = PropertyHolder.instance().getProperty(this, "output.namespace");

        if (value != null)
        {
            namespace = value;
        }

        value = PropertyHolder.instance().getProperty(this,
                "output.xmlDeclaration");

        if (value != null)
        {
            xmlDeclaration = value;
        }
    }

    private void initSAXparser(boolean verbose) throws IOException
    {
        boolean success = false;

        // Aelfred is prefered.

        /*                if (!success)
                                        {
                                                        try
                                                        {
                                                                        parser = new gnu.xml.aelfred2.XmlReader();
                                                                        logger.info("Using Aelfred2 XML parser.");
                                                                        success = true;
                                                        }
                                                        catch (Exception e)
                                                        {
                                                                        logger.warn("Could not instantiate Aelfred2 XML reader!");
                                                        }
                                        }*/

        // If Aelfred is not available try Xerces
        if (!success)
        {
            try
            {
                parser = new org.apache.xerces.parsers.SAXParser();

                if (verbose)
                {
                    logger.info("Using Xerces XML parser.");
                }

                success = true;
            }
             catch (Exception e)
            {
                logger.warn("Could not instantiate Xerces XML reader!");
            }
        }

        if (!success)
        {
            throw new IOException("Could not instantiate any XML parser!");
        }

        cdo = new MoleculeFileCDO();

        try
        {
            parser.setFeature("http://xml.org/sax/features/validation", false);

            if (verbose)
            {
                logger.info("Deactivated validation");
            }
        }
         catch (SAXException e)
        {
            logger.warn("Cannot deactivate validation.");
        }

        parser.setContentHandler(new CMLHandler((CDOInterface) cdo));
        parser.setEntityResolver(new CMLResolver());
        parser.setErrorHandler(new CMLErrorHandler());
    }

    private void startSAXparser(Reader reader)
    {
        try
        {
            parser.parse(new InputSource(reader));
        }
         catch (IOException e)
        {
            logger.warn("IOException: " + e.toString());
        }
         catch (SAXParseException saxe)
        {
            SAXParseException spe = (SAXParseException) saxe;
            String error = "Found well-formedness error in line " +
                spe.getLineNumber();
            logger.error(error);
        }
         catch (SAXException saxe)
        {
            logger.warn("SAXException: " + saxe.getClass().getName());
            logger.warn(saxe.toString());
            saxe.printStackTrace();
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
