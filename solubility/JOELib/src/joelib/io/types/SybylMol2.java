///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SybylMol2.java,v $
//  Purpose:  Reader/Writer for Undefined files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.16 $
//            $Date: 2004/07/25 20:43:20 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types;

import cformat.PrintfFormat;
import cformat.PrintfStream;
import cformat.ScanfReader;
import java.io.*;
import java.util.StringTokenizer;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOECommentData;
import joelib.data.JOEDataType;
import joelib.data.JOEElementTable;
import joelib.data.JOETypeTable;

import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.JHM;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;


/**
 * Reader/Writer for Sybyl mol2 files.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.16 $, $Date: 2004/07/25 20:43:20 $
 * @cite sybylmol2
 */
public class SybylMol2 implements MoleculeFileType
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.SybylMol2");
    private final static String description = new String("Sybyl Mol2");
    private final static String[] extensions = new String[]{"mol2"};

    //~ Instance fields ////////////////////////////////////////////////////////

    private LineNumberReader lnr;
    private PrintfStream ps;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the Undefined object
     */
    public SybylMol2()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void closeReader() throws IOException
    {
    }

    public void closeWriter() throws IOException
    {
    }

    /**
     *  Description of the Method
     *
     * @param is               Description of the Parameter
     * @exception IOException  Description of the Exception
     */
    public void initReader(InputStream is) throws IOException
    {
        lnr = new LineNumberReader(new InputStreamReader(is));
    }

    /**
     *  Description of the Method
     *
     * @param os               Description of the Parameter
     * @exception IOException  Description of the Exception
     */
    public void initWriter(OutputStream os) throws IOException
    {
        ps = new PrintfStream(os);
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public String inputDescription()
    {
        return description;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public String[] inputFileExtensions()
    {
        return extensions;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public String outputDescription()
    {
        return description;
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public String[] outputFileExtensions()
    {
        return extensions;
    }

    /**
     *  Reads an molecule entry as (unparsed) <tt>String</tt> representation.
     *
     * @return                  <tt>null</tt> if the reader contains no more
     *      relevant data. Otherwise the <tt>String</tt> representation of the
     *      whole molecule entry is returned.
     * @exception  IOException  typical IOException
     */
    public String read() throws IOException
    {
        logger.error(
            "Reading Sybyl mol2 data as String representation is not implemented yet !!!");

        return null;
    }

    /**
     *  Description of the Method
     *
     * @param mol              Description of the Parameter
     * @return                 Description of the Return Value
     * @exception IOException  Description of the Exception
     */
    public boolean read(JOEMol mol) throws IOException, MoleculeIOException
    {
        return read(mol, null);
    }

    /**
     *  Description of the Method
     *
     * @param mol              Description of the Parameter
     * @param title            Description of the Parameter
     * @return                 Description of the Return Value
     * @exception IOException  Description of the Exception
     */
    public boolean read(JOEMol mol, String title)
        throws IOException, MoleculeIOException
    {
        boolean foundAtomLine = false;
        String comment = null;
        String str;
        String str1;

        // of type String
        Vector vstr = new Vector();
        String line;

        mol.beginModify();

        for (;;)
        {
            if ((line = lnr.readLine()) == null)
            {
                return (false);
            }

            if (line.indexOf("@<TRIPOS>MOLECULE") != -1)
            {
                break;
            }
        }

        int lcount;
        int natoms = -1;
        int nbonds = -1;
        ScanfReader scanf;
        String partialChargeVendor = "NotDefined";

        //  scanf.useCstandard( false );
        //  ScanfFormat  dFormat              = new ScanfFormat( "%d" );
        for (lcount = 0;; lcount++)
        {
            if ((line = lnr.readLine()) == null)
            {
                return (false);
            }

            if (line.indexOf("@<TRIPOS>ATOM") != -1)
            {
                foundAtomLine = true;

                break;
            }

            if (lcount == 0)
            {
                JHM.tokenize(vstr, line);

                if (title == null)
                {
                    if (vstr.size() != 0)
                    {
                        mol.setTitle(line);
                    }
                }
                else
                {
                    mol.setTitle(title);
                }
            }
            else if (lcount == 1)
            {
                scanf = new ScanfReader(new StringReader(line));
                natoms = scanf.scanInt();
                nbonds = scanf.scanInt();
            }
            else if (lcount == 3)
            {
                if (line.trim().length() == 0)
                {
                    logger.warn(
                        "The vendor of the partial charges was not defined !");
                }
                else
                {
                    partialChargeVendor = line.trim();
                }
            }
            else if (lcount == 4) //energy
            {
                JHM.tokenize(vstr, line);

                if ((vstr.size() == 3) &&
                        ((String) vstr.get(0)).equalsIgnoreCase("Energy"))
                {
                    mol.setEnergy(Double.parseDouble((String) vstr.get(2)));
                }
            }
            else if (lcount == 5) //comment
            {
                if (line.trim().length() != 0)
                {
                    comment = line;
                }
            }
        }

        if (!foundAtomLine)
        {
            mol.endModify();
            mol.clear();
            skipReaderEntry();
            throw new MoleculeIOException("Can not find atom line.");
        }

        mol.reserveAtoms(natoms);

        int i;
        XYZVector v = new XYZVector();
        JOEAtom atom = new JOEAtom();
        boolean hasPartialCharges = false;
        double x;
        double y;
        double z;
        double pcharge = 0.0;
        String tmp;

        JOETypeTable ttab = JOETypeTable.instance();
        ttab.setFromType("SYB");

        //    ScanfFormat  fFormat              = new ScanfFormat( "%f" );
        //    ScanfFormat  sFormat              = new ScanfFormat( "%s" );
        for (i = 0; i < natoms; i++)
        {
            if ((line = lnr.readLine()) == null)
            {
                return (false);
            }
            
            line = line.trim();
            StringTokenizer tok = new StringTokenizer(line);
            int index = 0;
            str = null;
            x = 0.0;
            y = 0.0;
            z = 0.0; 
            try
            {
                while( tok.hasMoreTokens() )
                {
                    String value = tok.nextToken();
                    if( index == 2 )
                    {
                        x = new Double(value).doubleValue();
                    }
                    else if( index == 3 )
                    {
                        y = new Double(value).doubleValue();
                    }
                    else if( index == 4 )
                    {
                        z = new Double(value).doubleValue();
                    }
                    else if( index == 5 )
                    {
                        str = value;
                    }
                    else if( index == 8 )
                    {
                        pcharge = new Double(value).doubleValue();
                    }
                    ++index;
                }
            }
            catch( NumberFormatException nfe)
            {
                logger.error("Failed to read in atom line of mol2 file");
                logger.error(line);
                logger.error(nfe.getMessage());
                throw new MoleculeIOException(nfe.getMessage());
            }
            if( str == null )
            {
                logger.error("Failed to read atom type in line of mol2 file");
                logger.error(line);
                throw new MoleculeIOException();
            }

            /* replaced by eduthie

            scanf = new ScanfReader(new StringReader(line));
            tmp = scanf.scanString();
            tmp = scanf.scanString();
            x = scanf.scanDouble();
            y = scanf.scanDouble();
            z = scanf.scanDouble();
            str = scanf.scanString();

            // some databases does'nt contain these values
            try
            {
                tmp = scanf.scanString();
                tmp = scanf.scanString();
                pcharge = scanf.scanDouble();
            }
             catch (EOFException eof)
            {
                // that's o.k., do nothing
                logger.warn("Sybyl mol2 file contains no partial charges.");
            }
            */

            v = new XYZVector();
            v.setX(x);
            v.setY(y);
            v.setZ(z);
            atom.setVector(v);

            ttab.setToType("ATN");
            str1 = ttab.translate(str);
            atom.setAtomicNum(Integer.parseInt(str1));
            ttab.setToType("INT");
            str1 = ttab.translate(str);
            atom.setType(str1);

            // if pcharge was readed !
            if (pcharge != 0.0)
            {
                hasPartialCharges = true;
            }

            if (hasPartialCharges)
            {
                atom.setPartialCharge(pcharge);
            }

            if (!mol.addAtom(atom))
            {
                return (false);
            }
        }

        if (hasPartialCharges)
        {
            mol.setPartialChargesPerceived();
            mol.setAutomaticPartialCharge(false);
            mol.setPartialChargeVendor(partialChargeVendor.replace(' ', '_'));
        }

        for (;;)
        {
            if ((line = lnr.readLine()) == null)
            {
                return (false);
            }

            if (line.indexOf("@<TRIPOS>BOND") != -1)
            {
                break;
            }
        }

        int start;
        int end;
        int order;
        int iDummy;

        for (i = 0; i < nbonds; i++)
        {
            if ((line = lnr.readLine()) == null)
            {
                return (false);
            }

            scanf = new ScanfReader(new StringReader(line));
            iDummy = scanf.scanInt();
            start = scanf.scanInt();
            end = scanf.scanInt();
            str = scanf.scanString();
            order = 1;

            if (str.equalsIgnoreCase("ar"))
            {
                order = 5;
            }
            else if (str.equalsIgnoreCase("am"))
            {
                order = 1;
            }
            else
            {
                order = Integer.parseInt(str);
            }

            mol.addBond(start, end, order);
        }

        // do not overwrite flags
        mol.endModify(false);

        //must add generic data after end modify - otherwise it will be blown away
        if (comment != null)
        {
            JOECommentData cd = new JOECommentData();
            cd.setData(comment);
            mol.addData(cd);
        }

        return (true);
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public boolean readable()
    {
        return true;
    }

    public boolean skipReaderEntry() throws IOException
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @param mol              Description of the Parameter
     * @return                 Description of the Return Value
     * @exception IOException  Description of the Exception
     */
    public boolean write(JOEMol mol) throws IOException, MoleculeIOException
    {
        return write(mol, null);
    }

    /**
     *  Description of the Method
     *
     * @param mol              Description of the Parameter
     * @param title            Description of the Parameter
     * @return                 Description of the Return Value
     * @exception IOException  Description of the Exception
     */
    public boolean write(JOEMol mol, String title)
        throws IOException, MoleculeIOException
    {
        String str;
        String str1;
        String rnum;
        String rlabel;

        ps.println("@<TRIPOS>MOLECULE");

        if (title == null)
        {
            str = mol.getTitle();

            if ((str == null) || (str.trim().length() == 0))
            {
                ps.println("*****");
            }
            else
            {
                ps.println(str);
            }
        }
        else
        {
            ps.println(title);
        }

        ps.print(' ');
        ps.print(mol.numAtoms());
        ps.print(' ');
        ps.print(mol.numBonds());
        ps.println(" 0 0 0");
        ps.println("SMALL");

        //  if (mol.hasPartialChargesPerceived())
        //  {
        //    ps.println("GASTEIGER");
        //  }
        //  else
        //  {
        //    ps.println("NO_CHARGES");
        //  }
        ps.print("Energy = ");
        ps.println(mol.getEnergy());

        if (mol.hasData(JOEDataType.JOE_COMMENT_DATA))
        {
            JOECommentData cd = (JOECommentData) mol.getData(JOEDataType.JOE_COMMENT_DATA);
            ps.println(cd.getData());
        }
        else
        {
            ps.println();
        }

        ps.println("@<TRIPOS>ATOM");

        JOETypeTable ttab = JOETypeTable.instance();
        ttab.setFromType("INT");
        ttab.setToType("SYB");

        JOEAtom atom;
        int[] labelcount = new int[105]; //Number of elements
        PrintfFormat f10_4 = new PrintfFormat("%10.4f");
        PrintfFormat f12_4 = new PrintfFormat("%12.4f");
        PrintfFormat s1 = new PrintfFormat("%1s");
        PrintfFormat s4 = new PrintfFormat("%4s");
        PrintfFormat s5 = new PrintfFormat("%-5s");
        PrintfFormat s6 = new PrintfFormat("%-6s");
        PrintfFormat s8 = new PrintfFormat("%-8s");
        PrintfFormat d7 = new PrintfFormat("%7d");
        AtomIterator ait = mol.atomIterator();
        StringBuffer label = new StringBuffer(20);

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            str = atom.getType();

            str1 = ttab.translate(str);

            //    System.out.println("convert atom type :"+str+" to:"+str1);
            rlabel = "<1>";
            rnum = "1";

            ps.printf(d7, atom.getIdx());
            ps.printf(s1, "");

            if (label.length() > 0)
            {
                label.delete(0, label.length() - 1);
            }

            label.append(JOEElementTable.instance().getSymbol(atom.getAtomicNum()));
            label.append(++labelcount[atom.getAtomicNum()]);
            ps.printf(s6, label.toString());
            ps.printf(f12_4, atom.getX());
            ps.printf(f10_4, atom.getY());
            ps.printf(f10_4, atom.getZ());
            ps.print(' ');
            ps.printf(s5, str1);
            ps.printf(s4, rnum);
            ps.printf(s1, "");
            ps.print(' ');
            ps.printf(s8, rlabel);
            ps.printf(f10_4, atom.getPartialCharge());
            ps.println();
        }

        ps.println("@<TRIPOS>BOND");

        JOEBond bond;
        BondIterator bit = mol.bondIterator();
        String bondLabel;
        PrintfFormat s2 = new PrintfFormat("%2s");
        PrintfFormat s3 = new PrintfFormat("%3s");
        PrintfFormat d6 = new PrintfFormat("%6d");

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            if ((bond.getBO() == 5) || bond.isAromatic())
            {
                bondLabel = "ar";
            }
            else if (bond.isAmide())
            {
                bondLabel = "am";
            }
            else
            {
                bondLabel = Integer.toString(bond.getBO());
            }

            ps.printf(d6, bond.getIdx() + 1);
            ps.printf(d6, bond.getBeginAtomIdx());
            ps.printf(d6, bond.getEndAtomIdx());
            ps.printf(s3, "");
            ps.printf(s2, bondLabel);
            ps.println();
        }

        //ps.println();
        return (true);
    }

    /**
     *  Description of the Method
     *
     * @return   Description of the Return Value
     */
    public boolean writeable()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
