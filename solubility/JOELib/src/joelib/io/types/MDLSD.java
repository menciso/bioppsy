///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MDLSD.java,v $
//  Purpose:  Reader/Writer for SDF files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner, Wayne Volkmuth volkmuth@renovis.com
//  Version:  $Revision: 1.68 $
//            $Date: 2004/07/25 20:43:20 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types;

import cformat.PrintfFormat;
import cformat.PrintfStream;
import cformat.ScanfFormat;
import cformat.ScanfReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOECommentData;
import joelib.data.JOEDataType;
import joelib.data.JOEElementTable;
import joelib.data.JOEGenericData;
import joelib.data.JOEKernel;
import joelib.data.JOEPairData;
import joelib.data.JOERgroupData;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;
import joelib.io.PropertyWriter;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.IsomerismDetection;
import joelib.util.JHM;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.GenericDataIterator;
import joelib.util.iterator.NbrAtomIterator;
import joelib.util.types.IntInt;


/**
 *  Reader/Writer for SDF files.
 *
 * For speeding up descriptor molecule files have a look at the {@link joelib.desc.ResultFactory}.
 *
 * @author     wegnerj
 * @author Wayne Volkmuth
 * @license GPL
 * @cvsversion    $Revision: 1.68 $, $Date: 2004/07/25 20:43:20 $
 * @cite mdlMolFormat
 */
public class MDLSD implements MoleculeFileType, PropertyWriter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.MDLSD");

    /**
     *  Description of the Field
     */
    public static String DIMENSION_2D = "2D";
    public static String DIMENSION_3D = "3D";
    public static String dimension = DIMENSION_3D;
    private final static String description = new String("MDL SD file");
    private final static String[] extensions = new String[]
        {
            "sdf", "sd", "mdl", "mol", "smol"
        };

    //	private final static ScanfFormat f10 = new ScanfFormat("%10f");
    private final static ScanfFormat s3 = new ScanfFormat("%3s");

    //	private final static ScanfFormat d2 = new ScanfFormat("%2d");
    //	private final static ScanfFormat s2 = new ScanfFormat("%2s");
    private final static ScanfFormat d4 = new ScanfFormat("%4d");
    private final static ScanfFormat d3 = new ScanfFormat("%3d");

    //~ Instance fields ////////////////////////////////////////////////////////

    private LineNumberReader lnr;
    private PrintfStream ps;
    private boolean overWriteCommentWithKernelInfo = true;

    //	private final static ScanfFormat s6 = new ScanfFormat("%6s");
    private boolean writeAromaticAsKekule = false;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the MDLSD object
     *
     * @return    Description of the Return Value
     */
    public MDLSD()
    {
        //  Determine if we should write this structure as a Kekule structure
        java.util.Properties prop = wsi.ra.tool.PropertyHolder.instance()
                                                              .getProperties();

        String className = this.getClass().getName();
        String writeKekuleProp = prop.getProperty(className +
                ".writeAromaticityAsConjugatedSystem", "false");

        if (writeKekuleProp.equalsIgnoreCase("true"))
        {
            writeAromaticAsKekule = true;
        }

        String kernelProp = prop.getProperty(className +
                ".overWriteCommentWithKernelInfo", "true");

        if (kernelProp.equalsIgnoreCase("true"))
        {
            overWriteCommentWithKernelInfo = true;
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @exception  IOException  Description of the Exception
     */
    public void closeReader() throws IOException
    {
        lnr.close();
    }

    /**
     *  Description of the Method
     *
     * @exception  IOException  Description of the Exception
     */
    public void closeWriter() throws IOException
    {
        ps.close();
    }

    /**
     *  Description of the Method
     *
     * @param  is               Description of the Parameter
     * @exception  IOException  Description of the Exception
     */
    public void initReader(InputStream is) throws IOException
    {
        lnr = new LineNumberReader(new InputStreamReader( /*(ZipInputStream)*/
                    is));
    }

    /**
     *  Description of the Method
     *
     * @param  os               Description of the Parameter
     * @exception  IOException  Description of the Exception
     */
    public void initWriter(OutputStream os) throws IOException
    {
        ps = new PrintfStream(os);
    }

    public String inputDescription()
    {
        return description;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String[] inputFileExtensions()
    {
        return extensions;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String outputDescription()
    {
        return description;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String[] outputFileExtensions()
    {
        return extensions;
    }

    /**
     *  Reads an molecule entry as (unparsed) <tt>String</tt> representation.
     *
     * @return                  <tt>null</tt> if the reader contains no more
     *      relevant data. Otherwise the <tt>String</tt> representation of the
     *      whole molecule entry is returned.
     * @exception  IOException  typical IOException
     */
    public String read() throws IOException
    {
        StringBuffer molecule = new StringBuffer(10000);
        String delimiter = "$$$$";
        String line;

        while ((line = lnr.readLine()) != null)
        {
            if ((line.length() > 0) && (line.charAt(0) == delimiter.charAt(0)) &&
                    (line.indexOf(delimiter) != -1))
            {
                molecule.append(line);
                molecule.append(JHM.eol);

                break;
            }

            molecule.append(line);
            molecule.append(JHM.eol);
        }

        if (line == null)
        {
            return null;
        }
        else
        {
            return molecule.toString();
        }
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  IOException          Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public synchronized boolean read(JOEMol mol)
        throws IOException, MoleculeIOException
    {
        return read(mol, null);
    }

    /**
     *  Loads an molecule in MDL SD-MOL format and sets the title. If <tt>title
     *  </tt> is <tt>null</tt> the title line in the molecule file is used.
     *
     * @param  mol                      Description of the Parameter
     * @param  title                    Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  IOException          Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public synchronized boolean read(JOEMol mol, String title)
        throws IOException, MoleculeIOException
    {
        int i;
        int natoms;
        int nbonds;
        String line = null;

        ScanfReader scanf;

        // delete molecule data
        mol.clear();

        if (!readHeader(mol, title))
        {
            return false;
        }

        IntInt numbers = new IntInt();

        if (!readNumbers(mol, numbers))
        {
            return false;
        }

        natoms = numbers.i1;
        nbonds = numbers.i2;

        // read atoms block
        mol.beginModify();
        mol.reserveAtoms(natoms);

        XYZVector xyz = new XYZVector();
        JOEAtom atom = new JOEAtom();

        for (i = 1; i <= natoms; i++)
        {
            if (!readAtomAddAtom(mol, atom, xyz, i))
            {
                return false;
            }
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("atoms successful loaded.");
        }

        // read bonds block
        for (i = 0; i < nbonds; i++)
        {
            if (!readBondAddBond(mol, i))
            {
                return false;
            }
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("bonds successful loaded.");
        }

        mol.endModify();

        // get  molecule properties
        //        ScanfFormat s4 = new ScanfFormat("%4s");
        boolean readProperties = true;
        String value;
        int intValue1;
        int intValue2;
        JOERgroupData rgroup = null;

        while (readProperties && ((line = lnr.readLine()) != null))
        {
            if ((line.length() != 0) && (line.charAt(0) == 'M'))
            {
                scanf = new ScanfReader(new StringReader(line));
                scanf.scanString(s3);
                value = scanf.scanString(s3);

                //              System.out.println("PROP:'"+value+"'");
                switch (value.charAt(0))
                {
                case 'C':

                    if (value.equals("CHG"))
                    {
                        //charge
                        int entries = scanf.scanInt(d3);

                        for (i = 0; i < entries; i++)
                        {
                            intValue1 = scanf.scanInt(d4);
                            intValue2 = scanf.scanInt(d4);
                            mol.getAtom(intValue1).setFormalCharge(intValue2);
                        }
                    }

                    break;

                case 'E':

                    if (value.equals("END"))
                    {
                        readProperties = false;
                    }

                    break;

                case 'I':

                    if (value.equals("ISO"))
                    {
                        //isotope
                        int entries = scanf.scanInt(d3);

                        for (i = 0; i < entries; i++)
                        {
                            intValue1 = scanf.scanInt(d4);
                            intValue2 = scanf.scanInt(d4);
                            mol.getAtom(intValue1).setIsotope(intValue2);
                        }
                    }

                    break;

                case 'R':

                    //if (value.equals("RAD"))
                    //{
                    //    //radical
                    //} else
                    if (value.equals("RGP"))
                    {
                        // Rgroup
                        if (rgroup == null)
                        {
                            rgroup = new JOERgroupData();
                        }

                        int entries = scanf.scanInt(d3);

                        for (i = 0; i < entries; i++)
                        {
                            // atom index
                            intValue1 = scanf.scanInt(d4);

                            // Rgroup number
                            intValue2 = scanf.scanInt(d4);

                            //System.out.println("ADD RGROUP: "+intValue1+" "+intValue2);
                            rgroup.add(new IntInt(intValue1, intValue2));
                        }
                    }

                    break;
                }

                scanf = null;
            }
            else
            {
                // some databases don't contain a M END tag.
                // The data block is delimited with a white space, so
                // just break this loop
                break;
            }

            line = null;
        }

        // were there any Rgroups initialized ?
        if (rgroup != null)
        {
            //System.out.println("RGROUP added.");
            mol.addData(rgroup);
        }

        // get additional molecule data
        String attribute;
        boolean succeed;

        // be more robust for interchanged M END / descriptor entries
        boolean ignoreFirst = false;

        if ((line != null) && (line.charAt(0) == '>'))
        {
            ignoreFirst = true;
        }

        while (ignoreFirst || ((line = lnr.readLine()) != null))
        {
            ignoreFirst = false;

            if ((line.length() != 0) && (line.charAt(0) == '>'))
            {
                int begin = line.indexOf('<');
                int end2 = line.lastIndexOf('>');

                // check and get data item name and add it to the list
                if ((begin > 0) && (end2 > 0))
                {
                    attribute = line.substring(begin + 1, end2);
                }
                else
                {
                    return false;
                }

                //System.out.println("data entry '"+attribute+"':"+line);
                // read additional data until empty line or
                // line starts with $$$$
                succeed = true;

                StringBuffer dataEntry = new StringBuffer(25);
                String previousLine = lnr.readLine();

                while (succeed)
                {
                    if (previousLine == null)
                    {
                        //logger.error( "Data entry <" + attribute + "> has no data." );
                        skipReaderEntry();
                        throw new MoleculeIOException("Data entry <" +
                            attribute + "> has no data.");

                        //return ( false );
                    }

                    line = lnr.readLine();

                    //                    if(debug)logger.debug("data entry '"+attribute+"':"+line);
                    if ((line != null) && (line.length() != 0) &&
                            (line.charAt(0) != '$'))
                    {
                        dataEntry.append(previousLine);
                        dataEntry.append(JHM.eol);
                        previousLine = line;
                    }
                    else
                    {
                        // add all lines which are not the end tag: $$$$
                        if ((line != null) && (line.length() == 0))
                        {
                            if (previousLine.length() != 0)
                            {
                                dataEntry.append(previousLine);
                            }

                            //                        while ((line=lnr.readLine()).trim().equals("")){}
                            succeed = false;
                        }
                        else if ((line == null) ||
                                ((line.charAt(0) == '$') &&
                                (line.indexOf("$$$$") != -1)))
                        {
                            succeed = false;
                        }
                        else
                        {
                            dataEntry.append(previousLine);
                            succeed = false;
                        }
                    }
                }

                JOEPairData dp = new JOEPairData();
                dp.setAttribute(attribute);
                dp.setValue(dataEntry.toString());
                mol.addData(dp);

                if (logger.isDebugEnabled())
                {
                    logger.debug("data '" + attribute + "' added with value: " +
                        dataEntry.toString());
                }

                // slow's down parsing ... let's just use dataEntry=null;
                //        dataEntry.delete(0,dataEntry.length());
                dataEntry = null;
            }

            if ((line == null) || (line.length() == 0))
            {
                continue;
            }

            if ((line.charAt(0) == '$') && (line.indexOf("$$$$") != -1))
            {
                break;
            }
        }

        return (true);
    }

    /**
    *  Description of the Method
    *
    * @return    Description of the Return Value
    */
    public boolean readable()
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean skipReaderEntry() throws IOException
    {
        String line;

        while ((line = lnr.readLine()) != null)
        {
            if ((line.length() > 0) && (line.charAt(0) == '$') &&
                    (line.indexOf("$$$$") != -1))
            {
                break;
            }
        }

        return true;
    }

    /**
     *  Writes a molecule with his <tt>JOEPairData</tt> .
     *
     * @param  mol              Description of the Parameter
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol) throws IOException
    {
        return write(mol, null, true, null);
    }

    /**
     *  Writes a molecule with his <tt>JOEPairData</tt> .
     *
     * @param  mol              Description of the Parameter
     * @param  title            the molecule title or <tt>null</tt> if the title
     *      from the molecule should be used
     * @return                  <tt>true</tt> if the molecule and the data has
     *      been succesfully written.
     * @exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol, String title) throws IOException
    {
        return write(mol, title, true, null);
    }

    /**
     *  Writes a molecule with his <tt>JOEPairData</tt> .
     *
     * @param  mol              the molecule with additional data
     * @param  title            the molecule title or <tt>null</tt> if the title
     *      from the molecule should be used
     * @param  writePairData    if <tt>true</tt> then the additional molecule data
     *      is written
     * @param  attribs2write    Description of the Parameter
     * @return                  <tt>true</tt> if the molecule and the data has
     *      been succesfully written.
     * @exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol, String title, boolean writePairData,
        Vector attribs2write) throws IOException
    {
        //System.out.println(mol.toString(IOTypeHolder.instance().getIOType("SMILES")));
        //    System.out.println("write mol with "+attribs2write);
        PrintfFormat d3 = new PrintfFormat("%3d");
        PrintfFormat d4 = new PrintfFormat("%4d");

        String setTitle;

        if (title == null)
        {
            setTitle = mol.getTitle();

            if (setTitle == null)
            {
                setTitle = "Undefined";
            }
        }
        else
        {
            setTitle = title;
        }

        ps.println(setTitle);

        if (mol.has2D())
        {
            dimension = DIMENSION_2D;
        }
        else
        {
            dimension = DIMENSION_3D;
        }

        ps.printf("  -ISIS-            %s", dimension);
        ps.println();

        if (!overWriteCommentWithKernelInfo)
        {
            if (mol.hasData(JOEDataType.JOE_COMMENT_DATA))
            {
                JOECommentData cd = (JOECommentData) mol.getData(JOEDataType.JOE_COMMENT_DATA);

                if (cd.getData() != null)
                {
                    ps.println(cd.getData());
                }
            }
            else
            {
                ps.println(
                    "Used JOELib chemistry kernel (expert systems) ID is " +
                    JOEKernel.instance().getKernelHash());
            }
        }
        else
        {
            ps.println("Used JOELib chemistry kernel (expert systems) ID is " +
                JOEKernel.instance().getKernelHash());
        }

        // number of atoms
        ps.printf(d3, mol.numAtoms());

        // number of bonds
        ps.printf(d3, mol.numBonds());

        // number of atom lists
        ps.printf(d3, 0);

        // obsolete
        ps.printf(d3, 0);

        // chiral flag: 0=not_chiral, 1=chiral
        ps.printf(d3, 0);

        // number of sTest entries
        ps.printf(d3, 0);

        // number of reaction components+1
        ps.printf(d3, 0);

        // number of reactants
        ps.printf(d3, 0);

        // number of products
        ps.printf(d3, 0);

        // number if intermediates
        ps.printf(d3, 0);

        // number of lines of additional properties
        // including the M END line.
        // Obsolete: Default=999
        ps.printf(d3, 1);

        // Ctab version: V2000 or V3000
        ps.println(" V2000");

        JOEAtom atom;
        int charge;
        AtomIterator ait = mol.atomIterator();
        PrintfFormat f10_4 = new PrintfFormat("%10.4f");
        PrintfFormat s3 = new PrintfFormat("%-3s");
        PrintfFormat d2 = new PrintfFormat("%2d");
        Vector charges = new Vector(5);
        Vector isotopes = new Vector(5);

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            charge = 0;

            if ((atom.getFormalCharge() >= -3) &&
                    (atom.getFormalCharge() <= 3))
            {
                switch (atom.getFormalCharge())
                {
                case 1:
                    charge = 3;

                    break;

                case 2:
                    charge = 2;

                    break;

                case 3:
                    charge = 1;

                    break;

                case -1:
                    charge = 5;

                    break;

                case -2:
                    charge = 6;

                    break;

                case -3:
                    charge = 7;

                    break;
                }
            }
            else
            {
                charges.add(new IntInt(atom.getIdx(), atom.getFormalCharge()));
            }

            if (atom.getIsotope() != 0)
            {
                isotopes.add(new IntInt(atom.getIdx(), atom.getIsotope()));
            }

            ps.printf(f10_4, atom.getX());
            ps.printf(f10_4, atom.getY());
            ps.printf(f10_4, atom.getZ());
            ps.print(' ');
            ps.printf(s3,
                JOEElementTable.instance().getSymbol(atom.getAtomicNum()));

            // mass difference for isotopes
            ps.printf(d2, 0);

            // charge
            ps.printf(d3, charge);

            // stereo ignored
            ps.printf(d3, 0);

            // minimal number of H atoms
            ps.printf(d3, 0);

            // stereo care box
            ps.printf(d3, 0);

            // valence, number of implicite and explicite atoms connected to this atom
            ps.printf(d3, 0);

            // H designator
            ps.printf(d3, 0);

            // component: 1=reactant, 2=product, 3=intermediate
            ps.printf(d3, 0);

            // number of reaction components
            ps.printf(d3, 0);

            // atom mapping number
            ps.printf(d3, 0);

            // inversion flag
            ps.printf(d3, 0);

            // change flag
            ps.printf(d3, 0);
            ps.println();
        }

        //so the bonds come out sorted
        JOEAtom nbr;
        JOEBond bond;
        ait.reset();

        int bondType;
        int writtenBonds = 0;
        java.util.HashSet doubleBondAtoms = new java.util.HashSet();

        // unstable, if people are interested to use the molecule for further things
        // DO NOT USE HERE or clone molecule before using it !
        //if(writeAromaticAsKekule)mol.kekulize();
        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            NbrAtomIterator nait = atom.nbrAtomIterator();

            while (nait.hasNext())
            {
                nbr = nait.nextNbrAtom();

                if (atom.getIdx() < nbr.getIdx())
                {
                    writtenBonds++;
                    bond = nait.actualBond();

                    //check, if aromatic bond
                    if (logger.isDebugEnabled())
                    {
                        logger.debug("bond " + bond.getIdx() + "(" +
                            bond.getBeginAtomIdx() + "," +
                            bond.getEndAtomIdx() + ") has bond order " +
                            bond.getBO());
                    }

                    // please remember that the aromaticity typer assign ONLY aromaticity flags and
                    // NOT the internal aromatic bond order
                    if ((bond.getBO() == JOEBond.JOE_AROMATIC_BOND_ORDER) ||
                            bond.isAromatic())
                    {
                        if (writeAromaticAsKekule)
                        {
                            if (bond.isKDouble())
                            {
                                bondType = 2;
                            }
                            else if (bond.isKTriple())
                            {
                                bondType = 3;
                            }
                            else
                            {
                                bondType = 1;
                            }
                        }
                        else
                        {
                            bondType = 4;
                        }
                    }
                    else
                    {
                        // 1 single
                        // 2 double
                        bondType = bond.getBO();
                    }

                    ps.printf(d3, bond.getBeginAtomIdx());
                    ps.printf(d3, bond.getEndAtomIdx());
                    ps.printf(d3, bondType);

                    //check stereochemistry: up/down and cis/trans
                    int isomerism = IsomerismDetection.isCisTransBond(bond);

                    if ((isomerism != IsomerismDetection.EZ_ISOMERISM_UNDEFINED) ||
                            ((bond.getFlags() & JOEBond.JOE_WEDGE_BOND) != 0) ||
                            ((bond.getFlags() & JOEBond.JOE_HASH_BOND) != 0))
                    {
                        if (bond.isWedge())
                        {
                            // wedge bond
                            ps.printf(d3, 1);
                        }
                        else if (bond.isHash())
                        {
                            // hatch bond
                            ps.printf(d3, 6);
                        }
                        else if (isomerism != IsomerismDetection.EZ_ISOMERISM_UNDEFINED)
                        {
                            ps.printf(d3, 3);
                        }
                    }
                    else
                    {
                        ps.printf(d3, 0);
                    }

                    ps.printf(d3, 0);
                    ps.printf(d3, 0);
                    ps.printf(d3, 0);
                    ps.println();
                }
            }
        }

        if (writtenBonds != mol.numBonds())
        {
            logger.warn("Some bonds were not stored for " + mol.getTitle() +
                "!");
        }

        // write molecule properties
        // write atom charges
        if (charges.size() != 0)
        {
            for (int i = 0; i < charges.size(); i++)
            {
                ps.print("M  CHG  1 ");

                IntInt ii = (IntInt) charges.get(i);
                ps.printf(d3, ii.i1);
                ps.printf(d4, ii.i2);
                ps.println();
            }
        }

        // write isotope informations
        if (isotopes.size() != 0)
        {
            for (int i = 0; i < isotopes.size(); i++)
            {
                ps.print("M  ISO  1 ");

                IntInt ii = (IntInt) isotopes.get(i);
                ps.printf(d3, ii.i1);
                ps.printf(d4, ii.i2);
                ps.println();
            }
        }

        // write Rgroups
        //System.out.println("mol.hasData(JOEDataType.JOE_RGROUP_DATA)="+mol.hasData(JOEDataType.JOE_RGROUP_DATA));
        if (mol.hasData(JOEDataType.JOE_RGROUP_DATA))
        {
            Vector rg = ((JOERgroupData) mol.getData(JOEDataType.JOE_RGROUP_DATA)).getData();

            for (int i = 0; i < rg.size(); i++)
            {
                ps.print("M  RGP  1 ");

                IntInt ii = (IntInt) rg.get(i);
                ps.printf(d3, ii.i1);
                ps.printf(d4, ii.i2);
                ps.println();
            }
        }

        ps.println("M  END");

        // write additional descriptor data
        IOType sdf = IOTypeHolder.instance().getIOType("SDF");

        if (writePairData)
        {
            JOEGenericData genericData;
            JOEPairData pairData;

            // write all descriptors
            if (attribs2write == null)
            {
                GenericDataIterator gdit = mol.genericDataIterator();

                while (gdit.hasNext())
                {
                    genericData = gdit.nextGenericData();

                    if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                    {
                        ps.printf(">  <%s>", genericData.getAttribute());
                        ps.println();
                        pairData = (JOEPairData) genericData;
                        ps.println(pairData.toString(sdf));
                        ps.println();

                        //                        DescResult tmpPropResult;
                        //                        try {
                        //                        	tmpPropResult = DescriptorHelper.instance().descFromMol(mol, genericData.getAttribute());
                        //                        	AtomProperties atomProperties;
                        //                        	if (JOEHelper.hasInterface(tmpPropResult, "AtomProperties"))
                        //                        	{
                        //                        		atomProperties = (AtomProperties) tmpPropResult;
                        //                        		int atoms = mol.numAtoms();
                        //                        		for (int j = 0; j < atoms; j++)
                        //                        		{
                        //                        			System.out.println(mol.getTitle()+" "+genericData.getAttribute()+" "+atomProperties.getDoubleValue(j + 1));
                        //                        		}
                        //                        	}
                        //                        } catch (DescriptorException e) {
                        //                        	e.printStackTrace();
                        //                        }
                    }
                }
            }

            // write only descriptors specified in attrib2write
            else
            {
                int size = attribs2write.size();

                for (int i = 0; i < size; i++)
                {
                    // get unparsed data
                    //          System.out.println("write "+ attribs2write.get(i));
                    genericData = mol.getData((String) attribs2write.get(i),
                            false);

                    if (genericData == null)
                    {
                        logger.warn((String) attribs2write.get(i) +
                            " data entry don't exist in molecule: " +
                            mol.getTitle());
                    }
                    else
                    {
                        if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                        {
                            ps.printf(">  <%s>", genericData.getAttribute());
                            ps.println();
                            pairData = (JOEPairData) genericData;
                            ps.println(pairData.toString(sdf));
                            ps.println();
                        }
                    }
                }
            }
        }

        ps.println("$$$$");

        return (true);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean writeable()
    {
        return true;
    }

    /**
     *  Gets the float attribute of the MDLSD class
     *
     * @param  line      Description of the Parameter
     * @param  startPos  Description of the Parameter
     * @param  endPos    Description of the Parameter
     * @return           The float value
     */
    private static float getFloat(String line, int startPos, int endPos)
        throws MoleculeIOException
    {
        // check start and end positions
        if ((startPos < 0) || (endPos > (line.length() + 1)))
        {
            return 0;
        }

        String sub = null;
        String subTrim = null;
        float value = 0;

        try
        {
            // get trimmed substring
            sub = line.substring(startPos, endPos);
            subTrim = sub.trim();

            // try to convert substring to integer
            value = Float.parseFloat(subTrim);
        }
         catch (Exception e)
        {
            logger.error("Can not convert string '" + subTrim +
                "' to type 'float' in line: " + line);
            throw new MoleculeIOException(e.toString());
        }

        sub = null;
        subTrim = null;

        return value;
    }

    /**
     *  Gets the integer attribute of the MDLSD class
     *
     * @param  line      Description of the Parameter
     * @param  startPos  Description of the Parameter
     * @param  endPos    Description of the Parameter
     * @return           The integer value
     */
    private static int getInteger(String line, int startPos, int endPos)
        throws MoleculeIOException
    {
        // check start and end positions
        if ((startPos < 0) || (endPos > (line.length() + 1)))
        {
            return 0;
        }

        String sub = null;
        String subTrim = null;
        int value = 0;

        try
        {
            // get trimmed substring
            sub = line.substring(startPos, endPos);
            subTrim = sub.trim();

            // try to convert substring to integer
            value = Integer.parseInt(subTrim);
        }
         catch (Exception e)
        {
            logger.error("Can not convert string '" + subTrim +
                "' to type 'int' in line: " + line);
            throw new MoleculeIOException(e.toString());
        }

        sub = null;
        subTrim = null;

        return value;
    }

    /**
     * @param mol
     * @param atom
     * @return
     */
    private boolean readAtomAddAtom(JOEMol mol, JOEAtom atom, XYZVector xyz,
        int lineNbr) throws MoleculeIOException, IOException
    {
        String line;
        double x;
        double y;
        double z;
        String type;
        int charge = 0;
        int massDifference;
        int atomicNumber;

        if ((line = lnr.readLine()) == null)
        {
            return (false);
        }

        // ignore empty lines in atom block
        if (line.trim().length() == 0)
        {
            logger.warn("Skipping empty line in atom block.");

            return true;
        }

        //            if(debug)logger.debug("atom "+(i+1)+" line:"+line);
        //      scanf = new ScanfReader(new StringReader(line));
        //      scanf.useCstandard(false);
        try
        {
            // fast
            x = getFloat(line, 0, 10);
            y = getFloat(line, 10, 20);
            z = getFloat(line, 20, 30);

            // get element
            try
            {
                type = line.substring(31, 34).trim();
            }
             catch (Exception e)
            {
                throw new MoleculeIOException("No atom type defined.");
            }

            // get mass difference
            massDifference = getInteger(line, 34, 36);

            // get and parse charge of atom
            charge = getInteger(line, 36, 39);

            // slow
            //        x = scanf.scanFloat(f10);
            //        y = scanf.scanFloat(f10);
            //        z = scanf.scanFloat(f10);
            //        type = scanf.scanString(s3);
            //        massDifference = scanf.scanInt(d2);
            //        charge = scanf.scanInt(d3);
            // don't use this informations if not explicitely necessary
            // some databases don't contain all informations !!!
            //            String stereoIgnored = scanf.scanString(s3);
            //            int minQueryHatoms = scanf.scanInt(d3);
            //            String stereoCareBox = scanf.scanString(s3);
            //            int numImpAndExplBonds = scanf.scanInt(d3);
            //            String hDesignator = scanf.scanString(s3);
            //            // 1=reactant, 2=product, 3=intermediate
            //            int reactComponent = scanf.scanInt(d3);
            //            int reactCompNumber = scanf.scanInt(d3);
            //            int atomMappingNumber = scanf.scanInt(d3);
            //            int inversionFlag = scanf.scanInt(d3);
            //            int changeFlag = scanf.scanInt(d3);
        }
         catch (MoleculeIOException ex)
        {
            skipReaderEntry();
            throw new MoleculeIOException("Error in atom line #" + lineNbr +
                ". " + ex.getMessage());
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("type: " + type + " massDifference:" + massDifference +
                " charge:" + charge);
        }

        xyz.setX(x);
        xyz.setY(y);
        xyz.setZ(z);
        atom.setVector(xyz);

        if (type.charAt(0) == 'D')
        {
            if (type.length() == 1)
            {
                type = "H";
                atom.setIsotope(2);
            }
            else if (type.charAt(1) == 'u')
            {
                type = "Xx";
            }
        }

        atomicNumber = JOEElementTable.instance().getAtomicNum(type);

        if (atomicNumber == 0)
        {
            if ((type.equals("Xx") || type.equals("Du")) == false)
            {
                skipReaderEntry();
                throw new MoleculeIOException("Unknown atom type '" + type +
                    "' in atom line #" + lineNbr);
            }
            else
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Dummy atom '" + type + "' in atom line #" +
                        lineNbr);
                }
            }
        }

        atom.setAtomicNum(atomicNumber);
        atom.setType(type);

        switch (charge)
        {
        case 0:
            break;

        case 3:
            atom.setFormalCharge(1);

            break;

        case 2:
            atom.setFormalCharge(2);

            break;

        case 1:
            atom.setFormalCharge(3);

            break;

        case 5:
            atom.setFormalCharge(-1);

            break;

        case 6:
            atom.setFormalCharge(-2);

            break;

        case 7:
            atom.setFormalCharge(-3);

            break;

            // case 4: radical
        }

        if (!mol.addAtom(atom))
        {
            skipReaderEntry();
            throw new MoleculeIOException("Atom " + lineNbr +
                " could not be added.");
        }

        atom.clear();

        return true;
    }

    /**
         * @param mol
         * @param i
         * @return
         */
    private boolean readBondAddBond(JOEMol mol, int i)
        throws IOException, MoleculeIOException
    {
        String line;
        int start;
        int end;
        int order;
        int flag;
        int stereo;

        flag = 0;

        if ((line = lnr.readLine()) == null)
        {
            return (false);
        }

        // ignore empty lines in bond block
        if (line.trim().length() == 0)
        {
            logger.warn("Skipping empty line in bond block.");

            return true;
        }

        //            if(debug)logger.debug("bond "+(i+1)+" line:"+line);
        //      scanf = new ScanfReader(new StringReader(line));
        //      scanf.useCstandard(false);
        //    r1 = buffer;
        try
        {
            // fast
            start = getInteger(line, 0, 3);
            end = getInteger(line, 3, 6);

            if (start == end)
            {
                throw new MoleculeIOException("Bond can not connect atom " +
                    start + " with itself.");
            }

            order = getInteger(line, 6, 9);

            //                  short topology = (short) getInteger(line,15,18);
            //                  short center   = (short) getInteger(line,18,21);
            // slow
            //        start = scanf.scanInt(d3);
            //        end = scanf.scanInt(d3);
            //        order = scanf.scanInt(d3);
            order = (order == 4) ? JOEBond.JOE_AROMATIC_BOND_ORDER : order;

            if (line.length() >= 12)
            {
                //handle wedge/hash data
                //fast
                stereo = getInteger(line, 9, 12);

                //slow
                //          stereo = scanf.scanInt(d3);
                if (stereo != 0)
                {
                    // for single bonds
                    if (stereo == 1)
                    {
                        flag |= JOEBond.JOE_WEDGE_BOND;
                    }
                    else if (stereo == 6)
                    {
                        flag |= JOEBond.JOE_HASH_BOND;
                    }

                    //else if (stereo == 3)
                    //{
                    //	// for double bonds
                    //    // cis/trans
                    //}
                }
            }
        }
         catch (MoleculeIOException ex)
        {
            skipReaderEntry();
            throw new MoleculeIOException("Error in bond line #" + (i + 1) +
                ". " + ex.getMessage());
        }

        if (!mol.addBond(start, end, order, flag))
        {
            skipReaderEntry();
            throw new MoleculeIOException("Bond could not be added.");

            //return ( false );
        }

        return true;
    }

    /**
     *
     */
    private boolean readHeader(JOEMol mol, String title)
        throws IOException
    {
        String line;

        if ((line = lnr.readLine()) == null)
        {
            return (false);
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("molecule:" + line);
        }

        if (title == null)
        {
            mol.setTitle(line);
        }
        else
        {
            mol.setTitle(title);
        }

        if ((line = lnr.readLine()) == null)
        {
            return (false);
        }

        //creator
        if ((line = lnr.readLine()) == null)
        {
            return (false);
        }

        String comment = "";

        //comment
        if (line.length() > 0)
        {
            comment = line;
        }

        if ((comment != null) && (comment.trim().length() != 0))
        {
            JOECommentData cd = new JOECommentData();
            cd.setData(comment);
            mol.addData(cd);
        }

        return true;
    }

    /**
     * @param mol
     * @param numbers
     * @return
     */
    private boolean readNumbers(JOEMol mol, IntInt numbers)
        throws IOException, MoleculeIOException
    {
        String line;

        if ((line = lnr.readLine()) == null)
        {
            return (false);
        }

        //    scanf = new ScanfReader(new StringReader(line));
        //    scanf.useCstandard(false);
        try
        {
            // fast
            numbers.i1 = getInteger(line, 0, 3);
            numbers.i2 = getInteger(line, 3, 6);

            // slow
            //      natoms = scanf.scanInt(d3);
            //      nbonds = scanf.scanInt(d3);
            // don't use this informations if not explicitely necessary
            // some databases don't contain all informations !!!
            //        int numAtomLists = scanf.scanInt(d3);
            //        int obsolete = scanf.scanInt(d3);
            //        boolean chiral = (scanf.scanInt(d3)==1)?true:false;
            //        int numSTextEntries = scanf.scanInt(d3);
            //        int numReactComp_1 = scanf.scanInt(d3);
            //        int numReactants = scanf.scanInt(d3);
            //        int numProducts = scanf.scanInt(d3);
            //        int numIntermediates = scanf.scanInt(d3);
            //        int obsoletePropLines = scanf.scanInt(d3);
            //        String ctabVersion = scanf.scanString(s6);
        }
         catch (MoleculeIOException ex)
        {
            skipReaderEntry();
            throw new MoleculeIOException("Error in atom/bond definition line.");
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("atoms: " + numbers.i1 + " bonds:" + numbers.i2);

            //            logger.debug("atomLists: " + numAtomLists + " chiral:" + chiral +
            //                         " STextEntries:" + numSTextEntries+ " reactComponents+1:" + numReactComp_1+ " reactants:" + numReactants+
            //                         " products:" + numProducts+ " intermediates:" + numIntermediates+ " ctab version:" + ctabVersion);
        }

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
