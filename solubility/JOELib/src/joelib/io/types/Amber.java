///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Amber.java,v $
//  Purpose:  Reader/Writer for Undefined files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.3 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////////**
package joelib.io.types;

import cformat.PrintfStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;
import joelib.data.JOETypeTable;

import joelib.io.MoleculeFileType;
import joelib.io.MoleculeIOException;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import joelib.util.JHM;
import joelib.util.types.JOEInternalCoord;


/**
 * Reader for AmberPrep file format.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.3 $, $Date: 2004/07/25 20:43:19 $
 */
public class Amber implements MoleculeFileType
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.Amber");
    private static final String description = new String("Amber PREP");
    private static final String[] extensions = new String[]{"prep"};

    //~ Instance fields ////////////////////////////////////////////////////////

    private LineNumberReader lnr;
    private PrintfStream ps;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @exception  IOException  Description of the Exception
     */
    public void closeReader() throws IOException
    {
        lnr.close();
    }

    /**
     *  Description of the Method
     *
     * @exception  IOException  Description of the Exception
     */
    public void closeWriter() throws IOException
    {
        ps.close();
    }

    public void initReader(InputStream is) throws IOException
    {
        lnr = new LineNumberReader(new InputStreamReader( /*(ZipInputStream)*/
                    is));
    }

    /**
     *  Description of the Method
     *
     * @param  os               Description of the Parameter
     * @exception  IOException  Description of the Exception
     */
    public void initWriter(OutputStream os) throws IOException
    {
        ps = new PrintfStream(os);
    }

    public String inputDescription()
    {
        return description;
    }

    public String[] inputFileExtensions()
    {
        return extensions;
    }

    public String outputDescription()
    {
        return null;
    }

    public String[] outputFileExtensions()
    {
        return null;
    }

    /**
     *  Reads an molecule entry as (unparsed) <tt>String</tt> representation.
     *
     * @return                  <tt>null</tt> if the reader contains no more
     *      relevant data. Otherwise the <tt>String</tt> representation of the
     *      whole molecule entry is returned.
     * @exception  IOException  typical IOException
     */
    public String read() throws IOException
    {
        logger.error(
            "Reading Amber data as String representation is not implemented yet !!!");

        return null;
    }

    /**
     *  Description of the Method
     *
     * @param  mol                      Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  IOException          Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public synchronized boolean read(JOEMol mol)
        throws IOException, MoleculeIOException
    {
        return read(mol, null);
    }

    /**
     *  Loads an molecule in MDL SD-MOL format and sets the title. If <tt>title
     *  </tt> is <tt>null</tt> the title line in the molecule file is used.
     *
     * @param  mol                      Description of the Parameter
     * @param  title                    Description of the Parameter
     * @return                          Description of the Return Value
     * @exception  IOException          Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public synchronized boolean read(JOEMol mol, String title)
        throws IOException, MoleculeIOException
    {
        String line;
        Vector vs = new Vector();
        JOEAtom atom;
        JOEInternalCoord coord;
        Vector internals = new Vector();

        JOETypeTable.instance().setFromType("XYZ");
        JOETypeTable.instance().setToType("INT");
        mol.beginModify();

        while ((line = lnr.readLine()) != null)
        {
            JHM.tokenize(vs, line);

            if (vs.size() > 8)
            {
                atom = mol.newAtom();
                coord = new JOEInternalCoord();

                if (mol.numAtoms() > 1)
                {
                    coord._a = mol.getAtom(Integer.parseInt((String) vs.get(4)));
                }

                if (mol.numAtoms() > 2)
                {
                    coord._b = mol.getAtom(Integer.parseInt((String) vs.get(5)));
                }

                if (mol.numAtoms() > 3)
                {
                    coord._c = mol.getAtom(Integer.parseInt((String) vs.get(6)));
                }

                coord._dst = Double.parseDouble((String) vs.get(7));
                coord._ang = Double.parseDouble((String) vs.get(8));
                coord._tor = Double.parseDouble((String) vs.get(9));
                internals.add(coord);

                atom.setAtomicNum(JOEElementTable.instance().getAtomicNum((String) vs.get(
                            1)));
                atom.setType(JOETypeTable.instance().translate((String) vs.get(
                            1)));
            }
        }

        JHM.internalToCartesian(internals, mol);
        mol.endModify();

        mol.connectTheDots();
        mol.setTitle(title);

        return (true);
    }

    public boolean readable()
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean skipReaderEntry() throws IOException
    {
        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  mol              Description of the Parameter
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol) throws IOException
    {
        return write(mol, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol              Description of the Parameter
     * @param  title            Description of the Parameter
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol, String title) throws IOException
    {
        return (true);
    }

    public boolean writeable()
    {
        return false;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
