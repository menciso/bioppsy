///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeAttributeArray.java,v $
//  Purpose:  Reader/Writer for CML files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.3 $
//            $Date: 2004/07/25 20:43:21 $
//            $Author: wegner $
//  Original Author: steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu, egonw@sci.kun.nl
//  Original Version: Chemical Development Kit,  http://sourceforge.net/projects/cdk
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.io.PrintStream;

import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.desc.result.AtomDoubleResult;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.IsomerismDetection;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;
import joelib.util.types.StringInt;


/**
 * CML molecule array writer.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.3 $, $Date: 2004/07/25 20:43:21 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public class MoleculeAttributeArray extends CMLMoleculeWriterBase
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.MoleculeAttributeArray");

    //~ Constructors ///////////////////////////////////////////////////////////

    public MoleculeAttributeArray(PrintStream _output,
        CMLWriterProperties _writerProp)
    {
        super(_output, _writerProp);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    protected void writeAtoms(JOEMol mol, String molID, boolean has3D,
        boolean has2D, AtomDoubleResult adrX, AtomDoubleResult adrY,
        Hashtable atomIDs)
    {
        int size = 10 * mol.numAtoms();
        StringBuffer aids = new StringBuffer(size);
        StringBuffer elements = new StringBuffer(size);
        StringBuffer x2 = new StringBuffer(size);
        StringBuffer y2 = new StringBuffer(size);
        StringBuffer x3 = new StringBuffer(size);
        StringBuffer y3 = new StringBuffer(size);
        StringBuffer z3 = new StringBuffer(size);
        StringBuffer partCharge = new StringBuffer(size);
        StringBuffer formalCharge = new StringBuffer(size);
        StringBuffer impValence = new StringBuffer(size);
        StringBuffer isotope = new StringBuffer(size);
        boolean hasIsotopes = false;
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        XYZVector xyz;
        int index = 0;
        StringInt atomID;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            index++;
            xyz = atom.getVector();
            atomID = (StringInt) atomIDs.get(molID + ":" + atom.getIdx());
            aids.append(atomID.s);
            elements.append(atom);

            if (has3D)
            {
                x3.append(xyz._vx);
                y3.append(xyz._vy);
                z3.append(xyz._vz);
            }

            if (has2D)
            {
                if ((adrX != null) && (adrY != null))
                {
                    x2.append(adrX.getStringValue(index));
                    y2.append(adrY.getStringValue(index));
                }
                else
                {
                    x2.append(xyz._vx);
                    y2.append(xyz._vy);
                }
            }

            formalCharge.append(atom.getFormalCharge());

            if (writerProp.writePartialCharge())
            {
                partCharge.append(atom.getPartialCharge());
            }

            if (writerProp.writeImpliciteHydrogens())
            {
                impValence.append(atom.getImplicitValence() +
                    atom.explicitHydrogenCount());
            }

            if (atom.getIsotope() != 0)
            {
                hasIsotopes = true;
            }

            isotope.append(atom.getIsotope());

            if (ait.hasNext())
            {
                aids.append(' ');
                elements.append(' ');

                if (has3D)
                {
                    x3.append(' ');
                    y3.append(' ');
                    z3.append(' ');
                }

                if (has2D)
                {
                    x2.append(' ');
                    y2.append(' ');
                }

                formalCharge.append(' ');

                if (writerProp.writePartialCharge())
                {
                    partCharge.append(' ');
                }

                if (writerProp.writeImpliciteHydrogens())
                {
                    impValence.append(' ');
                }

                isotope.append(' ');
            }
        }

        Hashtable attributes = new Hashtable();
        attributes.put("atomID", aids.toString());
        attributes.put("elementType", elements.toString());
        attributes.put("formalCharge", formalCharge.toString());

        if (has3D)
        {
            attributes.put("x3", x3.toString());
            attributes.put("y3", y3.toString());
            attributes.put("z3", z3.toString());
        }

        if (has2D)
        {
            attributes.put("x2", x2.toString());
            attributes.put("y2", y2.toString());
        }

        //        if (writerProp.writePartialCharge())
        //        {
        //            //CML1
        //            attributes.clear();
        //            attributes.put("builtin", "partialCharge");
        //            writeOpenTag(output,writerProp,"floatArray", attributes, false);
        //            write(output,partCharge.toString());
        //            writeCloseTag(output,writerProp,"floatArray");
        //
        //            // CML2
        //            //<array dataType="xsd:float" dictRef="joelib:partialCharge" units="units:electron">.234 1,456 4.678</scalar>
        //        }
        if (writerProp.writeImpliciteHydrogens())
        {
            attributes.put("hydrogenCount", impValence.toString());
        }

        if (hasIsotopes)
        {
            attributes.put("isotope", isotope.toString());
        }

        writeOpenTag(output, writerProp, "atomArray", attributes, false);
        writeCloseTag(output, writerProp, "atomArray");
    }

    protected void writeBonds(JOEMol mol, String molID, Hashtable atomIDs,
        Hashtable bondIDs)
    {
        StringBuffer orders = new StringBuffer(10 * mol.numBonds());
        StringBuffer begins = new StringBuffer(10 * mol.numBonds());
        StringBuffer ends = new StringBuffer(10 * mol.numBonds());
        StringBuffer bondStereo = new StringBuffer(10 * mol.numBonds());
        BondIterator bit = mol.bondIterator();
        JOEBond bond;

        JOEAtom beginAtom;
        JOEAtom endAtom;
        StringInt beginAtomID;
        StringInt endAtomID;
        boolean hasStereo = false;

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            beginAtom = bond.getBeginAtom();
            endAtom = bond.getEndAtom();

            beginAtomID = (StringInt) atomIDs.get(molID + ":" +
                    beginAtom.getIdx());
            endAtomID = (StringInt) atomIDs.get(molID + ":" + endAtom.getIdx());

            begins.append(beginAtomID.s);
            ends.append(endAtomID.s);

            // is aromatic bond ?
            if (bond.getBondOrder() == 4)
            {
                orders.append("1.5");
            }
            else
            {
                orders.append(bond.getBondOrder());
            }

            int isomerism = IsomerismDetection.isCisTransBond(bond);

            if ((isomerism != IsomerismDetection.EZ_ISOMERISM_UNDEFINED) ||
                    ((bond.getFlags() & JOEBond.JOE_WEDGE_BOND) != 0) ||
                    ((bond.getFlags() & JOEBond.JOE_HASH_BOND) != 0))
            {
                hasStereo = true;
            }

            if (bond.isWedge())
            {
                // wedge bond
                bondStereo.append("W");
            }
            else if (bond.isHash())
            {
                // hatch bond
                bondStereo.append("H");
            }
            else if (isomerism != IsomerismDetection.EZ_ISOMERISM_UNDEFINED)
            {
                if (isomerism == IsomerismDetection.Z_ISOMERISM)
                {
                    // cis bond
                    bondStereo.append("C");
                }
                else if (isomerism == IsomerismDetection.E_ISOMERISM)
                {
                    // trans bond
                    bondStereo.append("T");
                }
            }
            else
            {
                bondStereo.append("none");
            }

            if (bit.hasNext())
            {
                begins.append(' ');
                ends.append(' ');
                orders.append(' ');
                bondStereo.append(' ');
            }
        }

        Hashtable attributes = new Hashtable();
        attributes.put("atomRef1", begins.toString());
        attributes.put("atomRef2", ends.toString());
        attributes.put("order", orders.toString());

        if (hasStereo)
        {
            attributes.put("bondStereo", bondStereo.toString());
        }

        writeOpenTag(output, writerProp, "bondArray", attributes, false);
        writeCloseTag(output, writerProp, "bondArray");
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
