///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: CMLPropertyWriter.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu,
//			egonw@sci.kun.nl, wegnerj@informatik.uni-tuebingen.de
//Version:  $Revision: 1.24 $
//			$Date: 2004/08/27 09:30:43 $
//			$Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 2 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.io.PrintStream;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEGenericData;
import joelib.data.JOEKernel;
import joelib.data.JOEPairData;

import joelib.desc.DescResult;
import joelib.desc.result.BitArrayResult;
import joelib.desc.result.BitResult;
import joelib.desc.result.BooleanResult;
import joelib.desc.result.DoubleArrayResult;
import joelib.desc.result.DoubleMatrixResult;
import joelib.desc.result.DoubleResult;
import joelib.desc.result.IntArrayResult;
import joelib.desc.result.IntMatrixResult;
import joelib.desc.result.IntResult;
import joelib.desc.result.StringArrayResult;
import joelib.desc.result.StringResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.io.types.ChemicalMarkupLanguage;

import joelib.molecule.JOEMol;

import joelib.util.iterator.GenericDataIterator;
import joelib.util.types.StringString;


/**
 * Helper class for a CML molecule property (descriptor) writer.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.24 $, $Date: 2004/08/27 09:30:43 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public class CMLPropertyWriter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.CMLPropertyWriter");
    private static IOType cml = IOTypeHolder.instance().getIOType("CML");
    public static String COORDINATES_2D_X = "coordinates2Dx";
    public static String COORDINATES_2D_Y = "coordinates2Dy";
    public static String COORDINATES_3D_X = "coordinates3Dx";
    public static String COORDINATES_3D_Y = "coordinates3Dy";
    public static String COORDINATES_3D_Z = "coordinates3Dz";

    //~ Methods ////////////////////////////////////////////////////////////////

    public static void writePairData(CMLWriterProperties _writerProp,
        PrintStream ps, JOEPairData pairData)
    {
        String attrib = pairData.getAttribute();
        Object value = pairData.getValue();

        if (logger.isDebugEnabled())
        {
            logger.debug(attrib + "=" + value + " is of type " +
                value.getClass().getName());
        }

        // don't write coordinate atom property helper arrays
        if (attrib.equals(CMLPropertyWriter.COORDINATES_2D_X) ||
                attrib.equals(CMLPropertyWriter.COORDINATES_2D_Y) ||
                attrib.equals(CMLPropertyWriter.COORDINATES_3D_X) ||
                attrib.equals(CMLPropertyWriter.COORDINATES_3D_Y) ||
                attrib.equals(CMLPropertyWriter.COORDINATES_3D_Z))
        {
            return;
        }

        if (_writerProp.getCMLversion() == ChemicalMarkupLanguage.CML_VERSION_1)
        {
            if (value instanceof IntResult)
            {
                writeScalarCML1(_writerProp, ps, "integer", attrib,
                    value.toString(), ((ResultCMLProperties) value));
            }
            else if (value instanceof DoubleResult)
            {
                writeScalarCML1(_writerProp, ps, "float", attrib,
                    value.toString(), ((ResultCMLProperties) value));
            }
            else if (value instanceof IntArrayResult)
            {
                if (((IntArrayResult) value).getIntArray() == null)
                {
                    logger.error("Integer array '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    writeArrayCML1(_writerProp, ps, "integerArray", attrib,
                        ((IntArrayResult) value).toString(cml),
                        ((IntArrayResult) value).getIntArray().length,
                        (ResultCMLProperties) value);
                }
            }
            else if (value instanceof DoubleArrayResult)
            {
                if (((DoubleArrayResult) value).getDoubleArray() == null)
                {
                    logger.error("Double array '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    writeArrayCML1(_writerProp, ps, "floatArray", attrib,
                        ((DoubleArrayResult) value).toString(cml),
                        ((DoubleArrayResult) value).getDoubleArray().length,
                        (ResultCMLProperties) value);
                }
            }
            else if (value instanceof StringArrayResult)
            {
            	if (((StringArrayResult) value).getStringArray() == null)
            	{
            		logger.error("String array '" + attrib +
            		"' is  not defined.");
            	}
            	else
            	{
            		writeArrayCML1(_writerProp, ps, "stringArray", attrib,
            				((DoubleArrayResult) value).toString(cml),
							((DoubleArrayResult) value).getDoubleArray().length,
							(ResultCMLProperties) value);
            	}
            }
            else if (value instanceof DoubleMatrixResult)
            {
                if (((DoubleMatrixResult) value).value == null)
                {
                    logger.error("Double matrix array '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    Hashtable attributes = new Hashtable();
                    attributes.clear();
                    attributes.put("title", attrib);
                    attributes.put("rows",
                        Integer.toString(
                            ((DoubleMatrixResult) value).value.length));
                    attributes.put("columns",
                        Integer.toString(
                            ((DoubleMatrixResult) value).value[0].length));

                    Enumeration e = ((ResultCMLProperties) value).getCMLProperties();
                    writeMatrixProperties(e, attributes);

                    CMLMoleculeWriterBase.writeOpenTag(ps, _writerProp,
                        "floatMatrix", attributes, false);
                    CMLMoleculeWriterBase.write(ps,
                        ((DoubleMatrixResult) value).toString(cml));
                    CMLMoleculeWriterBase.writeCloseTag(ps, _writerProp,
                        "floatMatrix");
                }
            }
            else
            {
                Hashtable attributes = new Hashtable();
                attributes.clear();
                attributes.put("title", attrib);
                CMLMoleculeWriterBase.writeOpenTag(ps, _writerProp, "string",
                    attributes, false);
                CMLMoleculeWriterBase.write(ps,
                    XMLSpecialCharacter.convertPlain2XML(
                        (String) value.toString()));
                CMLMoleculeWriterBase.writeCloseTag(ps, _writerProp, "string");
            }
        }
        else
        {
            if (value instanceof IntResult)
            {
                writeScalarCML2(_writerProp, ps, "xsd:integer", attrib,
                    value.toString(), ((ResultCMLProperties) value));
            }
            else if (value instanceof DoubleResult)
            {
                writeScalarCML2(_writerProp, ps, "xsd:double", attrib,
                    value.toString(), ((ResultCMLProperties) value));
            }
            else if (value instanceof StringResult)
            {
                writeScalarCML2(_writerProp, ps, "xsd:string", attrib,
                    XMLSpecialCharacter.convertPlain2XML(((StringResult) value).toString(
                            cml)), ((ResultCMLProperties) value));
            }
            else if (value instanceof BooleanResult)
            {
                writeScalarCML2(_writerProp, ps, "xsd:boolean", attrib,
                    value.toString(), ((ResultCMLProperties) value));
            }
            else if (value instanceof IntArrayResult)
            {
                if (((IntArrayResult) value).getIntArray() == null)
                {
                    logger.error("Integer array '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    writeArrayCML2(_writerProp, ps, "xsd:integer", attrib,
                        ((IntArrayResult) value).toString(cml),
                        ((IntArrayResult) value).getIntArray().length,
                        (ResultCMLProperties) value);
                }
            }
            else if (value instanceof DoubleArrayResult)
            {
                if (((DoubleArrayResult) value).getDoubleArray() == null)
                {
                    logger.error("Double array '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    writeArrayCML2(_writerProp, ps, "xsd:double", attrib,
                        ((DoubleArrayResult) value).toString(cml),
                        ((DoubleArrayResult) value).getDoubleArray().length,
                        (ResultCMLProperties) value);
                }
            }
            else if (value instanceof StringArrayResult)
            {
            	if (((StringArrayResult) value).getStringArray() == null)
            	{
            		logger.error("String array '" + attrib +
            		"' is  not defined.");
            	}
            	else
            	{
            		writeArrayCML2(_writerProp, ps, "xsd:string", attrib,
            				((StringArrayResult) value).toString(cml),
							((StringArrayResult) value).getStringArray().length,
							(ResultCMLProperties) value);
            	}
            }
            else if (value instanceof BitArrayResult)
            {
                if (((BitArrayResult) value).value == null)
                {
                    logger.error("Boolean array '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    boolean[] array = ((BitArrayResult) value).value.toBoolArray();
                    writeArrayCML2(_writerProp, ps, "xsd:boolean", attrib,
                        ((BitArrayResult) value).toString(cml), array.length,
                        (ResultCMLProperties) value);
                }
            }
            else if (value instanceof BitResult)
            {
                if (((BitResult) value).value == null)
                {
                    logger.error("Boolean array '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    boolean[] array = ((BitResult) value).value.toBoolArray();

                    writeArrayCML2(_writerProp, ps, "xsd:boolean", attrib,
                        ((BitResult) value).toString(cml), array.length,
                        (ResultCMLProperties) value);
                }
            }
            else if (value instanceof DoubleMatrixResult)
            {
                if (((DoubleMatrixResult) value).value == null)
                {
                    logger.error("Double matrix '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    writeMatrixCML2(_writerProp, ps, "xsd:double", attrib,
                        ((DoubleMatrixResult) value).toString(cml),
                        ((DoubleMatrixResult) value).value.length,
                        ((DoubleMatrixResult) value).value[0].length,
                        ((ResultCMLProperties) value));
                }
            }
            else if (value instanceof IntMatrixResult)
            {
                if (((IntMatrixResult) value).value == null)
                {
                    logger.error("Integer matrix '" + attrib +
                        "' is  not defined.");
                }
                else
                {
                    writeMatrixCML2(_writerProp, ps, "xsd:integer", attrib,
                        ((IntMatrixResult) value).toString(cml),
                        ((IntMatrixResult) value).value.length,
                        ((IntMatrixResult) value).value[0].length,
                        ((ResultCMLProperties) value));
                }
            }
            else
            {
                Hashtable attributes = new Hashtable();
                attributes.clear();
                attributes.put("title", attrib);
                attributes.put("dataType", "xsd:string");
                CMLMoleculeWriterBase.writeOpenTag(ps, _writerProp, "scalar",
                    attributes, false);
                CMLMoleculeWriterBase.write(ps,
                    XMLSpecialCharacter.convertPlain2XML(
                        (String) value.toString()));
                CMLMoleculeWriterBase.writeCloseTag(ps, _writerProp, "scalar");
            }
        }
    }

    public static void writeProperties(CMLWriterProperties _writerProp,
        PrintStream ps, JOEMol mol, boolean writePairData, Vector attribs2write)
    {
        // write additional descriptor data
        if (writePairData)
        {
            JOEGenericData genericData;
            JOEPairData pairData;

            // write all descriptors
            if (attribs2write == null)
            {
                GenericDataIterator gdit = mol.genericDataIterator();

                while (gdit.hasNext())
                {
                    genericData = gdit.nextGenericData();

                    if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                    {
                        pairData = (JOEPairData) genericData;

                        if (pairData.getValue() instanceof DescResult)
                        {
                            // all o.k.
                            if (logger.isDebugEnabled())
                            {
                                logger.debug("Descriptor '" +
                                    genericData.getAttribute() +
                                    "' is (parsed) PairData");
                            }
                        }
                        else
                        {
                            if (logger.isDebugEnabled())
                            {
                                logger.debug("Descriptor '" +
                                    genericData.getAttribute() +
                                    "' is (unparsed) String");
                            }

                            pairData = (JOEPairData) mol.getData(genericData.getAttribute(),
                                    true);
                        }

                        writePairData(_writerProp, ps, pairData);
                    }
                }
            }

            // write only descriptors specified in attrib2write
            else
            {
                int size = attribs2write.size();

                for (int i = 0; i < size; i++)
                {
                    // get unparsed data
                    //				System.out.println("write "+ attribs2write.get(i));
                    genericData = mol.getData((String) attribs2write.get(i),
                            true);

                    if (genericData == null)
                    {
                        logger.warn((String) attribs2write.get(i) +
                            " data entry doesn't exist in molecule: " +
                            mol.getTitle());
                    }
                    else
                    {
                        if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                        {
                            pairData = (JOEPairData) genericData;
                            writePairData(_writerProp, ps, pairData);
                        }
                    }
                }
            }
        }
    }

    private static void writeArrayCML1(CMLWriterProperties _writerProp,
        PrintStream ps, String dataType, String title, String value, int size,
        ResultCMLProperties props)
    {
        Hashtable attributes = new Hashtable();
        attributes.clear();
        attributes.put("title", title);
        attributes.put("size", Integer.toString(size));

        Enumeration e = props.getCMLProperties();
        writeArrayProperties(e, attributes);

        CMLMoleculeWriterBase.writeOpenTag(ps, _writerProp, dataType,
            attributes, false);
        CMLMoleculeWriterBase.write(ps, value);
        CMLMoleculeWriterBase.writeCloseTag(ps, _writerProp, dataType);
    }

    private static void writeArrayCML2(CMLWriterProperties _writerProp,
        PrintStream ps, String dataType, String title, String value, int size,
        ResultCMLProperties props)
    {
        Hashtable attributes = new Hashtable();
        attributes.clear();
        attributes.put("title", title);
        attributes.put("dataType", dataType);
        attributes.put("size", Integer.toString(size));

        Enumeration e = props.getCMLProperties();
        writeArrayProperties(e, attributes);

        CMLMoleculeWriterBase.writeOpenTag(ps, _writerProp, "array",
            attributes, false);
        CMLMoleculeWriterBase.write(ps, value);
        CMLMoleculeWriterBase.writeCloseTag(ps, _writerProp, "array");
    }

    private static void writeArrayProperties(Enumeration e, Hashtable attributes)
    {
        StringString ss;
        boolean delimiterWritten = false;

        if (e != null)
        {
            while (e.hasMoreElements())
            {
                ss = (StringString) e.nextElement();

                // ignore 'size' in CML properties
                if (!ss.s1.equals("size"))
                {
                    if (ss.s1.equals("delimiter"))
                    {
                        delimiterWritten = true;
                    }

                    attributes.put(ss.s1, ss.s2);
                }
            }
        }

        if (!delimiterWritten &&
                !ChemicalMarkupLanguage.getDefaultDelimiter().equals(" "))
        {
            attributes.put("delimiter",
                ChemicalMarkupLanguage.getDefaultDelimiter());
        }
    }

    private static void writeMatrixCML2(CMLWriterProperties _writerProp,
        PrintStream ps, String dataType, String title, String value, int rows,
        int columns, ResultCMLProperties props)
    {
        Hashtable attributes = new Hashtable();
        attributes.clear();
        attributes.put("title", title);
        attributes.put("dataType", dataType);
        attributes.put("rows", Integer.toString(rows));
        attributes.put("columns", Integer.toString(columns));

        Enumeration e = props.getCMLProperties();
        writeMatrixProperties(e, attributes);

        CMLMoleculeWriterBase.writeOpenTag(ps, _writerProp, "matrix",
            attributes, false);
        CMLMoleculeWriterBase.write(ps, value);
        CMLMoleculeWriterBase.writeCloseTag(ps, _writerProp, "matrix");
    }

    private static void writeMatrixProperties(Enumeration e,
        Hashtable attributes)
    {
        StringString ss;
        boolean delimiterWritten = false;

        if (e != null)
        {
            while (e.hasMoreElements())
            {
                ss = (StringString) e.nextElement();

                // ignore 'rows' and 'columns' in CML properties
                if (!ss.s1.equals("rows") && !ss.s1.equals("columns"))
                {
                    if (ss.s1.equals("delimiter"))
                    {
                        delimiterWritten = true;
                    }

                    attributes.put(ss.s1, ss.s2);
                }
            }
        }

        if (!delimiterWritten &&
                !ChemicalMarkupLanguage.getDefaultDelimiter().equals(" "))
        {
            attributes.put("delimiter",
                ChemicalMarkupLanguage.getDefaultDelimiter());
        }
    }

    private static void writeProperties(CMLWriterProperties _writerProp,
        
    //PrintStream ps,
    Enumeration e, Hashtable attributes)
    {
        StringString ss;

        if (e != null)
        {
            while (e.hasMoreElements())
            {
                ss = (StringString) e.nextElement();

                if (!_writerProp.storeChemistryKernelInfo())
                {
                    if (ss.s1.equals(JOEKernel.CML_KERNEL_REFERENCE))
                    {
                        continue;
                    }
                }

                attributes.put(ss.s1, ss.s2);
            }
        }
    }

    private static void writeScalarCML1(CMLWriterProperties _writerProp,
        PrintStream ps, String dataType, String title, String value,
        ResultCMLProperties props)
    {
        Hashtable attributes = new Hashtable();
        attributes.clear();
        attributes.put("title", title);

        Enumeration e = props.getCMLProperties();
        writeProperties(_writerProp, e, attributes);

        CMLMoleculeWriterBase.writeOpenTag(ps, _writerProp, dataType,
            attributes, false);
        CMLMoleculeWriterBase.write(ps, value);
        CMLMoleculeWriterBase.writeCloseTag(ps, _writerProp, dataType);
    }

    private static void writeScalarCML2(CMLWriterProperties _writerProp,
        PrintStream ps, String dataType, String title, String value,
        ResultCMLProperties props)
    {
        Hashtable attributes = new Hashtable();
        attributes.clear();
        attributes.put("title", title);
        attributes.put("dataType", dataType);

        Enumeration e = props.getCMLProperties();
        writeProperties(_writerProp, e, attributes);

        CMLMoleculeWriterBase.writeOpenTag(ps, _writerProp, "scalar",
            attributes, false);
        CMLMoleculeWriterBase.write(ps, value);
        CMLMoleculeWriterBase.writeCloseTag(ps, _writerProp, "scalar");
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
