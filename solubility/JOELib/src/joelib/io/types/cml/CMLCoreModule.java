///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: CMLCoreModule.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu,
//          egonw@sci.kun.nl, wegnerj@informatik.uni-tuebingen.de
//Version:  $Revision: 1.20 $
//			$Date: 2004/07/25 20:43:21 $
//			$Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public License
//as published by the Free Software Foundation; either version 2.1
//of the License, or (at your option) any later version.
//All we ask is that proper credit is given for our work, which includes
//- but is not limited to - adding the above copyright notice to the beginning
//of your source code files, and to any copyright notice that you may distribute
//with programs based on this work.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.util.*;

import org.apache.log4j.*;

import org.xml.sax.*;

import joelib.desc.result.*;

import joelib.io.*;
import joelib.io.types.cml.elements.*;

import joelib.math.CrystalGeometryTools;

import joelib.util.*;
import joelib.util.types.*;


/**
 * Core CML 1.x and 2.0 elements are parsed by this class.
 *
 * <p>Please file a bug report if this parser fails to parse
 * a certain element or attribute value in a valid CML document.
 *
 * @author egonw
 * @author c.steinbeck@uni-koeln.de
 * @author gezelter@maul.chem.nd.edu
 * @author wegnerj
 * @license LGPL
 * @cvsversion    $Revision: 1.20 $, $Date: 2004/07/25 20:43:21 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 **/
public class CMLCoreModule implements ModuleInterface
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.CMLCoreModule");
    public final static int UNKNOWN = -1;
    public final static int STRING = 1;
    public final static int LINK = 2;
    public final static int FLOAT = 3;
    public final static int INTEGER = 4;
    public final static int STRINGARRAY = 5;
    public final static int FLOATARRAY = 6;
    public final static int INTEGERARRAY = 7;
    public final static int FLOATMATRIX = 8;
    public final static int COORDINATE2 = 9;
    public final static int COORDINATE3 = 10;
    public final static int ANGLE = 11;
    public final static int TORSION = 12;
    public final static int LIST = 13;
    public final static int MOLECULE = 14;
    public final static int ATOM = 15;
    public final static int ATOMARRAY = 16;
    public final static int BOND = 17;
    public final static int BONDARRAY = 18;
    public final static int ELECTRON = 19;
    public final static int REACTION = 20;
    public final static int CRYSTAL = 21;
    public final static int SEQUENCE = 22;
    public final static int FEATURE = 23;
    public final static int MATRIX = 24;
    public final static int ARRAY = 25;
    public final static int SCALAR = 26;
    public final static int BOND_STEREO = 27;
    public final static int NAME = 28;
    public final static int SYMMETRY = 29;

    //~ Instance fields ////////////////////////////////////////////////////////

    public Hashtable atomElements = new Hashtable(23);
    public Hashtable elements = new Hashtable(23);
    protected final String SYSTEMID = "CML-1999-05-15";

    // array
    protected ArrayCML array;
    protected CDOInterface cdo;

    // matrix
    protected MatrixCML matrix;
    protected ScalarCML scalar;
    protected String BUILTIN;
    protected String DICTREF;
    protected String arrayTitle;
    protected String currentChars;
    protected String delimiter;
    protected String elementTitle;
    protected String matrixColumns;
    protected String matrixDelimiter;
    protected String matrixRows;
    protected String matrixTitle;
    protected String moleculeName;
    protected Vector arrays;
    protected Vector atomDictRefs;
    protected Vector atomParities;
    protected Vector bondARef1;
    protected Vector bondARef2;
    protected Vector bondDictRefs;
    protected Vector bondStereo;
    protected Vector bondid;
    protected Vector elid;
    protected Vector elsym;
    protected Vector eltitles;
    protected Vector formalCharges;
    protected Vector hCounts;
    protected Vector isotopes;
    protected Vector matrices;
    protected Vector order;
    protected Vector partialCharges;

    //descriptors
    protected Vector scalars;
    protected Vector strings;
    protected Vector x2;
    protected Vector x3;
    protected Vector xfract;
    protected Vector y2;
    protected Vector y3;
    protected Vector yfract;
    protected Vector z3;
    protected Vector zfract;
    protected double[] a;
    protected double[] b;
    protected double[] c;

    //crystal
    protected double[] unitcellparams;
    protected boolean cartesianAxesSet = false;
    protected boolean stereoGiven;
    protected int atomCounter;

    //scalar
    protected int bondCounter;
    protected int crystalScalar;
    protected int curRef;
    protected int currentElement;

    //~ Constructors ///////////////////////////////////////////////////////////

    public CMLCoreModule(CDOInterface cdo)
    {
        initialize();
        this.cdo = cdo;
    }

    public CMLCoreModule(ModuleInterface conv)
    {
        initialize();
        inherit(conv);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void characterData(CMLStack xpath, char[] ch, int start, int length)
    {
        currentChars = currentChars + new String(ch, start, length);

        //		if (logger.isDebugEnabled())
        //			logger.debug("CD: " + currentChars);
        String s = (new String(ch, start, length));

        //System.out.println("start:"+start+" length:"+length+" s:"+s);
        //        if (s.trim().length() == 0)
        //        {
        //            return;
        //        }
        //System.out.println(BUILTIN+" "+elementTitle+"="+s);
        switch (currentElement)
        {
        case MATRIX:

            if (!matrix.characterData(s))
            {
                logger.error("Error storing matrix character data.");
            }

            break;

        case ARRAY:

            if (!array.characterData(s))
            {
                logger.error("Error storing array character data.");
            }

            break;

        case SCALAR:

            if (!scalar.characterData(s))
            {
                logger.error("Error storing scalar character data.");
            }

            break;
        }
    }

    public void endDocument()
    {
        cdo.endDocument();

        if (logger.isDebugEnabled())
        {
            logger.debug("End XML Doc");
        }
    }

    public void endElement(CMLStack xpath, String uri, String name, String raw)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("EndElement: " + name);
        }

        setCurrentElement(name);

        currentChars = currentChars.trim();

        switch (currentElement)
        {
        case BOND:

            if (!stereoGiven)
            {
                bondStereo.addElement("");

                //System.out.println("Add stereo info <empty>");
            }

            if (bondStereo.size() > bondDictRefs.size())
            {
                bondDictRefs.addElement(null);
            }

            break;

        case ATOM:

            if (atomCounter > eltitles.size())
            {
                eltitles.addElement(null);
            }

            if (elsym.size() > formalCharges.size())
            {
                /* while strictly undefined, assume zero
                charge when no number is given */
                formalCharges.addElement("0");
            }

            if (elsym.size() > hCounts.size())
            {
                /* while strictly undefined, assume zero
                implicit hydrogens when no number is given */
                hCounts.addElement("0");
            }

            if (elsym.size() > isotopes.size())
            {
                /* while strictly undefined, assume zero
                charge when no number is given */
                isotopes.addElement("0");
            }

            /* It may happen that not all atoms have
               associated 2D coordinates. accept that */
            if ((elsym.size() > x2.size()) && (x2.size() != 0))
            {
                /* apparently, the previous atoms had atomic
                   coordinates, add 'null' for this atom */
                x2.add(null);
                y2.add(null);
            }

            break;

        case BOND_STEREO:
            addArrayElementsTo(bondStereo, currentChars);

            break;

        case MOLECULE:
            storeData();
            cdo.endObject("Molecule");

            break;

        case CRYSTAL:

            if (crystalScalar > 0)
            {
                // convert unit cell parameters to cartesians
                double[][] axes = CrystalGeometryTools.notionalToCartesian(unitcellparams[0],
                        unitcellparams[1], unitcellparams[2],
                        unitcellparams[3], unitcellparams[4], unitcellparams[5]);
                a[0] = axes[0][0];
                a[1] = axes[0][1];
                a[2] = axes[0][2];
                b[0] = axes[1][0];
                b[1] = axes[1][1];
                b[2] = axes[1][2];
                c[0] = axes[2][0];
                c[1] = axes[2][1];
                c[2] = axes[2][2];
                cartesianAxesSet = true;
                cdo.startObject("a-axis");
                cdo.setObjectProperty("a-axis", "x", Double.toString(a[0]));
                cdo.setObjectProperty("a-axis", "y", Double.toString(a[1]));
                cdo.setObjectProperty("a-axis", "z", Double.toString(a[2]));
                cdo.endObject("a-axis");
                cdo.startObject("b-axis");
                cdo.setObjectProperty("b-axis", "x", Double.toString(b[0]));
                cdo.setObjectProperty("b-axis", "y", Double.toString(b[1]));
                cdo.setObjectProperty("b-axis", "z", Double.toString(b[2]));
                cdo.endObject("b-axis");
                cdo.startObject("c-axis");
                cdo.setObjectProperty("c-axis", "x", Double.toString(c[0]));
                cdo.setObjectProperty("c-axis", "y", Double.toString(c[1]));
                cdo.setObjectProperty("c-axis", "z", Double.toString(c[2]));
                cdo.endObject("c-axis");
            }
            else
            {
                logger.error("Could not find crystal unit cell parameters");
            }

            cdo.endObject("Crystal");

            break;

        case COORDINATE3:

            if (BUILTIN.equals("xyz3"))
            {
                logger.debug("New coord3 xyz3 found: " + currentChars);

                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars);
                    x3.addElement(st.nextToken());
                    y3.addElement(st.nextToken());
                    z3.addElement(st.nextToken());

                    if (logger.isDebugEnabled())
                    {
                        logger.debug("coord3 x3.length: " + x3.size());
                        logger.debug("coord3 y3.length: " + y3.size());
                        logger.debug("coord3 z3.length: " + z3.size());
                    }
                }
                 catch (Exception e)
                {
                    logger.error("CMLParsing error while setting coordinate3!");
                }
            }
            else
            {
                logger.warn("Unknown coordinate3 BUILTIN: " + BUILTIN);
            }

            break;

        case MATRIX:
            matrix.endElement(name);

            break;

        case ARRAY:
            array.endElement(name);

            break;

        case SCALAR:

            if (xpath.toString().endsWith("crystal/scalar/"))
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Going to set a crystal parameter: " +
                        crystalScalar + " to " + currentChars);
                }

                try
                {
                    unitcellparams[crystalScalar - 1] = Double.parseDouble(currentChars.trim());
                }
                 catch (NumberFormatException exception)
                {
                    logger.error("Content must a float: " + currentChars);
                }
            }
            else if (xpath.toString().endsWith("bond/scalar/"))
            {
                if (DICTREF.equals("mdl:stereo"))
                {
                    bondStereo.addElement(currentChars.trim());
                    stereoGiven = true;
                }
            }
            else if (xpath.toString().endsWith("atom/scalar/"))
            {
                if (DICTREF.endsWith("partialCharge"))
                {
                    partialCharges.addElement(scalar.getAllCharacterData());
                }
            }
            else
            {
                // store as descriptor
                if (scalar.getTitle() == null)
                {
                    //logger.error("xpath: "+xpath.toString());
                    logger.error("No title defined for scalar element: " +
                        scalar.getAllCharacterData());
                }

                scalar.endElement(name);
            }

            break;

        case NAME:

            //System.out.println("Molecule name :"+s.trim());
            moleculeName = currentChars.trim();

            break;

        case STRING:

            if (BUILTIN.equals("elementType"))
            {
                logger.debug("Element: " + currentChars);
                elsym.addElement(currentChars);
            }
            else if (BUILTIN.equals("atomRef"))
            {
                curRef++;

                //logger.debug("Bond: ref #" + curRef);
                if (curRef == 1)
                {
                    bondARef1.addElement(currentChars.trim());
                }
                else if (curRef == 2)
                {
                    bondARef2.addElement(currentChars.trim());
                }
            }
            else if (BUILTIN.equals("atomRefs"))
            {
                StringTokenizer st = new StringTokenizer(currentChars.trim());
                bondARef1.addElement(st.nextToken());
                bondARef2.addElement(st.nextToken());
            }
            else if (BUILTIN.equals("order"))
            {
                //logger.debug("Bond: order " + s.trim());
                order.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("stereo"))
            {
                //logger.debug("Bond: stereo " + s.trim());
                //System.out.println("Bond: stereo " + s.trim());
                bondStereo.addElement(currentChars.trim());
                stereoGiven = true;
            }
            else if (BUILTIN.equals("formalCharge"))
            {
                // NOTE: this combination is in violation of the CML DTD!!!
                //logger.debug("Charge: " + s.trim());
                formalCharges.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("isotope"))
            {
                isotopes.addElement(currentChars.trim());
            }
            else
            {
                String tmp = currentChars.trim();

                if (tmp.length() != 0)
                {
                    strings.add(new StringString(elementTitle, tmp));
                }
            }

            break;

        case FLOAT:

            if (BUILTIN.equals("x3"))
            {
                x3.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("y3"))
            {
                y3.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("z3"))
            {
                z3.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("x2"))
            {
                x2.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("y2"))
            {
                y2.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("order"))
            {
                // NOTE: this combination is in violation of the CML DTD!!!
                order.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("charge") ||
                    BUILTIN.equals("partialCharge"))
            {
                partialCharges.addElement(currentChars.trim());
            }
            else
            {
                String tmp = currentChars.trim();
                DoubleResult dr = new DoubleResult();

                if (!dr.fromString(IOTypeHolder.instance().getIOType("CML"), tmp))
                {
                    logger.error("Double entry " + elementTitle + "=" + tmp +
                        " was not successfully parsed.");
                }
                else
                {
                    scalars.add(new StringObject(elementTitle, dr));
                }
            }

            break;

        case INTEGER:

            if (BUILTIN.equals("formalCharge"))
            {
                formalCharges.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("hydrogenCount"))
            {
                hCounts.addElement(currentChars.trim());
            }
            else if (BUILTIN.equals("isotope"))
            {
                isotopes.addElement(currentChars.trim());
            }
            else
            {
                IntResult ir = new IntResult();

                if (!ir.fromString(IOTypeHolder.instance().getIOType("CML"),
                            currentChars.trim()))
                {
                    logger.error("Integer entry " + elementTitle + "=" +
                        currentChars.trim() + " was not successfully parsed.");
                }
                else
                {
                    scalars.add(new StringObject(elementTitle, ir));
                }
            }

            break;

        case COORDINATE2:

            if (BUILTIN.equals("xy2"))
            {
                //logger.debug("New coord2 xy2 found." + s);
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());
                    x2.addElement(st.nextToken());
                    y2.addElement(st.nextToken());
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 175, 1);
                }
            }

            break;

        case STRINGARRAY:

            if (BUILTIN.equals("id") || BUILTIN.equals("atomId"))
            {
                // use of "id" seems incorrect by quick look at DTD
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        String token = st.nextToken();

                        //logger.debug("StringArray (Token): " + token);
                        elid.addElement(token);
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 186, 1);
                }
            }
            else if (BUILTIN.equals("elementType"))
            {
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        elsym.addElement(st.nextToken());
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 194, 1);
                }
            }
            else if (BUILTIN.equals("atomRefs"))
            {
                curRef++;

                //logger.debug("New atomRefs found: " + curRef);
                try
                {
                    boolean countBonds = (bondCounter == 0) ? true : false;
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        if (countBonds)
                        {
                            bondCounter++;
                        }

                        String token = st.nextToken();

                        //logger.debug("Token: " + token);
                        if (curRef == 1)
                        {
                            bondARef1.addElement(token);
                        }
                        else if (curRef == 2)
                        {
                            bondARef2.addElement(token);
                        }
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 194, 1);
                }
            }
            else if (BUILTIN.equals("order"))
            {
                //logger.debug("New bond order found.");
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        String token = st.nextToken();

                        //logger.debug("Token: " + token);
                        order.addElement(token);
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 194, 1);
                }
            }
            else if (BUILTIN.equals("stereo"))
            {
                //logger.debug("New bond order found.");
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        String token = st.nextToken();

                        //logger.debug("Token: " + token);
                        bondStereo.addElement(token);
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 194, 1);
                }
            }

            break;

        case INTEGERARRAY:

            //logger.debug("IntegerArray: builtin = " + BUILTIN);
            if (BUILTIN.equals("formalCharge"))
            {
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        String token = st.nextToken();

                        //logger.debug("Charge added: " + token);
                        formalCharges.addElement(token);
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 205, 1);
                }
            }
            else if (BUILTIN.equals("hydrogenCount"))
            {
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars);

                    while (st.hasMoreTokens())
                    {
                        String token = st.nextToken();

                        //logger.debug("Hydrogen count added: " + token);
                        hCounts.addElement(token);
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 205, 1);
                }
            }
            else if (BUILTIN.equals("isotope"))
            {
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        String token = st.nextToken();

                        //logger.debug("Isotope added: " + token);
                        isotopes.addElement(token);
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 205, 1);
                }
            }
            else
            {
                String tmp = currentChars.trim();

                if (tmp.length() != 0)
                {
                    //System.out.println("intArray " + elementTitle);
                    IntArrayResult iar = new IntArrayResult();

                    if (delimiter == null)
                    {
                        delimiter = " \t\r\n";
                    }
                    else
                    {
                        iar.addCMLProperty(new StringString("delimiter",
                                delimiter));
                    }

                    if (!iar.fromString(IOTypeHolder.instance().getIOType("CML"),
                                tmp))
                    {
                        logger.error("Integer array entry " + arrayTitle + "=" +
                            tmp + " was not successfully parsed.");
                    }

                    iar.setIntArray(ArrayHelper.intArrayFromSimpleString(tmp,
                            delimiter));
                    arrays.add(new StringObject(elementTitle, iar));
                }
            }

            break;

        case FLOATARRAY:

            //System.out.println(BUILTIN+"="+s);
            if (BUILTIN.equals("x3"))
            {
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        x3.addElement(st.nextToken());
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 205, 1);
                }
            }
            else if (BUILTIN.equals("y3"))
            {
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        y3.addElement(st.nextToken());
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 213, 1);
                }
            }
            else if (BUILTIN.equals("z3"))
            {
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        z3.addElement(st.nextToken());
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 221, 1);
                }
            }
            else if (BUILTIN.equals("x2"))
            {
                //logger.debug("New floatArray found.");
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        x2.addElement(st.nextToken());
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 205, 1);
                }
            }
            else if (BUILTIN.equals("y2"))
            {
                //logger.debug("New floatArray found.");
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        y2.addElement(st.nextToken());
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 454, 1);
                }
            }
            else if (BUILTIN.equals("partialCharge"))
            {
                //logger.debug("New floatArray with partial charges found.");
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        partialCharges.addElement(st.nextToken());
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 462, 1);
                }
            }
            else if (BUILTIN.equals("formalCharge"))
            {
                //logger.debug("New floatArray with partial charges found.");
                try
                {
                    StringTokenizer st = new StringTokenizer(currentChars.trim());

                    while (st.hasMoreTokens())
                    {
                        formalCharges.addElement(st.nextToken());
                    }
                }
                 catch (Exception e)
                {
                    notify("CMLParsing error: " + e, SYSTEMID, 462, 1);
                }
            }
            else
            {
                String tmp = currentChars.trim();

                if (tmp.length() != 0)
                {
                    //System.out.println("floatArray " + elementTitle);
                    DoubleArrayResult dar = new DoubleArrayResult();

                    if (delimiter == null)
                    {
                        delimiter = " \t\r\n";
                    }
                    else
                    {
                        dar.addCMLProperty(new StringString("delimiter",
                                delimiter));
                    }

                    if (!dar.fromString(IOTypeHolder.instance().getIOType("CML"),
                                tmp))
                    {
                        logger.error("Double array entry " + arrayTitle + "=" +
                            tmp + " was not successfully parsed.");
                    }
                    else
                    {
                        dar.setDoubleArray(ArrayHelper.doubleArrayFromSimpleString(
                                tmp, delimiter));
                        arrays.add(new StringObject(elementTitle, dar));
                    }
                }
            }

            break;

        case FLOATMATRIX:

            //logger.debug("FloatMatrix: builtin = " + BUILTIN);
            //if (BUILTIN.equals("???"))
            //{
            //}
            //else
            //{
            String tmp = currentChars.trim();

            if (tmp.length() != 0)
            {
                // much more efficient and more standard
                DoubleMatrixResult matrix = new DoubleMatrixResult();

                if (matrixDelimiter == null)
                {
                    matrixDelimiter = " \t\r\n";
                }
                else
                {
                    matrix.addCMLProperty(new StringString("delimiter",
                            matrixDelimiter));
                }

                if (matrixRows == null)
                {
                    logger.error("Number of rows is missing in FloatMatrix '" +
                        matrixTitle + "'.");
                }
                else if (matrixColumns == null)
                {
                    logger.error(
                        "Number of columns is missing in FloatMatrix '" +
                        matrixTitle + "'.");
                }
                else
                {
                    int rows = Integer.parseInt(matrixRows);
                    int columns = Integer.parseInt(matrixColumns);
                    matrix.value = MatrixHelper.doubleMatrixFromSimpleString(tmp,
                            rows, columns, matrixDelimiter);

                    if (matrix.value != null)
                    {
                        matrices.add(new StringObject(matrixTitle, matrix));
                    }

                    //floatMatrices.add(new StringObject(matrixTitle, tmp));
                }

                //}
            }

            break;

        case LIST:
            cdo.endObject("SetOfMolecules");

            break;
        }

        currentChars = "";
        BUILTIN = "";
        elementTitle = "";
    }

    public void inherit(ModuleInterface convention)
    {
        if (convention instanceof CMLCoreModule)
        {
            CMLCoreModule conv = (CMLCoreModule) convention;
            this.cdo = conv.returnCDO();
            this.BUILTIN = conv.BUILTIN;
            this.elsym = conv.elsym;
            this.eltitles = conv.eltitles;
            this.atomCounter = conv.atomCounter;
            this.elid = conv.elid;
            this.formalCharges = conv.formalCharges;
            this.partialCharges = conv.partialCharges;
            this.isotopes = conv.isotopes;
            this.x3 = conv.x3;
            this.y3 = conv.y3;
            this.z3 = conv.z3;
            this.x2 = conv.x2;
            this.y2 = conv.y2;
            this.hCounts = conv.hCounts;
            this.atomParities = conv.atomParities;
            this.bondid = conv.bondid;
            this.bondARef1 = conv.bondARef1;
            this.bondARef2 = conv.bondARef2;
            this.order = conv.order;
            this.bondStereo = conv.bondStereo;
            this.curRef = conv.curRef;
            this.bondCounter = conv.bondCounter;
            this.atomDictRefs = conv.atomDictRefs;
            this.bondDictRefs = conv.bondDictRefs;

            this.atomElements = conv.atomElements;

            //descriptors
            this.scalars = conv.scalars;
            this.strings = conv.strings;
            this.matrices = conv.matrices;
            this.arrays = conv.arrays;

            // parser
            this.scalar = conv.scalar;
            this.array = conv.array;
            this.matrix = conv.matrix;
        }
    }

    public CDOInterface returnCDO()
    {
        return (CDOInterface) this.cdo;
    }

    public void startDocument()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Start XML Doc");
        }

        cdo.startDocument();
        newMolecule();
        BUILTIN = "";
        curRef = 0;
        currentChars = "";
    }

    public void startElement(CMLStack xpath, String uri, String local,
        String raw, Attributes atts)
    {
        String name = local;

        if (logger.isDebugEnabled())
        {
            logger.debug("StartElement: " + name);
        }

        currentChars = "";

        BUILTIN = "";
        DICTREF = "";

        for (int i = 0; i < atts.getLength(); i++)
        {
            String qname = atts.getQName(i);

            if (qname.equals("builtin"))
            {
                BUILTIN = atts.getValue(i);

                if (logger.isDebugEnabled())
                {
                    logger.debug(name + "->BUILTIN found: " + atts.getValue(i));
                }
            }
            else if (qname.equals("dictRef"))
            {
                DICTREF = atts.getValue(i);

                if (logger.isDebugEnabled())
                {
                    logger.debug(name + "->DICTREF found: " + atts.getValue(i));
                }
            }
            else if (qname.equals("title"))
            {
                elementTitle = atts.getValue(i);

                if (logger.isDebugEnabled())
                {
                    logger.debug(name + "->TITLE found: " + atts.getValue(i));
                }
            }
            else
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Qname: " + qname);
                }
            }
        }

        setCurrentElement(name);

        switch (currentElement)
        {
        case ATOM:
            atomCounter++;

            for (int i = 0; i < atts.getLength(); i++)
            {
                String att = atts.getQName(i);
                String value = atts.getValue(i);

                if (att.equals("id"))
                { // this is supported in CML 1.x
                    elid.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("elementType"))
                {
                    elsym.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("title"))
                {
                    eltitles.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("x2"))
                {
                    x2.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("xy2"))
                {
                    StringTokenizer tokenizer = new StringTokenizer(value);
                    x2.addElement(tokenizer.nextToken());
                    y2.addElement(tokenizer.nextToken());
                }

                // this is supported in CML 2.0 
                else if (att.equals("xyzFract"))
                {
                    StringTokenizer tokenizer = new StringTokenizer(value);
                    xfract.addElement(tokenizer.nextToken());
                    yfract.addElement(tokenizer.nextToken());
                    zfract.addElement(tokenizer.nextToken());
                }

                // this is supported in CML 2.0 
                else if (att.equals("xyz3"))
                {
                    StringTokenizer tokenizer = new StringTokenizer(value);
                    x3.addElement(tokenizer.nextToken());
                    y3.addElement(tokenizer.nextToken());
                    z3.addElement(tokenizer.nextToken());
                }

                // this is supported in CML 2.0 
                else if (att.equals("y2"))
                {
                    y2.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("x3"))
                {
                    x3.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("y3"))
                {
                    y3.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("z3"))
                {
                    z3.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("xFract"))
                {
                    xfract.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("yFract"))
                {
                    yfract.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("zFract"))
                {
                    zfract.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("formalCharge"))
                {
                    formalCharges.addElement(value);
                }

                // this is supported in CML 2.0 
                else if (att.equals("hydrogenCount"))
                {
                    hCounts.addElement(value);
                }
                else if (att.equals("isotope"))
                {
                    isotopes.addElement(value);
                }
                else if (att.equals("dictRef"))
                {
                    atomDictRefs.addElement(value);
                }
                else
                {
                    logger.warn("Unparsed attribute: " + att);
                }
            }

            break;

        case BOND:
            stereoGiven = false;
            bondCounter++;

            for (int i = 0; i < atts.getLength(); i++)
            {
                String att = atts.getQName(i);

                if (logger.isDebugEnabled())
                {
                    logger.debug("B2 " + att + "=" + atts.getValue(i));
                }

                if (att.equals("id"))
                {
                    bondid.addElement(atts.getValue(i));
                    logger.debug("B3 " + bondid);
                }
                else if (att.equals("atomRefs") || // this is CML 1.x support
                        att.equals("atomRefs2")) // this is CML 1.x support
                { // this is CML 2.0 support

                    // expect exactly two references
                    try
                    {
                        StringTokenizer st = new StringTokenizer(atts.getValue(
                                    i));
                        bondARef1.addElement((String) st.nextElement());
                        bondARef2.addElement((String) st.nextElement());
                    }
                     catch (Exception e)
                    {
                        logger.error("Error in CML file: " + e.toString());
                    }
                }
                else if (att.equals("order"))
                { // this is CML 2.0 support
                    order.addElement(atts.getValue(i).trim());
                }
                else if (att.equals("stereo"))
                { // this is CML 2.0 support
                    bondStereo.addElement(atts.getValue(i).trim());
                    stereoGiven = true;
                    System.out.println("Add stereo info " +
                        atts.getValue(i).trim());
                }
                else if (att.equals("dictRef"))
                {
                    bondDictRefs.addElement(atts.getValue(i).trim());
                }
            }

            curRef = 0;

            break;

        case BONDARRAY:

            boolean bondsCounted = false;

            for (int i = 0; i < atts.getLength(); i++)
            {
                String att = atts.getQName(i);
                int count = 0;

                if (att.equals("bondID"))
                {
                    count = addArrayElementsTo(bondid, atts.getValue(i));
                }
                else if (att.equals("atomRefs1"))
                {
                    count = addArrayElementsTo(bondARef1, atts.getValue(i));
                }
                else if (att.equals("atomRefs2"))
                {
                    count = addArrayElementsTo(bondARef2, atts.getValue(i));
                }
                else if (att.equals("atomRef1"))
                {
                    count = addArrayElementsTo(bondARef1, atts.getValue(i));
                }
                else if (att.equals("atomRef2"))
                {
                    count = addArrayElementsTo(bondARef2, atts.getValue(i));
                }
                else if (att.equals("order"))
                {
                    count = addArrayElementsTo(order, atts.getValue(i));
                }
                else if (att.equals("bondStereo") || att.equals("stereo"))
                {
                    //System.out.println("bondStereo "+atts.getValue(i));
                    count = addArrayElementsTo(bondStereo, atts.getValue(i));
                }
                else
                {
                    logger.warn("Unparsed attribute: " + att);
                }

                if (!bondsCounted)
                {
                    bondCounter += count;
                    bondsCounted = true;
                }
            }

            curRef = 0;

            break;

        case ATOMARRAY:

            boolean atomsCounted = false;

            for (int i = 0; i < atts.getLength(); i++)
            {
                String att = atts.getQName(i);
                int count = 0;

                if (att.equals("atomID"))
                {
                    count = addArrayElementsTo(elid, atts.getValue(i));
                }
                else if (att.equals("elementType"))
                {
                    count = addArrayElementsTo(elsym, atts.getValue(i));
                }
                else if (att.equals("x2"))
                {
                    count = addArrayElementsTo(x2, atts.getValue(i));
                }
                else if (att.equals("y2"))
                {
                    count = addArrayElementsTo(y2, atts.getValue(i));
                }
                else if (att.equals("x3"))
                {
                    count = addArrayElementsTo(x3, atts.getValue(i));
                }
                else if (att.equals("y3"))
                {
                    count = addArrayElementsTo(y3, atts.getValue(i));
                }
                else if (att.equals("z3"))
                {
                    count = addArrayElementsTo(z3, atts.getValue(i));
                }
                else if (att.equals("xFract"))
                {
                    count = addArrayElementsTo(xfract, atts.getValue(i));
                }
                else if (att.equals("yFract"))
                {
                    count = addArrayElementsTo(yfract, atts.getValue(i));
                }
                else if (att.equals("zFract"))
                {
                    count = addArrayElementsTo(zfract, atts.getValue(i));
                }
                else
                {
                    logger.warn("Unparsed attribute: " + att);
                }

                if (!atomsCounted)
                {
                    atomCounter += count;
                    atomsCounted = true;
                }
            }

            break;

        case FLOATMATRIX:
            matrixRows = null;
            matrixColumns = null;
            matrixDelimiter = null;

            for (int i = 0; i < atts.getLength(); i++)
            {
                if (atts.getQName(i).equals("title"))
                {
                    matrixTitle = atts.getValue(i);
                }
                else if (atts.getQName(i).equals("rows"))
                {
                    matrixRows = atts.getValue(i);
                }
                else if (atts.getQName(i).equals("columns"))
                {
                    matrixColumns = atts.getValue(i);
                }
                else if (atts.getQName(i).equals("delimiter"))
                {
                    matrixDelimiter = atts.getValue(i);
                }
            }

            break;

        case MOLECULE:
            newMolecule();
            BUILTIN = "";
            cdo.startObject("Molecule");

            for (int i = 0; i < atts.getLength(); i++)
            {
                if (atts.getQName(i).equals("title"))
                {
                    moleculeName = atts.getValue(i);
                }

                //else if (atts.getQName(i).equals("id"))
                //{
                //    //moleculeID= atts.getValue(i);
                //}
            }

            break;

        case LIST:
            cdo.startObject("SetOfMolecules");

            break;

        case MATRIX:
            matrix.clear();

            for (int i = 0; i < atts.getLength(); i++)
            {
                matrix.startElement(atts.getQName(i), atts.getValue(i));
            }

            break;

        case ARRAY:
            array.clear();

            for (int i = 0; i < atts.getLength(); i++)
            {
                array.startElement(atts.getQName(i), atts.getValue(i));
            }

            break;

        case SCALAR:

            if (xpath.toString().endsWith("crystal/scalar/"))
            {
                crystalScalar++;
            }
            else
            {
                scalar.clear();

                for (int i = 0; i < atts.getLength(); i++)
                {
                    scalar.startElement(atts.getQName(i), atts.getValue(i));
                }
            }

            break;

        case CRYSTAL:
            newCrystalData();
            cdo.startObject("Crystal");

            for (int i = 0; i < atts.getLength(); i++)
            {
                String att = atts.getQName(i);

                if (att.equals("z"))
                {
                    cdo.setObjectProperty("Crystal", "z", atts.getValue(i));
                }
            }

            break;

        case SYMMETRY:

            for (int i = 0; i < atts.getLength(); i++)
            {
                String att = atts.getQName(i);

                if (att.equals("spaceGroup"))
                {
                    cdo.setObjectProperty("Crystal", "spacegroup",
                        atts.getValue(i));
                }
            }

            break;
        }
    }

    protected void setCurrentElement(String name)
    {
        //logger.debug("Current element: " + name);
        //System.out.println("Current element: " + name);
        Integer integer = (Integer) elements.get(name);

        if (integer != null)
        {
            currentElement = integer.intValue();
        }
        else
        {
            currentElement = UNKNOWN;
        }
    }

    /**
     * Clean all data about read bonds.
     */
    protected void newCrystalData()
    {
        unitcellparams = new double[6];
        cartesianAxesSet = false;
        crystalScalar = 0;
        a = new double[3];
        b = new double[3];
        c = new double[3];
    }

    protected void newMolecule()
    {
        atomCounter = 0;
        elsym = new Vector();
        eltitles = new Vector();
        elid = new Vector();
        formalCharges = new Vector();
        partialCharges = new Vector();
        isotopes = new Vector();
        x3 = new Vector();
        y3 = new Vector();
        z3 = new Vector();
        x2 = new Vector();
        y2 = new Vector();
        hCounts = new Vector();
        atomParities = new Vector();
        bondid = new Vector();
        bondARef1 = new Vector();
        bondARef2 = new Vector();
        order = new Vector();
        bondStereo = new Vector();
        bondCounter = 0;
        atomDictRefs = new Vector();
        bondDictRefs = new Vector();

        atomElements.put("id", elid);
        atomElements.put("elementType", elsym);
        atomElements.put("x2", x2);
        atomElements.put("y2", y2);
        atomElements.put("x3", x3);
        atomElements.put("y3", y3);
        atomElements.put("z3", z3);
        atomElements.put("formalCharge", formalCharges);
        atomElements.put("isotopes", isotopes);
        atomElements.put("hydrogenCount", hCounts);

        //descriptors
        scalars = new Vector();
        strings = new Vector();
        arrays = new Vector();
        matrices = new Vector();

        // parser
        scalar = new ScalarCML(scalars);
        array = new ArrayCML(arrays);
        matrix = new MatrixCML(matrices);

        // no molecule name
        moleculeName = null;
    }

    protected void notify(String message, String systemId, int line, int column)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Message=" + message + " systemID=" + systemId +
                " line=" + line + " column=" + column);
        }
    }

    protected void storeData()
    {
        int atomcount = elid.size();

        //        if (logger.isDebugEnabled())
        //        {
        //            logger.debug("No atom ids: " + atomcount);
        //        }
        boolean has3D = false;
        boolean has2D = false;
        boolean hasFormalCharge = false;
        boolean hasPartialCharge = false;
        boolean hasHCounts = false;
        boolean hasSymbols = false;
        boolean hasIsotopes = false;

        if (elsym.size() == atomcount)
        {
            hasSymbols = true;
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("No atom symbols: " + elsym.size() + " != " +
                    atomcount);
            }
        }

        if ((x3.size() == atomcount) && (y3.size() == atomcount) &&
                (z3.size() == atomcount))
        {
            has3D = true;
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                for (int i = 0; i < x3.size(); i++)
                {
                    System.out.println("x3(" + i + ")=" + x3.get(i));
                }

                for (int i = 0; i < y3.size(); i++)
                {
                    System.out.println("y3(" + i + ")=" + y3.get(i));
                }

                for (int i = 0; i < z3.size(); i++)
                {
                    System.out.println("z3(" + i + ")=" + z3.get(i));
                }

                logger.debug("No 3D info: " + x3.size() + " " + y3.size() +
                    " " + z3.size() + " != " + atomcount);
            }
        }

        if ((x2.size() == atomcount) && (y2.size() == atomcount))
        {
            has2D = true;
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("No 2D info: " + x2.size() + " " + y2.size() +
                    " != " + atomcount);
            }
        }

        if (formalCharges.size() == atomcount)
        {
            hasFormalCharge = true;
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("No formal Charge info: " + formalCharges.size() +
                    " != " + atomcount);
            }
        }

        if (isotopes.size() == atomcount)
        {
            hasIsotopes = true;
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("No formal Charge info: " + isotopes.size() +
                    " != " + atomcount);
            }
        }

        if (partialCharges.size() == atomcount)
        {
            hasPartialCharge = true;
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("No partial Charge info: " +
                    partialCharges.size() + " != " + atomcount);
            }
        }

        if (hCounts.size() == atomcount)
        {
            hasHCounts = true;
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("No hydrogen Count info: " + hCounts.size() +
                    " != " + atomcount);
            }
        }

        for (int i = 0; i < atomcount; i++)
        {
            //logger.info("Storing atom: " + i+", '"+elsym.elementAt(i)+"'");
            cdo.startObject("Atom");
            cdo.setObjectProperty("Atom", "id", (String) elid.elementAt(i));

            // store optional atom properties
            if (hasSymbols)
            {
                cdo.setObjectProperty("Atom", "type",
                    (String) elsym.elementAt(i));
            }

            if (has3D)
            {
                cdo.setObjectProperty("Atom", "x3", (String) x3.elementAt(i));
                cdo.setObjectProperty("Atom", "y3", (String) y3.elementAt(i));
                cdo.setObjectProperty("Atom", "z3", (String) z3.elementAt(i));
            }

            if (hasFormalCharge)
            {
                //System.out.println("formalCharge="+ (String) formalCharges.elementAt(i));
                cdo.setObjectProperty("Atom", "formalCharge",
                    (String) formalCharges.elementAt(i));
            }

            if (hasIsotopes)
            {
                //System.out.println("isotope="+ (String) isotopes.elementAt(i));
                cdo.setObjectProperty("Atom", "isotope",
                    (String) isotopes.elementAt(i));
            }

            if (hasPartialCharge)
            {
                //logger.debug("Storing partial atomic charge...");
                cdo.setObjectProperty("Atom", "partialCharge",
                    (String) partialCharges.elementAt(i));
            }

            if (hasHCounts)
            {
                cdo.setObjectProperty("Atom", "hydrogenCount",
                    (String) hCounts.elementAt(i));
            }

            if (has2D)
            {
                if (x2.elementAt(i) != null)
                {
                    cdo.setObjectProperty("Atom", "x2", (String) x2.elementAt(i));
                }

                if (y2.elementAt(i) != null)
                {
                    cdo.setObjectProperty("Atom", "y2", (String) y2.elementAt(i));
                }
            }

            cdo.endObject("Atom");
        }

        int bondcount = order.size();

        if (logger.isDebugEnabled())
        {
            logger.debug(
            //System.out.println(
            "Testing a1,a2,stereo, order: " + bondARef1.size() + "," +
                bondARef2.size() + "," + bondStereo.size() + "," +
                order.size());
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("bondcount:" + bondcount + " bondARef1:" +
                bondARef1.size() + " bondARef2:" + bondARef2.size());
        }

        if ((bondARef1.size() == bondcount) && (bondARef2.size() == bondcount))
        {
            logger.debug("About to add bond info to " +
                cdo.getClass().getName());

            Enumeration orders = order.elements();
            Enumeration bar1s = bondARef1.elements();
            Enumeration bar2s = bondARef2.elements();
            Enumeration stereos = bondStereo.elements();

            while (orders.hasMoreElements())
            {
                cdo.startObject("Bond");
                cdo.setObjectProperty("Bond", "atom1",
                    Integer.toString(elid.indexOf((String) bar1s.nextElement())));
                cdo.setObjectProperty("Bond", "atom2",
                    Integer.toString(elid.indexOf((String) bar2s.nextElement())));

                String bondOrder = (String) orders.nextElement();

                if ("S".equals(bondOrder))
                {
                    cdo.setObjectProperty("Bond", "order", "1");
                }
                else if ("D".equals(bondOrder))
                {
                    cdo.setObjectProperty("Bond", "order", "2");
                }
                else if ("T".equals(bondOrder))
                {
                    cdo.setObjectProperty("Bond", "order", "3");
                }
                else if ("A".equals(bondOrder))
                {
                    cdo.setObjectProperty("Bond", "order", "1.5");
                }
                else
                {
                    cdo.setObjectProperty("Bond", "order", bondOrder);
                }

                if (stereos.hasMoreElements())
                {
                    cdo.setObjectProperty("Bond", "stereo",
                        (String) stereos.nextElement());
                }

                cdo.endObject("Bond");
            }
        }
        else
        {
            logger.error("Wrong atom references: bondcount:" + bondcount +
                " bondARef1:" + bondARef1.size() + " bondARef2:" +
                bondARef2.size());
        }

        // set molecule name/title if available
        if (moleculeName != null)
        {
            cdo.setObjectProperty("Molecule", "title", moleculeName);
        }

        int size;
        StringString ss;
        StringObject so;

        size = scalars.size();

        //System.out.println("Number of IntArrays: " + size);
        if (logger.isDebugEnabled())
        {
            logger.debug("Number of scalars: " + size);
        }

        for (int i = 0; i < size; i++)
        {
            so = (StringObject) scalars.get(i);
            cdo.setObjectProperty("scalar", so.s, so.o);
        }

        size = strings.size();

        if (logger.isDebugEnabled())
        {
            logger.debug("Number of strings: " + size);
        }

        for (int i = 0; i < size; i++)
        {
            //System.out.println(strings.get(i).getClass().getName());
            ss = (StringString) strings.get(i);

            //System.out.println(ss.s2.getClass().getName());
            cdo.setObjectProperty("String", ss.s1, ss.s2);
        }

        size = arrays.size();

        if (logger.isDebugEnabled())
        {
            logger.debug("Number of arrays: " + size);
        }

        for (int i = 0; i < size; i++)
        {
            so = (StringObject) arrays.get(i);
            cdo.setObjectProperty("array", so.s, so.o);
        }

        size = matrices.size();

        //System.out.println("Number of FloatMatrices: " + size);
        if (logger.isDebugEnabled())
        {
            logger.debug("Number of matrices: " + size);
        }

        for (int i = 0; i < size; i++)
        {
            //System.out.println(floatMatrices.get(i).getClass().getName());
            so = (StringObject) matrices.get(i);
            cdo.setObjectProperty("matrix", so.s, so.o);
        }
    }

    private int addArrayElementsTo(Vector toAddto, String array)
    {
        StringTokenizer tokenizer = new StringTokenizer(array);
        int i = 0;

        while (tokenizer.hasMoreElements())
        {
            toAddto.addElement(tokenizer.nextToken());
            i++;
        }

        return i;
    }

    private void initialize()
    {
        elements.put("string", new Integer(STRING));
        elements.put("link", new Integer(LINK));
        elements.put("float", new Integer(FLOAT));
        elements.put("integer", new Integer(INTEGER));
        elements.put("stringArray", new Integer(STRINGARRAY));
        elements.put("floatArray", new Integer(FLOATARRAY));
        elements.put("integerArray", new Integer(INTEGERARRAY));
        elements.put("floatMatrix", new Integer(FLOATMATRIX));
        elements.put("coordinate2", new Integer(COORDINATE2));
        elements.put("coordinate3", new Integer(COORDINATE3));
        elements.put("angle", new Integer(ANGLE));
        elements.put("torsion", new Integer(TORSION));
        elements.put("list", new Integer(LIST));
        elements.put("molecule", new Integer(MOLECULE));
        elements.put("atom", new Integer(ATOM));
        elements.put("atomArray", new Integer(ATOMARRAY));
        elements.put("bond", new Integer(BOND));
        elements.put("bondArray", new Integer(BONDARRAY));
        elements.put("electron", new Integer(ELECTRON));
        elements.put("reaction", new Integer(REACTION));
        elements.put("crystal", new Integer(CRYSTAL));
        elements.put("sequence", new Integer(SEQUENCE));
        elements.put("feature", new Integer(FEATURE));
        elements.put("matrix", new Integer(MATRIX));
        elements.put("array", new Integer(ARRAY));
        elements.put("scalar", new Integer(SCALAR));
        elements.put("bondStereo", new Integer(BOND_STEREO));
        elements.put("name", new Integer(NAME));
        elements.put("symmetry", new Integer(SYMMETRY));
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
