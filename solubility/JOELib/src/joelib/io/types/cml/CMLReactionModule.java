///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: CMLReactionModule.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu,
//			egonw@sci.kun.nl, wegnerj@informatik.uni-tuebingen.de
//Version:  $Revision: 1.9 $
//			$Date: 2004/04/13 14:09:24 $
//			$Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public License
//as published by the Free Software Foundation; either version 2.1
//of the License, or (at your option) any later version.
//All we ask is that proper credit is given for our work, which includes
//- but is not limited to - adding the above copyright notice to the beginning
//of your source code files, and to any copyright notice that you may distribute
//with programs based on this work.
//  
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Lesser General Public License for more details.
//  
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.util.*;

import org.apache.log4j.*;

import org.xml.sax.*;


/**
 * Chemical Markup Language (CML) reaction module.
 * @author egonw
 * @license LGPL
 * @cvsversion    $Revision: 1.9 $, $Date: 2004/04/13 14:09:24 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public class CMLReactionModule extends CMLCoreModule
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.CMLReactionModule");

    //~ Instance fields ////////////////////////////////////////////////////////

    private CMLStack conventionStack = new CMLStack();
    private CMLStack xpath = new CMLStack();

    //~ Constructors ///////////////////////////////////////////////////////////

    public CMLReactionModule(CDOInterface cdo)
    {
        super(cdo);
    }

    public CMLReactionModule(ModuleInterface conv)
    {
        super(conv);

        if (logger.isDebugEnabled())
        {
            logger.debug("New CML-Reaction Module!");
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void characterData(char[] ch, int start, int length)
    {
        //String s = new String(ch, start, length).trim();
        super.characterData(xpath, ch, start, length);
    }

    public void endDocument()
    {
        super.endDocument();
    }

    public void endElement(String uri, String local, String raw)
    {
        if ("reaction".equals(local))
        {
            cdo.endObject("Reaction");
        }
        else if ("reactionList".equals(local))
        {
            cdo.endObject("SetOfReactions");
        }
        else if ("reactant".equals(local))
        {
            cdo.endObject("Reactant");
        }
        else if ("product".equals(local))
        {
            cdo.endObject("Product");
        }
        else if ("molecule".equals(local))
        {
            logger.debug("Storing Molecule");
            super.storeData();

            // do nothing else but store atom/bond information
        }
        else
        {
            super.endElement(xpath, uri, local, raw);
        }
    }

    public CDOInterface returnCDO()
    {
        return this.cdo;
    }

    public void startDocument()
    {
        super.startDocument();
    }

    public void startElement(String uri, String local, String raw,
        Attributes atts)
    {
        if ("reaction".equals(local))
        {
            cdo.startObject("Reaction");
        }
        else if ("reactionList".equals(local))
        {
            cdo.startObject("SetOfReactions");
        }
        else if ("reactant".equals(local))
        {
            cdo.startObject("Reactant");
        }
        else if ("product".equals(local))
        {
            cdo.startObject("Product");
        }
        else if ("molecule".equals(local))
        {
            // do nothing for now
            super.newMolecule();
        }
        else
        {
            super.startElement(xpath, uri, local, raw, atts);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
