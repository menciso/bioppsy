///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: CMLMoleculeWriterBase.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu,
//		  egonw@sci.kun.nl, wegnerj@informatik.uni-tuebingen.de
//Version:  $Revision: 1.8 $
//		  $Date: 2004/07/25 20:43:21 $
//		  $Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public License
//as published by the Free Software Foundation; either version 2.1
//of the License, or (at your option) any later version.
//All we ask is that proper credit is given for our work, which includes
//- but is not limited to - adding the above copyright notice to the beginning
//of your source code files, and to any copyright notice that you may distribute
//with programs based on this work.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.io.PrintStream;

import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEElementTable;
import joelib.data.JOEGenericData;
import joelib.data.JOEKernel;
import joelib.data.JOEPairData;

import joelib.desc.result.AtomDoubleResult;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.IsomerismDetection;
import joelib.util.types.StringInt;


/**
 * Basic CML molecule  writer.
 *
 * @author     egonw
 * @author     wegnerj
 * @license LGPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/07/25 20:43:21 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public abstract class CMLMoleculeWriterBase implements CMLMoleculeWriter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.CMLMoleculeWriterBase");
    public static final String VERSION = JOEKernel.transformCVStag(
            "$Revision: 1.8 $");

    //~ Instance fields ////////////////////////////////////////////////////////

    protected CMLWriterProperties writerProp = null;
    protected PrintStream output;
    private int openTags;

    //~ Constructors ///////////////////////////////////////////////////////////

    public CMLMoleculeWriterBase(PrintStream _output,
        CMLWriterProperties _writerProp)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initializing " + this.getClass().getName());
        }

        writerProp = _writerProp;
        output = _output;
        openTags = 0;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public static void write(PrintStream _output, double[] da)
    {
        for (int i = 0; i < da.length; i++)
        {
            write(_output, Double.toString(da[i]));

            if (i < (da.length - 1))
            {
                write(_output, " ");
            }
        }
    }

    public static void write(PrintStream _output, String s)
    {
        _output.print(s);
    }

    public static void writeCloseTag(PrintStream _output,
        CMLWriterProperties _writerProp, String name)
    {
        write(_output, "</");
        writeElementName(_output, _writerProp, name);
        write(_output, ">\n");

        //openTags--;
    }

    public static void writeElementName(PrintStream _output,
        CMLWriterProperties _writerProp, String name)
    {
        if (_writerProp.useNamespace())
        {
            write(_output, _writerProp.getNamespace() + ":");
        }

        write(_output, name);
    }

    public static void writeEmptyElement(PrintStream _output,
        CMLWriterProperties _writerProp, String name, Hashtable atts)
    {
        write(_output, "<");
        writeElementName(_output, _writerProp, name);
        writeOpenTagAtts(_output, atts);
        write(_output, "/>\n");
    }

    /**
     * Description of the Method
     *
     * @param mol  Description of the Parameter
     */
    public void writeMolecule(JOEMol mol, boolean writePairData,
        Vector attribs2write)
    {
        Hashtable atomIDs = new Hashtable();
        Hashtable bondIDs = new Hashtable();

        // create CML atom and bond ids
        //if (cmlIds)
        //{
        String molID = CMLIDCreator.createMoleculeID(mol);
        CMLIDCreator.createAtomAndBondIDs(mol, molID, atomIDs, bondIDs);

        //System.out.println(atomIDs);
        //}
        Hashtable attributes = new Hashtable();

        //		if (mol.getID() != null && mol.getID().length() != 0)
        //		{
        attributes.put("id", molID);

        //		}
        if (mol.getTitle() != null)
        {
            attributes.put("title",
                XMLSpecialCharacter.convertPlain2XML(mol.getTitle()));
        }

        if (this.writerProp.useNamespace())
        {
            attributes.put("xmlns:jk", "http://joelib.sf.net/joelib/kernel/dict");
        }

        // JOELib kernel specification
        attributes.put("xmlns:cml", this.writerProp.getXMLDeclaration());

        writeOpenTag(output, writerProp, "molecule", attributes);

        writeMetaInformations(mol);

        if (writerProp.storeChemistryKernelInfo())
        {
            writeChemistryKernel(mol);
        }

        boolean has3D = false;
        boolean has2D = false;
        AtomDoubleResult adrX = null;
        AtomDoubleResult adrY = null;

        if (mol.has2D())
        {
            has2D = true;
        }

        if (mol.hasData(CMLPropertyWriter.COORDINATES_2D_X) &&
                mol.hasData(CMLPropertyWriter.COORDINATES_2D_Y))
        {
            JOEGenericData genericData = mol.getData(CMLPropertyWriter.COORDINATES_2D_X,
                    true);
            JOEPairData pairData;

            if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
            {
                pairData = (JOEPairData) genericData;

                if (pairData.getValue() instanceof AtomDoubleResult)
                {
                    adrX = (AtomDoubleResult) pairData.getValue();
                    genericData = mol.getData(CMLPropertyWriter.COORDINATES_2D_Y,
                            true);

                    if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                    {
                        pairData = (JOEPairData) genericData;

                        if (pairData.getValue() instanceof AtomDoubleResult)
                        {
                            adrY = (AtomDoubleResult) pairData.getValue();
                            has2D = true;
                        }
                    }
                }
            }
        }

        if (mol.has3D())
        {
            has3D = true;
        }

        // assume 3D coordinates, if both are not available !;-)
        // Any coordinates must be stored
        if (!has3D && !has2D)
        {
            has3D = true;
        }

        writeAtoms(mol, molID, has3D, has2D, adrX, adrY, atomIDs);
        writeBonds(mol, molID, atomIDs, bondIDs);

        // write molecule title
        if (mol.getTitle() != null)
        {
            attributes.clear();
            attributes.put("convention", "trivial");
            writeOpenTag(output, writerProp, "name", attributes, false);
            write(output, XMLSpecialCharacter.convertPlain2XML(mol.getTitle()));
            writeCloseTag(output, writerProp, "name");
        }

        // write symmetry informations
        if (writerProp.writeSymmetryInformations())
        {
            CMLSymmetryWriter.writeSymmetry(output, mol);
        }

        // write descriptor values
        CMLPropertyWriter.writeProperties(writerProp, output, mol,
            writePairData, attribs2write);

        writeCloseTag(output, writerProp, "molecule");
    }

    public static void writeOpenTag(PrintStream _output,
        CMLWriterProperties _writerProp, String name)
    {
        writeOpenTag(_output, _writerProp, name, null, true);
    }

    public static void writeOpenTag(PrintStream _output,
        CMLWriterProperties _writerProp, String name, boolean writeEOL)
    {
        writeOpenTag(_output, _writerProp, name, null, writeEOL);
    }

    public static void writeOpenTag(PrintStream _output,
        CMLWriterProperties _writerProp, String name, Hashtable atts)
    {
        writeOpenTag(_output, _writerProp, name, atts, true);
    }

    public static void writeOpenTag(PrintStream _output,
        CMLWriterProperties _writerProp, String name, Hashtable atts,
        boolean writeEOL)
    {
        write(_output, "<");
        writeElementName(_output, _writerProp, name);
        writeOpenTagAtts(_output, atts);
        write(_output, ">");

        if (writeEOL)
        {
            write(_output, "\n");
        }

        //openTags++;
    }

    public static void writeOpenTagAtts(PrintStream _output, Hashtable atts)
    {
        if (atts != null)
        {
            Enumeration keys = atts.keys();

            while (keys.hasMoreElements())
            {
                String key = (String) keys.nextElement();
                write(_output, " " + key + "=\"");
                write(_output, atts.get(key).toString());
                write(_output, "\"");
            }
        }
    }

    /**
     * Description of the Method
     *
     * @param atom  Description of the Parameter
     */
    protected synchronized void writeAtom(JOEAtom atom, StringInt atomID,
        boolean write2D, boolean write3D, AtomDoubleResult x, AtomDoubleResult y)
    {
        Hashtable attributes = new Hashtable();

        attributes.put("id", atomID.s);
        writeOpenTag(output, writerProp, "atom", attributes);

        attributes.clear();
        attributes.put("builtin", "elementType");
        writeOpenTag(output, writerProp, "string", attributes, false);
        write(output, JOEElementTable.instance().getSymbol(atom.getAtomicNum()));
        writeCloseTag(output, writerProp, "string");

        //        write(atom.getPoint2D());
        if (write3D)
        {
            write3D(atom.getVector());
        }

        if (write2D)
        {
            write2D(atom.getVector(), x, y, atom.getIdx());
        }

        if (writerProp.forceWriteFormalCharge() ||
                (atom.getFormalCharge() != 0))
        {
            attributes.clear();
            attributes.put("builtin", "formalCharge");
            writeOpenTag(output, writerProp, "integer", attributes, false);
            write(output, Integer.toString(atom.getFormalCharge()));
            writeCloseTag(output, writerProp, "integer");
        }

        if (writerProp.writePartialCharge())
        {
            if (writerProp.getCMLversion() == 1.0)
            {
                //CML1
                attributes.clear();
                attributes.put("builtin", "partialCharge");
                writeOpenTag(output, writerProp, "float", attributes, false);
                write(output, Double.toString(atom.getPartialCharge()));
                writeCloseTag(output, writerProp, "float");
            }
            else
            {
                // CML2
                attributes.clear();
                attributes.put("dataType", "xsd:float");
                attributes.put("dictRef",
                    atom.getParent().getPartialChargeVendor());
                attributes.put("units", "units:electron");
                writeOpenTag(output, writerProp, "scalar", attributes, false);
                write(output, Double.toString(atom.getPartialCharge()));
                writeCloseTag(output, writerProp, "scalar");
            }
        }

        if (writerProp.writeImpliciteHydrogens())
        {
            attributes.clear();
            attributes.put("builtin", "hydrogenCount");
            writeOpenTag(output, writerProp, "integer", attributes, false);
            write(output,
                Integer.toString(atom.getImplicitValence() +
                    atom.explicitHydrogenCount()));
            writeCloseTag(output, writerProp, "integer");
        }

        if (atom.getIsotope() != 0)
        {
            attributes.clear();
            attributes.put("builtin", "isotope");
            writeOpenTag(output, writerProp, "integer", attributes, false);
            write(output, Integer.toString(atom.getIsotope()));
            writeCloseTag(output, writerProp, "integer");
        }

        writeCloseTag(output, writerProp, "atom");
    }

    protected abstract void writeAtoms(JOEMol mol, String molID, boolean has3D,
        boolean has2D, AtomDoubleResult adrX, AtomDoubleResult adrY,
        Hashtable atomIDs);

    protected abstract void writeBonds(JOEMol mol, String molID,
        Hashtable atomIDs, Hashtable bondIDs);

    protected synchronized void write2D(XYZVector p, AtomDoubleResult x,
        AtomDoubleResult y, int index)
    {
        if ((x != null) && (y != null))
        {
            Hashtable attributes = new Hashtable();
            attributes.clear();
            attributes.put("builtin", "x2");
            writeOpenTag(output, writerProp, "float", attributes, false);
            write(output, x.getStringValue(index));
            writeCloseTag(output, writerProp, "float");

            attributes.clear();
            attributes.put("builtin", "y2");
            writeOpenTag(output, writerProp, "float", attributes, false);
            write(output, y.getStringValue(index));
            writeCloseTag(output, writerProp, "float");
        }
        else if (p != null)
        {
            Hashtable attributes = new Hashtable();
            attributes.clear();
            attributes.put("builtin", "x2");
            writeOpenTag(output, writerProp, "float", attributes, false);
            write(output, Double.toString(p.x()));
            writeCloseTag(output, writerProp, "float");

            attributes.clear();
            attributes.put("builtin", "y2");
            writeOpenTag(output, writerProp, "float", attributes, false);
            write(output, Double.toString(p.y()));
            writeCloseTag(output, writerProp, "float");
        }
    }

    /**
     * Description of the Method
     *
     * @param p  Description of the Parameter
     */
    protected synchronized void write3D(XYZVector p)
    {
        if (p != null)
        {
            Hashtable attributes = new Hashtable();
            attributes.clear();
            attributes.put("builtin", "x3");
            writeOpenTag(output, writerProp, "float", attributes, false);
            write(output, Double.toString(p.x()));
            writeCloseTag(output, writerProp, "float");

            attributes.clear();
            attributes.put("builtin", "y3");
            writeOpenTag(output, writerProp, "float", attributes, false);
            write(output, Double.toString(p.y()));
            writeCloseTag(output, writerProp, "float");

            attributes.clear();
            attributes.put("builtin", "z3");
            writeOpenTag(output, writerProp, "float", attributes, false);
            write(output, Double.toString(p.z()));
            writeCloseTag(output, writerProp, "float");
        }
    }

    protected void writeBondOrder(JOEBond bond)
    {
        Hashtable attributes = new Hashtable();
        attributes.put("builtin", "order");
        writeOpenTag(output, writerProp, "string", attributes, false);

        // is aromatic bond ?
        if (bond.getBondOrder() == JOEBond.JOE_AROMATIC_BOND_ORDER)
        {
            write(output, "1.5");
        }
        else
        {
            write(output, Integer.toString(bond.getBO()));
        }

        writeCloseTag(output, writerProp, "string");
    }

    protected void writeBondStereo(JOEBond bond)
    {
        // check stereochemistry: up/down and cis/trans
        int isomerism = IsomerismDetection.isCisTransBond(bond);

        if ((isomerism != IsomerismDetection.EZ_ISOMERISM_UNDEFINED) ||
                ((bond.getFlags() & JOEBond.JOE_WEDGE_BOND) != 0) ||
                ((bond.getFlags() & JOEBond.JOE_HASH_BOND) != 0))
        {
            Hashtable attributes = new Hashtable();

            if (writerProp.getCMLversion() == 1.0)
            {
                attributes.put("builtin", "stereo");
                writeOpenTag(output, writerProp, "string", attributes, false);
            }
            else
            {
                attributes.put("dataType", "xsd:string");
                attributes.put("dictRef", "mdl:stereo");
                writeOpenTag(output, writerProp, "scalar", attributes, false);
            }

            if (bond.isWedge())
            {
                // wedge bond
                write(output, "W");
            }
            else if (bond.isHash())
            {
                // hatch bond
                write(output, "H");
            }
            else if (isomerism != IsomerismDetection.EZ_ISOMERISM_UNDEFINED)
            {
                if (isomerism == IsomerismDetection.Z_ISOMERISM)
                {
                    // cis bond
                    write(output, "C");
                }
                else if (isomerism == IsomerismDetection.E_ISOMERISM)
                {
                    // trans bond
                    write(output, "T");
                }
            }

            if (writerProp.getCMLversion() == 1.0)
            {
                writeCloseTag(output, writerProp, "string");
            }
            else
            {
                writeCloseTag(output, writerProp, "scalar");
            }
        }
    }

    /**
     * @param mol
     */
    protected void writeMetaInformations(JOEMol mol)
    {
        Hashtable attributes = new Hashtable();
        attributes.clear();
        attributes.put("title", "generated automatically from JOELib");
        writeOpenTag(output, writerProp, "metadataList", attributes);

        attributes.clear();
        attributes.put("name", "dc:creator");
        attributes.put("content",
            "Used JOELib chemistry kernel (expert systems) ID is " +
            JOEKernel.instance().getKernelHash() +
            " and the used CML writer is " + this.getClass().getName() +
            "(version " + VERSION + ")");
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");

        attributes.clear();
        attributes.put("name", "dc:description");
        attributes.put("content", "Conversion of legacy filetype to CML");
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");

        attributes.clear();
        attributes.put("name", "dc:identifier");
        attributes.put("content", "unknown");
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");

        attributes.clear();
        attributes.put("name", "dc:content");
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");

        attributes.clear();
        attributes.put("name", "dc:rights");
        attributes.put("content", "unknown");
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");

        attributes.clear();
        attributes.put("name", "dc:type");
        attributes.put("content", "chemistry");
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");
        attributes.clear();
        attributes.put("name", "dc:contributor");
        attributes.put("content",
            "see http://joelib.sf.net for a full list of contributors");
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");

        attributes.clear();
        attributes.put("name", "dc:date");
        attributes.put("content", (new Date()).toGMTString());
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");

        attributes.clear();
        attributes.put("name", "cmlm:structure");
        attributes.put("content", "yes");
        writeOpenTag(output, writerProp, "metadata", attributes, false);
        writeCloseTag(output, writerProp, "metadata");

        writeCloseTag(output, writerProp, "metadataList");
    }

    private void writeChemistryKernel(JOEMol mol)
    {
        String[] infos = JOEKernel.instance().getKernelInformations();
        String[] titles = JOEKernel.instance().getKernelTitles();
        Hashtable attributes = new Hashtable();
        int kernelHash = JOEKernel.instance().getKernelHash();
        StringBuffer sb = new StringBuffer();
        String id;
        String title;

        for (int i = 0; i < infos.length; i++)
        {
            attributes.clear();
            title = JOEKernel.CML_KERNEL_REFERENCE_PREFIX + kernelHash + ":" +
                titles[i];

            if (mol.hasData(title))
            {
                //remove old kernels from descriptor data base
                mol.deleteData(title);
            }

            attributes.put("title", title);
            id = JOEKernel.CML_KERNEL_REFERENCE_PREFIX + "s" +
                Integer.toString(infos[i].hashCode());
            sb.append(id);

            if (i < (infos.length - 1))
            {
                sb.append(",");
            }

            attributes.put("id", id);
            attributes.put("dataType", "xsd:string");
            attributes.put("dictRef",
                JOEKernel.CML_KERNEL_REFERENCE_PREFIX +
                Integer.toString(kernelHash));
            writeOpenTag(output, writerProp, "scalar", attributes, false);
            write(output, infos[i]);
            writeCloseTag(output, writerProp, "scalar");
        }

        attributes.clear();
        attributes.put("title",
            JOEKernel.CML_KERNEL_REFERENCE_PREFIX + kernelHash);
        attributes.put("id",
            JOEKernel.CML_KERNEL_REFERENCE_PREFIX +
            Integer.toString(kernelHash));
        attributes.put("dataType", "xsd:string");
        attributes.put("dictRef",
            JOEKernel.CML_KERNEL_REFERENCE_PREFIX +
            Integer.toString(kernelHash));
        writeOpenTag(output, writerProp, "array", attributes, false);
        write(output, sb.toString());
        writeCloseTag(output, writerProp, "array");
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
