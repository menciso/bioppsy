///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeArray.java,v $
//  Purpose:  Reader/Writer for CML files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.24 $
//            $Date: 2004/07/25 20:43:21 $
//            $Author: wegner $
//  Original Author: steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu, egonw@sci.kun.nl
//  Original Version: Chemical Development Kit,  http://sourceforge.net/projects/cdk
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.io.PrintStream;

import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.desc.result.AtomDoubleResult;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.IsomerismDetection;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;
import joelib.util.types.StringInt;


/**
 * CML molecule array writer.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.24 $, $Date: 2004/07/25 20:43:21 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public class MoleculeArray extends CMLMoleculeWriterBase
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.MoleculeArray");

    //~ Constructors ///////////////////////////////////////////////////////////

    public MoleculeArray(PrintStream _output, CMLWriterProperties _writerProp)
    {
        super(_output, _writerProp);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    protected void writeAtoms(JOEMol mol, String molID, boolean has3D,
        boolean has2D, AtomDoubleResult adrX, AtomDoubleResult adrY,
        Hashtable atomIDs)
    {
        int size = 10 * mol.numAtoms();
        StringBuffer aids = new StringBuffer(size);
        StringBuffer elements = new StringBuffer(size);
        StringBuffer x2 = new StringBuffer(size);
        StringBuffer y2 = new StringBuffer(size);
        StringBuffer x3 = new StringBuffer(size);
        StringBuffer y3 = new StringBuffer(size);
        StringBuffer z3 = new StringBuffer(size);
        StringBuffer partCharge = new StringBuffer(size);
        StringBuffer formalCharge = new StringBuffer(size);
        StringBuffer impValence = new StringBuffer(size);
        StringBuffer isotope = new StringBuffer(size);
        boolean hasIsotopes = false;
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        XYZVector xyz;
        int index = 0;
        StringInt atomID;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            index++;
            xyz = atom.getVector();
            atomID = (StringInt) atomIDs.get(molID + ":" + atom.getIdx());
            aids.append(atomID.s);
            elements.append(atom);

            if (has3D)
            {
                x3.append(xyz._vx);
                y3.append(xyz._vy);
                z3.append(xyz._vz);
            }

            if (has2D)
            {
                if ((adrX != null) && (adrY != null))
                {
                    x2.append(adrX.getStringValue(index));
                    y2.append(adrY.getStringValue(index));
                }
                else
                {
                    x2.append(xyz._vx);
                    y2.append(xyz._vy);
                }
            }

            formalCharge.append(atom.getFormalCharge());

            if (writerProp.writePartialCharge())
            {
                partCharge.append(atom.getPartialCharge());
            }

            if (writerProp.writeImpliciteHydrogens())
            {
                impValence.append(atom.getImplicitValence() +
                    atom.explicitHydrogenCount());
            }

            if (atom.getIsotope() != 0)
            {
                hasIsotopes = true;
            }

            isotope.append(atom.getIsotope());

            if (ait.hasNext())
            {
                aids.append(' ');
                elements.append(' ');

                if (has3D)
                {
                    x3.append(' ');
                    y3.append(' ');
                    z3.append(' ');
                }

                if (has2D)
                {
                    x2.append(' ');
                    y2.append(' ');
                }

                formalCharge.append(' ');

                if (writerProp.writePartialCharge())
                {
                    partCharge.append(' ');
                }

                if (writerProp.writeImpliciteHydrogens())
                {
                    impValence.append(' ');
                }

                isotope.append(' ');
            }
        }

        writeOpenTag(output, writerProp, "atomArray");

        Hashtable attributes = new Hashtable();
        attributes.put("builtin", "id");
        writeOpenTag(output, writerProp, "stringArray", attributes, false);
        write(output, aids.toString());
        writeCloseTag(output, writerProp, "stringArray");

        attributes.clear();
        attributes.put("builtin", "elementType");
        writeOpenTag(output, writerProp, "stringArray", attributes, false);
        write(output, elements.toString());
        writeCloseTag(output, writerProp, "stringArray");

        if (has3D)
        {
            attributes.clear();
            attributes.put("builtin", "x3");
            writeOpenTag(output, writerProp, "floatArray", attributes, false);
            write(output, x3.toString());
            writeCloseTag(output, writerProp, "floatArray");

            attributes.clear();
            attributes.put("builtin", "y3");
            writeOpenTag(output, writerProp, "floatArray", attributes, false);
            write(output, y3.toString());
            writeCloseTag(output, writerProp, "floatArray");

            attributes.clear();
            attributes.put("builtin", "z3");
            writeOpenTag(output, writerProp, "floatArray", attributes, false);
            write(output, z3.toString());
            writeCloseTag(output, writerProp, "floatArray");
        }

        if (has2D)
        {
            attributes.clear();
            attributes.put("builtin", "x2");
            writeOpenTag(output, writerProp, "floatArray", attributes, false);
            write(output, x2.toString());
            writeCloseTag(output, writerProp, "floatArray");

            attributes.clear();
            attributes.put("builtin", "y2");
            writeOpenTag(output, writerProp, "floatArray", attributes, false);
            write(output, y2.toString());
            writeCloseTag(output, writerProp, "floatArray");
        }

        attributes.clear();
        attributes.put("builtin", "formalCharge");
        writeOpenTag(output, writerProp, "floatArray", attributes, false);
        write(output, formalCharge.toString());
        writeCloseTag(output, writerProp, "floatArray");

        if (writerProp.writePartialCharge())
        {
            //CML1
            attributes.clear();
            attributes.put("builtin", "partialCharge");
            writeOpenTag(output, writerProp, "floatArray", attributes, false);
            write(output, partCharge.toString());
            writeCloseTag(output, writerProp, "floatArray");

            // CML2
            //<array dataType="xsd:float" dictRef="joelib:partialCharge" units="units:electron">.234 1,456 4.678</scalar>
        }

        if (writerProp.writeImpliciteHydrogens())
        {
            attributes.clear();
            attributes.put("builtin", "hydrogenCount");
            writeOpenTag(output, writerProp, "integerArray", attributes, false);
            write(output, impValence.toString());
            writeCloseTag(output, writerProp, "integerArray");
        }

        if (hasIsotopes)
        {
            attributes.clear();
            attributes.put("builtin", "isotope");
            writeOpenTag(output, writerProp, "integerArray", attributes, false);
            write(output, isotope.toString());
            writeCloseTag(output, writerProp, "integerArray");
        }

        writeCloseTag(output, writerProp, "atomArray");
    }

    protected void writeBonds(JOEMol mol, String molID, Hashtable atomIDs,
        Hashtable bondIDs)
    {
        writeOpenTag(output, writerProp, "bondArray");

        StringBuffer orders = new StringBuffer(10 * mol.numBonds());
        StringBuffer begins = new StringBuffer(10 * mol.numBonds());
        StringBuffer ends = new StringBuffer(10 * mol.numBonds());
        StringBuffer bondStereo = new StringBuffer(10 * mol.numBonds());
        BondIterator bit = mol.bondIterator();
        JOEBond bond;

        JOEAtom beginAtom;
        JOEAtom endAtom;
        StringInt beginAtomID;
        StringInt endAtomID;
        boolean hasStereo = false;

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            beginAtom = bond.getBeginAtom();
            endAtom = bond.getEndAtom();

            beginAtomID = (StringInt) atomIDs.get(molID + ":" +
                    beginAtom.getIdx());
            endAtomID = (StringInt) atomIDs.get(molID + ":" + endAtom.getIdx());

            begins.append(beginAtomID.s);
            ends.append(endAtomID.s);

            // is aromatic bond ?
            if (bond.getBondOrder() == 4)
            {
                orders.append("1.5");
            }
            else
            {
                orders.append(bond.getBondOrder());
            }

            int isomerism = IsomerismDetection.isCisTransBond(bond);

            if ((isomerism != IsomerismDetection.EZ_ISOMERISM_UNDEFINED) ||
                    ((bond.getFlags() & JOEBond.JOE_WEDGE_BOND) != 0) ||
                    ((bond.getFlags() & JOEBond.JOE_HASH_BOND) != 0))
            {
                hasStereo = true;
            }

            if (bond.isWedge())
            {
                // wedge bond
                bondStereo.append("W");
            }
            else if (bond.isHash())
            {
                // hatch bond
                bondStereo.append("H");
            }
            else if (isomerism != IsomerismDetection.EZ_ISOMERISM_UNDEFINED)
            {
                if (isomerism == IsomerismDetection.Z_ISOMERISM)
                {
                    // cis bond
                    bondStereo.append("C");
                }
                else if (isomerism == IsomerismDetection.E_ISOMERISM)
                {
                    // trans bond
                    bondStereo.append("T");
                }
            }
            else
            {
                bondStereo.append("none");
            }

            if (bit.hasNext())
            {
                begins.append(' ');
                ends.append(' ');
                orders.append(' ');
                bondStereo.append(' ');
            }
        }

        Hashtable attributes = new Hashtable();
        attributes.put("builtin", "atomRefs");
        writeOpenTag(output, writerProp, "stringArray", attributes, false);
        write(output, begins.toString());
        writeCloseTag(output, writerProp, "stringArray");

        attributes.clear();
        attributes.put("builtin", "atomRefs");
        writeOpenTag(output, writerProp, "stringArray", attributes, false);
        write(output, ends.toString());
        writeCloseTag(output, writerProp, "stringArray");

        attributes.clear();
        attributes.put("builtin", "order");
        writeOpenTag(output, writerProp, "stringArray", attributes, false);
        write(output, orders.toString());
        writeCloseTag(output, writerProp, "stringArray");

        writeCloseTag(output, writerProp, "bondArray");

        if (hasStereo)
        {
            writeOpenTag(output, writerProp, "bondStereo", null, false);
            write(output, bondStereo.toString());
            writeCloseTag(output, writerProp, "bondStereo");
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
