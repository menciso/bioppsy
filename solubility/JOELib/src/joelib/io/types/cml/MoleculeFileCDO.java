///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: MoleculeFileCDO.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu,
//			egonw@sci.kun.nl, wegnerj@informatik.uni-tuebingen.de
//Version:  $Revision: 1.30 $
//			$Date: 2004/07/25 20:43:21 $
//			$Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public License
//as published by the Free Software Foundation; either version 2.1
//of the License, or (at your option) any later version.
//All we ask is that proper credit is given for our work, which includes
//- but is not limited to - adding the above copyright notice to the beginning
//of your source code files, and to any copyright notice that you may distribute
//with programs based on this work.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEDataType;
import joelib.data.JOEElementTable;
import joelib.data.JOEGenericData;
import joelib.data.JOEPairData;

import joelib.desc.result.AtomDoubleResult;
import joelib.desc.result.BitArrayResult;
import joelib.desc.result.BooleanResult;
import joelib.desc.result.DoubleArrayResult;
import joelib.desc.result.DoubleMatrixResult;
import joelib.desc.result.DoubleResult;
import joelib.desc.result.IntArrayResult;
import joelib.desc.result.IntMatrixResult;
import joelib.desc.result.IntResult;
import joelib.desc.result.StringResult;

import joelib.io.IOTypeHolder;
import joelib.io.MoleculeCallback;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;
import joelib.molecule.JOEMolVector;

import joelib.util.IsomerismDetection;
import joelib.util.types.BondInt;


/**
 * CDO object needed as interface with the JCFL library for reading CML
 * encoded data.
 *
 * @author    egonw
 * @author     wegnerj
 * @license LGPL
 * @cvsversion    $Revision: 1.30 $, $Date: 2004/07/25 20:43:21 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public class MoleculeFileCDO implements CDOInterface
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.MoleculeFileCDO");

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable atomEnumeration = new Hashtable();
    private Hashtable strings = new Hashtable(10);
    private Hashtable unparsed = new Hashtable(10);
    private JOEAtom currentAtom;
    private JOEMol currentMolecule;
    private JOEMolVector currentSetOfMolecules;
    private MoleculeCallback moleculeCallback;
    private Vector c2Dx = new Vector();
    private Vector c2Dy = new Vector();
    private Vector c3Dx = new Vector();
    private Vector c3Dy = new Vector();
    private Vector c3Dz = new Vector();

    /**
     * Of type {@link joelib.util.types.BondInt}
         */
    private Vector ezInformations = new Vector(10);
    private double x_2D;
    private double x_3D;
    private double y_2D;
    private double y_3D;
    private double z_3D;
    private int bond_EZ;
    private int bond_a1;
    private int bond_a2;
    private int bond_order;
    private int bond_stereo;
    private int molIndex;
    private int numberOfAtoms;

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Procedure required by the CDOInterface. This methodName is only
     * supposed to be called by the JCFL library
     *
     * @param type   The new documentProperty value
     * @param value  The new documentProperty value
     */
    public void setDocumentProperty(String type, Object value)
    {
    }

    //    public MoleculeFileCDO(JOEMol mol)
    //    {
    //      currentMolecule = mol;
    //    }
    public void setMolecule(JOEMol mol)
    {
        currentMolecule = mol;
    }

    public void setMoleculeCallback(MoleculeCallback _moleculeCallback)
    {
        moleculeCallback = _moleculeCallback;
    }

    public void setMoleculeSetOfMolecules(JOEMolVector _currentSetOfMolecules)
    {
        currentSetOfMolecules = _currentSetOfMolecules;
    }

    /**
     * Procedure required by the CDOInterface. This methodName is only
     * supposed to be called by the JCFL library
     *
     * @param objectType     The new objectProperty value
     * @param propertyType   The new objectProperty value
     * @param propertyValue  The new objectProperty value
     */
    public void setObjectProperty(String objectType, String propertyType,
        Object propertyValue)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("objectType: " + objectType);
            logger.debug("propType: " + propertyType);
            logger.debug("property: " + propertyValue);
        }

        String stringValue = null;

        if (propertyValue instanceof String)
        {
            stringValue = (String) propertyValue;
        }

        if (objectType.equals("Atom"))
        {
            Double dTmp;

            if (propertyType.equals("type"))
            {
                currentAtom.setType(stringValue);
                currentAtom.setAtomicNum(JOEElementTable.instance()
                                                        .getAtomicNum(stringValue));
            }
            else if (propertyType.equals("x2"))
            {
                dTmp = new Double(stringValue);
                x_2D = dTmp.doubleValue();
                c2Dx.add(dTmp);
            }
            else if (propertyType.equals("y2"))
            {
                dTmp = new Double(stringValue);
                y_2D = dTmp.doubleValue();
                c2Dy.add(dTmp);
            }
            else if (propertyType.equals("x3"))
            {
                dTmp = new Double(stringValue);
                x_3D = dTmp.doubleValue();
                c3Dx.add(dTmp);
            }
            else if (propertyType.equals("y3"))
            {
                dTmp = new Double(stringValue);
                y_3D = dTmp.doubleValue();
                c3Dy.add(dTmp);
            }
            else if (propertyType.equals("z3"))
            {
                dTmp = new Double(stringValue);
                z_3D = dTmp.doubleValue();
                c3Dz.add(dTmp);
            }
            else if (propertyType.equals("formalCharge"))
            {
                currentAtom.setFormalCharge(new Integer(stringValue).intValue());
            }
            else if (propertyType.equals("charge"))
            {
                currentAtom.setPartialCharge(new Double(stringValue).doubleValue());
            }
            else if (propertyType.equals("hydrogenCount"))
            {
                currentAtom.setImplicitValence(new Integer(stringValue).intValue());
            }
            else if (propertyType.equals("isotope"))
            {
                //System.out.println("SET ISOTOPE: "+stringValue);
                currentAtom.setIsotope(new Integer(stringValue).intValue());
            }
            else if (propertyType.equals("id"))
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("id" + stringValue);
                }

                atomEnumeration.put(stringValue, new Integer(numberOfAtoms));
            }
        }
        else if (objectType.equals("Bond"))
        {
            //System.out.println(propertyType+"="+stringValue);
            if (propertyType.equals("atom1"))
            {
                bond_a1 = new Integer(stringValue).intValue() + 1;

                //                System.out.println("atom1:"+bond_a1);
            }
            else if (propertyType.equals("atom2"))
            {
                bond_a2 = new Integer(stringValue).intValue() + 1;
            }
            else if (propertyType.equals("order"))
            {
                double BO = 1.0;
                bond_order = (int) BO;

                //bond_order = new Integer(propertyValue).intValue();
                try
                {
                    bond_order = (int) Double.parseDouble(stringValue);
                }
                 catch (Exception e)
                {
                    logger.error("Cannot convert to double: " + stringValue);
                }

                if (BO == 1.5)
                {
                    bond_order = JOEBond.JOE_AROMATIC_BOND_ORDER;
                }

                //System.out.println("CDO BO="+bond_order);
            }
            else if (propertyType.equals("stereo"))
            {
                //System.out.println("Bond: stereo '"+stringValue+"'");
                bond_stereo = 0;
                bond_EZ = IsomerismDetection.EZ_ISOMERISM_UNDEFINED;

                if (stringValue.equalsIgnoreCase("H"))
                {
                    bond_stereo |= JOEBond.JOE_HASH_BOND;
                }
                else if (stringValue.equalsIgnoreCase("W"))
                {
                    bond_stereo |= JOEBond.JOE_WEDGE_BOND;
                }
                else if (stringValue.equalsIgnoreCase("T"))
                {
                    bond_EZ = IsomerismDetection.E_ISOMERISM;
                }
                else if (stringValue.equalsIgnoreCase("C"))
                {
                    bond_EZ = IsomerismDetection.Z_ISOMERISM;
                }
            }
        }
        else if (objectType.equals("scalar"))
        {
            if (propertyValue instanceof DoubleResult)
            {
                DoubleResult dr;

                //System.out.println(propertyType+"="+propertyValue);
                dr = (DoubleResult) propertyValue;

                //			if (ir.value == null)
                //			{
                //				logger.error(
                //					"Double entry " + propertyType + "=" + propertyValue + " was not successfully parsed.");
                //			}
                //			else
                //			{
                JOEPairData dp = new JOEPairData();
                dp.setAttribute(propertyType);
                dp.setValue(dr);
                currentMolecule.addData(dp);

                //			}
            }
            else if (propertyValue instanceof IntResult)
            {
                IntResult ir;

                //System.out.println(propertyType+"="+propertyValue);
                ir = (IntResult) propertyValue;

                //			if (ir.value == null)
                //			{
                //				logger.error(
                //					"Integer entry " + propertyType + "=" + propertyValue + " was not successfully parsed.");
                //			}
                //			else
                //			{
                JOEPairData dp = new JOEPairData();
                dp.setAttribute(propertyType);
                dp.setValue(ir);
                currentMolecule.addData(dp);

                //			}
            }
            else if (propertyValue instanceof StringResult)
            {
                StringResult sr;

                //System.out.println("SR:"+propertyType+"="+propertyValue);
                sr = (StringResult) propertyValue;

                if (propertyType.length() != 0)
                {
                    // use Hashtable to combine the String pieces
                    if (currentMolecule.hasData(propertyType))
                    {
                        JOEGenericData genericData = currentMolecule.getData(propertyType,
                                false);
                        JOEPairData pairData;

                        if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                        {
                            pairData = (JOEPairData) genericData;

                            StringResult previous = (StringResult) pairData.getValue();
                            StringBuffer sb = new StringBuffer(previous.value.length() +
                                    100);
                            sb.append(previous.value);
                            sb.append(sr.value);
                            previous.value = sb.toString();

                            //System.out.println(propertyType+"="+sb.toString());
                        }
                    }
                    else
                    {
                        JOEPairData dp = new JOEPairData();
                        dp.setAttribute(propertyType);
                        dp.setValue(sr);
                        currentMolecule.addData(dp);
                        strings.put(propertyType, "");
                    }
                }
            }
            else if (propertyValue instanceof BooleanResult)
            {
                BooleanResult br;

                //System.out.println(propertyType+"="+propertyValue);
                br = (BooleanResult) propertyValue;

                //			if (ir.value == null)
                //			{
                //				logger.error(
                //					"Boolean entry " + propertyType + "=" + propertyValue + " was not successfully parsed.");
                //			}
                //			else
                //			{
                JOEPairData dp = new JOEPairData();
                dp.setAttribute(propertyType);
                dp.setValue(br);
                currentMolecule.addData(dp);

                //			}
            }
        }
        else if (objectType.equals("String"))
        {
            //System.out.println("propertyType:"+propertyType);
            //System.out.println("stringValue:"+stringValue);
            if (propertyType.length() != 0)
            {
                // use Hashtable to combine the String pieces
                if (currentMolecule.hasData(propertyType))
                {
                    JOEGenericData genericData = currentMolecule.getData(propertyType,
                            false);
                    JOEPairData pairData;

                    if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                    {
                        pairData = (JOEPairData) genericData;

                        StringBuffer sb = (StringBuffer) pairData.getValue();
                        sb.append(stringValue);

                        //System.out.println(propertyType+"="+sb.toString());
                    }
                }
                else
                {
                    JOEPairData dp = new JOEPairData();
                    dp.setAttribute(propertyType);

                    // use StringBuffer instead of String to be more efficient
                    // The StringBuffer will be replaced by String, when
                    // Molecule-End-Tag occurs
                    StringBuffer sb = new StringBuffer(200);
                    sb.append(stringValue);
                    dp.setValue(sb);
                    currentMolecule.addData(dp);
                }

                unparsed.put(propertyType, "");
            }
            else
            {
                logger.error("No title defined for String entry: " +
                    stringValue);
            }
        }
        else if (objectType.equals("array"))
        {
            if (propertyValue instanceof DoubleArrayResult)
            {
                DoubleArrayResult dar;
                dar = (DoubleArrayResult) propertyValue;

                if (dar.getDoubleArray() == null)
                {
                    logger.error("Double array entry " + propertyType + "=" +
                        propertyValue + " was not successfully parsed.");
                }
                else
                {
                    JOEPairData dp = new JOEPairData();
                    dp.setAttribute(propertyType);
                    dp.setValue(dar);
                    currentMolecule.addData(dp);
                }
            }
            else if (propertyValue instanceof IntArrayResult)
            {
                IntArrayResult iar;
                iar = (IntArrayResult) propertyValue;

                if (iar.getIntArray() == null)
                {
                    logger.error("Integer array entry " + propertyType + "=" +
                        propertyValue + " was not successfully parsed.");
                }
                else
                {
                    JOEPairData dp = new JOEPairData();
                    dp.setAttribute(propertyType);
                    dp.setValue(iar);
                    currentMolecule.addData(dp);
                }
            }
            else if (propertyValue instanceof BitArrayResult)
            {
                BitArrayResult bar;
                bar = (BitArrayResult) propertyValue;

                if (bar.value == null)
                {
                    logger.error("Boolean array entry " + propertyType + "=" +
                        propertyValue + " was not successfully parsed.");
                }
                else
                {
                    JOEPairData dp = new JOEPairData();
                    dp.setAttribute(propertyType);
                    dp.setValue(bar);
                    currentMolecule.addData(dp);
                }
            }
        }
        else if (objectType.equals("matrix"))
        {
            if (propertyValue instanceof DoubleMatrixResult)
            {
                DoubleMatrixResult dmr;

                //System.out.println(propertyValue.getClass().getName());
                dmr = (DoubleMatrixResult) propertyValue;

                if (dmr.value == null)
                {
                    logger.error("Float matrix entry " + propertyType + "=" +
                        propertyValue + " was not successfully parsed.");
                }
                else
                {
                    JOEPairData dp = new JOEPairData();
                    dp.setAttribute(propertyType);
                    dp.setValue(dmr);
                    currentMolecule.addData(dp);
                }

                //System.out.println("SDF"+dmr.toString(IOTypeHolder.instance().getIOType("SDF")));
                //System.out.println("CML"+dmr.toString(IOTypeHolder.instance().getIOType("CML")));
            }
            else if (propertyValue instanceof IntMatrixResult)
            {
                IntMatrixResult dmr;

                //System.out.println(propertyType+"="+propertyValue);
                dmr = (IntMatrixResult) propertyValue;

                if (dmr.value == null)
                {
                    logger.error("Integer matrix entry " + propertyType + "=" +
                        propertyValue + " was not successfully parsed.");
                }
                else
                {
                    JOEPairData dp = new JOEPairData();
                    dp.setAttribute(propertyType);
                    dp.setValue(dmr);
                    currentMolecule.addData(dp);
                }
            }

            //System.out.println("Set object:"+objectType+" pType:"+propertyType+" pValue:"+propertyValue);
            if (logger.isDebugEnabled())
            {
                System.out.println("Set object:" + objectType + " pType:" +
                    propertyType + " pValue:" + propertyValue);
            }
        }
        else if (objectType.equals("Molecule"))
        {
            if (propertyType.equals("title"))
            {
                currentMolecule.setTitle((String) propertyValue);
            }
        }
    }

    /**
     * Procedure required by the CDOInterface. This methodName is only
     * supposed to be called by the JCFL library
     *
     * @return   Description of the Return Value
     */
    public CDOAcceptedObjects acceptObjects()
    {
        CDOAcceptedObjects objects = new CDOAcceptedObjects();
        objects.add("SetOfMolecules");
        objects.add("Molecule");
        objects.add("Fragment");
        objects.add("Atom");
        objects.add("Bond");
        objects.add("scalar");
        objects.add("String");
        objects.add("array");
        objects.add("matrix");

        /*objects.add("Animation");
        objects.add("Frame");
        objects.add("Crystal");
        objects.add("a-axis");
        objects.add("b-axis");
        objects.add("c-axis");
        objects.add("SetOfReactions");
        objects.add("Reactions");
        objects.add("Reactant");
        objects.add("Product");*/
        return objects;
    }

    /**
     * Procedure required by the CDOInterface. This methodName is only
     * supposed to be called by the JCFL library
     */
    public void endDocument()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("CML molecule added:" + currentMolecule);
        }
    }

    /**
     * Procedure required by the CDOInterface. This methodName is only
     * supposed to be called by the JCFL library
     *
     * @param objectType  Description of the Parameter
     */
    public void endObject(String objectType)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("END: " + objectType);
        }

        if (objectType.equals("Molecule"))
        {
            currentMolecule.endModify();
            currentMolecule.setOutputType(IOTypeHolder.instance().getIOType("SDF"));

            //System.out.println(""+currentMolecule.toString() );
            if (logger.isDebugEnabled())
            {
                logger.debug("Molecule added: \n" + currentMolecule.toString());
            }

            if (currentMolecule.has2D())
            {
                // are 3D coordinates also available ?
                // should never happen !!!!!!!!!
                if ((c3Dx.size() != 0) && (c3Dy.size() != 0) &&
                        (c3Dz.size() != 0))
                {
                    // o.k., available
                    if ((c3Dx.size() != c3Dy.size()) &&
                            (c3Dy.size() != c3Dz.size()))
                    {
                        logger.error("3D coordinats are inconsistent (x=" +
                            c3Dx.size() + ",y=" + c3Dy.size() + ",z=" +
                            c3Dz.size() + ").");
                    }
                    else
                    {
                        double[] c3DxArray = new double[c3Dx.size()];
                        double[] c3DyArray = new double[c3Dy.size()];
                        double[] c3DzArray = new double[c3Dz.size()];

                        for (int i = 0; i < c3DxArray.length; i++)
                        {
                            c3DxArray[i] = ((Double) c3Dx.get(i)).doubleValue();
                            c3DyArray[i] = ((Double) c3Dy.get(i)).doubleValue();
                            c3DzArray[i] = ((Double) c3Dz.get(i)).doubleValue();
                        }

                        AtomDoubleResult adr = new AtomDoubleResult();
                        adr.setDoubleArray(c3DxArray);

                        JOEPairData dp = new JOEPairData();
                        dp.setAttribute("coordinates3Dx");
                        dp.setValue(adr);
                        currentMolecule.addData(dp);
                        adr = new AtomDoubleResult();
                        adr.setDoubleArray(c3DyArray);
                        dp = new JOEPairData();
                        dp.setAttribute("coordinates3Dy");
                        dp.setValue(adr);
                        currentMolecule.addData(dp);
                        adr = new AtomDoubleResult();
                        adr.setDoubleArray(c3DzArray);
                        dp = new JOEPairData();
                        dp.setAttribute("coordinates3Dy");
                        dp.setValue(adr);
                        currentMolecule.addData(dp);
                    }
                }
            }

            if (currentMolecule.has3D())
            {
                // are 2D coordinates also available ?
                if ((c2Dx.size() != 0) && (c2Dy.size() != 0))
                {
                    // o.k., available
                    if (c2Dx.size() != c2Dy.size())
                    {
                        logger.error("2D coordinats are inconsistent (x=" +
                            c3Dx.size() + ",y=" + c3Dy.size() + ").");
                    }
                    else
                    {
                        double[] c2DxArray = new double[c2Dx.size()];
                        double[] c2DyArray = new double[c2Dy.size()];

                        for (int i = 0; i < c2DxArray.length; i++)
                        {
                            c2DxArray[i] = ((Double) c2Dx.get(i)).doubleValue();
                            c2DyArray[i] = ((Double) c2Dy.get(i)).doubleValue();
                        }

                        AtomDoubleResult adr = new AtomDoubleResult();
                        adr.setDoubleArray(c2DxArray);

                        JOEPairData dp = new JOEPairData();
                        dp.setAttribute("coordinates2Dx");
                        dp.setValue(adr);
                        currentMolecule.addData(dp);
                        adr = new AtomDoubleResult();
                        adr.setDoubleArray(c2DyArray);
                        dp = new JOEPairData();
                        dp.setAttribute("coordinates2Dy");
                        dp.setValue(adr);
                        currentMolecule.addData(dp);
                    }
                }
            }

            // resolve cis/trans isomerism informations
            BondInt bi = null;

            for (int i = 0; i < ezInformations.size(); i++)
            {
                bi = (BondInt) ezInformations.get(i);

                //System.out.println("Bond isomerism: "+bi.bond+" "+ bi.i);
                IsomerismDetection.setCisTransBond(bi.bond, bi.i);
            }

            // try to parse string entries, to check format and
            // enable correct storage functionality for other output
            // formats
            String tmpS;
            JOEGenericData genericData;

            for (Enumeration e = unparsed.keys(); e.hasMoreElements();)
            {
                tmpS = (String) e.nextElement();

                // convert descriptor entry from StringBuffer to String
                genericData = currentMolecule.getData(tmpS, false);

                JOEPairData pairData;

                if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA)
                {
                    pairData = (JOEPairData) genericData;

                    StringBuffer sb = (StringBuffer) pairData.getValue();
                    pairData.setValue(sb.toString());

                    //System.out.println(pairData.getAttribute()+"="+sb.toString());
                }

                // parse descriptor entry
                //System.out.println("Try parsing: "+currentMolecule.getData(tmpS, false));
                try
                {
                    genericData = currentMolecule.getData(tmpS, true);
                }
                 catch (Exception pe)
                {
                    logger.error("Error in parsing '" + tmpS + "': " +
                        currentMolecule.getData(tmpS, false));
                    pe.printStackTrace();
                }
            }

            for (Enumeration e = strings.keys(); e.hasMoreElements();)
            {
                tmpS = (String) e.nextElement();

                // parse descriptor entry
                //				System.out.println("Try parsing: "+currentMolecule.getData(tmpS, false));
                try
                {
                    genericData = currentMolecule.getData(tmpS, true);

                    JOEPairData pairData;
                    pairData = (JOEPairData) genericData;

                    //System.out.println(pairData.getValue().getClass().getName());
                    //System.out.println("parsed: "+((DescResult)pairData.getValue()).toString(IOTypeHolder.instance().getIOType("SDF")));
                }
                 catch (Exception pe)
                {
                    logger.error("Error in parsing '" + tmpS + "': " +
                        currentMolecule.getData(tmpS, false));
                    pe.printStackTrace();
                }
            }

            if (currentSetOfMolecules != null)
            {
                // store cloned molecule with all descriptors
                currentSetOfMolecules.addMol((JOEMol) currentMolecule.clone(
                        true));

                //System.out.println("Add molecule.");
            }

            if (moleculeCallback != null)
            {
                moleculeCallback.handleMolecule((JOEMol) currentMolecule.clone(
                        true));
            }

            //currentMolecule = null;
        }

        //		else if (objectType.equals("Frame"))
        //		{
        //		}
        //		else if (objectType.equals("Animation"))
        //		{
        //		}
        else if (objectType.equals("Atom"))
        {
            if (!Double.isNaN(z_3D))
            {
                currentAtom.setVector(x_3D, y_3D, z_3D);
            }
            else
            {
                currentAtom.setVector(x_2D, y_2D, 0.0);
            }

            //System.out.println("currentAtom:"+currentAtom.getIdx()+" "+currentAtom);
            //currentMolecule.addAtom(currentAtom);
        }
        else if (objectType.equals("Bond"))
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Bond: " + bond_a1 + ", " + bond_a2 + ", order=" +
                    bond_order + ", stereo=" + bond_stereo);
            }

            currentMolecule.addBond(bond_a1, bond_a2, bond_order, bond_stereo);

            //System.out.println("bond_a1:"+bond_a1+" bond_a2:"+bond_a2+" bond_order:"+bond_order+" "+bond_stereo);
            if (bond_EZ != IsomerismDetection.EZ_ISOMERISM_UNDEFINED)
            {
                JOEBond bond = currentMolecule.getBond(currentMolecule.numBonds() -
                        1);
                ezInformations.add(new BondInt(bond, bond_EZ));
            }
        }
    }

    // procedures required by CDOInterface

    /**
     * Procedure required by the CDOInterface. This methodName is only
     * supposed to be called by the JCFL library
     */
    public void startDocument()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Start new CML document");
        }

        //currentSetOfMolecules = new JOEMolVector();
        //currentMolecule = new JOEMol();
    }

    /**
     * Procedure required by the CDOInterface. This methodName is only
     * supposed to be called by the JCFL library
     *
     * @param objectType  Description of the Parameter
     */
    public void startObject(String objectType)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("START:" + objectType);
        }

        if (objectType.equals("Molecule"))
        {
            molIndex++;

            if (currentMolecule == null)
            {
                if (moleculeCallback == null)
                {
                    logger.error(
                        "No molecule initialized. Use setMolecule(JOEMol mol).");
                }

                currentMolecule = new JOEMol(IOTypeHolder.instance().getIOType("CML"),
                        IOTypeHolder.instance().getIOType("SDF"));
            }

            //			if (currentSetOfMolecules == null)
            //			{
            //				currentSetOfMolecules = new JOEMolVector();
            //			}
            //          if(molIndex!=1)   currentMolecule = new JOEMol();
            //currentMolecule = new JOEMol();
            atomEnumeration.clear();
            currentMolecule.clear();
            strings.clear();
            unparsed.clear();
            currentMolecule.beginModify();
            x_2D = y_2D = x_3D = y_3D = z_3D = Double.NaN;
            c2Dx.clear();
            c2Dy.clear();
            c3Dx.clear();
            c3Dy.clear();
            c3Dz.clear();
        }
        else if (objectType.equals("Atom"))
        {
            currentAtom = currentMolecule.newAtom();

            // set H atom
            currentAtom.setAtomicNum(1);
            currentAtom.setType(JOEElementTable.instance().getSymbol(1));

            if (logger.isDebugEnabled())
            {
                logger.debug("Atom # " + numberOfAtoms);
            }

            numberOfAtoms++;
        }

        //		else if (objectType.equals("Bond"))
        //		{
        //		}
        //		else if (objectType.equals("Animation"))
        //		{
        //		}
        //		else if (objectType.equals("Frame"))
        //		{
        //		}
        else if (objectType.equals("SetOfMolecules"))
        {
            //currentSetOfMolecules = new JOEMolVector();
            //currentMolecule = new JOEMol();
        }

        //		else if (objectType.equals("Crystal"))
        //		{
        //		}
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
