///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: CMLSymmetryWriter.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu,
//			egonw@sci.kun.nl, wegnerj@informatik.uni-tuebingen.de
//Version:  $Revision: 1.7 $
//			$Date: 2004/07/25 20:43:21 $
//			$Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 2 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.io.PrintStream;

import org.apache.log4j.Category;

import joelib.desc.result.DoubleArrayResult;

import joelib.io.IOType;
import joelib.io.IOTypeHolder;

import joelib.math.symmetry.PointGroup;
import joelib.math.symmetry.Symmetry;
import joelib.math.symmetry.SymmetryElement;
import joelib.math.symmetry.SymmetryException;

import joelib.molecule.JOEMol;

import joelib.util.JHM;


/**
 * Helper class for a CML molecule symmetry writer.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:21 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public class CMLSymmetryWriter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.CMLSymmetryWriter");
    private static IOType cml = IOTypeHolder.instance().getIOType("CML");

    //~ Methods ////////////////////////////////////////////////////////////////

    public static void writeAxes(PrintStream ps, String type,
        SymmetryElement[] symElements)
    {
        if (symElements == null)
        {
            return;
        }

        for (int i = 0; i < symElements.length; i++)
        {
            ps.print("    <symmetryElement type=\"" + type + "\" >" + JHM.eol);

            double[] tmp = new double[3];
            tmp[0] = symElements[i].distance * symElements[i].normal[0];
            tmp[1] = symElements[i].distance * symElements[i].normal[1];
            tmp[2] = symElements[i].distance * symElements[i].normal[2];
            writeDoubleArray(ps, "position", tmp);
            writeDoubleArray(ps, "direction", symElements[i].direction);
            writeAxisTypeScalar(ps, "axisType", symElements[i].order);

            ps.print("    </symmetryElement>" + JHM.eol);
        }
    }

    public static void writeAxisTypeScalar(PrintStream ps, String title,
        int value)
    {
        ps.print("      <scalar dataType=\"xsd:string\" title=\"" + title +
            "\" >");
        ps.print('C');
        ps.print(value);
        ps.print("</scalar>" + JHM.eol);
    }

    public static void writeDoubleArray(PrintStream ps, String title,
        double[] array)
    {
        DoubleArrayResult arrayResult = new DoubleArrayResult();
        arrayResult.setDoubleArray(array);

        ps.print("      <array dataType=\"xsd:double\" title=\"" + title +
            "\" >");
        ps.print(arrayResult.toString(cml));
        ps.print("</array>" + JHM.eol);
    }

    public static void writeDoubleScalar(PrintStream ps, String title,
        double value)
    {
        ps.print("      <scalar dataType=\"xsd:double\" title=\"" + title +
            "\" >");
        ps.print(value);
        ps.print("</scalar>" + JHM.eol);
    }

    public static void writeInversionCenter(PrintStream ps,
        SymmetryElement symElement)
    {
        if (symElement == null)
        {
            return;
        }

        ps.print("    <symmetryElement type=\"inversionCenter\" >" + JHM.eol);

        double[] tmp = new double[3];
        tmp[0] = symElement.distance * symElement.normal[0];
        tmp[1] = symElement.distance * symElement.normal[1];
        tmp[2] = symElement.distance * symElement.normal[2];
        writeDoubleArray(ps, "position", tmp);

        ps.print("    </symmetryElement>" + JHM.eol);
    }

    public static void writePlanes(PrintStream ps, SymmetryElement[] symElements)
    {
        if (symElements == null)
        {
            return;
        }

        for (int i = 0; i < symElements.length; i++)
        {
            ps.print("    <symmetryElement type=\"mirrorPlane\" >" + JHM.eol);

            writeDoubleArray(ps, "direction", symElements[i].normal);
            writeDoubleScalar(ps, "distance", symElements[i].distance);

            ps.print("    </symmetryElement>" + JHM.eol);
        }
    }

    public static void writeSymmetry(PrintStream ps, JOEMol mol)
    {
        Symmetry symmetry = new Symmetry();
        symmetry.readCoordinates(mol);

        try
        {
            symmetry.findSymmetryElements();
        }
         catch (SymmetryException e)
        {
            logger.error(e.getMessage());
        }

        if (symmetry.getBadOptimization())
        {
            logger.warn(
                "Refinement of some symmetry elements was terminated before convergence was reached.\n" +
                "Some symmetry elements may remain unidentified.");
        }

        PointGroup pointGroup = symmetry.identifyPointGroup();
        SymmetryElement symElement;
        SymmetryElement[] symElements;

        if (pointGroup != null)
        {
            ps.print("  <symmetry pointGroup=\"" + pointGroup.getGroupName() +
                "\" elements=\"" + pointGroup.getSymmetryCode().trim() +
                "\" id=\"s1\">" + JHM.eol);

            symElement = symmetry.getInversionCenter();

            if (symElement != null)
            {
                writeInversionCenter(ps, symElement);
            }

            symElements = symmetry.getAxes();

            if (symElements != null)
            {
                writeAxes(ps, "normalAxis", symElements);
            }

            symElements = symmetry.getImproperAxes();

            if (symElements != null)
            {
                writeAxes(ps, "improperAxis", symElements);
            }

            symElements = symmetry.getPlanes();

            if (symElements != null)
            {
                writePlanes(ps, symElements);
            }

            ps.print("  </symmetry>" + JHM.eol);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
