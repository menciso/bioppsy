///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: ResultCMLProperties.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  Joerg K. Wegner
//Version:  $Revision: 1.7 $
//			$Date: 2004/08/27 09:30:43 $
//			$Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 2 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.util.Enumeration;

import joelib.util.types.StringString;


/**
 * Interface for a CML molecule writer.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/08/27 09:30:43 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public interface ResultCMLProperties
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public Enumeration getCMLProperties();

    public void addCMLProperty(StringString property);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
