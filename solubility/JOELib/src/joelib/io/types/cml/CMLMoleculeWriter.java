///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: CMLMoleculeWriter.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  Joerg K. Wegner
//Version:  $Revision: 1.12 $
//			$Date: 2004/07/25 20:43:21 $
//			$Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 2 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.util.Vector;

import joelib.molecule.JOEMol;


/**
 * Interface for a CML molecule writer.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/07/25 20:43:21 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public interface CMLMoleculeWriter
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public void writeMolecule(JOEMol mol, boolean writePairData,
        Vector attribs2write);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
