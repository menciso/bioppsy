///////////////////////////////////////////////////////////////////////////////
//Filename: $RCSfile: CMLWriterProperties.java,v $
//Purpose:  Chemical Markup Language.
//Language: Java
//Compiler: JDK 1.4
//Authors:  Joerg K. Wegner
//Version:  $Revision: 1.8 $
//			$Date: 2004/04/13 14:09:24 $
//			$Author: wegner $
//
//Copyright (C) 1997-2003  The Chemistry Development Kit (CDK) project
//Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation version 2 of the License.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;


/**
 * Interface for a CML molecule writer.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.8 $, $Date: 2004/04/13 14:09:24 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public interface CMLWriterProperties
{
    //~ Methods ////////////////////////////////////////////////////////////////

    public float getCMLversion();

    public String getNamespace();

    public String getXMLDeclaration();

    public boolean forceWriteFormalCharge();

    public boolean storeChemistryKernelInfo();

    public boolean useNamespace();

    public boolean writeImpliciteHydrogens();

    public boolean writePartialCharge();

    public boolean writeSymmetryInformations();
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
