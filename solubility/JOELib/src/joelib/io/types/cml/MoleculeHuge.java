///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeHuge.java,v $
//  Purpose:  Reader/Writer for CML files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.23 $
//            $Date: 2004/07/25 20:43:21 $
//            $Author: wegner $
//  Original Author: steinbeck@ice.mpg.de, gezelter@maul.chem.nd.edu, egonw@sci.kun.nl
//  Original Version: Chemical Development Kit,  http://sourceforge.net/projects/cdk
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types.cml;

import java.io.PrintStream;

import java.util.Hashtable;

import org.apache.log4j.Category;

import joelib.desc.result.AtomDoubleResult;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;
import joelib.util.types.StringInt;


/**
 * CML molecule writing all informations explicitely.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.23 $, $Date: 2004/07/25 20:43:21 $
 * @cite rr99b
 * @cite mr01
 * @cite gmrw01
 * @cite wil01
 * @cite mr03
 * @cite mrww04
 */
public class MoleculeHuge extends CMLMoleculeWriterBase
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.io.types.cml.MoleculeHuge");

    //~ Constructors ///////////////////////////////////////////////////////////

    public MoleculeHuge(PrintStream _output, CMLWriterProperties _writerProp)
    {
        super(_output, _writerProp);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    protected void writeAtoms(JOEMol mol, String molID, boolean has3D,
        boolean has2D, AtomDoubleResult adrX, AtomDoubleResult adrY,
        Hashtable atomIDs)
    {
        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        StringInt atomID;
        int index = 0;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();
            atomID = (StringInt) atomIDs.get(molID + ":" + atom.getIdx());

            //System.out.println("get "+molID+":"+atom.getIdx()+"= id '"+ atomID.s+"'");
            writeAtom(atom, atomID, has2D, has3D, adrX, adrY);
            index++;
        }
    }

    protected void writeBonds(JOEMol mol, String molID, Hashtable atomIDs,
        Hashtable bondIDs)
    {
        BondIterator bit = mol.bondIterator();
        JOEBond bond;
        StringInt bondID;
        int index = 0;

        while (bit.hasNext())
        {
            bond = bit.nextBond();
            bondID = (StringInt) bondIDs.get(molID + ":" + bond.getIdx());

            //System.out.println("get "+molID+":"+bond.getIdx()+"= id '"+ bondID.s+"'");
            writeBond(molID, bond, bondID, atomIDs);
            index++;
        }
    }

    /**
     * Description of the Method
     *
     * @param bond  Description of the Parameter
     */
    private synchronized void writeBond(String molID, JOEBond bond,
        StringInt bondID, Hashtable atomIDs)
    {
        Hashtable attributes = new Hashtable();

        attributes.put("id", bondID.s);
        writeOpenTag(output, writerProp, "bond", attributes);

        JOEAtom beginAtom = bond.getBeginAtom();
        JOEAtom endAtom = bond.getEndAtom();
        StringInt beginAtomID = (StringInt) atomIDs.get(molID + ":" +
                beginAtom.getIdx());
        StringInt endAtomID = (StringInt) atomIDs.get(molID + ":" +
                endAtom.getIdx());
        attributes.clear();
        attributes.put("builtin", "atomRef");
        writeOpenTag(output, writerProp, "string", attributes, false);
        write(output, beginAtomID.s);
        writeCloseTag(output, writerProp, "string");
        writeOpenTag(output, writerProp, "string", attributes, false);
        write(output, endAtomID.s);
        writeCloseTag(output, writerProp, "string");

        this.writeBondOrder(bond);
        this.writeBondStereo(bond);

        writeCloseTag(output, writerProp, "bond");
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
