///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: PPM.java,v $
//  Purpose:  Reader/Writer for Undefined files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:20 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types;

import Acme.JPM.Encoders.PpmEncoder;

import java.awt.Image;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.log4j.Category;

import joelib.io.SimpleImageWriter;


/**
 * Writer for a Portable Pixelmap (PPM) image.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/07/25 20:43:20 $
 */
public class PPM extends SimpleImageWriter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */

    //private static Category logger = Category.getInstance("joelib.io.types.PPM");
    private static final String description = new String(
            "Portable Pixelmap (PPM) image");
    private static final String[] extensions = new String[]{"ppm"};

    //~ Methods ////////////////////////////////////////////////////////////////

    public String outputDescription()
    {
        return description;
    }

    public String[] outputFileExtensions()
    {
        return extensions;
    }

    public boolean writeImage(Image image, OutputStream os)
        throws IOException
    {
        PpmEncoder pc = new PpmEncoder(image, os);
        pc.encode();

        return (true);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
