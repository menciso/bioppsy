///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JPEG.java,v $
//  Purpose:  Reader/Writer for Undefined files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:20 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types;

import java.awt.Image;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.log4j.Category;

import com.obrador.JpegEncoder;

import joelib.io.SimpleImageWriter;


/**
 * Atom representation.
 */
public class JPEG extends SimpleImageWriter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */

    //    private static Category logger = Category.getInstance("joelib.io.types.JPEG");
    private static final String description = new String("JPEG image");
    private static final String[] extensions = new String[]{"jpg", "jpeg"};
    private static final int JPEG_QUALITY_IN_PERCENT = 100;

    //~ Methods ////////////////////////////////////////////////////////////////

    public String outputDescription()
    {
        return description;
    }

    public String[] outputFileExtensions()
    {
        return extensions;
    }

    public boolean writeImage(Image image, OutputStream os)
        throws IOException
    {
        JpegEncoder pc = new JpegEncoder(image, JPEG_QUALITY_IN_PERCENT, os);
        pc.Compress();

        return (true);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
