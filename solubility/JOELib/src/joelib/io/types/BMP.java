///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: BMP.java,v $
//  Purpose:  Reader/Writer for Undefined files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types;

import wsi.ra.image.BMPFile;

import java.awt.Image;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.log4j.Category;

import joelib.io.SimpleImageWriter;


/**
 * Writer for a Windows Bitmap (BMP) image.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:19 $
 */
public class BMP extends SimpleImageWriter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     *  Obtain a suitable logger.
     */

    //    private static Category logger = Category.getInstance("joelib.io.types.BMP");
    private static final String description = new String(
            "Windows Bitmap (BMP) image");
    private static final String[] extensions = new String[]{"bmp"};

    //~ Methods ////////////////////////////////////////////////////////////////

    public String outputDescription()
    {
        return description;
    }

    public String[] outputFileExtensions()
    {
        return extensions;
    }

    public boolean writeImage(Image image, OutputStream os)
        throws IOException
    {
        BMPFile bmp = new BMPFile();
        bmp.saveBitmap(os, image);

        return (true);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
