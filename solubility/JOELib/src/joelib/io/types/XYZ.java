///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: XYZ.java,v $
//  Purpose:  Reader/Writer for XYZ files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.17 $
//            $Date: 2004/07/25 20:43:20 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io.types;

import cformat.PrintfFormat;
import cformat.PrintfStream;
import cformat.ScanfReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.StringReader;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;
import joelib.data.JOETypeTable;

import joelib.io.MoleculeFileType;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;


/**
 * Reader/Writer for XYZ files.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.17 $, $Date: 2004/07/25 20:43:20 $
 */
public class XYZ implements MoleculeFileType
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance("joelib.io.types.XYZ");
    private final static String description = new String("XYZ");
    private final static String[] extensions = new String[]{"xyz"};

    //~ Instance fields ////////////////////////////////////////////////////////

    private LineNumberReader lnr;
    private PrintfStream ps;

    // helper variable for skipReaderEntry
    private int linesRemaining;
    private long lineCounter;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the XYZ object
     */
    public XYZ()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public void closeReader() throws IOException
    {
        lnr.close();
    }

    public void closeWriter() throws IOException
    {
        ps.close();
    }

    /**
     *  Description of the Method
     *
     * @param  is               Description of the Parameter
     * @exception  IOException  Description of the Exception
     */
    public void initReader(InputStream is) throws IOException
    {
        lnr = new LineNumberReader(new InputStreamReader(is));
    }

    /**
     *  Description of the Method
     *
     * @param  os               Description of the Parameter
     * @exception  IOException  Description of the Exception
     */
    public void initWriter(OutputStream os) throws IOException
    {
        ps = new PrintfStream(os);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String inputDescription()
    {
        return description;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String[] inputFileExtensions()
    {
        return extensions;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String outputDescription()
    {
        return description;
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String[] outputFileExtensions()
    {
        return extensions;
    }

    /**
     *  Reads an molecule entry as (unparsed) <tt>String</tt> representation.
     *
     * @return                  <tt>null</tt> if the reader contains no more
     *      relevant data. Otherwise the <tt>String</tt> representation of the
     *      whole molecule entry is returned.
     * @exception  IOException  typical IOException
     */
    public String read() throws IOException
    {
        String line;

        if ((line = lnr.readLine()) == null)
        {
            return null;
        }

        int s;

        try
        {
            s = Integer.parseInt(line);
        }
         catch (NumberFormatException ex)
        {
            return null;
        }

        StringBuffer sb = new StringBuffer(s * 100);

        // set number of atoms
        sb.append(line);
        sb.append('\n');

        // number of atoms
        linesRemaining = s;

        // set title
        if ((line = lnr.readLine()) == null)
        {
            return null;
        }

        sb.append(line);
        sb.append('\n');

        // set element and coordinates
        for (int i = 1; i <= s; i++, linesRemaining--)
        {
            if ((line = lnr.readLine()) == null)
            {
                linesRemaining--;
                skipReaderEntry();

                return null;
            }

            //sb.append(i-1);
            sb.append(line);
            sb.append('\n');
        }

        //System.out.println(sb);
        return sb.toString();
    }

    /**
     *  Description of the Method
     *
     * @param  mol              Description of the Parameter
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean read(JOEMol mol) throws IOException
    {
        return read(mol, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol              Description of the Parameter
     * @param  title            Description of the Parameter
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean read(JOEMol mol, String title) throws IOException
    {
        int i;
        int natoms;

        String line;

        ScanfReader scanf;

        // delete molecule data
        mol.clear();

        if ((line = lnr.readLine()) == null)
        {
            return (false);
        }

        scanf = new ScanfReader(new StringReader(line));
        natoms = scanf.scanInt();

        if (natoms == 0)
        {
            return (false);
        }

        mol.reserveAtoms(natoms);
        JOETypeTable.instance().setFromType("XYZ");

        String str;
        double x;
        double y;
        double z;
        JOEAtom atom;
        String elemString;

        if ((line = lnr.readLine()) == null)
        {
            return (false);
        }

        // set molecule title
        if (title == null)
        {
            mol.setTitle(line);
        }
        else
        {
            mol.setTitle(title);
        }

        JOETypeTable.instance().setToType("INT");

        // get all atoms
        linesRemaining = natoms;

        for (i = 1; i <= natoms; i++, linesRemaining--)
        {
            if ((line = lnr.readLine()) == null)
            {
                return (false);
            }

            scanf = new ScanfReader(new StringReader(line));

            //tokenize(vs,buffer);
            //if (vs.size() != 4) return(false);
            //x = atof((char*)vs[1].c_str());
            //y = atof((char*)vs[2].c_str());
            //z = atof((char*)vs[3].c_str());
            atom = mol.newAtom();

            //set atomic number
            elemString = scanf.scanString();
            atom.setAtomicNum(JOEElementTable.instance().getAtomicNum(elemString));

            //set coordinates
            x = scanf.scanFloat();
            y = scanf.scanFloat();
            z = scanf.scanFloat();
            atom.setVector(x, y, z);

            //set type
            str = JOETypeTable.instance().translate(elemString);
            atom.setType(str);
        }

        // connect the atoms with bonds
        mol.connectTheDots();

        return (true);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean readable()
    {
        return true;
    }

    public boolean skipReaderEntry() throws IOException
    {
        String line;

        for (int i = linesRemaining; i > 0; i--)
        {
            if ((line = lnr.readLine()) == null)
            {
                lineCounter++;

                return (false);
            }

            line = null;
        }

        return true;
    }

    /**
     *  Description of the Method
     *
     * @param  mol              Description of the Parameter
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol) throws IOException
    {
        return write(mol, null);
    }

    /**
     *  Description of the Method
     *
     * @param  mol              Description of the Parameter
     * @param  title            Description of the Parameter
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol, String title) throws IOException
    {
        int i;
        PrintfFormat f15_5 = new PrintfFormat("%15.5f");
        PrintfFormat s3 = new PrintfFormat("%3s");

        ps.printf("%d", mol.numAtoms());
        ps.println();

        //        System.out.println("TITLE:"+mol.getTitle());
        if (title == null)
        {
            ps.print(mol.getTitle());
        }
        else
        {
            ps.print(title);
        }

        ps.printf("\t%15.7f", mol.getEnergy());
        ps.println();
        JOETypeTable.instance().setFromType("INT");
        JOETypeTable.instance().setToType("XYZ");

        JOEAtom atom;

        for (i = 1; i <= mol.numAtoms(); i++)
        {
            atom = mol.getAtom(i);

            ps.printf(s3,
                JOEElementTable.instance().getSymbol(atom.getAtomicNum()));
            ps.printf(f15_5, atom.getX());
            ps.printf(f15_5, atom.getY());
            ps.printf(f15_5, atom.getZ());
            ps.println();
        }

        return (true);
    }

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public boolean writeable()
    {
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
