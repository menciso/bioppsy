///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeFileType.java,v $
//  Purpose:  Interface for implementing molecule reader/writer classes.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import joelib.molecule.JOEMol;


/**
 * Interface for implementing molecule reader/writer classes.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:43:19 $
 */
public interface MoleculeFileType
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Description of the Method
     *
     * @exception  IOException  Description of the Exception
     */
    public void closeReader() throws IOException;

    /**
     *  Description of the Method
     *
     * @exception  IOException  Description of the Exception
     */
    public void closeWriter() throws IOException;

    /**
     *  Description of the Method
     *
     * @param  is               Description of the Parameter
     * @exception  IOException  Description of the Exception
     */
    public void initReader(InputStream is) throws IOException;

    /**
     *  Description of the Method
     *
     * @param  os               Description of the Parameter
     * @exception  IOException  Description of the Exception
     */
    public void initWriter(OutputStream os) throws IOException;

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String inputDescription();

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String[] inputFileExtensions();

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String outputDescription();

    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    public String[] outputFileExtensions();

    /**
     *  Reads an molecule entry as  (unparsed) <tt>String</tt> representation.
     *
     * @param  mol                        the molecule to store the data
     * @return                            <tt>null</tt> if the reader contains no
     *      more relevant data. Otherwise the <tt>String</tt> representation
     *      of the whole molecule entry is returned.
     * @exception  IOException            typical IOException
     */
    public String read() throws IOException;

    /**
     *  Reads an molecule entry from the initialized molecule reader.
     *  If an <tt>MoleculeIOException</tt> occurs the molecule
     *  reader stream should be set to the start of the next molecule
     *  entry. Use: <tt>skipReaderEntry()</tt>
     *
     * @param  mol                        the molecule to store the data
     * @return                            <tt>false</tt> if the reader contains no
     *      more relevant data. <tt>true</tt> if a molecule was succesfull readed.
     *      If the molecule is incorrect or can't be parsed use <tt>
     *      MoleculeIOException</tt>.
     * @exception  IOException            typical IOException
     * @exception  MoleculeIOException  If an serious molecule parsing error
     *      occured. This is important to skip molecule entries.
     */
    public boolean read(JOEMol mol) throws IOException, MoleculeIOException;

    /**
     *  Reads an molecule entry from the initialized molecule reader.
     *  If an <tt>MoleculeIOException</tt> occurs the molecule
     *  reader stream should be set to the start of the next molecule
     *  entry. Use: <tt>skipReaderEntry()</tt>
     *
     * @param  mol                        the molecule to store the data
     * @param  title                      the title of the molecule. If
     *                                    <tt>title==null</tt> the title
     *                                    entry from the molecule <tt>mol.getTitle()</tt>
     *                                    should be used
     * @return                            <tt>false</tt> if the reader contains no
     *      more relevant data. <tt>true</tt> if a molecule was succesfull readed.
     *      If the molecule is incorrect or can't be parsed use <tt>
     *      MoleculeIOException</tt>.
     * @exception  IOException            typical IOException
     * @exception  MoleculeIOException  If an serious molecule parsing error
     *      occured. This is important to skip molecule entries.
     */
    public boolean read(JOEMol mol, String title)
        throws IOException, MoleculeIOException;

    /**
     * Returns <tt>true</tt> if this molecule data type is readable.
     *
     * @return    Description of the Return Value
     */
    public boolean readable();

    /**
     * Skips an molecule or the rest of a molecule entry to grant the
     * next <tt>read(JOEMol mol)</tt> invocation a proper starting position.
     * This method should always be called if you plan to throw an
     * <tt>MoleculeIOException</tt>.
     *
     * @return                  Description of the Return Value
     * @exception  IOException  Description of the Exception
     */
    public boolean skipReaderEntry() throws IOException;

    /**
     *  Reads an molecule entry from the initialized molecule reader.
     *
     * @param  mol                        the molecule to store the data
     * @return                            <tt>false</tt> if the reader contains no
     *      more relevant data. <tt>true</tt> if a molecule was succesfull readed.
     *      If the molecule is incorrect or can't be parsed use <tt>
     *      MoleculeIOException</tt>.
     * @exception  IOException            typical IOException
     * @exception  MoleculeIOException  If an serious molecule parsing error
     *      occured. This is important to skip molecule entries.
     */
    public boolean write(JOEMol mol) throws IOException, MoleculeIOException;

    /**
     *  Reads an molecule entry from the initialized molecule reader.
     *
     * @param  mol                        the molecule to store the data
     * @param  title                      the title of the molecule. If
     *                                    <tt>title==null</tt> the title
     *                                    entry from the molecule <tt>mol.getTitle()</tt>
     *                                    should be used
     * @return                            <tt>false</tt> if the reader contains no
     *      more relevant data. <tt>true</tt> if a molecule was succesfull readed.
     *      If the molecule is incorrect or can't be parsed use <tt>
     *      MoleculeIOException</tt>.
     * @exception  IOException            typical IOException
     * @exception  MoleculeIOException  If an serious molecule parsing error
     *      occured. This is important to skip molecule entries.
     */
    public boolean write(JOEMol mol, String title)
        throws IOException, MoleculeIOException;

    /**
     * Returns <tt>true</tt> if this molecule data type is writeable.
     *
     * @return    Description of the Return Value
     */
    public boolean writeable();
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
