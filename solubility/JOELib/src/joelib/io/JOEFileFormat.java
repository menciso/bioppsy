///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEFileFormat.java,v $
//  Purpose:  Factory class to get loader/writer classes.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Category;


/**
 * Factory class to get loader/writer classes.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/25 20:43:19 $
 */
public class JOEFileFormat
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.io.JOEFileFormat");

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOEFileFormat object
     */
    public JOEFileFormat()
    {
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the molReader attribute of the JOEFileFormat class. <br>
     *  <tt>MoleculeFileType loader = JOEFileFormat.getMolReader(InputStream is,
     *  IOType type)<br>
     *  if (loader.readable())<br>
     *  {<br>
     *  &nbsp;&nbsp;boolean success = loader.read(is, mol, title);<br>
     *  &nbsp;&nbsp;return success;<br>
     *  }<br>
     *  else<br>
     *  {<br>
     *  &nbsp;&nbsp;logger.warn(type.getRepresentation() + " is not readable.");
     *  <br>
     *  &nbsp;&nbsp;return false;<br>
     *  }</tt>
     *
     * @param  is                         Description of the Parameter
     * @param  type                       Description of the Parameter
     * @return                            The molReader value
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public static MoleculeFileType getMolReader(InputStream is, IOType type)
        throws IOException, MoleculeIOException
    {
        // try to load MoleculeFileType representation class
        MoleculeFileType loader = getMoleculeFileType(type);

        if (loader != null)
        {
            loader.initReader(is);

            return loader;
        }
        else
        {
            return null;
        }
    }

    /**
     *  Description of the Method<br>
     *  <tt>MoleculeFileType saver = JOEFileFormat.getMolWrite(OutputStream os,
     *  IOType type)<br>
     *  if (saver.writeable())<br>
     *  {<br>
     *  boolean success = saver.write(os, mol, title);<br>
     *  return success;<br>
     *  }<br>
     *  else<br>
     *  {<br>
     *  logger.warn(type.getRepresentation() + " is not writeable.");<br>
     *  return false;<br>
     *  }</tt>
     *
     * @param  os                         Description of the Parameter
     * @param  type                       Description of the Parameter
     * @return                            Description of the Return Value
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public static MoleculeFileType getMolWriter(OutputStream os, IOType type)
        throws IOException, MoleculeIOException
    {
        // try to load MoleculeFileType representation class
        MoleculeFileType saver = getMoleculeFileType(type);

        if (saver != null)
        {
            saver.initWriter(os);

            return saver;
        }
        else
        {
            return null;
        }
    }

    /**
     *  Gets the moleculeFileType attribute of the JOEFileFormat class
     *
     * @param  type                       Description of the Parameter
     * @return                            The moleculeFileType value
     * @exception  IOException            Description of the Exception
     * @exception  MoleculeIOException  Description of the Exception
     */
    public static MoleculeFileType getMoleculeFileType(IOType type)
        throws MoleculeIOException
    {
        // try to load MoleculeFileType representation class
        MoleculeFileType mfType = null;

        if (logger.isDebugEnabled())
        {
            logger.debug("Load MoleculeFileType: " + type.getRepresentation());
        }

        //System.out.println("Load MoleculeFileType: "+type.getRepresentation());
        try
        {
            mfType = (MoleculeFileType) Class.forName(type.getRepresentation())
                                             .newInstance();
        }
         catch (ClassNotFoundException ex)
        {
            throw new MoleculeIOException(type.getRepresentation() +
                " not found.");
        }
         catch (InstantiationException ex)
        {
            //ex.printStackTrace();
            throw new MoleculeIOException(type.getRepresentation() +
                " can not be instantiated.");
        }
         catch (IllegalAccessException ex)
        {
            throw new MoleculeIOException(type.getRepresentation() +
                " can't be accessed.");
        }

        if (mfType == null)
        {
            throw new MoleculeIOException("MoleculeFileType class " +
                type.getRepresentation() + " does'nt exist.");

            //            System.exit(1);
        }

        return mfType;
    }

    /**
     *  Description of the Method
     */
    public void finalize()
    {
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
