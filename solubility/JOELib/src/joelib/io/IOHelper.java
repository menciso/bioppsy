///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: IOHelper.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io;

import wsi.ra.tool.PropertyHolder;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;


/**
 * Some methods to faciliate the work with descriptors.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.11 $, $Date: 2004/07/25 20:43:19 $
 */
public final class IOHelper
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.desc.IOHelper");
    private static IOHelper instance;

    //~ Instance fields ////////////////////////////////////////////////////////

    private PropertyHolder propertyHolder;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initializes the input/output helper factory.
     */
    private IOHelper()
    {
        propertyHolder = PropertyHolder.instance();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the instance of the input/output helper factory.
     *
     * @return the instance of the input/output helper factory
     */
    public static synchronized IOHelper instance()
    {
        if (instance == null)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Getting " + IOHelper.class.getClass().getName() +
                    " instance.");
            }

            instance = new IOHelper();

            //			instance.loadInfos();
        }

        return instance;
    }

    /**
     * Loads a single molecule from a file.
     *
     * @param inputFile the input file
     * @param type the input type
     * @return the loaded molecule
     * @throws IOException input/ouput exception
     * @throws MoleculeIOException molecule parsing exception
     *
     * @see joelib.molecule.JOEMolVector
     */
    public static JOEMol loadMolFromFile(String inputFile, String type)
        throws IOException, MoleculeIOException
    {
        return loadMolFromFile(null, null, inputFile, type);
    }

    /**
     * Loads a single molecule from a file.
     *
     * @param loader the loader class (array with one element) for open molecule input stream
     * @param mol the molecule in which the first molecule will be stored
     * @param inputFile the input file
     * @param type the input type
     * @return the loaded molecule
     * @throws IOException input/ouput exception
     * @throws MoleculeIOException molecule parsing exception
     *
     * @see joelib.molecule.JOEMolVector
     */
    public static JOEMol loadMolFromFile(MoleculeFileType[] loader, JOEMol mol,
        String inputFile, String type) throws IOException, MoleculeIOException
    {
        if (inputFile == null)
        {
            //			logger.error("No input file defined.");
            //			return null;
            throw new IOException("No input file defined.");
        }

        IOType inType = null;

        if ((loader == null) || ((loader != null) && (loader[0] == null)))
        {
            if (loader == null)
            {
                loader = new MoleculeFileType[1];
            }

            if (type == null)
            {
                // try to resolve file extension
                inType = SimpleReader.checkGetInputType(inputFile);
            }
            else
            {
                inType = IOTypeHolder.instance().getIOType(type.toUpperCase());
            }

            FileInputStream in = null;

            // get molecule loader/writer
            try
            {
                in = new FileInputStream(inputFile);

                if ((loader != null) && (loader[0] == null))
                {
                    loader[0] = JOEFileFormat.getMolReader(in, inType);
                }
            }
             catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

        if (!loader[0].readable())
        {
            //			logger.error(inType.getRepresentation() + " is not readable.");
            //			logger.error("You're invited to write one !;-)");
            //			return null;
            throw new IOException(inType.getRepresentation() +
                " is not readable.");
        }

        if (mol == null)
        {
            mol = new JOEMol(inType, IOTypeHolder.instance().getIOType("SMILES"));
        }
        else
        {
            mol.setInputType(inType);
        }

        boolean success = true;
        mol.clear();

        try
        {
            success = loader[0].read(mol);

            if (!success)
            {
                //				logger.info("No molecule loaded");
                //				return null;
                throw new IOException("File contains no valid molecule.");
            }

            if (mol.empty())
            {
                logger.warn("Empty molecule loaded.");
            }
        }
         catch (IOException ex)
        {
            //			ex.printStackTrace();
            //			logger.error(ex.toString());
            //			return null;
            throw ex;
        }
         catch (MoleculeIOException ex)
        {
            //			logger.error(ex.toString());
            //			//logger.info("Molecule '" + mol.getTitle() + "' was skipped.");
            //			return null;
            throw ex;
        }

        return mol;
    }

    /**
     * Write a molecule to file.
     *
     * @param mol          the molecule to store
     * @param outputFile   the output file
     * @param type         the output type
     * @return             <tt>true</tt> if the molecule was written successfully
     * @throws IOException input/ouput exception
     * @throws MoleculeIOException molecule parsing exception
     *
     * @see joelib.molecule.JOEMolVector
     */
    public static boolean saveMolFromFile(JOEMol mol, String outputFile,
        String type) throws IOException, MoleculeIOException
    {
        return saveMolToFile(null, mol, outputFile, type);
    }

    /**
     * Write a molecule to file.
     *
     * @param writer       the writer class (array with one element) for open molecule output stream
     * @param mol          the molecule to store
     * @param outputFile   the output file
     * @param type         the output type
     * @return             <tt>true</tt> if the molecule was written successfully
     * @throws IOException input/ouput exception
     * @throws MoleculeIOException molecule parsing exception
     *
     * @see joelib.molecule.JOEMolVector
     */
    public static boolean saveMolToFile(MoleculeFileType[] writer, JOEMol mol,
        String outputFile, String type) throws IOException, MoleculeIOException
    {
        boolean writerWasNotDefined = false;

        if (outputFile == null)
        {
            //			logger.error("No output file defined.");
            //			return false;
            throw new IOException("No output file defined.");
        }

        IOType outType = null;

        if (mol == null)
        {
            //			logger.error("No molecule defined.");
            //			return false;
            throw new IOException("No molecule defined.");
        }

        if ((writer == null) || ((writer != null) && (writer[0] == null)))
        {
            writerWasNotDefined = true;

            if (writer == null)
            {
                writer = new MoleculeFileType[1];
            }

            if (type == null)
            {
                // try to resolve file extension
                try
                {
                    outType = SimpleWriter.checkGetOutputType(outputFile);
                }
                 catch (IOException ioEx)
                {
                    if ((writer != null) && (writer[0] != null))
                    {
                        writer[0].closeWriter();
                    }

                    throw ioEx;
                }
            }
            else
            {
                outType = IOTypeHolder.instance().getIOType(type.toUpperCase());
            }

            FileOutputStream out = null;

            // get molecule loader/writer
            try
            {
                out = new FileOutputStream(outputFile);

                if ((writer != null) && (writer[0] == null))
                {
                    writer[0] = JOEFileFormat.getMolWriter(out, outType);
                }
            }
             catch (Exception ex)
            {
                try
                {
                    if ((writer != null) && (writer[0] != null))
                    {
                        writer[0].closeWriter();
                    }
                }
                 catch (Exception exception)
                {
                    throw new IOException(exception.getMessage());
                }

                throw new IOException(ex.getMessage());
            }
        }

        if (!writer[0].writeable())
        {
            if (writerWasNotDefined)
            {
                try
                {
                    if ((writer != null) && (writer[0] != null))
                    {
                        writer[0].closeWriter();
                    }
                }
                 catch (Exception exception)
                {
                    throw new IOException(exception.getMessage());
                }
            }

            //			logger.error(outType.getRepresentation() + " is not writeable.");
            //			logger.error("You're invited to write one !;-)");
            //			return false;
            throw new IOException(outType.getRepresentation() +
                " is not writeable.");
        }

        boolean success = false;

        try
        {
            success = writer[0].write(mol);

            if (!success)
            {
                if (writerWasNotDefined)
                {
                    if ((writer != null) && (writer[0] != null))
                    {
                        writer[0].closeWriter();
                    }
                }

                //				logger.error(mol.getTitle() + " was not saved successfully.");
                //				return false;
                throw new IOException(mol.getTitle() +
                    " was not saved successfully.");
            }
        }
         catch (IOException ex)
        {
            //			ex.printStackTrace();
            //			return false;
            throw ex;
        }
         catch (MoleculeIOException ex)
        {
            //			ex.printStackTrace();
            //			return false;
            throw ex;
        }
         finally
        {
            if (writerWasNotDefined)
            {
                try
                {
                    if ((writer != null) && (writer[0] != null))
                    {
                        writer[0].closeWriter();
                    }
                }
                 catch (Exception exception)
                {
                    throw new IOException(exception.getMessage());
                }
            }
        }

        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
