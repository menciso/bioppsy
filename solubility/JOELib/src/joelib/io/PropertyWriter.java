///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: PropertyWriter.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io;

import java.io.IOException;

import java.util.Vector;

import joelib.molecule.JOEMol;


/**
 * Interface for molecule file formats, which accepts descriptor values.
 *
 * For speed optimization of loading descriptor molecule files have a
 * look at the {@link joelib.desc.ResultFactory}.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.10 $, $Date: 2004/07/25 20:43:19 $
 */
public interface PropertyWriter
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Writes a molecule with his <tt>JOEPairData</tt>.
     *
     *@param  mol              the molecule with additional data
     *@param  title            the molecule title or <tt>null</tt> if the title
     *                         from the molecule should be used
     *@param  writePairData    if <tt>true</tt> then the additional molecule
     *                         data is written
     *@param  attrib2write     if <tt>null</tt> all <tt>JOEPairData</tt> elements
     *                         are written, otherwise all data elements are
     *                         written which are listed in <tt>attrib2write</tt>.
     *@return                  <tt>true</tt> if the molecule and the data
     *                         has been succesfully written.
     *@exception  IOException  Description of the Exception
     */
    public boolean write(JOEMol mol, String title, boolean writePairData,
        Vector attribs2write) throws IOException, MoleculeIOException;
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
