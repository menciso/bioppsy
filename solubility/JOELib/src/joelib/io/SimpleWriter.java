///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: SimpleWriter.java,v $
//  Purpose:  Example for converting molecules.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.16 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io;

import wsi.ra.tool.StopWatch;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.log4j.Category;

import joelib.molecule.JOEMol;


/**
 * Simple writer implementation.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.16 $, $Date: 2004/07/25 20:43:19 $
 */
public class SimpleWriter
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.io.SimpleWriter");
    private static boolean VERBOSE = false;
    private static IOType verboseType = IOTypeHolder.instance().getIOType("SMILES");

    //~ Instance fields ////////////////////////////////////////////////////////

    private IOType outType;
    private MoleculeFileType writer = null;
    private OutputStream out = null;
    private StopWatch watch;
    private int molCounter;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Creates a simple writer where the file type is resolved by the file extension.
     *
     * @param outputFile      Output file
     * @throws IOException input/output exception
     */
    public SimpleWriter(String outputFile) throws IOException
    {
        IOType tmpOut = checkGetOutputType(outputFile);
        init(new FileOutputStream(outputFile), tmpOut);
    }

    /**
     * Creates a simple writer.
     *
     * @param outputFile      Output file
     * @param _outTypeString  Output type
     * @throws IOException input/output exception
     */
    public SimpleWriter(String outputFile, String _outTypeString)
        throws IOException
    {
        init(new FileOutputStream(outputFile),
            IOTypeHolder.instance().getIOType(_outTypeString.toUpperCase()));
    }

    /**
     * Creates a simple writer.
     *
     * @param _out            Output stream
     * @param _outTypeString  Output type
     * @throws IOException input/output exception
     */
    public SimpleWriter(OutputStream _out, String _outTypeString)
        throws IOException
    {
        init(_out,
            IOTypeHolder.instance().getIOType(_outTypeString.toUpperCase()));
    }

    /**
     * Creates a simple writer.
     *
     * @param _out            Output stream
     * @param _outType        Output type
     * @throws IOException input/output exception
     */
    public SimpleWriter(OutputStream _out, IOType _outType)
        throws IOException
    {
        init(_out, _outType);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the output type by the output file.
     *
     * The selected output type will be sended to the logger.
     *
     * @param output output file
     * @return the IOType resolved by the file name
     * @throws IOException Input/output exception
     */
    public static IOType checkGetOutputType(String outputFile)
        throws IOException
    {
        IOType outType = null;

        if (outType == null)
        {
            outType = IOTypeHolder.instance().filenameToType(outputFile);

            if (outType == null)
            {
                throw new IOException("Output type of " + outputFile +
                    " could not be estimated.");
            }

            MoleculeFileType mfType = null;

            try
            {
                mfType = JOEFileFormat.getMoleculeFileType(outType);
            }
             catch (MoleculeIOException ex)
            {
                throw new IOException(ex.getMessage());
            }

            logger.info("Output type set to " + outType.toString() + ": " +
                mfType.outputDescription());
        }

        return outType;
    }

    /**
     * Close the simple reader.
     * IOExceptions are supressed.
     */
    public void close()
    {
        try
        {
            if (writer != null)
            {
                writer.closeWriter();
            }
        }
         catch (IOException ex)
        {
            logger.error(ex.toString());
        }
    }

    /**
     * Returns the number of written molecules.
     *
     * @return the number of written molecules
     */
    public int moleculesWritten()
    {
        return molCounter;
    }

    /**
     * Writes next molecule.
     *
     * @param mol molecule to store
     * @return <tt>true</tt> if the molecule was written successfully
     * @throws IOException input/output exception
     * @throws MoleculeIOException molecule parsing exception
     */
    public boolean writeNext(JOEMol mol)
        throws IOException, MoleculeIOException
    {
        if (out == null)
        {
            throw new IOException(this.getClass().getName() +
                " not initialized.");
        }

        boolean success = true;

        try
        {
            success = writer.write(mol);

            if (!success)
            {
                logger.info("... " + molCounter +
                    " molecules successful written in " +
                    watch.getPassedTime() + " ms.");

                return false;
            }

            if (VERBOSE)
            {
                System.out.println("written: " + mol.toString(verboseType));
            }

            molCounter++;
        }
         catch (IOException ex)
        {
            throw ex;
        }

        if ((molCounter % 1000) == 0)
        {
            logger.info("... " + molCounter +
                " molecules successful written in " + watch.getPassedTime() +
                " ms.");
        }

        return true;
    }

    /**
     * Creates a simple writer.
     *
     * @param _out      output stream
     * @param _outType  output type
     * @return          <tt>true</tt> if more molecules are available
     * @throws IOException input/output exception
     */
    private boolean init(OutputStream _out, IOType _outType)
        throws IOException
    {
        outType = _outType;

        if (outType == null)
        {
            //logger.error("Output type not defined.");
            throw new IOException("Output type not defined.");
        }

        try
        {
            out = _out;
            writer = JOEFileFormat.getMolWriter(out, outType);
        }
         catch (Exception ex)
        {
            //ex.printStackTrace();
            throw new IOException("Can not get molecule writer instance.");
        }

        if (!writer.writeable())
        {
            logger.error(outType.getRepresentation() + " is not writeable.");
            logger.error("You're invited to write one !;-)");
            System.exit(1);
        }

        watch = new StopWatch();
        molCounter = 0;

        //    logger.info("Start writing ...");
        return true;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
