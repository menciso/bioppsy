///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: IOTypeHolder.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.31 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io;

import cformat.PrintfFormat;
import cformat.PrintfStream;

import wsi.ra.tool.PropertyHolder;

import java.io.ByteArrayOutputStream;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Category;

import joelib.util.JHM;
import joelib.util.types.StringString;


/**
 * Holder for input/output types for molecules.
 * Molecule import/export types are defined in the {@link wsi.ra.tool.PropertyHolder}.
 * The {@link wsi.ra.tool.ResourceLoader} loads the <tt>joelib.properties</tt> file.
 *
 * <p>
 * File types can be defined by using <tt>joelib.filetypes.NUMBER.name</tt>.
 * The default representating class will be taken from the internal list.
 * If you want another representating class use additional
 * <tt>joelib.filetypes.NUMBER.representation</tt>.<br>
 * Example:
 * <blockquote><pre>
 * joelib.filetypes.1.name           = SDF
 * joelib.filetypes.1.representation = joelib.io.types.MDLSD
 * joelib.filetypes.2.name           = SMILES
 * joelib.filetypes.3.name           = MOL2
 * </pre></blockquote>
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.31 $, $Date: 2004/07/25 20:43:19 $
 * @see wsi.ra.tool.PropertyHolder
 * @see wsi.ra.tool.ResourceLoader
 */
public final class IOTypeHolder
{
    //~ Static fields/initializers /////////////////////////////////////////////

    // Obtain a suitable logger.
    private static Category logger = Category.getInstance(
            "joelib.io.IOTypeHolder");
    private static IOTypeHolder instance;
    private final static StringString[] defaultTypes = 
    {
        new StringString("UNDEFINED", "joelib.io.types.Undefined"),
        new StringString("ALCHEMY", "joelib.io.types.Alchemy"),
        new StringString("BALLSTICK", "joelib.io.types.BallAndStick"),
        new StringString("BGF", ""),
        new StringString("BMP", "joelib.io.types.BMP"),
        new StringString("BMIN", ""),
        new StringString("BOX", "joelib.io.types.Box"),
        new StringString("BIOSYM", "joelib.io.types.BiosymCar"),
        new StringString("CML", "joelib.io.types.ChemicalMarkupLanguage"),
        new StringString("CTX", "joelib.io.types.ClearTextFormat"),
        new StringString("CHARMM", ""), new StringString("CADPAC", ""),
        new StringString("CHEM3D1", ""), new StringString("CHEM3D2", ""),
        new StringString("CCC", "joelib.io.types.CCC"),
        new StringString("CACAO", "joelib.io.types.Cacao"),
        new StringString("CACAOINT", "joelib.io.types.CacaoInternal"),
        new StringString("CACHE", "joelib.io.types.Cache"),
        new StringString("CHEMDRAW", "joelib.io.types.ChemDraw"),
        new StringString("CSR", "joelib.io.types.CSR"),
        new StringString("CSSR", "joelib.io.types.CSSR"),
        new StringString("DELPDB", "joelib.io.types.DelphiPDB"),
        new StringString("DMOL", "joelib.io.types.DMol"),
        new StringString("DOCK", ""), new StringString("FDAT", ""),
        new StringString("FIX", "joelib.io.types.FixSmiles"),
        new StringString("FEATURE", "joelib.io.types.Feat"),
        new StringString("FRACT", ""),
        new StringString("FH", "joelib.io.types.FenskeZMat"),
        new StringString("GAMESSIN", "joelib.io.types.Gamess"),
        new StringString("GAMESSOUT", "joelib.io.types.Gamess"),
        new StringString("GAUSSIANZMAT", ""),
        new StringString("GAUSSIANCART", "joelib.io.types.Gaussian"),
        new StringString("GAUSSIAN92", ""), new StringString("GAUSSIAN94", ""),
        new StringString("GHEMICAL", "joelib.io.types.Ghemical"),
        new StringString("GIF", "joelib.io.types.GIF"),
        new StringString("GSTAT", ""),
        new StringString("GROMOS96A", "joelib.io.types.Gromos96A"),
        new StringString("GROMOS96N", "joelib.io.types.Gromos96A"),
        new StringString("HIN", "joelib.io.types.HIN"),
        new StringString("ICON8", ""), new StringString("IDATM", ""),
        new StringString("JAGUARIN", "joelib.io.types.Jaguar"),
        new StringString("JAGUAROUT", "joelib.io.types.Jaguar"),
        new StringString("JCAMP", "joelib.io.types.JCAMP"),
        new StringString("JPEG", "joelib.io.types.JPEG"),
        new StringString("MOL2", "joelib.io.types.SybylMol2"),
        new StringString("MM2IN", ""), new StringString("MM2OUT", ""),
        new StringString("MM3", ""), new StringString("MMADS", ""),
        new StringString("MMD", "joelib.io.types.MacroMode"),
        new StringString("MOLIN", ""), new StringString("MOLINVENT", ""),
        new StringString("MPQC", "joelib.io.types.MPQC"),
        new StringString("M3D", ""),
        new StringString("MOPACCART", "joelib.io.types.MopacCartesian"),
        new StringString("MOPACINT", ""),
        new StringString("MOPACOUT", "joelib.io.types.Mopac"),
        new StringString("MACCS", ""),
        new StringString("MATLAB", "joelib.io.types.Matlab"),
        new StringString("MSF", ""), new StringString("NWCHEMIN", ""),
        new StringString("NWCHEMOUT", ""),
        new StringString("JOEBINARY", "joelib.util.JOEBinaryIO "),
        new StringString("PREP", "joelib.io.types.Amber"),
        new StringString("PCMODEL", ""),
        new StringString("PDB", "joelib.io.types.PDB"),
        new StringString("PDF", "joelib.io.types.PDF"),
        new StringString("PNG", "joelib.io.types.PNG"),
        new StringString("POV", "joelib.io.types.POVRay"),
        new StringString("PPM", "joelib.io.types.PPM"),
        new StringString("QCHEMIN", "joelib.io.types.QChem"),
        new StringString("QCHEMOUT", "joelib.io.types.QChem"),
        new StringString("RDF", ""),
        new StringString("REPORT", "joelib.util.Report"),
        new StringString("SDF", "joelib.io.types.MDLSD"),
        new StringString("SMILES", "joelib.io.types.Smiles"),
        new StringString("SMIRKS", ""), new StringString("SCHAKAL", ""),
        new StringString("SHELX", ""), new StringString("SPARTAN", ""),
        new StringString("SPARTANSEMI", ""), new StringString("SPARTANMM", ""),
        new StringString("TITLE", "joelib.io.types.Title"),
        new StringString("TINKER", "joelib.io.types.Tinker"),
        new StringString("UNICHEM", "joelib.io.types.UniChem"),
        new StringString("XED", "joelib.io.types.XED"),
        new StringString("XYZ", "joelib.io.types.XYZ"),
        new StringString("ZIP", "joelib.io.types.ZIP")
    };
    private static final int DEFAULT_TYPES_NUMBER = 21;

    //~ Instance fields ////////////////////////////////////////////////////////

    private Hashtable inputExtensions;
    private Hashtable outputExtensions;
    private Hashtable typeHolder;
    private int typeNumber;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initializes the input/output holder factory.
     */
    private IOTypeHolder()
    {
        // initialize hash tables
        typeHolder = new Hashtable(DEFAULT_TYPES_NUMBER);

        // assume two extensions for each type
        inputExtensions = new Hashtable(DEFAULT_TYPES_NUMBER << 1);
        outputExtensions = new Hashtable(DEFAULT_TYPES_NUMBER << 1);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the instance of the input/output holder factory.
     *
     * @return the instance of the input/output holder factory
     */
    public static synchronized IOTypeHolder instance()
    {
        if (instance == null)
        {
            instance = new IOTypeHolder();
            instance.loadFileTypes();
        }

        return instance;
    }

    /**
     * Gets an enumeration with all available file types.
     *
     * @return    the enumeration with available file types
     */
    public Enumeration getFileTypes()
    {
        return typeHolder.elements();
    }

    /**
     *  Gets the IOType from a given input/output name.
     *  The name must be in upper case letters.
     *
     * @param  name  Description of the Parameter
     * @return       The iOType value
     */
    public IOType getIOType(String name)
    {
        IOType ioType = (IOType) typeHolder.get(name);

        // try to get io type
        if (ioType != null)
        {
            return ioType;
        }

        // try now with BIG letters (... ohhh, BIG hands ... only for Futurama fans)
        else
        {
            ioType = (IOType) typeHolder.get(name.toUpperCase());

            return ioType;
        }

        //    else return typeHolder.get("UNDEFINED");
        //    return null;
    }

    /**
     * Returns <tt>true</tt> if this input type is readable.
     *
     * @param  name  input type
     * @return       <tt>true</tt> if this input type is readable
     */
    public boolean isReadable(String name)
    {
        IOType ioType = (IOType) typeHolder.get(name);

        if (ioType == null)
        {
            return false;
        }
        else
        {
            MoleculeFileType mfType = null;

            try
            {
                mfType = JOEFileFormat.getMoleculeFileType(ioType);
            }
             catch (MoleculeIOException ex)
            {
                logger.error(ex.getMessage());

                return false;
            }

            if (mfType != null)
            {
                return mfType.readable();
            }

            return false;
        }
    }

    /**
     * Returns <tt>true</tt> if this output type is writeable.
     *
     * @param  name  output type
     * @return       <tt>true</tt> if this output type is writeable
     */
    public boolean isWriteable(String name)
    {
        IOType ioType = (IOType) typeHolder.get(name);

        if (ioType == null)
        {
            return false;
        }
        else
        {
            MoleculeFileType mfType = null;

            try
            {
                mfType = JOEFileFormat.getMoleculeFileType(ioType);
            }
             catch (MoleculeIOException ex)
            {
                logger.error(ex.getMessage());

                return false;
            }

            if (mfType != null)
            {
                return mfType.writeable();
            }

            return false;
        }
    }

    /**
     * Returns <tt>true</tt> if this file with the appropriate input type can be readed.
     *
     * @param  filename  the filename of the molecule file
     * @return           <tt>true</tt> if this file with the appropriate input type can be readed
     */
    public boolean canReadExtension(String filename)
    {
        Vector vs = new Vector();
        JHM.tokenize(vs, filename, ".\n\t");

        if (vs.size() == 0)
        {
            return false;
        }

        String ext = ((String) vs.get(vs.size() - 1)).toUpperCase();
        IOType ioType = (IOType) inputExtensions.get(ext);

        boolean read = false;

        if (ioType != null)
        {
            read = true;
        }

        return read;
    }

    /**
     * Returns <tt>true</tt> if this file with the appropriate output type can be written.
     *
     * @param  filename  the filename of the molecule file
     * @return           <tt>true</tt> if this file with the appropriate output type can be written
     */
    public boolean canWriteExtension(String filename)
    {
        Vector vs = new Vector();
        JHM.tokenize(vs, filename, ".\n\t");

        if (vs.size() == 0)
        {
            return false;
        }

        String ext = ((String) vs.get(vs.size() - 1)).toUpperCase();
        IOType ioType = (IOType) outputExtensions.get(ext);

        boolean write = false;

        if (ioType != null)
        {
            write = true;
        }

        return write;
    }

    /**
     * Gets an appropriate molecule input/output type for the given filename.
     *
     * @param  filename  the molecule filename
     * @return           the input/output type for the given filename
     */
    public IOType filenameToType(String filename)
    {
        Vector vs = new Vector();
        JHM.tokenize(vs, filename, ".\n\t");

        if (vs.size() == 0)
        {
            return null;
        }

        String ext = ((String) vs.get(vs.size() - 1)).toUpperCase();
        IOType ioType = (IOType) inputExtensions.get(ext);

        if (ioType != null)
        {
            return ioType;
        }
        else
        {
            ioType = (IOType) outputExtensions.get(ext);
        }

        if (ioType != null)
        {
            return ioType;
        }

        //System.out.println("extension:"+ext+" "+outputExtensions.get(ext));
        //try now slopyy match
        //get the first found entry with matching file extension
        for (Enumeration e = getFileTypes(); e.hasMoreElements();)
        {
            ioType = (IOType) e.nextElement();

            MoleculeFileType mfType = null;

            try
            {
                mfType = JOEFileFormat.getMoleculeFileType(ioType);
            }
             catch (MoleculeIOException ex)
            {
                logger.error(ex.getMessage());

                return null;
            }

            if (mfType != null)
            {
                String[] extensions = mfType.inputFileExtensions();

                if (extensions != null)
                {
                    for (int i = 0; i < extensions.length; i++)
                    {
                        if (ext.indexOf(extensions[i]) != -1)
                        {
                            return ioType;
                        }
                    }
                }

                extensions = mfType.outputFileExtensions();

                if (extensions != null)
                {
                    for (int i = 0; i < extensions.length; i++)
                    {
                        if (ext.indexOf(extensions[i]) != -1)
                        {
                            return ioType;
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Shows a table of all available input/output types.
     *
     * @return    the table of all available input/output types
     */
    public String toString()
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(10000);
        PrintfStream ps = new PrintfStream(baos);
        PrintfFormat s13 = new PrintfFormat("%-13s");
        PrintfFormat s25 = new PrintfFormat("%-25s");
        PrintfFormat d4 = new PrintfFormat("%4d");
        int index = 0;
        ps.println("     Name          Readable      Writeable     Description");
        ps.println("----------------------------------------------------------");

        IOType ioType;

        for (Enumeration e = getFileTypes(); e.hasMoreElements(); index++)
        {
            ioType = (IOType) e.nextElement();

            MoleculeFileType mfType = null;

            try
            {
                mfType = JOEFileFormat.getMoleculeFileType(ioType);
            }
             catch (MoleculeIOException ex)
            {
                logger.error(ex.getMessage());

                return null;
            }

            if (mfType != null)
            {
                ps.printf(d4, index);
                ps.print(' ');
                ps.printf(s13, ioType.getName());
                ps.print(' ');
                ps.printf(s13, "" + mfType.readable());
                ps.print(' ');
                ps.printf(s13, "" + mfType.writeable());
                ps.print(' ');

                if (mfType.inputDescription() != null)
                {
                    ps.printf(s25, mfType.inputDescription());
                }
                else if (mfType.outputDescription() != null)
                {
                    ps.printf(s25, mfType.outputDescription());
                }

                ps.printf(s25, "");
                ps.print(' ');

                if (e.hasMoreElements())
                {
                    ps.print('\n');
                }
            }
        }

        return baos.toString();
    }

    /**
     *  Description of the Method
     *
     * @param  types  Description of the Parameter
     * @return        Description of the Return Value
     */
    private boolean loadFileTypes()
    {
        String name;
        String representation;

        //    String descriptionFile;
        Properties prop = PropertyHolder.instance().getProperties();
        IOType ioType;
        MoleculeFileType mfType = null;
        String[] inExt;
        String[] outExt;

        // add UNDEFINED molecule loader
        ioType = new IOType(defaultTypes[0].s1, defaultTypes[0].s2, typeNumber++);
        typeHolder.put(defaultTypes[0].s1, ioType);

        // add other molecule types
        boolean allInfosLoaded = true;
        int i = 0;

        while (true)
        {
            i++;
            name = prop.getProperty("joelib.filetypes." + i + ".name");

            //      System.out.println("joelib.filetypes." + i + ".name:"+name);
            if (name == null)
            {
                logger.info("" + (i - 1) + " input/output types loaded.");

                break;
            }

            representation = prop.getProperty("joelib.filetypes." + i +
                    ".representation");

            // load default representation if no other representation was defined
            if (representation == null)
            {
                for (int j = 0; j < defaultTypes.length; j++)
                {
                    if (defaultTypes[j].s1.equals(name))
                    {
                        representation = defaultTypes[j].s2;
                    }
                }
            }

            if ((name != null) && (representation != null))
            {
                ioType = new IOType(name, representation, typeNumber);

                //check if IOType representation exists
                mfType = null;

                try
                {
                    mfType = JOEFileFormat.getMoleculeFileType(ioType);
                }
                 catch (MoleculeIOException ex)
                {
                    ex.printStackTrace();
                    System.exit(1);
                }

                if (mfType == null)
                {
                    // representation don't exist
                    continue;
                }
                else
                {
                    // o.k., representation exists
                    typeHolder.put(name, ioType);

                    // get input extensions
                    inExt = mfType.inputFileExtensions();

                    if (mfType.readable() && (inExt != null))
                    {
                        for (int m = 0; m < inExt.length; m++)
                        {
                            inputExtensions.put(inExt[m].toUpperCase(), ioType);
                        }
                    }

                    // get output extensions
                    outExt = mfType.outputFileExtensions();

                    if (mfType.writeable() && (outExt != null))
                    {
                        for (int m = 0; m < outExt.length; m++)
                        {
                            outputExtensions.put(outExt[m].toUpperCase(), ioType);
                        }
                    }

                    typeNumber++;
                }
            }
            else
            {
                allInfosLoaded = false;

                logger.error("File type " + name + " number " + i +
                    " not properly defined.");
            }
        }

        return allInfosLoaded;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
