///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: MoleculeIOException.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2004/07/25 20:43:19 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.io;


/**
 * Molecule IO exception, which will be thrown if the molecule entry is corrupt.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.6 $, $Date: 2004/07/25 20:43:19 $
 */
public class MoleculeIOException extends Exception
{
    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the MoleculeIOException object
     */
    public MoleculeIOException()
    {
        super();
    }

    /**
     *  Constructor for the MoleculeIOException object
     *
     * @param  s  Description of the Parameter
     */
    public MoleculeIOException(String s)
    {
        super(s);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
