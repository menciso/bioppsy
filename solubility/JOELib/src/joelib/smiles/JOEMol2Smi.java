///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOEMol2Smi.java,v $
//  Purpose:  Reader/Writer for SDF files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.22 $
//            $Date: 2004/07/29 10:16:49 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smiles;

import wsi.ra.tool.PropertyHolder;

import java.util.Vector;

import org.apache.log4j.Category;

import joelib.data.JOEElementTable;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.IsomerismDetection;
import joelib.util.JOEBitVec;
import joelib.util.iterator.AtomIterator;
import joelib.util.iterator.BondIterator;
import joelib.util.iterator.NbrAtomIterator;
import joelib.util.types.AtomIntInt;
import joelib.util.types.BondInt;
import joelib.util.types.IntInt;


/**
 * Molecule to SMILES methods.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.22 $, $Date: 2004/07/29 10:16:49 $
 */
public class JOEMol2Smi implements java.io.Serializable
{
    //~ Static fields/initializers /////////////////////////////////////////////

    /**
     * Obtain a suitable logger.
     */
    private static Category logger = Category.getInstance(
            "joelib.smiles.JOEMol2Smi");
    private final static boolean KEKULE = false;
    
    //~ Instance fields ////////////////////////////////////////////////////////

    private JOEBitVec _ubonds;
    private JOEBitVec visitedAtoms;

    /**
     * {@link java.util.Vector} of <tt>boolean[1]</tt>
     */
    private Vector _aromNH;

    /**
     * {@link java.util.Vector} of <tt>JOEBond</tt>
     */
    private Vector _vclose;

    /**
     * {@link java.util.Vector} of <tt>AtomIntInt</tt>
     */
    private Vector _vopen;

    /**
     * {@link java.util.Vector} of <tt>int[1]</tt>
     */
    private Vector atomOrdering;

    /**
     * {@link java.util.Vector} of <tt>int[1]</tt>
     */
    private Vector stereoOrdering;
    private boolean assignChirality = true;
    private boolean assignCisTrans = true;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *Constructor for the JOEMol2Smi object
     */
    public JOEMol2Smi()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Initialize " + this.getClass().getName());
        }

        atomOrdering = new Vector();
        stereoOrdering = new Vector();
        _aromNH = new Vector();
        _vclose = new Vector();
        _vopen = new Vector();

        visitedAtoms = new JOEBitVec();
        _ubonds = new JOEBitVec();

        String value = PropertyHolder.instance().getProperties().getProperty(JOEMol2Smi.class.getName() +
                ".assignCisTransInformations");

        if (((value != null) && value.equalsIgnoreCase("false")))
        {
            assignCisTrans = false;
        }
        else
        {
            assignCisTrans = true;
        }

        value = PropertyHolder.instance().getProperties().getProperty(JOEMol2Smi.class.getName() +
                ".assignChiralityInformations");

        if (((value != null) && value.equalsIgnoreCase("false")))
        {
            assignChirality = false;
        }
        else
        {
            assignChirality = true;
        }
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the chiralStereo attribute of the JOEMol2Smi object
     *
     * @param node    Description of the Parameter
     * @param smiles  Description of the Parameter
     * @return        The chiralStereo value
     */
    public boolean getChiralStereo(JOESmiNode node, StringBuffer smiles)
    {
        boolean is2D = false;
        double torsion;
        JOEAtom a;
        JOEAtom b;
        JOEAtom c;
        JOEAtom d;
        JOEAtom hydrogen = new JOEAtom();

        if (assignChirality == false)
        {
            return false;
        }

        b = node.getAtom();

        JOEMol mol = b.getParent();

        if (!mol.hasNonZeroCoords())
        {
            //must have come in from smiles string
            if (!b.hasChiralitySpecified())
            {
                return (false);
            }

            if (b.isClockwise())
            {
                smiles.append("@@");
            }
            else if (b.isAntiClockwise())
            {
                smiles.append("@");
            }
            else
            {
                return (false);
            }

            //if (b.getHvyValence() == 3) smiles.append("H");
            return (true);
        }

        //give peudo Z coords if mol is 2D
        if (!mol.has3D())
        {
            XYZVector v = new XYZVector();
            XYZVector vz = new XYZVector(0.0, 0.0, 1.0);
            is2D = true;

            JOEAtom nbr;
            JOEBond bond;
            BondIterator bit = b.bondIterator();

            while (bit.hasNext())
            {
                bond = bit.nextBond();
                nbr = bond.getEndAtom();

                if (nbr != b)
                {
                    v = nbr.getVector();

                    if (bond.isWedge())
                    {
                        v.adding(vz);
                    }
                    else if (bond.isHash())
                    {
                        v.subing(vz);
                    }

                    nbr.setVector(v);
                }
                else
                {
                    nbr = bond.getBeginAtom();
                    v = nbr.getVector();

                    if (bond.isWedge())
                    {
                        v.subing(vz);
                    }
                    else if (bond.isHash())
                    {
                        v.adding(vz);
                    }

                    nbr.setVector(v);
                }
            }
        }

        c = d = null;
        a = node.getParent();

        //  oeAssert(a);
        //    assert a==null;//chiral atom can't be used as root node - must have parent
        if (b.getHvyValence() == 3)
        {
            //must have attached hydrogen
            if (b.getValence() == 4)
            {
                //has explicit hydrogen
                NbrAtomIterator nait = b.nbrAtomIterator();

                while (nait.hasNext())
                {
                    c = nait.nextNbrAtom();

                    if (c.isHydrogen())
                    {
                        break;
                    }
                }

                //  assert c==null;
            }
            else
            {
                //implicit hydrogen
                XYZVector v = new XYZVector();
                b.getNewBondVector(v, 1.0);
                hydrogen.setVector(v);
                c = hydrogen;
            }
        }

        //get connected atoms in order
        JOEAtom nbr;

        //try to get neighbors that are closure atoms in the order they appear in the string
        Vector va = new Vector();

        // of type JOEAtom
        getClosureAtoms(b, va);

        JOEAtom atom;

        if (va.size() != 0)
        {
            for (int k = 0; k < va.size(); k++)
            {
                atom = (JOEAtom) va.get(k);

                if (atom != a)
                {
                    if (c == null)
                    {
                        c = atom;
                    }
                    else if (d == null)
                    {
                        d = atom;
                    }
                }
            }
        }

        int[] itmp;

        for (int j = 0; j < stereoOrdering.size(); j++)
        {
            itmp = (int[]) stereoOrdering.get(j);
            nbr = mol.getAtom(itmp[0]);

            if (!b.isConnected(nbr))
            {
                continue;
            }

            if ((nbr == a) || (nbr == b) || (nbr == c))
            {
                continue;
            }

            if (c == null)
            {
                c = nbr;
            }
            else if (d == null)
            {
                d = nbr;
            }
        }

        torsion = XYZVector.calcTorsionAngle(a.getVector(), b.getVector(),
                c.getVector(), d.getVector());

        smiles.append((torsion < 0.0) ? "@" : "@@");

        //if (b.getHvyValence() == 3) smiles.append("H");
        //re-zero psuedo-coords
        if (is2D)
        {
            XYZVector v;
            AtomIterator ait = mol.atomIterator();

            while (ait.hasNext())
            {
                atom = ait.nextAtom();
                v = atom.getVector();
                v.setZ(0.0);
                atom.setVector(v);
            }
        }

        return (true);
    }

    /**
     * @param va    {@link java.util.Vector} of <tt>JJOEAtom</tt>
     * @param atom  Description of the Parameter
     */
    public void getClosureAtoms(JOEAtom atom, Vector va)
    {
        //look through closure list for start atom
        JOEBond bond;

        for (int i = 0; i < _vclose.size(); i++)
        {
            bond = (JOEBond) _vclose.get(i);

            if (bond != null)
            {
                if (bond.getBeginAtom() == atom)
                {
                    va.add(bond.getEndAtom());
                }

                if (bond.getEndAtom() == atom)
                {
                    va.add(bond.getBeginAtom());
                }
            }
        }

        JOEAtom nbr;
        AtomIntInt aii;
        NbrAtomIterator nait = atom.nbrAtomIterator();

        for (int j = 0; j < _vopen.size(); j++)
        {
            aii = (AtomIntInt) _vopen.get(j);
            nait.reset();

            while (nait.hasNext())
            {
                nbr = nait.nextNbrAtom();

                if (nbr == aii.a)
                {
                    va.add(nbr);
                }
            }
        }
    }

    /**
     * @param atom  Description of the Parameter
     * @return      {@link java.util.Vector} of <tt>BondInt</tt>
     */
    public Vector getClosureDigits(JOEAtom atom)
    {
        Vector vc = new Vector();

        // of type BondInt
        //look through closure list for start atom
        int idx;

        // of type BondInt
        //look through closure list for start atom
        int bo;
        JOEBond bond;

        //        System.out.println("size"+_vclose.size());
        for (int i = 0; i < _vclose.size(); i++)
        {
            bond = (JOEBond) _vclose.get(i);

            if (bond != null)
            {
                //                System.out.println("check: "+bond.getBO());
                if ((bond.getBeginAtom() == atom) ||
                        (bond.getEndAtom() == atom))
                {
                    idx = getUnusedIndex();
                    vc.add(new BondInt(bond, idx));
                    bo = (bond.isAromatic()) ? 1 : bond.getBO();
                    _vopen.add(new AtomIntInt(bond.getNbrAtom(atom),
                            new IntInt(idx, bo)));

                    //                    _vclose.set(i, null);
                    _vclose.remove(i--);

                    //remove bond from closure list
                }
            }
        }

        //try to complete closures
        if (_vopen.size() != 0)
        {
            AtomIntInt aii;

            for (int j = 0; j < _vopen.size();)
            {
                aii = (AtomIntInt) _vopen.get(j);

                if (aii.a == atom)
                {
                    vc.add(new BondInt(null, aii.ii.i1));
                    _vopen.remove(j);
                    j = 0;
                }
                else
                {
                    j++;
                }
            }
        }

        return (vc);
    }

    /**
     * @return   {@link java.util.Vector} of <tt>int[1]</tt>
     */
    public Vector getOutputOrder()
    {
        return (atomOrdering);
    }

    /**
     * Gets the smilesElement attribute of the JOEMol2Smi object
     *
     * @param node    Description of the Parameter
     * @param smiles  Description of the Parameter
     * @return        The smilesElement value
     */
    public boolean getSmilesElement(JOESmiNode node, StringBuffer smiles)
    {
        //***handle reference atom stuff here and return***
        char[] symbol;
        boolean bracketElement = false;
        boolean normalValence = true;

        JOEAtom atom = node.getAtom();

        int bosum = atom.KBOSum();

        switch (atom.getAtomicNum())
        {
        case 0:
            break;

        case 5:

            /*bracketElement = !(normalValence = (bosum == 3)); break; */
            break;

        case 6:
            break;

        case 7:

            if (atom.isAromatic() && (atom.getHvyValence() == 2) &&
                    (atom.getImplicitValence() == 3))
            {
                bracketElement = !(normalValence = false);

                break;
            }
            else
            {
                bracketElement = !(normalValence = ((bosum == 3) ||
                        (bosum == 5)));
            }

            break;

        case 8:
            break;

        case 9:
            break;

        case 15:
            break;

        case 16:
            bracketElement = !(normalValence = ((bosum == 2) || (bosum == 4) ||
                    (bosum == 6)));

            break;

        case 17:
            break;

        case 35:
            break;

        case 53:
            break;

        default:
            bracketElement = true;
        }

        if ((atom.getHvyValence() > 2) && atom.isChiral())
        {
            if (atom.getParent().hasNonZeroCoords() ||
                    atom.hasChiralitySpecified())
            {
                bracketElement = true;
            }
        }

        if (atom.getFormalCharge() != 0)
        {
            //bracket charged elements
            bracketElement = true;
        }

        if (!bracketElement)
        {
            if (atom.getAtomicNum() == 0)
            {
                boolean external = false;

                //never happens !!!!
                //old code
                //	  Vector externalBonds = atom.getParent().getData("extBonds");
                //	  if (externalBonds!=null)
                //	    for(externalBond = externalBonds->begin();externalBond != externalBonds->end();externalBond++)
                //	      {
                //		if (externalBond->second.first == atom)
                //		  {
                //		    external = true;
                //		    smiles.append("&");
                //		    JOEBond bond = externalBond->second.second;
                //		    if (bond.isUp())                              smiles.append("\\");
                //		    if (bond.isDown())                            smiles.append("/");
                //if(!KEKULE)
                //{
                //		    if (bond.getBO() == 2 && !bond.isAromatic()) smiles.append("=");
                //		    if (bond.getBO() == 2 && bond.isAromatic())  smiles.append(";");
                //}else{
                //		    if (bond.getBO() == 2)                        smiles.append("=");
                //}
                //		    if (bond.getBO() == 3)                        smiles.append("#");
                //		    smiles.append(externalBond->first);
                //		    break;
                //		  }
                //	      }
                if (!external)
                {
                    smiles.append("*");
                }
            }
            else
            {
                symbol = (JOEElementTable.instance()
                                         .getSymbol(atom.getAtomicNum())).toCharArray();

                if (!KEKULE)
                {
                    if (atom.isAromatic())
                    {
                        symbol[0] = Character.toLowerCase(symbol[0]);
                    }
                }

                smiles.append(symbol);
            }

            return (true);
        }

        smiles.append("[");

        if (atom.getAtomicNum() == 0)
        {
            smiles.append("*");
        }
        else
        {
            symbol = (JOEElementTable.instance().getSymbol(atom.getAtomicNum())).toCharArray();

            if (!KEKULE)
            {
                if (atom.isAromatic())
                {
                    symbol[0] = Character.toLowerCase(symbol[0]);
                }
            }

            smiles.append(symbol);
        }

        //if (atom.isCarbon() && atom.getHvyValence() > 2 && atom.isChiral())
        if ((atom.getHvyValence() > 2) && atom.isChiral())
        {
            //char stereo[5];
            //if (getChiralStereo(node,stereo))	  strcat(element,stereo);
            getChiralStereo(node, smiles);
        }

        //add extra hydrogens
        //  if (!normalValence && atom.implicitHydrogenCount())
        if (atom.implicitHydrogenCount() != 0)
        {
            smiles.append("H");

            if (atom.implicitHydrogenCount() > 1)
            {
                // char tcount[10];
                // sprintf(tcount,"%d",atom.implicitHydrogenCount());
                // strcat(element,tcount);
                smiles.append(atom.implicitHydrogenCount());
            }
        }

        //cat charge on the end
        if (atom.getFormalCharge() != 0)
        {
            //            /*
            //             *if (atom.implicitHydrogenCount())
            //             *{
            //             *logger.error("imp = "+atom.getAtomicNum()+" "+atom.getImplicitValence());
            //             *smiles.append("H");
            //             *if (atom.implicitHydrogenCount() > 1)
            //             *{
            //             */char tcount[10];
            //             */sprintf(tcount,"%d",atom.implicitHydrogenCount());
            //             */strcat(element,tcount);
            //             *smiles.append( atom.implicitHydrogenCount() );
            //             *}
            //             *}
            //             */
            if (atom.getFormalCharge() > 0)
            {
                smiles.append("+");
            }
            else
            {
                smiles.append("-");
            }

            if (Math.abs(atom.getFormalCharge()) > 1)
            {
                //char tcharge[10];
                //sprintf(tcharge,"%d",abs(atom.getFormalCharge()));
                //strcat(element,tcharge);
                smiles.append(Math.abs(atom.getFormalCharge()));
            }
        }

        smiles.append("]");

        return (true);
    }

    /**
     * Gets the unusedIndex attribute of the JOEMol2Smi object
     *
     * @return   The unusedIndex value
     */
    public int getUnusedIndex()
    {
        int idx = 1;

        AtomIntInt aii;

        for (int j = 0; j < _vopen.size();)
        {
            aii = (AtomIntInt) _vopen.get(j);

            if (aii.ii.i1 == idx)
            {
                idx++;

                //increment idx and start over if digit is already used
                j = 0;
            }
            else
            {
                j++;
            }
        }

        return (idx);
    }

    /**
     * Traverse the tree searching for acyclic olefins and assign
     * stereochemistry if it has at least one heavy atom attachment
     * on each end.
     *
     * @param node  Description of the Parameter
     */
    public void assignCisTrans(JOESmiNode node)
    {
        if (assignCisTrans == false)
        {
            return;
        }

        JOEBond bond;

        for (int i = 0; i < node.size(); i++)
        {
            bond = node.getNextBond(i);

            // allow consitency with all other auto-detection methods 
            IsomerismDetection.isCisTransBond(bond, true);

            // this single specialized code causes inconsitencies with
            // other code snippets
            /*            // handle only double bonds
                        if ((bond.getBO() == 2) && !bond.isInRing())
                        {
                                // get atoms of this double bond
                            JOEAtom b = node.getAtom();
                            JOEAtom c = bond.getNbrAtom(b);
                            //System.out.println("atoms b="+b.getIdx()+" c="+c.getIdx());

                            //skip allenes
                            if ((b.getHyb() == 1) || (c.getHyb() == 1))
                            {
                                continue;
                            }

                            if ((b.getHvyValence() > 1) && (c.getHvyValence() > 1))
                            {
                                JOEAtom a = null;
                                JOEAtom d = null;

                                //look for bond with assigned stereo as in poly-ene
                                NbrAtomIterator nait = b.nbrAtomIterator();

                                boolean upDownFound=false;
                                while (nait.hasNext())
                                {
                                    a = nait.nextNbrAtom();

                                    if (nait.actualBond().isUp() ||
                                            nait.actualBond().isDown())
                                    {
                                            upDownFound=true;
                                        break;
                                    }
                                }

                                if (!upDownFound)
                                {
                                    nait.reset();

                                    while (nait.hasNext())
                                    {
                                        a = nait.nextNbrAtom();

                                               if ((a != c) && !a.isHydrogen())
                                        {
                                            break;
                                        }
                                    }
                                }

                                NbrAtomIterator nait2 = c.nbrAtomIterator();

                                while (nait2.hasNext())
                                {
                                    d = nait2.nextNbrAtom();

                                    if ((d != b) && !d.isHydrogen())
                                    {
                                        break;
                                    }
                                }

                                //System.out.println("atoms: a="+a.getIdx()+" d="+d.getIdx());

                                //              assert a==null;
                                //              assert d==null;
                                if (nait.actualBond().isUp() || nait.actualBond().isDown())
                                {
                                    //stereo already assigned
                                    if (Math.abs(XYZVector.calcTorsionAngle(a.getVector(),
                                                    b.getVector(), c.getVector(),
                                                    d.getVector())) > 10.0)
                                    {
                                        if (nait.actualBond().isUp())
                                        {
                                            nait2.actualBond().setDown();
                                        }
                                        else
                                        {
                                            nait2.actualBond().setUp();
                                        }
                                    }
                                    else if (nait.actualBond().isUp())
                                    {
                                        nait2.actualBond().setUp();
                                    }
                                    else
                                    {
                                        nait2.actualBond().setDown();
                                    }
                                }
                                else
                                {
                                    //assign stereo to both ends
                                    nait.actualBond().setUp();

                                    if (Math.abs(XYZVector.calcTorsionAngle(a.getVector(),
                                                    b.getVector(), c.getVector(),
                                                    d.getVector())) > 10.0)
                                    {
                                        nait2.actualBond().setDown();
                                    }
                                    else
                                    {
                                        nait2.actualBond().setUp();
                                    }
                                }
                            }
                        }
            */
            assignCisTrans(node.getNextNode(i));
        }
    }

    /**
     * Description of the Method
     *
     * @param node  Description of the Parameter
     * @return      Description of the Return Value
     */
    public boolean buildTree(JOESmiNode node)
    {
        JOEAtom nbr;
        JOEAtom atom = node.getAtom();

        //        System.out.println("atom:"+atom.getIdx());
        //mark the atom as visited
        visitedAtoms.setBitOn(atom.getIdx());

        //store the atom ordering
        atomOrdering.add(new int[]{atom.getIdx()});

        //store the atom ordering for stereo
        stereoOrdering.add(new int[]{atom.getIdx()});

        NbrAtomIterator nait = atom.nbrAtomIterator();

        while (nait.hasNext())
        {
            nbr = nait.nextNbrAtom();

            //            System.out.println("NBR:"+nbr.getIdx());
            //            if (!nbr->IsHydrogen() && !visitedAtoms[nbr->GetIdx()])
            //            {
            //		cout<<"_ubonds.SetBitOn("<<(*i)->GetIdx()<<")"<<endl;
            //			_ubonds.SetBitOn((*i)->GetIdx());
            //			OESmiNode *next = new OESmiNode (nbr);
            //			next->SetParent(atom);
            //			node->SetNextNode(next,*i);
            //			BuildTree(next);
            //            }
            if (!nbr.isHydrogen() && !visitedAtoms.get(nbr.getIdx()))
            {
                //                System.out.println("_ubonds.setBitOn("+nait.actualBond().getIdx()+")");
                _ubonds.setBitOn(nait.actualBond().getIdx());

                JOESmiNode next = new JOESmiNode(nbr);
                next.setParent(atom);
                node.setNextNode(next, nait.actualBond());
                buildTree(next);
            }
        }

        return (true);
    }

    /**
     * Description of the Method
     *
     * @param mol  Description of the Parameter
     */
    public void correctAromaticAmineCharge(JOEMol mol)
    {
        _aromNH.clear();
        _aromNH.ensureCapacity(mol.numAtoms() + 1);

        for (int i = 0; i <= mol.numAtoms(); i++)
        {
            _aromNH.add(new boolean[]{false});
        }

        JOEAtom atom;
        AtomIterator ait = mol.atomIterator();
        boolean[] btmp;

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            if (atom.isNitrogen() && atom.isAromatic())
            {
                if (atom.getHvyValence() == 2)
                {
                    if ((atom.getValence() == 3) ||
                            (atom.getImplicitValence() == 3))
                    {
                        btmp = (boolean[]) _aromNH.get(atom.getIdx());
                        btmp[0] = true;
                    }
                }
            }
        }
    }

    /**
     * Description of the Method
     *
     * @param mol     Description of the Parameter
     * @param smiles  Description of the Parameter
     */
    public void createSmiString(JOEMol mol, StringBuffer smiles)
    {
        JOEAtom atom;
        JOESmiNode root;

        //    buffer[0] = '\0';
        //		BondIterator bit = mol.bondIterator();
        //		JOEBond bond;
        //		while (bit.hasNext())
        //		{
        //			bond = bit.nextBond();
        //			//    System.out.println("BOND:"+bond.getIdx()+" "+bond.getBeginAtomIdx()+" "+bond.getEndAtomIdx());
        //		}
        //  System.out.println("createSmiString");
        AtomIterator ait = mol.atomIterator();

        while (ait.hasNext())
        {
            atom = ait.nextAtom();

            //    if ((!atom.isHydrogen() || atom.getValence() == 0) && !visitedAtoms[atom.getIdx()])
            if (!atom.isHydrogen() && !visitedAtoms.get(atom.getIdx()))
            {
                if (!atom.isChiral())
                {
                    //don't use chiral atoms as root node
                    //                    System.out.println("ATOM:"+atom.getIdx());
                    //clear out closures in case structure is dot disconnected
                    _vclose.clear();
                    atomOrdering.clear();
                    stereoOrdering.clear();
                    _vopen.clear();

                    //dot disconnected structure
                    if (smiles.length() > 0)
                    {
                        smiles.append(".");
                    }

                    root = new JOESmiNode(atom);
                    buildTree(root);
                    findClosureBonds(mol);

                    if (mol.has2D())
                    {
                        assignCisTrans(root);
                    }

                    toSmilesString(root, smiles);
                    root = null;
                }
            }
        }
    }

    /**
     * Description of the Method
     */
    public void finalize()
    {
    }

    /**
     * Description of the Method
     *
     * @param mol  Description of the Parameter
     */
    public void findClosureBonds(JOEMol mol)
    {
        //find closure bonds
        JOEAtom a1;

        //find closure bonds
        JOEAtom a2;
        JOEBond bond;
        JOEBitVec bv = new JOEBitVec();
        bv.fromVectorWithIntArray(stereoOrdering);

        BondIterator bit = mol.bondIterator();

        while (bit.hasNext())
        {
            bond = bit.nextBond();

            //            System.out.println("!_ubonds.get("+bond.getIdx()+"):"+(!_ubonds.get(bond.getIdx()) ));
            //            System.out.println("bv.get("+bond.getBeginAtomIdx()+"):"+bv.get(bond.getBeginAtomIdx()) );
            if (!_ubonds.get(bond.getIdx()) && bv.get(bond.getBeginAtomIdx()))
            {
                a1 = bond.getBeginAtom();
                a2 = bond.getEndAtom();

                if (!a1.isHydrogen() && !a2.isHydrogen())
                {
                    //                  System.out.print("add");
                    _vclose.add(bond);
                }
            }
        }

        //modify _order to reflect ring closures
        int[] itmp;

        for (int j = _vclose.size() - 1; j >= 0; j--)
        {
            bond = (JOEBond) _vclose.get(j);
            a1 = a2 = null;

            for (int k = 0; k < stereoOrdering.size(); k++)
            {
                itmp = (int[]) stereoOrdering.get(k);

                if ((bond.getBeginAtomIdx() == itmp[0]) ||
                        (bond.getEndAtomIdx() == itmp[0]))
                {
                    //System.out.println("found "+itmp[0]+" of "+bond.getBeginAtomIdx()+" and "+bond.getEndAtomIdx());
                    if (a1 == null)
                    {
                        a1 = mol.getAtom(itmp[0]);
                    }
                    else if (a2 == null)
                    {
                        a2 = mol.getAtom(itmp[0]);
                        stereoOrdering.remove(k);

                        break;
                    }
                }
            }

            for (int k = 0; k < stereoOrdering.size(); k++)
            {
                itmp = (int[]) stereoOrdering.get(k);

                if (a1.getIdx() == itmp[0])
                {
                    k++;

                    // vector<int>::iterator k;
                    if (k != (stereoOrdering.size() - 1))
                    {
                        stereoOrdering.insertElementAt(new int[]{a2.getIdx()}, k);
                    }
                    else
                    {
                        stereoOrdering.add(new int[]{a2.getIdx()});
                    }

                    break;
                }
            }
        }
    }

    //public void         correctAromaticAmineCharge(JOEMol mol)
    //  {
    //    JOEAtom atom;
    //
    //  _aromNH.clear();
    //  _aromNH.ensureCapacity(mol.numAtoms()+1);
    //
    //  AtomIterator ait = this.atomIterator();
    //  boolean btmp[];
    //  while(ait.hasNext())
    //  {
    //    atom = ait.nextAtom();
    //     if (atom.isNitrogen() && atom.isAromatic())
    //     {
    //      if (atom.getHvyValence() == 2)
    //	{
    //	  if (atom.getValence() == 3 || atom.getImplicitValence() == 3)
    //          {
    //            btmp = (boolean[])_aromNH.get(atom.getIdx());
    //	    btmp[0] = true;
    //          }
    //	}
    //    }
    //  }
    //
    //  }

    /**
     * Description of the Method
     */
    public void init()
    {
        _vclose.clear();
        atomOrdering.clear();
        stereoOrdering.clear();
        _aromNH.clear();
        visitedAtoms.clear();
        _ubonds.clear();
        _vopen.clear();
    }

    /**
     * Description of the Method
     */
    public void removeUsedClosures()
    {
    }

    /**
     * Description of the Method
     *
     * @param node    Description of the Parameter
     * @param smiles  Description of the Parameter
     */
    public void toSmilesString(JOESmiNode node, StringBuffer smiles)
    {
        //      char tmpbuf[10];
        JOEAtom atom = node.getAtom();

        //write the current atom to the string
        getSmilesElement(node, smiles);

        //handle ring closures here
        Vector vc = getClosureDigits(atom);

        // ot type BondInt
        if (vc.size() != 0)
        {
            BondInt bi;

            for (int i = 0; i < vc.size(); i++)
            {
                bi = (BondInt) vc.get(i);

                if (bi.bond != null)
                {
                    if (assignCisTrans)
                    {
                        if (bi.bond.isUp())
                        {
                            smiles.append(JOESmilesParser.UP_BOND_FLAG);
                        }

                        if (bi.bond.isDown())
                        {
                            smiles.append(JOESmilesParser.DOWN_BOND_FLAG);
                        }
                    }

                    if (!KEKULE)
                    {
                        //                      System.out.println("isArom:"+bi.bond.isAromatic());
                        if ((bi.bond.getBO() == 2) && !bi.bond.isAromatic())
                        {
                            smiles.append("=");
                        }
                    }
                    else
                    {
                        if (bi.bond.getBO() == 2)
                        {
                            smiles.append("=");
                        }
                    }

                    if (bi.bond.getBO() == 3)
                    {
                        smiles.append("#");
                    }
                }

                if (bi.i > 9)
                {
                    smiles.append("%");
                }

                smiles.append(bi.i);

                //                System.out.println("Ring'"+bi.i+"'");
            }
        }

        //follow path to child atoms
        JOEBond bond;

        for (int i = 0; i < node.size(); i++)
        {
            bond = node.getNextBond(i);

            if ((i + 1) < node.size())
            {
                smiles.append("(");
            }

            if (assignCisTrans)
            {
                if (bond.isUp())
                {
                    smiles.append(JOESmilesParser.UP_BOND_FLAG);
                }

                if (bond.isDown())
                {
                    smiles.append(JOESmilesParser.DOWN_BOND_FLAG);
                }
            }

            if (!KEKULE)
            {
                if ((bond.getBO() == 2) && !bond.isAromatic())
                {
                    smiles.append("=");
                }
            }
            else
            {
                if (bond.getBO() == 2)
                {
                    smiles.append("=");
                }
            }

            if (bond.getBO() == 3)
            {
                smiles.append("#");
            }

            toSmilesString(node.getNextNode(i), smiles);

            if ((i + 1) < node.size())
            {
                smiles.append(")");
            }
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
