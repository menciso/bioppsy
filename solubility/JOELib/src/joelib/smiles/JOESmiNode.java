///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOESmiNode.java,v $
//  Purpose:  Reader/Writer for SDF files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2004/07/25 20:43:26 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smiles;

import java.util.Vector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;


/**
 * SMILES node holding atom and bond informations.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/07/25 20:43:26 $
 */
public class JOESmiNode implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private JOEAtom _atom;
    private JOEAtom _parent;

    /**
     * {@link java.util.Vector} of <tt>JOEBond</tt>
     */
    private Vector _nextbond;

    /**
     * {@link java.util.Vector} of <tt>JOESmiNode</tt>
     */
    private Vector _nextnode;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *Constructor for the JOESmiNode object
     *
     * @param atom  Description of the Parameter
     */
    public JOESmiNode(JOEAtom atom)
    {
        _atom = atom;
        _parent = null;
        _nextnode = new Vector();
        _nextbond = new Vector();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Gets the atom attribute of the JOESmiNode object
     *
     * @return   The atom value
     */
    public JOEAtom getAtom()
    {
        return (_atom);
    }

    /**
     * Gets the nextAtom attribute of the JOESmiNode object
     *
     * @param i  Description of the Parameter
     * @return   The nextAtom value
     */
    public JOEAtom getNextAtom(int i)
    {
        return ((JOESmiNode) _nextnode.get(i)).getAtom();
    }

    /**
     * Gets the nextBond attribute of the JOESmiNode object
     *
     * @param i  Description of the Parameter
     * @return   The nextBond value
     */
    public JOEBond getNextBond(int i)
    {
        return ((JOEBond) _nextbond.get(i));
    }

    /**
     * Sets the nextNode attribute of the JOESmiNode object
     *
     * @param node  The new nextNode value
     * @param bond  The new nextNode value
     */
    public void setNextNode(JOESmiNode node, JOEBond bond)
    {
        _nextnode.add(node);
        _nextbond.add(bond);
    }

    /**
     * Gets the nextNode attribute of the JOESmiNode object
     *
     * @param i  Description of the Parameter
     * @return   The nextNode value
     */
    public JOESmiNode getNextNode(int i)
    {
        return ((JOESmiNode) _nextnode.get(i));
    }

    /**
     * Sets the parent attribute of the JOESmiNode object
     *
     * @param a  The new parent value
     */
    public void setParent(JOEAtom a)
    {
        _parent = a;
    }

    /**
     * Gets the parent attribute of the JOESmiNode object
     *
     * @return   The parent value
     */
    public JOEAtom getParent()
    {
        return (_parent);
    }

    /**
     * Description of the Method
     */
    public void finalize()
    {
        for (int i = 0; i < _nextnode.size(); i++)
        {
            _nextnode.set(i, null);
        }

        _nextnode.clear();
    }

    /**
     * Description of the Method
     *
     * @return   Description of the Return Value
     */
    public int size()
    {
        return _nextnode.size();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
