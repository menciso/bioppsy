///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: ClosureBond.java,v $
//  Purpose:  Closure bond of an SMILES expression.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.7 $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.smiles;


/**
 *  Closure bond of an SMILES expression.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.7 $, $Date: 2004/07/25 20:43:26 $
 */
public class ClosureBond implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     * Flags of the bond.
     */
    public int bondflags;

    /**
     * Number of the closure bond.
     */
    public int closureNumber;

    /**
     * Order.
     */
    public int order;

    /**
     * Index of the previous atom.
     */
    public int previous;

    /**
     * Valence of the previous atom.
     */
    public int valence;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Initialize the <tt>ClosureBond</tt>
     *
     * @param _closureNumber  Number of the closure bond
     * @param _previous       Index of the previous atom
     * @param _order          Order
     * @param _bondflags      Flags of the bond
     * @param _valence        Valence of the previous atom
     */
    public ClosureBond(int _closureNumber, int _previous, int _order,
        int _bondflags, int _valence)
    {
        closureNumber = _closureNumber;
        previous = _previous;
        order = _order;
        bondflags = _bondflags;
        valence = _valence;
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
