///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOERingSearch.java,v $
//  Purpose:  Find the smallest set of smallest rings.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.13 $
//            $Date: 2004/07/25 20:43:25 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.ring;

import wsi.ra.tool.Deque;
import wsi.ra.tool.DequeIterator;
import wsi.ra.tool.DequeNode;

import java.util.Vector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;
import joelib.molecule.JOERTree;

import joelib.sort.QuickInsertSort;

import joelib.util.JHM;
import joelib.util.JOEBitVec;
import joelib.util.iterator.BondIterator;
import joelib.util.iterator.RingIterator;


/**
 * Finds the Smallest Set of Smallest Rings (SSSR).
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.13 $, $Date: 2004/07/25 20:43:25 $
 * @cite fig96
 */
public class JOERingSearch implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
      *  {@link java.util.Vector} of <tt>JOEBond</tt>
      */
    //private Vector _bonds;

    /**
     *  {@link java.util.Vector} of <tt>JOERing</tt>
     */
    private Vector _rlist;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOERingSearch object
     */
    public JOERingSearch()
    {
        //_bonds = new Vector();
        _rlist = new Vector();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     *  Gets the ringIterator attribute of the JOERingSearch object
     *
     * @return    The ringIterator value
     */
    public RingIterator getRingIterator()
    {
        return new RingIterator(_rlist);
    }

    /**
     *  Adds a feature to the RingFromClosure attribute of the JOERingSearch
     *  object
     *
     * @param  mol    The feature to be added to the RingFromClosure attribute
     * @param  cbond  The feature to be added to the RingFromClosure attribute
     * @param  level  The feature to be added to the RingFromClosure attribute
     */
    public void addRingFromClosure(JOEMol mol, JOEBond cbond, int level)
    {
        //Vector t1 = new Vector(mol.numAtoms()+1 );  // of type JOERTree
        //Vector t2 = new Vector(mol.numAtoms()+1 );  // of type JOERTree
        JOERTree[] t1 = new JOERTree[mol.numAtoms() + 1];
        JOERTree[] t2 = new JOERTree[mol.numAtoms() + 1];
        JOEBitVec bv1 = new JOEBitVec();
        JOEBitVec bv2 = new JOEBitVec();

        bv1.setBitOn(cbond.getEndAtomIdx());

        //    System.out.println("end"+cbond.getEndAtomIdx());
        bv2.setBitOn(cbond.getBeginAtomIdx());

        //    System.out.println("beg"+cbond.getBeginAtomIdx());
        JHM.buildOERTreeVector(cbond.getBeginAtom(), null, t1, bv1);
        JHM.buildOERTreeVector(cbond.getEndAtom(), null, t2, bv2);

        boolean pathok;

        Deque p1 = new Deque();
        Deque p2 = new Deque();

        // of type int[1]
        Vector path1 = new Vector();
        Vector path2 = new Vector();

        // of type JOEAtom
        JOERTree rtTmp1;

        // of type JOEAtom
        JOERTree rtTmp2;

        for (int i = 0; i < t1.length; i++)
        {
            rtTmp1 = t1[i];

            //(JOERTree)t1.get(i);
            if (rtTmp1 != null)
            {
                path1.clear();
                rtTmp1.pathToRoot(path1);

                rtTmp2 = t2[rtTmp1.getAtomIdx()];

                //(JOERTree)t2.get(rtTmp1.getAtomIdx());
                if (rtTmp2 != null)
                {
                    //          System.out.println("rtTmp2-idx:"+rtTmp1.getAtomIdx());
                    pathok = true;
                    path2.clear();
                    rtTmp2.pathToRoot(path2);

                    p1.removeAll();

                    JOEAtom atom_m;
                    atom_m = (JOEAtom) path1.get(0);

                    //          System.out.println(""+p1.size());
                    if (atom_m != (JOEAtom) path1.get(path1.size() - 1))
                    {
                        p1.pushBack(new int[]{atom_m.getIdx()});
                    }

                    //          System.out.println(""+p1.size());
                    //          System.out.println("*********\npath1("+path1.size()+")");
                    //          for (int m=0; m< path1.size();m++)
                    //      	  {
                    //            System.out.print(" "+((JOEAtom)path1.get(m)).getIdx());
                    //          }
                    //          System.out.println("");
                    JOEAtom atom_n;

                    for (int m = 1; m < path1.size(); m++)
                    {
                        atom_m = (JOEAtom) path1.get(m);
                        p1.pushBack(new int[]{atom_m.getIdx()});

                        //            System.out.println("p1.pushBack(new int[]{atom_m.getIdx()}"+atom_m.getIdx()+" );");
                        //            System.out.println("path2("+path2.size()+")");
                        //            for (int n=0; n< path2.size();n++)
                        //            {
                        //              System.out.print(" "+((JOEAtom)path2.get(n)).getIdx());
                        //            }
                        //            System.out.println("");
                        p2.removeAll();

                        for (int n = 1; n < path2.size(); n++)
                        {
                            atom_n = (JOEAtom) path2.get(n);
                            p2.pushFront(new int[]{atom_n.getIdx()});

                            //                  System.out.println(""+n+" p2.pushFront(new int[]{atom_n.getIdx()}"+atom_n.getIdx()+" );");
                            if (atom_n == atom_m)
                            {
                                //don't traverse across identical atoms
                                //                    System.out.println("n=m");
                                p2.popFront();

                                //                    System.out.println("p1s: "+p1.size()+" p2s: "+p2.size());
                                if ((p1.size() + p2.size()) > 2)
                                {
                                    saveUniqueRing(p1, p2);
                                }

                                pathok = false;

                                break;
                            }

                            if (atom_n.isConnected(atom_m) &&
                                    ((p1.size() + p2.size()) > 2))
                            {
                                saveUniqueRing(p1, p2);
                            }
                        }

                        //            System.out.println("pathok:"+pathok+" i"+i);
                        if (!pathok)
                        {
                            break;
                        }
                    }
                }
            }
        }

        //clean up JOERTree vectors
        int i;

        for (i = 0; i < t1.length; i++)
        {
            t1[i] = null;
        }

        for (i = 0; i < t2.length; i++)
        {
            t2[i] = null;
        }
    }

//    public void finalize()
//    {
//        for (int i = 0; i < _rlist.size(); i++)
//        {
//            Object obj = _rlist.get(i);
//            obj = null;
//        }
//    }

    /**
     * @param  path    {@link java.util.Vector} of <tt>int[1]</tt>
     * @param  mol     Description of the Parameter
     * @param  avisit  Description of the Parameter
     * @param  bvisit  Description of the Parameter
     * @param  natom   Description of the Parameter
     * @param  depth   Description of the Parameter
     */
    public static void findRings(JOEMol mol, Vector path, JOEBitVec avisit,
        JOEBitVec bvisit, int natom, int depth)
    {
        JOEAtom atom;
        JOEBond bond;
        int[] itmp;

        //  System.out.println("avisit.get(natom="+natom+") "+avisit.get(natom)+"depth "+depth);
        if (avisit.get(natom))
        {
            int j = depth - 1;

            itmp = (int[]) path.get(j--);
            bond = mol.getBond(itmp[0]);

            //    System.out.println("mol.getBond(itmp[0]);"+itmp[0]);
            bond.setInRing();

            while (j >= 0)
            {
                itmp = (int[]) path.get(j--);
                bond = mol.getBond(itmp[0]);

                bond.setInRing();
                (bond.getBeginAtom()).setInRing();
                (bond.getEndAtom()).setInRing();

                //          System.out.println("bg || end == natom("+natom+")"+(bond.getBeginAtomIdx()==natom)+" "+(bond.getEndAtomIdx()==natom));
                if ((bond.getBeginAtomIdx() == natom) ||
                        (bond.getEndAtomIdx() == natom))
                {
                    break;
                }
            }
        }
        else
        {
            avisit.setBitOn(natom);
            atom = mol.getAtom(natom);

            BondIterator bit = atom.bondIterator();

            while (bit.hasNext())
            {
                bond = bit.nextBond();

                if (!bvisit.get(bond.getIdx()))
                {
                    itmp = (int[]) path.get(depth);
                    itmp[0] = bond.getIdx();

                    //          System.out.println("((int[])path.get(depth="+depth+"))[0]=bond.getIdx()="+bond.getIdx());
                    bvisit.setBitOn(bond.getIdx());
                    findRings(mol, path, avisit, bvisit,
                        bond.getNbrAtomIdx(atom), depth + 1);
                }
            }
        }
    }

    /**
     *  Description of the Method
     *
     * @param  frj  Description of the Parameter
     */
    public void removeRedundant(int frj)
    {
        JOEBitVec tmp = new JOEBitVec();
        int i;
        int j;

        //    System.out.println("rsize:"+_rlist.size());
        //remove identical rings
        for (i = _rlist.size() - 1; i > 0; i--)
        {
            for (j = i - 1; j >= 0; j--)
            {
                if (((JOERing) _rlist.get(i))._pathset == ((JOERing) _rlist.get(
                            j))._pathset)
                {
                    _rlist.remove(i);

                    break;
                }
            }
        }

        //    System.out.println("rsizeb:"+_rlist.size());
        //make sure tmp is the same size as the rings
        for (j = 0; j < _rlist.size(); j++)
        {
            tmp.set((JOEBitVec) ((JOERing) _rlist.get(j))._pathset);
        }

        //    System.out.println("rsizec:"+_rlist.size());
        //remove larger rings that cover the same atoms as smaller rings
        //JOEBitVec pathset_i;

        for (i = _rlist.size() - 1; i >= 0; i--)
        {
            //pathset_i = ((JOERing) _rlist.get(i))._pathset;

            tmp.clear();

            for (j = 0; j < _rlist.size(); j++)
            {
                if ((((JOERing) _rlist.get(j))._path.size() <= ((JOERing) _rlist.get(
                            i))._path.size()) && (i != j))
                {
                    tmp.orSet(((JOERing) _rlist.get(j))._pathset);
                }
            }

            tmp.andSet(((JOERing) _rlist.get(i))._pathset);

            if (tmp.equals(((JOERing) _rlist.get(i))._pathset))
            {
                _rlist.remove(i);
            }

            //      System.out.println("s"+_rlist.size()+"=frj"+frj+" "+(_rlist.size() == frj));
            if (_rlist.size() == frj)
            {
                break;
            }
        }

        //    System.out.println("rsized:"+_rlist.size());
    }

    /**
     * @param  d1  <tt>Deque</tt> of <tt>int[1]</tt>
     * @param  d2  <tt>Deque</tt> of <tt>int[1]</tt>
     * @return     Description of the Return Value
     */
    public boolean saveUniqueRing(Deque d1, Deque d2)
    {
        Vector path = new Vector();

        // of type int[1]
        JOEBitVec bv = new JOEBitVec();
        int[] itmp;

        //    System.out.println("*****");
        DequeIterator di1 = d1.getDequeIterator();
        DequeNode dNode;

        while (di1.hasNext())
        {
            dNode = (DequeNode) di1.next();
            itmp = (int[]) dNode.key;
            bv.setBitOn(itmp[0]);
            path.add(itmp);

            //      System.out.println("d1.set:"+itmp[0]);
        }

        DequeIterator di2 = d2.getDequeIterator();

        while (di2.hasNext())
        {
            dNode = (DequeNode) di2.next();
            itmp = (int[]) dNode.key;
            bv.setBitOn(itmp[0]);
            path.add(itmp);

            //      System.out.println("d2.set:"+itmp[0]);
        }

        RingIterator rit = new RingIterator(_rlist);
        JOERing ring;

        //    int c=0;
        while (rit.hasNext())
        {
            ring = rit.nextRing();

            //      System.out.println("c: "+c+bv.equals(ring._pathset));
            if (bv.equals(ring._pathset))
            {
                return false;
            }

            //      c++;
        }

        ring = new JOERing();
        ring._path = path;
        ring._pathset = bv;
        _rlist.add(ring);

        return true;
    }

    /*-------------------------------------------------------------------------*
     * public methods
     *------------------------------------------------------------------------- */

    /**
     *  Description of the Method
     */
    public void sortRings()
    {
        QuickInsertSort sorting = new QuickInsertSort();

        RingSizeComparator ringSizeComparator = new RingSizeComparator();
        sorting.sort(_rlist, ringSizeComparator);
    }

    /**
     *  Description of the Method
     */
    public void writeRings()
    {
        for (int i = 0; i < _rlist.size(); i++)
        {
            System.out.println(((JOERing) _rlist.get(i))._pathset);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
