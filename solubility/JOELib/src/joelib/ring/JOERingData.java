///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOERingData.java,v $
//  Purpose:  Stores ring data.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.12 $
//            $Date: 2004/07/25 20:43:25 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.ring;

import java.util.Vector;

import joelib.data.JOEDataType;
import joelib.data.JOEGenericData;


/**
 * Stores ring data.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.12 $, $Date: 2004/07/25 20:43:25 $
 */
public class JOERingData extends JOEGenericData implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  {@link java.util.Vector} of <tt>JOERing</tt>
     */
    protected Vector _vr;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOERingData object
     */
    public JOERingData()
    {
        dataType = JOEDataType.JOE_RING_DATA;
        this.setAttribute(dataType.getDefaultAttr());

        //_attr = "RingData";
        //	        _vr.clear();
        _vr = new Vector();
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * @param  vr  {@link java.util.Vector} of <tt>JOERing</tt>
     */
    public void setData(Vector vr)
    {
        _vr = vr;
    }

    /**
     * @return    {@link java.util.Vector} of <tt>JOERing</tt>
     */
    public Vector getData()
    {
        return (_vr);
    }

    /**
     *  Description of the Method
     *
     * @param  r  Description of the Parameter
     */
    public void add(JOERing r)
    {
        _vr.add(r);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
