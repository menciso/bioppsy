///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: JOERing.java,v $
//  Purpose:  Ring representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.16 $
//            $Date: 2004/07/25 20:43:25 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package joelib.ring;

import java.util.Vector;

import joelib.math.XYZVector;

import joelib.molecule.JOEAtom;
import joelib.molecule.JOEBond;
import joelib.molecule.JOEMol;

import joelib.util.JOEBitVec;


/**
 * Ring representation.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.16 $, $Date: 2004/07/25 20:43:25 $
 */
public class JOERing implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /**
     *  Description of the Field
     */
    public JOEBitVec _pathset;

    /**
     *  {@link java.util.Vector} of <tt>int[1]</tt>
     */
    public Vector _path;
    private JOEMol _parent;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     *  Constructor for the JOERing object
     */
    public JOERing()
    {
    }

    /**
     * @param  path  {@link java.util.Vector} of <tt>int[1]</tt>
     * @param  size  Description of the Parameter
     */
    public JOERing(Vector path, int size)
    {
        _path = path;
        _pathset = new JOEBitVec(size);
        _pathset.fromVectorWithIntArray(_path);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Returns <tt>true</tt> if this ring is a aromatic ring.
     *
     * @return    <tt>true</tt> if this ring is a aromatic ring
     */
    public boolean isAromatic()
    {
        JOEMol mol = _parent;

        int[] itmp;

        for (int i = 0; i < _path.size(); i++)
        {
            itmp = (int[]) _path.get(i);

            if (!(mol.getAtom(itmp[0])).isAromatic())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns all atom indexes of atoms which are contained in this ring.
     *
     * @return    atom indexes of atoms which are contained in this ring
     */
    public int[] getAtoms()
    {
        int[] tmp = new int[_path.size()];

        for (int i = 0; i < _path.size(); i++)
        {
            tmp[i] = ((int[]) _path.get(i))[0];
        }

        return tmp;
    }

    public int[] getBonds()
    {
        JOEBond bond;
        int numBonds = 0;

        for (int i = 0; i < _parent.numBonds(); i++)
        {
            bond = _parent.getBond(i);

            if ((_pathset.bitIsOn(bond.getBeginAtomIdx())) &&
                    (_pathset.bitIsOn(bond.getEndAtomIdx())))
            {
                numBonds++;
            }
        }

        int[] bonds = new int[numBonds];
        int index = 0;

        if (numBonds != 0)
        {
            for (int i = 0; i < _parent.numBonds(); i++)
            {
                bond = _parent.getBond(i);

                if ((_pathset.bitIsOn(bond.getBeginAtomIdx())) &&
                        (_pathset.bitIsOn(bond.getEndAtomIdx())))
                {
                    bonds[index] = bond.getIdx();
                    index++;
                }
            }
        }

        return bonds;
    }

    /**
     * Returns <tt>true</tt> if this ring is a heterocycle.
     *
     * @return <tt>true</tt> if this ring is a heterocycle
     */
    public boolean isHetero()
    {
        JOEMol mol = _parent;

        int[] itmp;

        for (int i = 0; i < _path.size(); i++)
        {
            itmp = (int[]) _path.get(i);

            if ((mol.getAtom(itmp[0])).isHeteroatom())
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns <tt>true</tt> if the atom with index <tt>i</tt> is contained in this ring.
     *
     * @param  i  the index number of the atom
     * @return    <tt>true</tt> if the atom with index <tt>i</tt> is contained in this ring.
     */
    public boolean isInRing(int i)
    {
        return _pathset.bitIsOn(i);
    }

    /**
     *  Sets the parent attribute of the JOERing object
     *
     * @param  m  The new parent value
     */
    public void setParent(JOEMol m)
    {
        _parent = m;
    }

    /**
     * Returns the parent molecule for this ring.
     *
     * @return    the parent molecule for this ring
     */
    public JOEMol getParent()
    {
        return _parent;
    }

    /**
     *  Description of the Method
     *
     * @param  center  Description of the Parameter
     * @param  norm1   Description of the Parameter
     * @param  norm2   Description of the Parameter
     * @return         Description of the Return Value
     */
    public boolean findCenterAndNormal(XYZVector center, XYZVector norm1,
        XYZVector norm2)
    {
        JOEMol mol = this._parent;
        int j = 0;
        final int nA = this._path.size();
        XYZVector tmp = new XYZVector();
        int[] itmp;

        center.set(0.0f, 0.0f, 0.0f);
        norm1.set(0.0f, 0.0f, 0.0f);
        norm2.set(0.0f, 0.0f, 0.0f);

        for (j = 0; j < nA; j++)
        {
            itmp = (int[]) _path.get(j);
            center.adding((mol.getAtom(itmp[0])).getVector());
        }

        center.diving((double) nA);

        XYZVector v1 = new XYZVector();
        XYZVector v2 = new XYZVector();

        for (j = 0; j < nA; j++)
        {
            itmp = (int[]) _path.get(j);
            v1.set((mol.getAtom(itmp[0])).getVector().sub(center));
            itmp = (int[]) _path.get(((j + 1) == nA) ? 0 : (j + 1));
            v2.set((mol.getAtom(itmp[0])).getVector().sub(center));
            XYZVector.cross(tmp, v1, v2);
            norm1.adding(tmp);
        }

        norm1.diving(nA);
        norm1.normalize();
        norm2 = norm1.setTo(norm2);
        norm2.muling(-1.0f);

        return true;
    }

    /**
     * Returns the size of the ring.
     *
     * @return   the size of this ring
     */
    public final int pathSize()
    {
        return (_path.size());
    }

    /**
     * Returns the size of this ring.
     *
     * @return    the size of this ring
     */
    public final int size()
    {
        return (_path.size());
    }

    /**
     * Returns <tt>true</tt> if the atom is contained in this ring.
     *
     * @param  a  the atom
     * @return    <tt>true</tt> if the atom is contained in this ring.
     */
    public boolean isMember(JOEAtom a)
    {
        return _pathset.bitIsOn(a.getIdx());
    }

    /**
     * Returns <tt>true</tt> if the bond is contained in this ring.
     *
     * @param  b  the bond to check
     * @return    <tt>true</tt> if the bond is contained in this ring
     */
    public boolean isMember(JOEBond b)
    {
        return (_pathset.bitIsOn(b.getBeginAtomIdx())) &&
        (_pathset.bitIsOn(b.getEndAtomIdx()));
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        int[] tmp = getAtoms();
        sb.append("<");

        for (int i = 0; i < tmp.length; i++)
        {
            sb.append(tmp[i]);

            if (i < (tmp.length - 1))
            {
                sb.append(",");
            }
        }

        sb.append(">");

        return sb.toString();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
