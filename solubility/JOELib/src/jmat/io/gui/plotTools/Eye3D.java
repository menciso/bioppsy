package jmat.io.gui.plotTools;


/**
 * DOCUMENT ME!
 *
 * @author $author$
 * @version $Revision: 1.2 $
 */
public class Eye3D
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private double phi;
    private double theta;

    //~ Constructors ///////////////////////////////////////////////////////////

    public Eye3D()
    {
        theta = Math.PI / 4;
        phi = Math.PI / 4;
    }

    public Eye3D(double t, double p)
    {
        theta = t;
        phi = p;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public double getPhi()
    {
        return phi;
    }

    public double getTheta()
    {
        return theta;
    }

    public Object clone()
    {
        return new Eye3D(theta, phi);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
