package jmat.data;

import jmat.io.data.TextFile;
import jmat.io.data.fileTools.MatrixString;

import jmat.io.gui.FrameView;
import jmat.io.gui.TextWindow;

import javax.swing.JPanel;


/**
<P>
   The Text Class is just designed to provide easy-to-use string operations
   like building log files, displaying text in a window, converting matrix to String format...

@author Yann RICHET
@version 2.0
*/
public class Text implements java.io.Serializable
{
    //~ Instance fields ////////////////////////////////////////////////////////

    /* ------------------------
       Class variables
    * ------------------------ */

    /** String for internal storage.
    @serial internal string storage.
    */
    private String string = new String("");

    //~ Constructors ///////////////////////////////////////////////////////////

    /* ------------------------
       Constructor
     * ------------------------ */

    /** Construct a text.
    @param str  String of the text.
    */
    public Text(String str)
    {
        setString(str);
    }

    /** Construct a text.
    @param X  Matrix to convert in text.
    */
    public Text(Matrix X)
    {
        setString(X);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /** Provides access to the string of the text.
    @param str  String of the text.
    */
    public void setString(String str)
    {
        string = str;
    }

    public void setString(Matrix X)
    {
        string = MatrixString.printMatrix(X);
    }

    /* ------------------------
       Public Methods
     * ------------------------ */

    /** Provides access to the string of the text.
    @return String of the text.
    */
    public String getString()
    {
        String s = new String(string);

        return s;
    }

    /** Load the text from a file.
    @param fileName    fileName.
    @return Text.
    */
    public static Text fromFile(String fileName)
    {
        TextFile mf = new TextFile(fileName);

        return mf.getText();
    }

    /** Save the text from a file.
    @param file    file.
    @return Text.
    */
    public static Text fromFile(java.io.File file)
    {
        TextFile mf = new TextFile(file);

        return mf.getText();
    }

    /** Merge the two Texts.
    @param text    text to merge.
    */
    public void merge(Text text)
    {
        string = new String(string + text.getString());
    }

    /** Merge the Matrix.
    @param X    Matrix to merge.
    */
    public void merge(Matrix X)
    {
        Text text = new Text(X);
        string = new String(string + text.getString());
    }

    /** Merge the string.
    @param s    String to merge.
    */
    public void merge(String s)
    {
        string = new String(string + s);
    }

    /** Print the Text in the Command Line.
    */
    public void toCommandLine()
    {
        System.out.println(string);
    }

    /** Save the Text in a file.
    @param fileName    fileName.
    */
    public void toFile(String fileName)
    {
        TextFile mf = new TextFile(fileName, this);
        mf = null;
    }

    /** Save the text in a file.
    @param file    file.
    */
    public void toFile(java.io.File file)
    {
        TextFile mf = new TextFile(file, this);
        mf = null;
    }

    /** Display the text in a Window in a Frame.
    @param title Title of the JFrame.
    */
    public void toFrame(String title)
    {
        FrameView fv = new FrameView(title, toPanel());
        fv = null;
    }

    /** Display the text in a Window.
    @return JPanel.
    */
    public JPanel toPanel()
    {
        return new TextWindow(this);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
