///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DequeNode.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.3 $
//            $Date: 2003/08/19 13:11:30 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package wsi.ra.tool;


/*============================================================================
 * IMPORTS
 *============================================================================*/
import java.lang.*;


/*============================================================================
 *  CLASS DECLARATION
 *============================================================================*/

/**
 * Deque node representation.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.3 $, $Date: 2003/08/19 13:11:30 $
 */
public class DequeNode extends java.lang.Object
{
    //~ Instance fields ////////////////////////////////////////////////////////

    public DequeNode next;
    public DequeNode prev;

    /*-------------------------------------------------------------------------*
     * public member variables
     *-------------------------------------------------------------------------*/
    public Object key;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*------------------------------------------------------------------------*
     * constructor / destructor
     *------------------------------------------------------------------------*/
    public DequeNode(DequeNode p, Object key, DequeNode suc)
    {
        this.key = key;
        next = suc;

        if (next != null)
        {
            next.prev = this;
        }

        prev = p;

        if (prev != null)
        {
            prev.next = this;
        }
    }

    public DequeNode(DequeNode p, Object key)
    {
        this(p, key, null);
    }

    public DequeNode(Object key)
    {
        this(null, key, null);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
