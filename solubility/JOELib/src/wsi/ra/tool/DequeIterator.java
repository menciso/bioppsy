///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DequeIterator.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.4 $
//            $Date: 2003/08/19 13:11:30 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package wsi.ra.tool;


/*============================================================================
 * IMPORTS
 *============================================================================*/
import java.lang.*;

import java.util.*;


/*============================================================================
 *  CLASS DECLARATION
 *============================================================================*/

/**
 * Iterator class for the list.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.4 $, $Date: 2003/08/19 13:11:30 $
 */
public class DequeIterator implements Iterator
{
    //~ Instance fields ////////////////////////////////////////////////////////

    protected Deque l;

    /*-------------------------------------------------------------------------*
     * protected member variables
     *-------------------------------------------------------------------------*/
    protected DequeNode current;

    //~ Constructors ///////////////////////////////////////////////////////////

    /*------------------------------------------------------------------------*
     * constructor / destructor
     *------------------------------------------------------------------------*/
    public DequeIterator(Deque l)
    {
        current = l.head;
        this.l = l;
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------*/
    public boolean hasNext()
    {
        return current != null;
    }

    public Object next()
    {
        Object o = current;
        current = current.next;

        return o;
    }

    public void remove()
    {
        if (current.prev == null)
        {
            return;
        }

        l.remove(current.prev);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
