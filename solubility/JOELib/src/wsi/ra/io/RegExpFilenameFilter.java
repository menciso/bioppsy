///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: RegExpFilenameFilter.java,v $
//  Purpose:  Regular expression filter for files.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2004/07/25 20:43:30 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package wsi.ra.io;

import java.io.File;
import java.io.FilenameFilter;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Regular expression filter for files.
 */
public class RegExpFilenameFilter implements FilenameFilter
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private Pattern pattern;
    private Vector skipIfExtension;

    //~ Constructors ///////////////////////////////////////////////////////////

    public RegExpFilenameFilter(String regExp)
    {
        pattern = Pattern.compile(regExp);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public boolean accept(File dir, String name)
    {
        Matcher m;
        m = pattern.matcher(name);

        if (m.matches())
        {
            if (skipIfExtension != null)
            {
                int size = skipIfExtension.size();

                for (int i = 0; i < size; i++)
                {
                    if (name.endsWith((String) skipIfExtension.get(i)))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

    public void addSkipExtension(String extension)
    {
        if (skipIfExtension == null)
        {
            skipIfExtension = new Vector();
        }

        skipIfExtension.add(extension);
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
