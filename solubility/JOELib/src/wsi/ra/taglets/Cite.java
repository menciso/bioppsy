///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Cite.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.10 $
//            $Date: 2003/08/22 15:56:22 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package wsi.ra.taglets;

import java.util.*; // Used in register(Map)

import com.sun.javadoc.*; // Doclet API
import com.sun.tools.doclets.Taglet; // Taglet API


/**
 * A Taglet that defines the <code>@cite</code> tag for Javadoc
 * comments.
 *
 * -J-Dwsi.ra.taglets.Cite.path=D:/workingAt/joelib/src/<br>
 * -J-Dwsi.ra.taglets.Cite.file=literature.html<br>
 * or under Ant:<br>
 * additionalparam=" -J-Dwsi.ra.taglets.Cite.path=D:/workingAt/joelib/src/ -J-Dwsi.ra.taglets.Cite.file=literature.html "<br>
 *
 * @author Joerg K. Wegner
 */
public class Cite extends ListTag
{
    //~ Instance fields ////////////////////////////////////////////////////////

    private String _file;
    private String _path;

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Create a new ListTag, with tag name 'todo'.  Default
     * the tag header to 'To Do:' and default to an
     * unordered list.
     *
     * @todo a single todo entry
     */
    public Cite()
    {
        super("cite", "References:", ListTag.ORDERED_LIST);

        String clName = this.getClass().getName();
        _path = System.getProperty(clName + ".path");

        if (_path == null)
        {
            System.err.println("WARNING: System property " + clName + ".path" +
                " must be defined.");
        }
        else
        {
            //			System.out.println(
            //				"INFO: " + "Cite taglet uses base path: " + _path);
            _path = _path.toUpperCase();
            _path = _path.replace('\\', '/');
        }

        _file = System.getProperty(clName + ".file", "literature.html");

        //System.out.println(System.getProperties());
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    public static void register(Map tagletMap)
    {
        ListTag.register(tagletMap, new Cite());
    }

    public boolean isInlineTag()
    {
        return true;
    }

    public String toString(Tag tag)
    {
        // Use relative path ONLY, when this is not a *.HTM file
        // The package.HTMl files are still a problem, but what can we do
        // without writting an own doclet ???
        // Use workaround in combination with ant and
        // copy all literature.html files to that directories,
        // which will use inline citation in package summary
        if (tag.position().file().toString().toUpperCase().indexOf(".HTM") != -1)
        {
            return getCiteTag(tag, true);
        }
        else
        {
            return getCiteTag(tag, false);
        }
    }

    public String toString(Tag[] tags)
    {
        if (tags.length == 0)
        {
            return "";
        }

        StringBuffer sbuf = new StringBuffer(200 + (800 * tags.length));

        startingTags();

        emitHeader(sbuf, true);

        for (int i = 0; i < tags.length; i++)
        {
            emitTag(getCiteTag(tags[i], false), sbuf, true);
        }

        emitFooter(sbuf, true);

        endingTags(sbuf);

        return sbuf.toString();
    }

    /**
     * Method getCiteTag.
     *
     * @param tag
     * @return String
     */
    protected final String getCiteTag(final Tag tag, boolean inline)
    {
        String path2use = "";

        if (_path != null)
        {
            String actfile = tag.position().file().toString();
            actfile = actfile.toUpperCase();
            actfile = actfile.replace('\\', '/');

            int index = actfile.indexOf(_path);
            String sub = actfile.substring(index + _path.length());
            StringTokenizer t = new StringTokenizer(sub, "/");
            int num = t.countTokens();

            //System.out.println("num:"+num);
            StringBuffer path = new StringBuffer(num * 4);

            if (!inline)
            {
                for (int i = 1; i < num; i++)
                {
                    path.append("../");
                }
            }

            path2use = path.toString();
        }
        else
        {
            return tag.text();
        }

        String cite = tag.text();
        String ref = "#";

        if (cite == null)
        {
            System.err.println("WARNING: Could not find reference " +
                tag.text() + " in ???");
        }
        else
        {
            return "<a href=\"" + path2use + _file + ref + cite + "\">" + cite +
            "</a>";
        }

        return "";
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
