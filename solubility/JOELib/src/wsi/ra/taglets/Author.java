///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: Author.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.11 $
//            $Date: 2004/07/25 20:43:30 $
//            $Author: wegner $
//  Original Author: Patrick Tullmann <taglets@tullmann.org>
//  Original Version: ???
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package wsi.ra.taglets;

import java.util.HashMap;
import java.util.Map; // Used in register(Map)

import com.sun.javadoc.*; // Doclet API
import com.sun.tools.doclets.Taglet; // Taglet API


/**
 * A Taglet that defines the <code>@author</code> tag for Javadoc
 * comments.
 */
public class Author extends ListTag
{
    //~ Static fields/initializers /////////////////////////////////////////////

    private static final HashMap authorTextMap = new HashMap(5);

    static
    {
        authorTextMap.put("wegner",
            "J&ouml;rg Kurt Wegner (wegnerj at informatik.uni-tuebingen.de) at the <a href=\"http://www-ra.informatik.uni-tuebingen.de\" target=\"_top\">Department of Computer Architecture</a>");
        authorTextMap.put("wegnerj",
            "J&ouml;rg Kurt Wegner (wegnerj at informatik.uni-tuebingen.de) at the <a href=\"http://www-ra.informatik.uni-tuebingen.de\" target=\"_top\">Department of Computer Architecture</a>");
        authorTextMap.put("abolmaal",
            "Seyed Foad Badreddin Abolmaali (abolmaal at informatik.uni-tuebingen.de) at the <a href=\"http://www-ra.informatik.uni-tuebingen.de\" target=\"_top\">Department of Computer Architecture</a>");
        authorTextMap.put("rapp", "Fred Rapp (fred_rapp at yahoo.com)");
        authorTextMap.put("egonw",
            "<a href=\"http://www.openscience.org/~egonw/\" target=\"_top\">Egon Willighagen</a> (<a href=\"mailto:egonw@sci.kun.nl\">egonw@sci.kun.nl</A>)");
        authorTextMap.put("John E. Lloyd",
            "<a href=\"http://www.cs.ubc.ca//~lloyd/index.html\" target=\"_top\">John E. Lloyd</a>");
        authorTextMap.put("Stephen Jelfs",
            "Stephen Jelfs (<a href=\"mailto:s.jelfs@sheffield.ac.uk\">s.jelfs@sheffield.ac.uk</A>) at <a href=\"http://cisrg.shef.ac.uk\" target=\"_top\">ChemoInformatics at Sheffield</a>");
        authorTextMap.put("Serguei Patchkovskii",
            "Serguei Patchkovskii (<a href=\"mailto:Serguei.Patchkovskii@sympatico.ca\">Serguei.Patchkovskii@sympatico.ca</a>)");
        authorTextMap.put("steinbeck",
            "Christoph Steinbeck (<a href=\"mailto:steinbeck@ice.mpg.de\">steinbeck@ice.mpg.de</a>)");
    }

    //~ Constructors ///////////////////////////////////////////////////////////

    /**
     * Create a new License tag.
     */
    public Author()
    {
        super("author", "Author:", ListTag.TABLE_LIST);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    /**
     * Register this taglet with the given name.
     */
    public static void register(Map tagletMap)
    {
        ListTag.register(tagletMap, new Author());
    }

    public String toString(Tag tag)
    {
        StringBuffer sbuf = new StringBuffer(1000);

        // XXX make it an option to emit single entries with the list header/etc.
        startingTags();

        emitHeader(sbuf, false);
        emitTag(getAuthorTag(tag), sbuf, false);
        emitFooter(sbuf, false);

        endingTags(sbuf);

        return sbuf.toString();
    }

    public String toString(Tag[] tags)
    {
        if (tags.length == 0)
        {
            return "";
        }

        StringBuffer sbuf = new StringBuffer(200 + (800 * tags.length));

        startingTags();

        emitHeader(sbuf, true);

        for (int i = 0; i < tags.length; i++)
        {
            emitTag(getAuthorTag(tags[i]), sbuf, true);
        }

        emitFooter(sbuf, true);

        endingTags(sbuf);

        return sbuf.toString();
    }

    public String toString()
    {
        return this.getClass().getName() +
        " which contains also a list of predefined authors (including E-mail adress).";
    }

    protected final String getAuthorTag(final Tag tag)
    {
        String expLicense = (String) authorTextMap.get(tag.text());

        if (expLicense == null)
        {
            return tag.text();
        }
        else
        {
            return expLicense;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
