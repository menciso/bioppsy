///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: DecimalFormatter.java,v $
//  Purpose:  Descriptor base class.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg K. Wegner
//  Version:  $Revision: 1.2 $
//            $Date: 2003/08/19 13:11:30 $
//            $Author: wegner $
//
//  Copyright (c) Dept. Computer Architecture, University of Tuebingen, Germany
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package wsi.ra.text;


/*==========================================================================*
 * IMPORTS
 *==========================================================================  */
/*==========================================================================*
 * INTERFACE DECLARATION
 *==========================================================================  */

/**
 * Interface for defining a decimal formatter.
 *
 * @author     wegnerj
 * @license GPL
 * @cvsversion    $Revision: 1.2 $, $Date: 2003/08/19 13:11:30 $
 */
public interface DecimalFormatter
{
    //~ Methods ////////////////////////////////////////////////////////////////

    /*-------------------------------------------------------------------------*
     * private static member variables
     *-------------------------------------------------------------------------  */
    /*-------------------------------------------------------------------------*
     * public methods
     *-------------------------------------------------------------------------  */

    /**
     * A unit test for JUnit
     */
    public String format(double value);
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
