# Version:  $Revision: 1.32 $
#           $Date: 2004/08/27 09:30:30 $

Change Log :
---------------------------------------------------------------------------
- 2004-08-27 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/src/joelib/desc/SimpleStringAtomProperty.java
     Added calculations class for String based atom properties

   - joelib/src/joelib/desc/result/StringArrayResult.java
     Added result class for String based arrays

   - joelib/src/joelib/desc/result/AtomStringResult.java
     Added result class for String based atom properties

   - joelib/src/joelib/desc/types/AtomType.java
     Added JOELib internal atom type as String based atom property descriptor

   - joelib/src/joelib/desc/types/AtomInAromaticSystem.java
     Added atom property descriptor for aromatic status of atoms

   - joelib/src/joelib/util/JOEPropertyHelper.java
     Bug fix: Fixed null pointer exception

   - joelib/src/joelib/desc/types/atompair/TopologicalAtomPair.java
     Bug fix: Fixed null pointer exception for parameter initalization

   - joelib/src/joelib/util/LineArrayHelper.java
     joelib/src/joelib/util/ArrayHelper.java
     Added String based storing and loading methods for String arrays

- 2004-07-29 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/src/joelib/smarts/ParseSmart.java
     Bug fix: AGAIN! SMARTS encoder writes Stereo-Up as '\' and
     Stereo-Down as '/' but the specification says it vise versa.
     And added unspecific matching using '\?' and '/?', please remember that
     the implementation in JOELib requires C/?C=C/?C to match CC=CC
     sucessfully

- 2004-07-25 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - Partial code cleanup.

   - Added formal definition of a molecular graph to the DocBook-Tutorial.

- 2004-07-07 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/src/joelib/io/MDLSD.java
     Added dummy atom support for 'Du' entries.

   - joelib/src/joelib/data/plain/types.txt
     Internal representation of dummy atoms changed from 'X' to 'Xx'.
     Now we are conform with joelib/src/joelib/data/plain/element.txt

- 2004-07-02 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/src/joelib/smarts/ParseSmart.java
     Cis/trans detection was replaced by IsomerismDetection.isCisTransBond.

   - joelib/src/joelib/util/IsomerismDetection.java
     Cis/trans detection from 2D/3D contains now up/up flags for cis and
     up/down flags for trans bonds.

- 2004-07-02 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/src/joelib/smarts/ParseSmart.java
     Added cis/trans support or more correctly up/down detection ability.

   - joelib/src/joelib/smiles/JOEMol2Smi.java
     Bug fix: Wrong detection of the 'a' atom in a 'abcd' cis/trans pattern
     corrected.

   - joelib/src/resources/smartsEvaluation/cis_ and trans_butene.mol
     joelib/src/resources/smartsEvaluation/evaluation.txt
     Added testing methods for MDL SDF import with cis/trans autodetection
     and then SMARTS matching.

- 2004-06-29 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/src/joelib/desc/DescriptorHelper.java
     Bug fix for handling empty DescResult and avoiding to calculate it
     twice.
     
   - joelib/src/joelib/smarts/ParseSmart.java
     Bug fix: SMARTS encoder writes Stereo-Up as '\' and Stereo-Down as '/'
     but the specification says it vise versa.
     
- 2004-06-21 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/src/joelib/io/types/MDLSD.java
     Stabelized kekule-writting mode. Uses now directly isKSingle/isKDouble/
     isKTriple instead of mol.kekulize().
     
   - wsi/ra/PropertyHolder.java
     Allow additional joelib.properties in user.home/.joelib-directory.
     If found, this will used, otherwise the one in the classpath/jar-file.

- 2004-06-15 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/src/joelib/smarts/test/SMARTSEvaluation.java
     SMARTS matching test class to check cross-dependencies for the atom
     typer or simple testing purpose. So, this is the first step to be
     able to formulate a classification problem for the atom type
     assignment process. So let me know if you are interested to
     discuss the details for this idea. I think this can be published, also.
     Data files are located in:
     joelib/src/resource/smartsEvaluation/evaluation.txt
     joelib/src/resource/smartsEvaluation/*.mol

   - joelib/src/wsi/ra/io/RegExpFilenameFilter.java
     Regular expression filter for files.
     
- 2004-05-28 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - viewer*.sh
     New shell scripts to allow easier access to the 'primitive' viewers.
     Especially the 2D visualisation has not a good user interface at
     the moment (only one molecule allowed). But SMARTS highlighting
     and retro-synthetical cutting visualisation is still possible.

- 2004-04-20 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.algo.datamining.weka.*
     Weka specific Instances with molecular data.
     The first step to general similarity/dissimilarity metrics for the
     already introduced joelib.math.similarity interfaces.
     
   - joelib.desc.data.*
     Introduced MoleculeCache interface to store data.

   - joelib.util.MDMatrixCache renamed to joelib.util.MoleculeDataCacheHolder
     Molecule data holder for matrices AND Weka-Molecule-instances.
     
   - Corrected usage of kekulization for MDL SD export and visualization using 
     JOEMol.kekulize()
     
   - Slightly updated DocBook-tutorial.

- 2004-03-20 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.io.types.cml.*
     Bug fixes for complex descriptor entries.

   - joelib.io.types.Amber
     joelib.io.types.Mopac
     Added support for 'Amber PREP' and 'Mopac Out' format.
     
   - joelib.io.Sybyl
     Corrected reading of partial charges from Sybyl format.
     Now the readed partial charges will not be overwritten by JOELib's
     Gasteiger-Marsili partial charges.

   - joelib.molecule.JOEMol
     Added partial charge vendor to allow to store this information in CML2.

- 2004-03-18 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.io.MoleculeCallback
     joelib.io.types.cml.CMLSequentialSAXReader
     Support for reading sequentially huge uncompressed CML files using
     a molecule callback method.

   - joelib.io.types.ChemicalMarkupLanguage
     Added molecule string reader and support for using this facility e.g.
     for loading huge CML files in compressed ZIP files (workaround to avoid
     using the SAX parser directly)

   - joelib.io.types.cml.MoleculeAttributeArray
     Added for allowing CML in RSS feeds by avoiding showing single entries
     (attributes are not shown, e.g. in Mozilla).

   - joelib.io.types.cml.MoleculeArray
     Added missing stereochemistry 'bondStereo'.

   - joelib.test.GhemicalTest
     Added geometry optimization test using Ghemical. This can be only used
     under Linux or from the Cygwin shell !

- 2004-03-16 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.data.*
     Added hashed chemistry kernel identifier, which is always used by
     descriptor calculation and file format conversion.
     Now you know which version was used. This kernel ID is written to
     SDF as comment and CML as metadata !

   - joelib.io.types.MDLSD
     Added missing stereochemistry informations to MDL SD writer.

   - joelib.gui.render.Renderer2D
     Corrected bond visualization for stereochemistry (cis/trans should
     not be drawn as hash/wedge!:-)

   - joelib.io.types.cml.*
     Updated to CML2 support with namespace and CML2 tags !

   - joelib.io.types.ZIP
     Added compressed file reader/writer. Can read multiple files in one
     ZIP file, except CML entries, because the SAX parser closes after
     the first readed entry. Please send me a workaround if you find one.

   - joelib.gui.test.ConvertPanel
     Added '+split' option to panel.

   - joelib.gui.test.InfoPanel
     Added chemistry kernel (used expert systems) informations to panel.

- 2004-01-24 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.desc.types.*
     Added a hug amount of descriptor calculation methods.

- 2003-12-09 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.molecule.JOEMol
     Fixed 'dead loop' in deleteAtom(JOEAtom) for heavy weight atoms. Concerns also
     stripSalts().

- 2003-12-09 Gerd Mueller (gerd@smb-tec.com)

   - wsi.ra.tool.ResourceLoader
     Fixed resource locations for paths with white spaces.

- 2003-12-03 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.desc.data.MoleculesDescriptorMatrix
     The generated 'yourMoleculeFile.matrix'-files contains now also
     informations if the descriptor is a binary-descriptor. So if you have
     already such files, please delete them and let JOELib them create again.
   - joelib.desc.result.IntResult
     The descriptor class can now also read double values and stores also
     double values set by setDoubleNV(double). This is important if you
     plan to normalize your data.
   - joelib/molecule/JOEMol
     Dead-loop-bug was fixed in deleteHydrogen(JOEAtom)

- 2003-11-01 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/test/Convert.java
     Added 'split' option to generate splitted files
   - joelib/data/JOEAromaticTyper.java
     Avoiding two root atoms in one ring !

- 2003-10-20 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/io/types/PDF.java
     Improved layout and storage of descriptor values

   - joelib/lib/vecmath.jar
     Added, because the 2D renderer requires some classes. This is not
     really necessary for Java3D users, but for the others we will obtain
     some errors without it.

- 2003-10-13 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/gui/render
     2D molecule rendering facility using AWT
   - joelib/io/SimpleImageWriter
     joelib/io/types/BMP, GIF, JPEG, PDF, PPM
     2D molecule rendering image writting classes

- 2003-10-01 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/molecule/JOEMol
     assigning stereo flags for tempoary JOEVirtualBond when adding bonds to an
     molecule where an atom is missing

- 2003-09-15 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib/docs/docbook:
     Changing .xml to .sgml for avoiding misunderstanding of the SGML-DocBook-Files !
     Index and Glossary was added when changing from DocBook 4.1 to 4.2 !
     Java based equation generation jtt/docbook/* without shell scripts and
     changed docbook generation shell script joelib/docs/docbook/generate.sh

- 2003-08-26 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.test.Convert:
     joelib.test.ConvertSkip:
     Recognize file output type correctly (and do not use the input file name !;-)

   - joelib.molecule.JOEBond:
     Calculate correct bond length.

- 2003-08-19 Gert Sclep (http://sourceforge.net/users/gsclep/)

   - joelib.gui.molviewer.java3d.Viewer:
     joelib.gui.molviewer.java3d.util.Java3DHelper:
     When molecules couldn't be drawn due to a buggy Java3D,
     the ViewerFrame was still shown when executing the Viewer
     class.

- 2003-08-19 Joerg Kurt Wegner (http://sourceforge.net/users/wegner/)

   - joelib.molecule.GenericDataHolder:
     Return null for emtpy descriptor names.

   - joelib.data.JOEAromaticTyper:
     Find new root ring atoms, if more than 2 other neighbour ring atoms
     (ignore non-heavy weight atoms).

- 2003-08-19 Gert Sclep (http://sourceforge.net/users/gsclep/)

   - joelib.io.types.ClearTextFormat:
     Comment of a CTX-file was added to the JOEMol object as an empty
     JOECommentData-object.

- 2003-08-12 Gert Sclep (http://sourceforge.net/users/gsclep/)

   -joelib.io.types.ClearTextFormat:
     Code for parsing /2DCOORD section of CTX-format wasn't compliant with
     CTX-format produced by CACTVS program (version 2.95).
