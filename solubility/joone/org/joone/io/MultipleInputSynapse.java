/*
 * MultipleInputSynapse.java
 *
 * Created on 10 gennaio 2005, 21.23
 */

package org.joone.io;

import org.joone.engine.*;
/**
 * This class reads sequentially all the connected input synapses,
 * in order to be able to use multiple sources as inputs.
 * It reads all the rows of the first input synapse, then all the
 * rows of the second one, and so on. The Stop Pattern is injected
 * only after the last row of the last input synapse is read.
 *
 * When this class is used, the monitor.trainingPatterns must be set
 * to the sum of the rows of all the attached input synapses.
 *
 * @author  P.Marrone
 */
public class MultipleInputSynapse extends InputSwitchSynapse {
    private int currentInput = 0;
    private int currentPatt = 0;
    
    /** Creates a new instance of MultipleInputSynapse */
    public MultipleInputSynapse() {
        super();
    }
    
    public Pattern fwdGet() {
        super.setActiveSynapse((StreamInputSynapse)inputs.get(currentInput));
        Pattern patt = super.fwdGet();
        return elaboratePattern(patt);
    }
    
    public Pattern fwdGet(InputConnector conn) {
        super.setActiveSynapse((StreamInputSynapse)inputs.get(currentInput));
        Pattern patt = super.fwdGet(conn);
        return elaboratePattern(patt);
    }
    
    private Pattern elaboratePattern(Pattern patt) {
        int count = patt.getCount();
        if (count == -1) {
            currentInput = currentPatt = 0;
        }
        else {
            patt.setCount(++currentPatt);
            if (getActiveSynapse().isEOF()) {
                // The current Input Synapse reached the EOF,
                // then swicth to the next
                ++currentInput;
                if (currentInput == inputs.size()) {
                    currentInput = currentPatt = 0; // Cycles if end of all the input synapses
                }
            }
        }
        return patt;
    }
    
    public void reset() {
        super.reset();
        currentInput = currentPatt = 0;
    }
    
}
