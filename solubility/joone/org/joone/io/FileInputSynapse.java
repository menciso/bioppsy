package org.joone.io;

import java.io.*;
import java.util.TreeSet;

import org.joone.log.*;
import org.joone.net.NetCheck;
import org.joone.exception.JooneRuntimeException;
import org.joone.engine.NetErrorManager;

/** Allows data to be presented to the network from a file.  The file must contain
 * semi-colon seperated values e.g '2;5;7;2.1'.
 */
public class FileInputSynapse extends StreamInputSynapse {
    /** The logger used to log errors and warnings. */
    private static final ILogger log = LoggerFactory.getLogger(FileInputSynapse.class);
    /** The name of the file to extract information from. */
    private String fileName = "";
    protected File cFile;
    
    private static final long serialVersionUID = 7456627292136514416L;
    
    public FileInputSynapse() {
        super();
    }
    
    /** Gets the file name that this synapse uses to extract the input data from.
     * @return The file name that this synapse uses to extract the input data from.
     */
    public java.lang.String getFileName() {
        return fileName;
    }
    
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        super.readObjectBase(in);
        fileName = (String) in.readObject();
        //if (!isBuffered() || (getInputVector().size() == 0))
        //    setFileName(fileName);
    }
    
    /** Sets the file name that this synapse should extract data from.
     * @param newFileName The file name that this synapse should extract data from.
     */
    public void setFileName(java.lang.String newFileName) {
        if (!fileName.equals(newFileName)) {
            fileName = newFileName;
            this.resetInput();
            super.setTokens(null);
            //initInputStream();
        }
    }
    
    private void writeObject(ObjectOutputStream out) throws IOException {
        super.writeObjectBase(out);
        out.writeObject(fileName);
    }
    
    protected void initInputStream() throws JooneRuntimeException {
        if ((fileName != null) && (!fileName.equals(new String("")))) {
            try {
                cFile = new File(fileName);
                FileInputStream fis = new FileInputStream(cFile);
                StreamInputTokenizer sit;
                if (getMaxBufSize() > 0)
                    sit = new StreamInputTokenizer(new InputStreamReader(fis), getMaxBufSize());
                else
                    sit = new StreamInputTokenizer(new InputStreamReader(fis));
                super.setTokens(sit);
            } catch (IOException ioe) {
                String error = "IOException in "+getName()+". Message is : ";
                log.warn(error + ioe.getMessage());
                if ( getMonitor() != null)
                    new NetErrorManager(getMonitor(),error+ioe.getMessage());
            }
        }
    }
    
    /** Returns a TreeSet of errors or problems regarding the setup of this synapse.
     * @return A TreeSet of errors or problems regarding the setup of this synapse.
     */
    public TreeSet check() {
        TreeSet checks = super.check();
        
        if (fileName == null || fileName.trim().equals("")) {
            checks.add(new NetCheck(NetCheck.ERROR, "File Name not set." , this));
        }
        
        return checks;
    }
}