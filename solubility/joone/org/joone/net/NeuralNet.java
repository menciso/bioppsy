/*
 * NeuralNet.java
 *
 * Created on 17 april 2001, 12.08
 */

package org.joone.net;

import java.util.*;
import java.io.*;
import org.joone.log.*;

import org.joone.engine.*;
import org.joone.engine.learning.*;
import org.joone.io.*;
import org.joone.script.MacroInterface;
import org.joone.exception.JooneRuntimeException;

/** This object represents a container of a neural network,
 * giving to the developer the possibility to manage a
 * neural network as a whole.
 * Thanks to it, a neural network can be saved and restored
 * using an unique writeObject and readObject command, without
 * be worried about its internal composition.
 * Not only this, because using a NeuralNet object, we can
 * also easily transport a neural network on remote machines
 * and runnit there, writing only few and generalized java code.
 *
 */
public class NeuralNet implements NeuralLayer, Serializable {
    
    private static final int MAJOR_RELEASE = 1;
    private static final int MINOR_RELEASE = 2;
    private static final int BUILD = 0;
    private static final String SUFFIX = "";
    
    private static final ILogger log = LoggerFactory.getLogger( NeuralNet.class );
    private Vector layers;
    private String NetName;
    private Monitor mon;
    private Layer inputLayer;
    private Layer outputLayer;
    private TeachingSynapse teacher;
    
    private static final long serialVersionUID = 8351124226081783962L;
    public static final int INPUT_LAYER = 0;
    public static final int HIDDEN_LAYER = 1;
    public static final int OUTPUT_LAYER = 2;
    
    protected Vector listeners;
    private MacroInterface macroPlugin;
    private boolean scriptingEnabled = false;
    
    private NeuralNetAttributes descriptor = null;
    
    private Hashtable params;
    
    /** Creates new NeuralNet */
    public NeuralNet() {
        layers = new Vector();
        // Creates a monitor with a back-reference to this neural-net
        mon = new Monitor();
    }
    
    public void start() {
        this.stop(false);
        if (readyToStart()) {
            Layer ly = null;
            int i;
            try {
                for (i=0; i < layers.size(); ++i) {
                    ly = (Layer)layers.elementAt(i);
                    ly.start();
                }
            }
            catch (RuntimeException rte) {
                // If some layer can't start, resets the state of the NN
                this.stop();
                String msg;
                log.error(msg = "RuntimeException was thrown while starting the neural network. Message is:" + rte.getMessage(), rte);
                throw new JooneRuntimeException(msg, rte);
            }
        }
        else {
            String msg;
            log.warn(msg = "NeuralNet: The neural net is already running");
            throw new JooneRuntimeException(msg);
        }
    }
    
    
    /**
     * Check if the neural net is ready to be started again
     * @return true only if there isn't any running layer
     */
    private boolean readyToStart() {
        for (int i=0; i < 100; ++i) {
            if (!this.isRunning())
                return true;
            else
                try {
                    // waits for 10 millisecs
                    Thread.sleep(10);
                }
                catch (InterruptedException e) {
                    return false;
                }
        }
        return false;
    }
    
    /** Waits for all the running Threads to finish
     * @see Thread.join()
     */
    public void join() {
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.elementAt(i);
            ly.join();
        }
        if (teacher != null)
            teacher.getTheLinearLayer().join();
    }
    
    public void stop(boolean notify) {
        if (isRunning()) {
            Layer ly = null;
            int i;
            for (i=0; i < layers.size(); ++i) {
                ly = (Layer)layers.elementAt(i);
                ly.stop();
            }
            if (teacher != null) {
                teacher.stop();
            }
            if ((getMonitor() != null) && (notify))
                new NetStoppedEventNotifier(getMonitor()).start();
        }
    }
    
    public void stop() {
        this.stop(true);
    }
    
    protected int getNumOfstepCounters() {
        int count = 0;
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.elementAt(i);
            if (ly.hasStepCounter())
                ++count;
        }
        if (teacher != null) {
            if ((teacher.getDesired() != null) && (teacher.getDesired().isStepCounter()))
                ++count;
        }
        return count;
    }
    
    /** Changed to public so that save as XML can find this method
     *
     */
    
    public Layer getInputLayer() {
        if (inputLayer != null)
            return inputLayer;
        if (layers == null)
            return null;
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.elementAt(i);
            if (ly.isInputLayer()) {
                inputLayer = ly;
                break;
            }
        }
        return inputLayer;
    }
    
    
    /** Changed to public so that save as XML can find it
     */
    
    public Layer getOutputLayer() {
        if (outputLayer != null)
            return outputLayer;
        if (layers == null)
            return null;
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.elementAt(i);
            if (ly.isOutputLayer()) {
                outputLayer = ly;
                break;
            }
        }
        return outputLayer;
    }
    
    
    public int getRows() {
        Layer ly = this.getInputLayer();
        if (ly != null)
            return ly.getRows();
        else
            return 0;
    }
    
    public void setRows(int p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.setRows(p1);
    }
    
    public void addNoise(double p1) {
        Layer ly;
        int i;
        for (i=0; i < layers.size(); ++i) {
            ly = (Layer)layers.elementAt(i);
            ly.addNoise(p1);
        }
    }
    
    public void randomize(double amplitude) {
        Layer ly;
        int i;
        for (i=0; i < layers.size(); ++i) {
            ly = (Layer)layers.elementAt(i);
            ly.randomize(amplitude);
        }
    }
    
    public Matrix getBias() {
        Layer ly = this.getInputLayer();
        if (ly != null)
            return ly.getBias();
        else
            return null;
    }
    
    public Vector getAllOutputs() {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            return ly.getAllOutputs();
        else
            return null;
    }
    
    public String getLayerName() {
        return NetName;
    }
    
    public void removeOutputSynapse(OutputPatternListener p1) {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            ly.removeOutputSynapse(p1);
    }
    
    public void setAllInputs(Vector p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.setAllInputs(p1);
    }
    
    public void removeAllOutputs() {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            ly.removeAllOutputs();
        setTeacher(null);
    }
    
    public Vector getAllInputs() {
        Layer ly = this.getInputLayer();
        if (ly != null)
            return ly.getAllInputs();
        else
            return null;
    }
    
    public boolean addOutputSynapse(OutputPatternListener p1) {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            return ly.addOutputSynapse(p1);
        else
            return false;
    }
    
    public void setBias(Matrix p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.setBias(p1);
    }
    
    public void removeInputSynapse(InputPatternListener p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.removeInputSynapse(p1);
    }
    
    public void setLayerName(String p1) {
        NetName = p1;
    }
    
    public boolean addInputSynapse(InputPatternListener p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            return ly.addInputSynapse(p1);
        else
            return false;
    }
    
    public void setAllOutputs(Vector p1) {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            ly.setAllOutputs(p1);
    }
    
    public void setMonitor(Monitor p1) {
        mon = p1;
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.elementAt(i);
            ly.setMonitor(mon);
        }
        setScriptingEnabled(isScriptingEnabled());
        if (getTeacher() != null)
            getTeacher().setMonitor(p1);
    }
    
    public Monitor getMonitor() {
        return mon;
    }
    
    public void removeAllInputs() {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.removeAllInputs();
    }
    
    public NeuralLayer copyInto(NeuralLayer p1) {
        return null;
    }
    
    /**
     * @deprecated Use instead setTeacher
     */
    public void addTeacher(TeachingSynapse ts) {
        teacher = ts;
        teacher.setMonitor(mon);
    }
    
    public void addLayer(Layer layer) {
        this.addLayer(layer, HIDDEN_LAYER);
    }
    
    public void addLayer(Layer layer, int tier) {
        if (!layers.contains(layer)) {
            layer.setMonitor(mon);
            layers.addElement(layer);
        }
        if (tier == INPUT_LAYER)
            inputLayer = layer;
        if (tier == OUTPUT_LAYER)
            outputLayer = layer;
    }
    
    public void removeLayer(Layer layer) {
        if (layers.contains(layer)) {
            layers.removeElement(layer);
            if (getMonitor() != null)
                getMonitor().removeNeuralNetListener(layer);
            if (layer == inputLayer)
                inputLayer = null;
            else
                if (layer == outputLayer)
                    outputLayer = null;
        }
    }
    
    /**
     * Resets all the StreamInputLayer of the net
     */
    public void resetInput() {
        Layer ly = null;
        int i;
        for (i=0; i < layers.size(); ++i) {
            ly = (Layer)layers.elementAt(i);
            Vector inputs = ly.getAllInputs();
            if (inputs != null)
                for (int x=0; x < inputs.size(); ++x) {
                    InputPatternListener syn = (InputPatternListener)inputs.elementAt(x);
                    if (syn instanceof StreamInputSynapse)
                        ((StreamInputSynapse)syn).resetInput();
                    if (syn instanceof InputSwitchSynapse)
                        ((InputSwitchSynapse)syn).resetInput();
                }
        }
        if (getTeacher() != null)
            getTeacher().resetInput();
    }
    
    public void addNeuralNetListener(NeuralNetListener listener) {
        if (getListeners().contains(listener))
            return;
        listeners.addElement(listener);
        if (getMonitor() != null)
            getMonitor().addNeuralNetListener(listener);
    }
    
    public Vector getListeners() {
        if (listeners == null)
            listeners = new Vector();
        return listeners;
    }
    
    public void removeNeuralNetListener(NeuralNetListener listener) {
        getListeners().removeElement(listener);
        if (getMonitor() != null)
            getMonitor().removeNeuralNetListener(listener);
    }
    
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        /* Since the listeners' vector in the Monitor object is transient,
         * we must fill it from the NeuralNet.listeners vector
         */
        Vector lst = getListeners();
        if (getMonitor() != null)
            for (int i=0; i < lst.size(); ++i) {
                getMonitor().addNeuralNetListener((NeuralNetListener)lst.elementAt(i));
            }
        // Restores the exported variables jNet and jMon
        setMacroPlugin(macroPlugin);
    }
    
    /**
     * Method to get the version.
     * @return A string containing the version of joone's engine in the format xx.yy.zz
     */
    public static String getVersion() {
        return MAJOR_RELEASE +
        "." +
        MINOR_RELEASE +
        "." +
        BUILD + SUFFIX;
    }
    
    /**
     * Method to get the numeric version.
     * @return an integer containing the joone's engine version
     */
    public static Integer getNumericVersion() {
        return new Integer(MAJOR_RELEASE * 1000000 +
        MINOR_RELEASE * 1000 +
        BUILD);
    }
    
    public Layer getLayer(String layerName) {
        Layer ly = null;
        for (int i=0; i < layers.size(); ++i) {
            ly = (Layer)layers.elementAt(i);
            if (ly.getLayerName().compareToIgnoreCase(layerName) == 0)
                return ly;
        }
        return null;
    }
    
    public Vector getLayers() {
        return this.layers;
    }
    
    /** Permits to initialize a neural network with a Vector
     *  containing layers.
     *
     */
    public void setLayers(Vector newlayers) {
        this.layers = newlayers;
    }
    
    /** Permits to initialize a neural network with an
     *  ArrayList containing layers. Added for Spring.
     *
     */
    public void setLayersList(ArrayList list) {
        this.setLayers(new Vector(list));
    }
    
    /** Sets the Teacher for this NeuralNet object
     * @param TeachingSynapse - the new teacher. It can be null to make unsupervised this neural network
     */
    public void setTeacher(TeachingSynapse ts) {
        if (getMonitor() != null)
            if (ts != null)
                getMonitor().setSupervisioned(true);
            else
                getMonitor().setSupervisioned(false);
        teacher = ts;
    }
    
    /** Added for save as XML
     *
     */
    public TeachingSynapse getTeacher() {
        return teacher;
    }
    
    /** Added for save as XML
     *
     */
    public void setListeners(Vector listeners) {
        //addNeuralNetListener(listeners);
    }
    
    /** Added for save as XML
     *
     */
    public void setInputLayer(Layer newLayer) {
        inputLayer = newLayer;
    }
    
    /** Added for save as XML
     *
     */
    public void setOutputLayer(Layer newLayer) {
        outputLayer = newLayer;
    }
    
    
    public NeuralNetAttributes getDescriptor() {
        if (descriptor == null) {
            descriptor = new NeuralNetAttributes();
            descriptor.setNeuralNetName(this.getLayerName());
        }
        return descriptor;
    }
    
    /** Added for save as XML
     *
     */
    public void setDescriptor(NeuralNetAttributes newdescriptor) {
        this.descriptor = newdescriptor;
    }
    
    /**
     * Returns true if at least one layer is running
     * @return boolean
     */
    public boolean isRunning() {
        Layer ly = null;
        for (int i=0; i < layers.size(); ++i) {
            ly = (Layer)layers.elementAt(i);
            if (ly.isRunning()) {
                //log.info(ly.getLayerName()+" is runninng");
                return true;
            }
        }
        if (teacher != null)
            if (teacher.getTheLinearLayer().isRunning()) {
                return true;
            }
        return false;
    }
    
    /**
     * Creates a copy of the contained neural network
     *
     * @return the cloned NeuralNet
     */
    public NeuralNet cloneNet() {
        NeuralNet newnet = null;
        try {
            ByteArrayOutputStream f = new ByteArrayOutputStream();
            ObjectOutput s = new ObjectOutputStream(f);
            s.writeObject(this);
            s.flush();
            ByteArrayInputStream fi = new ByteArrayInputStream(f.toByteArray());
            ObjectInput oi = new ObjectInputStream(fi);
            newnet = (NeuralNet)oi.readObject();
        }
        catch (Exception ioe) {
            log.warn( "IOException while cloning the Net. Message is : " + ioe.getMessage(),
            ioe );
        }
        return newnet;
    }
    
    public void removeAllListeners() {
        listeners = null;
        if (getMonitor() != null)
            getMonitor().removeAllListeners();
    }
    
    /** Enable/disable the scripting engine for the net.
     * If disabled, all the event-driven macros will be not run
     * @param enabled true to enable the scripting, otherwise false
     */
    public void setScriptingEnabled(boolean enabled) {
        scriptingEnabled = enabled;
        if (enabled) {
            NeuralNetListener listener = getMacroPlugin();
            if (listener == null)
                log.info("MacroPlugin not set: Impossible to enable the scripting");
            else
                this.addNeuralNetListener(getMacroPlugin());
        }
        else {
            if (macroPlugin != null)
                this.removeNeuralNetListener(macroPlugin);
        }
    }
    
    /** Gets if the scripting engine is enabled
     * @return true if enabled
     */
    public boolean isScriptingEnabled() {
        return scriptingEnabled;
    }
    
    /** Getter for property macroPlugin.
     * @return Value of property macroPlugin.
     */
    public MacroInterface getMacroPlugin() {
        return macroPlugin;
    }
    
    /** Setter for property macroPlugin.
     * @param macroPlugin New value of property macroPlugin.
     */
    public void setMacroPlugin(MacroInterface macroPlugin) {
        this.macroPlugin = macroPlugin;
        if (macroPlugin != null) {
            macroPlugin.set("jNet", this);
            macroPlugin.set("jMon", getMonitor());
        }
    }
    
    /** Gets a custom parameter from the neural net.
     * The user is free to use the custom parameters as s/he wants.
     * They are useful to transport a whatever value along with the net.
     * @param key The searched key
     * @return The value of the parameter if found, otherwise null
     */
    public Object getParam(String key) {
        if (params == null)
            return null;
        return params.get(key);
    }
    
    /** Sets a custom parameter of the neural net.
     * The user is free to use the custom parameters as s/he wants.
     * They are useful to transport a whatever value along with the net.
     * @param key The key of the param
     * @param obj The value of the param
     */
    public void setParam(String key, Object obj) {
        if (params == null)
            params = new Hashtable();
        if (params.containsKey(key))
            params.remove(key);
        params.put(key, obj);
    }
    
    /** Return all the keys of the parameters contained in the net.
     * @return An array of Strings containing all the keys if found, otherwise null
     */
    public String[] getKeys() {
        if (params == null)
            return null;
        String[] keys = new String[params.keySet().size()];
        Enumeration myEnum = params.keys();
        for (int i=0; myEnum.hasMoreElements(); ++i) {
            keys[i] = (String)myEnum.nextElement();
        }
        return keys;
    }
    
    /**
     * Compiles all layers' check messages.
     *
     * @see NeuralLayer
     * @return validation errors.
     */
    public TreeSet check() {
        
        // Prepare an empty set for check messages;
        TreeSet checks = new TreeSet();
        
        // Check for an empty neural network
        if ((layers == null) || layers.isEmpty()) {
            checks.add(new NetCheck(NetCheck.ERROR, "The Neural Network doesn't contain any layer", mon));
            // If empty it makes no sense to continue the check
            return checks;
        }
        else
            // Check for the presence of more than one InputSynpase having stepCounter set to true
            if (getNumOfstepCounters() > 1)
                checks.add(new NetCheck(NetCheck.ERROR, "More than one InputSynapse having stepCounter set to true is present", mon));
        
        // Check all layers.
        for (int i = 0; i < layers.size(); i++) {
            Layer layer = (Layer) layers.elementAt(i);
            checks.addAll(layer.check());
        }
        
        // Check the teacher (only if it exists and this is not a nested neural network)
        if (mon.getParent() == null) {
            if (teacher != null) {
                checks.addAll(teacher.check());
                if (mon != null && mon.isLearning() && !mon.isSupervisioned())
                    checks.add(new NetCheck(NetCheck.WARNING, "Teacher is present: the supervised property should be set to true", mon));
            }
            else {
                if (mon != null && mon.isLearning() && mon.isSupervisioned())
                    checks.add(new NetCheck(NetCheck.ERROR, "Teacher not present: set to false the supervised property", mon));
            }
        }
        
        
        // Check the Monitor.
        if (mon != null) {
            checks.addAll(mon.check());
        }
        
        // Return check messages.
        return checks;
    }
    
}
