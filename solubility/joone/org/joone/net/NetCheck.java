package org.joone.net;

import org.joone.engine.NeuralLayer;
import org.joone.engine.InputPatternListener;
import org.joone.engine.OutputPatternListener;
import org.joone.engine.Monitor;

/**
 * Class to represent a network validation check error.
 * NetChecks of ERROR severity prevent the network from running,
 * NetChecks of WARNING severity do not.
 */
public class NetCheck implements Comparable {

    /** Fatal check severity. */
    public static final int ERROR = 0;

    /** Non-fatal check severity. */
    public static final int WARNING = 1;

    /** The check severity. */
    private int severity;

    /** The check message. */
    private String message;

    /** The network object producing the check. */
    private Object object;

    /**
     * Constructor.
     *
     * @param severityArg the severity of the check. Should be ERROR or WARNING.
     * @param messageArg the message assosiated with the check.
     * @param objectArg the network component producing the check.
     */
    public NetCheck(int severityArg, String messageArg, Object objectArg) {
        severity = severityArg;
        message = messageArg;
        object = objectArg;
    }

    /**
     * Produce a String representation of the check.
     *
     * @return the String representation of the check.
     */
    public String toString() {

        // Get the class name without the package extension.
        String className = object.getClass().getName();
        className = className.substring(1 + className.lastIndexOf("."));

        // Get the instance name. Try to find the Joone interface name if possible.
        String instanceName;
        if (object instanceof NeuralLayer) {
            instanceName = ((NeuralLayer) object).getLayerName();
        } else if (object instanceof InputPatternListener) {
            instanceName = ((InputPatternListener) object).getName();
        } else if (object instanceof OutputPatternListener) {
            instanceName = ((OutputPatternListener) object).getName();
        } else if (object instanceof Monitor) {
            instanceName = "Monitor";
        } else {
            instanceName = object.toString();
        }

        // Build up the check message.
        StringBuffer checkMessage = new StringBuffer();
        if (severity == ERROR) {
            checkMessage.append("Error - ");
        } else if (severity == WARNING) {
            checkMessage.append("Warning - ");
        }
        checkMessage.append(className);
        checkMessage.append(" - ");

        if (instanceName != null && !instanceName.trim().equals("")) {
            checkMessage.append(instanceName);
            checkMessage.append(" - ");
        }

        checkMessage.append(message);

        return checkMessage.toString();
    }

    /**
     * Method to see if this check is a WARNING.
     *
     * @return true if warning.
     */
    public boolean isWarning() {
        return severity == WARNING;
    }

    /**
     * Method to see if this check is a ERROR.
     *
     * @return true if error.
     */
    public boolean isError() {
        return severity == ERROR;
    }

    /**
     * Method to order by message when in TreeSet.
     *
     * @return true if error.
     */
    public int compareTo(Object o) {
        if (o instanceof NetCheck) {
            NetCheck nc = (NetCheck) o;
            return toString().compareTo(nc.toString());
        } else {
            return 0;
        }
    }
}
