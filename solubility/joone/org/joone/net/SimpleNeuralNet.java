/*
 * Copyright (C) 2004  Derek James and Philip Tucker
 * created by Philip Tucker on Dec 23, 2003
 */
package org.joone.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.joone.engine.DirectSynapse;
import org.joone.engine.InputPatternListener;
import org.joone.engine.Layer;
import org.joone.engine.Matrix;
import org.joone.engine.NeuralLayer;
import org.joone.engine.OutputPatternListener;
import org.joone.engine.OutputSwitchSynapse;
import org.joone.engine.Pattern;
import org.joone.engine.learning.TeacherSynapse;
import org.joone.engine.learning.TeachingSynapse;
import org.joone.io.StreamInputSynapse;
import org.joone.io.StreamOutputSynapse;

/**
 * @author Philip Tucker
 * @see org.joone.net.NeuralNet
 * Version of org.joone.net.NeuralNet with Monitor removed.
 * TODO - add NeuralNetListeners?
 */
public class SimpleNeuralNet implements Serializable, Cloneable {
    
    private List layers = new ArrayList();
    private String name = "SimpleNeuralNet";
    private Layer inputLayer;
    private Layer outputLayer;
    
    private DirectSynapse in = null;
    private DirectSynapse out = null;
    
    public static final int INPUT_LAYER = NeuralNet.INPUT_LAYER;
    public static final int HIDDEN_LAYER = NeuralNet.HIDDEN_LAYER;
    public static final int OUTPUT_LAYER = NeuralNet.OUTPUT_LAYER;
    
//    protected List listeners = new ArrayList();
    
    public SimpleNeuralNet(String aName) {
    	name = aName;
    }
    
    public void start() {
        if (readyToStart()) {
			in = new DirectSynapse();
			in.setName(getName() + ": in synapse");
			addInputSynapse(in);
				
			out = new DirectSynapse();
			out.setName(getName() + ": out synapse");
			addOutputSynapse(out);

            try {
                for (int i=0; i < layers.size(); ++i) {
					Layer ly = (Layer)layers.get(i);
                    ly.start();
                }
            }
            catch (RuntimeException rte) {
                // If some layer can't start, resets the state of the NN
                this.stop();
                throw rte;
            }
        }
        else
            throw new RuntimeException(
				"SimpleNeuralNet: The neural net is already running");
    }
    
    
    /**
     * Check if the neural net is ready to be started again
     * @return true only if there isn't any running layer
     */
    private boolean readyToStart() {
        for (int i=0; i < 100; ++i) {
            if (!this.isRunning())
                return true;
            else
                try {
                    // waits for 10 millisecs
                    Thread.sleep(10);
                }
                catch (InterruptedException e) {
                    return false;
                }
        }
        return false;
    }
    
	public void stop() {
		if (isRunning()) {
			for (int i=0; i < layers.size(); ++i) {
				Layer ly = (Layer)layers.get(i);
				ly.stop();
			}
			
			removeInputSynapse( in );
			removeOutputSynapse( out );
			in = null;
			out = null;
		}
	}
    
    /** Changed to public so that save as XML can find this method
     *
     */
    
    public Layer getInputLayer() {
        if (inputLayer != null)
            return inputLayer;
        if (layers == null)
            return null;
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.get(i);
            Vector inps = ly.getAllInputs();
            if ((inps == null) || (inps.size() == 0)) {
                inputLayer = ly;
                break;
            }
        }
        return inputLayer;
    }
    
    /** Changed to public so that save as XML can find it
     */
    
    public Layer getOutputLayer() {
        if (outputLayer != null)
            return outputLayer;
        if (layers == null)
            return null;
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.get(i);
            Vector outs = ly.getAllOutputs();
            if ((outs == null) || (outs.size() == 0)) {
                outputLayer = ly;
                break;
            }
            if (checkOutputs(outs)) {
                outputLayer = ly;
                break;
            }
        }
        return outputLayer;
    }
    
    
    protected boolean checkOutputs(Vector outs) {
        if (outs == null)
            return true;
        for (int x=0; x < outs.size(); ++x) {
            if ((outs.elementAt(x) instanceof StreamOutputSynapse)
            || (outs.elementAt(x) instanceof TeachingSynapse)
            || (outs.elementAt(x) instanceof TeacherSynapse))
                return true;
            if (outs.elementAt(x) instanceof OutputSwitchSynapse) {
                OutputSwitchSynapse os = (OutputSwitchSynapse)outs.elementAt(x);
                if (checkOutputs(os.getAllOutputs()))
                    return true;
            }
        }
        return false;
    }
    
    /**
     * must be called between start() and stop()
     */
    public double[] getNextPattern(double[] input) {
    	if (in == null || out == null)
			throw new RuntimeException("SimpleNeuralNet: not started");    	
		Pattern inPattern = new Pattern(input);
		inPattern.setCount(1);
		in.fwdPut(inPattern);
		return out.fwdGet().getArray();
    }
    
    public int getRows() {
        Layer ly = this.getInputLayer();
        if (ly != null)
            return ly.getRows();
        else
            return 0;
    }
    
    public void setRows(int p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.setRows(p1);
    }
    
    public void addNoise(double p1) {
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.get(i);
            ly.addNoise(p1);
        }
    }
    
    public void randomize(double amplitude) {
        for (int i=0; i < layers.size(); ++i) {
			Layer ly = (Layer)layers.get(i);
            ly.randomize(amplitude);
        }
    }
    
    public Matrix getBias() {
        Layer ly = this.getInputLayer();
        if (ly != null)
            return ly.getBias();
        else
            return null;
    }
    
    public Vector getAllOutputs() {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            return ly.getAllOutputs();
        else
            return null;
    }
    
    public String getName() {
        return name;
    }
    
    public void removeOutputSynapse(OutputPatternListener p1) {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            ly.removeOutputSynapse(p1);
    }
    
    public void setAllInputs(Vector p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.setAllInputs(p1);
    }
    
    public void removeAllOutputs() {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            ly.removeAllOutputs();
    }
    
    public Vector getAllInputs() {
        Layer ly = this.getInputLayer();
        if (ly != null)
            return ly.getAllInputs();
        else
            return null;
    }
    
    public void addOutputSynapse(OutputPatternListener p1) {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            ly.addOutputSynapse(p1);
    }
    
    public void setBias(Matrix p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.setBias(p1);
    }
    
    public void removeInputSynapse(InputPatternListener p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.removeInputSynapse(p1);
    }
    
    public void setName(String p1) {
		name = p1;
    }
    
    public void addInputSynapse(InputPatternListener p1) {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.addInputSynapse(p1);
    }
    
    public void setAllOutputs(Vector p1) {
        Layer ly = this.getOutputLayer();
        if (ly != null)
            ly.setAllOutputs(p1);
    }
    
    public void removeAllInputs() {
        Layer ly = this.getInputLayer();
        if (ly != null)
            ly.removeAllInputs();
    }
    
    public NeuralLayer copyInto(NeuralLayer p1) {
        return null;
    }
    
    public void addLayer(Layer layer) {
        this.addLayer(layer, HIDDEN_LAYER);
    }
    
    public void addLayer(Layer layer, int tier) {
        if (!layers.contains(layer))
            layers.add(layer);
        if (tier == INPUT_LAYER)
            inputLayer = layer;
        if (tier == OUTPUT_LAYER)
            outputLayer = layer;
    }
    
    public void removeLayer(Layer layer) {
        if (layers.contains(layer))
            layers.remove(layer);
        if (layer == inputLayer)
            inputLayer = null;
        else
            if (layer == outputLayer)
                outputLayer = null;
    }
    
    /**
     * Resets all the StreamInputLayer of the net
     */
    public void resetInput() {
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.get(i);
            Vector inputs = ly.getAllInputs();
            if (inputs != null)
                for (int x=0; x < inputs.size(); ++x) {
                    InputPatternListener syn = (InputPatternListener)inputs.elementAt(x);
                    if (syn instanceof StreamInputSynapse)
                        ((StreamInputSynapse)syn).resetInput();
                }
        }
    }
    
//    public void addNeuralNetListener(NeuralNetListener listener) {
//        if (getListeners().contains(listener))
//            return;
//        listeners.add(listener);
//    }
//    
//    protected List getListeners() {
//        if (listeners == null)
//            listeners = new ArrayList();
//        return listeners;
//    }
//    
//    public void removeNeuralNetListener(NeuralNetListener listener) {
//        getListeners().remove(listener);
//    }
    
    public Layer getLayer(String layerName) {
        for (int i=0; i < layers.size(); ++i) {
			Layer ly = (Layer)layers.get(i);
            if (ly.getLayerName().compareToIgnoreCase(layerName) == 0)
                return ly;
        }
        return null;
    }
    
    public List getLayers() {
        return this.layers;
    }
    
    /** Added for save as XML
     *
     */
    public void setInputLayer(Layer newLayer) {
        inputLayer = newLayer;
    }
    
    /** Added for save as XML
     *
     */
    public void setOutputLayer(Layer newLayer) {
        outputLayer = newLayer;
    }
    
    /**
     * Returns true if at least one layer is running
     * @return boolean
     */
    public boolean isRunning() {
        for (int i=0; i < layers.size(); ++i) {
            Layer ly = (Layer)layers.get(i);
            if (ly.isRunning())
                return true;
        }
        return false;
    }

//    public void removeAllListeners() {
//        listeners.clear();
//    }
    
	public void resetRecurrent() {
		double[] zeros = new double[in.getOutputDimension()];
		Arrays.fill(zeros, 0d);
		for (int i = 0; i < (layers.size() * 100); ++i)
			getNextPattern(zeros);
	}
	
	public SimpleNeuralNet cloneNet() {
		return (SimpleNeuralNet) clone();
	}

	public Object clone() {
		try {
			ByteArrayOutputStream f = new ByteArrayOutputStream();
			ObjectOutput s = new ObjectOutputStream(f);
			s.writeObject(this);
			s.flush();
			ByteArrayInputStream fi = new ByteArrayInputStream(f.toByteArray());
			ObjectInput oi = new ObjectInputStream(fi);
			return oi.readObject();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
