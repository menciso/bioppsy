/*
 * TestXML.java
 *
 * Created on 28 aprile 2004, 22.17
 */

package org.joone.samples.engine.xml;

import org.joone.net.*;
import org.joone.engine.*;
import org.joone.io.*;
import java.io.*;
import java.beans.*;
/**
 *
 * @author  paolo
 */
public class TestXML implements NeuralNetListener {
    
    /** Creates a new instance of TestXML */
    public TestXML() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new TestXML().elaborate();
    }
    
    private void elaborate() {
        NeuralNet nn = new NeuralNetLoader("/tmp/xor.snet").getNeuralNet();
        nn.getDescriptor().setTrainingError(0.03);
        try {
            XMLEncoder e = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("/tmp/Test.xml")));
            e.writeObject(nn);
            e.close();
            System.out.println("Marshaling Done");
            
            XMLDecoder d = new XMLDecoder(new BufferedInputStream(new FileInputStream("/tmp/Test.xml")));
            NeuralNet result = (NeuralNet)d.readObject();
            d.close();
            System.out.println("Unmarshaling Done");
            
            result.addNeuralNetListener(this);
            result.getMonitor().setTotCicles(3000);
            result.randomize(0.2);
            //result.resetInput();
            result.start();
            result.getMonitor().Go();
            
        } catch (FileNotFoundException fnfe) { fnfe.printStackTrace(); }
    }
    
    public void cicleTerminated(NeuralNetEvent e) {
    }
    
    public void errorChanged(NeuralNetEvent e) {
        Monitor mon = (Monitor)e.getSource();
        if ((mon.getCurrentCicle() % 100) == 0)
            System.out.println("RMSE at epoch "+(mon.getTotCicles()-mon.getCurrentCicle())+": "+mon.getGlobalError());
    }
    
    public void netStarted(NeuralNetEvent e) {
        System.out.println("Running...");
    }
    
    public void netStopped(NeuralNetEvent e) {
        System.out.println("Finished");
    }
    
    public void netStoppedError(NeuralNetEvent e, String error) {
    }
    
}
