/*
 * XOR.java
 * Sample class to demostrate the use of the Joone's core engine
 * see the Developer Guide for more details
 */

/*
 * JOONE - Java Object Oriented Neural Engine
 * http://joone.sourceforge.net
 */
package org.joone.samples.engine.xor;

import org.joone.engine.*;
import org.joone.engine.learning.*;
import org.joone.io.*;

public class XOR implements NeuralNetListener {
    
    /** Creates new XOR */
    public XOR() {
    }
    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String args[]) {
        if (args.length < 2) {
            System.out.println("Usage: java org.joone.samples.xor.XOR <inputFile> <errorFile>");
            System.out.println("where <inputFile> is the input file name (with its complete path) that contains the XOR truth table");
            System.out.println("      <errorFile> is the output file name (with its complete path) that contains the error values of the net");
        }
        else {
            XOR xor = new XOR();
            xor.Go(args[0], args[1]);
        }
    }
    
    public void Go(String inputFile, String outputFile) {
        /*
         * Firts, creates the three Layers
         */
        LinearLayer input = new LinearLayer();
        SigmoidLayer hidden = new SigmoidLayer();
        SigmoidLayer output = new SigmoidLayer();
        input.setLayerName("input");
        hidden.setLayerName("hidden");
        output.setLayerName("output");
        /* sets their dimensions */
        input.setRows(2);
        hidden.setRows(3);
        output.setRows(1);
        
        /*
         * Now create the two Synapses
         */
        FullSynapse synapse_IH = new FullSynapse(); /* input -> hidden conn. */
        FullSynapse synapse_HO = new FullSynapse(); /* hidden -> output conn. */
        
        synapse_IH.setName("IH");
        synapse_HO.setName("HO");
        /*
         * Connect the input layer whit the hidden layer
         */
        input.addOutputSynapse(synapse_IH);
        hidden.addInputSynapse(synapse_IH);
        /*
         * Connect the hidden layer whit the output layer
         */
        hidden.addOutputSynapse(synapse_HO);
        output.addInputSynapse(synapse_HO);
        
        /*
         * Create the Monitor object and set the learning parameters
         */
        Monitor monitor = new Monitor();
        monitor.setLearningRate(0.8);
        monitor.setMomentum(0.3);
        /*
         * Passe the Monitor to all components
         */
        input.setMonitor(monitor);
        hidden.setMonitor(monitor);
        output.setMonitor(monitor);
        
        /* The application registers itself as monitor's listener
         * so it can receive the notifications of termination from
         * the net.
         */
        monitor.addNeuralNetListener(this);
        
        FileInputSynapse inputStream = new FileInputSynapse();
        /* The first two columns contain the input values */
        inputStream.setAdvancedColumnSelector("1,2");

        /* This is the file that contains the input data */
        inputStream.setFileName(inputFile);
        input.addInputSynapse(inputStream);
        
        
        TeachingSynapse trainer = new TeachingSynapse();
        trainer.setMonitor(monitor);
        
        /* Setting of the file containing the desired responses,
         provided by a FileInputSynapse */
        FileInputSynapse samples = new FileInputSynapse();
        samples.setFileName(inputFile);
        /* The output values are on the third column of the file */
        samples.setAdvancedColumnSelector("3");

        trainer.setDesired(samples);
        
        /* Creates the error output file */
        FileOutputSynapse error = new FileOutputSynapse();
        error.setFileName(outputFile);
        //error.setBuffered(false);
        trainer.addResultSynapse(error);
        
        /* Connects the Teacher to the last layer of the net */
        output.addOutputSynapse(trainer);
        
        /* Creates the results output file */ 
        //FileOutputSynapse results = new FileOutputSynapse();
        //results.setFileName("c:\\temp\\results.txt");
        //output.addOutputSynapse(results);
        
        /* All the layers must be activated invoking their method start;
         * the layers are implemented as Runnable objects, then they are
         * instanziated on separated threads.
         */
        input.start();
        hidden.start();
        output.start();
        
        monitor.setTrainingPatterns(4); /* # of rows (patterns) contained in the input file */
        monitor.setTotCicles(5000); /* How many times the net must be trained on the input patterns */
        monitor.setLearning(true); /* The net must be trained */
        monitor.Go(); /* The net starts the training job */
    }
    
    public void netStopped(NeuralNetEvent e) {
        System.out.println("Training finished");
        System.exit(0);
    }
    
    public void cicleTerminated(NeuralNetEvent e) {
    }
    
    public void netStarted(NeuralNetEvent e) {
        System.out.println("Training...");
    }
    
    public void errorChanged(NeuralNetEvent e) {
        Monitor mon = (Monitor)e.getSource();
        long c = mon.getCurrentCicle();
        long cl = c / 500;
        /* We want print the results every 1000 cycles */
        if ((cl * 500) == c)
            System.out.println(c + " cycles remaining - Error = " + mon.getGlobalError());
    }
    
    public void netStoppedError(NeuralNetEvent e,String error) {
    }
}
