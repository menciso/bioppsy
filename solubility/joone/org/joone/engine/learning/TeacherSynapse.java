package org.joone.engine.learning;

import org.joone.log.*;
import org.joone.engine.*;
import org.joone.io.*;
import org.joone.net.NetCheck;

import java.io.IOException;
import java.util.TreeSet;


/**
 * Final element of a neural network; it permits to calculate
 * both the error of the last training cycle and the vector
 * containing the error pattern to apply to the net to
 * calculate the backprop algorithm.
 */
public class TeacherSynapse extends Synapse implements NeuralNetListener {
    /**
     * Logger
     * */
    private static final ILogger log = LoggerFactory.getLogger(TeacherSynapse.class);
    
    private StreamInputSynapse desired;
    protected transient Fifo error;
    protected transient double GlobalError = 0;
    protected transient boolean firstTime = true;
    private transient int patterns = 0;
    
    private static final long serialVersionUID = -1301682557631180066L;
    
    public TeacherSynapse() {
        super();
        firstTime = true;
    }
    
    protected void backward(double[] pattern) {
        // Not used.
    }
    
    /**
     * Pushes the calculated array in the FIFO queue at the end of a 
     * epoch, that is after all patterns have been seen.
     *
     * @param error, the calculated error ((R)MSE).
     * @param count, the cycle of the  calculated error.
     */
    protected void pushError(double error, int count) {
        double[] cost = new double[1];
        cost[0] = error;
        Pattern ptnErr = new Pattern(cost);
        ptnErr.setCount(count);
        synchronized (this) {
            getError().push(ptnErr);
            notify();
        }
    }
    
    protected void forward(double[] pattern) {
        Pattern pattDesired;
        double[] pDesired;
        int x;
        if ((m_pattern.getCount() == 1) || (m_pattern.getCount() == -1)) {
            try {
                desired.gotoFirstLine();
                if ((!firstTime) && (patterns == getMonitor().getNumOfPatterns())) {
                    // MSE
                    GlobalError = GlobalError / getMonitor().getNumOfPatterns();
                    if(getMonitor().isUseRMSE()) {
                        GlobalError = Math.sqrt(GlobalError);
                    }
                    pushError(GlobalError, getMonitor().getTotCicles() - getMonitor().getCurrentCicle());
                    getMonitor().setGlobalError(GlobalError);
                    GlobalError = 0;
                    patterns = 0;
                }
            } catch (IOException ioe) {
                log.warn("IOException while forwarding the influx. Message is : " + ioe.getMessage(),
                ioe);
            }
        }
        if (m_pattern.getCount() == -1) {
            stopTheNet();
            return;
        }
        firstTime = false;
        outs = new double[pattern.length];
        pattDesired = desired.fwdGet();
        if (m_pattern.getCount() != pattDesired.getCount()) {
            //log.warn("TeacherSynapse: No matching patterns - input#" + m_pattern.getCount() + " desired#" + pattDesired.getCount());
            new NetErrorManager(getMonitor(),"TeacherSynapse: No matching patterns - input#" + m_pattern.getCount() + " desired#" + pattDesired.getCount());
            return;
        }
        // The error calculation starts from the preLearning+1 pattern
        if (getMonitor().getPreLearning() < m_pattern.getCount()) {
            pDesired = pattDesired.getArray();
            if (pDesired != null) {
                double cost = 0;
                if(pDesired.length != outs.length) {
                    // if the desired output differs in size, we will back propagate
                    // an pattern of the same size as the desired output so the output 
                    // layer will adjust its size. The error pattern will contain zero 
                    // values so now learning takes place during this backward pass.
                    log.warn("Size output pattern mismatches size desired pattern." +
                        " Zero-valued desired pattern sized error pattern will be backpropagated.");
                    outs = new double[pDesired.length];
                } else {
                    for (x = 0; x < pDesired.length; ++x) {
                        outs[x] = pDesired[x] - pattern[x];
                        cost += outs[x] * outs[x]; // cost = SUM[(Dn - Yn)^2]
                    }
                    GlobalError += cost / 2;  // GE = SUM[ 1/2 SUM[(Dn - Yn)^2]]
                }
            }
        }
        ++patterns;
    }
    
    protected void stopTheNet() {
        pushError(0.0, -1);
        patterns = 0;
        firstTime = true;
        if (getMonitor() != null)
            new NetStoppedEventNotifier(getMonitor()).start();
    }
    
    public Pattern fwdGet() {
        synchronized (this) {
            while (getError().empty()) {
                try {
                    wait();
                } catch (InterruptedException ie) {   //e.printStackTrace();
                    //log.warn("wait() was interrupted. Message is : " + ie.getMessage());
                    return null;
                }
            }
            Pattern errPatt = (Pattern) error.pop();
            notify();
            return errPatt;
        }
    }
    
    public void fwdPut(Pattern pattern) {
        int step = pattern.getCount();
        if ((getMonitor() == null) || (!isEnabled())) {
            if (step == -1)
                stopTheNet();
            return;
        }
        if (!getMonitor().isLearning() && !getMonitor().isValidation()) {
            if (step == -1)
                stopTheNet();
            return;
        }
        
        super.fwdPut(pattern);
        if (step != -1) {
            if (!getMonitor().isLearningCicle(step)) {
                items = 0;
                //GlobalError = 0;
            }
        } else {
            items = 0;
            GlobalError = 0;
        }
    }
    
    /**
     * Inserire qui la descrizione del metodo.
     * Data di creazione: (11/04/00 1.12.04)
     * @return neural.engine.StreamInputSynapse
     */
    public StreamInputSynapse getDesired() {
        return desired;
    }
    
    /**
     * Insert the method's description here.
     * Creation date: (23/09/2000 2.16.17)
     * @return neural.engine.Fifo
     */
    private Fifo getError() {
        if (error == null)
            error = new Fifo();
        return error;
    }
    
    private void readObject(java.io.ObjectInputStream in)
    throws java.io.IOException, java.lang.ClassNotFoundException {
        in.defaultReadObject();
        firstTime = true;
        if (getMonitor()!=null) {
            getMonitor().setSupervisioned(true);
            getMonitor().addNeuralNetListener(this, false); // Add this layer as a net listener.
            //getMonitor().setGlobalError(0.0);
        }
    }
    
    public Pattern revGet() {
        if (isEnabled())
            return super.fwdGet();
        else
            return null;
    }
    
    public void revPut(Pattern pattern) {
        // Non usato.
    }
    
    /**
     * setArrays method comment.
     */
    protected void setArrays(int rows, int cols) {
    }
    
    /**
     * Set the input data stream containing desired training data
     * @param newDesired neural.engine.StreamInputSynapse
     */
    public boolean setDesired(StreamInputSynapse newDesired) {
        if (newDesired == null) {
            if (desired != null)
                desired.setInputFull(false);
            desired = newDesired;
        }
        else {
            if (newDesired.isInputFull())
                return false;
            desired = newDesired;
            desired.setStepCounter(false);
            desired.setOutputDimension(getInputDimension());
            desired.setInputFull(true);
        }
        return true;
    }
    
    public void resetInput() {
        if (getDesired() != null)
            getDesired().resetInput();
    }
    
    protected void setDimensions(int rows, int cols) {
    }
    
    public void setInputDimension(int newInputDimension) {
        super.setInputDimension(newInputDimension);
        if (getDesired() != null)
            getDesired().setOutputDimension(newInputDimension);
    }
    
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
        out.defaultWriteObject();
    }
    
    public TreeSet check() {
        TreeSet checks = super.check();
        
        if (desired == null) {
            checks.add(new NetCheck(NetCheck.ERROR, "Desired Input has not been set.", this));
        }
        else
            checks.addAll(desired.check());
        
        
        return checks;
    }
    
    /** reset of the input synapse
     *
     */
    public void reset() {
        super.reset();
        if (getDesired() != null)
            getDesired().reset();
    }
    
    /** Sets the Monitor object of the Teacher Synapse.
     * Adds this Techer Synapse as a NeuralNetListener so that it can reset after a critical error.
     * @param newMonitor neural.engine.Monitor
     */
    public void setMonitor(Monitor newMonitor) {
        super.setMonitor(newMonitor);
        if (getMonitor() != null) {
            this.getMonitor().setSupervisioned(true);
            this.getMonitor().addNeuralNetListener(this, false);
        }
    }
    
    // Neural Net Listener methods
    
    public void cicleTerminated(NeuralNetEvent e) {
    }
    
    public void errorChanged(NeuralNetEvent e) {
    }
    
    public void netStarted(NeuralNetEvent e) {
    }
    
    public void netStopped(NeuralNetEvent e) {
    }
    
    
    public void netStoppedError(NeuralNetEvent e, String error) {
        pushError(0.0, -1);
        patterns = 0;
        firstTime = true;
        this.reset();
    }
    
    public void init() {
        super.init();
        if (getDesired() != null)
            getDesired().init();
    }
    
}