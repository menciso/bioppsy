/*
 * ErrorBasedTerminator.java
 *
 * Created on October 28, 2004, 2:45 PM
 */

package org.joone.engine.listeners;

import org.joone.engine.*;
import org.joone.util.MonitorPlugin;

/**
 * Stops a network whenever the training error of the network falls below a 
 * certain value.
 *
 * @author  Boris Jansen
 */
public class ErrorBasedTerminator extends MonitorPlugin {
    
    /** The error level. Whenever the training error falls below this value 
     * the network should be stopped. */
    private double errorLevel;
    
    /** Creates a new instance of ErrorBasedTerminator */
    public ErrorBasedTerminator() {
    }
    
    /** 
     * Creates a new instance of ErrorBasedTerminator
     *
     * @param anErrorLevel the error level. A network having a training error
     * equal to or below this level will be stopped.
     */
    public ErrorBasedTerminator(double anErrorLevel) {
        errorLevel = anErrorLevel;
    }
    
    /**
     * Sets the error level. A network having a training error
     * equal to or below this level will be stopped.
     *
     * @param anErrorLevel the error level to set.
     */
    public void setErrorLevel(double anErrorLevel) {
        errorLevel = anErrorLevel;
    }
    
    /**
     * Gets the error level.
     *
     * @return the error level.
     */
    public double getErrorLevel() {
        return errorLevel;
    }

    protected void manageStop(Monitor mon) {
        
    }
    
    protected void manageCycle(Monitor mon) {
        
    }
    
    protected void manageStart(Monitor mon) {
        
    }
    
    protected void manageError(Monitor mon) {
        if(mon.getGlobalError() <= errorLevel) {
            mon.Stop();
        }
    }
    
    protected void manageStopError(Monitor mon, String msgErr) {
        
    }
}
