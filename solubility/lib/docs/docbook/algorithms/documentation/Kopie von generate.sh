#!/bin/sh

# create RTF from SGML
find . -name '*.sgml' -exec ${JADE} -t rtf -d $DB_PRINT {} \;
# create HTML from SGML
find . -name '*.sgml' -exec ${JADE} -t sgml -d $DB_HTML {} \;

# create PS and PDF versions
# stable JadeTeX->DVI ?
# does anyone know an automatic RTF2PS utility
# please mail me: wegnerj@informatik.uni-tuebingen.de
