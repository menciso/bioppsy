#!/bin/bash

# Create GIF images from LaTeX equations by using
# ImageMagick's convert method.
#
# http://www.imagemagick.org/
#
# These images have a nice layout and can be easily used
# for DocBook documents, HTML pages, Word !;-) ...
#
# Precondition:
# CONVERT environment variable must point to the convert executable.
#
# Author: Joerg K. Wegner (wegnerj@informatik.uni-tuebingen.de)

echo ""| cat > log.txt

# Traverse list of LaTeX files.
for filename in *.tex
do
  # Remove file extension
  file=${filename%.tex}
  echo DESCRIPTOR FORMULAS: generating image from ${file} ...
  echo ===========================------------------generating image from ${file} ... >>log.txt
  # Create DVI --> EPS --> GIF from LaTeX equation.
  latex ${file} >>log.txt
  dvips -E ${file}.dvi -o ${file}.eps 2>>log.txt >>log.txt
  ${CONVERT} -antialias ${file}.eps ${file}.gif >>log.txt
#  ${CONVERT} -antialias -scale 300%x300% ${file}.eps ${file}.gif >>log.txt
  # Remove temporary created files.
  rm -f ${file}.aux
  rm -f ${file}.dvi
  rm -f ${file}.eps
  rm -f ${file}.log
done

exit 0
