#!/bin/sh

if [ $# -ne 1 ]
then
   echo "$0: Wrong number of arguments" >&2
   echo tex2html TEXfile
   exit 1
else
  if test -f $1.tex;
  then
    tth -L$1 <$1.tex >$1.html
  else
   echo "$0: The file \"$1.tex\" does not exist" >&2
   exit 1
  fi
fi
