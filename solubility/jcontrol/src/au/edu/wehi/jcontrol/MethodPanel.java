/*
 * Created on: Apr 5, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import javax.swing.*;
import org.apache.log4j.Logger;

import au.edu.wehi.jcontrol.util.ClassFactory;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.lang.reflect.*;
import java.io.IOException;

/**
 * A MethodPanel is a JPanel that presents a Method to a
 * user. The user is able to enter parameters for the
 * Method. When a 'Run' button is clicked by the user
 * a new MethodRunner is created with the entered parameters
 * and the set Method. This MethodRunner is added to the
 * ConsoleFrame to run the Method.
 * 
 * @author eduthie
 * 
 */
public class MethodPanel extends JPanel
{
    private static Logger logger = Logger.getLogger(MethodPanel.class);
    private static String NAME = "Method";
    
    protected Class theClass; // Class the method comes from
    private Object object = null; // Object to run the method from
    private Vector<ObjectEntryPanel> objectEntryPanels;
    private Method method;
    
    protected MethodPanel()
    {
        super();
    }
    
    /**
     * Initialises the Class and Method. To be used for
     * static Methods.
     * 
     * @param theClass - Class Method comes from
     * @param method - Method to run
     */
    public MethodPanel(Class theClass, Method method)
    {
        super();
        add(new JLabel(method.toString()));
        construct(theClass);
        this.method = method;
        addArguments(method.getParameterTypes());
    }
    
    /**
     * Initialises the Class and Method. To be used for
     * non-static methods.
     * 
     * @param theClass - Class Method comes from
     * @param method - Method to run
     * @param object - Object to run non-static Method from
     */
    public MethodPanel(Class theClass, Method method, Object object)
    {
        super();
        add(new JLabel(method.toString()));
        construct(theClass);
        this.method = method;
        this.object = object;
        addArguments(method.getParameterTypes());
    }
    
    /**
     * Sets up the MethodPanel and adds Components.
     * 
     * @param theClass
     */
    public void construct(Class theClass)
    {
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        setBorder(BorderFactory.createTitledBorder(NAME));
        objectEntryPanels = new Vector<ObjectEntryPanel>();
        this.theClass = theClass;
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        setAlignmentX(Component.LEFT_ALIGNMENT);
        JButton button = new JButton("Run");
        button.addActionListener(new RunActionListener());
        add(button);
    }

    /**
     *  Displays an argument of the given class
     *  at the end of the MethodPanel
     *  
     * @param argClass - Class of argument to display
     */
    public void addArgument( Class argClass )
    {
        ObjectEntryPanel oep = new ObjectEntryPanel(argClass);
        objectEntryPanels.add(oep);
        add(oep);
    }
    
    /**
     * Displays an array of arguments of the given classes,
     * in order, at the end of the MethodPanel.
     * 
     * @param argClasses - Array of argument Class Objects ordered
     * according to their order in the Method.
     */
    public void addArguments( Class[] argClasses )
    {
        for( int i=0; i < argClasses.length; ++i)
        {
            Class theClass = ClassFactory.instance().getClassFromInBuilt(argClasses[i]);
            addArgument(theClass);
        }
    }
    
    /**
     * Listens to when a user clicks on the 'Run' button.
     */
    public class RunActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            collectArgumentsAndRun();
        }
    }
    
    /**
     * Retrieves an up-to-date copy of all the arguments entered
     * by the user and runs the Method using these arguments.
     */
    public void collectArgumentsAndRun()
    {
        Object[] arguments;
        ObjectEntryPanel oep;
        Object object;
        
        arguments = new Object[objectEntryPanels.size()];
        
        for( int i=0; i < objectEntryPanels.size(); ++i )
        {
            try
            {
                oep = objectEntryPanels.get(i);
                object = oep.getObject();
                arguments[i] = object;
            }
            catch( IOException ioe )
            {
                arguments[i] = null;
            }
        }
        
        runMethod(arguments);
    }
    
    /**
     * Creates a new MethodRunner with the given arguments for the
     * set Method and runs it in the ConsoleFrame.
     *  
     * @param args - collected parameters for Method.
     */
    public void runMethod(Object[] args)
    {
        if( method != null )
        {
            MethodRunner mr;
            if( object != null)
            {
                mr = new MethodRunner(args,method,object);
            }
            else
            {
                mr = new MethodRunner(args,method);
            }
            Bridge.instance().runMethodRunner(mr);
        }
        else
        {
            logger.error("No method to run, MethodPanel.runMethod()");
        }
    }
}
