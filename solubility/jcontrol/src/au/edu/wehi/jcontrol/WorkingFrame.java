/*
 * Created on: Apr 21, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import java.util.Hashtable;
import javax.swing.*;
import net.infonode.docking.View;
import org.apache.log4j.Logger;
import java.awt.*;

/**
 * @author eduthie
 */
public class WorkingFrame extends View
{
    public static String WORKING_FRAME = "Workspace";
    
    private JPanel content;
    
    private static Logger logger = Logger.getLogger(WorkingFrame.class);
    private static String CREATE_OBJECT = "Class";
    private static String OBJECT_VIEW = "Object View";
    
    private JTabbedPane tabbedPane = null;
    private Hashtable<String,Item> items;
    
    public WorkingFrame()
    {
        super(WORKING_FRAME, null, new JPanel());
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        items = new Hashtable<String,Item>();
        tabbedPane = new JTabbedPane();
        tabbedPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        content.add(tabbedPane);
        addComponents();
    }
    
    /**
     *  Creates all the components of the frame
     */
    private void addComponents()
    {
        JPanel createObject = new ClassPanel();
        Item createObjectItem = new Item(CREATE_OBJECT,createObject,true);
        items.put(CREATE_OBJECT,createObjectItem);
        displayItem(createObjectItem);
        
        JPanel objectView = new ObjectViewContainer();
        Item objectViewItem = new Item(OBJECT_VIEW,objectView,true);
        items.put(OBJECT_VIEW,objectViewItem);
        displayItem(objectViewItem);
    }
    
    /**
     * Contains a JPanel and a MenuItem and whether
     * the item is displayed
     */
    public class Item
    {
        public String name;
        public JPanel jPanel;
        public boolean displayed = false;
        
        public Item(String name, JPanel jPanel, boolean displayed)
        {
            this.name = name;
            this.jPanel = jPanel;
            this.displayed = displayed;
        }
    }

    
    public void displayItem(Item item)
    {
        tabbedPane.addTab(item.name,item.jPanel);
        item.displayed = true;
        tabbedPane.setSelectedComponent(item.jPanel);
    }
    
    /**
     * Displays the given Object in the ObjectViewContainer.
     * 
     * @param objectHolder - ObjectHolder containing Object to
     * display
     */
    public void displayObject(ObjectHolder objectHolder)
    {
        Item item = items.get(OBJECT_VIEW);
        ObjectViewContainer ovc = (ObjectViewContainer) item.jPanel;
        ovc.addObject(objectHolder);
        if( !item.displayed )
        {
            displayItem(item);
        }
        tabbedPane.setSelectedComponent(item.jPanel);
    }

}
