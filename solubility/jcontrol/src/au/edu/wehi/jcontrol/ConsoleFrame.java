/*
 * Created on: Apr 21, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import java.awt.Component;
import javax.swing.*;
import net.infonode.docking.View;
import org.apache.log4j.Logger;

/**
 * A ConsoleFrame contains a tabbed pane that displays the
 * execution of methods using MethodRunner objects.
 * 
 * @author eduthie
 */
public class ConsoleFrame extends View
{
    public static String CONSOLE_FRAME = "Console";
    
    private JPanel content;
    private JTabbedPane tabbedPane;
    
    public ConsoleFrame()
    {
        super(CONSOLE_FRAME, null, new JPanel());
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        tabbedPane = new JTabbedPane();
        content.add(tabbedPane);
    }
    
    /**
     * Adds the given MethodRunner to the JTabbedPane
     * and starts its execution in a new thread.
     * 
     * @param mr - MethodRunner to run
     */
    public void addTab(MethodRunner mr)
    {
        tabbedPane.add(mr);
        tabbedPane.setSelectedComponent(mr);
        Thread methodThread = new Thread(mr,"Method");
        methodThread.start();
    }
    
    public void removeTab(Component component)
    {
        tabbedPane.remove(component);
    }
}
