/*
 * Created on: Apr 20, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import javax.swing.tree.*;
import org.apache.log4j.Logger;
import java.util.Enumeration;
import javax.swing.JTree;

/**
 * An ObjectTreeModel is a DefaultTreeModel that caters for
 * DefaultMutableTree nodes which have ObjectHolder objects
 * as their user objects. When a new node is added as a child
 * to a parent DefaultMutableTreeNode, if the new node is
 * identical (using ObjectHolder.equals()) to a current
 * child of the target parent, the node is not added but
 * the current equal child is selected. Please use the
 * insertNodeInto(MutableTreeNode,MutableTreeNode,int,JTree)
 * for this functionality as the JTree is required to select
 * the current existing child. 
 * 
 * @author eduthie
 */
public class ObjectTreeModel extends DefaultTreeModel
{
    private static Logger logger = Logger.getLogger(ObjectTreeModel.class);
    
    /**
     * @param root
     */
    public ObjectTreeModel(TreeNode root)
    {
        super(root);
    }

    /**
     * @param root
     * @param asksAllowsChildren
     */
    public ObjectTreeModel(TreeNode root, boolean asksAllowsChildren)
    {
        super(root, asksAllowsChildren);
    }
    
    /**
     * Invoked this to insert newChild at location index in parents children. 
     * This will then message nodesWereInserted to create the appropriate event. 
     * This is the preferred way to add children as it will create the appropriate event.
     * <br><br>
     * If the given child already exists as a child of the given parent, the already
     * existing child is selected in the given JTree. The new child is not added.
     * The children must be DefaultMutableTree nodes and have ObjectHolders as
     * user objects. The ObjectHolder.equals() method is used to compare children.
     * <br><br>
     * @param newChild - DefaultMutableTreeNode containing an ObjectHolder as its
     * user object.<br>
     * @param parent - DefaultMutableTreeNode containing (DefaultMutableTreeNodes with
     * ObjectHolders as their user object) as childern.
     * @param JTree - JTree displaying all the nodes. Required so that if the 
     * requested child already exists it can be selected.
     */
    public void insertNodeInto(MutableTreeNode newChild,
            MutableTreeNode parent,
            int index,
            JTree tree)
    {
        DefaultMutableTreeNode newChildDMTN =
            (DefaultMutableTreeNode) newChild;
        Enumeration children = parent.children();
        while( children.hasMoreElements() )
        {
            DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) children.nextElement();
            ObjectHolder child = ((ObjectHolder) dmtn.getUserObject());
            
            ObjectHolder newChildOH = 
                (ObjectHolder) (newChildDMTN.getUserObject());
            if( child.equals(newChildOH) )
            {
                TreePath path = new TreePath(dmtn.getPath());
                tree.setSelectionPath(path);
                return;
            }
        }
        super.insertNodeInto(newChild,parent,index);
        TreePath path = new TreePath(newChildDMTN.getPath());
        tree.setSelectionPath(path);
    }

}
