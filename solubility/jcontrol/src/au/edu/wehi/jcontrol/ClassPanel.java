/*
 * Created on: Apr 1, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import org.apache.log4j.Logger;
import au.edu.wehi.jcontrol.util.ClassFactory;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.lang.reflect.*;
import net.infonode.docking.View;

/**
 * A ClassPanel is one of the tabs availaible
 * in the WorkingFrame tabbedPane. It provides an
 * interface for the user to create a new object which
 * is displayed in the ObjectView and added to the
 * main ObjectCollection
 * 
 * @author eduthie
 *
 */
public class ClassPanel extends View
{
    private static Logger logger = Logger.getLogger(ClassPanel.class);
    
    public static String CLASS_FRAME = "Class";
    
    private JPanel content;
    
    private JPanel chooserPanel;
    private ClassChooser classChooser;
    private MethodChooser constructorChooser = null;
    private Class theClass = null;
    private MethodPanel methodPanel = null;
    
    public ClassPanel()
    {
        super(CLASS_FRAME, null, new JPanel());
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        chooserPanel = new JPanel();
        chooserPanel.setLayout(new BoxLayout(chooserPanel,BoxLayout.LINE_AXIS));
        chooserPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        classChooser = new ClassChooser();
        classChooser.addActionListener(new DisplayConstructorsActionListener());
        chooserPanel.add(classChooser);
        content.add(chooserPanel);
    }
    
    /**
     * @author eduthie
     * 
     * Listens for ActionEvents indicating that the user has double-clicked
     * on a class in the tree of displayed classes. If the given class
     * (obtained from the ActionEvent.getActionCommand()) is available
     * the constructors and static methods are displayed in a new
     * MethodChooser JPanel. Otherwise an error message is displayed.
     */
    public class DisplayConstructorsActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String name = null;
            
            try
            {
                name = e.getActionCommand();
                theClass = ClassFactory.instance().getClass(name);
                displayConstructors();
            }
            catch(ClassNotFoundException cnfe)
            {
                String message = "CLASS NOT AVAILABLE";
                logger.debug(message + ": " + name);
                JOptionPane.showMessageDialog(null, name, message, JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * @author eduthie
     *
     * Listens for ActionEvents triggered when a user selects a constructor
     * or static method from the MethodChooserPanel. The ActionEvent.getSource()
     * object is the method that the user wishes to use. A panel allowing the
     * users to enter parameters for the method is displayed (a MethodPanel)
     * in the ClassPanel.
     */
    public class DisplayParametersActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if( e.getActionCommand().equals(MethodChooser.STATIC_METHODS) )
            {
                displayMethod((Method) e.getSource());
            }
            else if( e.getActionCommand().equals(MethodChooser.CONSTRUCTORS) )
            {
                displayConstructor((Constructor) e.getSource());
            }
            else
            {
                logger.error("Invalid ActionEvent: " + e.getActionCommand());
            }
        }
    }
    
    /**
     * Creates a MethodChooser JPanel for the current Class and
     * adds it to the ClassPanel. Users can then view and select
     * constructors and static methods from the Class. A
     * DisplayParametersActionListener is used to listen for
     * when a user selects a constructor/static method to use.
     */
    public void displayConstructors()
    {
        if( constructorChooser != null )
        {
            chooserPanel.remove(constructorChooser);
        }
        constructorChooser = new MethodChooser(theClass);
        constructorChooser.addActionListener(new DisplayParametersActionListener());
        chooserPanel.add(constructorChooser);
        revalidate();
    }
    
    /**
     * Displays the given Method in a MethodPanel in the current
     * ClassPanel. A MethodPanel allows the user to enter parameters
     * for a method before running it.
     * 
     * @params object - a Method or Constructor to display in a 
     * MethodPanel
     */
    public void displayMethod(Method method)
    {  
        changeMethodPanel(
                MethodPanelFactory.createMethodPanel(theClass,method));
    }
    
    /**
     * Displays the given Constructor in a MethodPanel in the current
     * ClassPanel. A MethodPanel allows the user to enter parameters
     * for a method before running it.
     * 
     * @params object - a Constructor to display in a 
     * MethodPanel
     */
    public void displayConstructor(Constructor constructor)
    {  
        changeMethodPanel(
                MethodPanelFactory.createMethodPanel(theClass,constructor));
    }
    
    /**
     * Swaps the current MethodPanel with the given newPanel, or just adds
     * it if there is no current MethodPanel.
     */
    public void changeMethodPanel(MethodPanel newPanel)
    {
        if( methodPanel != null)
        {
            content.remove(methodPanel);
        }
        methodPanel = newPanel;
        content.add(methodPanel);
        revalidate();
    }
}
