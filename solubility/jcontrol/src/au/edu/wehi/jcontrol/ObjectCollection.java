/*
 * Created on: Apr 21, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import java.io.IOException;
import java.util.Hashtable;
import javax.swing.*;
import javax.swing.tree.*;
import net.infonode.docking.View;
import org.apache.log4j.Logger;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.awt.Component;

/**
 * An ObjectCollection contains ObjectHolder objects. These
 * are sorted according to the package name of the Object held
 * by the ObjectHolder class. ObjectHolders are displayed in a 
 * JTree as child nodes of the package they belong to. Objects
 * can be added using the addObject() method or can be dragged
 * and dropped by the user. When the user double clicks on an 
 * Object in the ObjectCollection it is displayed in the
 * ObjectViewContainer of the WorkingFrame of the application.
 * 
 * @author eduthie
 */
public class ObjectCollection extends View
{
    public static String OBJECT_COLLECTION = "Object Collection";
    
    private JPanel content;
    
    private static Logger logger = Logger.getLogger(ObjectCollection.class);
    
    private JTree tree;
    private ObjectTreeModel treeModel;
    private DefaultMutableTreeNode root;
    private Hashtable<String,DefaultMutableTreeNode> packages;
    
    public ObjectCollection()
    {
        super(OBJECT_COLLECTION, null, new JPanel());
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        packages = new Hashtable<String,DefaultMutableTreeNode>();
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        root = new DefaultMutableTreeNode("Objects");
        treeModel = new ObjectTreeModel(root);
        tree = new JTree(treeModel);
        tree.setDragEnabled(true);
        tree.setShowsRootHandles(true);
        tree.setTransferHandler(new ObjectCollectionTransferHandler());
        tree.addMouseListener(new DragMouseListener());
        JScrollPane scrollPane = new JScrollPane(tree);
        scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        content.add(scrollPane);
    }
    
    /**
     * Adds the given ObjectHolder to the JTree of this
     * ObjectCollection. The ObjectHolder is put under the
     * subnode containing the package name of the Object in
     * the given ObjectHolder.
     * 
     * @param objectHolder - ObjectHolder to add to JTree
     */
    public void addObject(ObjectHolder objectHolder)
    {
        String packageName = objectHolder.getPackageName();
        DefaultMutableTreeNode packageNode = packages.get(packageName);
        if( packageNode == null )
        {
            packageNode = new DefaultMutableTreeNode(packageName);
            treeModel.insertNodeInto(packageNode,root,root.getChildCount());
            packages.put(packageName,packageNode);
        }
        DefaultMutableTreeNode objectNode =
            new DefaultMutableTreeNode(objectHolder);
        treeModel.insertNodeInto(objectNode,packageNode,packageNode.getChildCount(),tree);
    }
    
    /**
     * Returns a Hastable mapping the package name to the package
     * tree node.
     */
    public Hashtable<String,DefaultMutableTreeNode> getPackages()
    {
        return packages;
    }
    
    /**
     * Allows ObjectHolder object to be dragged into the tree. The dragged
     * ObjectHolder is put under a sub-node labled by its package name.
     * If a sub-node with the given ObjectHolder's package name does
     * not exist it is created and added to the tree.
     */
    public class ObjectCollectionTransferHandler extends TransferHandler
    {
        /**
         * Indicates whether a component would accept an import of the given 
         * set of data flavors prior to actually attempting to import it.
         */
        public boolean canImport(JComponent comp, DataFlavor[] transferFlavors)
        {
            for( int i=0; i < transferFlavors.length; ++i )
            {
                if( !transferFlavors[i].equals(ObjectHolder.DATA_FLAVOR))
                {
                    return false;
                }
            }
            return true;
        }
        
        /**
         * Causes a transfer to a component from a clipboard or a DND drop operation.
         */
        public boolean importData(JComponent comp, Transferable t) 
        {
            try
            {
                Object object = t.getTransferData(ObjectHolder.DATA_FLAVOR);
                ObjectHolder oh = (ObjectHolder) object;
                addObject(oh);
                return true;
            }
            catch(UnsupportedFlavorException usfe)
            {
                logger.error(usfe.getMessage());
                return false;
            }
            catch( IOException ioe )
            {
                logger.error(ioe.getMessage());
                return false;
            }
        }
    }

    /**
     * Allows the user to drag ObjectHolder Objects from the tree,
     * extracting them from the current selected node.
     * The TransferHandler.exportAsDrag() method of the
     * ObjectHolder method is called whenever a user clicks
     * on an node in the tree.<br><br>
     * 
     * When a node in the tree is double clicked, the Object it
     * represents is displayed in the WorkingFrame's 
     * ObjectViewContainer and focus is shifted to that view.
     */
    public class DragMouseListener extends MouseAdapter 
    {
        
        public void mousePressed(MouseEvent e) 
        {
            int selRow = tree.getRowForLocation(e.getX(), e.getY());
            TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
            if( selPath != null )
            {
                DefaultMutableTreeNode node = 
                    (DefaultMutableTreeNode) selPath.getLastPathComponent();
                Object object = node.getUserObject();
                
                if( object.getClass().getName().equals(ObjectHolder.class.getName()) )
                {
                    JComponent c = (JComponent) object;
                    
                    if(selRow != -1) 
                    {
                        if(e.getClickCount() == 1) 
                        {
                            TransferHandler handler = c.getTransferHandler();
                            handler.exportAsDrag(c,e,TransferHandler.COPY);
                        }
                        else if(e.getClickCount() == 2) 
                        {
                            ObjectHolder oh = (ObjectHolder) c;
                            Bridge.instance().displayObject(oh);
                        }
                    }
                }
            }
        }
    }


}
