/*
 * Created on: Apr 1, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;
import org.apache.log4j.Logger;
import au.edu.wehi.jcontrol.util.ClassFactory;
import java.lang.reflect.*;
import au.edu.wehi.jcontrol.util.TreeMouseAdapter;

/**
 * A MethodChooser is a JPanel that dispalys a tree of 
 * all available constructors and static methods for a chosen
 * class. Initialise a MethodPanel by providing it the Class
 * which to display. The user can then select methods from
 * the tree. When a method is a selected, the ActionListener
 * registered with MethodChooser.addActionListener() is sent
 * an ActionEvent. There are two possible ActionEvents:<br><br>
 * 
 * 1. When a Constructor is chosen, an ActionEvent with the action command:
 * MethodChooser.CONSTRUCTORS is triggered. The source object of
 * the ActionEvent is the Constructor object chosen.<br>
 * 2. When a Static Method is chosen, and ActionEvent with the action command:
 * MethodChooser.STATIC_METHODS is triggered. The source object of
 * the ActionEvent is the Method object chosen.
 * 2. When a non-static Method is chosen, and ActionEvent with the action 
 * command: MethodChooser.METHODS is triggered. The source object of
 * the ActionEvent is the Method object chosen.
 * 
 * @author eduthie
 * 
 */
public class MethodChooser extends JPanel
{
    private static Logger logger = Logger.getLogger(MethodChooser.class);
    
    /**
     * ActionEvent.getActionCommand() if Constructor is chosen
     */
    public static String CONSTRUCTORS = "Constructors";
    /**
     * ActionEvent.getActionCommand() if Static Method is chosen
     */
    public static String STATIC_METHODS = "Static Methods";
    /**
     * ActionEvent.getActionCommand() if Method is chosen
     */
    public static String METHODS = "Methods";
    
    private Class theClass; // class being displayed
    private JTree tree;
    // listener called when a user selects a method
    private ActionListener listener;
    private Object object = null;  // object being displayed
    
    /**
     * @param theClass - class to display constructors and
     * static methods of
     */
    public MethodChooser(Class theClass)
    {
        super();
        setup(theClass);
    }
    

    public MethodChooser(Object object)
    {
        super();
        this.object = object;
        setup(object.getClass());
    }
    
    private void setup(Class theClass)
    {
        this.theClass = theClass;
        setBorder(BorderFactory.createTitledBorder(theClass.getName()));
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        addTree();
    }
    
    /**
     * Creates the tree.
     */
    private void addTree()
    {
        DefaultMutableTreeNode root =
            new DefaultMutableTreeNode("Methods");
        addConstructors(root);
        addStaticMethods(root);
        if( object != null )
        {
            addMethods(root);
        }
        tree = new JTree(root);
        tree.getSelectionModel().setSelectionMode
           (TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.setAlignmentX(Component.LEFT_ALIGNMENT);
        MouseListener ml = new TreeMouseAdapter(tree) 
        {
            public void selected(int selRow, TreePath selPath) 
            {
                methodSelected(selRow,selPath);
            }
        };
        tree.addMouseListener(ml);
        JScrollPane scrollPane = new JScrollPane(tree);
        add(scrollPane);
    }
    
    /**
     * Adds the Constructor nodes to the given root node.
     */
    private void addConstructors(DefaultMutableTreeNode root)
    {
        DefaultMutableTreeNode cRoot = 
            new DefaultMutableTreeNode(CONSTRUCTORS);
        Constructor[] cs = theClass.getConstructors();
        for( int i=0; i < cs.length; ++i )
        {
            Constructor c = cs[i];
            DefaultMutableTreeNode node =
                new DefaultMutableTreeNode(c);
            cRoot.add(node);
        }
        root.add(cRoot);
    }
    
    /**
     * Adds the Static Method nodes to the given root node.
     */
    private void addStaticMethods(DefaultMutableTreeNode root)
    {
        DefaultMutableTreeNode smRoot =
            new DefaultMutableTreeNode(STATIC_METHODS);
        
        Method[] methods = theClass.getMethods();
        for( int i=0; i < methods.length; ++i )
        {
            Method method = methods[i];
            if( ClassFactory.instance().isStatic(method) )
            {
                DefaultMutableTreeNode methodNode =
                    new DefaultMutableTreeNode(method);
                smRoot.add(methodNode);
            }
        }
        
        root.add(smRoot);
    }
    
    /**
     * Adds the non-static Method nodes to the given root node.
     */
    private void addMethods(DefaultMutableTreeNode root)
    {
        DefaultMutableTreeNode mRoot =
            new DefaultMutableTreeNode(METHODS);
        
        Method[] methods = theClass.getMethods();
        for( int i=0; i < methods.length; ++i )
        {
            Method method = methods[i];
            if( !ClassFactory.instance().isStatic(method) )
            {
                DefaultMutableTreeNode methodNode =
                    new DefaultMutableTreeNode(method);
                mRoot.add(methodNode);
            }
        }
        
        root.add(mRoot);
    }
    
    /**
     * This function is called whenever the user selects a node in the tree.
     * If the node is a leaf node (i.e. represents a Constructor or Method),
     * the appropriate ActionEvent is triggered in the current listener.
     * 
     * @param row - row number of selected method
     * @param path - path to node of selected method
     */
    private void methodSelected(int row, TreePath path)
    {
        if( listener == null )
        {
            return;
        }
        Object object = ( (DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject();
        if( object.getClass().getName().equals(Constructor.class.getName()) )
        {
            listener.actionPerformed(new ActionEvent(object,ActionEvent.ACTION_FIRST,CONSTRUCTORS));
        }
        else if( object.getClass().getName().equals(Method.class.getName()) )
        {
            Method method = (Method) object;
            if(ClassFactory.instance().isStatic(method))
            {
                listener.actionPerformed(new ActionEvent(object,ActionEvent.ACTION_FIRST,STATIC_METHODS));
            }
            else
            {
                listener.actionPerformed(new ActionEvent(object,ActionEvent.ACTION_FIRST,METHODS));
            }
        }
    }
    
    /**
     * Sets the ActionListener. When a user selects a Constructor or Method
     * an ActionEvent is triggered with the set ActionListener. This 
     * ActionEvent has the following properties:<br><br>
     * 
     * 1. action command - set to MethodChooser.CONSTRUCTORS, MethodChooser.STATIC_METHODS
     *    or MethodChooser.METHODS
     * 2. source object - set to the Constructor or Method object chosen
     * 
     * @param listener - an ActionListener
     */
    public void addActionListener(ActionListener listener)
    {
        this.listener = listener;
    }

}
