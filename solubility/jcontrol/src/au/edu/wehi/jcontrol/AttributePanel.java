/*
 * Created on: Apr 20, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import javax.swing.*;
import org.apache.log4j.Logger;
import java.awt.*;
import java.lang.reflect.*;

/**
 * An AttrubutePanel displays the attributes of the given object in a JPanel.
 * The first item that is displayed is a JTextField containing the 
 * toString() result of the given Object. Then all the accessable attributes
 * are displayed as ObjectHolder JPanels.
 * 
 * @author eduthie
 */
public class AttributePanel extends JPanel
{
    private static Logger logger = Logger.getLogger(AttributePanel.class);
    
    private static int HEIGHT = 200;
    private static int WIDTH = 200;
    private static int TEXT_HEIGHT = 30;
    
    private JScrollPane scrollPane;
    private JPanel panel;
    
    private Object object;
    
    /**
     * @param object - Object to display attributes of
     */
    public AttributePanel(Object object)
    {
        super();
        this.object = object;
        display();
    }
    
    public void display()
    {
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        setPreferredSize(new Dimension(WIDTH,HEIGHT));
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
        panel.setPreferredSize(new Dimension(WIDTH,HEIGHT));
        JTextField stringLabel = new JTextField(object.toString());
        stringLabel.setMaximumSize(new Dimension(WIDTH,TEXT_HEIGHT));
        stringLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        stringLabel.setBorder(BorderFactory.createEtchedBorder());
        panel.add(stringLabel);
        displayAttributes();
        scrollPane = new JScrollPane(panel);
        add(scrollPane);
    }
    
    /**
     * Adds all the attrubutes as ObjectHolders to panel
     */
    public void displayAttributes()
    {
        Field[] fields = object.getClass().getDeclaredFields();
        for( int i=0; i < fields.length; ++i )
        {
            try
            {
                Field field = fields[i];
                Object attObject = field.get(object);
                if( attObject == null )
                {
                    attObject = field.get(null);
                }
                if( attObject != null )
                {
                    ObjectHolder objectHolder = new ObjectHolder(attObject);
                    objectHolder.setName(field.getName());
                    panel.add(objectHolder);
                }
            }
            catch( IllegalAccessException iae )
            {
                // this will happen
            }
        }
    }

}
