/*
 * Created on: Apr 6, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import org.apache.log4j.Logger;
import java.lang.reflect.Constructor;

/**
 * A ConstructorMethodRunner is a MethodRunner that uses
 * a Constructor instead of a Method. 
 * 
 * @author eduthie
 *
 */
public class ConstructorMethodRunner extends MethodRunner
{
    private static Logger logger = Logger.getLogger(ConstructorMethodRunner.class);
    
    private Constructor constructor;

    /**
     * Please use this constructor to provide a Constructor instead of 
     * a Method.
     */
    public ConstructorMethodRunner(Object[] args, Constructor constructor)
    {
        super(args);
        this.constructor = constructor;
    }
    
    /**
     * See MethodRunner.runMethod()
     */
    public Object runMethod() throws Exception
    {
        return constructor.newInstance(getArgs());
    }
}
