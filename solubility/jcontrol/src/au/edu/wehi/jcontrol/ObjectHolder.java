/*
 * Created on: Apr 11, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import org.apache.log4j.Logger;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import javax.swing.border.*;

/**
 * An ObjectHolder contains and instance of
 * Object and has a name. The name is displayed
 * to the user and can be changed by the user.
 * ObjectHolders can be dragged and dropped
 * to a ObjectCollection or a ObjectEntryPanel.
 * 
 * @author eduthie
 *
 */
public class ObjectHolder extends JLabel implements Transferable
{
    private static Logger logger = Logger.getLogger(ObjectHolder.class);
    /**
     * MIME type of this ObjectHolder
     */
    public static String DATA_FLAVOR_MIME = DataFlavor.javaJVMLocalObjectMimeType +
         ";class=" + ObjectHolder.class.getName();
    /**
     * DataFlavor of this ObjectHolder. getHumanPresentableName() for
     * this DataFlavor will return the class name of the object held
     * in this ObjectHolder.
     */
    public static DataFlavor DATA_FLAVOR;
   
    private static int lastNewName = 1;
    
    private Object object;
    private String name;
    
    /**
     * Creates a new ObjectHolder holding the given Object.
     * The user identifiable name for this ObjectHolder is
     * set to 'New Object NUMBER' where NUMBER is the 
     * sequence number of the Object since the start of
     * the application.
     * 
     * @param object - the Object to hold
     */
    public ObjectHolder(Object object)
    {
        super();
        try
        {
            DATA_FLAVOR = new DataFlavor(DATA_FLAVOR_MIME);
            DATA_FLAVOR.setHumanPresentableName(object.getClass().getName());
        }
        catch( ClassNotFoundException cnfe )
        {
            logger.error("Unable to create DATA_FLAVOR, " + cnfe.getMessage());
        }
        this.object = object;
        this.name = "New Object " + lastNewName++;
        setText(name);
        String fullClassName = object.getClass().getName();
        int lastDotIndex = fullClassName.lastIndexOf(".");
        String suffix = fullClassName.substring(lastDotIndex+1);
        Border blueBorder = 
            BorderFactory.createTitledBorder(
                    BorderFactory.createLineBorder(Color.BLUE),
                    suffix);
        setBorder(blueBorder);
        MouseListener listener = new DragMouseAdapter();
        addMouseListener(listener);
        setTransferHandler(new ObjectHolderTransferHandler());
    }
    
    /**
     * @return - the object held by this ObjectHolder
     */
    public Object getObject()
    {
        return object;
    }
    
    /**
     * Sets the user identifiable name of the object.
     */
    public void setName(String name)
    {
        this.name = name;
        setText(name);
    }
    
    /**
     * Gets the user identifiable name of the object.
     */
    public String getName()
    {
        return name;
    }

    public class ObjectHolderTransferHandler extends TransferHandler
    {
        
        public ObjectHolderTransferHandler()
        {
            super();
        }
        
        protected Transferable createTransferable(JComponent c)
        {
            return (ObjectHolder) c;
        }
        
        public int getSourceActions(JComponent c)
        {
            return COPY;
        }
    }
    
    public class DragMouseAdapter extends MouseAdapter {
        
        public void mousePressed(MouseEvent e) 
        {
            if(e.getClickCount() == 1) 
            {
                JComponent c = (JComponent)e.getSource();
                TransferHandler handler = c.getTransferHandler();
                handler.exportAsDrag(c, e, TransferHandler.COPY);
            }
            else if(e.getClickCount() == 2) 
            {
                 displayObject();
            }
        }
    }
    
    /**
     * Displays this ObjectHolder in the WorkingPanel's 
     * ObjectViewContainer as well as adding it to the
     * ObjectCollection
     */
    public void displayObject()
    {
        Bridge.instance().displayObject(this);
        Bridge.instance().addObject(this);
    }

    /**
     * Returns the package name of the Object held.
     */
    public String getPackageName()
    {
        String name = object.getClass().getName();
        int lastDotIndex = name.lastIndexOf('.');
        String packageName = name.substring(0,lastDotIndex);
        return packageName;
    }
    
    // The following methods are reqired for the Transferable interface
    
    /**
     *     Returns an object which represents the data to be transferred.
     */
    public Object getTransferData(DataFlavor flavor)
    {
        if( flavor.equals(DATA_FLAVOR) )
        {
            return this;
        }
        else
        {
            return null;
        }
    }

     /**
      * Returns an array of DataFlavor objects indicating the flavors
      *  the data can be provided in.
      */
    public DataFlavor[] getTransferDataFlavors()
    {
        DataFlavor[] flavors = {DATA_FLAVOR};
        return flavors;
    }
    
    /**
     * Returns true if the given DataFlavor is supported, false
     * otherwise.
     */
    public boolean isDataFlavorSupported(DataFlavor flavor)
    {
        if( flavor.equals(DATA_FLAVOR) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public String toString()
    {
        return object.getClass().getSimpleName() + ": " + name;
    }
    
    /**
     * Two ObjectHolders are equals if they have the same name and
     * contain the same Object (comparing objects by their address
     * rather than by the Object.equals() method).
     */
    public boolean equals(Object other)
    {
        if( other.getClass().getName().equals(getClass().getName()) )
        {
            ObjectHolder otherObjectHolder =
                (ObjectHolder) other;
            if( otherObjectHolder.getName().equals(getName()) && 
                (otherObjectHolder.getObject() == getObject()) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
