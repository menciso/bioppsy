/*
 * Created on: Mar 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import org.apache.log4j.Logger;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;

/**
 * An ObjectView displays an Object from a given ObjectHolder. A
 * MethodPanel is shown allowing the user to select any constructors,
 * static methods, or non-static methods of the Object. When
 * they are selected they are displayed as a MethodPanel at
 * the bottom of the ObjectView, allowing the user to run the
 * selected method. An AttributePanel is also shown, displaying the
 * attributes and a string representation of the Object.
 * 
 * @author eduthie
 */
public class ObjectView extends JPanel
{
    private static Logger logger = Logger.getLogger(ObjectView.class);
    
    private ObjectHolder objectHolder;
    private MethodPanel methodPanel = null;
    
    /**
     * @param objectHolder - ObjectHolder containing Object to display
     */
    public ObjectView(ObjectHolder objectHolder)
    {
        super();
        this.objectHolder = objectHolder;
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        addComponents();
    }
    
    /**
     * Creates and adds all components.
     */
    public void addComponents()
    {
        JPanel topPanel = new JPanel();
        topPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        topPanel.setLayout(new BoxLayout(topPanel,BoxLayout.LINE_AXIS));
        AttributePanel attributePanel = new AttributePanel(objectHolder.getObject());
        topPanel.add(attributePanel);
        MethodChooser methodChooser = 
            new MethodChooser(objectHolder.getObject());
        methodChooser.addActionListener(new MethodChooserActionListener());
        topPanel.add(methodChooser);
        add(topPanel);
    }
    
    /**
     * When the user selects a Method using the MethodChooser in the ObjectView,
     * a display of the Method (a MethodPanel) is added to the ObjectView.
     * 
     * @author eduthie
     */
    public class MethodChooserActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if( e.getActionCommand().equals(MethodChooser.CONSTRUCTORS) )
            {
                Constructor constructor = (Constructor) e.getSource();
                ConstructorMethodPanel newMethodPanel = 
                    new ConstructorMethodPanel(
                        objectHolder.getObject().getClass(),
                        constructor);
                displayMethodPanel(newMethodPanel);
            }
            else if( e.getActionCommand().equals(MethodChooser.STATIC_METHODS) )
            {
                Method method = (Method) e.getSource();
                MethodPanel newMethodPanel = new MethodPanel(
                        objectHolder.getObject().getClass(),
                        method);
                displayMethodPanel(newMethodPanel);
            }
            else if( e.getActionCommand().equals(MethodChooser.METHODS) )
            {
                Method method = (Method) e.getSource();
                MethodPanel newMethodPanel = new MethodPanel(
                        objectHolder.getObject().getClass(),
                        method,
                        objectHolder.getObject());
                displayMethodPanel(newMethodPanel);
            }
        }
    }
    
    /**
     * Displays the given new MethodPanel at the bottom of the ObjectView,
     * replacing any old MethodPanel displayed.
     * 
     * @param newMethodPanel - MethodPanel to display
     */
    public void displayMethodPanel(MethodPanel newMethodPanel)
    {
        if( methodPanel != null)
        {
            remove(methodPanel);
        }
        add(newMethodPanel);
        methodPanel = newMethodPanel;
        revalidate();
    }
    
    /**
     * ObjectViews are equal if their ObjectHolders are
     * equal using the ObjectHolder.equals() method.
     */
    public boolean equals(Object other)
    {
        if( other instanceof ObjectView )
        {
            ObjectView otherObjectView = (ObjectView) other;
            if( otherObjectView.getObjectHolder().equals(getObjectHolder()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    public ObjectHolder getObjectHolder()
    {
        return objectHolder;
    }
}
