/*
 * Created on: Apr 5, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import org.apache.log4j.Logger;
import java.lang.reflect.*;

/**
 * The MethodPanelFactory is responsible for creating MethodPanel Objects.
 * The createMethodPanel() can be passed either a Method or a Constructor and
 * creates a MethodPanel or ConstructorMethodPanel respectively.
 * 
 * @author eduthie
 */
public class MethodPanelFactory
{
    private static Logger logger = Logger.getLogger(MethodPanelFactory.class);
    
    /**
     * Constructor a new MethodPanel from a Class and a Method.
     * 
     * @param theClass - Class containing given Method.
     * @param method - Method of MethodPanel
     * @return - a new MethodPanel
     */
    public static MethodPanel createMethodPanel(Class theClass, Method method)
    {
        return new MethodPanel(theClass,method);
    }
    
    /**
     * Constructor a new MethodPanel from a Class and a Constructor.
     * 
     * @param theClass - Class containing given Constructor.
     * @param constructor - Constructor of ConstructorMethodPanel
     * @return - a new ConstructorMethodPanel
     */
    public static ConstructorMethodPanel createMethodPanel(Class theClass,
            Constructor constructor)
    {
        return new ConstructorMethodPanel(theClass,constructor);
    }

}
