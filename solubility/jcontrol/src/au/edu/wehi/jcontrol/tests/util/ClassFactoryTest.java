/*
 * Created on: Mar 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.tests.util;

import au.edu.wehi.jcontrol.util.ClassFactory;
import au.edu.wehi.jcontrol.util.PackageClasses;
import junit.framework.TestCase;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;
import java.lang.reflect.*;

/**
 * @author eduthie
 *
 */
public class ClassFactoryTest extends TestCase
{
    static Logger logger = Logger.getLogger(ClassFactoryTest.class);

    public static void main(String[] args)
    {
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        BasicConfigurator.configure();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for ClassFactoryTest.
     * @param arg0
     */
    public ClassFactoryTest(String arg0)
    {
        super(arg0);
    }
    
    /**
     * Tests that the available class names are recieved 
     * correctly.
     */
    public void testRefreshNames()
    {
        ClassFactory classF = ClassFactory.instance();
        Hashtable<String,PackageClasses> packages = classF.getPackages();
        
        /*
        Set<String> keys = packages.keySet();
        Iterator<String> keyIterator = keys.iterator();
        while(keyIterator.hasNext())
        {
            String key = keyIterator.next();
            PackageClasses classes = packages.get(key);
            Iterator<String> classesI = classes.iterator();
            while(classesI.hasNext())
            {
                String className = classesI.next();
                logger.debug("Package: " + key + " CLASS: " + className);
            }
        }
        */
        
        PackageClasses packageClasses = packages.get("java.util");
        assertTrue( packageClasses.contains("Vector") );
        packageClasses = packages.get("javax.swing");
        assertTrue( packageClasses.contains("JPanel"));
        packageClasses = packages.get("java.lang");
        assertTrue( packageClasses.contains("Object"));
        int lastPeriod = getClass().getName().lastIndexOf('.');
        String packageName = getClass().getName().substring(0,lastPeriod);
        packageClasses = packages.get(packageName);
        assertNotNull(packageClasses);
        String className = getClass().getName().substring(lastPeriod+1);
        assertTrue( packageClasses.contains(className) );
    }
    
    public void testIsStatic()
    {
        try
        {
            Class theClass = ClassFactory.class;
            Class[] parameters = {};
            Method method = theClass.getMethod("instance",parameters);
            assertTrue(ClassFactory.instance().isStatic(method));
            method = theClass.getMethod("refreshNames");
            assertFalse(ClassFactory.instance().isStatic(method));
        }
        catch( NoSuchMethodException nsme )
        {
            fail(nsme.getMessage());
        }   
    }
    
    public void testIsPrimitive()
    {
        assertNotNull(ClassFactory.instance().isPrimitive(Integer.class));
        assertNull(ClassFactory.instance().isPrimitive(ClassFactory.class));
    }

}
