/*
 * Created on: Apr 19, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.tests.util;

import junit.framework.TestCase;
import org.apache.log4j.BasicConfigurator;
import au.edu.wehi.jcontrol.util.PreferencesDialog;
import javax.swing.*;
import java.io.*;

/**
 * @author eduthie
 *
 */
public class PreferencesDialogTest extends TestCase
{
    private PreferencesDialog pDialog;
    
    public static void main(String[] args)
    {
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        BasicConfigurator.configure();
        pDialog = new PreferencesDialog(new JFrame());
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
        pDialog.dispose();
    }

    /**
     * Constructor for PreferencesDialogTest.
     * @param arg0
     */
    public PreferencesDialogTest(String arg0)
    {
        super(arg0);
    }

    public void testGetBool()
    {
        File file = new File(PreferencesDialog.PREFERENCES_FILE);
        if( file.exists() )
        {
            file.delete();
        }
        assertFalse(pDialog.fileAvailable());
        retrieveAPreference();
        pDialog.applyChanges();
        assertTrue(pDialog.fileAvailable());
        retrieveAPreference();
    }
    
    public void retrieveAPreference()
    {
        PreferencesDialog dialog = new PreferencesDialog();
        try
        {
            boolean preference = dialog.getBool(PreferencesDialog.DOUBLE_CLICK_TO_OPEN);
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
