/*
 * Created on: Apr 15, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.tests;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

import au.edu.wehi.jcontrol.ObjectEntryPanel;
import au.edu.wehi.jcontrol.ObjectHolder;

import javax.swing.*;

/**
 * @author eduthie
 *
 */
public class ObjectEntryPanelTest extends TestCase
{
    private static Logger logger = Logger.getLogger(ObjectEntryPanelTest.class);
    
    public static void main(String[] args)
    {
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        BasicConfigurator.configure();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for ObjectEntryPanelTest.
     * @param arg0
     */
    public ObjectEntryPanelTest(String arg0)
    {
        super(arg0);
    }
    
    public void testJustSetObject()
    {
        try
        {
            String string = "testString";
            String changedString = "changedString";
            ObjectEntryPanel oep = new ObjectEntryPanel(String.class);
            assertFalse(oep.isJustSetObject());
            ObjectHolder oh = new ObjectHolder(string);
            oep.getTransferHandler().importData(new JPanel(),oh);
            assertTrue(oep.isJustSetObject());
            Object ret = oep.getObject();
            assertTrue(string.equals((String) ret));
            oep.constructStringObject(changedString);
            assertFalse(oep.isJustSetObject());
            oep.setText(changedString);
            ret = oep.getObject();
            assertTrue(changedString.equals((Object) ret));
        }
        catch( java.io.IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
