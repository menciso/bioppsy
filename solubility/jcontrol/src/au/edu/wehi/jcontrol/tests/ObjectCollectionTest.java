/*
 * Created on: Apr 12, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.tests;

import junit.framework.TestCase;
import org.apache.log4j.Logger;
import au.edu.wehi.jcontrol.ObjectCollection;
import au.edu.wehi.jcontrol.ObjectHolder;
import java.util.Hashtable;
import javax.swing.tree.*;

/**
 * @author eduthie
 *
 */
public class ObjectCollectionTest extends TestCase
{
    private static Logger logger = Logger.getLogger(ObjectCollectionTest.class);
    
    public static void main(String[] args)
    {
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for ObjectCollectionTest.
     * @param arg0
     */
    public ObjectCollectionTest(String arg0)
    {
        super(arg0);
    }
    
    public void testAddObject()
    {
        ObjectCollection oc = new ObjectCollection();
        String string = new String("New String");
        String stringPackage = "java.lang";
        ObjectHolder oh = new ObjectHolder(string);
        oc.addObject(oh);
        Hashtable<String,DefaultMutableTreeNode> packages =
            oc.getPackages();
        DefaultMutableTreeNode packageNode =
            packages.get(stringPackage);
        assertNotNull(packageNode);
    }

}
