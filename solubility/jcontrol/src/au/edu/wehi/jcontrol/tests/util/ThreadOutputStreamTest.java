/*
 * Created on: Apr 6, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.tests.util;

import junit.framework.TestCase;
import java.io.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import java.awt.event.*;

import au.edu.wehi.jcontrol.util.ThreadOutputStream;

/**
 * @author eduthie
 *
 */
public class ThreadOutputStreamTest extends TestCase
{
    private static Logger logger = Logger.getLogger(ThreadOutputStreamTest.class);
    
    String result = "";
    
    public static void main(String[] args)
    {
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        BasicConfigurator.configure();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for ThreadOutputStreamTest.
     * @param arg0
     */
    public ThreadOutputStreamTest(String arg0)
    {
        super(arg0);
    }
    
    public void testThreadOutputStream()
    {
        final String message = "RUNNING IN THREAD";
        
        Thread testThread = new Thread() {
            public void run()
            {
                TestOutputStream tos = new TestOutputStream();
                tos.addActionListener(new OutputActionListener());
                ThreadOutputStream.instance().setOutputStream(tos);
                System.out.println(message);
            }
        };
        
        testThread.start();
        while(testThread.getState()!=Thread.State.TERMINATED)
        {
        }
        // get rid of the trailing newline
        result = result.substring(0,result.length()-1);
        assertEquals(result,message);
    }
    
    public class TestOutputStream extends OutputStream
    {
        private ActionListener listener;
        
        public TestOutputStream()
        {
            super();
        }
        
        public void write(int c)
        {
            String cStr = (new Character((char)c)).toString();
            ActionEvent event = 
                new ActionEvent(this,ActionEvent.ACTION_FIRST,cStr);
            listener.actionPerformed(event);
        }
        
        public void addActionListener(ActionListener l)
        {
            listener = l;
        }
    }
    
    public class OutputActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            result += e.getActionCommand();
        }
    }
}
