/*
 * Created on: Apr 12, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.tests;

import junit.textui.TestRunner;
import junit.framework.*;
import org.apache.log4j.Logger;
import au.edu.wehi.jcontrol.tests.*;
import au.edu.wehi.jcontrol.tests.util.*;

import org.apache.log4j.BasicConfigurator;

/**
 * @author eduthie
 *
 */
public class AllTests extends TestCase
{

    public static void main(String[] args)
    {
        BasicConfigurator.configure();
        TestRunner.run(suite());
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for AllTests.
     * @param arg0
     */
    public AllTests(String arg0)
    {
        super(arg0);
    }
    
    public static Test suite() 
    {
        TestSuite suite = new TestSuite("Unit Tests");
        suite.addTest(new TestSuite(ObjectHolderTest.class));
        suite.addTest(new TestSuite(ClassFactoryTest.class));
        suite.addTest(new TestSuite(ThreadOutputStreamTest.class));
        suite.addTest(new TestSuite(ObjectCollectionTest.class));
        suite.addTest(new TestSuite(ObjectEntryPanelTest.class));
        suite.addTest(new TestSuite(PreferencesDialogTest.class));
        return suite;
    }
}
