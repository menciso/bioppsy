/*
 * Created on: Apr 12, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.tests;

import junit.framework.TestCase;
import org.apache.log4j.Logger;

import au.edu.wehi.jcontrol.ObjectHolder;

/**
 * @author eduthie
 *
 */
public class ObjectHolderTest extends TestCase
{

    public static void main(String[] args)
    {
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     * Constructor for ObjectHolderTest.
     * @param arg0
     */
    public ObjectHolderTest(String arg0)
    {
        super(arg0);
    }

    public void testGetPackageName()
    {
        String string = "This is a java.lang.String";
        String expectedPackage = "java.lang";
        ObjectHolder objectHolder = new ObjectHolder(string);
        String recievedPackage = objectHolder.getPackageName();
        assertEquals(expectedPackage,recievedPackage);
    }

}
