/*
 * Created on: Apr 21, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import au.edu.wehi.jcontrol.Bridge.PreferencesActionListener;
import au.edu.wehi.jcontrol.util.PreferencesDialog;
import net.infonode.docking.*;
import net.infonode.docking.util.*;
import java.io.*;
import net.infonode.util.*;

/**
 * The Bridge is the Class of the application with the main
 * method. Bridge implements the singleton pattern and the
 * only possible instance of the Bridge class is accessed
 * using the Bridge.instance() method.<br><br>
 * 
 * The Bridge controls the coordination of various tasks.
 * It contains a RootWindow with three View objects contained
 * within the RootWindow; ObjectCollection, WorkingFrame, and
 * ConsoleFrame.
 * 
 * @author eduthie
 */
public class Bridge extends JFrame
{
    private static Bridge theBridge = null;
    
    private static String APPLICATION_NAME = "jcontrol";
    private static int WIDTH = 500;
    private static int HEIGHT = 500;
    private static float OBJECT_COLLECTION_FRACTION = 0.3f;
    private static float WORKING_FRAME_FRACTION = 0.7f;
    
    private RootWindow rootWindow = null;
    private ObjectCollection objectCollection = null;
    private ClassPanel classFrame = null;
    private ObjectViewContainer objectViewFrame = null;
    private ConsoleFrame consoleFrame = null;
    
    private ViewMap viewMap;
    
    public static void main(String[] args)
    {
        javax.swing.SwingUtilities.invokeLater(
            new Runnable() 
            {
                public void run() 
                {
                    Bridge bridge = Bridge.instance();
                    bridge.createAndShowGUI();
                }
            }
        );
     }
    
    private Bridge()
    {
        super(APPLICATION_NAME);
        PropertyConfigurator.configure(getClass().getResource("application.log4j"));
    }
    
    /**
     * Returns the one instance of Bridge object that exists.
     */
    public static Bridge instance()
    {
        if( theBridge == null )
        {
            theBridge = new Bridge();
        }
        return theBridge;
    }
    
    /**
     * Sets up the JFrame, adds all components, and makes the Frame
     * visible.
     */
    public void createAndShowGUI()
    {
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(new Dimension(WIDTH,HEIGHT));
        addMenu();
        
        objectCollection = new ObjectCollection();
        classFrame = new ClassPanel();
        objectViewFrame = new ObjectViewContainer();
        consoleFrame = new ConsoleFrame();
        
        viewMap = new ViewMap();
        viewMap.addView(0,objectCollection);
        viewMap.addView(1,classFrame);
        viewMap.addView(2,objectViewFrame);
        viewMap.addView(3,consoleFrame);
        
        rootWindow = DockingUtil.createRootWindow(viewMap, true);
        rootWindow.getWindowBar(Direction.DOWN).setEnabled(true);
        
        DockingWindow[] workingFrames = {classFrame,objectViewFrame};
        DockingWindow workingFrame = new TabWindow(workingFrames);
        DockingWindow rightFrame = new SplitWindow(false,WORKING_FRAME_FRACTION,
                workingFrame, consoleFrame);
        
        rootWindow.setWindow(new SplitWindow(true,OBJECT_COLLECTION_FRACTION,
                    objectCollection,rightFrame));
        
        setContentPane(rootWindow);
        classFrame.restoreFocus();
        validate();
    }
    
    public void addMenu()
    {
        JMenuBar menuBar = new JMenuBar();
        JMenu objectMenu = new JMenu("Object");
        menuBar.add(objectMenu);
        JMenu preferencesMenu = new JMenu("Preferences");
        JMenuItem preferences = new JMenuItem("change preferences");
        preferences.addActionListener(new PreferencesActionListener());
        preferencesMenu.add(preferences);
        menuBar.add(preferencesMenu);
        setJMenuBar(menuBar);
    }
    
    /**
     * When an ActionEvent is recieved the PreferencesActionListener
     * creates a dialog allowing the user to change the preferences
     */
    public class PreferencesActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            preferences();
        }
    }
    
    /**
     * Displays the preferences dialog.
     */
    public void preferences()
    {
        PreferencesDialog preferences = new PreferencesDialog(this);    
    }
    
    /**
     * The given MethodRunner JPanel is added to the current
     * ConsoleFrame. The method is then run in a separate 
     * thread.
     */
    public void runMethodRunner(MethodRunner mr)
    {
        consoleFrame.addTab(mr);
        validate();
    }
    
    /**
     * Displays the given object in the WorkingPanel's ObjectViewContainer
     * and switches focus to this display.
     * 
     * @param oh - ObjectHolder containing Object to display
     */
    public void displayObject(ObjectHolder oh)
    {
        if( objectViewFrame != null )
        {
            objectViewFrame.addObject(oh);
            objectViewFrame.restoreFocus();
        }
    }
    
    /**
     * Adds the given Object to the ObjectCollection
     */
    public void addObject(ObjectHolder oh)
    {
        if( objectCollection != null )
        {
            objectCollection.addObject(oh);
        }
    }
}
