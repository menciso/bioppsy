/*
 * Created on: Apr 19, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import javax.swing.*;

import org.apache.log4j.Logger;

import au.edu.wehi.jcontrol.WorkingFrame.Item;

import java.awt.*;
import net.infonode.tabbedpanel.*;
import net.infonode.tabbedpanel.titledtab.*;
import java.awt.event.*;
import net.infonode.docking.View;

/**
 * The ObjectViewContainer displays ObjectView panels
 * in its internal CloseAndMaxTabbedPane. Simply construct
 * and add ObjectHolders to display Objects using the
 * addObject() method.
 * 
 * @author eduthie
 */
public class ObjectViewContainer extends View
{
    private static Logger logger = Logger.getLogger(ObjectViewContainer.class);
    
    private static String OBJECT_VIEW_FRAME = "Object";
    
    private JPanel content;
    
    private TabbedPanel tabbedPane;
    
    public ObjectViewContainer()
    {
        super(OBJECT_VIEW_FRAME, null, new JPanel());
        content = (JPanel) getComponent();
        content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
        tabbedPane = new TabbedPanel();
        content.add(tabbedPane);
        revalidate();
    }
    
    /**
     * Adds and displays the given Object as another tab
     * in the tabbed pane using an ObjectView object.
     * If the given Object already is displayed (compared
     * via the ObjectHolder.equals() method) a new tab
     * is not added, the current displayed tab is selected.
     * 
     * @param oh - ObjectHolder containing Object to display
     */
    public void addObject(ObjectHolder oh)
    {
        ObjectView ov = new ObjectView(oh);
        Tab currentTab = getComponent(ov);
        if( currentTab == null )
        {
            TitledTab newTab = new TitledTab(
                    oh.getName(),null,ov,null);
            newTab.setHighlightedStateTitleComponent(createCloseTabButton(newTab));
            tabbedPane.addTab(newTab);
            tabbedPane.setSelectedTab(newTab);
        }
        else
        {
            tabbedPane.setSelectedTab(currentTab);
        }
    }
    
    /**
     * If the given ObjectView is currently displayed as a tab in
     * this ObjectViewContainer, the displayed Tab is returned.
     * Otherwise null is returned. ObjectViews are compared via
     * their equals() method.
     * 
     * @param objectView - ObjectView to search for.
     * @return
     */
    public Tab getComponent(ObjectView objectView)
    {
        int i=0;
        
        // we have to be a bit tricky here and loop until
        // and exception is thrown indicating that we have
        // reached a tab index i that is out of bounds so
        // we have finished
        while(true)
        {
            try
            {
                Tab tab = tabbedPane.getTabAt(i);
                ObjectView currentOV = (ObjectView) tab.getContentComponent();
                if( currentOV.equals(objectView))
                {
                    return tab;
                }
            }
            catch(IndexOutOfBoundsException e)
            {
                break;
            }
            ++i;
        }
        return null;
    }
    
    /**
     * Creates a JButton with an X
     *
     * @return the button
     */
    private JButton createXButton() 
    {
        JButton closeButton = new JButton("X");
        closeButton.setOpaque(false);
        closeButton.setMargin(null);
        closeButton.setFont(closeButton.getFont().deriveFont(Font.BOLD).deriveFont(10f));
        closeButton.setForeground(Color.RED);
        return closeButton;
    }
    
    /**
     * Creates a close tab button that closes the given tab when the button is
     * selected
     *
     * @param tab the tab what will be closed when the button is pressed
     * @return the close button
     */
    private JButton createCloseTabButton(final TitledTab tab) 
    {
        JButton closeButton = createXButton();
        closeButton.addActionListener(
            new ActionListener() 
            {
                public void actionPerformed(ActionEvent e) {
                // Closing the tab by removing it from the tabbed panel it is a
                // member of
                    tab.getTabbedPanel().removeTab(tab);
                }
            }
        );
        return closeButton;
    }

}
