/*
 * Created on: Apr 5, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import org.apache.log4j.Logger;
import java.lang.reflect.*;
import javax.swing.*;

/**
 * A ConstrucorMethodPanel is a MethodPanel that uses a
 * Constructor instead of a Method.
 * 
 * @author eduthie
 * 
 */
public class ConstructorMethodPanel extends MethodPanel
{
    private static Logger logger = Logger.getLogger(ConstructorMethodPanel.class);
    
    private Constructor constructor;

    /**
     * Creates the MethodPanel and sets the title of the border as
     * the String representation of the given Constructor.
     */
    public ConstructorMethodPanel(Class theClass, Constructor constructor)
    {
        super();
        add(new JLabel(constructor.toString()));
        construct(theClass);
        this.constructor = constructor;
        addArguments(constructor.getParameterTypes());
    }
    
    /**
     * See MethodPanel.runMethod(Object[] args)
     */
    public void runMethod(Object[] args)
    {
        ConstructorMethodRunner cmr = 
            new ConstructorMethodRunner(args,constructor);
        Bridge.instance().runMethodRunner(cmr);
    }

}
