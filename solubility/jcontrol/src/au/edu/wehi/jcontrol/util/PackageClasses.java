/*
 * Created on: Mar 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.util;

import org.apache.log4j.Logger;
import java.util.Vector;
import java.util.Arrays;

/**
 * @author eduthie
 * 
 * PackageClasses has a name which represents
 * the name of a Package. PackageClasses extend
 * Vector<String>, the Vector contains
 * Strings which are the names of classes in
 * the Package. 
 */
public class PackageClasses extends Vector<String> implements Comparable
{
    private String name;
    
    public PackageClasses(String name)
    {
        super();
        this.name = name;
    }
    
    public String getName()
    {
        return name;
    }
    
    public String toString()
    {
        return getName();
    }
    
    public int compareTo(Object other)
    {
        if( other.getClass().getName().equals(getClass().getName()) )
        {
            return getName().compareTo(((PackageClasses) other).getName());
        }
        else
        {
            return -1;
        }
    }
    
    /**
     * sort() returns a new PackageClasses object sorted
     * according to the class names.
     * 
     * @return a new PackageClasses object sorted by class name
     */
    public PackageClasses sort()
    {
        String[] array = new String[size()];
        array = toArray(array);
        Arrays.sort(array);
        PackageClasses ret = new PackageClasses(name);
        for( int i=0; i < array.length; ++i )
        {
            ret.add(array[i]);
        }
        return ret;
    }
}
