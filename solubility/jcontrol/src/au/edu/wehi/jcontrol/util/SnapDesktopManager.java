/*
 * Created on: Mar 30, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.util;

import javax.swing.DefaultDesktopManager;

import org.apache.log4j.Logger;
import javax.swing.*;
import java.awt.*;

/**
 * @author eduthie
 *
 */
public class SnapDesktopManager extends DefaultDesktopManager
{
    private static int TOLERANCE = 30;
    
    private int x;
    private int y;
    private int w;
    private int h;
    private JInternalFrame frame;
    private JDesktopPane desk;
    private Dimension deskSize;
    
    private static Logger logger = Logger.getLogger(SnapDesktopManager.class);
    
    /**
     * 
     */
    public SnapDesktopManager()
    {
        super();
    }

    // This is called any time a frame is moved or resized. This
    // implementation keeps the frame from leaving the desktop.
    public void snap()
    {
        logger.debug("SNAP TO");
        
        JInternalFrame[] allFrames = desk.getAllFrames();
        for( int i=0; i < allFrames.length; ++i )
        {
            JInternalFrame otherFrame = allFrames[i];
            if( otherFrame != frame )
            {
                snapToInternalFrame(otherFrame);
            }
        }
        //snapToDesk();
    }
    
    public void dragFrame(JComponent f, int newX, int newY)
    {
        logger.debug("DRAG FRAME");
        initialise(f,newX,newY);
        snap();
        super.dragFrame(frame,x,y);
    }
    
    public void resizeFrame(JComponent f, int newX, int newY, 
            int newWidth, int newHeight)
    {
        logger.debug("RESIZE FRAME");
        initialise(f,newX,newY,newWidth,newHeight);
        snap();
        super.resizeFrame(frame,x,y,w,h);
    }
    
    public void initialise(JComponent f, int newX, int newY)
    {
        frame = (JInternalFrame) f;
        desk = frame.getDesktopPane();
        deskSize = desk.getSize();
        this.x = newX;
        this.y = newY;
        this.w = f.getWidth();
        this.h = f.getHeight();
    }
    
    public void initialise(JComponent f, int newX, int newY, 
            int newWidth, int newHeight)
    {
        initialise(f,newX,newY);
        w = newWidth;
        h = newHeight;
    }
    
    private void snapToDesk()
    {
        // Nothing all that fancy below, just figuring out how to adjust
        // to keep the frame on the desktop.
        if (x < 0)
        { // too far left?
            x = 0; // flush against the left side
        }
        else
        {
            if (x + w > deskSize.width)
            { // too far right?
                x = deskSize.width - w; // flush against the right side
            }
        }
        if (y < 0)
        { // too high?
            y = 0; // flush against the top
        }
        else
        {
            if (y + h > deskSize.height)
            { // too low?
                y = deskSize.height - h; // flush against the bottom
            }
        }
    }
    
    private void snapToInternalFrame(JInternalFrame otherFrame)
    {
        Rectangle r = otherFrame.getBounds();
        boolean snapOthersX = false;
        logger.debug("Y: " + y);
        logger.debug("OTHER Y: " + r.getY());
        logger.debug("OTHER Y + h: " + (r.getY() + r.getHeight()));
        if( y > (r.getY() + TOLERANCE) && 
            y < (r.getY() + r.getHeight() - TOLERANCE) )
        {
            snapOthersX = true;
        }
        
        boolean topLeft, topRight, bottomLeft, bottomRight;
        topLeft = false;
        topRight = false;
        bottomLeft = false;
        bottomRight = false;
        boolean x1,x2,y1,y2;
        
        if( (x > r.getX()) && 
            (x < r.getX() + r.getWidth()) )
        {
            x1 = true;
        }
        else
        {
            x1 = false;
        }
        
        if( (y > r.getY()) &&
            (y < r.getY() + r.getHeight()) )
        {
            y1 = true;
        }
        else
        {
            y1 = false;
        }
        
        if( (x + w > r.getX()) && 
            (x + w < r.getX() + r.getWidth()) )
        {
            x2 = true;
        }
        else
        {
            x2 = false;
        }
        
        if( (y + h > r.getY()) &&
            (y + h < r.getY() + r.getHeight()) )
        {
            y2 = true;
        }
        else
        {
            y2 = false;
        }
        
        if( x1 && y1 )
        {
            topLeft = true;
            logger.debug("TOP LEFT");
        }
        if( x2 && y1 )
        {
            topRight = true;
            logger.debug("TOP RIGHT");
        }
        if( x1 && y2 )
        {
            bottomLeft = true;
            logger.debug("BOTTOM LEFT");
        }
        if( x2 && y2 )
        {
            bottomRight = true;
            logger.debug("BOTTOM RIGHT");
        }
        
        if( snapOthersX )
        {
            if( topLeft || bottomLeft )
            {
                x = (int) r.getX() + (int) r.getWidth();
            }
            else if( topRight || bottomRight )
            {
                x = (int) r.getX() - w;
            }
        }
        else
        {
            if( topLeft || topRight )
            {
                y = (int) r.getY() + (int) r.getHeight();
            }
            else if( bottomLeft || bottomRight )
            {
                y = (int) r.getY() - h;
            }
        }
        
    }
    
}
