/*
 * Created on: Apr 18, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.util;

import javax.swing.*;
import javax.swing.JDialog;
import org.apache.log4j.Logger;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Properties;

/**
 * @author eduthie
 *
 * The PreferencesDialog is a JDialog that allows the user to change
 * the application preferences. Present the dialog to the user by
 * using the constructor with a Frame as an argument. 
 * Then the various accessor functions can be used to retrieved stored 
 * preferences by creating a PreferencesDialog with the constructor with
 * no arguments.<br><br>
 * 
 * USAGE EXAMPLE:<br>
 * PreferencesDialog displayDialog = new PreferencesDialog(myFrame);<br>
 * // dialog has been displayed<br>
 * PreferencesDialog usePref = new PreferencesDialog();<br>
 * boolean doubleClick = usePref.getBool(PreferencesDialog.DOUBLE_CLICK_TO_OPEN);<br>
 */
public class PreferencesDialog extends JDialog
{
    private static Logger logger = Logger.getLogger(PreferencesDialog.class);
    private static String NAME = "PREFERENCES";
    /**
     * File where preferences are stored.
     */
    public static String PREFERENCES_FILE = "preferences.properties";
    private static String DEFAULT_PREFERENCES = 
        "/au/edu/wehi/jcontrol/util/default_preferences.properties";
    private static String TRUE = "true";
    private static String FALSE = "false";
    private static String COMMENT = "Stored preferences for the application";
    
    /**
     * Key for the boolean property determining whether the user wishes things to
     * be opened on a double click, if true, or on a single click if false.
     */
    public static String DOUBLE_CLICK_TO_OPEN = "DOUBLE_CLICK_TO_OPEN";
    
    private JCheckBox doubleClickToOpen = null;
    // whether or not a preferences file is availiable
    private boolean fileAvailable = false; 
    
    /**
     * Used to display the dialog to the user 
     * 
     * @param owner - Frame that, if closed, will close the dialog
     */
    public PreferencesDialog(Frame owner)
    {
        super(owner,NAME);
        getContentPane().setLayout(
                new BoxLayout(getContentPane(),BoxLayout.PAGE_AXIS));
        addComponents();
        loadChanges(); // loads the preferences into the dialog display
        pack();
        setVisible(true);
    }
    
    /**
     * Used to access preferences by the application
     */
    public PreferencesDialog()
    {
    }

    private void addComponents()
    {
        doubleClickToOpen = new JCheckBox(DOUBLE_CLICK_TO_OPEN);
        doubleClickToOpen.setAlignmentX(Component.LEFT_ALIGNMENT);
        getContentPane().add(doubleClickToOpen);
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
        buttonPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        JButton apply = new JButton("Apply Changes");
        apply.addActionListener(new ApplyActionListener());
        buttonPanel.add(apply);
        getContentPane().add(buttonPanel);
    }
    
    /**
     * When an ApplyActionListener recieves an ActionEvent it applies all
     * the new changes to the preferences made in the PreferencesDialog.
     */
    public class ApplyActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            applyChanges();
        }
    }
    
    /**
     * Applies all changes made to preferences.
     */
    public void applyChanges()
    {
        try
        {

            Properties properties = new Properties();
            if( doubleClickToOpen.isSelected() )
            {
                properties.setProperty(DOUBLE_CLICK_TO_OPEN,TRUE);
            }
            else
            {
                properties.setProperty(DOUBLE_CLICK_TO_OPEN,FALSE);
            }
            OutputStream output = new FileOutputStream(PREFERENCES_FILE);
            if( output == null )
            {
                logger.error("Unable to write to file: " + PREFERENCES_FILE);
            }
            else
            {
                properties.storeToXML(output,COMMENT);
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
        }
    }
    
    /**
     * Loads the preferences from the saved file, if it exists,
     * or from the default preferences properties file otherwise.
     */
    public void loadChanges()
    {
        try
        {
            Properties typesProp = getProperties();
            String property = typesProp.getProperty("DOUBLE_CLICK_TO_OPEN");
            if( property.equals(TRUE) )
            {
                doubleClickToOpen.setSelected(true);
            }
            else if( property.equals(FALSE) )
            {
                doubleClickToOpen.setSelected(false);
            }
            else
            {
                logger.error("Unidentified type in preferences file: " + property);
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
        }
    }
    
    /**
     * Returns the boolean value of the given key. For example if the
     * key PreferencesDialog.DOUBLE_CLICK_TO_OPEN is provided the
     * returned value will be the preference of whether the user wishes
     * items to open on a double click, if true, or on a single click,
     * if false. Throws an IOException if the key is not found or on
     * error.
     */
    public boolean getBool(String key)
        throws IOException
    {
        Properties properties = getProperties();
        String property = properties.getProperty(key);
        if( property == null )
        {
            throw new IOException("Property not found for key: " + key);
        }
        if( property.equals(TRUE) )
        {
            return true;
        }
        else if( property.equals(FALSE) )
        {
            return false;
        }
        else
        {
            throw new IOException("Unidentified type in preferences file: " + property);
        }
    }
    
    private Properties getProperties()
        throws IOException
    {
        Properties properties = new Properties();
        InputStream input;
        if(fileAvailable())
        {
            input = new FileInputStream(PREFERENCES_FILE);
        }
        else
        {
            input = getClass().getResourceAsStream(DEFAULT_PREFERENCES);
        }
        properties.loadFromXML(input);
        return properties;
    }
    
    /**
     * Returns true if the saved properties file is available to read,
     * false otherwise
     */
    public boolean fileAvailable()
    {
        File file = new File(PREFERENCES_FILE);
        return file.canRead();
    }
}
