/*
 * Created on: Apr 19, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.util;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.*;
import javax.swing.tree.*;
import org.apache.log4j.Logger;

/**
 * @author eduthie
 *
 * The TreeMouseAdapter is an abstract MouseAdapter. The
 * selected() method must be implemented. A JTree is provided
 * to the constructor. When the mouse is pressed, if it is
 * over an item in the provided JTree, the row and path to
 * that tree item is provided to the selected() method. 
 */
public abstract class TreeMouseAdapter extends MouseAdapter
{
    private static Logger logger = Logger.getLogger(TreeMouseAdapter.class);
    private JTree tree;
    
    /**
     * @args tree - the JTree to monitor mouse events on.
     */
    public TreeMouseAdapter(JTree tree)
    {
        super();
        this.tree = tree;
    }
    
    /**
     * When the mouse is pressed, the selected() method is called
     * with the row and path to the tree node that the mouse is
     * currently over.
     */
    public void mousePressed(MouseEvent e) 
    {
        try
        {
            int selRow = tree.getRowForLocation(e.getX(), e.getY());
            TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
            if(selRow != -1) 
            {
                PreferencesDialog pDialog = new PreferencesDialog();
                if( e.getClickCount() == 1)
                {
                    if( !pDialog.getBool(PreferencesDialog.DOUBLE_CLICK_TO_OPEN))
                    {
                        selected(selRow, selPath);
                    }
                }
                if(e.getClickCount() == 2) 
                {
                    if( pDialog.getBool(PreferencesDialog.DOUBLE_CLICK_TO_OPEN))
                    {
                        selected(selRow, selPath);
                    }
                }
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
        }
    }
    
    /**
     * When a tree node is selected either by a single or double
     * click (depending on preferences) this method is called providing
     * the row and path to the selected node. Implement this method
     * to handle a node selection event.
     * 
     * @param selRow - Row index of selected tree node.
     * @param selPath - Path to selected node.
     */
    public abstract void selected(int selRow, TreePath selPath);

}
