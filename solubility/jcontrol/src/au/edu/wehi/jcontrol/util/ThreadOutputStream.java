/*
 * Created on: Apr 6, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol.util;

import java.io.*;
import org.apache.log4j.Logger;
import java.util.*;

/**
 * @author eduthie
 * 
 * ThreadOutputStream uses the Singleton pattern and hence
 * only once occurence can exist and is accessed via the
 * static ThreadOutputStream.instance() method.<br><br>
 * 
 * A ThreadOutputStream replaces standard output. This
 * happens the first time any Thread calls the
 * setOutputStream(OutputStream) method.<br><br>
 * 
 * The ThreadOutputStream allows every Thread to use
 * a different standard output to the default one.
 * To do this, just call the setOutputStream(OutputStream)
 * from within a particular Thread. From then on all
 * output on that Thread will be redirected to the specified
 * OutputStream. When the Thread has ended you must call
 * removeOutputStream() to remove the OutputStream so that
 * the memory is released.<br><br>
 * 
 * Any other Threads that have not called setOutputStream(OutputStream)
 * or have called removeOutputStream() will have all their output
 * directed to the original standard output. The original standard
 * output is the current System.out just before setOutputStream(OutputStream)
 * is first called.
 */
public class ThreadOutputStream extends OutputStream
{
    private static ThreadOutputStream instance = null;
    private OutputStream systemOut = null;
    private Hashtable<Thread,OutputStream> threadStreams;
    
    private ThreadOutputStream()
    {
        super();
        threadStreams = new Hashtable<Thread,OutputStream>();
    }
    
    /**
     * @return - the single instance of ThreadOutputStream
     */
    public static ThreadOutputStream instance()
    {
        if(instance==null)
        {
            instance = new ThreadOutputStream();
        }
        return instance;
    }

    public void write(int b) throws IOException
    {
        getOut().write(b);
    }
    
    public void write(byte[] b, int off, int len)
        throws IOException
    {
        getOut().write(b,off,len);
    }
    
    public void write(byte[] b)
        throws IOException
    {
        getOut().write(b);
    }
    
    public void close()
        throws IOException
    {
        getOut().close();
    }
    
    public void flush()
        throws IOException
    {
        getOut().flush();
    }
    
    /**
     * @return - the current OutputStream that all standard output
     * is redirected to for the Thread that executes this method.
     */
    public OutputStream getOut()
    {
        Thread thread = Thread.currentThread();
        OutputStream output = threadStreams.get(thread);
        if( output == null )
        {
            // since we have set system out to this 
            // ThreadOutputStream we have to use a 
            // saved value
            output = systemOut;
        }
        return output;
    }
    
    /**
     * Redirects all standard output to the given OutputStream
     * within the Thread that executes this method. Once
     * the calling thread has finished with the redirected output,
     * removeOutputStream() must be called to free up resources.
     * 
     * @param stream - OutputStream to direct all standard output to
     */
    public void setOutputStream(OutputStream stream)
    {
        Thread thread = Thread.currentThread();
        threadStreams.put(thread,stream);
        
        // only need to set system out to this object
        // the first time this method is called
        if( systemOut == null )
        {
            systemOut = System.out;
            System.setOut(new PrintStream(this));
        }
    }
    
    /**
     * Removes the OutputStream specified by setOutputStream()
     * for the Thread that executes this method. removeOutputStream()
     * must be called when a Thread has finished if it called 
     * setOutputStream() to free resources.
     */
    public void removeOutputStream()
    {
        Thread thread = Thread.currentThread();
        threadStreams.remove(thread);
    }

}
