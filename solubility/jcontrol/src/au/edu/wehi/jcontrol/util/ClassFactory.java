/*
 * Created on: Mar 23, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute

 */
package au.edu.wehi.jcontrol.util;

import org.apache.log4j.Logger;


import java.io.*;
import java.util.jar.*;
import java.util.*;
import java.lang.reflect.*;

/**
 * @author eduthie
 * <p>
 * Only one single instance of the ClassFactory class exists,
 * obtained with the static ClassFactory.instance() method.
 * </p>
 * <p>
 * The ClassFactory object seraches for classes that are
 * currently available to be created. The user can select
 * an available class using the getClassFromUser() method.
 * If any new classes have been made available in the 
 * classpath the refreshNames() method should be used
 * to search for all available classes again.
 * </p>
 * Several helper methods are provided for working with
 * Class objects.
 */
public class ClassFactory
{    
    
    private static String SUN_BOOT_CLASS_PATH = "sun.boot.class.path";
    private static String JAVA_EXT_CLASS_PATH = "java.ext.class.path";
    private static String JAVA_CLASS_PATH = "java.class.path";
    private static String JAR_SUFFIX = ".jar";
    private static String TYPES_PROPERTIES = "/au/edu/wehi/jcontrol/types.properties";
    
    private static Logger logger = Logger.getLogger(ClassFactory.class);
    
    private static ClassFactory classFactory = null;
    private Hashtable<String,PackageClasses> packages = null;
    // Hash between the in-built type name to the corresponding
    // class name (e.g. 'double' -> "java.lang.Double")
    private Hashtable<String,String> typeClasses = null;
    
    private ClassFactory()
    {
        refreshNames();
        try
        {
            InputStream stream = getClass().getResourceAsStream(TYPES_PROPERTIES);
            if( stream == null )
            {
                logger.error("Could not find properties: " + TYPES_PROPERTIES);
            }
            else
            {
                // load the class names for in-built types
                // such as double, int, etc.., from a
                // .properties file
                Properties typesProp = new Properties();
                typesProp.loadFromXML(stream);
                typeClasses = new Hashtable<String,String>();
                Enumeration typeNames = typesProp.keys();
                while( typeNames.hasMoreElements() )
                {
                    String typeName = (String) typeNames.nextElement();
                    String className = typesProp.getProperty(typeName);
                    typeClasses.put(typeName,className);
                }
            }
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
        }
    }
    
    /**
     * @return ClassFactory - the single instance of 
     * the ClassFactory class.
     */
    public static synchronized ClassFactory instance()
    {
        if( classFactory == null )
        {
            classFactory = new ClassFactory();
        }
        return classFactory;
    }
    
    /**
     * Searches through the current classpath for all
     * available class names.
     * 
     * @return true on success, false on failure
     */
    public boolean refreshNames()
    {
        packages = new Hashtable<String,PackageClasses>();
        
        String bootClassPath = System.getProperty(SUN_BOOT_CLASS_PATH);
        //logger.debug("BOOT CLASS PATH: " + bootClassPath);
        getClassesFromPath(bootClassPath);
        String extClassPath = System.getProperty(JAVA_EXT_CLASS_PATH);
        getClassesFromPath(extClassPath);
        //logger.debug("EXT CLASS PATH: " + extClassPath);
        String classPath = System.getProperty(JAVA_CLASS_PATH);
        getClassesFromPath(classPath);
        //logger.debug("JAVA CLASS PATH: " + classPath);
        
        return true;
    }
    
    /**
     * Searches through the given path for jar files and directories.
     * If any class files are found they are added to the current 
     * packages.
     * 
     * @param path - colon separated list of path names
     */
    private void getClassesFromPath(String path)
    {
        if( path == null )
        {
            return;
        }
        String[] splitPath = path.split(":");
        
        for( int i=0; i < splitPath.length; ++i )
        {
            if( splitPath[i].endsWith(JAR_SUFFIX) )
            {
                getClassesFromJar(splitPath[i]);
            }
            else
            {
                getClassesFromDir(splitPath[i],splitPath[i]);
            }
        }
    }
    
    /**
     * The jar file given is searched for all available classes.
     * These are added to the current packages list of classes.
     * 
     * @param path - absolute or relative path name of a jar file
     */
    private void getClassesFromJar(String path)
    {
        JarEntry entry;
        String name;
        String packageName;
        PackageClasses packageClasses;
        
        //logger.debug("CLASSES FROM JAR: " + path);
        
        try
        {
            JarFile jar = new JarFile(path);
            Enumeration<JarEntry> entries = jar.entries();
            
            while( entries.hasMoreElements() )
            {   
                entry = entries.nextElement();
                name = entry.getName();
                addClassToPackages(name);
            }
        }
        catch( IOException ioe )
        {
            logger.error("IOException for path: " + path
                + " " + ioe.getMessage());
        }
    }
    
    /**
     * Chops off the class name from the end of the
     * path. Does not end in a trailing '/'.
     * 
     * @param path - full path name of a class
     * @return String packageName - name of just
     * the package of the class.
     */
    private String getPackage(String path)
    {
        int lastSlash = path.lastIndexOf('/');
        if( lastSlash >= 0 )
        {
            return path.substring(0,lastSlash);
        }
        else
        {
            return path;
        }
    }
    
    /**
     * Repalces all occurences of slash charaters in
     * the given path name with dot characters.
     *
     * @param path - path name
     * @return - path with '/' characters replaced with
     * '.' characters.
     */
    private String slashToDot(String path)
    {
        return path.replace('/','.');
    }
    
    /**
     * Removes the package name off the front of the
     * path of a .class file as well as the .class
     * suffix.
     * 
     * @param path - path name of a .class file
     * @return just the class name
     */
    private String getClassName(String path)
    {
        int lastSlashIndex = path.lastIndexOf("/");
        String ret = path.substring(lastSlashIndex+1);
        ret = ret.replace(".class","");
        return ret;
    }
    
    /**
     * The given absolute path of a directory is searched for
     * any .class files and these classes are added to the 
     * current packages Hashtable.
     * 
     * @param path - Absolute path of directroy without a 
     * trailing slash.
     * @param base - Path to the base directory. This is 
     * the base that is searched from.
     */
    private void getClassesFromDir(String path,String base)
    {   
        //logger.debug("CLASSES FROM DIR: " + path);
        
        File dir = new File(path);
        if( !dir.isDirectory() )
        {
            logger.error("Non-directory path in getClassesFromDir: " + path);
        }
        else
        {
            File[] files = dir.listFiles();
            for( int i=0; i < files.length; ++i )
            {
                // if we encounter a dir recursively traverse it
                if( files[i].isDirectory() )
                {
                    getClassesFromDir(files[i].getAbsolutePath(),base);
                }
                // otherwise add it to the classes list
                else
                {
                    String fullPath = files[i].getAbsolutePath();
                    String relativePath = fullPath.replaceFirst(base,"");
                    if( relativePath.charAt(0) == '/')
                    {
                        relativePath = relativePath.replaceFirst("/","");
                    }
                    //logger.debug("RELATIVE PATH: " + relativePath);
                    addClassToPackages(relativePath);
                }
            }
        }
    }
    
    /**
     * The given class is added to the current 'packages' Hashtable.
     * 
     * @param relativePath - Relative path of a .class file.
     */
    private void addClassToPackages(String relativePath)
    {
        String packageName;
        PackageClasses packageClasses;
        
        if( !relativePath.endsWith(".class") || relativePath.contains("$") )
        {
            // ignore files that are not .class files
            // ignore inner classes (containing $)
            return;
        }
        packageName = slashToDot(getPackage(relativePath));
        //logger.debug("PACKAGE NAME: " + packageName);
        
        // if the appropriate package is not found
        // create a new one
        packageClasses = packages.get(packageName);
        if( packageClasses == null )
        {
            packageClasses = new PackageClasses(packageName);
            packages.put(packageName,packageClasses);
        }
        String className = getClassName(relativePath);
        packageClasses.add(className);
    }
    
    /**
     * Returns a Class object for the class of the specified
     * name. Throws a ClassNotFoundException if a class by
     * the given name is not available.
     * 
     * @param name - fully qualified name of required class
     * @return Class - Class of specified name
     */
    public Class getClass(String name)
        throws ClassNotFoundException
    {
        return Class.forName(name);
    }
    
    /**
     * @return A  Hashtable of PackageClasses containing
     * all available classes in the current classpath
     * since refreshNames() was last called.
     */
    public Hashtable<String,PackageClasses> getPackages()
    {
        return packages;
    }
    
    /**
     * Determines whether the given method is static
     * 
     * @param method
     * @return true if the given method is a static method,
     * false otherwise
     */
    public boolean isStatic(Method method)
    {
        String[] splitName = method.toGenericString().split("[ ]+");
        for( int i=0; i < splitName.length; ++i )
        {
            String string = splitName[i];
            if( string.equals("static") )
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Determines whether the given class is primitive or not.
     * A primitive class is simply a class with a constructor
     * that has a single String argument. If the class is 
     * a primitive class, the constructor with a single string
     * argument is returned. Otherwise null is returned.
     * 
     * @param aClass
     * @return Constructor - a constructor with a single String
     * argument, or null if the given class does not have one
     */
    public Constructor isPrimitive(Class aClass)
    {
        Constructor[] cs = aClass.getConstructors();
        for( int i=0; i < cs.length; ++i )
        {
            Constructor c = cs[i];
            Class[] params = c.getParameterTypes();
            if( params.length == 1 )
            {
                Class param = params[0];
                if( String.class.getName().equals(param.getName()) )
                {
                    return c;
                }
            }
        }
        return null;
    }
    
    /**
     * If the provided Class is an in-built type such as long,
     * double, float, etc... The corresponding class is returned.
     * For example if the class for long is provided, the Class
     * java.lang.Long will be returned. If a class that is 
     * not in-built is input, that class will be returned.
     * If there is an error the input class is returned.
     * 
     * @param aClass - Class of in-built type (or any Class)
     * @return - Class of in built type (e.g. java.lang.Double)
     * or the input class if it is not in-built.
     */
    public Class getClassFromInBuilt(Class aClass)
    {
        String name = null;
        
        try
        {
            if( typeClasses == null )
            {
                logger.error("typeClasses null in ClassFactory");
                return aClass;
            }
            name = typeClasses.get(aClass.getName());
            if( name != null )
            {
                return Class.forName(name);
            }
            else
            {
                return aClass;
            }
        }
        catch( ClassNotFoundException cnfe )
        {
            logger.error("ClassNotFoundException: " + cnfe.getMessage());
            logger.error("in-built class not found: " + name);
            return aClass;
        }
    }
}
