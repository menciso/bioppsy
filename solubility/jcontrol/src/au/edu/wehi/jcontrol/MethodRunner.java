/*
 * Created on: Apr 6, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import javax.swing.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import au.edu.wehi.jcontrol.util.ThreadOutputStream;

import java.lang.reflect.Method;
import java.awt.*;
import java.io.*;

/**
 * A MethodRunner is a JPanel that displays and controls the
 * execution of a Method. A MethodRunner is displayed and
 * run in a ConsoleFrame by using ConsoleFrame.add(MethodRunner).
 * To create a MethodRunner you must pass it the Method to
 * be executed and an array of parameters. If you are running
 * a static method without an Object to run from, use the
 * constructor without an Object as an argument. To run
 * a non-static method, use the constructor passing an
 * Object to run the Method from.
 * 
 * @author eduthie
 *
 */
public class MethodRunner extends JPanel implements Runnable
{
    private static Logger logger = Logger.getLogger(MethodRunner.class);
    
    public static int WIDTH = 600;
    public static int HEIGHT = 400;
    
    private Object[] args = null; // parameters
    private Method method = null; // Method to execute
    private JTextArea text = null; // output display of Method execution
    private Object runObject = null; // Object to run Method from
    private Object retObject = null; // retObject returned from Method execution

    protected MethodRunner(Object[] args)
    {
        super();
        this.args = args;
        setup();
    }
    
    /**
     * Create a new MethodRunner with the given array of
     * parameters (must be in correct order) and the given
     * Method. The Object to run the Method from is set
     * to null which means this constructor should only be used
     * to run static methods.
     * 
     * @param args - Ordered array of parameter objects.
     * @param method - Method to run.
     */
    public MethodRunner(Object[] args, Method method)
    {
        super();
        this.args = args;
        this.method = method;
        runObject = null;
        setup();
    }
    
    /**
     * Create a new MethodRunner with the given array of
     * parameters (must be in correct order) and the given
     * Method. The Object to run the Method from is set
     * to the given 'runObject'. Thuse this constructor
     * should be used with non-static Methods.
     * 
     * @param args - Ordered array of parameter objects.
     * @param method - Method to run.
     * @param runObject - Object to run the method from.
     */
    public MethodRunner(Object[] args, Method method, Object runObject)
    {
        super();
        this.args = args;
        this.method = method;
        this.runObject = runObject;
        setup();
    }
    
    /**
     * Sets up the panel and adds the components.
     */
    public void setup()
    {
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        setAlignmentX(Component.LEFT_ALIGNMENT);
        text = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(text);
        add(scrollPane);
    }
    
    /**
     * @return - Object[] the arguments provided to the 
     * constructor.
     */
    public Object[] getArgs()
    {
        return args;
    }
    
    /**
     * The run() method of the Runnable interface. Called when a
     * MethodRunner is run as a thread. Usage:<br><br>
     * 
     * MethodRunner mr = new MethodRunner(args,method);<br>
     * Thread thread = new Thread(mr,"Name of Thread");<br>
     * thread.start();<br><br>
     * 
     * Then this run() method will be executed in a separate thread.
     * <br><br>
     * There are two features of this run() implementation that you should be 
     * aware of:<br>
     * 1. When this run() method is in a new Thread it calls the
     * ThreadOutputStream.instance().setOutputStream() to change
     * the standard output for the Thread to a TextOutputStream.
     * This TextOutputStream displays the standard output to the
     * user on the MethodRunner JPanel. When the thread is terminated
     * it removes this TextOutputStream from ThreadOutputStream so
     * that ThreadOutputStream does not overflow. Hence it is important
     * not to call the Thread.stop() method on the running thread
     * because ThreadOutputStream will overflow eventually.<br>
     * 2. At the beggining of the execution of this thread the log4j
     * logger is configured to the file 'invocations.log4j'. This
     * is so that log4j is made aware of the standard output stream
     * switching and will display its output on the MethodRunner JPanel.
     */
    public void run()
    {
        TextOutputStream tos = new TextOutputStream();
        // we must remember to remove the thread from the
        // ThreadOutputStream instance() so we don't overflow
        // memory
        ThreadOutputStream.instance().setOutputStream(tos);
        // Configure the logger so that it will pick up the swich of standard out
        // to the JTextArea
        PropertyConfigurator.configure(getClass().getResource("invocations.log4j"));
        logger.debug("RUNNING THREAD");
        try
        {
            Object object = runMethod();
            if( object != null )
            {
                ObjectHolder objectHolder = new ObjectHolder(object);
                add(objectHolder);
                validate();
            }
        }
        catch( Exception e )
        {
            String message = e.getMessage();
            if( message == null )
            {
                Throwable cause = e.getCause();
                if( cause != null )
                {
                    message = cause.getLocalizedMessage();
                }
            }
            if( message == null)
            {
                message = e.getClass().getName();
            }
            logger.warn(message);
            JOptionPane.showMessageDialog(this,message,"Exception",
                    JOptionPane.ERROR_MESSAGE);
        }
        ThreadOutputStream.instance().removeOutputStream();
    }
    
    /**
     *  Runs the set Method with the set arguments.
     *  
     * @return - the Object returned on successful method execution.
     * @throws Exception
     */
    public Object runMethod() throws Exception
    {
        if( method != null )
        {
            return method.invoke(runObject,args);
        }
        else
        {
            logger.error("No method found to run, MethodRunner.runMethod()");
            return null;
        }
    }
    
    /**
     * @return - the JTextArea that displays the output of the Method.
     */
    public JTextArea getJTextArea()
    {
        return text;
    }
    
    /**
     * When any character is written to this TextOutputStream, it is
     * displayed in the JTextArea.
     */
    public class TextOutputStream extends OutputStream
    {
        public void write(int c)
        {
            String cStr = (new Character((char)c)).toString();
            getJTextArea().append(cStr);
        }
    }
    
    /**
     * @param retObject - the Object returned from the Method execution
     */
    public void setObject(Object object)
    {
        this.retObject = object;
    }

}
