/*
 * Created on: Apr 1, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import java.util.*;
import javax.swing.*;
import org.apache.log4j.Logger;
import au.edu.wehi.jcontrol.util.ClassFactory;
import au.edu.wehi.jcontrol.util.PackageClasses;
import au.edu.wehi.jcontrol.util.TreeMouseAdapter;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;

/**
 * A ClassChooser is a JPanel that is displayed as part of 
 * a ClassPanel. It provides a list of classes organised by 
 * packages that the users can select. When a class is
 * selected a MethodChooser is displayed for that class inside
 * the ClassPanel.
 * 
 * @author eduthie
 *
 */
public class ClassChooser extends JPanel
{
    private static Logger logger = Logger.getLogger(ClassChooser.class);
    
    private JTree tree;
    // listener of user selecting classes from tree
    private ActionListener listener;
    
    public ClassChooser()
    {
        super();
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        addTree();
    }
    
    /**
     * Creates the tree of classes to choose from
     */
    private void addTree()
    {
        Hashtable<String,PackageClasses> packages = 
            ClassFactory.instance().getPackages();
        DefaultMutableTreeNode root = 
            new DefaultMutableTreeNode("Available Classes");
        
        Collection<PackageClasses> elements = packages.values();
        Object[] valuesArray = elements.toArray();
        Arrays.sort(valuesArray);
        
        for( int j=0; j < valuesArray.length; ++j)
        {
            PackageClasses pc = (PackageClasses) valuesArray[j];
            pc = pc.sort();
            DefaultMutableTreeNode packageNode =
                new DefaultMutableTreeNode(pc);
            for( int i=0; i < pc.size(); ++i )
            {
                String className = pc.get(i);
                DefaultMutableTreeNode classNode =
                    new DefaultMutableTreeNode(className);
                packageNode.add(classNode);
            }
            root.add(packageNode);
        }
        
        tree = new JTree(root);
        tree.getSelectionModel().setSelectionMode
            (TreeSelectionModel.SINGLE_TREE_SELECTION);
        MouseListener ml = new TreeMouseAdapter(tree) 
        {
            public void selected(int selRow, TreePath selPath) 
            {
                classSelected(selRow,selPath);
            }
        };
        tree.addMouseListener(ml);
        JScrollPane treeView = new JScrollPane(tree);
        treeView.setAlignmentX(Component.LEFT_ALIGNMENT);
        add(treeView);
    }
    
    /**
     * When the user double clicks on a tree node, this method is called.
     * The actionPerformed(ActionEvent method of the ActionListener 
     * registered with addActionListener(ActionListener) is called if 
     * the tree node that was clicked on is a class. The ActionEvent
     * provided has the fully qualified class name as it's action
     * command, retrievable through ActionEvent.getActionCommand().
     * 
     * @param row - row in tree
     * @param path - path to selected node in tree
     */
    private void classSelected(int row, TreePath path)
    {
        String fullName = null;
        
        DefaultMutableTreeNode last = 
            (DefaultMutableTreeNode) path.getLastPathComponent();
        Object object = last.getUserObject();
        if( (object.getClass().getName() == String.class.getName()) &&
            path.getPathCount() >= 2)
        {
            String className = (String) object;
            PackageClasses pc = ((PackageClasses) 
                    ((DefaultMutableTreeNode) 
                            path.getPathComponent(path.getPathCount()-2)).
                            getUserObject());
            String packageName = pc.getName();
            fullName = packageName + "." + className;
            if( listener != null )
            {
                ActionEvent event = 
                    new ActionEvent(this,ActionEvent.ACTION_FIRST,fullName);
                listener.actionPerformed(event);
            }
        }
    }
    
    public void addActionListener(ActionListener listener)
    {
        this.listener = listener;
    }
}
