/*
 * Created on: Apr 5, 2005
 * by: eduthie
 * The Walter and Eliza Hall Institute
 * 
 */
package au.edu.wehi.jcontrol;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.*;
import javax.swing.*;
import org.apache.log4j.Logger;

import au.edu.wehi.jcontrol.util.ClassFactory;

import java.io.IOException;
import java.lang.reflect.*;

/**
 * An ObjectEntryPanel is constructed with a Class.
 * It extends JPanel and allows the user to input an
 * Object of the specified Class. This can be either by
 * entering a String for primitive types (with Class objects
 * that have a ClassName(String) constructor), or by
 * dragging and dropping an XObject over the ObjectEntryPanel.
 * The entered object can be retrieved by calling the
 * getObject() method.
 * 
 * @author eduthie
 *
 */
public class ObjectEntryPanel extends JPanel
{
    private static Logger logger = Logger.getLogger(ObjectEntryPanel.class);
 
    private static int NUM_COLUMNS = 1;
    private static int WIDTH = 500;
    private static int HEIGHT = 30;
    
    private Class theClass = null; // Class of Object to enter
    // Constructor for object if it has a Constructor with a 
    // single String parameter
    private Constructor stringConstructor = null;
    private Object object = null; // Object entered
    // if we have just set the object by drag an dropping and
    // the user had not pressed enter since then in the
    // JTextField, justSetObject will be true, otherwise false
    private boolean justSetObject = false;
    // Where the user enters the String to construct the Object
    // if it has a stringConstructor
    private JTextField textField = null;

    /**
     * Sets up a new ObjectEntryPanel
     * 
     * @param theClass - Class of Object to be entered by user.
     */
    public ObjectEntryPanel( Class theClass )
    {
        super();
        setAlignmentX(Component.LEFT_ALIGNMENT);
        setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
        setBorder(BorderFactory.createLineBorder(Color.BLUE));
        setTransferHandler(new ObjectEntryTransferHandler());
        this.theClass = theClass;
        
        JLabel label = new JLabel(theClass.getName());
        add(label);
        
        stringConstructor = ClassFactory.instance().isPrimitive(theClass);
        if( stringConstructor != null )
        {
            textField = new JTextField(NUM_COLUMNS);
            textField.setDragEnabled(true);
            textField.setMaximumSize(new Dimension(WIDTH,HEIGHT));
            textField.addActionListener(new TextFieldActionListener());
            add(textField);
        }
    }
    
    /**
     * @author eduthie
     *
     * This ActionListener is fired when a user presses enter
     * in the JTextField (only present for primitive types).
     */
    public class TextFieldActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                constructStringObject(e.getActionCommand());
            }
            catch( IOException ioe )
            {
                // not too much of a problem
            }
        }
    }
    
    /**
     * After the user presses enter in the JTextField, an attempt
     * is made to construct an object of the current Class theClass
     * by the constructor ClassName(String). If it is successfull
     * the current Object object is set to the constructed object.
     * If is fails a log message is written and an error message
     * dialog is presented to the user.
     */
    public void constructStringObject(String s)
        throws IOException
    {
        String heading = "Failed to Construct Object";
        
        try
        {
            if( stringConstructor != null )
            {
                Object[] args = {s};
                setObject(stringConstructor.newInstance(args));
            }
        }
        catch( Exception e )
        {
            String message = e.getCause().toString();
            logger.debug(heading + ": " + message);
            JOptionPane.showMessageDialog(this,message,heading,JOptionPane.ERROR_MESSAGE);
            throw new IOException(e.getMessage());
        }
    }
    
    /**
     * Returns the Object entered by the user or null if no Object
     * has been entered. If the Object is a primitive type, has a
     * constructor with one argument of type String, the text in
     * the JTextField is retrieved and an an attempt to create
     * that object is made. This is regardless of whether the
     * user has pressed enter or not.
     * 
     * @return Object - the object entered by the user, null
     * if no object has been successfully entered.
     * @throws IOException - if there is an input error.
     */
    public Object getObject()
        throws IOException
    {
        if( (stringConstructor != null) &&
            (textField != null) &&
            (!isJustSetObject()) )
        {
            // we retrieve the text again in case
            // it has changed, but we don't do this
            // if the object has just been set
            // by dragging and dropping
            String s = textField.getText();
            if( s != null )
            {
                constructStringObject(s);
            }
        }
        
        setJustSetObject(false);
        return object;
    }
    
    /**
     * Allows ObjectHolder object to be dragged in.
     */
    public class ObjectEntryTransferHandler extends TransferHandler
    {
        /**
         * Indicates whether a component would accept an import of the given 
         * set of data flavors prior to actually attempting to import it.
         */
        public boolean canImport(JComponent comp, DataFlavor[] transferFlavors)
        {
            for( int i=0; i < transferFlavors.length; ++i )
            {
                if( !transferFlavors[i].equals(ObjectHolder.DATA_FLAVOR))
                {
                    return false;
                }
                if( (stringConstructor == null) &&
                    (!theClass.getName().equals(
                            transferFlavors[i].getHumanPresentableName())) )
                {
                    // we don't have the option of just creating the object with
                    // the toString method of the source object
                    // and the class of the dragged object is different
                    // to the required class
                    return false;
                }
            }
            return true;
        }
        
        /**
         * Causes a transfer to a component from a clipboard or a DND drop operation.
         */
        public boolean importData(JComponent comp, Transferable t) 
        {
            try
            {
                Object object = t.getTransferData(ObjectHolder.DATA_FLAVOR);
                ObjectHolder oh = (ObjectHolder) object;
                setObject(oh.getObject());
                setJustSetObject(true);
                return true;
            }
            catch(UnsupportedFlavorException usfe)
            {
                logger.error(usfe.getMessage());
                return false;
            }
            catch( IOException ioe )
            {
                logger.error(ioe.getMessage());
                return false;
            }
        }
    }
    
    /**
     * Sets the user entered Object to the given Object and
     * informs the user of this by changing the border color
     * of the ObjectEntryPanel to green. If the given object
     * is not of the correct class a message is displayed to
     * the user. If a stringConstructor for the Object we
     * are constructing is availiable and the given Object is
     * of the wrong Class, we try and construct the Object
     * using the toString() String from the given Object.
     */
    public void setObject(Object object)
    {   
        if(object.getClass().getName().equals(theClass.getName()))
        {
            this.object = object;
            setBorder(BorderFactory.createLineBorder(Color.GREEN));
        }
        else if( stringConstructor != null)
        {
            // if we have a string constructor and the Object is 
            // of the wrong class, lets try and construct it
            // with the toString() method of the given Object
            try
            {
                constructStringObject(object.toString());
            }
            catch( IOException ioe )
            {
                displayWrongClassMessage(object);
            }
        }
        else
        {
            displayWrongClassMessage(object);
        }
    }
    
    /**
     * Displays a message to the user and the log that the given object
     * is of the wrong class.
     */
    public void displayWrongClassMessage(Object object)
    {
        String message = "<html>Recieved: " + object.getClass().getName()
                + "<br>Expected: " + theClass.getName() + "</html>";
        String title = "Invalid Class of Object";
        String logMessage = "Invalid class recieved in ObjectEntryPanel";
        logger.warn(logMessage);
        JOptionPane.showMessageDialog(this, message, title,
                JOptionPane.ERROR_MESSAGE); 
    }
    
    /**
     * Sets whether or not the Object entered has just been
     * set by dragging and dropping an ObjectHolder or not.
     */
    public void setJustSetObject(boolean justSetObject)
    {
        this.justSetObject = justSetObject;
    }
    
    /**
     * If the contained Object has just been set by dragging
     * and dropping an Object over this ObjectEntryPanel
     * this will return true. If Enter has been pressed in
     * the text field since then or if no Object has yet
     * been dropped, false is returned.
     */
    public boolean isJustSetObject()
    {
        return justSetObject;
    }
    
    /**
     * Sets the text in the JTextField, if there is one,
     * to the given string
     */
    public void setText(String text)
    {
        if( textField != null)
        {
            textField.setText(text);
        }
    }
}
