package pademelon.das;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import org.apache.log4j.*;
import pademelon.lib.Database;
import pademelon.lib.Constants;
import java.io.Serializable;

/**
 * Provides message-driven services for saving and accessing 
 * objects in persistent storage
 * 
 * @ejb.bean name="DASMDBean"
 *           display-name="DataAccessServiceMessageDrivenBean"
 *           description="A Message Driven Bean that provides data access services"
 *           destination-type="javax.jms.Queue"
 *           acknowledge-mode="Auto-acknowledge"
 *     
 */
public class DASMDBean implements MessageDrivenBean, MessageListener 
{
    private Logger logger = Logger.getLogger(getClass());
    
    public DASMDBean() 
    {
        super();
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx)
        throws EJBException 
        {
        // TODO Auto-generated method stub

    }

    public void ejbRemove() throws EJBException 
    {
        // TODO Auto-generated method stub

    }
    
    public void ejbCreate() throws EJBException
    {
        
    }

    public void onMessage(Message message) 
    {

        try
        {
            if( message instanceof ObjectMessage )
            {
                String command = message.getStringProperty(Constants.COMMAND_NAME);
                if( command == null )
                {
                    logger.error("no command passed in JMS message");
                }
                else
                {
                    Serializable object = ((ObjectMessage) message).getObject();
                        
                    if( command.equals(Constants.SAVE_DATABASE_COMMAND_DAS) )
                    {
                        new DASHelper().saveDatabase((Database) object);
                    }
                    else
                    {
                        logger.warn("Unknown JMS command recieved: " + command);
                    }
                }
            }
            else
            {
                logger.warn("non-object message recieved in: " + getClass().getName());
            }
        }
        catch (JMSException e)
        {
            logger.error(e.getMessage());
        }

    }
}
