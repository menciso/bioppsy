package pademelon.das;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.db4o.*;
import com.db4o.messaging.*;
import org.apache.log4j.Logger;

/**
 * Used to start up the server for client server operation of 
 * the database. Not currently used because it is way too
 * slow! 
 * 
 * @author eduthie
 *
 */
public class StartServer extends Thread implements MessageRecipient
{
    private static Logger logger = Logger.getLogger(StartServer.class);
    
    private static String PROPERTIES_FILE = "das.properties";
    private int numProp = 6;
    private String[] properties = new String[numProp];
    private String[] keys = {"server","port","user","password","filename","stopMessage"};
    String server, user, password, filename, stopMessage;
    int port;

    /**
     * setting the value to true denotes that the server should be closed
     */
    private boolean stop = false;
    
    public static void main(String[] args)
    {
        new StartServer().start();
    }
    
    /*
     * Loads the data for database connection
     */
    public boolean loadProperties()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(PROPERTIES_FILE);
            if( stream == null )
            {
                logger.error("Could not find properties: " + PROPERTIES_FILE);
                return false;
            }
            
            // get database filename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            for(int i=0; i < numProp; ++i )
            {
                String prop = typesProp.getProperty(keys[i]);
                if( prop == null )
                {
                    logger.error("Failed to load property: " + keys[i]);
                    return false;
                }
                properties[i] = prop;
            }
            server = properties[0];
            port = new Integer(properties[1]).intValue();
            user = properties[2];
            password = properties[3];
            filename = properties[4];
            stopMessage = properties[5];
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        
        return true;
    }

    /**
     * opens the ObjectServer, and waits forever until close() is called or a
     * StopServer message is being received.
     */
    public void run()
    {
        synchronized (this)
        {
            if( !loadProperties() )
            {
                logger.error("Failed to load properties");
                return;
            }
            
            //logger.info("About to open database server");
            ObjectServer db4oServer = Db4o.openServer(filename, port);
            //logger.info("About to obtain grant access to database server");
            db4oServer.grantAccess(user, password);

            // Using the messaging functionality to redirect all
            // messages to this.processMessage
            db4oServer.ext().configure().setMessageRecipient(this);

            // to identify the thread in a debugger
            Thread.currentThread().setName(this.getClass().getName());

            // We only need low priority since the db4o server has
            // it's own thread.
            Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
            try
            {
                if (!stop)
                {
                    // wait forever for notify() from close()
                    //logger.info("About to wait in thread");
                    this.wait(Long.MAX_VALUE);
                    //logger.info("Wait timed out");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            logger.info("About to close database server");
            db4oServer.close();
        }
    }

    /**
     * messaging callback
     * 
     * @see com.db4o.messaging.MessageRecipient#processMessage(ObjectContainer,
     *      Object)
     */
    public void processMessage(ObjectContainer con, Object message)
    {
        if (stopMessage.equals((String) message))
        {
            close();
        }
    }

    /**
     * closes this server.
     */
    public void close()
    {
        synchronized (this)
        {
            stop = true;
            this.notify();
        }
    }
}
