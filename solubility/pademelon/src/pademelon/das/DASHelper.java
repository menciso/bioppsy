package pademelon.das;

import pademelon.lib.Database;
import com.db4o.ObjectSet;
import org.apache.log4j.Logger;
import pademelon.lib.Molecule;
import pademelon.lib.MoleculeCollection;

/**
 * DASHelper provides all methods for storing and retrieving
 * objects from persistent storage.
 * 
 * @author eduthie
 */
public class DASHelper
{
    private static Logger logger = Logger.getLogger(DASHelper.class);
    
    /**
     * Saves the given Database in persistent storage. If a database already
     * exists with the same name it is replaced.
     */
    public void saveDatabase(Database database)
    {
        DatabaseHelper helper = new DatabaseHelper();
        if( !helper.connect() )
        {
            logger.error("Failed to connect to database");
            return;
        }
        Database query = new Database(database.getName());
        ObjectSet result = helper.get(query);
        if( (result != null) && (result.size() != 0) )
        {
            logger.debug("Database being replaced: " + query.getName());
            helper.delete(result.next());
        }
        else
        {
            logger.debug("Database not found with name: " + database.getName());
        }
        helper.set(database);
        helper.close();
    }
    
    /**
     * Searches for the given Database in the db by example. If it is found the
     * given Database is returned populated with full data. Otherwise null
     * is returned.
     */
    public Database getDatabase(Database database)
    {
        Database toReturn = database;
        DatabaseHelper helper = new DatabaseHelper();
        if( !helper.connect() )
        {
            logger.error("Failed to connect to database");
            return null;
        }
        ObjectSet result = helper.get(database);
        if( result.size() > 1 )
        {
            logger.error(result.size() + " Databases found for name: " + database.getName());
            toReturn = (Database) result.next();
        }
        if( result.size() <= 0 )
        {
            logger.debug("No Database found for name: " + database.getName());
            toReturn = null;
        }
        else
        {
            toReturn = (Database) result.next();
        }       
        helper.close();
        
        return toReturn;
    }
    
    /**
     * Searches for Molecules by example. Returns all molecules with same
     * fields as provided Molecule. Returns an empty MoleculeCollectionTest
     * if none are found.
     */
    public MoleculeCollection searchForMolecules(Molecule molecule)
    {
        MoleculeCollection molecules = new MoleculeCollection();
        DatabaseHelper helper = new DatabaseHelper();
        if( !helper.connect() )
        {
            logger.error("Failed to connect to database");
        }
        ObjectSet results = helper.get(molecule);
        while( results.hasNext() )
        {
            Molecule result = (Molecule) results.next();
            molecules.add(result);
        }
        helper.close();
        return molecules;
    }
}
