package pademelon.das;
import org.apache.log4j.Logger;

import com.db4o.*;
import java.util.Properties;
import java.io.*;

/**
 * A DatabaseHelper represents a transaction with the database. Use DatabaseHelper()
 * to start a transaction. Use get(), set(), or delete() to perform operations and
 * use close() to finalise the transaction. Note close() must be called.
 * 
 * It you modify this class you must redeploy the DatabaseServer.sar
 * and the Pademelon.ear.
 * 
 * @author eduthie
 */
public class DatabaseHelper
{
    private static Logger logger = Logger.getLogger(DatabaseHelper.class);
    private static String PROPERTIES_FILE = "das.properties";
    private int numProp = 8;
    private String[] properties = new String[numProp];
    private String[] keys = {"server","port","user","password","filename","semaphore","timeout","numTries"};
    String server, user, password, filename, semaphore;
    int port, timeout, numTries;
    
    private ObjectContainer client = null;
    private static ObjectServer os = null;
    // true if locked, false otherwise
    private static boolean lock = false;
    private boolean connected = false;
    
    /**
     * Constructs a DatabaseHelper which forms a transaction with the 
     * database. Don't forget to call close() when done with the 
     * transaction as the semaphore must be released.
     * 
     * You must call connect() to start the transaction.
     */
    public DatabaseHelper()
    {
        loadProperties();
    }
    
    /*
     * Loads the data for database connection
     */
    public boolean loadProperties()
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream(PROPERTIES_FILE);
            if( stream == null )
            {
                logger.error("Could not find properties: " + PROPERTIES_FILE);
                return false;
            }
            
            // get database filename
            Properties typesProp = new Properties();
            typesProp.loadFromXML(stream);
            for(int i=0; i < numProp; ++i )
            {
                String prop = typesProp.getProperty(keys[i]);
                if( prop == null )
                {
                    logger.error("Failed to load property: " + keys[i]);
                    return false;
                }
                properties[i] = prop;
            }
            server = properties[0];
            port = new Integer(properties[1]).intValue();
            user = properties[2];
            password = properties[3];
            filename = properties[4];
            semaphore = properties[5];
            timeout = new Integer(properties[6]).intValue();
            numTries = new Integer(properties[7]).intValue();
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }
        
        return true;
    }
    

    /**
     * Connects to the database
     */
    public synchronized boolean connect()
    {
        logger.info("Connecting to database");
        if( !getClient() )
        { 
            logger.error("Failed to connect to the database");
            return false;
        }
        
        // obtain exclusive access to the database
        logger.debug("Obtaining semaphore");
        while(getLock() != true)
        {
            logger.debug("Failed to obtain a semaphore");
            try
            {
                this.wait(numTries);
            }
            catch( InterruptedException ie )
            {
                logger.info(ie.getMessage());
            }
        }
        connected = true;
        return true;
    }
    
    private boolean getClient()
    {
        try
        {
            client = os.openClient();
            return true;
        }
        catch( Exception e)
        {
            client = null;
            return false;
        }
    }
    
    /**
     * Starts up the database server which must be run
     * in the same JVM as the DAS services for performance
     * reasons.
     */
    public synchronized void startServer()
    {
        if( os == null )
        {
            os = Db4o.openServer(filename,port);
        }
    }
    
    /**
     * Stops the database server
     */
    public synchronized void stopServer()
    {
        if( os != null )
        {
            os.close();
            os = null;
        }
    }
    
    /**
     * Closes the connection to the database
     */
    public void close()
    {
        logger.debug("Closing database connection");
        if( connected )
        {
            logger.debug("Releasing semaphore");
            releaseLock();
            client.close();
            client = null;
            connected = false;
        }
    }
    
    /**
     * Looks up and Object by example and returns the result
     * if found, null otherwise.
     */
    public ObjectSet get(Object key)
    {
        if( !connected )
        {
            logger.error("Called get() with no database connection");
            return null;
        }
        return client.get(key);
    }
    
    /**
     * Deletes the object if found, does nothing otherwise
     */
    public void delete(Object key)
    {
        if( !connected )
        {
            logger.error("Called delete() with no database connection");
        }
        client.delete(key);
    }
    
    /**
     * Sets the object in the database
     */
    public void set(Object obj)
    {
        if( !connected )
        {
            logger.error("Called set() with no database connection");
        }
        client.set(obj);
    }
    
    public void finalize()
    {
        close();     
    }
    
    public synchronized boolean getLock()
    {
        if( lock == false )
        {
            lock = true;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public synchronized void releaseLock()
    {
        lock = false;
    }
}
