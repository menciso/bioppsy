package pademelon.das;

import org.jboss.system.ServiceMBeanSupport;
import org.apache.log4j.Logger;

/**
 * An MBean that starts the database server in the jboss JVM
 * so that any DAS objects in the same JVM can access it.
 * 
 * @author eduthie
 *
 */
public class DatabaseServer extends ServiceMBeanSupport implements DatabaseServerMBean
{
    private static Logger logger = Logger.getLogger(DatabaseServer.class);
    
    public void startServer()
    {
        logger.info("Starting Pademelon Database Server");
        new DatabaseHelper().startServer();
    }
    
    public void stopServer()
    {
        logger.info("Stoping Pademelon Database Server");
        new DatabaseHelper().stopServer();
    }
    
    public void startService() throws Exception
    {
        startServer();
    }
    
    public void stopService() throws Exception
    {
        stopServer();
    }
}
