package pademelon.das;

import java.rmi.RemoteException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.CreateException;
import pademelon.lib.Database;
import org.apache.log4j.Logger;
import pademelon.lib.MoleculeCollection;
import pademelon.lib.Molecule;

/**
 * @ejb.bean name="DASSession" display-name="DASSession" description="Session
 *           Bean for access to database services" jndi-name="ejb/DASSession"
 *           type="Stateless" view-type="remote"
 */
public class DASSessionBean implements SessionBean
{

    private static Logger logger = Logger.getLogger(DASSessionBean.class);

    public DASSessionBean()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Returns the given Database populated with data if found, null otherwise.
     * 
     * Business method
     * 
     * @ejb.interface-method view-type = "remote"
     */
    public Database getDatabaseData(Database database)
    {
        return new DASHelper().getDatabase(database);
    }

    public void setSessionContext(SessionContext ctx) throws EJBException,
            RemoteException
    {
        // TODO Auto-generated method stub

    }

    public void ejbRemove() throws EJBException, RemoteException
    {
        // TODO Auto-generated method stub

    }

    public void ejbActivate() throws EJBException, RemoteException
    {
        // TODO Auto-generated method stub

    }

    public void ejbPassivate() throws EJBException, RemoteException
    {
        // TODO Auto-generated method stub

    }

    /**
     * Default create method
     * 
     * @throws CreateException
     * @ejb.create-method
     */
    public void ejbCreate() throws CreateException
    {
        // TODO Auto-generated method stub
    }

    /**
     * Returns all Molecules that have identical fields to the given Molecule.
     * Returns and empty MoleculeCollectionTest if none are found.
     * 
     * Business method
     * 
     * @ejb.interface-method view-type = "remote"
     */
    public MoleculeCollection searchForMolecules(Molecule molecule)
    {
        return new DASHelper().searchForMolecules(molecule);
    }
}
