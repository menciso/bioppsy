package pademelon.das;

import org.jboss.system.*;

/**
 * An MBean that starts the database server in the jboss JVM
 * so that any DAS objects in the same JVM can access it.
 * 
 * Not for some reason this class cannot be in the interfaces
 * package and must be in the same package as the implementing
 * DatabaseServer class. Otherwise the MBean does not deploy
 * correctly.
 * 
 * @author eduthie
 *
 */
public interface DatabaseServerMBean extends ServiceMBean
{
    public void startServer();
    public void stopServer();
}
