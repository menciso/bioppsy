package pademelon.cs;

import java.rmi.RemoteException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.CreateException;
import pademelon.lib.Molecule;
import pademelon.lib.MoleculeCollection;

/**
 * @ejb.bean name="CSSession"
 *           display-name="Name for CSSession"
 *           description="Description for CSSession"
 *           jndi-name="ejb/CSSession"
 *           type="Stateless"
 *           view-type="remote"
 */
public class CSSessionBean implements SessionBean {

    public CSSessionBean() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void setSessionContext(SessionContext ctx)
        throws EJBException,
        RemoteException {
        // TODO Auto-generated method stub

    }

    public void ejbRemove() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    public void ejbActivate() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    public void ejbPassivate() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    /**
     * Default create method
     * 
     * @throws CreateException
     * @ejb.create-method
     */
    public void ejbCreate() throws CreateException {
        // TODO Auto-generated method stub
    }

    /**
     * Returns Molecules with the same fields as the given Molecule.
     * An empty MoleculeCollectionTest is returned if none are found.
     * 
     * Business method
     * @ejb.interface-method  view-type = "remote"
     */
    public MoleculeCollection searchForMolecules(Molecule molecule) 
    {
        return new CSHelper().searchForMolecules(molecule);
    }
}
