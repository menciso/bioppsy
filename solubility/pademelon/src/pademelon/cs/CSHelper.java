package pademelon.cs;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import java.rmi.RemoteException;
import javax.ejb.CreateException;
import pademelon.das.interfaces.DASSessionHome;
import pademelon.lib.Database;
import pademelon.lib.Constants;
import pademelon.lib.MoleculeCollection;
import pademelon.lib.Molecule;
import org.apache.log4j.Logger;

/**
 * The CSHelper class provides all services provided by the CSHelper module.
 * 
 * @author eduthie
 *
 */
public class CSHelper
{
    private static Logger logger = Logger.getLogger(CSHelper.class);
    
    /**
     * Saves the given Database to persistent storage using
     * the DASHelper.
     */
    public void saveDatabase(Database database)
    {
        InitialContext context = null;
        ConnectionFactory connectionFactory = null;
        Connection connection = null;
        Session session = null;
        Destination destination = null;
        MessageProducer messageProducer = null;
        ObjectMessage message = null;
        
        try 
        {
            context = new InitialContext();
            connectionFactory = (ConnectionFactory) context.lookup(
                    Constants.CONNECTION_FACTORY);
            destination = (Queue) context.lookup(
                    Constants.DAS_QUEUE);
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            messageProducer = session.createProducer(destination);
            message = session.createObjectMessage();
            message.setStringProperty(Constants.COMMAND_NAME,Constants.SAVE_DATABASE_COMMAND_DAS);
            message.setObject(database);
            messageProducer.send(message);
        } 
        catch (JMSException e) 
        {
            logger.error("JMSException occurred: " + e.toString());
            logger.error(e.getMessage());
        }
        catch (NamingException e) 
        {
            logger.error("JNDI lookup failed: " + e.toString());
            logger.error(e.getMessage());
            return;
        }
        finally 
        {
            if (connection != null) 
            {
                try 
                {
                    connection.close();
                } 
                catch (JMSException e) 
                {
                }
            }
        }
    }
    
    /**
     * Returns a collection of Molecules with identical fields
     * to the given Molecule. Returns null on error and an 
     * empty MoleculeCollectionTest if no Molecules are found.
     */
    public MoleculeCollection searchForMolecules(Molecule molecule)
    {
        try
        {
            Context context = new InitialContext();
            Object object = context.lookup(Constants.DAS_EJB);
            if( object == null )
            {
                logger.error("Object not found: " + Constants.DAS_EJB);
                return null;
            }
            DASSessionHome home = (DASSessionHome) PortableRemoteObject.narrow(object,DASSessionHome.class);
            if( home == null )
            {
                logger.error("Retrieved object is null in searchForMolecules");
                return null;
            }
            pademelon.das.interfaces.DASSession bean = home.create();
            MoleculeCollection results = bean.searchForMolecules(molecule);
            return results;
        }
        catch( NamingException ne )
        {
            logger.error(ne.getMessage());
            return null;
        }
        catch( RemoteException re )
        {
            logger.error(re.getMessage());
            return null;
        }
        catch( CreateException ce )
        {
            logger.error(ce.getMessage());
            return null;
        }
    }
}
