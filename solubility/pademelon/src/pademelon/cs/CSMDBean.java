package pademelon.cs;

import java.io.Serializable;
import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.ejb.CreateException;
import org.apache.log4j.Logger;
import pademelon.lib.Database;
import pademelon.lib.Constants;

/**
 * @ejb.bean name="CSMDBean"
 *           display-name="CentralServicesMessageDrivenBean"
 *           description="Central Services Message Driven Bean"
 *           destination-type="javax.jms.Queue"
 *           acknowledge-mode="Auto-acknowledge"
 */
public class CSMDBean implements MessageDrivenBean, MessageListener 
{
    private Logger logger = Logger.getLogger(getClass());
    
    public CSMDBean() 
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx)
        throws EJBException 
    {
        // TODO Auto-generated method stub

    }

    public void ejbRemove() throws EJBException 
    {
        // TODO Auto-generated method stub

    }

    public void onMessage(Message message) 
    {

        try
        {
            if( message instanceof ObjectMessage )
            {
                String command = message.getStringProperty(Constants.COMMAND_NAME);
                if( command == null )
                {
                    logger.error("no command passed in JMS message");
                }
                else
                {
                    Serializable object = ((ObjectMessage) message).getObject();
                        
                    if( command.equals(Constants.SAVE_DATABASE_COMMAND_CS) )
                    {
                        new CSHelper().saveDatabase((Database) object);
                    }
                    else
                    {
                        logger.warn("Unknown JMS command recieved: " + command);
                    }
                }
            }
            else
            {
                logger.warn("non-object message recieved in: " + getClass().getName());
            }
        }
        catch (JMSException e)
        {
            logger.error(e.getMessage());
        }
    }

    /**
     * Default create method
     * 
     * @throws CreateException
     */
    public void ejbCreate() throws EJBException 
    {
        // TODO Auto-generated method stub
    }
}
