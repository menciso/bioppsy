package pademelon.user;

import org.jargp.*;
import pademelon.lib.Constants;
import pademelon.lib.Database;
import pademelon.lib.Molecule;
import pademelon.lib.MoleculeCollection;
import pademelon.test.AllTests;
import java.io.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * The main command-line interface for the pademelon package
 * 
 * @author eduthie
 */
public class CommandLine
{
    private static Logger logger = Logger.getLogger(CommandLine.class);
    
    /** Command line parameter definitions. */
    private static final ParameterDef[] BASE_PARAMETERS =
    {
        new BoolDef('C', "createFlag","create a database"),
        new BoolDef('S', "searchFlag", "search for molecules"),
        new IntDef('n', "molNumber", "molecule number"),
        new StringDef('d', "databaseName", "database name"),
        new StringDef('f', "filename", "database filename"),
        new StringDef('m', "molName", "molecule name"),
        new StringDef('s', "supCode", "molecule supplier code"),
        new StringDef('F',"outFile","output mol2 filename"),
        new BoolDef('?', "helpFlag", "display usage information")
    };
    
    private boolean createFlag;
    private boolean searchFlag;
    private String databaseName;
    private String filename;
    private int molNumber = -1;
    private String molName;
    private String supCode;
    private boolean helpFlag;
    private String outFile;
    
    public CommandLine()
    {
        PropertyConfigurator.configure(
                CommandLine.class.getResource("user.log4j"));
    }
    
    public static void main(String[] args) 
    {
        if( !mainMethod(args) )
        {
            System.exit(1);
        }
    }
    
    public static boolean mainMethod(String[] args)
    {
        PropertyConfigurator.configure(
                CommandLine.class.getResource("user.log4j"));
        CommandLine instance = new CommandLine();
        if( !instance.parseCommandLine(args) )
        {
            logger.error("Failed to parse command line arguments");
            return false;
        }
        instance.operate();
        return true;
    }
    
    public boolean parseCommandLine(String[] args)
    {
        logger.debug("Parsing command line arguments");
        try
        {    
            // parse the command line arguments
            ArgumentProcessor proc = new ArgumentProcessor(BASE_PARAMETERS);
            if (args.length > 0) 
            {
                proc.processArgs(args, this);
                StringTracker xargs = proc.getArgs();
                while (xargs.hasNext()) 
                {
                    logger.info("extra argument: " + xargs.next());
                }
            } 
            else 
            {
                this.helpFlag = true;
            }
            
            // print usage information if problem with parameters
            if (this.helpFlag) 
            {
                System.out.println("\nUsage: java -jar Pademelon.jar [-options] extra\n" +
                    "Options are:");
                proc.listParameters(80, System.out);
            }
            return true;
        }
        catch( ArgumentErrorException aee )
        {
            logger.error(aee);
            return false;
        }
    }
    
    /**
     * Runs the required commands
     */
    public void operate()
    {
        logger.debug("Running pademelon");
        // create a database
        if(createFlag)
        {
            logger.info("Creating a database");
            if( filename == null )
            {
                logger.error("Please provide a database filename");
                return;
            }
            if( databaseName == null )
            {
                String[] splitLine = filename.split("/");
                String endName = splitLine[splitLine.length-1];
                int index = endName.indexOf('.');
                databaseName = endName.substring(0,index);
            }
            if( !createDatabase() )
            {
                logger.error("Failed to create database");
            }
        }
        else if(searchFlag)
        {
            logger.info("Searching for Molecules");
            if( !searchForMolecules() )
            {
                logger.error("Search Failed");
            }
        }
        else
        {
            logger.warn("No commands to perform");
        }
    }
    
    public boolean createDatabase()
    {
        try
        {
            Database database = new Database(databaseName);
            InputStream input = new FileInputStream(filename);
            if( input == null )
            {
                logger.error("Failed to read file: " + filename);
                return false;
            }
            if( !database.load(input))
            {
                logger.error("Failed to load file: " + filename);
                return false;
            }
            UserHelper helper = new UserHelper();
            if( !helper.createDatabase(database) )
            {
                logger.error("Failed to send create Database message");
                return false;
            }
            logger.info("Sent create database message for: " + filename);
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error("File not found: " + filename);
            logger.error(fnfe.getMessage());
            return false;
        }
        
        return true;
    }
    
    public boolean searchForMolecules()
    {
        Molecule query = new Molecule();
        if( molNumber != -1 )
        {
            query.setNumber(molNumber);
        }
        if( molName != null )
        {
            query.setName(molName);
        }
        if( supCode != null )
        {
            query.setSupCode(supCode);
        }
        if( databaseName != null )
        {
            Database database = new Database(databaseName);
            query.setDatabase(database);
        }
        UserHelper helper = new UserHelper();
        MoleculeCollection results = helper.searchForMolecules(query);
        if( results == null )
        {
            logger.error("Failed to search for molecules");
            return false;
        }
        logger.debug("Number of molecules found: " + results.size());
        displayMoleculeCollection(results);
        
        return true;
    }
    
    public void displayMoleculeCollection(MoleculeCollection collection)
    {
        if( outFile == null )
        {
            if( collection.size() == 0 )
            {
                System.out.println("No Molecules found");
                return;
            }
            System.out.println("");
            System.out.println("number, name, supCode, database");
            System.out.println("");
            for( int i=0; i < collection.size(); ++i )
            {
                Molecule mol = collection.elementAt(i);
                String line = "";
                line += mol.getNumber();
                line += ", ";
                line += mol.getName();
                line += ", ";
                line += mol.getSupCode();
                Database database = mol.getDatabase();
                if( database != null )
                {
                    line += ", ";
                    line += database.getName();
                }
                System.out.println(line);
            }
        }
        else
        {
            try
            {
                logger.info("Sending search output to file: " + outFile);
                FileOutputStream fos = new FileOutputStream(outFile);
                if( fos == null )
                {
                    logger.error("Failed to open a file: " + outFile);
                    return;
                }
                if( !collection.writeToStream(fos) )
                {
                    logger.error("Failed to write to file: " + outFile);
                }
                logger.info("Written data to file: " + outFile);
            }
            catch( FileNotFoundException fnfe )
            {
                logger.error(fnfe.getMessage());
                return;
            }
            catch( IOException ioe )
            {
                logger.error(ioe.getMessage());
                return;
            }
        }
        
    }
}
