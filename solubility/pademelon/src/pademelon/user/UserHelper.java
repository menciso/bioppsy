package pademelon.user;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.apache.log4j.Logger;

import pademelon.cs.interfaces.CSSession;
import pademelon.cs.interfaces.CSSessionHome;
import pademelon.lib.*;
import pademelon.test.cs.CSTestHelper;

/**
 * The UserHelper class provides the methods that access
 * the CS, encapsulating the use of the CS interface.
 * 
 * @author eduthie
 *
 */
public class UserHelper
{
    private static Logger logger = Logger.getLogger(UserHelper.class);
    
    /**
     * Sends a message to the CS to save the given database.
     * Returns true if the message is send correctly, false
     * otherwise.
     */
    public boolean createDatabase(Database database)
    {
        InitialContext context = null;
        ConnectionFactory connectionFactory = null;
        Connection connection = null;
        Session session = null;
        Destination destination = null;
        MessageProducer messageProducer = null;
        ObjectMessage message = null;

        try
        {
            // test sending a SaveDatabase
            context = new InitialContext();
            connectionFactory = (ConnectionFactory) context
                    .lookup(Constants.CONNECTION_FACTORY);
            destination = (Queue) context.lookup(Constants.CS_QUEUE);
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            messageProducer = session.createProducer(destination);
            message = session.createObjectMessage();
            message.setStringProperty(Constants.COMMAND_NAME,
                    Constants.SAVE_DATABASE_COMMAND_CS);
            message.setObject(database);
            logger.debug("Sending Message");
            messageProducer.send(message);
        }
        catch (NamingException e)
        {
            logger.error("JNDI lookup failed: " + e.toString());
            logger.error(e.getMessage());
            return false;
        }
        catch (JMSException e)
        {
            logger.error("Exception occurred: " + e.toString());
            return false;
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (JMSException e)
                {
                }
            }
        }
        return true;
    }
    
    /**
     * Returns a MoleculeCollectionTest of all Molecules with the
     * same fields as the given Molecule. Returns null on error
     * and an empty MoleculeCollectionTest if none are found.
     */
    public MoleculeCollection searchForMolecules(Molecule molecule)
    {
        try
        {
            InitialContext initial = new InitialContext();
            Object obj = initial.lookup(Constants.CS_EJB);
            CSSessionHome home = (CSSessionHome) PortableRemoteObject.narrow(obj,CSSessionHome.class);
            CSSession bean = home.create();
            MoleculeCollection results = bean.searchForMolecules(molecule);
            return results;
        }
        catch (NamingException ne)
        {
            logger.error("NamingException");
            logger.error(ne.getMessage());
            return null;
        }
        catch( CreateException ce )
        {
            logger.error(ce.getMessage());
            return null;
        }
        catch( RemoteException re )
        {
            logger.error(re.getMessage());
            return null;
        }
    }
}
