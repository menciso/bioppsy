package pademelon.test.lib;

import java.io.*;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import pademelon.lib.Database;
import pademelon.test.AllTests;
import junit.framework.TestCase;

public class MoleculeCollectionTest extends TestCase
{
    private String filename = "drugs.mol2";
    private String tempFilename = "temp.mol2";
    private static Logger logger = Logger.getLogger(MoleculeCollectionTest.class);

    public MoleculeCollectionTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /*
     * Test method for 'pademelon.lib.MoleculeCollection.writeToStream(OutputStream)'
     */
    public void testWriteToStream()
    {
        try
        {
            // Initiate a molecule collection
            InputStream in = AllTests.class.getResourceAsStream(filename);
            Database db = new Database(filename);
            assertNotNull(db);
            assertTrue(db.load(in) );
            in.close();
            
            // Try writing it to a file and reading it again
            FileOutputStream fos = new FileOutputStream(tempFilename);
            assertTrue(db.writeToStream(fos));
            File file = new File(tempFilename);
            assertTrue(file.exists());
            FileInputStream tempIn = new FileInputStream(tempFilename);
            Database retrieved = new Database(tempFilename);
            assertTrue(retrieved.load(tempIn));
            tempIn.close();
            assertEquals(db.size(),retrieved.size());
            file = new File(tempFilename);
            file.delete();
        }
        catch(FileNotFoundException fnfe)
        {
            fail(fnfe.getMessage());
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
