package pademelon.test.lib;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import pademelon.lib.Molecule;
import pademelon.lib.Database;
import pademelon.test.AllTests;
import junit.framework.TestCase;

public class DatabaseTest extends TestCase
{
    private String filename = "drugs.mol2";
    private static Logger logger = Logger.getLogger(DatabaseTest.class);
    
    public DatabaseTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /*
     * Test method for 'pademelon.lib.Database.load(InputStream)'
     */
    public void testLoad()
    {
        InputStream in = AllTests.class.getResourceAsStream(filename);
        Database db = new Database(filename);
        assertNotNull(db);
        assertTrue(db.load(in) );
        
        String last = null;
        for( int j=0; j < db.size(); ++j )
        {
            Vector<String> mol2 = db.elementAt(j).getMol2();
            String line = mol2.elementAt(1);
            if( last != null )
            {
                if( last.equals("27067-96-5") )
                {
                    // do nothing there is a duplicate
                }
                else if( line.equals(last) )
                {
                    logger.debug("Last: " + last);
                    logger.debug("Current: " + line);
                    fail("Identical mol2 fields for different molecules");
                }
            }
            last = line;
        }

        Molecule mol = db.elementAt(0);
        assertEquals(mol.getName(),"1021-65-4");
        assertEquals(mol.getNumber(),1);
        assertNotNull(mol);
        mol = db.elementAt(db.size()-1);
        assertEquals(mol.getName(),"114-07-8");
        assertEquals(mol.getNumber(),221);
        assertEquals(mol.getDatabase(),db);
    }

}
