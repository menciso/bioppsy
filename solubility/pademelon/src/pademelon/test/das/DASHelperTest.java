package pademelon.test.das;

import java.io.InputStream;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import pademelon.das.DASMDBean;
import pademelon.das.DASHelper;
import pademelon.lib.Database;
import pademelon.lib.Molecule;
import pademelon.lib.MoleculeCollection;
import pademelon.test.AllTests;
import junit.framework.TestCase;
import java.util.Vector;

/**
 * Will not work unless in client/server mode as tests
 * are running outside jboss jvm where database server
 * runs.
 * 
 * @author eduthie
 *
 */
public class DASHelperTest extends TestCase
{
    private static Logger logger = Logger.getLogger(DASHelperTest.class);
    private String DATABASE_NAME = getClass().getName();
    
    private DASHelper das = null;

    public DASHelperTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    /**
     * Tests both saveDatabse() and getDatabase()
     *
     */
    public void testSaveGetDatabase()
    {
        saveTest();
    }
    
    public void testSearchForMolecules()
    {
        saveTest();
        Molecule query = new Molecule();
        query.setNumber(1);
        Database queryDatabase = new Database(DATABASE_NAME);
        query.setDatabase(queryDatabase);
        MoleculeCollection results = das.searchForMolecules(query);
        if( results.size() != 1 )
        {
            fail("More than one molecule returned searching by number");
        }
        Molecule mol = results.elementAt(0);
        assertEquals(1,mol.getNumber());
        assertEquals("1021-65-4",mol.getName());
        if( mol.getMol2().size() <= 0 )
        {
            fail("No mol2 data stored for molecule");
        }
    }
    
    private void saveTest()
    {
        Database db = new Database(DATABASE_NAME);
        InputStream is = AllTests.class.getResourceAsStream(DASTestHelper.FILENAME);
        assertNotNull(is);
        assertTrue(db.load(is));
        das = new DASHelper();
        das.saveDatabase(db);
        Database queryDatabase = new Database(DATABASE_NAME);
        Molecule query = new Molecule();
        query.setDatabase(queryDatabase);
        MoleculeCollection results = das.searchForMolecules(query);
        assertEquals(DASTestHelper.NUM_MOL,results.size());
        Molecule mol = results.elementAt(0);
        if( mol.getMol2().size() <= 0 )
        {
            fail("No mol2 data stored for molecule");
        }
    }

}
