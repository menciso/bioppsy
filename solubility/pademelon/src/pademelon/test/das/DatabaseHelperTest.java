package pademelon.test.das;

import junit.framework.TestCase;
import pademelon.das.DatabaseHelper;
import pademelon.test.AllTests;
import org.apache.log4j.PropertyConfigurator;
import com.db4o.ObjectContainer;

/**
 * Will not work unless in client/server mode as tests
 * are running outside jboss jvm where database server
 * runs.
 * 
 * @author eduthie
 *
 */
public class DatabaseHelperTest extends TestCase
{

    public static void main(String[] args)
    {
    }

    public DatabaseHelperTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    public void testAll()
    {
        DatabaseHelper dh = new DatabaseHelper();
        assertNotNull(dh);
        dh.close();
    }

}
