package pademelon.test.das;

import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Vector;
import junit.framework.TestCase;
import javax.ejb.CreateException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import pademelon.test.AllTests;
import pademelon.lib.Constants;
import pademelon.lib.Database;
import pademelon.lib.Molecule;
import pademelon.lib.MoleculeCollection;
import pademelon.das.DASHelper;
import pademelon.das.DASMDBean;
import pademelon.das.interfaces.DASSessionHome;

public class DASSessionBeanTest extends TestCase
{
    private static Logger logger = Logger.getLogger(DASSessionBeanTest.class);
    
    private String DATABASE_NAME = getClass().getName();

    public DASSessionBeanTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /*
     * Test method for 'pademelon.das.DASSessionBean.getDatabaseData(Database)'
     */
    public void testGetDatabaseData()
    {
        try
        {
            DASTestHelper helper = new DASTestHelper();
            if( !helper.saveDatabaseByMessage(DATABASE_NAME) )
            {
                fail("Failed to create database by message");
            }
            
            Context context = new InitialContext();
            Object object = context.lookup(Constants.DAS_EJB);
            if( object == null )
            {
                logger.error("Object not found");
                fail();
            }
            DASSessionHome home = (DASSessionHome) PortableRemoteObject.narrow(object,DASSessionHome.class);
            if( home == null )
            {
                logger.error("Retrieved object is null");
                fail();
            }
            pademelon.das.interfaces.DASSession bean = home.create();
            Database query = new Database(DATABASE_NAME);
            Database result = bean.getDatabaseData(query);
            assertNotNull(result);
            assertEquals(DATABASE_NAME, result.getName());
            Vector<Molecule> molecules = result.getMolecules();
            assertNotNull(molecules);
            assertEquals(DASTestHelper.NUM_MOL,molecules.size());
            Molecule mol = molecules.elementAt(0);
            assertNotNull(mol);
            assertEquals(helper.getDatabase().getMolecules().elementAt(0).getMol2(),mol.getMol2());
        }
        catch( NamingException ne)
        {
            ne.printStackTrace();
            fail(ne.getMessage());
        }
        catch (RemoteException re)
        {
            fail("Remote Exception: " + re.getMessage());
        }
        catch (CreateException ce)
        {
            fail("Create Exception: " + ce.getMessage());
        }
    }
    
    public void testSearchForMolecules()
    {   
        try
        {
            DASTestHelper helper = new DASTestHelper();
            if( !helper.saveDatabaseByMessage(DATABASE_NAME) )
            {
                fail("Failed to create database by message");
            }
            
            Context context = new InitialContext();
            Object object = context.lookup(Constants.DAS_EJB);
            if( object == null )
            {
                logger.error("Object not found");
                fail();
            }
            DASSessionHome home = (DASSessionHome) PortableRemoteObject.narrow(object,DASSessionHome.class);
            if( home == null )
            {
                logger.error("Retrieved object is null");
                fail();
            }
            pademelon.das.interfaces.DASSession bean = home.create();
            Molecule query = new Molecule();
            Database queryDatabase = new Database(DATABASE_NAME);
            query.setDatabase(queryDatabase);
            query.setNumber(1);
            MoleculeCollection results = bean.searchForMolecules(query);
            Molecule mol = results.elementAt(0);
            assertEquals(1,mol.getNumber());
            assertEquals("1021-65-4",mol.getName());
            if( mol.getMol2().size() <= 0 )
            {
                fail("No mol2 data stored for molecule");
            }
        }
        catch( NamingException ne)
        {
            ne.printStackTrace();
            fail(ne.getMessage());
        }
        catch (RemoteException re)
        {
            fail("Remote Exception: " + re.getMessage());
        }
        catch (CreateException ce)
        {
            fail("Create Exception: " + ce.getMessage());
        }
    }

}
