package pademelon.test.das;

import junit.framework.TestCase;
import pademelon.das.*;
import pademelon.lib.*;
import pademelon.test.AllTests;
import javax.ejb.CreateException;
import javax.jms.*;
import javax.naming.*;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.io.InputStream;
import javax.rmi.PortableRemoteObject;
import java.rmi.RemoteException;
import pademelon.das.interfaces.*;
import java.util.Vector;

public class DASMDBeanTest extends TestCase
{
    private static Logger logger = Logger.getLogger(DASMDBeanTest.class);
    private static String DATABASE_NAME = DASMDBeanTest.class.getName();
    
    public DASMDBeanTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator
                .configure(AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    public void testSaveDatabaseMessage()
    {
        try
        {
            DASTestHelper helper = new DASTestHelper();
            if( !helper.saveDatabaseByMessage(DATABASE_NAME) )
            {
                fail("Failed to create database by message");
            }
            Context context = new InitialContext();
            Object object = context.lookup(Constants.DAS_EJB);
            DASSessionHome home = (DASSessionHome) PortableRemoteObject.narrow(
                    object, DASSessionHome.class);
            pademelon.das.interfaces.DASSession bean = home.create();
            Database query = new Database(DATABASE_NAME);
            Database result = bean.getDatabaseData(query);
            assertNotNull(result);
            assertEquals(DATABASE_NAME, result.getName());
            Vector<Molecule> molecules = result.getMolecules();
            assertNotNull(molecules);
            assertEquals(DASTestHelper.NUM_MOL,molecules.size());
            Molecule mol = molecules.elementAt(0);
            assertNotNull(mol);
            assertEquals(helper.getDatabase().getMolecules().elementAt(0).getMol2(),mol.getMol2());
        }
        catch (NamingException e)
        {
            fail("JNDI lookup failed: " + e.getMessage());
        }
        catch (CreateException ce)
        {
            fail("Create Exception: " + ce.getMessage());
        }
        catch (RemoteException re)
        {
            fail("Remote Exception: " + re.getMessage());
        }
    }
}
