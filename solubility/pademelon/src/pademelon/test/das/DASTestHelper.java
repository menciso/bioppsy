package pademelon.test.das;

import java.io.InputStream;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import pademelon.lib.Constants;
import pademelon.lib.Database;
import pademelon.test.AllTests;
import org.apache.log4j.Logger;

public class DASTestHelper
{
    private static Logger logger = Logger.getLogger(DASTestHelper.class);
    
    public static String FILENAME = "drugs.mol2";
    public static int NUM_MOL = 221;
    
    private Database db = null;
    
    /**
     * Creates a database from the file given in databaseName and
     * submits it for creation by sending a message. Returns true
     * on success and false if it fails.
     */
    public boolean saveDatabaseByMessage(String databaseName)
    {
        db = new Database(databaseName);
        InputStream is = AllTests.class.getResourceAsStream(FILENAME);
        if( is == null )
        {
            logger.error("Failed to read mol2 file: " + FILENAME);
            return false;
        }
        if( !db.load(is) )
        {
            logger.error("Failed to parse molecules from mol2 file" + FILENAME);
            return false;
        }

        InitialContext context = null;
        ConnectionFactory connectionFactory = null;
        Connection connection = null;
        Session session = null;
        Destination destination = null;
        MessageProducer messageProducer = null;
        ObjectMessage message = null;

        try
        {
            // test sending a SaveDatabase
            context = new InitialContext();
            connectionFactory = (ConnectionFactory) context
                    .lookup(Constants.CONNECTION_FACTORY);
            destination = (Queue) context.lookup(Constants.DAS_QUEUE);
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            messageProducer = session.createProducer(destination);
            message = session.createObjectMessage();
            message.setStringProperty(Constants.COMMAND_NAME,
                    Constants.SAVE_DATABASE_COMMAND_DAS);
            message.setObject(db);
            messageProducer.send(message);
            return true;
        }
        catch (NamingException e)
        {
            logger.error("JNDI lookup failed: " + e.toString());
            logger.error(e.getMessage());
            return false;
        }
        catch (JMSException e)
        {
            logger.error("Exception occurred: " + e.toString());
            return false;
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (JMSException e)
                {
                }
            }
        }
    }
    
    /**
     * Returns a copy of the database created using 
     * saveDatabaseByMessage()
     */
    public Database getDatabase()
    {
        return db;
    }
}
