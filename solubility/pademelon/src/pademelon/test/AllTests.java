package pademelon.test;

import junit.framework.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import pademelon.test.das.*;
import pademelon.test.lib.*;
import pademelon.test.cs.*;
import pademelon.test.user.*;

/***************************************************************
 * TestSuite that runs all the sample tests
 ***************************************************************/
public class AllTests {

    static Logger logger = Logger.getLogger(AllTests.class);
    
    public static String FILENAME = "drugs.mol2";
    public static String LOG4J = "test.log4j";
    public static int NUM_MOL = 221;
    
    public static void main (String[] args) {

        PropertyConfigurator.configure(
            AllTests.class.getResource("test.log4j"));

        junit.textui.TestRunner.run(suite());
    }
    public static Test suite ( ) 
    {
        TestSuite suite= new TestSuite("pademelon unit tests");
        //suite.addTest(new TestSuite(DatabaseHelperTest.class));
        suite.addTest(new TestSuite(DASMDBeanTest.class));
        suite.addTest(new TestSuite(DatabaseTest.class));
        //suite.addTest(new TestSuite(DASHelperTest.class));
        suite.addTest(new TestSuite(CSHelperTest.class));
        suite.addTest(new TestSuite(DASSessionBeanTest.class));
        suite.addTest(new TestSuite(CSMDBeanTest.class));
        suite.addTest(new TestSuite(CSSessionBeanTest.class));
        suite.addTest(new TestSuite(MoleculeCollectionTest.class));
        suite.addTest(new TestSuite(UserHelperTest.class));
        suite.addTest(new TestSuite(CommandLineTest.class));
        return suite;
    }
}
