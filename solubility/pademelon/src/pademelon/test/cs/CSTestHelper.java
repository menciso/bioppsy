package pademelon.test.cs;

import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import pademelon.das.interfaces.DASSessionHome;
import pademelon.lib.Constants;
import pademelon.lib.Database;
import pademelon.lib.Molecule;
import pademelon.test.AllTests;

import org.apache.log4j.Logger;

public class CSTestHelper
{
    private static Logger logger = Logger.getLogger(CSTestHelper.class);
    private String FILENAME = "drugs.mol2";
    public int NUM_MOL = 221;
    public Database db;
    
    public boolean sendCreateDatabseMessage(String DATABASE_NAME)
    {
        db = new Database(DATABASE_NAME);
        InputStream is = AllTests.class.getResourceAsStream(FILENAME);
        if( is == null )
        {
            logger.error("Failed to open file: " + FILENAME);
            return false;
        }
        if( !db.load(is) )
        {
            logger.error("Failed to load file: " + FILENAME);
        }

        InitialContext context = null;
        ConnectionFactory connectionFactory = null;
        Connection connection = null;
        Session session = null;
        Destination destination = null;
        MessageProducer messageProducer = null;
        ObjectMessage message = null;

        try
        {
            // test sending a SaveDatabase
            context = new InitialContext();
            connectionFactory = (ConnectionFactory) context
                    .lookup(Constants.CONNECTION_FACTORY);
            destination = (Queue) context.lookup(Constants.CS_QUEUE);
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            messageProducer = session.createProducer(destination);
            message = session.createObjectMessage();
            message.setStringProperty(Constants.COMMAND_NAME,
                    Constants.SAVE_DATABASE_COMMAND_CS);
            message.setObject(db);
            logger.debug("Sending Message");
            messageProducer.send(message);

            Object object = context.lookup(Constants.DAS_EJB);
            DASSessionHome home = (DASSessionHome) PortableRemoteObject.narrow(
                    object, DASSessionHome.class);
            pademelon.das.interfaces.DASSession bean = home.create();
            Database query = new Database(DATABASE_NAME);
            Database result = bean.getDatabaseData(query);
            if (result == null)
            {
                logger.error("Failed to retrieve Database data");
                return false;
            }
            if( !DATABASE_NAME.equals(result.getName()) )
            {
                logger.error("Returned incorrect database: " + result.getName() + 
                        " expected: " + DATABASE_NAME);
                return false;
            }
            Vector<Molecule> molecules = result.getMolecules();
            if( NUM_MOL != molecules.size())
            {
                logger.error("Incorrect number of molecules retrieved: " +
                        molecules.size() + " expected: " + NUM_MOL);
                return false;
            }
            Molecule mol = molecules.elementAt(0);
            if( !db.getMolecules().elementAt(0).getMol2().equals(mol.getMol2()) )
            {
                logger.error("Incorrect mol2 file stored");
                return false;
            }
        }
        catch (NamingException e)
        {
            logger.error("JNDI lookup failed: " + e.toString());
            logger.error(e.getMessage());
            return false;
        }
        catch (JMSException e)
        {
            logger.error("Exception occurred: " + e.toString());
            return false;
        }
        catch (CreateException ce)
        {
            logger.error("Create Exception: " + ce.getMessage());
            return false;
        }
        catch (RemoteException re)
        {
            logger.error("Remote Exception: " + re.getMessage());
            return false;
        }
        finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (JMSException e)
                {
                }
            }
        }
        return true;
    }
}
