package pademelon.test.cs;

import java.io.InputStream;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import junit.framework.TestCase;
import pademelon.cs.CSHelper;
import pademelon.lib.Database;
import pademelon.lib.Molecule;
import pademelon.lib.MoleculeCollection;
import pademelon.lib.Constants;
import pademelon.test.AllTests;

/**
 * This test mainly tests the searchForMolecules() function
 *
 * @author eduthie
 */
public class CSHelperTest extends TestCase
{
    private static Logger logger = Logger.getLogger(CSHelperTest.class);
    
    private String DATABASE_NAME = getClass().getName();
    private String FILENAME = AllTests.FILENAME;
    private int NUM_MOL = AllTests.NUM_MOL;
    
    public CSHelperTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /*
     * Test method for 'pademelon.cs.CSHelper.saveDatabase(Database)'
     */
    public void testSaveDatabase()
    {
        Database db = new Database(DATABASE_NAME);
        InputStream is = AllTests.class.getResourceAsStream(FILENAME);
        assertNotNull(is);
        assertTrue(db.load(is));
        new CSHelper().saveDatabase(db);
    }
    
    public void testSearchForMolecules()
    {
        // create the database
        Database db = new Database(DATABASE_NAME);
        InputStream is = AllTests.class.getResourceAsStream(FILENAME);
        assertNotNull(is);
        assertTrue(db.load(is));
        new CSHelper().saveDatabase(db);
        
        // retrieve the database
        Database queryDatabase = new Database(DATABASE_NAME);
        Molecule queryMol = new Molecule();
        queryMol.setDatabase(queryDatabase);
        MoleculeCollection results = new CSHelper().searchForMolecules(queryMol);
        assertNotNull(results);
        assertEquals(NUM_MOL,results.size());
    }

}
