package pademelon.test.cs;

import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Vector;
import javax.ejb.CreateException;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import pademelon.das.interfaces.DASSessionHome;
import pademelon.lib.Constants;
import pademelon.lib.Database;
import pademelon.lib.Molecule;
import pademelon.test.AllTests;
import junit.framework.TestCase;

public class CSMDBeanTest extends TestCase
{
    private static Logger logger = Logger.getLogger(CSMDBeanTest.class);
    
    String DATABASE_NAME = getClass().getName();
    
    public CSMDBeanTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    public void testSaveDatabaseMessage()
    {
        try
        {
            CSTestHelper helper = new CSTestHelper();
            if( !(helper.sendCreateDatabseMessage(DATABASE_NAME)))
            {
                fail("Failed to create database: " + DATABASE_NAME);
            }

            InitialContext context = new InitialContext();
            Object object = context.lookup(Constants.DAS_EJB);
            DASSessionHome home = (DASSessionHome) PortableRemoteObject.narrow(
                    object, DASSessionHome.class);
            pademelon.das.interfaces.DASSession bean = home.create();
            Database query = new Database(DATABASE_NAME);
            Database result = bean.getDatabaseData(query);
            if (result == null)
            {
                fail("Failed to retrieve Database data");
            }
            assertEquals(DATABASE_NAME, result.getName());
            Vector<Molecule> molecules = result.getMolecules();
            assertEquals(helper.NUM_MOL,molecules.size());
            Molecule mol = molecules.elementAt(0);
            assertEquals(helper.db.getMolecules().elementAt(0).getMol2(),mol.getMol2());
        }
        catch (NamingException e)
        {
            logger.error("JNDI lookup failed: " + e.toString());
            logger.error(e.getMessage());
            fail();
        }
        catch (CreateException ce)
        {
            logger.error("Create Exception: " + ce.getMessage());
            fail();
        }
        catch (RemoteException re)
        {
            logger.error("Remote Exception: " + re.getMessage());
            fail();
        }
    }

}
