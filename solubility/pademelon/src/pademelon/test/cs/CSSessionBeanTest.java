package pademelon.test.cs;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import pademelon.test.AllTests;
import junit.framework.TestCase;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import pademelon.cs.interfaces.*;
import pademelon.lib.*;
import javax.ejb.CreateException;
import java.rmi.RemoteException;

public class CSSessionBeanTest extends TestCase
{
    private static Logger logger = Logger.getLogger(CSSessionBeanTest.class);
    
    private String DATABASE_NAME = getClass().getName();
    
    public CSSessionBeanTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /*
     * Test method for 'pademelon.cs.CSSessionBean.searchForMolecules(Molecule)'
     */
    public void testSearchForMolecules()
    {
        try
        {
            CSTestHelper helper = new CSTestHelper();
            assertTrue(helper.sendCreateDatabseMessage(DATABASE_NAME));
            
            InitialContext initial = new InitialContext();
            Object obj = initial.lookup(Constants.CS_EJB);
            CSSessionHome home = (CSSessionHome) PortableRemoteObject.narrow(obj,CSSessionHome.class);
            CSSession bean = home.create();
            Molecule query = new Molecule();
            Database db = new Database(DATABASE_NAME);
            query.setName("1021-65-4");
            query.setDatabase(db);
            MoleculeCollection results = bean.searchForMolecules(query);
            assertNotNull(results);
            if( results.size() <= 0 )
            {
                fail("No results retrieved");
            }
            Molecule result = results.elementAt(0);
            assertNotNull(result);
            assertEquals(1,result.getNumber());
        }
        catch (NamingException ne)
        {
            fail(ne.getMessage());
        }
        catch( CreateException ce )
        {
            fail(ce.getMessage());
        }
        catch( RemoteException re )
        {
            fail(re.getMessage());
        }
    }

}
