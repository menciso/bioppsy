package pademelon.test.user;

import java.io.*;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import pademelon.lib.Database;
import pademelon.test.AllTests;
import pademelon.user.CommandLine;
import junit.framework.TestCase;

public class CommandLineTest extends TestCase
{
    private static Logger logger = Logger.getLogger(CommandLineTest.class);
    
    public CommandLineTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    /*  
     * tests creating and searching for a database
     */
    public void testCreate()
    {
        String filename = AllTests.FILENAME;
        String tempFilename = "temp.mol2";
        String DATABASE = getClass().getName();
        
        try
        {   
            // Initiate a Database
            InputStream in = AllTests.class.getResourceAsStream(filename);
            Database db = new Database(DATABASE);
            assertNotNull(db);
            assertTrue(db.load(in) );
            in.close();
            assertEquals(AllTests.NUM_MOL,db.size());
            
            // Create the correct file
            FileOutputStream fos = new FileOutputStream(tempFilename);
            assertNotNull(db.writeToStream(fos));
            
            // create the database
            String[] args = {"-C","-d",DATABASE,"-f",tempFilename};
            CommandLine.mainMethod(args);
            
            // delete the temp file
            File file = new File(tempFilename);
            if( file.exists() )
            {
                file.delete();
            }
            
            // try retrieving the database
            String[] args2 = {"-S","-d",DATABASE,"-F",tempFilename};
            CommandLine.mainMethod(args2);
            
            // Check correct number of mols were written
            BufferedReader input = new BufferedReader(new FileReader(tempFilename));
            String line;
            int count = 0;
            while( (line = input.readLine()) != null )
            {
                if( line.trim().startsWith("@<TRIPOS>MOLECULE") )
                {
                    ++count;
                }
            }
            logger.debug("Number of molecules written to file: " + count);
            assertEquals(AllTests.NUM_MOL,count);
            
            // Initiate a Database with retrieved data
            FileInputStream fis = new FileInputStream(tempFilename);
            assertNotNull(fis);
            Database retrieved = new Database(DATABASE);
            assertNotNull(db);
            assertTrue(retrieved.load(fis) );
            fis.close();
            
            // check they are the same
            assertEquals(db.size(),retrieved.size());
            
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
        finally
        {
            // remove the temporary file
            File file = new File(tempFilename);
            if( file.exists() )
            {
                file.delete();
            }
        }
    }

}
