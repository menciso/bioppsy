package pademelon.test.user;

import java.io.InputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import pademelon.lib.*;
import pademelon.test.AllTests;
import pademelon.user.UserHelper;
import junit.framework.TestCase;

public class UserHelperTest extends TestCase
{
    private static Logger logger = Logger.getLogger(UserHelperTest.class);
    
    public UserHelperTest(String arg0)
    {
        super(arg0);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        PropertyConfigurator.configure(
                AllTests.class.getResource("test.log4j"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    public void testCreateDatabaseSearchForMolecules()
    {
        try
        {
            // Initiate a Database
            String filename = AllTests.FILENAME;
            String DATABASE = getClass().getName();
            
            InputStream in = AllTests.class.getResourceAsStream(filename);
            Database db = new Database(DATABASE);
            assertNotNull(db);
            assertTrue(db.load(in) );
            in.close();
            
            // create the database
            UserHelper helper = new UserHelper();
            assertNotNull(helper.createDatabase(db));
            Molecule mol = new Molecule();
            Database query = new Database(DATABASE);
            mol.setDatabase(query);
            MoleculeCollection result = helper.searchForMolecules(mol);
            assertNotNull(result);
            assertEquals(db.size(),result.size());
        }
        catch( IOException ioe )
        {
            fail(ioe.getMessage());
        }
    }

}
