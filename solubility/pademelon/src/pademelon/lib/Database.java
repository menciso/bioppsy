package pademelon.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.InputStream;
import java.util.Vector;

import org.apache.log4j.Logger;

public class Database extends MoleculeCollection implements Serializable
{
    private String name;
    private static String MOLECULE_KEY = "@<TRIPOS>MOLECULE";
    private static String COMMENT_KEY = "#";
    private static int NAME_LINE = 2;
    
    public Database()
    {
        super();
    }
    
    public Database(String name)
    {
        super();
        this.name = name;   
    }
    
    public String getName()
    {
        return name;
    }
    
    /**
     * Populates this MoleculeCollectionTest with all the Molecules in
     * the given InputStream. The InputStream must be in the mol2 format.
     * Returns true on success, false on failure.
     */
    public boolean load(InputStream input)
    {
        InputStreamReader isr = new InputStreamReader(input);
        BufferedReader br = new BufferedReader(isr);
        String line = null;
        Vector<String> lines = new Vector<String>();
        boolean haveMolecule = false;
        int moleculeNumber = 1;
        try
        {
            while( (line = br.readLine()) != null )
            {
                line = line.trim();
                
                // first look for the start of a molecule
                if( line.startsWith(MOLECULE_KEY) )
                {
                    // if we have a molecule already, process it
                    if( haveMolecule == true )
                    {
                        addMolecule(lines,moleculeNumber);
                        lines = new Vector<String>();
                        lines.add(line);
                        ++moleculeNumber;
                    }
                    else
                    {
                        lines = new Vector<String>();
                        lines.add(line);
                        haveMolecule = true;
                    }
                }
                // if there is a comment, skip it
                else if( line.startsWith(COMMENT_KEY))
                {
                    continue;
                }
                // if we are in a molecule
                else if( haveMolecule )
                {
                    // fill the lines with the data until we hit a comment
                    // or another molecule
                    lines.add(line);
                }
            }
            
            // if there is a last molecule we process it
            // since we have been processing molecules every time
            // we hit the next one
            if( haveMolecule )
            {
                addMolecule(lines,moleculeNumber);
            }
            
            return true;
        }
        catch( IOException ioe )
        {
            Logger.getLogger(getClass()).error(ioe.getMessage());
            return false;
        }
        
    }
    
    /**
     * Adds a molecule with the given List of String as lines in it's mol2
     * file and with the given number as the index of the molecule.
     */
    public void addMolecule(Vector<String> lines, int number)
    {
        if( (lines == null) || (lines.size() == 0) )
        {
            Logger.getLogger(getClass()).warn("Attempting to add an empty molecule");
            return;
        }
        String name = lines.elementAt(NAME_LINE-1);
        Molecule molecule = new Molecule(number,this,name,name,lines);
        super.add(molecule);
    }
}
