package pademelon.lib;

import java.io.Serializable;
import java.util.Vector;

/**
 * A Molecule is uniquely determined by its number and database
 * 
 * @author eduthie
 */
public class Molecule implements Serializable
{
    private int number;
    private Database database = null;
    private String name = null ;
    private String supCode = null;
    private Vector<String> mol2 = null;
    
    public Molecule()
    {
    }
    
    public Molecule(int number,Database database, String name, String supCode, Vector<String> mol2)
    {
        this.number = number;
        this.database = database;
        this.name = name;
        this.supCode = supCode;
        this.mol2 = mol2;
    }
    
    public int getNumber()
    {
        return number;
    }
    
    public Database getDatabase()
    {
        return database;
    }
    
    public String getName()
    {
        return name;
    }
    
    public Vector<String> getMol2()
    {
        return mol2;
    }
    
    public String getSupCode()
    {
        return supCode;
    }
    
    public void setNumber(int number)
    {
        this.number = number;
    }
    
    public void setDatabase(Database database)
    {
        this.database = database;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public void setSupCode(String supCode)
    {
        this.supCode = supCode;
    }
}
