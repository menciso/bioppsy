package pademelon.lib;

import java.io.Serializable;
import java.util.Vector;
import java.io.*;
import org.apache.log4j.Logger;

public class MoleculeCollection implements Serializable
{
    private  Vector<Molecule> molecules = null;
    
    public MoleculeCollection()
    {
        molecules = new Vector<Molecule>();
    }
    
    public Vector<Molecule> getMolecules()
    {
        return molecules;
    }
    
    public void add(Molecule molecule)
    {
        molecules.add(molecule);
    }
    
    public int size()
    {
        return molecules.size();
    }
    
    public Molecule elementAt(int index)
    {
        return molecules.elementAt(index);
    }
    
    /**
     * Writes all the mol2 data in the MoleculeCollectionTest
     * out to the given OutputStream. Returns true on success
     * and false on failure. Can throw an IOException if the
     * OutputStream cannot be closed.
     */
    public boolean writeToStream(OutputStream os)
        throws IOException
    {
        PrintWriter pw = new PrintWriter(os);
        for( int i=0; i < molecules.size(); ++i )
        {
            Molecule molecule = molecules.elementAt(i);
            Vector<String> lines = molecule.getMol2();
            for( int j=0; j < lines.size(); ++j )
            {
                pw.println(lines.elementAt(j));
            }
        }
        pw.close();
        os.close();
        return true;
    }
}
