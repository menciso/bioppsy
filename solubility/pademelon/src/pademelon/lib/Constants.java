package pademelon.lib;

/**
 * Shared constants between modules
 *
 */
public class Constants
{
    public static final String DAS_EJB = "ejb/DASSession";
    public static final String DAS_WEB = "DASHelper";
    public static final String DAS_QUEUE = "queue/DASMDBean";
    public static final String CS_EJB = "ejb/CSSession";
    public static final String CS_QUEUE = "queue/CSMDBean";
    public static final String CONNECTION_FACTORY = "ConnectionFactory";
    public static final String COMMAND_NAME = "COMMAND";
    public static final String SAVE_DATABASE_COMMAND_DAS = "SAVE_DATABASE";
    public static final String SAVE_DATABASE_COMMAND_CS = "SAVE_DATABASE";
}
