package property.user;

//property
import property.Database;
import property.Constants;
import property.MolSet;
import property.algorithm.Algorithm;
import property.HouAlgorithm;

// gui
import javax.swing.*; 
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

//java
import java.io.File;
import java.util.Vector;

public class AlgRenameDialog extends JDialog
{
	private Database db=null;
	
	private ObjectListener ol = null;
    
    private JTextField newnameText = null;

    private JComboBox oldnameComboBox = null;


    private JButton submit = null;;

    static Logger logger = Logger.getLogger(AlgCreateDialog.class);

    public AlgRenameDialog(JFrame parent,Database db)
    {
        super(parent, "Rename Algorithm");
        this.db=db;
        JFrame.setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        createAndShowGUI();
        pack();
        position();
    }

    public void position()
    {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension size = getSize();
        int x = (int) ((dim.getWidth()/2)-(size.getWidth()/2));
        int y = (int) ((dim.getHeight()/2)-(size.getHeight()/2));
        setLocation(x,y);
    }

    public void setObjectListener(ObjectListener ol)
    {
        this.ol = ol;
    }

    public void start()
    {
        setVisible(true);
    }

    public void createAndShowGUI()
    {
        Vector names = null;

        names = Algorithm.getNames(db);
    	
        getContentPane().setLayout(
            new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

     //   getContentPane().add(Box.createRigidArea(new Dimension(5,10)));
        
        JPanel oldnamePane=new JPanel();        
        oldnamePane.setLayout(
                new BoxLayout(oldnamePane, BoxLayout.Y_AXIS));

        JLabel oldnameLabel = new JLabel("Please select the Algorithm to rename");
        //oldnameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        oldnamePane.add(oldnameLabel);
        oldnameComboBox = new JComboBox(names);
        //oldnameComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        oldnamePane.add(oldnameComboBox);
        getContentPane().add(oldnamePane);
        
        
        //getContentPane().add(Box.createRigidArea(new Dimension(5,10)));
        
        JPanel newnamePane=new JPanel();        
        newnamePane.setLayout(
                new BoxLayout(newnamePane, BoxLayout.Y_AXIS));
        JLabel newnameLabel = new JLabel("Please enter the new name");
       // newnameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        newnamePane.add(newnameLabel);
        newnameText = new JTextField(10);
        newnamePane.add(newnameText);
        getContentPane().add(newnamePane);
        
        
       // getContentPane().add(Box.createRigidArea(new Dimension(5,10)));

        
        JPanel buttonPane=new JPanel();        
        buttonPane.setLayout(
                new BoxLayout(buttonPane, BoxLayout.X_AXIS));
        
        submit = new JButton("OK");
        submit.setAlignmentX(Component.CENTER_ALIGNMENT);
        submit.addActionListener(new OkActionListener());
        buttonPane.add(submit);

        	JButton cancel = new JButton("Cancel");
        	cancel.setAlignmentY(Component.CENTER_ALIGNMENT);
        	cancel.addActionListener(new CancelActionListener());
        	buttonPane.add(cancel);
        	getContentPane().add(buttonPane);
        	getContentPane().add(Box.createRigidArea(new Dimension(5,10)));
        
        validate();

        }    

    public class OkActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
             ok();
        }
    }
    
    public class CancelActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
             cancel();
        }
    }

    public void cancel()
    {
    		dispose();
    }
    
    public void ok()
    {
        if( !parseInput() )
        {
            return;
        }

        dispose();
    }

    public boolean parseInput()
    {
        String newname = newnameText.getText();
        
        if( (newname == null) ||
            (newname.trim().length() == 0) )
        {
            JOptionPane.showMessageDialog(
                this,
                "Please enter a name",
                "Error", 
                JOptionPane.ERROR_MESSAGE );
            return false;
        }

        String oldname = (String) oldnameComboBox.getSelectedItem();
        	
        return Algorithm.renameAlgorithm(oldname, newname, db);
    }
}
