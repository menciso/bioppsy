# --- UCSF Chimera Copyright ---
# Copyright (c) 2000 Regents of the University of California.
# All rights reserved.  This software provided pursuant to a
# license agreement containing restrictions on its disclosure,
# duplication and use.  This notice must be embedded in or
# attached to all copies, including partial copies, of the
# software or any revisions or derivations thereof.
# --- UCSF Chimera Copyright ---
#
# $Id: AddSeqDialog.py 27358 2009-04-21 00:32:47Z pett $

import chimera
from chimera.baseDialog import ModelessDialog
from chimera import replyobj, UserError
from MAViewer import ADDDEL_SEQS

class FetchAnnotationsDialog(ModelessDialog):
	"""Fetch (UniProt) annotations"""

	buttons = ("OK", "Apply", "Close")
	default = "OK"
	title = "Get UniProt Info"
	help = "ContributedSoftware/multalignviewer/multalignviewer.html#unifetch"
	
	def __init__(self, mav, *args, **kw):
		self.mav = mav
		ModelessDialog.__init__(self, *args, **kw)

	def fillInUI(self, parent):
		import Pmw, Tkinter
		row = 0
		self.menu = Pmw.OptionMenu(parent, labelpos='w',
			label_text="Fetch UniProt annotations for",
			items=[s.name for s in self.mav.seqs], command=self._seqCB)
		self.menu.grid(row=row, column=0, sticky='w')
		row += 1
		self.handlerID = self.mav.triggers.addHandler(ADDDEL_SEQS,
						self._seqsChangeCB, None)

		f = Tkinter.LabelFrame(parent, text="Identify UniProt Entry")
		f.grid(row=row, column=0, sticky='nsew')
		row += 1

		self.methodVar = Tkinter.StringVar(parent)

		uniprotf = Tkinter.Frame(f)
		uniprotf.grid(row=0, column=0, sticky='w')
		Tkinter.Radiobutton(uniprotf, variable=self.methodVar, value="uniprot",
			text="from UniProt ID:").grid(row=0, column=0)
		self.uniprotEntry = Pmw.EntryField(uniprotf, entry_width=15,
			modifiedcommand=lambda v=self.methodVar: v.set("uniprot"))
		self.uniprotEntry.grid(row=0, column=1)

		pdbf = Tkinter.Frame(f)
		pdbf.grid(row=1, column=0, sticky='w')
		Tkinter.Radiobutton(pdbf, variable=self.methodVar, value="pdb",
			text="from PDB code:").grid(row=0, column=0)
		self.pdbEntry = Pmw.EntryField(pdbf, entry_width=4, command=self.OK,
			modifiedcommand=lambda v=self.methodVar: v.set("pdb"))
		self.pdbEntry.grid(row=0, column=1)
		self.chainEntry = Pmw.EntryField(pdbf, labelpos='w', entry_width=1,
			label_text="and chain ID:")
		self.chainEntry.grid(row=0, column=2)

		blastf = Tkinter.Frame(f)
		blastf.grid(row=2, column=0, sticky='w')
		Tkinter.Radiobutton(blastf, variable=self.methodVar, value="blast", text=
			"by Blast search of UniProt (may take minutes)").grid(row=0, column=0)

		f = Tkinter.Frame(parent)
		f.grid(row=row, column=0)
		row += 1
		self.annotateVar = Tkinter.IntVar(parent)
		self.annotateVar.set(True)
		Tkinter.Checkbutton(f, text="Annotate sequence", variable=self.annotateVar
			).grid(row=0, column=0, sticky='w')
		self.showPageVar = Tkinter.IntVar(parent)
		self.showPageVar.set(False)
		Tkinter.Checkbutton(f, text="Show UniProt page(s)", variable=self.showPageVar
			).grid(row=1, column=0, sticky='w')

		self.ignoreCachesVar = Tkinter.IntVar(parent)
		self.ignoreCachesVar.set(False)
		cacheBut = Tkinter.Checkbutton(parent, text="Ignore any cached data",
			variable=self.ignoreCachesVar)
		from CGLtk.Font import shrinkFont
		shrinkFont(cacheBut)
		cacheBut.grid(row=row, column=0)
		row += 1

		self.menu.invoke()

	def destroy(self):
		self.mav.triggers.deleteHandler(ADDDEL_SEQS, self.handlerID)
		self.mav = None
		ModelessDialog.destroy(self)

	def Apply(self):
		import Pmw
		index = self.menu.index(Pmw.SELECT)
		seq = self.mav.seqs[index]
		method = self.methodVar.get()
		if method == "pdb":
			pdbID = self.pdbEntry.getvalue().strip()
			if len(pdbID) != 4:
				self.enter()
				raise UserError("PDB ID code must be exactly 4 characters long!")
			pdbID = pdbID.upper()
			chainID = self.chainEntry.getvalue().strip()
			if not chainID:
				self.enter()
				raise UserError("No chains ID supplied!")
			if len(chainID) != 1 or not chainID.isalnum():
				self.enter()
				raise UserError("Chain ID must be 1 letter or digit!")
			methodInfo = (pdbID, chainID)
		elif method == "uniprot":
			uniprotID = self.uniprotEntry.getvalue()
			if not uniprotID:
				self.enter()
				raise UserError("No UniProt ID supplied!")
			methodInfo = uniprotID
		else: # blast
			methodInfo = None
		self.mav.uniprotInfo(seq, method, methodInfo,
			annotate=self.annotateVar.get(), showPage=self.showPageVar.get(),
			ignoreCache=self.ignoreCachesVar.get())

	def _seqCB(self, *args):
		if self.methodVar.get() != "uniprot":
			self.methodVar.set("blast")
		import Pmw
		seq = self.mav.seqs[self.menu.index(Pmw.SELECT)]
		if len(getattr(seq, 'matchMaps', {})) == 1:
			mseq = seq.matchMaps.values()[0]['mseq']
			from SeqAnnotations import chain2pdbID
			pdbID = chain2pdbID(mseq)
			if pdbID:
				self.methodVar.set("pdb")
				self.pdbEntry.setvalue(pdbID)
				self.chainEntry.setvalue(mseq.chainID)

	def _seqsChangeCB(self, trigName, d1, d2):
		curItem = self.menu.getvalue()
		items = [s.name for s in self.mav.seqs]
		if curItem not in items:
			curItem=None
		self.menu.setitems(items, index=curItem)

