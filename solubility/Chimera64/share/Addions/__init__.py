from chimera import replyobj

ValidIontypes = ("Cl-", "Cs+", "K+",  "Li+", "MG2", "Na+", "Rb+" )

def writeLeaprc(tempDir, iontype, numion, leaprc ) :
	import os
	f = open( leaprc, 'w' )

	f.write( "source leaprc.ff03.r1\n" )
	f.write( "tmp = loadmol2 " + os.path.join(tempDir, "sleap.in.mol2\n") )
      
        if numion=="neutralize":
	    f.write( "addions tmp " + iontype + " 0\n" )
        else:
            f.write( "addions tmp " + iontype + " " + numion + "\n" )

	f.write( "savemol2 tmp " + os.path.join(tempDir, "sleap.out.mol2\n") )
	f.write( "quit\n" )

def is_solvent(r):
    for a in r.atoms:
        for n in a.neighbors:
            if n.residue !=r:
                return False

    return True

def get_solute_nresd(m):

    i=len(m.residues)-1

    while i>=0 and is_solvent(m.residues[i]):
        i -= 1

    return i+1
    


def initiateAddions(models, iontype, numion, status):
	import os
	import chimera
	from chimera import replyobj
	from chimera.molEdit import addAtom
	from WriteMol2 import writeMol2
	from tempfile import mkdtemp

	for m in models:
		if m.__destroyed__:
			continue

		noGaff = [a for a in m.atoms if not hasattr(a, 'gaffType')]
		if noGaff:
			from WritePrmtop import noGaffComplain
			noGaffComplain(noGaff, "Add Ions")
			continue

		tempDir = mkdtemp()
		def _clean():
			for fn in os.listdir(tempDir):
				os.unlink(os.path.join(tempDir, fn))
			try:
				os.rmdir(tempDir)
			except WindowsError, v:
				# ignore inexplicable "sharing violation" errors
				# ("file in use by another process")
				if v.winerror != 32:
					raise

		sleapIn = os.path.join(tempDir, "sleap.in.mol2")
		sleapOut= os.path.join(tempDir, "sleap.out.mol2")
		writeMol2([m], sleapIn, status=status, gaffType=True, relModel=m,
			temporary=True)

		leaprc = os.path.join(tempDir, "solvate.cmd")
		writeLeaprc(tempDir, iontype, numion, leaprc)

		from AmberInfo import amberHome, amberBin
		command = [os.path.join(amberBin, "sleap"), "-f", leaprc]

		if status:
			status("Running sleap" )
		from subprocess import Popen, STDOUT, PIPE
		# For some reason on Windows, if shell==False then antechamber
		# cannot run bondtype via system().
		import sys
		if sys.platform == "win32":
			shell = True
		else:
			shell = False
		replyobj.info("Running sleap command: %s\n" % " ".join(command))
		import os
		os.environ["AMBERHOME"]=amberHome
		sleapMessages = Popen(command, stdin=PIPE, stdout=PIPE, stderr=STDOUT,
				cwd=tempDir, shell=shell, bufsize=1).stdout
		while True:
			line = sleapMessages.readline()
			if not line:
				break
			replyobj.status("(addions) %s" % line, log=True)
		if not os.path.exists(sleapOut):
			_clean()
			from chimera import NonChimeraError
			raise NonChimeraError("Failure running sleap \n"
				"Check reply log for details\n")
		if status:
			status("Reading sleap output")
		from chimera import Mol2io, defaultMol2ioHelper
		mol2io = Mol2io(defaultMol2ioHelper)
		mols = mol2io.readMol2file(sleapOut)
		if not mol2io.ok():
			_clean()
			raise IOError(mol2io.error())
		if not mols:
			_clean()
			raise RuntimeError("No molecules in sleap output")

		assert len(mols)==1

		outm = mols[0]
		solute_nresd = get_solute_nresd(m)


		inAtoms = m.atoms
		outAtoms = outm.atoms
		status("Correlating sleap output with Chimera structure")
		# sort input atoms into order they were written to mol2 file...
		from WriteMol2 import writeMol2Sort
		inAtoms.sort(lambda a1, a2, ri={}: writeMol2Sort(a1, a2, resIndices=ri))
		# sort output atoms into order they were read from mol2 file...
		serialSort = lambda a1, a2: cmp(a1.coordIndex, a2.coordIndex)
		outAtoms.sort(serialSort)

		state = "before ions"
		oi = 0
		preserveOut = []
		resTypeInfo = {}
		def findRtype(rname):
			rend = len(rname)
			while rend > 1 and rname[rend-1].isdigit():
				rend -= 1
			if rend < len(rname) and rname[rend-1] == '-':
				# possible for negative sequence number
				# but check possible negative ion...
				from chimera.elements import halides
				if rname[:rend-1].capitalize() not in [e.name for e in halides]:
					rend -= 1
			return rname[:rend], rend
		for inAtom in inAtoms:
			try:
				outAtom = outAtoms[oi]
			except IndexError:
				if state == "water":
					# adding ions may have deleted waters
					preserveOut += [None] * (len(inAtoms) - len(preserveOut))
					break
				raise
			rtypes = []
			for rname in (inAtom.residue.type, outAtom.residue.type):
				rtype, rend = findRtype(rname)
				rtypes.append(rtype)
			if rend < len(rname):
				resTypeInfo[outAtom] = (rtypes[-1], int(rname[rend:]))
			same = inAtom.name == outAtom.name and rtypes[0] == rtypes[1]
			if state == "before ions":
				if same:
					preserveOut.append(outAtom)
					oi += 1
					continue
				state = "ions"
			if state == "ions":
				while outAtom.name[-1] in "+-":
					oi += 1
					outAtom = outAtoms[oi]
				state = "water"
				rtypes[1] = findRtype(outAtom.residue.type)[0]
				same = inAtom.name == outAtom.name and rtypes[0] == rtypes[1]
			if state == "water":
				if rtypes[0] in ("HOH", "WAT"):
					if same:
						oi += 1
					preserveOut.append(None)
					continue
				state = "hydrogens"
			if state == "hydrogens":
				if same:
					if rtypes[1] in ("HOH", "WAT"):
						preserveOut.append(None)
					else:
						preserveOut.append(outAtom)
					oi += 1
					continue
				if rtypes[0] == "HOH":
					preserveOut.append(None)
					continue
				raise ValueError("Cannot match sleap output with Chimera structure")
		assert(len(inAtoms) == len(preserveOut))

		# sleap repositions solute...
		if status:
			status("Translating %d atoms" % len(inAtoms))
		for inA, outA in zip(inAtoms, preserveOut):
			if outA == None:
				continue
			inA.setCoord(outA.coord())

		if status:
			status( "Deleting old solvents" )
		while len(m.residues) > solute_nresd:
			m.deleteResidue( m.residues[solute_nresd] )

		final = outm.residues[-1]
		for r in outm.residues[solute_nresd:]:
			solvIonNum = r.id.position - solute_nresd
			if status and solvIonNum % 1000 == 0:
				status("Creating ion/solvent residue %d " % solvIonNum)

			atomMap = {}
			if r.atoms[0] in resTypeInfo:
				rtype, pos = resTypeInfo[r.atoms[0]]
				nr = m.newResidue(rtype, ' ', pos, ' ')
			else:
				nr = m.newResidue(r.type, ' ', 1, ' ')
			for a in r.atoms:
				na = addAtom(a.name, a.element, nr, a.coord(),
							serialNumber=a.serialNumber)
				na.charge = a.charge
				na.gaffType = a.mol2type

				if len(a.neighbors)==0:
					na.drawMode = chimera.Atom.Sphere

				atomMap[a] = na

				if a.name[0:2]=="Br": na.element = 35
				elif a.name[0:2]=="Cl": na.element = 17
				elif a.name[0:2]=="Cs": na.element = 47
				elif a.name[0:2]=="Mg": na.element = 12
				elif a.name[0:2]=="Na": na.element = 11
				elif a.name[0:2]=="Rb": na.element = 48
				elif a.name[0]=='F': na.element = 9
				elif a.name[0]=='I': na.element = 53
				elif a.name[0]=='K': na.element = 19
				elif a.name[0]=="H": na.element = 1
				elif a.name[0]=="C": na.element = 6
				elif a.name[0]=="N": na.element = 7
				elif a.name[0]=="O": na.element = 8
				elif a.name[0]=="P": na.element = 15
				elif a.name[0]=="S": na.element = 16

			for a in r.atoms:
				na = atomMap[a]
				for n in a.neighbors:
					assert n.residue == r
					nn = atomMap[n]
					if nn in na.bondsMap:
						continue
					m.newBond(na, nn)

			if status and r == final:
				status("Created %d ion/solvent residues" % solvIonNum)
		_clean()

def test():
	import chimera
	models = chimera.openModels.list()
	iontype = "Cl-"
	numion = "4"
	status = chimera.replyobj.status
	initiateAddions(models, method, solvent, extent, center, status)
