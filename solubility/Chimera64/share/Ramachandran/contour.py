# Title and contour levels about the top500-angles files are obtained by
# examining the Kinemage files in kin/rama
ContourInfo = {
	"Alanine (no repet sec struct)":
		( "rama500-ala-nosec.data",	( "0.02", "0.002" ) ),
	"General case (no repet sec struct)":
		( "rama500-general-nosec.data",	( "0.02", "0.0005", ) ),
	"General case (not Gly, Pro or pre-Pro)":
		( "rama500-general.data",	( "0.02", "0.0005", ) ),
	"Glycine (sym, no repet sec struct)":
		( "rama500-gly-sym-nosec.data",	( "0.02", "0.002", ) ),
	"Glycine (sym)":
		( "rama500-gly-sym.data",	( "0.02", "0.002", ) ),
	"pre-Proline (not Gly or Pro)":
		( "rama500-prepro.data",	( "0.02", "0.002", ) ),
	"Proline":
		( "rama500-pro.data",		( "0.02", "0.002", ) ),
}
DefaultContour = "General case (not Gly, Pro or pre-Pro)"
