# -----------------------------------------------------------------------------
#
def open_apr(path):
  import AutoPack
  return AutoPack.open_autopack_results(path)

from chimera import fileInfo
fileInfo.register('AutoPack Results', open_apr, ['.apr'], ['apr'],
                  category = fileInfo.GENERIC3D)
