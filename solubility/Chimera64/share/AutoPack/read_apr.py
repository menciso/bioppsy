def read_apr_file(path):

    import sys
    f = open(path, 'r')
    lines = f.readlines()
    f.close()

    pieces = {}
    for line in lines:
        fields = line.replace('<','').replace('>','').split(',')
        if len(fields) < 23:
            continue
        t0,t1,t2 = tuple(float(x) for x in fields[0:3])
        m = tuple(float(x) for x in fields[3:19])
        r00,r01,r02 = m[0:3]
        r10,r11,r12 = m[4:7]
        r20,r21,r22 = m[8:11]
        tf = ((r00,r01,r02,t0),
              (r10,r11,r12,t1),
              (r20,r21,r22,t2))
        unk1 = fields[19]
        fname = fields[20]
        unk2 = fields[21]
        unk3 = fields[22]
        if fname in pieces:
            pieces[fname].append(tf)
        else:
            pieces[fname] = [tf]

    return pieces

def print_pieces(pieces):

    flist = pieces.keys()
    flist.sort()
    for fn in flist:
        print fn, len(pieces[fn])

def create_surfaces(apr_path, pieces):

    not_found = set()
    pdbs = []
    from os.path import dirname, basename, join, exists
    dir = dirname(apr_path)
    fnames = pieces.keys()
    fnames.sort()
    for fname in fnames:
        path = join(dir, fname)
        tflist = pieces[fname]
        pdb_path = path + '.pdb'
        if exists(pdb_path):
            pdbs.append((pdb_path, tflist))
        else:
            stl_path = path + '.stl'
            surf = create_surface_copies(stl_path, tflist)
            if not surf:
                not_found.add(fname)

    make_multiscale_models(pdbs)

    for fname in fnames:
        descrip = '%s, %d copies' % (fname, len(tflist))
        if fname in not_found:
            descrip += ', file not found'
        print descrip
        
    return []

def create_surface_copies(path, tflist):

    from os.path import exists
    if not exists(path) or len(tflist) == 0:
        return None

    from chimera import openModels
    surf = openModels.open(path)[0]
    p = surf.surfacePieces[0]
    va, ta = p.geometry
    na = p.normals
    p.color = color = random_color(surf.name)
    p.placement = tflist[0]
    p.save_in_session = True
    import Matrix
    for tf in tflist[1:]:
        pc = surf.addPiece(va, ta, color)
        pc.normals = na
        pc.placement = tf
        pc.save_in_session = True
    return surf

def make_multiscale_models(pdbs):

    if len(pdbs) == 0:
        return

    from chimera import openModels
    from Matrix import chimera_xform
    import MultiScale
    mm = MultiScale.multiscale_manager()
    for pdb_path, tflist in pdbs:
        pdb = openModels.open(pdb_path)[0]
        # Multiscale keeps the original pdb unmoved.
        # Make it align with surfaces.
        pdb.openState.localXform(chimera_xform(tflist[0]))
        mgroup = mm.molecule_multimer(pdb, tflist)
        multiscale_single_color([mgroup], random_color(pdb.name))

    MultiScale.show_multiscale_model_dialog()

def multiscale_single_color(mpieces, color):

    from MultiScale import find_pieces, Chain_Piece
    for cp in find_pieces(mpieces, Chain_Piece):
        cp.set_color(color)

def random_color(seed = None):

    import random
    if not seed is None:
        if isinstance(seed, basestring):
            # Make 64-bit machines and 32-bit produce the same 32-bit seed
            # by casting 64-bit hash values to signed 32-bit.
            seed = hash(str(seed)) % 2**32
        random.seed(seed)
    from random import uniform
    return (uniform(.2,1), uniform(.2,1), uniform(.2,1), 1)

def open_autopack_results(path):

    pieces = read_apr_file(path)
    create_surfaces(path, pieces)
    from chimera import viewer
    viewer.viewAll()
    return []

