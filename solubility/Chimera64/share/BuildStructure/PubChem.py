# --- UCSF Chimera Copyright ---
# Copyright (c) 2000-2006 Regents of the University of California.
# All rights reserved.  This software provided pursuant to a
# license agreement containing restrictions on its disclosure,
# duplication and use.  This notice must be embedded in or
# attached to all copies, including partial copies, of the
# software or any revisions or derivations thereof.
# --- UCSF Chimera Copyright ---
#
# $Id: PubChem.py 35113 2011-12-29 01:38:52Z pett $

import chimera

def openPubChem(pcID, resName="UNK", ignore_cache=False):
	m = pubChem2mol(pcID, resName=resName, ignore_cache=ignore_cache)
	chimera.openModels.add([m])
	return m

class InvalidPub3dID(ValueError):
	def __init__(self, pcID):
		if pcID.isdigit():
			msg = "Either no such PubChem ID (%s) or structure cannot be handled by Pub3D service (typically inorganic, metal-containing or unstable structures)" % pcID
		else:
			msg = "No such PubChem ID (%s)" % pcID
		ValueError.__init__(self, msg)

def pubChem2mol(pcID, resName="UNK", ignore_cache=False):
	from chimera import fetch
	path, headers = fetch.fetch_file(
		"http://cheminfov.informatics.indiana.edu/rest/db/pub3d/%s" % pcID,
		pcID, save_dir="Pub3D", save_name="%s.sdf" % pcID,
		ignore_cache=ignore_cache)
	from ReadSDF import readSDF
	result = readSDF(path, identifyAs="CID %s" % pcID)
	if not result:
		raise InvalidPub3dID(pcID)
	m = result[0]
	m.residues[0].type = resName
	return m
