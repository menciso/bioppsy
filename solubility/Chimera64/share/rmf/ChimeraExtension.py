# -----------------------------------------------------------------------------
# Register rmf file reader.
#
def open_rmf_file(path):
    import rmf
    return rmf.open_rmf(path)

from chimera import fileInfo, FileInfo
fileInfo.register('Rich Molecular Format', open_rmf_file,
			['.rmf', '.rmf2info'], ['rmf'],
			category=FileInfo.STRUCTURE)
