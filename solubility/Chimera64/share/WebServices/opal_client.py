#!/usr/local/bin/python2.5

from httplib import HTTPConnection
class _ProxyConnection(HTTPConnection):
	proxyHost = None
	proxyPort = None

	def __init__(self, host, port=None, **kw):
		if self.proxyHost:
			self.realHost = host
			self.realPort = port
			host = self.proxyHost
			port = self.proxyPort
		HTTPConnection.__init__(self, host, port, **kw)

	def putrequest(self, method, url, **kw):
		if self.proxyHost and "://" not in url:
			# Proxying and host not in URL
			prefix = "http://%s" % self.realHost
			if self.realPort:
				prefix += ":%s" % self.realPort
			url = prefix + url
		return HTTPConnection.putrequest(self, method, url, **kw)

class OpalService:

	DefaultOpalURL="http://webservices.rbvi.ucsf.edu/opal2/services/"

	def __init__(self, serviceName=None, opalURL=None, sessionData=None):
		if opalURL is None:
			opalURL = self.DefaultOpalURL
		try:
			self._setup(serviceName, opalURL, sessionData)
		except:
			self.dumpTraceback("connection setup")
			print """
Typically, if you get a TypeError, it's a problem on the remote server
and it should be fixed shortly.  If you get a different error or
get TypeError consistently for more than a day, please report the
problem using the Report a Bug... entry in the Help menu.  Please
include the traceback printed above as part of the problem description."""
			from chimera import NonChimeraError
			raise NonChimeraError("Web service appears "
						"to be down.  See Reply Log "
						"for more details.")

	def _setup(self, serviceName, opalURL, sessionData):
		from chimera import preferences
		from DBPuppet.waprefs import WEBACCESS_PREF, WA_PROXY
		from DBPuppet.waprefs import WA_PROXY_HOST, WA_PROXY_PORT
		kw = dict()
		if preferences.get(WEBACCESS_PREF, WA_PROXY):
			h = preferences.get(WEBACCESS_PREF, WA_PROXY_HOST)
			p = preferences.get(WEBACCESS_PREF, WA_PROXY_PORT)
			try:
				p = int(p)
				if p < 1 or p > 65535:
					raise ValueError("out of range")
			except ValueError:
				from chimera import UserError
				raise UserError("illegal proxy port number")
			class ProxyConn(_ProxyConnection):
				proxyHost = h
				proxyPort = p
			kw["transport"] = ProxyConn
		from AppService_client import AppServiceLocator
		from AppService_client import getAppMetadataRequest
		self.busy = False
		self.appLocator = AppServiceLocator()
		if sessionData:
			self.serviceURL, self.jobID, self.status = sessionData
		else:
			self.serviceURL = opalURL + serviceName
			self.jobID = None
			self.status = None
		self.times = None
		self.appServicePort = self.appLocator.getAppServicePort(
							self.serviceURL, **kw)
		if not sessionData:
			req = getAppMetadataRequest()
			resp = self.appServicePort.getAppMetadata(req)
			#print resp._usage

	def sessionData(self):
		return self.serviceURL, self.jobID, self.status

	def _saveStatus(self, status):
		self.status = (status._code, status._message, status._baseURL)

	def currentStatus(self):
		if self.status:
			return self.status[1]
		elif self.busy:
			return "waiting for response from Opal server"
		else:
			return "no Opal job running"

	def logJobID(self):
		print "Opal service URL: %s" % self.serviceURL
		if self.status:
			print "Opal job URL: %s" % self.status[2]

	def launchJob(self, cmdLine, **kw):
		if self.jobID is not None or self.busy:
			raise RuntimeError("Job has been launched already")
		import chimera
		from AppService_client import launchJobRequest
		import socket
		import ZSI
		req = launchJobRequest()
		req._argList = cmdLine
		req._numProcs = 1
		for key, value in kw.iteritems():
			setattr(req, key, value)
		if chimera.nogui:
			try:
				resp = self.appServicePort.launchJob(req)
			except (socket.error, ZSI.FaultException), e:
				self.dumpTraceback("launchJob")
				from chimera import NonChimeraError
				raise NonChimeraError(str(e))
			self.jobID = resp._jobID
			self._saveStatus(resp._status)
		else:
			from chimera.tkgui import runThread
			self.busy = True
			#import sys
			#print >> sys.__stderr__, "calling launchJobInThread"
			runThread(self._launchJobInThread, req)

	def _launchJobInThread(self, q, req):
		#import sys
		#print >> sys.__stderr__, "executing launchJobInThread"
		import socket
		import ZSI
		try:
			resp = self.appServicePort.launchJob(req)
		except (socket.error, ZSI.FaultException), e:
			self.dumpTraceback("launchJobInThread")
			def f(e=e):
				from chimera import NonChimeraError
				raise NonChimeraError(str(e))
			q.put(f)
		else:
			self.jobID = resp._jobID
			self._saveStatus(resp._status)
			q.put(self.logJobID)
		self.busy = False
		q.put(q)
		#import sys
		#print >> sys.__stderr__, "returning from launchJobInThread"

	def launchJobBlocking(self, cmdLine, **kw):
		if self.jobID is not None:
			raise RuntimeError("Job has been launched already")
		from AppService_client import launchJobBlockingRequest
		import socket
		import ZSI
		req = launchJobBlockingRequest()
		req._argList = cmdLine
		req._numProcs = 1
		for key, value in kw.iteritems():
			setattr(req, key, value)
		try:
			resp = self.appServicePort.launchJobBlocking(req)
		except (socket.error, ZSI.FaultException), e:
			self.dumpTraceback("launchJobBlocking")
			from chimera import NonChimeraError
			raise NonChimeraError(str(e))
		self._saveStatus(resp._status)
		self.logJobID()
		try:
			fileMap = self._makeOutputs(resp._jobOut)
		except:
			fileMap = None
		return (resp._status._code == 8, fileMap)

	def showStatus(self):
		print "Status:"
		code, message, baseURL = self.status
		print "\tCode:", code
		print "\tMessage:", message
		print "\tOutput Base URL:", baseURL

	def isFinished(self):
		#import sys
		#print >> sys.__stderr__, "executing isFinished", self.busy
		if self.busy:
			#print >> sys.__stderr__, "busy"
			return 0
		if self.status is None:
			#print >> sys.__stderr__, "no job"
			return 2
		code = self.status[0]
		if code == 8:
			# Normal finish
			#print >> sys.__stderr__, "normal"
			return 1
		elif code == 4:
			# Abnormal finish
			#print >> sys.__stderr__, "abnormal"
			return -1
		else:
			# Not finished
			#print >> sys.__stderr__, "not finished"
			return 0

	def queryStatus(self):
		#import sys
		#print >> sys.__stderr__, "calling from queryStatus", self.busy
		if self.busy:
			return
		if self.status is None:
			raise RuntimeError("No job has been launched yet")
		import chimera
		from AppService_client import queryStatusRequest
		import socket
		import ZSI
		req = queryStatusRequest(self.jobID)
		if chimera.nogui:
			try:
				status = self.appServicePort.queryStatus(req)
			except (socket.error, ZSI.FaultException), e:
				self.dumpTraceback("queryStatus")
				from chimera import NonChimeraError
				raise NonChimeraError(str(e))
			self._saveStatus(status)
		else:
			from chimera.tkgui import runThread
			self.busy = True
			#import sys
			#print >> sys.__stderr__, "calling from queryStatusInThread"
			runThread(self._queryStatusInThread, req)

	def _queryStatusInThread(self, q, req):
		#import sys
		#print >> sys.__stderr__, "executing from queryStatusInThread"
		import socket
		import ZSI
		try:
			status = self.appServicePort.queryStatus(req)
		except (socket.error, ZSI.FaultException), e:
			self.dumpTraceback("queryStatusInThread")
			def f(e=e):
				from chimera import NonChimeraError
				raise NonChimeraError(str(e))
			q.put(f)
		else:
			self._saveStatus(status)
		self.busy = False
		q.put(q)
		#import sys
		#print >> sys.__stderr__, "returning from queryStatusInThread"

	def getJobStatistics(self, background=False):
		#import sys
		#print >> sys.__stderr__, "calling from getJobStatistics", self.busy
		if self.busy:
			return
		if self.status is None:
			raise RuntimeError("No job has been launched yet")
		import chimera
		from AppService_client import getJobStatisticsRequest
		import socket
		import ZSI
		req = getJobStatisticsRequest(self.jobID)
		if chimera.nogui or not background:
			try:
				stats = self.appServicePort.getJobStatistics(req)
			except (socket.error, ZSI.FaultException), e:
				self.dumpTraceback("getJobStatistics")
				from chimera import NonChimeraError
				raise NonChimeraError(str(e))
			self._saveTimes(stats)
		else:
			from chimera.tkgui import runThread
			self.busy = True
			#import sys
			#print >> sys.__stderr__, "calling from getJobStatisticsInThread"
			runThread(self._getJobStatisticsInThread, req)

	def _getJobStatisticsInThread(self, q, req):
		#import sys
		#print >> sys.__stderr__, "executing from getJobStatisticsInThread"
		import socket
		import ZSI
		try:
			stats = self.appServicePort.getJobStatistics(req)
		except (socket.error, ZSI.FaultException), e:
			self.dumpTraceback("getJobStatisticsInThread")
			def f(e=e):
				from chimera import NonChimeraError
				raise NonChimeraError(str(e))
			q.put(f)
		else:
			self._saveTimes(stats)
		self.busy = False
		q.put(q)
		#import sys
		#print >> sys.__stderr__, "returning from getJobStatisticsInThread"

	def _saveTimes(self, stats):
		import time
		offset = time.timezone
		try:
			self.times = (time.mktime(stats._startTime) - offset,
				time.mktime(stats._completionTime) - offset)
		except:
			self.dumpTraceback("_saveTimes")

	def getOutputs(self):
		if self.status is None:
			raise RuntimeError("No job has been launched yet")
		from AppService_client import getOutputsRequest
		import socket
		import ZSI
		req = getOutputsRequest(self.jobID)
		try:
			resp = self.appServicePort.getOutputs(req)
		except (socket.error, ZSI.FaultException), e:
			self.dumpTraceback("getOutputs")
			from chimera import NonChimeraError
			raise NonChimeraError(str(e))
		return self._makeOutputs(resp)

	def _makeOutputs(self, out):
		self.fileMap = {
			"stdout.txt": out._stdOut,
			"stderr.txt": out._stdErr,
		}
		for file in out._outputFile:
			self.fileMap[file._name] = file._url
		return self.fileMap
	
	def getStdOut(self):
		"""
		return the content of stdout.txt file
		"""
		if self.status is None:
			raise RuntimeError("No job has been launched yet")
		stdOut_URL = self.status[2] + '/stdout.txt'
		return self.getURLContent(stdOut_URL)

	def destroy(self):
		if self.jobID is None:
			self.status = None
			return
		from AppService_client import destroyRequest
		import socket
		import ZSI
		req = destroyRequest(self.jobID)
		try:
			status = self.appServicePort.destroy(req)
		except (socket.error, ZSI.FaultException), e:
			self.dumpTraceback("destroy")
			from chimera import NonChimeraError
			raise NonChimeraError(str(e))
		#self.status = self._saveStatus(status)
		#self.showStatus()
		# Mark that no jobs are running
		self.status = None
		self.jobID = None

	def getURLContent(self, url):
		import urllib2
		f = urllib2.urlopen(url)
		data = f.read()
		f.close()
		return data

	def showURLContent(self, title, url):
		from chimera import replyobj
		data = self.getURLContent(url)
		if not data:
			data = "[no output]\n"
		replyobj.message("%s\n-----\n%s-----\n" % (title, data))
	
	def getFileContent(self, filename):
		if hasattr(self, 'fileMap'):
			fm = self.fileMap
		else:
			fm = self.getOutputs()
		if filename in fm:
			return self.getURLContent(fm[filename])
		else:
			return None

	def dumpTraceback(self, msg):
		import traceback, sys
		print "Traceback from web service request (%s):" % msg
		traceback.print_exc(file=sys.stdout)
		try:
			asp = self.appServicePort
			b = asp.binding
		except AttributeError:
			pass
		else:
			print "appServicePort", asp, "url:", b.url
	
def makeInputFile(path, name=None, mode="r"):
	try:
		# Path can be a 2-tuple of (file path, open mode)
		path, mode = path
	except ValueError:
		pass
	from AppService_types import ns0
	inputFile = ns0.InputFileType_Def("inputFile")
	if name is None:
		import os.path
		name = os.path.basename(path)
	inputFile._name = name
	f = open(path, mode)
	inputFile._contents = f.read()
	f.close()
	return inputFile

if __name__ == "__main__":
	def launchJobTest(opal, argList, **kw):
		import time, sys
		opal.launchJob(argList, **kw)
		while not opal.isFinished():
			opal.showStatus()
			sys.stdout.flush()
			time.sleep(10)
			opal.queryStatus()
		opal.showStatus()
		success = opal.isFinished() > 0
		fileMap = opal.getOutputs()
		return success, fileMap

	if 0:
		import pprint
		# Test pdb2pqr at NBCR
		service = "Pdb2pqrOpalService"
		NBCR_Opal = "http://ws.nbcr.net:8080/opal/services/"
		argList = "--ff=amber sample.pdb sample.pqr"
		inputFiles = [ makeInputFile("opal_testdata/sample.pdb") ]

		opal = OpalService(service, NBCR_Opal)

		print "Testing non-blocking job"
		success, fileMap = launchJobTest(opal, argList,
							_inputFile=inputFiles)
		print "Success", success
		print "Outputs:"
		pprint.pprint(fileMap)
		opal.destroy()
		print "Finished non-blocking job"

		print "Testing blocking job"
		success, fileMap = opal.launchJobBlocking(argList,
							_inputFile=inputFiles)
		print "Outputs:"
		pprint.pprint(fileMap)
		opal.destroy()
		print "Finished blocking job"

	if 1:
		import pprint
		service = "BlastproteinServicePort"
		argList = "-i blastpdb.in -o blastpdb.out -e 1e-10"
		inputFiles = [ makeInputFile("opal_testdata/blastpdb.in") ]
		print "Launching blastprotein job"
		opal = OpalService(service)
		success, fileMap = launchJobTest(opal, argList,
							_inputFile=inputFiles)
		print "Success", success
		print "Outputs:"
		pprint.pprint(fileMap)
		print "Finished blastpdb job"
		def showFile(name, url):
			import sys, urllib2
			print "%s:" % name
			print "-----"
			f = urllib2.urlopen(url)
			sys.stdout.write(f.read())
			f.close()
			print "-----"
		showFile("blastpdb.in", fileMap["blastpdb.in"])
		if success:
			showFile("blastpdb.out", fileMap["blastpdb.out"])
		else:
			showFile("stdout", fileMap["stdout"])
			showFile("stderr", fileMap["stderr"])
