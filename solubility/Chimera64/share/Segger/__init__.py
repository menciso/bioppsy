dev_menus = False       # Include under-development menus.
timing = False          # Report execution times for optimizing code.

from regions import Segmentation, Region, SelectedRegions
