
# Copyright (c) 2009 Greg Pintilie - pintilie@mit.edu

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import chimera
import os
import os.path
import Tkinter
from CGLtk import Hybrid
import VolumeData
import _multiscale
import MultiScale.surface
import _surface
import numpy
import _contour
import Matrix
import VolumeViewer
from sys import stderr
from time import clock

from axes import prAxes
import regions
import graph
from Segger import dev_menus, timing

OML = chimera.openModels.list

REG_OPACITY = 0.45


from segment_dialog import current_segmentation, segmentation_map


def umsg ( txt ) :
    print txt
    status ( txt )

def status ( txt ) :
    txt = txt.rstrip('\n')
    msg.configure(text = txt)
    msg.update_idletasks()


class Extract_Region_Dialog ( chimera.baseDialog.ModelessDialog ):

    title = "Extract Densities (Segger v1.8.1)"
    name = "extract region"
    buttons = ('Extract', "Close")
    help = 'https://sites.google.com/site/electronmicroscopyportal/tools/segger/user-guide/extractregion'

    def fillInUI(self, parent):

        self.group_mouse_mode = None

        tw = parent.winfo_toplevel()
        self.toplevel_widget = tw
        tw.withdraw()

        parent.columnconfigure(0, weight = 1)

        row = 0

        menubar = Tkinter.Menu(parent, type = 'menubar', tearoff = False)
        tw.config(menu = menubar)


        f = Tkinter.Frame(parent)
        f.grid(column=0, row=row, sticky='ew')

        l = Tkinter.Label(f, text='  ')
        l.grid(column=0, row=row, sticky='w')

        row += 1


        ff = Tkinter.Frame(f)
        ff.grid(column=0, row=row, sticky='w')

        l = Tkinter.Label(ff, text='  Extract densities from map:')
        l.grid(column=0, row=0, sticky='w')

        self.dmap = Tkinter.StringVar(parent)

        self.mb  = Tkinter.Menubutton ( ff, textvariable=self.dmap, relief=Tkinter.RAISED )
        self.mb.grid (column=1, row=0, sticky='we', padx=5)
        self.mb.menu  =  Tkinter.Menu ( self.mb, tearoff=0, postcommand=self.MapMenu )
        self.mb["menu"]  =  self.mb.menu


        # set the segmentation map (if any) as the initial default
        self.cur_dmap = segmentation_map()
        st1, st2, st3 = "", "", ""
        d1, d2, d3 = "", "", ""
        if self.cur_dmap :
            # print "Current segmentation map: ", self.cur_dmap.name
            self.dmap.set ( self.cur_dmap.name )
            st = self.cur_dmap.data.step
            
            st1 = "%g" % self.cur_dmap.data.step[0]
            st2 = "%g" % self.cur_dmap.data.step[1]
            st3 = "%g" % self.cur_dmap.data.step[2]

            d1 = "%g" % self.cur_dmap.data.size[0]
            d2 = "%g" % self.cur_dmap.data.size[1]
            d3 = "%g" % self.cur_dmap.data.size[2]


        row += 1

        l = Tkinter.Label(f, text='  ')
        l.grid(column=0, row=row, sticky='w')


        row += 1
        l = Tkinter.Label(f, text='  Dimensions of new map:')
        l.grid(column=0, row=row, sticky='w')


        row += 1
        ff = Tkinter.Frame(f)
        ff.grid(column=0, row=row, sticky='w')
        
        l = Tkinter.Label(ff, text=' ', width=5)
        l.grid(column=0, row=0, sticky='w')

        self.newMapDimOption = Tkinter.StringVar()
        self.newMapDimOption.set ( 'box' )

        l = Tkinter.Radiobutton(ff, text="Same as map from which densities are extracted: ", variable=self.newMapDimOption, value = 'same')
        l.grid (column=1, row=0, sticky='w')

        self.oMapDim1 = Tkinter.StringVar(ff, d1)
        e = Tkinter.Entry(ff, width=5, textvariable=self.oMapDim1, state="disabled")
        e.grid(column=2, row=0, sticky='w', padx=5)

        self.oMapDim2 = Tkinter.StringVar(ff, d2)
        e = Tkinter.Entry(ff, width=5, textvariable=self.oMapDim2, state="disabled")
        e.grid(column=3, row=0, sticky='w', padx=5)

        self.oMapDim3 = Tkinter.StringVar(ff, d3)
        e = Tkinter.Entry(ff, width=5, textvariable=self.oMapDim3, state="disabled")
        e.grid(column=4, row=0, sticky='w', padx=5)


        row += 1
        ff = Tkinter.Frame(f)
        ff.grid(column=0, row=row, sticky='w')
        l = Tkinter.Label(ff, text=' ', width=5)
        l.grid(column=0, row=0, sticky='w')

        c = Tkinter.Radiobutton(ff, text="Cube around region, with border of", variable=self.newMapDimOption, value = 'cube')
        c.grid (column=1, row=0, sticky='w')

        self.borderWidth = Tkinter.StringVar(ff, st1)
        self.borderWidth.set ( "2" )
        e = Tkinter.Entry(ff, width=5, textvariable=self.borderWidth)
        e.grid(column=2, row=0, sticky='w', padx=5)

        c = Tkinter.Label(ff, text='voxels')
        c.grid (column=3, row=0, sticky='w')

        c = Tkinter.Radiobutton(ff, text="Box around region, with border of", variable=self.newMapDimOption, value = 'box')
        c.grid (column=1, row=1, sticky='w')

        self.borderWidth.set ( "2" )
        e = Tkinter.Entry(ff, width=5, textvariable=self.borderWidth)
        e.grid(column=2, row=1, sticky='w', padx=5)

        c = Tkinter.Label(ff, text='voxels')
        c.grid (column=3, row=1, sticky='w')


        row += 1
        l = Tkinter.Label(f, text='  ')
        l.grid(column=0, row=row, sticky='w')


        row += 1
        l = Tkinter.Label(f, text='  Voxel size in new map:')
        l.grid(column=0, row=row, sticky='w')



        row += 1
        ff = Tkinter.Frame(f)
        ff.grid(column=0, row=row, sticky='w')

        self.newGridStep = Tkinter.StringVar()
        self.newGridStep.set ( 'same' )

        l = Tkinter.Label(ff, text=' ', width=5)
        l.grid(column=0, row=0, sticky='w')

        c = Tkinter.Radiobutton(ff, text="Same as map from which densities are extracted: ", variable=self.newGridStep, value = 'same')
        c.grid (column=1, row=0, sticky='w')

        self.oMapStep1 = Tkinter.StringVar(ff, st1)
        e = Tkinter.Entry(ff, width=5, textvariable=self.oMapStep1, state="disabled")
        e.grid(column=2, row=0, sticky='w', padx=5)

        self.oMapStep2 = Tkinter.StringVar(ff, st2)
        e = Tkinter.Entry(ff, width=5, textvariable=self.oMapStep2, state="disabled")
        e.grid(column=3, row=0, sticky='w', padx=5)

        self.oMapStep3 = Tkinter.StringVar(ff, st3)
        e = Tkinter.Entry(ff, width=5, textvariable=self.oMapStep3, state="disabled")
        e.grid(column=4, row=0, sticky='w', padx=5)


        row += 1
        ff = Tkinter.Frame(f)
        ff.grid(column=0, row=row, sticky='w')

        l = Tkinter.Label(ff, text=' ', width=5)
        l.grid(column=0, row=0, sticky='w')

        c = Tkinter.Radiobutton(ff, text="Custom: ", variable=self.newGridStep, value = 'custom')
        c.grid (column=1, row=0, sticky='w')

        self.newMapStep1 = Tkinter.StringVar(ff, ".5")
        e = Tkinter.Entry(ff, width=5, textvariable=self.newMapStep1)
        e.grid(column=2, row=0, sticky='w', padx=5)

        self.newMapStep2 = Tkinter.StringVar(ff, ".5")
        e = Tkinter.Entry(ff, width=5, textvariable=self.newMapStep2)
        e.grid(column=3, row=0, sticky='w', padx=5)

        self.newMapStep3 = Tkinter.StringVar(ff, ".5")
        e = Tkinter.Entry(ff, width=5, textvariable=self.newMapStep3)
        e.grid(column=4, row=0, sticky='w', padx=5)


        row += 1
        l = Tkinter.Label(f, text='  ')
        l.grid(column=0, row=row, sticky='w')


        row += 1
        l = Tkinter.Label(f, text='  Which densities to extract:')
        l.grid(column=0, row=row, sticky='w')

        self.whichDensities = Tkinter.StringVar()
        self.whichDensities.set ( 'inside' )

        row += 1
        ff = Tkinter.Frame(f)
        ff.grid(column=0, row=row, sticky='w')
        l = Tkinter.Label(ff, text=' ', width=5)
        l.grid(column=0, row=0, sticky='w')

        c = Tkinter.Radiobutton(ff, text="Only densities inside the selected region(s)", variable=self.whichDensities, value = 'inside')
        c.grid (column=1, row=0, sticky='w')

        c = Tkinter.Radiobutton(ff, text="All densities inside the bounds of the new map", variable=self.whichDensities, value = 'all')
        c.grid (column=1, row=1, sticky='w')
        
        
        row += 1
        l = Tkinter.Label(f, text='  ')
        l.grid(column=0, row=row, sticky='w')
        

        row += 1
        ff = Tkinter.Frame(f)
        ff.grid(column=0, row=row, sticky='w')
        l = Tkinter.Label(ff, text=' ', width=1)
        l.grid(column=0, row=0, sticky='w')

        c = Hybrid.Checkbutton(ff, 'Add drop-off densities outside region boundary. Number of iterations:', True )
        c.button.grid (column=1, row=0, sticky='w')
        self.addDropOff = c.variable

        self.dropOffWidth = Tkinter.StringVar(ff, "10")
        e = Tkinter.Entry(ff, width=5, textvariable=self.dropOffWidth)
        e.grid(column=2, row=0, sticky='w', padx=5)

        
        row += 1
        dummyFrame = Tkinter.Frame(parent, relief='groove', borderwidth=1)
        Tkinter.Frame(dummyFrame).pack()
        dummyFrame.grid(row=row,column=0,columnspan=7, pady=7, sticky='we')

        row = row + 1

        global msg
        msg = Tkinter.Label(parent, width = 60, anchor = 'w', justify = 'left')
        msg.grid(column=0, row=row, sticky='ew')
        self.msg = msg
        row += 1



    def MapMenu ( self ) :

        self.mb.menu.delete ( 0, 'end' )        # Clear menu
        from VolumeViewer import Volume
        mlist = OML(modelTypes = [Volume])
        for m in mlist :
            self.mb.menu.add_radiobutton ( label=m.name, variable=self.dmap,
                                           command=lambda m=m: self.MapSelected(m) )

    def SetMapMenu (self, dmap):

        mname = dmap.name if dmap else ''
        self.dmap.set(mname)
        self.cur_dmap = dmap
        #print "Set map menu to ", dmap.name


    def MapSelected ( self, dmap ) :

        self.cur_dmap = dmap
        if dmap:
            dmap.display = True

            self.oMapDim1.set ( "%d" % dmap.data.size[0] )
            self.oMapDim2.set ( "%d" % dmap.data.size[1] )
            self.oMapDim3.set ( "%d" % dmap.data.size[2] )

            self.oMapStep1.set ( "%g" % dmap.data.step[0] )
            self.oMapStep2.set ( "%g" % dmap.data.step[1] )
            self.oMapStep3.set ( "%g" % dmap.data.step[2] )

        else :
            self.oMapDim1.set ( "" )
            self.oMapDim2.set ( "" )
            self.oMapDim3.set ( "" )
        
            self.oMapStep1.set ( "" )
            self.oMapStep2.set ( "" )
            self.oMapStep3.set ( "" )






    def Extract ( self ) :

        fromMap = self.cur_dmap
        if fromMap == None :
            umsg ( "Please select a map to extract densities from" )
            return

        segMap = segmentation_map()
        segMod = current_segmentation ()

        if segMap == None :
            umsg ( "Please select a map in the Segment Map Dialog" )
            return

        if segMod == None :
            umsg ( "Please select a segmentation in the Segment Map Dialog" )
            return

        umsg ( "Extracting densities from " + fromMap.name + " based on selected regions in " + segMap.name )

        if segMap == fromMap :
            print "Same map!"

        else:
            print "Different maps!"


        regs = segMod.selected_regions()
        reg_str = ""
        for r in regs : reg_str = reg_str + "_r%d" % r.rid
        print reg_str

        ndata = None


        if self.newMapDimOption.get() == "same" :

            print " - same dimensions as original map"

            if self.whichDensities.get() == "inside" :
                print " - want inner densities only"
                ndata = self.dataMaskedWithSelectedRegions ( segMod, fromMap )
                if ndata == None : return

            else :
                print " - want all densities"
                # nothing to do, since they want the same-dimension map, with all the densities
                ndata = fromMap.data


        elif self.newMapDimOption.get() == "cube" or self.newMapDimOption.get() == "box" :

            print " - shrinking to region bounds "

            #dims = self.boundsOfSelectedRegions ( segMod, fromMap )
            #if dims == None : return
            #li,lj,lk, hi,hj,hk, n1,n2,n3 = dims

            ndata = self.dataMaskedWithSelectedRegions ( segMod, fromMap )
            if ndata == None : return

            regsm = ndata.matrix()
            nze = numpy.nonzero ( regsm )
            # print nze

            li = numpy.min ( nze[0] )
            lj = numpy.min ( nze[1] )
            lk = numpy.min ( nze[2] )

            hi = numpy.max ( nze[0] )
            hj = numpy.max ( nze[1] )
            hk = numpy.max ( nze[2] )

            bound = int ( self.borderWidth.get() )
            li = li - bound; lj = lj - bound; lk = lk - bound
            hi = hi + bound; hj = hj + bound; hk = hk + bound

            n1 = hi - li + 1
            n2 = hj - lj + 1
            n3 = hk - lk + 1

            if self.newMapDimOption.get() == "cube" :
                n = max ( n1, n2, n3 )
                li -= int ( (n-n1) / 2 )
                lj -= int ( (n-n2) / 2 )
                lk -= int ( (n-n3) / 2 )
                n1, n2, n3 = n, n, n

            print " - bounds of selected regions: %d %d %d --> %d %d %d --> %d %d %d" % ( li,lj,lk, hi,hj,hk, n1,n2,n3 )


            nmat = numpy.zeros ( (n1,n2,n3), numpy.float32 )
            #dmat = dmap.full_matrix()

            dmat = fromMap.full_matrix()
            print "map grid dim: ", numpy.shape ( dmat )
            print "masked grid dim: ", numpy.shape ( regsm )
            print "new map grid dim: ", numpy.shape ( nmat )

            if self.whichDensities.get() == "inside" :
                # copy the densities from insize the regions only
                for ii in range ( len(nze[0]) ) :
                    i,j,k = nze[0][ii], nze[1][ii], nze[2][ii]
                    #nmat[k-lk,j-lj,i-li] = regsm[k,j,i]
                    nmat[i-li,j-lj,k-lk] = regsm[i,j,k]
            else :
                # capy the densities inside the entire block
                for i in range ( li, hi ) :
                    for j in range ( lj, hj ) :
                        for k in range ( lk, hk ) :
                            #val = dmat[k,j,i]
                            #nmat[k-lk,j-lj,i-li] = val
                            try :
                                #nmat[k-lk,j-lj,i-li] = dmat[k,j,i]
                                nmat[i-li,j-lj,k-lk] = dmat[i,j,k]
                            except :
                                #print "iout (%d,%d,%d) (%d,%d,%d)" % (k, j, i, k-lk,j-lj,i-li)
                                pass
    
            O = fromMap.data.origin
            print "origin:", O
            nO = ( O[0] + float(lk) * fromMap.data.step[0],
                   O[1] + float(lj) * fromMap.data.step[1],
                   O[2] + float(li) * fromMap.data.step[2] )
            
            print "new origin:", nO

            ndata = VolumeData.Array_Grid_Data ( nmat, nO, fromMap.data.step, fromMap.data.cell_angles )


        if self.newGridStep.get() == "custom" :

            st1 = float ( self.newMapStep1.get() )
            st2 = float ( self.newMapStep2.get() )
            st3 = float ( self.newMapStep3.get() )

            print " - new step: %d %d %d", st1, st2, st3

            # make a temp map from which the density values will be interpolated

            try : nv = VolumeViewer.volume.add_data_set ( ndata, None )
            except : nv = VolumeViewer.volume.volume_from_grid_data ( ndata )
            nv.name = "TempMap"

            n1 = int ( numpy.ceil ( ndata.size[0] * ( ndata.step[0] / st1 ) ) )
            n2 = int ( numpy.ceil ( ndata.size[1] * ( ndata.step[1] / st2 ) ) )
            n3 = int ( numpy.ceil ( ndata.size[2] * ( ndata.step[2] / st3 ) ) )

            print " - new dimensions: %d %d %d" % (n1, n2, n3)

            # make a new matrix with the desired voxel size
            nmat = numpy.zeros ( (n3,n2,n1), numpy.float32 )
            ndata = VolumeData.Array_Grid_Data ( nmat, ndata.origin, (st1,st2,st3), ndata.cell_angles )

            npoints = VolumeData.grid_indices ( (n1,n2,n3), numpy.single)  # i,j,k indices
            _contour.affine_transform_vertices ( npoints, ndata.ijk_to_xyz_transform )

            dvals = nv.interpolated_values ( npoints, None, method = 'linear' )  # dmap.openState.xform )
            #dvals = numpy.where ( dvals > threshold, dvals, numpy.zeros_like(dvals) )
            #nze = numpy.nonzero ( dvals )

            nmat = dvals.reshape( (n3,n2,n1) )
            #f_mat = fmap.data.full_matrix()
            #f_mask = numpy.where ( f_mat > fmap.surface_levels[0], numpy.ones_like(f_mat), numpy.zeros_like(f_mat) )
            #df_mat = df_mat * f_mask

            ndata = VolumeData.Array_Grid_Data ( nmat, ndata.origin, (st1,st2,st3), ndata.cell_angles )

            nv.close ()

        try : nv = VolumeViewer.volume.add_data_set ( ndata, None )
        except : nv = VolumeViewer.volume.volume_from_grid_data ( ndata )
        nv.name = "Extracted Densities Map"

        if segMap == fromMap :
            nv.name = os.path.splitext(fromMap.name)[0] + reg_str + ".mrc"
        else :
            nv.name = os.path.splitext(fromMap.name)[0] + "_with_" + os.path.splitext(segMap.name)[0] + reg_str + ".mrc"


        if self.addDropOff.get () :

            print "\n---adding dropoff---\n"

            nvm = nv.full_matrix()
            f_mask = numpy.where ( nvm > 0, numpy.zeros_like(nvm), numpy.ones_like(nvm) )


            if  ( 0 ) :
                # use gaussian
                s = nv.data.step
                width = max (nv.data.step) #float ( self.dropOffWidth.get() )
                width = numpy.sqrt ( s[0]*s[0] + s[1]*s[1] + s[2]*s[2] ) #float ( self.dropOffWidth.get() )
                from VolumeFilter import gaussian
                gvol = gaussian.gaussian_convolve (nv, width )
                gvol.name = nv.name + "_g"
                gvm = gvol.full_matrix()
            
            elif ( 1 ) :
                # heat equation - boundary conditions == fixed densities at region boundary
                numit = int ( self.dropOffWidth.get() )
                gvm = nvm.copy();
                for i in range (numit) :
                    nv_1 = numpy.roll(gvm, 1, axis=0)
                    nv_2 = numpy.roll(gvm, -1, axis=0)
                    nv_3 = numpy.roll(gvm, 1, axis=1)
                    nv_4 = numpy.roll(gvm, -1, axis=1)
                    nv_5 = numpy.roll(gvm, 1, axis=2)
                    nv_6 = numpy.roll(gvm, -1, axis=2)
                    gvm = 1.0/6.0 * ( nv_1 + nv_2 + nv_3 + nv_4 + nv_5 + nv_6 )
                    gvm = f_mask * gvm + nvm
            
            else :
                from numpy import maximum as MMAX
                # fraction of max
                gvm = nvm.copy();
                for i in range (1) :
                    nv_1 = numpy.roll(gvm, 1, axis=0)
                    nv_2 = numpy.roll(gvm, -1, axis=0)
                    nv_3 = numpy.roll(gvm, 1, axis=1)
                    nv_4 = numpy.roll(gvm, -1, axis=1)
                    nv_5 = numpy.roll(gvm, 1, axis=2)
                    nv_6 = numpy.roll(gvm, -1, axis=2)
                    gvm = 4.0/6.0 * ( MMAX(nv_1, MMAX(nv_2, MMAX(nv_3, MMAX(nv_4, MMAX(nv_5,nv_6))))) )

            ngvm = f_mask * gvm + nvm

            ndata = VolumeData.Array_Grid_Data ( ngvm, nv.data.origin, nv.data.step, nv.data.cell_angles )
            try : nvg = VolumeViewer.volume.add_data_set ( ndata, None )
            except : nvg = VolumeViewer.volume.volume_from_grid_data ( ndata )
            nvg.name = nv.name

            chimera.openModels.close ( [nv] )
            nv = nvg

        nv.openState.xform = fromMap.openState.xform
        
        umsg ( "Done - created " + nv.name )



    def dataMaskedWithSelectedRegions ( self, segModel, fromMap ) :

        regs = segModel.selected_regions()
        if len(regs)==0 :
            umsg ( "No regions from current segmentation are selected" )
            return None

        points = regs[0].points().astype ( numpy.float32 )
        for r in regs[1:] :
            npoints = r.points().astype ( numpy.float32 )
            points = numpy.concatenate ( [points, npoints], axis=0 )

        _contour.affine_transform_vertices ( points, segModel.seg_map.data.ijk_to_xyz_transform )
        _contour.affine_transform_vertices ( points, Matrix.xform_matrix( segModel.openState.xform ) )
        _contour.affine_transform_vertices ( points, Matrix.xform_matrix( fromMap.openState.xform.inverse() ) )

        d = min ( segModel.seg_map.data.step ) * 0.99
        ndata = VolumeData.zone_masked_grid_data ( fromMap.data, points, d )

        return ndata

          

    def MaskAnotherMapWRegions ( self ) :

        if len(self.dmap.get()) == 0 : umsg ("Please select a map from which density will be taken" ); return
        dmap = self.SegmentationMap()
        if dmap == None : umsg ( "%s is not open" % self.dmap.get() ); return

        smod = self.CurrentSegmentation()
        if smod is None : return

        regs = smod.selected_regions()
        if len(regs)==0 :
            umsg ( "Please select one ore more regions" )
            return

        points = regs[0].points().astype ( numpy.float32 )
        for r in regs[1:] :
            npoints = r.points().astype ( numpy.float32 )
            points = numpy.concatenate ( [points, npoints], axis=0 )

        _contour.affine_transform_vertices ( points, smod.seg_map.data.ijk_to_xyz_transform )
        _contour.affine_transform_vertices ( points, Matrix.xform_matrix( smod.openState.xform ) )
        _contour.affine_transform_vertices ( points, Matrix.xform_matrix( dmap.openState.xform.inverse() ) )

        sg = VolumeData.zone_masked_grid_data ( dmap.data, points, smod.seg_map.data.step[0] )

        try : gv = VolumeViewer.volume.add_data_set ( sg, None )
        except : gv = VolumeViewer.volume.volume_from_grid_data ( sg )
        gv.openState.xform = dmap.openState.xform
        #chimera.openModels.add ( [gv] )
        gv.name = "Masked"





    def MaskMapWRegionsCube ( self ) :

        # thsi is useful for input to EMAN fitting procedures which
        # requires a cube map

        if len(self.dmap.get()) == 0 : umsg ("Please select a map from which density will be taken" ); return
        dmap = self.SegmentationMap()
        if dmap == None : umsg ( "%s is not open" % self.dmap.get() ); return

        smod = self.CurrentSegmentation()
        if smod is None : return

        regs = smod.selected_regions()
        if len(regs)==0 :
            umsg ( "Please select one ore more regions" )
            return

        if 0 :
            points = regs[0].points().astype ( numpy.float32 )
            for r in regs[1:] :
                npoints = r.points().astype ( numpy.float32 )
                points = numpy.concatenate ( [points, npoints], axis=0 )

        for rri, reg in enumerate ( regs ) :

            print " ---- Region %d/%d ---- " % (rri+1, len(regs))

            points = reg.points().astype ( numpy.float32 )

            _contour.affine_transform_vertices ( points, smod.seg_map.data.ijk_to_xyz_transform )
            _contour.affine_transform_vertices ( points, Matrix.xform_matrix( smod.openState.xform ) )
            _contour.affine_transform_vertices ( points, Matrix.xform_matrix( dmap.openState.xform.inverse() ) )

            sg = VolumeData.zone_masked_grid_data ( dmap.data, points, smod.seg_map.data.step[0] )
            regsm = sg.matrix()

            nze = numpy.nonzero ( regsm )

            # print nze

            li = numpy.min ( nze[0] )
            lj = numpy.min ( nze[1] )
            lk = numpy.min ( nze[2] )

            hi = numpy.max ( nze[0] )
            hj = numpy.max ( nze[1] )
            hk = numpy.max ( nze[2] )

            ci = int ( numpy.ceil ( (hi + li) / 2 ) )
            cj = int ( numpy.ceil ( (hj + lj) / 2 ) )
            ck = int ( numpy.ceil ( (hk + lk) / 2 ) )

            n1 = hi - li + 1
            n2 = hj - lj + 1
            n3 = hk - lk + 1

            n = 120 # max ( n1, n2, n3 ) + 4
            n2 = int ( numpy.ceil ( n / 2 ) )

            li = ci - n2; lj = cj - n2; lk = ck - n2
            hi = ci + n2; hj = cj + n2; hk = ck + n2

            print "Bounds - %d %d %d --> %d %d %d --> %d %d %d (%d)" % ( li, lj, lk, hi, hj, hk, n1, n2, n3, n )

            umsg ( "Saving %d regions to mrc file..." % len(regs) )

            nmat = numpy.zeros ( (n,n,n), numpy.float32 )
            #dmat = dmap.full_matrix()

            print "map grid dim: ", numpy.shape ( dmap.full_matrix() )
            print "masked grid dim: ", numpy.shape ( regsm )
            print "new map grid dim: ", numpy.shape ( nmat )


            #regs_name = ""
            for ii in range ( len(nze[0]) ) :
                i,j,k = nze[0][ii], nze[1][ii], nze[2][ii]
                mapVal = regsm[i,j,k]
                nmat[i-li,j-lj,k-lk] = mapVal

            O = dmap.data.origin
            print "origin:", O
            if 1 :
                nO = ( O[0] + float(lk) * dmap.data.step[0],
                       O[1] + float(lj) * dmap.data.step[1],
                       O[2] + float(li) * dmap.data.step[2] )
                print "new origin:", nO
            else :
                nO = ( -float(n2) * dmap.data.step[0],
                       -float(n2) * dmap.data.step[1],
                       -float(n2) * dmap.data.step[2] )
                print "new origin:", nO
            

            ndata = VolumeData.Array_Grid_Data ( nmat, nO, dmap.data.step, dmap.data.cell_angles )
            try : nv = VolumeViewer.volume.add_data_set ( ndata, None )
            except : nv = VolumeViewer.volume.volume_from_grid_data ( ndata )

            suff = "_CubeRid%d.mrc" % reg.rid

            import os.path
            nv.name = os.path.splitext (dmap.name) [0] + suff
            nv.openState.xform = dmap.openState.xform

            path = os.path.splitext (dmap.data.path) [0] + suff
            nv.write_file ( path, "mrc" )


    def ExtractMapWRegionsCube ( self ) :

        if len(self.dmap.get()) == 0 : umsg ("Please select a map from which density will be taken" ); return
        dmap = self.SegmentationMap()
        if dmap == None : umsg ( "%s is not open" % self.dmap.get() ); return

        smod = self.CurrentSegmentation()
        if smod is None : return

        regs = smod.selected_regions()
        if len(regs)==0 :
            umsg ( "Please select one ore more regions" )
            return

        if 0 :
            points = regs[0].points().astype ( numpy.float32 )
            for r in regs[1:] :
                npoints = r.points().astype ( numpy.float32 )
                points = numpy.concatenate ( [points, npoints], axis=0 )

        for rri, reg in enumerate ( regs ) :

            (li,lj,lk), (hi,hj,hk) = regions.region_bounds( [reg] )

            ci = int ( numpy.ceil ( (hi + li) / 2 ) )
            cj = int ( numpy.ceil ( (hj + lj) / 2 ) )
            ck = int ( numpy.ceil ( (hk + lk) / 2 ) )

            n1 = hi - li + 1
            n2 = hj - lj + 1
            n3 = hk - lk + 1

            n = 62 # max ( n1, n2, n3 ) + 4
            n2 = int ( numpy.ceil ( n / 2 ) )

            li = ci - n2; lj = cj - n2; lk = ck - n2
            hi = ci + n2; hj = cj + n2; hk = ck + n2

            #bound = 2
            #li = li - bound; lj = lj - bound; lk = lk - bound
            #hi = hi + bound; hj = hj + bound; hk = hk + bound

            print "Bounds - %d %d %d --> %d %d %d --> %d %d %d, %d" % ( li, lj, lk, hi, hj, hk, n1,n2,n3, n )

            umsg ( "Saving %d regions to mrc file..." % len(regs) )

            #nmat = numpy.zeros ( (n3,n2,n1), numpy.float32 )
            nmat = numpy.zeros ( (n,n,n), numpy.float32 )
            dmat = dmap.full_matrix()

            #regs_name = ""
            for i in range ( li, hi ) :
                for j in range ( lj, hj ) :
                    for k in range ( lk, hk ) :
                        try :
                            nmat[k-lk,j-lj,i-li] = dmat[k,j,i]
                        except :
                            pass

            O = dmap.data.origin
            print "origin:", O
            nO = O
            if 1 :
                nO = ( O[0] + float(li) * dmap.data.step[0],
                       O[1] + float(lj) * dmap.data.step[1],
                       O[2] + float(lk) * dmap.data.step[2] )
            else :            
                nO = ( -float(n2) * dmap.data.step[0],
                       -float(n2) * dmap.data.step[1],
                       -float(n2) * dmap.data.step[2] )
                print "new origin:", nO

            ndata = VolumeData.Array_Grid_Data ( nmat, nO, dmap.data.step, dmap.data.cell_angles )
            try : nv = VolumeViewer.volume.add_data_set ( ndata, None )
            except : nv = VolumeViewer.volume.volume_from_grid_data ( ndata )

            suff = "_MC_%d_%d_%d_%d.mrc" % (li, lj, lk, n)

            import os.path
            nv.name = os.path.splitext (dmap.name) [0] + suff
            nv.openState.xform = dmap.openState.xform

            path = os.path.splitext (dmap.data.path) [0] + suff
            nv.write_file ( path, "mrc" )
        





def extract_region_dialog ( create=False ) :

  from chimera import dialogs
  return dialogs.find ( "extract region", create=create )


def close_extract_region_dialog ():

    from chimera import dialogs
    d = extract_region_dialog ()
    if d :
        d.toplevel_widget.update_idletasks ()
        d.Close()
        d.toplevel_widget.update_idletasks ()



def show_extract_region_dialog ():

    from chimera import dialogs
    d = extract_region_dialog ( create = True )
    
    # Avoid transient dialog resizing when created and mapped for first time.
    d.toplevel_widget.update_idletasks ()
    d.enter()

    return d



# -----------------------------------------------------------------------------
#
from chimera import dialogs
dialogs.register (Extract_Region_Dialog.name, Extract_Region_Dialog,
                  replace = True)

