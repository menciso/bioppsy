import chimera
import numpy

def AxesMod ( COM=[0,0,0], U=None, Extents=[30,30,30], rad=1.0, f=1.0,
              alignTo = None ) :

    import _surface
    mol = _surface.SurfaceModel()
    chimera.openModels.add([mol], sameAs = alignTo)

    axes = AddAxes ( rad, Extents[0]*f, Extents[1]*f, Extents[2]*f, 1.0, mol )
    axes.name = "Axes"

    if U != None :
        R = numpy.array([
            [  U[0,0], U[0,1], U[0,2], 0.0    ],
            [  U[1,0], U[1,1], U[1,2], 0.0    ],
            [  U[2,0], U[2,1], U[2,2], 0.0    ]  ] )

        T = numpy.array([
            [  1.0, 0.0, 0.0, COM[0]   ],
            [  0.0, 1.0, 0.0, COM[1]   ],
            [  0.0, 0.0, 1.0, COM[2]   ]  ] )

        import Matrix
        M = Matrix.multiply_matrices ( T, R )

        ps = []
        for p in axes.surfacePieces :
            v, t = numpy.copy(p.geometry[0]), numpy.copy(p.geometry[1])
            ps.append ( [v,t,p.color] )
            axes.removePiece ( p )

        import _contour
        for p in ps :
            _contour.affine_transform_vertices( p[0], M )
            axes.addPiece ( p[0], p[1], p[2] )

        from random import random as rand
        clr = ( rand()*.7, rand()*.7, rand()*.7, 1.0 )
        # for p in axes.surfacePieces : p.color = clr

    #for g in axes.surfacePieces :
    #    g.initial_v = numpy.copy ( g.geometry[0] )

    return axes


def AddArrow2 ( pos, v, d, clr=(0,1,1,1), rad=0.2, mol=None ) :

    xf = AlignXf ( pos, v )
    mol = CylinderMesh2 (rad, rad, d-(rad*3), 10, clr, xf, mol )

    xf = AlignXf ( pos+(v*(d-(rad*3))), v )
    mol = CylinderMesh2 (rad*3, 0.01, rad*3, 10, clr, xf, mol )

    return mol


def AddAxes ( rad, lX, lY, lZ, cF, mol ) :

    pos = chimera.Vector(0,0,0)
    mol = AddArrow2 ( pos, chimera.Vector(1,0,0), lX, (cF,0,0,1), rad, mol )
    mol = AddArrow2 ( pos, chimera.Vector(0,1,0), lY, (0,cF,0,1), rad, mol )
    mol = AddArrow2 ( pos, chimera.Vector(0,0,1), lZ, (0,0,cF,1), rad, mol )
    mol.name = "XYZ (RGB) Axes"

    return mol



def AlignXf ( pos, v ) :
    Z = v
    Z.normalize()
    from random import random as rand
    dZ = chimera.Vector( rand(), rand(), rand() )
    dZ.normalize()
    X = chimera.cross ( Z, dZ )
    X.normalize ()
    Y = chimera.cross ( Z, X )
    Y.normalize ()

    xf = chimera.Xform.xform (
        X.x, Y.x, Z.x, pos[0],
        X.y, Y.y, Z.y, pos[1],
        X.z, Y.z, Z.z, pos[2] )

    #xf3 = chimera.Xform.xform (
    #    d, 0, 0, 0,
    #    0, d, 0, 0,
    #    0, 0, d, 0 )
    #print xf3

    return xf


def Quad2Tri ( vi ) :
    t1 = (vi[0], vi[1], vi[2])
    t2 = (vi[0], vi[2], vi[3])
    return t1, t2


def CylinderMesh2 (r1, r2, Length, div, color, xf, mol) :

    v = None
    vi = []

    # print "CylinderMesh:", div

    at = 0
    for psi_i in range(div) :

        psi = float(psi_i) * 360.0/float(div)

        #print "%.0f(%d)(%d)" % (psi, at, at-l),
        x1 = r1 * numpy.sin(psi * numpy.pi/180)
        y1 = r1 * numpy.cos(psi * numpy.pi/180)

        x2 = r2 * numpy.sin(psi * numpy.pi/180)
        y2 = r2 * numpy.cos(psi * numpy.pi/180)

        p = chimera.Point ( x1,y1,0 );
        if xf : p = xf.apply ( p )

        if psi_i == 0 :
            v = numpy.array( [ [p[0], p[1], p[2]], ], numpy.float32 )
        else :
            pt1 = numpy.array( [ [p[0], p[1], p[2]], ], numpy.float32 )
            v = numpy.concatenate ( [v, pt1] )

        p = chimera.Point ( x2,y2,Length );
        if xf : p = xf.apply ( p )
        pt2 = numpy.array( [ [p[0], p[1], p[2]], ], numpy.float32 )
        v = numpy.concatenate ( [v, pt2] )

        at = at + 2                

        if psi_i == 0 :
            pass
        else :
            tris = Quad2Tri ( [at-3, at-1, at-2, at-4] )
            vi.extend(tris)

        if psi_i == div-1 :
            tris = Quad2Tri ( [at-1, 1, 0, at-2] )
            vi.extend(tris)


    p = chimera.Point ( 0,0,0 );
    if xf : p = xf.apply ( p )
    pt1 = numpy.array( [ [p[0], p[1], p[2]], ], numpy.float32 )
    v = numpy.concatenate ( [v, pt1] )

    p = chimera.Point ( 0,0,Length );
    if xf : p = xf.apply ( p )
    pt1 = numpy.array( [ [p[0], p[1], p[2]], ], numpy.float32 )
    v = numpy.concatenate ( [v, pt1] )

    sph = mol.addPiece ( v, vi, color )
    return mol


def prAxes ( points ) :

    com = numpy.sum(points, axis=0) / len(points)
    C = chimera.Vector ( com[0], com[1], com[2] )

    comv = numpy.ones_like ( points ) * com
    points = points - comv

    i = numpy.matrix ( [[1,0,0], [0,1,0], [0,0,1]] )
    ii = i * numpy.sum ( numpy.multiply ( points, points ) )
    p_t = numpy.transpose(points)
    td = numpy.tensordot ( points, p_t, axes=[0,1] )

    I0 = ii - td

    try :
        U, S, V = numpy.linalg.svd( I0 )
    except :
        print "- error computing SVD - prob. singular matrix"
        return []

    #U[0,0] = U[0,0] * -1.0
    #U[1,0] = U[1,0] * -1.0
    #U[2,0] = U[2,0] * -1.0

    #U[0,2] = U[0,2] * -1.0
    #U[1,2] = U[1,2] * -1.0
    #U[2,2] = U[2,2] * -1.0

    return [C, U, S, V]




def AxesModOffset ( COM=[0,0,0], U=None, Extents=[30,30,30], rad=1.0, f=1.0,
			 alignTo = None ) :
	
    import _surface
    mol = _surface.SurfaceModel()
    chimera.openModels.add([mol], sameAs = alignTo)
	
    pos = chimera.Vector(0,0,0)
    axes = AddArrow2 ( pos, chimera.Vector(0,1,0), lY, (cF,.3,.3,1), rad, mol )
	
    axes.name = "Riboarrow"
	
    if U != None :
        R = numpy.array([
						 [  U[0,0], U[0,1], U[0,2], 0.0    ],
						 [  U[1,0], U[1,1], U[1,2], 0.0    ],
						 [  U[2,0], U[2,1], U[2,2], 0.0    ]  ] )
		
        T = numpy.array([
						 [  1.0, 0.0, 0.0, COM[0]   ],
						 [  0.0, 1.0, 0.0, COM[1]   ],
						 [  0.0, 0.0, 1.0, COM[2]   ]  ] )
		
        Ti = numpy.array([
						  [  1.0, 0.0, 0.0, Extents[0]*0.7  ],
						  [  0.0, 1.0, 0.0, -Extents[1]/2.0   ],
						  [  0.0, 0.0, 1.0, -Extents[0]*0.7   ]  ] )
		
        import Matrix
        M = Matrix.multiply_matrices ( R, Ti )
        M = Matrix.multiply_matrices ( T, M )
		
        ps = []
        for p in axes.surfacePieces :
            v, t = numpy.copy(p.geometry[0]), numpy.copy(p.geometry[1])
            ps.append ( [v,t,p.color] )
            axes.removePiece ( p )
		
        import _contour
        for p in ps :
            _contour.affine_transform_vertices( p[0], M )
            axes.addPiece ( p[0], p[1], p[2] )
		
        from random import random as rand
        clr = ( rand()*.7, rand()*.7, rand()*.7, 1.0 )
	# for p in axes.surfacePieces : p.color = clr
	
    #for g in axes.surfacePieces :
    #    g.initial_v = numpy.copy ( g.geometry[0] )
	
    return axes

