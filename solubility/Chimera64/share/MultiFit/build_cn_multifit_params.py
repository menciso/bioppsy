import sys,os
import IMP.multifit
from optparse import OptionParser
def usage():
    usage =  "usage %prog [options] <cyclic symmetry degree> <monomer PDB file> "
    usage += "<density map> <resolution> <spacing> <density threshold> <origin X> <origin Y> <origin Z>\n"
    usage+="A script that builds the parameters file for SymmMultiFit\n";
    usage+="Notice: If you have negative numbers as input, add -- as the first parameter, due to OptionParser ambiguaty in dealing with negative numbers\n"
    parser = OptionParser(usage)
    parser.add_option("-o", "--out", dest="out",default="multifit.output",
                      metavar="FILE",
                      help="the name of multifit output file. The default filename is multifit.output")
    parser.add_option("-i", "--med", dest="med",default="",
                      help="Print intermediate results.")
    parser.add_option("-p", "--params", dest="params",default="multifit.param",
                      help="the name of multifit params file. The default filename is multifit.params")
    parser.add_option("-m", "--model", dest="model",default="asmb.model",
                      help="the name of multifit solutions. The default filename is asmb.model.X.pdb")
    parser.add_option("-n", "--numsols", dest="numsols",default=10,
                      help="the number of fits to report")
    (options, args) = parser.parse_args()
    print len(args)
    print options
    print args
    if len(args) != 9:
        parser.error("incorrect number of arguments")
    return [options,args]

def get_files_data(monomer_fn):
    monomer_ms_fn=monomer_fn+".ms"
    msg="\n#    File Names:\n"
    msg+="monomer "+monomer_fn+"\n"
    msg+="monomer_ms "+monomer_ms_fn+"\n"
    msg+="log-file multifit.log\n"
    msg+="prot_lib "+ IMP.multifit.get_data_path("chem.lib")
    return msg

def get_symmetry_data(cn_units,dn_units):
    msg="\n#   Symmetry Parameters:\n";
    msg+="#    cn_symm_deg <Cyclic symmetry of (trimer=3, tetramer=4, etc)>\n";
    msg+="#    dn_symm_deg <Dihedral symmetry >\n";
    msg+="cn_symm_deg "+str(cn_units)+"\n";
    msg+="dn_symm_deg "+str(dn_units)+"\n";
    return msg

def get_scoring_data(liberal=True):
    msg="\n#    Scoring Parameters:\n";
    msg+="#        scoreParams <small_interfaces_ratio> <max_penetration> <ns_thr> <w1 w2 w3 w4 w5>\n"
    msg+="#    <small_interfaces_ratio> - the ratio of the low scoring transforms to be removed\n"
    msg+="#    <max_penetration> - maximal allowed penetration between molecules surfaces\n"
    msg+="#    <ns_thr> - normal score threshold\n"
    msg+="#    <w1 w2 w3 w4 w5> - scoring weights for ranges:\n"
    msg+="#                [-5.0,-3.6],[-3.6,-2.2],[-2.2,-1.0],[-1.0,1.0],[1.0-up]\n"
    if liberal:
        msg+="scoreParams 0.1 -10.0 0.5 -8 -4 0 1 0\n"
    else:
        msg+="scoreParams 0.1 -5.0 0.5 -8 -4 0 1 0\n"
    return msg

def get_density_data(density_map_fn,resolution,spacing,
                     threshold,pca_matching,origin):
    msg="\n#    Density Parameters:\n"
    msg +="# <density> -the density map in MRC format\n"
    msg +="# <density_resolution> -the resolution of the density map in A \n"
    msg +="# <density_spacing> -the voxel spacing of the density in A\n"
    msg +="# <density origin> - the origin of the map <x> <y> <z> \n"
    msg +="# <density_threshold> - the threshold of the density map, used for PCA matching \n"
    msg +="# <pca_matching_threshold> - corresponding principal components whose eigen values differ in less than pca_matching_threshold are considered to be a match \n"
    msg+="density "+density_map_fn +"\n"
    msg+="density_resolution "+str(resolution)+"\n"
    msg+="density_spacing "+str(spacing)+"\n"
    msg+="density_origin "+str(origin[0])+" " + str(origin[1]) +" " + str(origin[2])+"\n"
    msg+="density_threshold "+str(threshold)+"\n"
    msg+="pca_matching_threshold "+str(pca_matching_thr)+"\n"
    return msg

def get_log_data(intermediate_fn,output_fn,model_fn):
    msg  = "# log Parameters:\n"
    msg += "output "+output_fn+"\n"
    msg += "model "+model_fn+"\n"
    if intermediate_fn != "":
        msg += "intermediate "+intermediate_fn+"\n"
    return msg
def get_clustering_data():
    msg="#    Clustering Parameters:\n";
    msg +="#    clusterParams < axis_angle_thr DEGREES > < min_cluster_size > < distance_between_centers_of_mass >\n";
    msg += "clusterParams 18 1 2.0\n";
    return msg;

def get_base_data():
    msg = "\n#    Base Parameters:\n"
    msg += "#    baseParams <min_base_dist> <max_base_dist>\n"
    msg +="baseParams 5.0 50.0\n";
    return msg;

def get_grid_data():
    msg = "\n#    Grid Parameters:\n"
    msg+="#      grid <grid_step> <max_distance> <vol_func_radius>\n"
    msg += "grid 0.5 6.0 6.0\n"
    return msg

def get_surface_data():
    msg="\n#    Surface Parameters:\n";
    msg+="\n# surfacePruneThereshold - threshold fore surface pruning, i.e. no 2 points with distanca below this thr are left  for matching\n";
    msg+="surfacePruneThereshold 1.5\n";
    return msg;


def get_fitting_data(num_sols):
    msg="\n#    Fitting Parameters:\n";
    msg+="\n# num_sols - number of solutinos to fit \n"
    msg+="fitting "+str(num_sols)+"\n";
    return msg;

if __name__=="__main__":
    (options,args) = usage()
    cn_units = int(args[0])
    dn_units = 1

    liberal=False #TODO - make a parameter
    unit_pdb_fn = args[1]
    density_map_fn = args[2]
    resolution=float(args[3])
    spacing=args[4]
    threshold=args[5]
    origin=args[6:]
    if resolution>15:
        pca_matching_thr=15
    else:
        pca_matching_thr=resolution*0.75;
    params_fn=options.params
    intermediate_fn=""
    log_fn="multifit.log"
    output_fn=options.out
    model_fn=options.model
    f=open(params_fn,"w")
    f.write(get_files_data(unit_pdb_fn))
    f.write(get_symmetry_data(cn_units,dn_units))
    f.write(get_scoring_data(liberal))
    f.write(get_density_data(density_map_fn,resolution,spacing,threshold,pca_matching_thr,origin))
    f.write(get_log_data(intermediate_fn,output_fn,model_fn))
    f.write("#######   Advanced Parameters\n")
    f.write(get_clustering_data())
    f.write(get_base_data())
    f.write(get_grid_data())
    f.write(get_surface_data())
    f.write(get_fitting_data(options.numsols))
    f.close()
