import chimera
import os
from chimera.baseDialog import ModelessDialog
from chimera import UserError
from chimera import openModels

class RunAnchorGraph:
	# This is actually an abstract base class.
	# Derived class needs to run AnchorGraph and call _showAnchorGraph

	def __init__(self, message, tempPath, volumeName):
		self.message = message
		self.tempPath = tempPath
		self.volumeName = volumeName
	
	def _showAnchorGraph(self, output):
		openModels.open(output, temporary=True)
		
class RunAnchorGraphWS(RunAnchorGraph):
	
	def __init__(self, title, message=None, inputFileMap=None, command=None,
			sessionData=None, tempPath=None, volumeName=None):
		RunAnchorGraph.__init__(self, message, tempPath, volumeName)
		from WebServices import appWebService
		serviceName = "AnchorGraphWebService"
		kw = dict()
		kw["finishTest"] = self.volumeName + "_AnchorGraph.cmm"
		kw["progressCB"] = self._progressCBWeb
		if sessionData:
			kw["sessionData"] = sessionData
		else:
			kw["params"] = (serviceName, title, inputFileMap, command)
		self.ws = appWebService.AppWebService(self._wsFinish, **kw)
	
	def _wsFinish(self, opal, fileMap):
		data = opal.getURLContent(fileMap[self.volumeName+"_AnchorGraph.cmm"])
		pathOutput = os.path.join(self.tempPath, self.volumeName+"_AnchorGraph.cmm")
		foutput = open(pathOutput, 'w')
		lines = data.strip().split('\n')
		if len(lines) > 1:
			self.message('Calculating Anchor Graph on web service completed.')
		print >> foutput, '<marker_set name="%s_AnchorGraph">' % self.volumeName
		for l in lines[1:]:
			print >> foutput, l
		#foutput.write(data)
		foutput.close()
		self._showAnchorGraph(pathOutput)


	def _progressCBWeb(self, stdout):
		if stdout:
			fstdout = open(os.path.join(self.tempPath, 'stdout.txt'), 'w')
			fstdout.write(stdout)
			fstdout.close()
			count = 1.0
			for line in stdout.split('\n'):
				if line.startswith('*'):
					count += len(line)
			prog = count/53.0
			if prog > 1.0: prog = 0.99
			self.message("Calculating Anchor Graph on web service: %d%% completed." %(prog*100))
			return prog
		else:
			return 0.0


	

