# -----------------------------------------------------------------------------
# Dialog for MultiFit
#
REG_OPACITY = 0.45

import chimera
import os
import shutil
import Pmw
from chimera.baseDialog import ModelessDialog
from chimera import UserError

# -----------------------------------------------------------------------------
#
class MultiFit_Dialog(ModelessDialog):
	
	title = 'MultiFit'
	name = 'MultiFit'
	buttons = ('Fit', 'Options', 'Reset', 'Close')
	help = 'ContributedSoftware/multifit/multifit.html' 

	def fillInUI(self, parent):
		self.requested_halt = False
		self.pathTemp = "@_undefined_@"
		self.mapDefaultOpacity = 1.0

		import Tkinter
		from CGLtk import Hybrid
		from VolumeViewer import Volume_Menu
		from chimera.tkoptions import OutputFileOption, InputFileOption

		from chimera.preferences import addCategory, HiddenCategory
		self.prefs = addCategory("MultiFit", HiddenCategory,
				optDict = {'MSDOTS_PATH':'',
						'IMP_PATH':'',
						'TEMP_FOLDER':'',
						'NUMCOPY':'7',
						'OUTPUTMODELS':'10',
						'RESOLUTION':'10.0',
						'SAMPLEANGLE':'30',
						'MULTIFIT_USE_WEB':True,
						'SYMMETRY':True,
						})

		row = 0
		ff = Tkinter.Frame(parent)
		self.inputFrame = ff
		ff.grid(row = row, column = 0, sticky = 'news')
		parent.columnconfigure(0, weight=1)
		parent.rowconfigure(0, weight=1)
		ff.columnconfigure(0, weight=1)
		row += 1
		
		# Option group for symmetry or non-symmetry 
		# o ---- 0	
		symmetrygroup = []
		self.symmetry = Tkinter.IntVar(parent)
		self.symmetry.set(self.prefs['SYMMETRY'])
		self.symmetryPanel = Pmw.Group(ff, 
					tag_pyclass = Tkinter.Radiobutton,
					tag_text = 'Cyclic symmetry',
					tag_value = 1,
					tag_variable = self.symmetry)
		self.symmetryPanel.grid(row=row, column=0, columnspan=3, sticky='ew', padx=5)
		row += 1
		symmetrygroup.append(self.symmetryPanel)

		# o ---- 1
		self.nonSymmetryPanel = Pmw.Group(ff, 
					tag_pyclass = Tkinter.Radiobutton,
					tag_text = 'No symmetry',
					tag_value = 0,
					tag_variable = self.symmetry)
		self.nonSymmetryPanel.grid(row=row, column=0, columnspan=3, sticky='news', padx=5)
		self.nonSymmetryRow = row
		row += 1
		symmetrygroup.append(self.nonSymmetryPanel)
		Pmw.aligngrouptags(symmetrygroup)
		
		self.symmetry.trace("w", self._symtyCB)

		# cyclic symmetry panel
		sypf = self.symmetryPanel.interior()
		self.numCopyEntry = Pmw.Counter(sypf,
					labelpos = 'w',
					label_text = 'Fit ',
					entry_width = 2,
					datatype = 'integer',
					entryfield_value = self.prefs['NUMCOPY'],
					entryfield_validate = {'validator' : 'integer','min' : 1, 'max' : 99}
					)
		self.numCopyEntry.grid(row = 0, column = 0, sticky= 'w')
		
		from chimera.widgets import ModelOptionMenu
		om = ModelOptionMenu(sypf, labelpos = 'w', label_text = 'copies of ', 
					listFunc = fit_object_models,
					sortFunc = compare_fit_objects,
					command = self.object_chosen_cb)
		om.grid(row = 0, column = 1, sticky= 'w')
		self.object_menu = om

		fm = Volume_Menu(sypf, ' in map ')
		fm.frame.grid(row = 0, column = 2, sticky = 'w')
		self.map_menu = fm

		# non-symmetry panel 
		nspf = self.nonSymmetryPanel.interior()
		nsrow = 0
		nonSymMolTabLabel = Tkinter.Label(nspf, text = 'Choose models to fit: ')
		nonSymMolTabLabel.grid(row=nsrow, column=0, columnspan=2, padx=5, sticky="ws")
		nsrow += 1
		from chimera.widgets import MoleculeSortableTable, ModelSortableTable
		NonSymMolTabFilter = lambda m: not m.name.endswith('_AnchorGraph') 
		self.nonSymMolTab = MoleculeSortableTable(nspf, filtFunc=NonSymMolTabFilter, autoselect='all')
		self.nonSymMolTab.grid(row = nsrow, column = 0, columnspan=2,
					padx=5, sticky="news")
		nspf.rowconfigure(nsrow, weight=1)
		nsrow += 1
		self.nonSymMolTab.addColumn("ID", "id", format="%d ")
		self.nonSymMolTab.addColumn("c", "color", format=(True, True))
		self.nonSymMolTab.addColumn("A", "openState.active", format=bool)
		self.nonSymMolTab.addColumn("S", "display", format=bool)
		from chimera import Molecule
		Molecule._MultiFittingGlobally = property(getFittingGlobal, setFittingGlobal)
		self.nonSymMolTab.addColumn("Fit globally", "_MultiFittingGlobally", format=bool)
		self.nonSymMolTab.launch(selectMode="extended")
		self.nonSymMolTab.tixTable.hlist.configure(height=5)

		fmap = Volume_Menu(nspf, ' Fit in map ')
		fmap.frame.grid(row = nsrow, column = 0, sticky = 'wn')
		self.as_map_menu = fmap
		agbt = Tkinter.Button(nspf, text="Calculate anchor graph", command = self.AnchorGraph)
		agbt.grid(row = nsrow, column = 1, sticky='en')
		nsrow += 1

		nspf.columnconfigure(0, weight=1)
		nspf.columnconfigure(1, weight=1)


		# ||-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-||
		# Results table 
		#rt = Tkinter.Frame(parent)
		#rt.grid(row = row, column = 0, sticky = 'news')
		ResultTabLabel = Tkinter.Label(ff, text = 'Fitting results: ')
		ResultTabLabel.grid(row=row, column=0, columnspan=3, padx=5, sticky="news")
		row += 1
		from CGLtk.Table import SortableTable
		self.resultTable = SortableTable(ff)
		self.resultTable.grid(row=row, column=0, columnspan=3, 
					padx=5, sticky="news")
		ff.rowconfigure(row, weight=1)
		row += 1
		self.resultTable.addColumn("Index", "index", format="%d ", anchor="nw")
		self.resultTable.addColumn("c", "color", format=(True, True))
		self.resultTable.addColumn("Correlation", "cc", format="%.2f")
		self.resultTable.addColumn("Name", "AsbName", format="%s ", anchor="nw")
		self._initialResultTable()


		# Options groups
		op =  Hybrid.Popup_Panel(parent)
		opf = op.frame
		opf.grid(row = row, column = 0, sticky = 'news', padx=5, pady=5)
		opf.grid_remove()
		opf.columnconfigure(0, weight=1)
		self.options_panel = op.panel_shown_variable
		row += 1
		advRow = 0

		cb = op.make_close_button(opf)
		cb.grid(row = advRow, column = 1, sticky = 'ne')
		advRow += 1

		# Advanced Options panel
		#self.advancedOptionPanel = Pmw.Group(opf,
		#			tag_pyclass = Tkinter.Button,
		#			tag_text = 'Advanced Options')
		#self.advancedOptionPanel.configure(
		#		tag_command = self.advancedOptionPanel.toggle)
		#self.advancedOptionPanel.collapse()
		#radiogroups.append(self.advancedOptionPanel)
		#self.advancedOptionPanel.grid(row = row, column = 0, 
		#		columnspan=3, sticky='news', padx=5)
		#row += 1

		#advOptFrame = self.advancedOptionPanel.interior()
		advOptFrame = opf

		# Number of output models
		numOutputLabel = Tkinter.Label(advOptFrame, 
				text = ' Number of output assemblies:')
		numOutputLabel.grid(row=advRow, column=0, sticky = 'e')
		self.numOutputEntry = Pmw.EntryField(advOptFrame,
					value = self.prefs['OUTPUTMODELS'],
					validate = {'validator' : 'numeric',
						'min' : '1', 'max' : '1000',
						'minstrict' : 1, 'maxstrict' : 1},
						entry_width = 10)
		self.numOutputEntry.grid(row=advRow, column=1, sticky = 'w')
		advRow += 1
		# Resolution of the density map
		resoluLabel = Tkinter.Label(advOptFrame, 
				text = u' Resolution of the density map (\u00C5):')
		resoluLabel.grid(row=advRow, column=0, sticky = 'e')
		self.resoluEntry = Pmw.EntryField(advOptFrame,
					value = self.prefs['RESOLUTION'],
					validate = {'validator' : 'real',
						'min' : '0.0', 'max' : '1000.0',
						'minstrict' : 1, 'maxstrict' : 1},
						entry_width = 10)
		self.resoluEntry.grid(row=advRow, column=1, sticky = 'w')
		advRow += 1
		# Coarse-graining Level
		self.sampleAngleLabel = Tkinter.Label(advOptFrame, 
				text = u' Sampling angle (15 to 270\u00B0):')
		self.sampleAngleLabel.grid(row=advRow, column=0, sticky = 'e')
		self.sampleAngleEntry = Pmw.EntryField(advOptFrame,
					value = self.prefs['SAMPLEANGLE'],
					validate = {'validator' : 'numeric',
						'min' : '15', 'max' : '270',
						'minstrict' : 0, 'maxstrict' : 1},
					entry_width = 10)
		self.sampleAngleEntry.grid(row=advRow, column=1, sticky = 'w')
		balloonText = "Angle delta for FFT rotational search " \
				u"(between 15 to 270\u00B0 degree, default is 30\u00B0)"
		Pmw.Balloon(advOptFrame).bind(self.sampleAngleEntry, balloonText)
		advRow += 1

		# Temp folder:
		balloonText = "If specify the output files location, the calculation output files \n" \
					"and temp PDB/map files will be stored there."
		self.pathTempOpt = OutputFileOption(advOptFrame, advRow,
							' Output file location (optional)', 
							self.prefs['TEMP_FOLDER'], None,
							balloon = balloonText, dirsOnly=True)
		advRow += 1

		
		# MultiFit Location Choice
		self.runOnWeb = Tkinter.IntVar(parent)
		self.runOnWeb.set(self.prefs['MULTIFIT_USE_WEB'])
		self.runOnWeb.trace("w", self._runningLocCB)
		self.runOnWebPanel = Pmw.Group(advOptFrame,
					tag_pyclass = Tkinter.Radiobutton,
					tag_text = 'Run MultiFit via web service',
					tag_value = 1,
					tag_variable = self.runOnWeb)
		#self.runOnWebPanel.grid(row=advRow, column=0, columnspan=2, sticky='ew', padx=5)
		#TODO To include the run on local option. uncomment the above line.
		advRow += 1

		# place MultiFit run-on-web options here
		#
		#
		radiogroups = []
		radiogroups.append(self.runOnWebPanel)

		# Run MultiFit in Expert Mode locally
		self.runOnLocalPanel = Pmw.Group(advOptFrame,
					tag_pyclass = Tkinter.Radiobutton,
					tag_text = 'Run MultiFit locally (expert mode)',
					tag_value = 0,
					tag_variable = self.runOnWeb)
		#self.runOnLocalPanel.grid(row = advRow, column = 0, columnspan=2, sticky='ew', padx=5)
		#TODO To include the run on local option. uncomment the above line.
		advRow += 1
		
		# place MultiFit run-on-local options here
		rolf = self.runOnLocalPanel.interior()
		orow = 0
		# msdots location:
		balloonText = "Please specify the location of msdots binary file."
		self.msdotsPath = InputFileOption(rolf, 0,
					'Location of msdots',
					self.prefs['MSDOTS_PATH'], None,
					balloon = balloonText)
		
		# IMP location:
		balloonText = "If you have IMP compiled, please specify the location of IMP root folder."
		self.impPathOpt = InputFileOption(rolf, 1,
					'Location of IMP root folder',
					self.prefs['IMP_PATH'], None,
					balloon = balloonText, dirsOnly=True)
		
		self.runOnLocalPanel.interior().columnconfigure(0, weight=0)
		self.runOnLocalPanel.interior().columnconfigure(1, weight=1)

		radiogroups.append(self.runOnLocalPanel)
		#
		orow += 1
		Pmw.aligngrouptags(radiogroups)

		# Citation
		from CGLtk.Citation import Citation
		Citation(advOptFrame, 
					"K. Lasker, M. Topf, A. Sali, H. Wolfson. Inferential optimization for\n"\
					"simultaneous fitting of multiple components into a cryoEM map of\n"\
					"their assembly. Journal of Molecular Biology 388, 180-194, 2009.",
					prefix= "Using MultiFit please cite:",
					url='http://www.ncbi.nlm.nih.gov/pubmed/19233204' ).grid(
					row = advRow, column = 0, columnspan=2, padx=5, pady=5)
		advRow += 1

		advOptFrame.columnconfigure(0, weight=0)
		advOptFrame.columnconfigure(1, weight=1)


		self._runningLocCB()
		self._symtyCB()
		self.options_panel.set(False)

		# Specify a label width so dialog is not resized for long messages.
		msg = Tkinter.Label(parent, width = 40, anchor = 'w', justify = 'left')
		msg.grid(row = row, column = 0, sticky = 'ew')
		row += 1
		self.message_label = msg
	
	def _symtyCB(self, *args):
		if self.symmetry.get():
			self.symmetryPanel.expand()
			self.nonSymmetryPanel.collapse()
			self.sampleAngleLabel.config(state='disabled')
			self.sampleAngleEntry.component('entry').config(state="disabled")
			self.prefs['SYMMETRY']=True
			self.inputFrame.rowconfigure(self.nonSymmetryRow, weight=0)
		else:
			self.nonSymmetryPanel.expand()
			self.symmetryPanel.collapse()
			self.sampleAngleLabel.config(state='normal')
			self.sampleAngleEntry.component('entry').config(state="normal")
			self.prefs['SYMMETRY']=False
			self.inputFrame.rowconfigure(self.nonSymmetryRow, weight=1)
		return

	def _runningLocCB(self, *args):
		"""
		Change the MultiFit running location call back function to disable some UI
		"""
		if self.runOnWeb.get():
			self.runOnWebPanel.expand()
			self.runOnLocalPanel.collapse()
		else:
			self.runOnWebPanel.collapse()
			self.runOnLocalPanel.expand()
		return


	def object_chosen_cb(self, obj_name):
		return # TODO

	def message(self, text):
		"""
		update the message bar
		"""
		self.message_label['text'] = text
		self.message_label.update_idletasks()

	def check_fit_atoms(self):
		"""
		check atoms chosen in dialog for fitting. return bool
		"""
		if self.symmetry.get():
			m = self.object_menu.getvalue()
			if m == 'selected atoms':
				from chimera import selection
				if len(selection.currentAtoms()) > 0:
					return True
				else:
					self.message('Choose a moleucule or select atoms to fit.')
					raise UserError('Choose a moleucule or select atoms to fit.')
			else:		
				from chimera import Molecule
				if isinstance(m, Molecule):
					return True
				else:
					self.message('The chosen molecule is not exist.')
		else:
			mols = []
			for m in self.nonSymMolTab.selected():
				from chimera import Molecule
				if isinstance(m, Molecule):
					mols.append(m)
			if len(mols) > 0:
				return True
			else:
				self.message('Select at least one molecule.')
				raise UserError('Select at least one molecule.')
		return False


	def check_fit_map(self):
		"""
		check map chosen to fit into base map. return bool
		"""
		if self.symmetry.get(): 
			vmanu = self.map_menu
		else:
			vmanu = self.as_map_menu
		from VolumeViewer import Volume
		if isinstance(vmanu.volume(), Volume):
			return True
		else:
			self.message('No volume map is selected!')
			raise UserError('No volume map is selected!')
			return False


	def AnchorGraph(self):
		# prepare the input file map for the web service
		agFileMap = {}
		# Temp dir
		tempPath = self._pathTempCheck()
		# Writing out the maps
		self.volumeMap = self.as_map_menu.volume()
		if self.volumeMap == None:
			self.message('No volume map is selected!')
			raise UserError('No volume map is selected!')
			return
		if len(self.nonSymMolTab.selected()) == 0:
			self.message('Select at least one molecule.')
			raise UserError('Select at least one molecule.')
			return
		self.message('Writing out the maps ...')
		volumeName = self.volumeMap.name.replace('.mrc','').replace('.MRC','')
		volumeName = volumeName.replace(' ','_').replace('+','_')
		self.volumePath = os.path.join(tempPath, volumeName+'.mrc')
		self.volumeMap.write_file(self.volumePath, format = 'mrc',
								temporary=True)
		agFileMap[volumeName+'.mrc'] = (self.volumePath, "rb")
		# close previous generated anchor graph
		mlist = chimera.openModels.list()
		for m in mlist:
			if m.name == volumeName + '_AnchorGraph':
				chimera.openModels.close([m])
		# Config file
		configName = 'AnchorGraphConfig.xml'
		pathConfig = os.path.join(tempPath, configName)
		fconfig = open( pathConfig, 'w')
		print >> fconfig, '<?xml version="1.0" encoding="UTF-8"?>'
		print >> fconfig, '<AnchorGraphConfig>'
		print >> fconfig, '\t<version>1</version>'
		print >> fconfig, '\t<numSubunit>%d</numSubunit>' % len(self.nonSymMolTab.selected())
		print >> fconfig, '\t<volumeFileName>%s.mrc</volumeFileName>' % volumeName
		print >> fconfig, '\t<level>%s</level>' % self.as_map_menu.volume().surface_levels[0]
		print >> fconfig, '\t<origin>%f %f %f</origin>' % self.as_map_menu.volume().data.origin
		print >> fconfig, '</AnchorGraphConfig>'
		fconfig.close()
		agFileMap[configName] = pathConfig
		# testing 
		#for m in self.nonSymMolTab.data:
		#	print "%s\t%r" %(m.name, m._MultiFittingGlobally)
		# calculate the anchor graph
		if self.runOnWeb.get():
			# run on web
			self.prefs['MULTIFIT_USE_WEB'] = True
			title = "Anchor Graph calculation for: %s" % volumeName
			from runAnchorGraph import RunAnchorGraphWS
			self.message('Calculating Anchor Graph on web service, check the Task Panel for status...')
			RunAnchorGraphWS(title, message = self.message, \
					inputFileMap = agFileMap, \
					command = configName, \
					tempPath = tempPath, \
					volumeName = volumeName)
		else:
			# run on local
			#TODO
			return
		r, g, b, a = self.volumeMap.surface_colors[0]
		self.volumeMap.set_parameters(surface_colors=[(r,g,b,REG_OPACITY)])
		self.volumeMap.show()


	def Fit(self):
		if self.check_fit_map() and self.check_fit_atoms():
			# Save prefs
			self.prefs['NUMCOPY'] = self.numCopyEntry.get()
			numOutput = self.numOutputEntry.getvalue()
			self.prefs['OUTPUTMODELS'] = numOutput
			resolution = self.resoluEntry.get().strip()
			self.prefs['RESOLUTION'] = resolution
			self.prefs['SAMPLEANGLE'] = self.sampleAngleEntry.get().strip()
			#
			# Construct input file map
			inputFileMap = self._inputFilePrep()
			# create a MolStack object which hold copies of the self.fitMols for after calculation
			# display
			from runMultiFit import MolsStack
			self.molStack = MolsStack(self.fitMols, self.message)
			# pre-calculate transformation invariant
			from Matrix import xform_matrix, chimera_xform, multiply_matrices
			mv = xform_matrix(self.volumeMap.openState.xform.inverse())
			transInvariant = []
			for m in self.fitMols:
				mm = xform_matrix(m.openState.xform)
				transInvariant.append(multiply_matrices(mv, mm))
			self.runOnWeb.set(True) # TODO force run on web
			if self.runOnWeb.get():
				# run on web
				self.prefs['MULTIFIT_USE_WEB'] = True
				from runMultiFit import RunMultiFitWS
				self.message('Running MultiFit on web service, check the Task Panel for status...')
				RunMultiFitWS(
							self.jobName, 
							self.message,  
							inputFileMap,
							self.symmetry.get(),
							self.molStack,
							transInvariant,
							self.resultTable,
							volumeMap = self.volumeMap )
			else:
				# run on local
				if which(self.msdotsPath.get()) is None or self.msdotsPath.get() == '':
					self.message('Binary file msdots is not found or not executable!')
					raise UserError('Binary file msdots is not found or not executable!')
					return
				if self.impPathOpt.get() is None or not os.path.exists(self.impPathOpt.get()):
					self.message('Error! IMP folder is not located!')
					raise UserError('IMP folder is not located!')
					return
				msdotsPath = self.msdotsPath.get()
				self.prefs['MSDOTS_PATH'] = msdotsPath
				impPath = self.impPathOpt.get()
				imppyPath = os.path.join(impPath, 'tools/imppy.sh')
				if not os.path.exists(imppyPath):
					self.message('Error! $IMP/tools/imppy.sh is not found! Please check the IMP root folder.')
					raise UserError('$IMP/tools/imppy.sh is not found! please check the IMP root folder.')
					return
				cnMultiFitPath = os.path.join(impPath, 'modules/cn_multifit/bin/cn_multifit_main')
				if not os.path.exists(cnMultiFitPath):
					self.message('Error! $IMP/modules/cn_multifit/bin/cn_multifit_main is not found! please check the IMP root folder.')
					raise UserError('$IMP//modules/cn_multifit/bin/cn_multifit_main is not found! Please check the IMP root folder.')
					return
				self.prefs['MULTIFIT_USE_WEB'] = False
				impPath = self.impPathOpt.get()
				self.prefs['IMP_PATH'] = impPath

				from runMultiFit import RunMultiFitLocal
				RunMultiFitLocal( message = self.message, 
								msdotsPath = msdotsPath, 
								impPath = impPath,
								volumePath = self.volumePath, 
								volumeMap = self.volumeMap, 
								resolution = resolution, 
								pdbPath = self.pdbPath, 
								fitMols = self.fitMols, 
								symmetry = self.symmetry.get(),
								resultTable = self.resultTable)


	def Options(self):
		self.options_panel.set(not self.options_panel.get())
		#self.advancedOptionPanel.expand()
	
	# show the Rely Dialog
	def Reset(self):
		# TODO link close session trigger to call this function
		# clean up anchor graph
		mlist = chimera.openModels.list()
		for m in mlist:
			if m.name.endswith('_AnchorGraph'):
				chimera.openModels.close([m])
		# Temp dir
		tempPath = self._pathTempCheck()
		# clean up the tempPath
		self._pathTempClean(tempPath) 
		# clean up the results table
		if len(self.resultTable.data) > 0 and hasattr(self, 'fitMols'):
			for r in self.resultTable.data:
				for fm in self.fitMols:
					if fm not in r.molList and not fm.__destroyed__:
						r.hide()
		self.resultTable.data = []
		self.resultTable.refresh()
		# reset the fitMols
		if hasattr(self, 'fitMols'): 
			for fm in self.fitMols:
				if not fm.__destroyed__ and not fm.display:
					fm.display = True 
		#self.nonSymMolTab.refresh()
		if hasattr(self, 'volumeMap') and not self.volumeMap.__destroyed__:
			r, g, b, a = self.volumeMap.surface_colors[0]
			self.volumeMap.set_parameters(surface_colors=[(r,g,b,self.mapDefaultOpacity)])
			self.volumeMap.show()
		if hasattr(self, 'molStack'):
			self.molStack.adjust(0)

	def _inputFilePrep(self):
		"""
		prepare the input files, create a temp folder and copy the input files into the temp folder
		return a inputFileMap {fileName: pathToFile}
		"""
		# Temp dir
		tempPath = self._pathTempCheck()
		# clean up the tempPath
		self._pathTempClean(tempPath) 
		inputFileMap = {}
		#
		# Writing out the maps
		if self.symmetry.get(): 
			vmanu = self.map_menu
		else:
			vmanu = self.as_map_menu
		self.volumeMap = vmanu.volume()
		self.message('Writing out the volume maps: %s' % self.volumeMap.name)
		self.mapDefaultOpacity = self.volumeMap.surface_colors[0][3]
		volumeName = self.volumeMap.name.replace('.mrc','').replace('.MRC','')
		volumeName = volumeName.replace(' ','_').replace('+','_')
		if len(volumeName) == 0 :
			volumeName = "NoNameVolume"
		self.volumePath = os.path.join(tempPath, volumeName+'.mrc')
		self.volumeMap.write_file(self.volumePath, format = 'mrc',
								temporary=True)
		inputFileMap[volumeName+'.mrc'] = (self.volumePath, "rb")
		#
		
		#
		# Writing out the selected atoms
		inputPDBsListFileName = 'InputPDBsList.dat'
		pathInputPDBsList = os.path.join(tempPath, inputPDBsListFileName)
		fInputPDBsList = open( pathInputPDBsList, 'w')
		inputFileMap[inputPDBsListFileName] = pathInputPDBsList
		self.fitMols = []
		if self.symmetry.get():
			m = self.object_menu.getvalue()
			selected_only = False
			from chimera import Molecule, openModels, selection
			from Combine import combine
			if isinstance(m, Molecule):
				self.message('Writing out the selected model: %s' % m.name)
				pdbName = m.name.replace('.pdb','').replace('.PDB','')
				pdbName = pdbName.replace(' ','_').replace('+','_')
				self.fitMols.append(m)
			elif m == 'selected atoms':
				self.message('Writing out the selected atoms ...')
				mlist = list(set([a.molecule for a in selection.currentAtoms()]))
				atomMap, combined = combine(mlist, mlist[0], returnMapping = True)
				satoms = selection.currentAtoms(asDict = True)
				datoms = [ac for a, ac in atomMap.items() if not a in satoms]
				Midas.deleteAtomsBonds(datoms)
				self.fitMols.append(combined)
				selected_only = True
				pdbName = 'selected_atoms'
			elif m == 'all molecules':
				self.message('Writing out all molecules...')
				mlist = openModels.list(modelTypes = [Molecule])
				combined = combine(mlist, mlist[0])
				self.fitMols.append(combined)
				pdbName = 'all_molecules'
			#
			pdbPath = os.path.join(tempPath, pdbName+'.pdb')
			import Midas
			Midas.write(self.fitMols, self.volumeMap, pdbPath,
							selOnly=selected_only,
							temporary=True)
			# To keep fitMols as same as the asym case, the fitMols[0] is muliplicated according to
			# mumCopy
			for i in range(int(self.numCopyEntry.get())-1):
				self.fitMols.append(self.fitMols[0])
			inputFileMap[pdbName+'.pdb'] = pdbPath
			print >> fInputPDBsList, '%s\t%s\t1' %(pdbName, pdbName+'.pdb')
			self.jobName = (pdbName, volumeName)
		else:
			for m in self.nonSymMolTab.selected():
				self.message('Writing out the selected model: %s' % m.name)
				self.fitMols.append(m)
				pdbName = m.name.replace('.pdb','').replace('.PDB','')
				pdbName = pdbName.replace(' ','_').replace('+','_')
				pdbPath = os.path.join(tempPath, pdbName+'.pdb')
				import Midas
				Midas.write([m], self.volumeMap, pdbPath,
							temporary=True)
				inputFileMap[pdbName+'.pdb'] = pdbPath
				print >> fInputPDBsList, '%s\t%s\t%s' \
						% (pdbName, pdbName+'.pdb', ("0","1")[m._MultiFittingGlobally])
			self.jobName = (self.nonSymMolTab.selected()[0].name+'...', volumeName)
		#
		fInputPDBsList.close()
		#
		# write out the vdw.lib
		pkgdir = os.path.dirname(__file__)
		vdwPathPkg = os.path.join(pkgdir, 'vdw.lib')
		vdwPathTmp = os.path.join(tempPath, 'vdw.lib')
		if not os.path.exists(vdwPathTmp):
			if os.path.normpath(vdwPathPkg) != os.path.normpath(vdwPathTmp):
				shutil.copy(vdwPathPkg, tempPath)
		inputFileMap['vdw.lib'] = vdwPathTmp
		# write out the chem.lib
		chemPathPkg = os.path.join(pkgdir, 'chem.lib')
		chemPathTmp = os.path.join(tempPath, 'chem.lib')
		if not os.path.exists(chemPathTmp):
			if os.path.normpath(chemPathPkg) != os.path.normpath(chemPathTmp):
				shutil.copy(chemPathPkg, tempPath)
		inputFileMap['chem.lib'] = chemPathTmp
		#
		# Write out a config file for the web service
		# Remember to bump version when changing XML contents
		voxelSize = sum(self.volumeMap.data.step)/len(self.volumeMap.data.step)
		origin = self.volumeMap.data.origin
		configName = 'MultiFitConfig.xml'
		pathConfig = os.path.join(tempPath, configName)
		fconfig = open( pathConfig, 'w')
		print >> fconfig, '<?xml version="1.0" encoding="UTF-8"?>'
		print >> fconfig, '<multifit>'
		print >> fconfig, '\t<version>1</version>'
		print >> fconfig, '\t<inputPDBsListFileName>%s</inputPDBsListFileName>' % inputPDBsListFileName
		print >> fconfig, '\t<cnSymmetry>%s</cnSymmetry>' %("0","1")[self.symmetry.get()]
		#print >> fconfig, '\t<pdbFileName>%s.pdb</pdbFileName>' % pdbName 
		print >> fconfig, '\t<volumeFileName>%s.mrc</volumeFileName>' % volumeName 
		print >> fconfig, '\t<numCopy>%s</numCopy>' % self.numCopyEntry.get()
		print >> fconfig, '\t<numOutput>%s</numOutput>' % self.numOutputEntry.getvalue()
		print >> fconfig, '\t<resolution>%s</resolution>' % self.resoluEntry.get().strip()
		print >> fconfig, '\t<sampleAngle>%s</sampleAngle>' % self.sampleAngleEntry.get().strip()
		print >> fconfig, '\t<voxelSize>%f</voxelSize>' % voxelSize
		print >> fconfig, '\t<level>%s</level>' % self.volumeMap.surface_levels[0]
		print >> fconfig, '\t<origin>%f %f %f</origin>' % origin
		print >> fconfig, '</multifit>'
		fconfig.close()
		inputFileMap[configName] = pathConfig
		#
		#pkgdir = os.path.dirname(__file__)
		#fbpara = 'build_cn_multifit_params.py'
		#inputFileMap[fbpara] = os.path.join(pkgdir, fbpara)
		return inputFileMap


	def _initialResultTable(self):
		"""
		initialize the results table
		"""
		self.resultsList = []
		#if self.resultTable.data is None: #inital
		self.resultTable.setData(self.resultsList)
		self.resultTable.launch(browseCmd=self._resultTableBrowseCB, selectMode="extended")
		self.resultTable.tixTable.hlist.configure(height=5)
		return

	def _resultTableBrowseCB(self, selectedItems):
		"""
		results table browse call back function	
		"""
		# TODO separate from GUI, since this function is called after running or session restore
		if len(selectedItems) > 0:
			r, g, b, a = self.volumeMap.surface_colors[0]
			self.volumeMap.set_parameters(surface_colors=[(r,g,b,REG_OPACITY)])
			self.volumeMap.show()
		else:
			r, g, b, a = self.volumeMap.surface_colors[0]
			self.volumeMap.set_parameters(surface_colors=[(r,g,b,self.mapDefaultOpacity)])
			self.volumeMap.show()
		# complicated algorithm to keep the molStack have at least 1 copies of fitMol 
		nc = int(self.numCopyEntry.get()) 
		currPool = self.molStack.size()
		totalmol = self.molStack.size()
		for result in self.resultTable.data:
			if result.shown:
				totalmol += 1
		needsize = len(selectedItems) 
		if needsize > totalmol and totalmol > currPool:
			if len(selectedItems) <= 1:
				self.molStack.adjust(1)
				self.message('control + click to show multiple result models.')
			else:
				self.molStack.adjust(currPool+needsize-totalmol)
		elif needsize + 1 < totalmol and totalmol > currPool:
			self.molStack.adjust(0)
		# show or hide accordint to selection
		for result in self.resultTable.data:
			if result.shown and result not in selectedItems:
				result.hide()
		for result in self.resultTable.data:
			if not result.shown and result in selectedItems:
				result.show()
		

		return

	def _pathTempCheck(self):
		"""
		check the existance of the temp dir. if it doese not exist, create one.
		"""
		if self.pathTemp == "@_undefined_@" : # first time using
			if os.path.exists(self.pathTempOpt.get()) :
				self.pathTemp = self.pathTempOpt.get()
			else:
				self.pathTemp = self._pathTempCreate()
		else : # not first time using
			if self.pathTemp != self.pathTempOpt.get():
				self.pathTemp = self.pathTempOpt.get()
			if self.pathTemp == "" or not os.path.exists(self.pathTemp):
				self.pathTemp = self._pathTempCreate()

		if os.path.exists(self.pathTempOpt.get()) \
				or self.pathTempOpt.get() == "" : # save the customised temp path
			self.prefs['TEMP_FOLDER'] = self.pathTempOpt.get()
		return self.pathTemp


	def _pathTempCreate(self): 
		"""
		create a temp dir, which will be removed after closing Chimera
		"""
		from OpenSave import osTemporaryFile
		tempFile = osTemporaryFile(suffix=".tmp")
		tempDir = os.path.dirname(tempFile)
		os.remove(tempFile)
		return tempDir

	def _pathTempClean(self, tempPath):
		"""
		clean up the tempPath, remove files from previous run
		"""
		if hasattr(self, 'volumePath') and os.path.exists(self.volumePath):
			os.remove(self.volumePath)
		if hasattr(self, 'pdbPath') and os.path.exists(self.pdbPath):
			os.remove(self.pdbPath)
		rmList = ['BEFORE', 'chem.lib', 'CONTACT', 'multifit.param', \
				'out', 'REENTRANT', 'vdw.lib', 'multifit.output', \
				'multifit.output.symm.ref', 'intermediate_asmb_sols.out',\
				'stdout.txt', 'MultiFitConfig.xml', 'multifit.chimera.output',\
				'AnchorGraphConfig.xml', 'InputPDBsList.dat']
		fileList = os.listdir(tempPath)
		prefix = '...'
		for f in fileList:
			if f in rmList:
				os.remove(os.path.join(tempPath, f))
			elif f.endswith(".pdb.ms"):
				prefix = f.rstrip(".pdb.ms")
		fileList = os.listdir(tempPath)
		for f in fileList:
			if f == prefix + '.pdb.ms' or \
					(f.startswith(prefix) and f.endswith(".pdb")) or\
					f.endswith('.mrc') or \
					f.endswith('_AnchorGraph.cmm') or \
					(f.startswith('asmb_model') and f.endswith(".pdb")):
				os.remove(os.path.join(tempPath, f))

	def chosenMolecule(self):
		"""
		based on the self.object_menu return: 
		the chosen molecules (list), selected_only or not, name to save
		"""
		m = self.object_menu.getvalue()
		from chimera import Molecule, openModels, selection
		from Combine import combine
		import Midas
		if isinstance(m, Molecule):
			fname = m.name.replace('.pdb','').replace('.PDB','')
			fname = fname.replace(' ','_').replace('+','_')
			return m, False, fname
		elif m == 'selected atoms':
			mlist = list(set([a.molecule for a in selection.currentAtoms()]))
			atomMap, combined = combine(mlist, mlist[0], returnMapping = True)
			satoms = selection.currentAtoms(asDict = True)
			datoms = [ac for a, ac in atomMap.items() if not a in satoms]
			Midas.deleteAtomsBonds(datoms)
			return combined, True, 'selected_atoms'
		elif m == 'all molecules':
			mlist = openModels.list(modelTypes = [Molecule])
			combined = combine(mlist, mlist[0])
			return combined, False, 'all_molecules'
		return None, False, ''
# -----------------------------------------------------------------------------
def fit_object_models():
	"""
	Define a list for the fit object drop down manu
	"""
	from VolumeViewer import Volume
	from chimera import openModels as om, Molecule
	#mlist = om.list(modelTypes = [Molecule, Volume])
	mlist = om.list(modelTypes = [Molecule])
	folist = mlist + ['selected atoms']
	return folist
# -----------------------------------------------------------------------------
def compare_fit_objects(a, b):
	"""
	Put 'selected atoms' last, then all molecules, then all volumes.
	"""
	if a == 'selected atoms':
		return 1 
	if b == 'selected atoms':
		return -1
	from VolumeViewer import Volume
	from chimera import Molecule
	if isinstance(a,Molecule) and isinstance(b,Volume):
		return -1
	if isinstance(a,Volume) and isinstance(b,Molecule):
		return 1
	return cmp((a.id, a.subid), (b.id, b.subid))

# -----------------------------------------------------------------------------
def multiFit_dialog(create = False):

	from chimera import dialogs
	return dialogs.find(MultiFit_Dialog.name, create=create)

# -----------------------------------------------------------------------------
def show_multiFit_dialog():

	from chimera import dialogs
	return dialogs.display(MultiFit_Dialog.name)

# -----------------------------------------------------------------------------
from chimera import dialogs
dialogs.register(MultiFit_Dialog.name, MultiFit_Dialog, replace = True)

def which(program):
	"""
	wchich program mimics the behavior of the UNIX 'which' command
	"""
	import os
	def is_exe(fpath):
		return os.path.exists(fpath) and os.access(fpath, os.X_OK)

	fpath, fname = os.path.split(program)
	if fpath:
		if is_exe(program):
			return program
	else:
		for path in os.environ["PATH"].split(os.pathsep):
			exe_file = os.path.join(path, program)
			if is_exe(exe_file):
				return exe_file

	return None



def getFittingGlobal(mol):
	return getattr(mol, "_FittingGlobally", False)

def setFittingGlobal(mol, glob):
	mol._FittingGlobally = glob
