<!--
--- UCSF Chimera Copyright ---
Copyright (c) 2011-2012 Regents of the University of California.
All rights reserved.  This software provided pursuant to a
license agreement containing restrictions on its disclosure,
duplication and use.  This notice must be embedded in or
attached to all copies, including partial copies, of the
software or any revisions or derivations thereof.
--- UCSF Chimera Copyright ---
-->

<html><head>
<title>Image Tutorial: Surface Properties</title>
</head><body>
<font size="-1">
<a href="../frametut.html" target="_top">Chimera Tutorials Index</a>
</font>

<h2>Image Tutorial: Surface Properties</h2>

<p>
This tutorial describes generating high-quality images of a protein surface
colored to show <a href="#hydrophobicity">hydrophobicity</a> and 
<a href="#esp">electrostatic potential</a>.
Note there are several routes to the same or similar results. See also:
<a href="../menu.html#menupresets">presets</a>,
<a href="../print.html#tips">tips on preparing images</a>
</p><p>
<a href="../startup.html">Start Chimera</a> and
show the <a href="../chimerawindow.html#emulator"><b>Command Line</b></a>
(for example, with <b>Favorites... Command Line</b>).
Fetch Protein Data Bank entry
<a href="http://www.rcsb.org/pdb/cgi/explore.cgi?pdbId=3EEB"
target="_blank">3eeb</a>: 
<blockquote>
<i>Command</i>: <b><a href="../midas/open.html">open</a> 3eeb</b>
</blockquote>
The structure is a toxin that is activated by binding 
inositol hexakisphosphate (IHP), a highly negatively charged small molecule.
As might be expected, the binding site is polar and lined with many positively
charged groups. This can be illustrated by coloring the surface to
show its properties.
</p><p>
<a href="getting_started.html#moving">Move and scale</a> the structure 
as you wish throughout the tutorial.
Delete one of the copies of the structure, chain A, and solvent:
<blockquote>
<i>Command</i>: <b><a href="../midas/delete.html">delete</a> :.a</b>
<br>
<i>Command</i>: <b><a href="../midas/delete.html">del</a> solvent</b>
</blockquote>
Resize the window as desired, either by dragging its lower right corner 
with the mouse or by using the command
<a href="../midas/windowsize.html"><b>windowsize</b></a>. 
The window dimensions define the aspect ratio (width:height)
of output images, but image resolution (pixel dimensions)
can be specified independently when an image is saved.
</p>

<table align="right" style="margin:8px 8px 8px 8px" 
cellpadding="0" cellspacing="0">
<tr>
<td><a href="3eebB-hydrophob.png"><img src="3eebB-hydrophob.png" width="250"
height="250" title="click for full-sized image..." 
alt="hydrophobicity coloring"></a></td>
</tr>
</table>

<h3><a name="hydrophobicity">Amino Acid Hydrophobicity</a></h3>
<p>
Use the &ldquo;hydrophobicity surface&rdquo; preset to display
the molecular surface colored by amino acid hydrophobicity:
<blockquote>
<i>Command</i>: <b><a href="../midas/preset.html">preset</a> apply int 3</b>
</blockquote>
Colors are mapped to <a href="../midas/hydrophob.html">Kyte-Doolittle 
hydrophobicity</a>: from <b>dodger blue</b> for the most hydrophilic, to 
<b>white</b>, to <b>orange red</b> for the most hydrophobic.
The IHP molecule looks somewhat like a distorted snowflake.
The IHP-binding pocket is primarily blue, indicating its hydrophilic character.
However, as shown in the figure, we will use different colors 
than in the preset.
</p><p>
A nearby sodium ion and the heteroatoms in IHP are
<a href="../colortables.html#byelement">colored by element</a>.
Hide the ion, make the sticks fatter, and make ligand carbons yellow:
<blockquote>
<i>Command</i>: <b><a href="../midas/display.html">~disp</a> ions</b>
<br>
<i>Command</i>: <b><a href="../midas/setattr.html">setattr</a> m stickScale 2</b>
<br>
<i>Command</i>: <b><a href="../midas/color.html">color</a> yellow ligand & C</b>
</blockquote>
Different colors can be mapped to this <b>kdHydrophobicity</b> attribute 
of residues using the command
<b><a href="../midas/rangecolor.html">rangecolor</a></b>:
<blockquote>
<i>Command</i>: <b><a href="../midas/rangecolor.html">rangecol</a> kdHydrophobicity min medium purple 0 white max tan</b>
</blockquote>
Now the most polar residues are <b>medium purple</b> and the most hydrophobic
are <b>tan</b>.  
(The same thing can be done in the 
<a href="../../ContributedSoftware/render/render.html#render"><b>Render
by Attribute</b></a> graphical interface; for a similar process, see the
<a href="bfactor.html">B-Factor Coloring</a> image tutorial.) 
</p><p>
The built-in colors can be viewed 
<a href="../colortables.html#allcolors">here</a>
or by choosing <b>Actions... Color... all options</b> from the menu and
in the resulting dialog, checking the option to <b>Show all colors</b>.
Additional colors can be created with the command 
<a href="../midas/colordef.html"><b>colordef</b></a>.
</p><p>
Use a publication preset for nice image settings, including white background,
black outlines, and increased smoothness:
<blockquote>
<i>Command</i>: <b><a href="../midas/preset.html">preset</a> apply pub 1</b>
</blockquote>
The example image was saved from a 500x500-pixel window
using <b>File... Save Image</b> with default settings.
</p>

<table align="right" style="margin:8px 8px 8px 8px" 
cellpadding="0" cellspacing="0">
<tr>
<td><a href="3eebB-coulombic.png"><img src="3eebB-coulombic.png" width="250"
height="250" title="click for full-sized image..."
alt="Coulombic ESP coloring"></a></td>
</tr>
<tr>
<td><a href="3eebB-ambient.png"><img src="3eebB-ambient.png" width="250"
height="250" title="click for full-sized image..."
alt="Coulombic ESP & ambient"></a></td>
</table>

<h3><a name="esp">Electrostatic Potential</a></h3>
<p>
If not already done, show the molecular surface and use a publication preset:
<blockquote>
<i>Command</i>: <b><a href="../midas/surface.html">surf</a></b>
<br>
<i>Command</i>: <b><a href="../midas/preset.html">preset</a> apply pub 1</b>
</blockquote>
Start <a href="../../ContributedSoftware/coulombic/coulombic.html"><b>Coulombic
Surface Coloring</b></a> (under <b>Tools... Surface/Binding Analysis</b>),
choose the surface model, and click <b>OK</b>.
<!-- image 3eebB-coulombic.png saved at this point -->
</p><p>
The publication presets provide a nice starting point, but you might want to
adjust some settings, such as the thickness of the black outlines:
<blockquote>
<i>Command</i>: <b><a href="../midas/set.html#silhouettewidth">set silhouetteWidth</a> 3</b>
</blockquote>
If you prefer a simple &ldquo;line drawing&rdquo; appearance, try
ambient-only lighting:
<blockquote>
<i>Command</i>: <b><a href="../midas/lighting.html">light</a> mode ambient</b>
</blockquote>
<!-- image 3eebB-ambient.png saved at this point -->
To restore the default lighting mode:
<blockquote>
<i>Command</i>: <b><a href="../midas/lighting.html">light</a> mode two-point</b>
</blockquote>
The example images were saved from a 500x500-pixel window
using <b>File... Save Image</b> with default settings.
</p><p>
There is a limit to how thick lines such as silhouette edges, wire, and mesh
can be drawn while rendering an image.  If supersampling is done, the image is
initially drawn at a larger size than requested (3x3 by default) and then 
sampled back down to the final size.  In large and/or supersampled images,
lines may be thinner than expected.  The <b>File... Save Image</b> dialog 
reports the effective maximum linewidth. 
It may be possible to achieve the desired thickness by reducing the
supersampling level and/or the pixel dimensions of the image.
</p><p>
Chimera itself does not perform Poisson-Boltzmann electrostatics calculations, 
but it can read a grid file or &ldquo;map&rdquo; 
of electrostatic potential values output by another program such as
<a href="http://compbio.clemson.edu/delphi.php" target="_blank">DelPhi</a>,
<a href="http://www.poissonboltzmann.org/apbs/" target="_blank">APBS</a>,
or <a href="http://mccammon.ucsd.edu/uhbd.html" target="_blank">UHBD</a>.
Chimera also includes interfaces for running some of these programs:
the <a href="../../ContributedSoftware/apbs/apbs.html"><b>APBS</b></a> 
tool uses a web service, whereas
<a href="../../ContributedSoftware/delphicontroller/delphicontroller.html"><b>DelPhiController</b></a>
requires a local (user-installed) copy of 
<a href="http://compbio.clemson.edu/delphi.php" target="_blank">DelPhi</a>.
Surfaces can be colored by the potential map values using
<a href="../../ContributedSoftware/surfcolor/surfcolor.html"><b>Electrostatic 
Surface Coloring</b></a> or the command 
<a href="../midas/scolor.html"><b>scolor</b></a>.
An example map file
(<a href="http://www.cgl.ucsf.edu/chimera/data/tutorials/surfprop/3eebB.phi">3eebB.phi</a>,
calculated with DelPhi for the same structure as in the tutorial above) 
is provided for those who wish to try this approach.
</p>

<hr>
<address>meng-at-cgl.ucsf.edu / October 2012</address>
</body>
</html>
