<!--
--- UCSF Chimera Copyright ---
Copyright (c) 2010-2012 Regents of the University of California.
All rights reserved.  This software provided pursuant to a
license agreement containing restrictions on its disclosure,
duplication and use.  This notice must be embedded in or
attached to all copies, including partial copies, of the
software or any revisions or derivations thereof.
--- UCSF Chimera Copyright ---
-->

<html><head>
<title>coulombic</title>
</head><body>
<font size="-1">
<a href="../framecommand.html" target="_top">Chimera Commands Index</a>
</font>

<h3><a href="usageconventions.html">Usage</a>:
 <BR><b>coulombic</b> &nbsp;<a href="#options"><i>options</i></a>
&nbsp;<i>value1 &nbsp;color1</i> &nbsp;<i>value2 &nbsp;color2</i>&nbsp;
[... <i>valueN</i> &nbsp;<i>colorN</i>]
&nbsp;<a href="frameatom_spec.html" target="_top"><i>atom-spec</i></a></h3>
<p>
<b>Coulombic</b> is the command-line implementation of
<a href="../../ContributedSoftware/coulombic/coulombic.html"><b>Coulombic
Surface Coloring</b></a>, which colors
<a href="../representation.html#surfaces">molecular surfaces</a>
by electrostatic potential calculated with Coulomb's law:
<blockquote>
<font size ="+1">
&phi; = &Sigma; [q<sub>i</sub> / (&epsilon;d<sub>i</sub>)]
</font>
</blockquote>
&phi; is the potential (which varies in space),
q are the atomic partial charges,
d are the distances from the atoms, and
&epsilon; is the dielectric, representing screening by the medium or solvent.
A distance-dependent dielectric is sometimes used to approximate screening 
by implicit solvent.  
By default, <b>coulombic</b> uses a distance-dependent dieletric:
&epsilon; = 4d
</p><p>
<b>Coulombic</b> can handle structures with or without explicit
<a href="#hydrogens">hydrogens</a>.
It can also <a href="#volume">generate a grid</a>
of the potential values
(see <a href="../../ContributedSoftware/coulombic/coulombic.html#volume">why
this might be useful</a>).
</p><p>
The value/color pairs define how surface electrostatic potential
is mapped to colors.  At least two value/color pairs must be supplied. 
Electrostatic potential values (<i>value1</i>, <i>value2</i>, <i>etc.</i>)
are in units of kcal/(mol&middot;<i>e</i>) at 298 K.
Each color name (<i>color1</i>, <i>color2</i>, <i>etc.</i>) can be any
<a href="color.html#colorname">color name</a> that specifies a single color.
The command does not have default color settings, but the defaults in
the <a href="../../ContributedSoftware/coulombic/coulombic.html"><b>Coulombic
Surface Coloring</b></a> tool correspond to 
<b>&ndash;10 red 0 white 10 blue</b>.
</p><p>
The molecular surface(s) should first be displayed,
for example with the command <a href="surface.html"><b>surface</b></a>.
Only the residues enclosed by a molecular surface will be used
to calculate the potential on that surface.
The <a href="frameatom_spec.html" target="_top"><i>atom-spec</i></a>
indicates which molecular surfaces should be colored.
Entire (not partial) molecular surfaces will be affected, even
if only a subset of the atoms enclosed in a surface are specified.
</p><p>
<a name="hydrogens">
The calculation requires charge assignments, which in turn require hydrogens.
</a>
An existing structure lacking hydrogens is not changed,
but a copy is created in memory, <a href="addh.html">protonated</a>,
and <a href="addcharge.html">assigned charges</a>
(<a href="../../ContributedSoftware/addcharge/addcharge.html#standard">details</a>),
which are then transferred to the existing structure.  
Selenomethionine (MSE) residues are treated as methionines (MET)
for purposes of charge assignment.  Where hydrogens 
are missing from the existing structure, their charges are collapsed
onto the adjacent heavy atom: such hydrogens are <i>implicit</i>.
</p><p>
Alternatively, 
a structure may already have <i>explicit</i> hydrogens, or they can be
<a href="addh.html">added</a> beforehand in Chimera.
A structure may also have pre-existing charge assignments, such as from
<a href="addcharge.html"><b>addcharge</b></a> or a previous
use of <b>coulombic</b>.
If all of the atoms corresponding to the chosen surface already have
charges, those values are used rather than assigned anew
the first time Coulombic coloring is applied to that surface.
In subsequent applications, the existing charges will be used unless
the <a href="#hisScheme"><b>hisScheme</b></a> is changed,
which forces the charges to be assigned anew.
Another way to force reassignment is to remove the charges with the command 
<a href="setattr.html"><b>~setattr a charge</b></a>.
</p><p>
See <a href="../../ContributedSoftware/coulombic/coulombic.html"><b>Coulombic
Surface Coloring</b></a> for more details, including discussions of 
<a href="../../ContributedSoftware/coulombic/coulombic.html#hydrogens">implicit
vs. explicit hydrogens</a> and
<a href="../../ContributedSoftware/coulombic/coulombic.html#limitations">limitations</a> of the method.
See also:
<b><A href="apbs.html">apbs</A></b>,
<b><A href="scolor.html">scolor</A></b>,
<b><A href="rangecolor.html">rangecolor</A></b>,
<b><A href="addh.html">addh</A></b>,
<b><A href="addcharge.html">addcharge</A></b>,
<b><A href="pdb2pqr.html">pdb2pqr</A></b>
</p>

<a name="options">
<h4>Options</h4>
</a><p>
Option keywords for <b>coulombic</b> can be truncated to unique strings
and keyword case does not matter.
A vertical bar "|" designates mutually exclusive options, and
default values are indicated with <b>bold</b>.
Synonyms for true: True, 1.  Synonyms for false: False, 0.
</p>
<blockquote>
  <a name="distDep"><b>distDep</b> <b>true</b>&nbsp;|&nbsp;false</a>
  <br>Whether the dielectric should be distance-dependent; whether
  &epsilon; should vary in proportion to the distance from each charge.
</blockquote>
<blockquote>
  <b>dielectric</b> <i>C</i>
  <br>Set the dielectric constant to <i>C</i> (default <b>4.0</b>), where
  &epsilon; = <i>C</i>d if
  <a href="#distDep"><b>distDep</b></a> is <b>true</b>, 
  &epsilon; = <i>C</i> if
  <a href="#distDep"><b>distDep</b></a> is <b>false</b>.
</blockquote>
<blockquote>
  <b>surfDist</b> <i>offset</i>
  <br>How far out from each surface vertex, along its normal,
  to evaluate the electrostatic potential (default <b>1.4</b> &Aring;).
  The rationale for looking outward is that the values at the centers of 
  any interacting atoms are more relevant than those at their surfaces.  A
  <a href="../representation.html#surfaces">molecular surface</a>
  is <b><i>solvent-excluded</i></b>; it shows where the surface of a
  spherical probe (typically of radius 1.4 &Aring;) can lie.  
  Thus, 1.4 &Aring; out from the molecular surface is about as close 
  as the probe center can get, the <b><i>solvent-accessible</i></b> surface.
</blockquote>
<blockquote>
  <a name="hisScheme"><b>hisScheme</b> 
  HID&nbsp;|&nbsp;HIE&nbsp;|&nbsp;HIP&nbsp;|&nbsp;<b>none</b></a>
  <br>How to determine the charge states of histidines
  in structures without hydrogens:
  <ul>
  <li>HID
  - neutral sidechain, implicitly protonated at &delta;-nitrogen
  <li>HIE 
  - neutral sidechain, implicitly protonated at &epsilon;-nitrogen
  <li>HIP 
  - positive sidechain, implicitly protonated at both sidechain nitrogens
  <li><b>none</b> (default) 
  - protonation state chosen based on the local H-bonding environment
  </ul>
  These settings apply only to residues named HIS. Histidines that already have
  the special names (HID, HIE, HIP) will be protonated accordingly.
  Changing the <b>hisScheme</b> from what was used previously (via command
  or graphical interface) indicates that the charges on the
  should be erased and assigned anew.
</blockquote>
<blockquote>
  <b>key</b> 
  <br>Bring up the 
<a href="../../ContributedSoftware/2dlabels/2dlabels.html#colorkey"><b>Color 
Key</b></a> tool, filled in with the appropriate colors and values, and set to
<b>Use mouse for key placement</b> for creating/positioning the color key
in the graphics window.	 The mouse setting can be toggled to allow
moving models with the mouse.
</blockquote>
<a name="volume">
Using any of the following three options
generates a grid of the Coulombic potential values
and starts the
<a href="../../ContributedSoftware/surfcolor/surfcolor.html"><b>Electrostatic Surface Coloring</b></a>
and <a href="../../ContributedSoftware/volumeviewer/framevolumeviewer.html" 
target="_top"><b>Volume Viewer</b></a> tools
(see <a href="../../ContributedSoftware/coulombic/coulombic.html#volume">why
this might be useful</a>):
</a>
<blockquote>
  <b>gspacing</b>  <i>r</i>
  <br>Grid point spacing in each dimension (default <i>r</i> = 1.0 &Aring;).
</blockquote>
<blockquote>
  <b>gpadding</b> <i>d</i>
  <br>Distance to extend the grid in each dimension beyond the atoms
  enclosed in the surface (default <i>d</i> = 5.0 &Aring;).
</blockquote>
<blockquote>
  <b>gname</b> <i>data-name</i>
  <br>Dataset name (default <b>Coulombic ESP</b>).
</blockquote>
</body></html>
