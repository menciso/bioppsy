# ----------------------------------------------------------------------------
# Volume series command.
#
#   Syntax: vseries <operation> <mapSpec>
#
from Commands import CommandError

players = set()         # Active players.

def vseries_command(cmdname, args):

    from Commands import bool_arg, float_arg, enum_arg, int_arg
    from Commands import perform_operation
    ops = {
        'play': (play_op,
                 (('series', series_arg),),
                 (),
                 (('loop', bool_arg),
                  ('direction', enum_arg,
                   {'values':('forward', 'backward', 'oscillate')}),
                  ('normalize', bool_arg),
                  ('maxFrameRate', float_arg),
                  ('showMarkers', bool_arg),
                  ('precedingMarkerFrames', int_arg),
                  ('followingMarkerFrames', int_arg),
                  ('colorRange', float_arg),
                  ('cacheFrames', int_arg),
                  )),
        'stop': (stop_op,
                 (('series', series_arg),),
                 (),
                 ()),
        }

    perform_operation(cmdname, args, ops)

# -----------------------------------------------------------------------------
#
def play_op(series, direction = 'forward', loop = False, maxFrameRate = None,
            normalize = False, showMarkers = False,
            precedingMarkerFrames = 0, followingMarkerFrames = 0,
            colorRange = None, cacheFrames = 1):

    tstart = len(series[0].data_regions)-1 if direction == 'backward' else 0
    import play
    p = play.Play_Series(series,
                         start_time = tstart,
                         play_direction = direction,
                         loop = loop,
                         max_frame_rate = maxFrameRate,
                         normalize_thresholds = normalize,
                         show_markers = showMarkers,
                         preceding_marker_frames = precedingMarkerFrames,
                         following_marker_frames = followingMarkerFrames,
                         color_range = colorRange,
                         rendering_cache_size = cacheFrames)
    global players
    players.add(p)
    p.play()
    release_stopped_players()

# -----------------------------------------------------------------------------
#
def stop_op(series):

    for p in players:
        for s in series:
            if s in p.series:
                p.stop()
    release_stopped_players()
  
# -----------------------------------------------------------------------------
#
def series_arg(s):

  import Commands
  vlist = Commands.volumes_arg(s)
  sset = set()
  import gui
  d = gui.volume_series_dialog(create = False)
  if d:
    for v in vlist:
      for ser in d.series:
        if v in ser.data_regions:
          sset.add(ser)
  ns = len(sset)
  if ns == 0:
    raise CommandError('"%s" does not specify a volume series' % s)
  return list(sset)

# -----------------------------------------------------------------------------
#
def release_stopped_players():

  players.difference_update([p for p in players if p.play_handler is None])
