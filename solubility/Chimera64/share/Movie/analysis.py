# --- UCSF Chimera Copyright ---
# Copyright (c) 2000 Regents of the University of California.
# All rights reserved.  This software provided pursuant to a
# license agreement containing restrictions on its disclosure,
# duplication and use.  This notice must be embedded in or
# attached to all copies, including partial copies, of the
# software or any revisions or derivations thereof.
# --- UCSF Chimera Copyright ---
#
# $Id: analysis.py 36306 2012-04-26 20:54:25Z pett $

from chimera import selection

class AnalysisError(ValueError):
	pass

def analysisAtoms(mol, useSel, ignoreBulk, ignoreHyds, polymericOnly=False):
	if useSel:
		selAtoms = selection.currentAtoms()
		if selAtoms:
			# reduce to just ours
			sel1 = selection.ItemizedSelection()
			sel1.add(selAtoms)
			sel2 = selection.ItemizedSelection()
			sel2.add(mol.atoms)
			sel1.merge(selection.INTERSECT, sel2)
			atoms = sel1.atoms()
			if not atoms:
				raise AnalysisError("No selected atoms in"
							" trajectory!")
		else:
			atoms = mol.atoms
	else:
		atoms = mol.atoms

	if ignoreBulk:
		bulkSel = selection.OSLSelection("@/surfaceCategory="
				"solvent or surfaceCategory=ions")
		atomSel = selection.ItemizedSelection()
		atomSel.add(atoms)
		atomSel.merge(selection.REMOVE, bulkSel)
		atoms = atomSel.atoms()
		if not atoms:
			raise AnalysisError("No atoms remaining after ignoring"
							" solvent/ions")
	if ignoreHyds:
		atoms = [a for a in atoms if a.element.number != 1]
		if not atoms:
			raise AnalysisError("No atoms remaining after ignoring hydrogens")

	if polymericOnly:
		atoms = [a for a in atoms
			if len(a.residue.atoms) < mol.rootForAtom(a, True).size.numAtoms]
		if not atoms:
			raise AnalysisError("No atoms remaining after eliminating non-polymers")
	return atoms
