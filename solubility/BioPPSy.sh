#! bash
CHIMERA_PATH=/programs/x86_64-linux/chimera/1.8/bin/chimera
BIOPPSY_PATH=~/Repos/BioPPSy2/solubility
echo "CHIMERA_PATH set to " $CHIMERA_PATH 
export CHIMERA_PATH
export BIOPPSY_PATH
java -jar $BIOPPSY_PATH/BioPPSy.jar
