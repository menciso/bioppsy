I found it a bit hard to make the program run because there were many versions of
 the same files in several places.

As far as I know, the user should stick to the ''property'' directory
1) sh Solubility.sh - BioPPSy opens
2) Have an algorithm up and running. The first time, create a new one:
	a) Select name and type of algorithm (property.algorithm.Algorithm works)
	b) Select filename of training molecule set
		- Specify full path: $PATH/property/bin/data/test/expSolSdDev.sdf
		- Make sure the mol2/sdf files have the properties in there!!
	c) Add descriptors
	d) Start train
	e) Plot parameters (plot parameter vs error will appear)
	f) SAVE the algorithm
3) Upload a molecule set (eg. $PATH/property/bin/data/data_set.sdf)
	- You can browse each of them using chimera (make sure you set the path properly)
	- If there is experimental data you can compute its standard deviation
	- The interesting part is the ''Estimate property'' tab
	- Results will appear in ''Results'' (print to file works)


Marta Enciso
1st April 2014
-----------------------------------------------------------------------------------------
TASK 1: How many algorithms work?
- property.algorithm.Algorithm works
- property.algorithm.KlopmanAlgorithm works (slowly)
- property.algorithm.PLSalgorithm gives 0.0 back
- property.algorithm.MCUVAlgorithm works

TASK 2: uploading property data independently does not work

---THEN---
Should I improve the program or just start writing up the paper???

