package property;

//log4j
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

//sql
import java.sql.SQLException;
import java.sql.ResultSet;

/**********************************************************************
 * <p>
 * A Result represents a calculated logS property value and 
 * has a Mol and a ResultCollection. A Result is uniquely determined
 * by its Mol's number, its ResultCollection's Algorithm name, 
 * and its ResultCollection's MolSet's filename. A Result also
 * has a database id which is used to save space in the database.
 * </p>
 **********************************************************************/
public class Result
{
    private boolean modified = true;

    private ResultCollection rc;
    private Mol mol;
    private double value;
    private int id = -1;

    static Logger logger = Logger.getLogger(Result.class);

    public Result()
    {
    }

    public Result(ResultCollection parent, Mol mol, double value)
    {
        this.rc = parent;
        this.mol = mol;
        this.value = value;
    }

    public Mol getMol()
    {
        return mol;
    }

    public double getValue()
    {
        return value;
    }

    /**********************************************
     * Returns the id of this Result. If the Result
     * has not been loaded from the Database it will
     * not have an id and hence getId() will attempt
     * to retrieve the id from the Database.
     * Returns -1 if no id could be found.
     * Throws an SQLException on a database error.
     **********************************************/
    public int getId(Database db)
        throws SQLException
    {
        int retId = -1;

        if( id >= 0 )
        {
            return id;
        }
        else
        {
            String getId =
                "SELECT id FROM Result WHERE " +
                " algorithm='" + rc.getAlgorithm().getName() + "'" +
                " AND molSet=" + 
                    rc.getMolSet().getId() +
                " AND molNumber=" + mol.getNumber() + ";";

            ResultSet rs = db.executeQuery(getId);
            while( rs.next() )
            {
                retId = rs.getInt("id");
            }
            if( retId >= 0 )
            {
                return retId;
            }
        }

        return -1;
    }

    public void setResultCollection(ResultCollection rc)
    {
        this.rc = rc;
    }

    public void setMol(Mol mol)
    {
        this.mol = mol;
        setModified();
    }

    public void setValue(double value)
    {
        this.value = value;
        setModified();
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public ResultCollection getRC()
    {
        return rc;
    }

    /*************************************************
     * Saves this Result to the Database. Returns true
     * on success and false on failure.
     *************************************************/
    public boolean save(Database db)
    {
        if( !modified() )
        {
            return true;
        }

        try 
        {
            String query = getSaveSql(db);
            db.execute(query);
        }
        catch(SQLException sqle)
        {
            logger.error(sqle);
            return false;
        }

        setNotModified();

        return true;
    }

    /*************************************************
     * Saves this Result to the Database. Returns true
     * on success and false on failure.
     *************************************************/
    public boolean save(Database db, property.util.Profiler profiler)
    {
        if( !modified() )
        {
            return true;
        }

        try 
        {
            String query = getSaveSql(db, profiler);
profiler.addToCount(6);
            db.execute(query);
profiler.addToCount(7);
        }
        catch(SQLException sqle)
        {
            logger.error(sqle);
            return false;
        }

        setNotModified();

        return true;
    }

    /********************************************
     * Returns the sql query to save this Result
     * in the database. Returns null on error.
      *******************************************/
    public String getSaveSql(Database db)
    {
        try 
        {
            if( !exists(db) )
            {
                return getCreateSql();
            }
            else
            {
                return getDoSaveSql();
            }
        }
        catch(SQLException sqle)
        {
            logger.error(sqle);
            return null;
        }
    }

    /********************************************
     * Returns the sql query to save this Result
     * in the database. Returns null on error.
      *******************************************/
    public String getSaveSql(Database db, property.util.Profiler profiler)
    {
        try 
        {
profiler.addToCount(0);
            if( !exists(db,profiler) )
            {
profiler.addToCount(5);
                return getCreateSql();
            }
            else
            {
profiler.addToCount(5);
                return getDoSaveSql();
            }
        }
        catch(SQLException sqle)
        {
            logger.error(sqle);
            return null;
        }
    }

    public String getDoSaveSql()
    {
        double expSol = 0.0;

        String update =
            "UPDATE Result SET";

        if( mol.getCAS() != null )
        {
            update += 
                " molCAS='" + mol.getCAS() + "'";
        }

        update +=
            ", value=" + value;

        try
        {
            expSol = mol.getExpSol();
            update +=
                ", expSol=" + expSol;
        }
        catch(NotFoundException nfe)
        {
            // do nothing            
        }

        update += "WHERE " +
            " algorithm='" + rc.getAlgorithm().getName() + "'" +
            " AND molSet=" + 
                rc.getMolSet().getId() +
            " AND molNumber=" + mol.getNumber() + ";";

        logger.debug(update);

        return update;
    }

    public String getCreateSql()
    {
        double expSol = 0.0;
        boolean haveExpSol;

        try
        {
            expSol = mol.getExpSol();
            haveExpSol = true;
        }
        catch( NotFoundException nfe )
        {
            haveExpSol = false;
        }

        String create =
            "INSERT INTO Result(" +
            "algorithm,molSet,molNumber";
        if( mol.getCAS() != null )
        {
            create += ",molCAS";
        }
        create += ",value";

        if( haveExpSol )
        {
            create += ",expSol";
        }

        create +=
            ") VALUES(" +
            "'" + rc.getAlgorithm().getName() + "'" +
            "," + rc.getMolSet().getId() +
            "," + mol.getNumber();

        if( mol.getCAS() != null )
        {
            create +=
                ",'" + mol.getCAS() + "'";
        }

        create += 
            "," + value;

        if( haveExpSol )
        {
            create += "," + expSol;
        }

        create += ");";

        return create;
    }

    /************************************************
     * Deletes this Result from the database. Returns
     * true on success, false on failure. Returns
     * true if the Result does not exist.
     ************************************************/
    public boolean delete(Database db)
    {
        String delete =
            "DELETE FROM Result WHERE " +
            " algorithm='" + rc.getAlgorithm().getName() + "'" +
            " AND molSet=" + 
                rc.getMolSet().getId() +
            " AND molNumber=" + mol.getNumber() + ";";

        //logger.debug(delete); 

        try
        {
            db.execute(delete);
        }
        catch(SQLException sqle)
        {
            logger.error(sqle.getMessage());
            return false;
        }

        setModified();

        return true;
    }

    /*************************************************
     * Returns true if this Result is found in the Database,
     * false otherwise. Throws an SQLException on error
     ****************************************************/
    public boolean exists(Database db)
        throws SQLException 
    {
        String exists =
            "SELECT molNumber FROM Result WHERE " +
            " algorithm='" + rc.getAlgorithm().getName() + "'" +
            " AND molSet=" + 
                rc.getMolSet().getId() +
            " AND molNumber=" + mol.getNumber() + ";";

        // logger.debug(exists);
        ResultSet rs = db.executeQuery(exists);
        if( rs.next() )
        {
            return true;
        }

        return false;
    }

    /*************************************************
     * Returns true if this Result is found in the Database,
     * false otherwise. Throws an SQLException on error
     ****************************************************/
    public boolean exists(Database db, property.util.Profiler profiler)
        throws SQLException 
    {
profiler.addToCount(1);
        String exists =
            "SELECT id FROM Result WHERE " +
            " algorithm='" + rc.getAlgorithm().getName() + "'" +
            " AND molSet=" + 
                rc.getMolSet().getId() +
            " AND molNumber=" + mol.getNumber() + ";";
profiler.addToCount(2);
        // logger.debug(exists);
        ResultSet rs = db.executeQuery(exists);
profiler.addToCount(3);
        if( rs.next() )
        {
profiler.addToCount(4);
            return true;
        }
profiler.addToCount(4);
        return false;
    }

    /*****************************************************
     * Returns true if this object has been modified since
     * it has last been saved, false otherwise.
     *****************************************************/
    public boolean modified()
    {
        return modified;
    }

    /*****************************************************
     * Registers that this object has been modified.
     *****************************************************/
    public void setModified()
    {
        modified = true;
    }

    /*****************************************************
     * Registers that this object has not been modified.
     *****************************************************/
    public void setNotModified()
    {
        modified = false;
    }
}
