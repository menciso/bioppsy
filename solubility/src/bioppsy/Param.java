package bioppsy;

//log4j
import org.apache.log4j.Logger;

import bioppsy.algorithm.MLR;


// sql
import java.sql.SQLException;
import java.sql.ResultSet;
//JOElib
import joelib.desc.*;

/******************************************************
 * A Param represents a parameter for an Algorithm. 
 * Parameters have an id which is unique with regards
 * to a particular Algorithm and have a value. Parameter
 * values are calculated for an Algorithm (usually using
 * Multiple Linear Regression) over a set of molecules
 * that have measured experimental property values.
 * The parameter value is the coefficient of that parameter
 * in the prediction equation. For each molecule an
 * occurrence value is calculated for each parameter. For
 * example with an atomic typing parameter, the occurrence
 * value for a molecule is the number of times that atom
 * occurs in the molecule. This occurrence value is 
 * multiplied by the parameter value in the prediction
 * equation. A Param has a frequency which is the total 
 * occurrence of this Param in the training set.
 * When extending Param make sure to override the
 * getDouble(Mol) method and return the occurrence
 * of the parameter for the given Mol.
 ******************************************************/
public class Param
{
    private boolean modified = true;

    private int id;
    private double value;
    private MLR al;

    // variance of occurrence values for
    // this Param in the training set
    private double variance;
    // mean of occurrence values for this
    // Param in the training set
    private double mean = 0.0;

    // occurrence of this Param in
    // the training set
    private int frequency = 0;

    static Logger logger = Logger.getLogger(Param.class);

    protected Param() 
    {
    }

    public Param(MLR al)
    {
        this.al = al;
        this.id = 0;
        this.value = 0.0;
        this.frequency = 0;
    }

    public Param(MLR al, int id, double value)
    {
        this.al = al;
        this.id = id;
        this.value = value;
        this.frequency = 0;
    }

    /*************************************************
     * Returns the type of this Param
     ************************************************/
    public String getType()
    {
        return "Parameter";
    }

    public int getId()
    {
        return id;
    }

    public double getValue()
    {
        return value;
    }

    public MLR getAlgorithm()
    {
        return al;
    }

    public int getFrequency()
    {
        return frequency;
    }

    public void setId(int id)
    {
        this.id = id;
        setModified();
    }

    public void setValue(double value)
    {
        this.value = value;
        setModified();
    }

    public void setAlgorithm(MLR al)
    {
        this.al = al;
        setModified();
    }

    public double getMean()
    {
        return mean;
    }

    public void setMean(double mean)
    {
        this.mean = mean;
        setModified();
    }

    public double getVariance()
    {
        return variance;
    }

    public void setVariance(double variance)
    {
        this.variance = variance;
        setModified();
    }

    public void setFrequency(int frequency)
    {
        this.frequency = frequency;
        setModified();
    }

    /*******************************************
     * Increments the frequency of this parameter
     * in the training set by the specified amount.
     *******************************************/
    public void incrementFrequency(int amount)
    {
        frequency += amount;
        setModified();
    }

    /*******************************************************
     * Saves this object in the database. Returns true on
     * success, false on failure. Creates the object in the
     * database if it does not exist.
     ******************************************************/
    public boolean save(Database db)
    {
        if( !modified() )
        {
            return true;
        }

        try
        {
            if( !exists(db) )
            {
                create(db);
            }
            else
            {
                doSave(db);
            }
        }
        catch(SQLException sqle)
        {
            logger.debug(sqle.getMessage());
            return false;
        }

        setNotModified();

        return true;
    }

    /******************************************************
     * Returns true if this Param already exists in the 
     * database, false otherwise. Throws an SQLException on
     * error.
     ******************************************************/
    public boolean exists(Database db)
        throws SQLException
    {
        ResultSet rs;

        String checkExists =
            "SELECT id FROM Parameter WHERE algorithm='" +
                getAlgorithm().getName() + "' AND id=" +
                getId() + ";";

        //logger.debug(checkExists);

        rs = db.executeQuery(checkExists);
        while(rs.next())
        {
            // it exists
            return true;
        }
        return false;
    }

    private void create(Database db)
        throws SQLException
    {
        String create =
            "INSERT INTO Parameter(algorithm,id,value,frequency,type,mean,variance) " +
            "VALUES(" +
            "'" + getAlgorithm().getName() + "'," +
            getId() + "," +
            getValue() + "," +
            getFrequency() + "," +
            "'" + getClass().getName() + "'" +
            "," + mean +
            "," + variance +
            ");";

        //logger.debug(create);

        db.execute(create);
    }

    private void doSave(Database db)
        throws SQLException
    {
        String save =
            "UPDATE Parameter SET " +
            "value=" + getValue() + "," +
            "frequency=" + getFrequency() + "," +
            "type='" + getClass().getName() + "'" +
            " , mean=" + mean +
            " , variance=" + variance +
            " WHERE algorithm='" + getAlgorithm().getName() + "'" +
            " AND id=" + getId() + ";";

        //logger.debug(save);

        db.execute(save);
    }

    /****************************************************
     * Returns a Param with the given id and Algorithm.
     * Returns null if the Param is not found or if there
     * has been an error.
     ***************************************************/
    public static Param getParam(int id, MLR alg, Database db)
    {
        ResultSet rs;
        double retMean;
        double retVariance;

        // first we have to determine the type of the
        // Param

        String load =
            "SELECT type,mean,variance FROM Parameter WHERE" + 
            " algorithm='" + alg.getName() + "'" +
            " AND id=" + id + ";";

        try
        {
            rs = db.executeQuery(load);
            while(rs.next())
            {
                 String type = rs.getString("type");
                 Class classType = Class.forName(type);
                 Param param = (Param) classType.newInstance();
                 retMean = rs.getDouble("mean");
                 retVariance = rs.getDouble("variance");
                 Param retParam = param.get(id,alg,db);
                 retParam.setMean(retMean);
                 retParam.setVariance(retVariance);
                 retParam.setNotModified();
                 return retParam;
            }
        }
        catch( ClassNotFoundException cnfe )
        {
            logger.error("Class not found: " + cnfe.getMessage());
            return null;
        }
        catch( InstantiationException ie )
        {
            logger.error("Instatiation Exception: " + ie.getMessage());
            return null;
        }
        catch( IllegalAccessException iae )
        {
            logger.error("IllegalAccessException: " + iae.getMessage());
            return null;
        }
        catch(SQLException sqle)
        {
             logger.error(sqle.getMessage());
             return null;
        }

        return null;
    }

    protected Param get(int id, MLR alg, Database db)
        throws SQLException
    {
        ResultSet rs;
        Param param;

        String load =
            "SELECT value,frequency FROM Parameter WHERE" + 
            " algorithm='" + alg.getName() + "'" +
            " AND id=" + id + ";";


        rs = db.executeQuery(load);
        while(rs.next())
        {
             param = new Param(alg,id,rs.getDouble("value"));
             param.setFrequency(rs.getInt("frequency"));
             param.setNotModified();
             return param;
        }
        return null;
    } 

    /*****************************************************
     * Deletes this object from the database based on its
     * primary key. Returns true on success and false if
     * there has been an error. If the object is not found
     * true is still returned.
     *****************************************************/
    public boolean delete(Database db)
    {
        String delete =
            "DELETE FROM Parameter WHERE algorithm='" +
            getAlgorithm().getName() + "' AND id=" +
            getId() + ";";

        try
        {
            db.execute(delete);
        }
        catch(SQLException sqle)
        {
            logger.error(sqle.getMessage());
            return false;
        }

        setModified();

        return true;
    }

    /*****************************************************
     * Returns true if this Object has been modified since
     * it has last been saved, false otherwise. If this
     * Object does not have an id (or other primary key) 
     * true is returned.
     ****************************************************/
    public boolean modified()
    {
        return modified;
    }

    /*****************************************************
     * Registers that this object has been modified.
     *****************************************************/
    public void setModified()
    {
        modified = true;
    }

    /*****************************************************
     * Registers that this object has not been modified.
     *****************************************************/
    public void setNotModified()
    {
        modified = false;
    }

    /*****************************************************
     * Returns true if this object is equal to the given
     * object, false otherwise.
     ****************************************************/
    public boolean equals(Object other)
    {
        Param otherParam;

        if( getClass().getName().equals(
            other.getClass().getName() ) )
        {
            otherParam = (Param) other;
        }
        else
        {
            return false;
        }

        if( getId() == otherParam.getId() &&
            getAlgorithm().equals(otherParam.getAlgorithm()) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /***********************************************************
     * Returns the occurrence of this Param in the given Mol
     * <br>Jocelyn thinks it does not!
     * @param mol 
     ***********************************************************/
    public double getDouble(Mol mol)
    {
        return Double.NaN;
    }

    /*************************************************************
     * Returns true if this Param is an Intercept DescriptorParam,
     * false otherwise
     ****************************************************************/
    public boolean isIntercept()
    {
        String[] paramTypes = Constants.instance().getStringArray(
            "PARAM_TYPES");
        int descriptorIndex = Constants.instance().getInt(
            "DESCRIPTOR_PARAM_INDEX");
        String descriptorParam = paramTypes[descriptorIndex];
        String interceptClass = Constants.instance().getString(
            "INTERCEPT_CLASS");

        if( getClass().getName().equals(descriptorParam) )
        {
            DescriptorParam dp = (DescriptorParam) this;
            Descriptor descriptor = dp.getDescriptor();

            if( descriptor.getClass().getName().equals(interceptClass) )
            {
                return true;
            }
        }

        return false;
    }
}
