package bioppsy.io;


//log4j
import org.apache.log4j.Logger;

import bioppsy.NotFoundException;



//java
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

/********************************************************
 * An ExpPropFile object represents a File that contains
 * experimental property data. To obtain the experimental
 * property of a molecule with a given CAS number from
 * the file, just use the getExpSol(String) method.
 * <p>
 * Each line in the file represents a molecule and must have
 * the following format:<br>
 * 56-47-3	deoxycorticosterone acetate	-4.63_0.34<br>
 * - The first word must contain the number, this can either
 * be just the id number of the molecule or the CAS number<br>
 * - The last word must contain the experimental property,
 * if it is not present the line is skipped. This value
 * can include an optional error value after an underscore.<br>
 * - Any words in between the first and last word are taken as
 * the name of the molecule<br>
 * </p>
 ************************************************/
public class ExpPropFile extends PropFile
{
    static Logger logger = Logger.getLogger(ExpPropFile.class);

    /**
     * The inputFile filename can take one of two forms:
     * <br><br>
     * 1. If the first character of inputFile is the character
     * '$' then the file is taken to be the absolute or relative
     * filename of a file in the filesystem (after the leading '$'
     * is removed).
     * <br>
     * 2. If the first character is not a '$' character inputFile
     * is taken as the absolute package location of the file
     * as a class resource. When obtaining a stream from this
     * resource, a '/' character is prepended to inputFile to search 
     * for the given path name 'inputFile' from the package root.
     * For example if 'data/test_set.sdf' is provided the 
     * Class.getResrouceAsStream() function is passed the argument
     * '/data/test_set.sdf' which looks in the absolute path location
     * '..../src/data/test_set.sdf'.
     */
    public ExpPropFile(String filename)
    {
        super(filename);
    }

    /************************************************
     * Returns the experimental property of the molecule
     * with the given cas number. Throws a
     * NotFoundException if the Molecule is 
     * not found. Throws an IOException is there was
     * an error reading the file.
     ************************************************/
    public double getExpProp(String targetCAS)
        throws NotFoundException
    {
        BufferedReader in = null;
        String line;
        String fileCAS;
        double expProp;
        String[] splitLine;
        String[] splitValue;

        try 
        {
            in = new BufferedReader(new InputStreamReader(getInputStream()));
            while( (line = in.readLine()) != null )
            {
                // split on whitespace
                splitLine = line.split("[ \t\n\f\r]+",0);
                if( splitLine.length <= 1 )
                {
                    // do nothing, skip the line
                }
                else
                {
                    fileCAS = splitLine[0];
                    if( fileCAS.equals(targetCAS) )
                    {
                        // the last entry is the experimental property in
                        // to format 4.63_0.03 where 0.03 is the error
                        // we just take the first portion
                        splitValue = splitLine[splitLine.length-1].split("_");
                        expProp = 
                            new Double(splitValue[0]).doubleValue();
                        return expProp;
                    }
                }
            }
        }
        catch( IOException ioe )
        {
            if( in != null )
            {
                try
                { 
                    in.close(); 
                }
                catch (IOException ioe2) 
                { }
            }
            throw new NotFoundException(ioe.getMessage());
        }

        throw new NotFoundException(
           "Property for molecule: " + targetCAS + " not found in: " +
           this.getName());
    }




/************************************************
 * Returns the experimental property of the molecule
 * with the given id number. Throws a
 * NotFoundException if the Molecule is 
 * not found. Throws an IOException is there was
 * an error reading the file.
 ************************************************/
public double getExpProp(int targetNumber)
    throws NotFoundException
{
    BufferedReader in = null;
    String line;
    int fileNumber;
    double expProp;
    String[] splitLine;
    String[] splitValue;

    try 
    {
        in = new BufferedReader(new InputStreamReader(getInputStream()));
        while( (line = in.readLine()) != null )
        {
            // split on whitespace
            splitLine = line.split("[ \t\n\f\r]+",0);
            if( splitLine.length <= 1 )
            {
                // do nothing, skip the line
            }
            else
            {
                fileNumber = Integer.parseInt(splitLine[0]);
                if( fileNumber==targetNumber)
                {
                    // the last entry is the experimental property in
                    // to format 4.63_0.03 where 0.03 is the error
                    // we just take the first portion
                    splitValue = splitLine[splitLine.length-1].split("_");
                    expProp = 
                        new Double(splitValue[0]).doubleValue();
                    return expProp;
                }
            }
        }
    }
    catch( IOException ioe )
    {
        if( in != null )
        {
            try
            { 
                in.close(); 
            }
            catch (IOException ioe2) 
            { }
        }
        throw new NotFoundException(ioe.getMessage());
    }

    throw new NotFoundException(
       "Property for molecule: " + targetNumber + " not found in: " +
       this.getName());
}
}
