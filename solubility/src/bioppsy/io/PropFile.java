package bioppsy.io;

import java.io.*;
import org.apache.log4j.Logger;

public class PropFile
{
    private String filename;
    
    static Logger logger = Logger.getLogger(PropFile.class);

    /**
     * The inputFile filename can take one of two forms:
     * <br><br>
     * 1. If the first character of inputFile is the character
     * '$' then the file is taken to be the absolute or relative
     * filename of a file in the filesystem (after the leading '$'
     * is removed).
     * <br>
     * 2. If the first character is not a '$' character inputFile
     * is taken as the absolute package location of the file
     * as a class resource. When obtaining a stream from this
     * resource, a '/' character is prepended to inputFile to search 
     * for the given path name 'inputFile' from the package root.
     * For example if 'data/test_set.sdf' is provided the 
     * Class.getResrouceAsStream() function is passed the argument
     * '/data/test_set.sdf' which looks in the absolute path location
     * '..../src/data/test_set.sdf'.
     */
    public PropFile(String inputFile)
    {
        filename = inputFile;
    }

    /********************************************************************
     * Returns the path name of the file.
     *******************************************************************/
    public String getAbsolutePath()
    {
        return filename;
    } 
    
    public InputStream getInputStream()
    {
        if( filename.charAt(0) == '$' )
        {
            return getFileInputStream();
        }
        
        InputStream inputStream = 
            getClass().getResourceAsStream("/" + filename);
        if( inputStream == null )
        {
            logger.error("Warning, file not found: " + filename);
            return null;
        }
        return inputStream;
    }
    
    public InputStream getFileInputStream()
    {
        try
        {
            return new FileInputStream(filename.substring(1));
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error(fnfe.getMessage());
            return null;
        }
    }
    
    public String getName()
    {
        return filename;
    }

}
