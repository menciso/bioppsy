package bioppsy;

/***********************************************
 * A NotFoundException is thrown when
 * an object has not been found.
 ***********************************************/
public class NotFoundException
    extends Exception
{
    public NotFoundException(String str)
    {
        super(str);
    }
}
