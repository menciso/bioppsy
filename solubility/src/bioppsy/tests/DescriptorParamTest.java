package bioppsy.tests;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.Constants;
import bioppsy.Database;
import bioppsy.DescriptorParam;
import bioppsy.FailedConstructorException;
import bioppsy.MolSet;
import bioppsy.Param;
import bioppsy.algorithm.MLR;


//java
import java.sql.SQLException;

//JOELib
import joelib.desc.*;

/*************************************************************
 * Tests the DescriptorParam class.
 ************************************************************/
public class DescriptorParamTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(DescriptorParamTest.class);

    MLR alg;
    MolSet molSet;
    DescriptorParam param;

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
            molSet = new MolSet("filename");
            alg = new MLR(molSet,"algorithm");
            Descriptor descriptor = DescriptorFactory.getDescriptor("TestDescriptor");
            param = new DescriptorParam(alg,0,4.0);
            param.setDescriptor(descriptor);
            
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
        catch( DescriptorException de )
        {
            logger.error(de.getMessage());
        }
	}

    protected void tearDown() 
    {
    }

    /**************************************************************
     * Tests all the Param database functions
     *************************************************************/
    public void testDatabaseFunctions()
    {
        try
        {
            param.setFrequency(4847);
            
            if( param.exists(db) )
            {
                fail("Param exists before it has been created");
            }
            if( !param.save(db) )
            {
                fail("Failed to save param");
            }
            if( param.modified() )
            {
                fail("Parameter set to modified after saved");
            }
            if( !param.exists(db) )
            {
                fail("Param does not exist after being saved");
            }

            Param retParam = Param.getParam(0,alg,db);
            if( retParam == null )
            {
                fail("Failed to load Param");
            }
            if( retParam.modified() )
            {
                fail("Parameter set to modified after loaded");
            }
            if( retParam.getValue() != param.getValue() )
            {
                fail("Loaded value different to saved value");
            }
            if( retParam.getFrequency() != param.getFrequency() )
            {
                fail("Loaded frequency different to saved frequency");
            }
            if( retParam.getType() != param.getType() )
            {
                fail("Loaded Descriptor type: " + 
                    retParam.getType() + " different to saved type: " +
                    param.getType());
            }

            param.setValue(3834234.3434);
            if( !param.modified() )
            {
                fail("Parameter not modified after value change");
            }
            param.save(db);
            retParam = Param.getParam(0,alg,db);
            if( retParam.getValue() != param.getValue() )
            {
                fail("Loaded value different to saved value");
            }

            if( !param.delete(db) )
            {
                fail("Unable to delete Param");
            }
            if( !param.modified() )
            {
                fail("Parameter not modified after being deleted");
            }
            if( param.exists(db) )
            {
                fail("Param still exists after being deleted");
            }
        }
        catch(SQLException sqle)
        {
            fail(sqle.getMessage());
        }
    }
}
