package bioppsy.tests.descriptors;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.descriptors.*;




//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the Wiener descriptors
 ***************************************************************/
public class WienerTest extends TestCase 
{
    static Logger logger = Logger.getLogger(WienerTest.class);
    
    Database db;

	protected void setUp() 
    {
	}
    
    /**********************************************************
     * Tests the getDistanceMatrix method.
     *********************************************************/
    public void testGetDistanceMatrix()
    { 
        try
        {
            Wiener wiener = new Wiener();
            Mol mol = new Mol("CCCC");
            int[][] result = wiener.getDistanceMatrix(mol);
            int[][] compare = {{0,1,2,3},{1,0,1,2},{2,1,0,1},{3,2,1,0}};
            for( int i=0; i < 4; ++i )
            {
                for( int j=0; j < 4; ++j )
                {
                    assertEquals(compare[i][j],result[i][j]);
                }
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /*****************************************************
     * Tests the getDouble() method
     ****************************************************/
    public void testGetDouble()
    {
        try
        {
            Mol mol = new Mol("CC(C)(C)C(CC)C");
            Descriptor descriptor = DescriptorFactory.getDescriptor("Wiener");
            if( descriptor == null )
            {
                fail("Descriptor 'Wiener' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 63.0;
            if( expected != result.getDouble() )
            {
                fail("Incorrectly calculated wiener recieved: " + result.getDouble() +
                    " expected: " + expected);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
