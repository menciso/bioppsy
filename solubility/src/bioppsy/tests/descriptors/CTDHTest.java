package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the CTDH descriptor
 ***************************************************************/
public class CTDHTest extends TestCase 
{
    static Logger logger = Logger.getLogger(CTDHTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor
     ************************************************************/
    public void testDescriptor()
    {
        try
        {
            Mol mol = new Mol("CC(C)(C)C(CC)C[NH3]");
            Descriptor descriptor = DescriptorFactory.getDescriptor("CTDH");
            if( descriptor == null )
            {
                fail("Descriptor 'CTDH' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            if( result.getDouble() != 3.0 )
            {
                fail("Incorrect calculation of CTDH expected 3.0 recieved: " +
                    result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
