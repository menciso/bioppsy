package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the AP descriptor
 ***************************************************************/
public class APTest extends TestCase 
{
    static Logger logger = Logger.getLogger(APTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor
     ************************************************************/
    public void testDescriptor()
    {
        try
        {
            Mol mol = new Mol("c1ccccc1");
            Descriptor descriptor = DescriptorFactory.getDescriptor("AP");
            if( descriptor == null )
            {
                fail("Descriptor 'AP' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 1.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of AP for benzene. Expected: " +
                    expected + " Recieved: " + result.getDouble());
            }
            mol = new Mol("CCCCCCCCCCC");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of AP for carbon chain. Expected: " +
                    expected + " Recieved: " + result.getDouble());
            }
            mol = new Mol("[H]");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of AP for H Expected: " +
                    expected + " Recieved: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
