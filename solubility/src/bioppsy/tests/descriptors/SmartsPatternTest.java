package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the SmartsPattern descriptors
 ***************************************************************/
public class SmartsPatternTest extends TestCase 
{
    static Logger logger = Logger.getLogger(SmartsPatternTest.class);
    
    Database db;

	protected void setUp() 
    {
	}

    /*****************************************************
     * Tests the subclasses
     ****************************************************/
    public void testSubClasses()
    {
        try
        {
            Mol mol = new Mol("CCCCCC[OH]");
            Descriptor descriptor = DescriptorFactory.getDescriptor("S_sOH");

            if( descriptor == null )
            {
                fail("Descriptor 'S_sOH' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 1.0;
            if( result.getDouble() != expected)
            {
                fail("Failed to calculate correct S_sOH result: " +
                    result.getDouble() + " expected: " + expected);
            }
            mol = new Mol("c1ccccc1");
            descriptor = DescriptorFactory.getDescriptor("S_aaaC");
            if( descriptor == null )
            {
                fail("Descriptor 'S_aaaC' could not be initialised");
            }
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 6.0;
            if( result.getDouble() != expected)
            {
                fail("Failed to calculate correct S_aaaC result: " +
                    result.getDouble() + " expected: " + expected);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
