package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the Hy descriptor. Examples taken from the original paper
 *  from Todeschini et al, DOI:10.1080/10629369708039130
 ***************************************************************/
public class HyTest extends TestCase 
{
    static Logger logger = Logger.getLogger(HyTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor
     ************************************************************/
    public void testDescriptor()
    {
        try
        {
            Descriptor descriptor = DescriptorFactory.getDescriptor("Hy");
            if( descriptor == null )
            {
                fail("Descriptor 'Hy' could not be initialised");
            }
            String smiles = "CCCO";
            Mol mol = new Mol(smiles);
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 0.37067201267659955;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of Hy for: " + smiles +
                     " Expected: " + expected + " Received: " + result.getDouble());
            }
            
            smiles = "C"; //methane
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of Hy for: " + smiles +
                     " Expected: " + expected + " Received: " + result.getDouble());
            }
            
            
            smiles = "OO"; //hydrogen peroxide
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 3.6436363296498353;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of Hy for: " + smiles +
                     " Expected: " + expected + " Received: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
