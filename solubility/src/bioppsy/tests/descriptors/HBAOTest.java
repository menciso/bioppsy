package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the HBondAcceptors descriptor
 ***************************************************************/
public class HBAOTest extends TestCase 
{
    static Logger logger = Logger.getLogger(HBAOTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor HBAO
     * Last example taken from Osterberg,
     * J. Chem. Inf. Comput. Sci. 2000, 40, 1408-1411
     ************************************************************/
    public void testDescriptor()
    {
        try
        {
            String smiles = "c1ccccc1";
            Mol mol = new Mol(smiles);
            Descriptor descriptor = DescriptorFactory.getDescriptor("HBAO");
            if( descriptor == null )
            {
                fail("Descriptor 'HBAO' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBAO for: " + smiles +
                     " Expected: " + expected + " Received: " + result.getDouble());
            }
            smiles = "C(=O)";
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 1.0; // Oxygen acceptor 
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBAO for: " + smiles +
                     " Expected: " + expected + " Received: " + result.getDouble());
            }
            smiles = "c1ccncc1";
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 0.0;
            if( result.getDouble() != expected )
              if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBAO for: " + smiles +
                     " Expected: " + expected + " Received: " + result.getDouble());
            }
            smiles = "CCCN(CCC)CCC1=C2CC(=O)NC2=C(C=C1)O";
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 2.0;
            if( result.getDouble() != expected )
              if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBAO for: " + smiles +
                     " Expected: " + expected + " Received: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
