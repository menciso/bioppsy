package bioppsy.tests.descriptors;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.descriptors.*;




//JOELib
import jmat.data.Matrix;
import joelib.desc.*;
import joelib.desc.result.*;

import java.util.Iterator;

/****************************************************************
 * Tests the MDE descriptors
 ***************************************************************/
public class MDETest extends TestCase 
{
    static Logger logger = Logger.getLogger(MDETest.class);
    
    Database db;

	protected void setUp() 
    {
	}
    
    /**********************************************************
     * Tests the getDistanceMatrix method.
     *********************************************************/
    public void testGetDistanceMatrix()
    { 
        try
        {
            Mol mol = new Mol("CCCC");
            Matrix result = Topological.getTopologicalDistanceMatrix(mol);
            int[][] compare = {{0,1,2,3},{1,0,1,2},{2,1,0,1},{3,2,1,0}};
            for( int i=0; i < 4; ++i )
            {
                for( int j=0; j < 4; ++j )
                {
                    assertEquals(compare[i][j],(int) result.get(i, j));
                }
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /**********************************************************
     * Tests the getMDE method.
     *********************************************************/
    public void testGetMDE()
    { 
        try
        {
            MDE mde = new MDE();

            Mol mol = new Mol("CCCC");
            // expected = n / pow(d, 1/n)
            double result = mde.getMDE(mol,1,1);
            double expected = 1 / Math.pow(3, 1/1); // d=3, n=1
            if( result != expected )
            {
                fail("Failed to get MDE-11. Expected: " + expected + ". Result: " + result);
            }
            result = mde.getMDE(mol,1,2); // d=4.0, n=4
            expected = 4/ Math.pow(4.0,1.0/4.0);
            if( result != expected )
            {
                fail("Failed to get MDE-12. Expected: " + expected + ". Result: " + result);
            }
            result = mde.getMDE(mol,2,2);
            // mde = pow(1, 1/(2*1))
            if( result != 1.0 )
            {
                fail("Failed to get MDE-22");
            }
            
            mol = new Mol("c1ccccc1");
            result = mde.getMDE(mol,2,2);
            expected = 15 / Math.pow(1728.0, 1.0/15.0); // d=1728(5!/2!), n=15
            if( result !=  expected )
            {
                fail("Failed to get MDE-22 with benzene. Expected: " + expected + ". Result: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /*****************************************************
     * Tests the subclasses
     ****************************************************/
    public void testInit()
    {
        try
        {
            Mol mol = new Mol("CC(C)(C)C(CC)C");
            mol.setTitle("First");
            Descriptor descriptor = DescriptorFactory.getDescriptor("MDE14");
            if( descriptor == null )
            {
                fail("Descriptor 'MDE14' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 5.0 / Math.pow(6.0, 1.0/5.0); // n=5, d=6
            if( result.getDouble() != expected )
            {
                fail("Failed to calculate correct MDE14. Expected: " + expected + ". Result: " + result);
            }
            descriptor = DescriptorFactory.getDescriptor("MDE24");
            if( descriptor == null )
            {
                fail("Descriptor 'MDE24' could not be initialised");
            }
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 1.0 / Math.pow(2.0, 1.0);
            if( result.getDouble() != expected )
            {
                fail("Failed to calculate correct MDE14. Expected: " + expected + ". Result: " + result);
            }
            descriptor = DescriptorFactory.getDescriptor("MDE34");
            if( descriptor == null )
            {
                fail("Descriptor 'MDE34' could not be initialised");
            }
            result = (DoubleResult) descriptor.calculate(mol);
            if( result.getDouble() != 1.0 )
            {
                fail("Failed to calculate correct MDE14");
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /*******************************************************
     * Makes sure that we never get NaN as a result
     ******************************************************/
    public void testAlwaysANumber()
    {
        MDE mde = new MDE();
        Double result;
        MolSet molSet = new MolSet(
                Constants.instance().getString("TEST_SDF_FILENAME"));
        Iterator i = molSet.getIterator();
        while( i.hasNext() )
        {
            try
            {
                Mol mol = (Mol) i.next();
                result = new Double(mde.getMDE(mol,1,1));
                if( result.isNaN() )
                {
                    fail("Recieved NaN from MDE11");
                }
            }
            catch( DescriptorException de )
            {
                fail(de.getMessage());
            }
        }
    }
}
