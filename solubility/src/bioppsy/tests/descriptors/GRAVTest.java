package bioppsy.tests.descriptors;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.descriptors.*;




//JOELib
import joelib.desc.*;
import joelib.desc.result.*;
import joelib.molecule.*;

/****************************************************************
 * Tests the GRAV descriptors
 ***************************************************************/
public class GRAVTest extends TestCase 
{
    static Logger logger = Logger.getLogger(GRAVTest.class);

	protected void setUp() 
    {
	}
    
    /**********************************************************
     * Tests the getGrav method.
     *********************************************************/
    public void testGetGrav()
    { 
        JOEAtom atom = null;

        GRAV GRAV = new GRAV();
        Mol mol = new Mol("CCCCC");

        int i=-4;
        for( int j=1; j <= mol.numAtoms(); ++j )
        {
            atom = mol.getAtom(j);
            atom.setVector(i,0.0,0.0);
            i += 2;
        }
        
        double expected = 0.;
        for (int j=1; j<=4; ++j)
        {
        	int j1=2*(j-1);
        	expected+=j/(Math.pow(8-j1,2.0));
        }


        double mass = atom.getAtomicMass();
        expected *=Math.pow(mass,2.0);
        double result = GRAV.getGrav(mol);
        if( result != expected )
        {
            fail("Error calculating gravitational index. " +
                "RESULT: " + result + " EXPECTED: " + expected);
        }
    }

    /*************************************************************
     * Tests that the descriptors are registered correctly.
     ************************************************************/
    public void testDescriptors()
    {
        try
        {
            Mol mol = new Mol("CC(C)(C)C(CC)C");
            Descriptor descriptor = DescriptorFactory.getDescriptor("GRAV6");
            if( descriptor == null )
            {
                fail("Descriptor 'GRAV6' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
