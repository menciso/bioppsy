package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the Polar surface area descriptors
 * Note that in all cases we have added the suitable
 * number of hydrogens (see class)
 * 
 ***************************************************************/
public class JPolarSurfaceTest extends TestCase 
{
    static Logger logger = Logger.getLogger(JPolarSurfaceTest.class);
    
    Database db;

	protected void setUp() 
    {
	}

    /*****************************************************
     * Tests the descriptor
     ****************************************************/
    public void testJPolarSurfaceArea()
    {
        try
        {
            Mol mol = new Mol("[CH3][OH]");
            Descriptor descriptor = DescriptorFactory.getDescriptor("JPolarSurfaceArea");

            if( descriptor == null )
            {
                fail("Descriptor 'JPolarSurfaceArea' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 20.23;
            if( result.getDouble() != expected)
            {
                fail("Failed to calculate correct JPolarSurfaceArea result: " +
                    result.getDouble() + " expected: " + expected);
            }
            
            mol = new Mol("[CH3][NH2]");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 27.64;
            if( result.getDouble() != expected)
            {
                fail("Failed to calculate correct JPolarSurfaceArea result: " +
                    result.getDouble() + " expected: " + expected);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
    
    public void testPPSA3() 
    // Note charge calculations are based on groups instead of individual atoms
    {
        try
        {
            Mol mol = new Mol("[CH3][OH]");
            Descriptor descriptor = DescriptorFactory.getDescriptor("PPSA3");

            if( descriptor == null )
            {
                fail("Descriptor 'PPSA3' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 0.0;
            if( result.getDouble() != expected)
            {
                fail("Failed to calculate correct PPSA3 result: " +
                    result.getDouble() + " expected: " + expected);
            }
            mol = new Mol("[CH3][NH2]");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 9.455887203497051;
            if( result.getDouble() != expected)
            {
                fail("Failed to calculate correct PPSA3 result: " +
                    result.getDouble() + " expected: " + expected);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
    
    public void testFPSA3()
    {
        try
        {
            Mol mol = new Mol("CO");
            Descriptor descriptor = DescriptorFactory.getDescriptor("FPSA3");

            if( descriptor == null )
            {
                fail("Descriptor 'FPSA3' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 0.0;
            if( result.getDouble() != expected)
            {
                fail("Failed to calculate correct FPSA3 result: " +
                    result.getDouble() + " expected: " + expected);
            }
            mol = new Mol("[CH3][NH2]");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 9.455887203497051/27.64;
            if( result.getDouble() != expected)
            {
                fail("Failed to calculate correct FPSA3 result: " +
                    result.getDouble() + " expected: " + expected);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
