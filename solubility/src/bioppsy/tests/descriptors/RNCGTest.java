package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the RNCG descriptor
 ***************************************************************/
public class RNCGTest extends TestCase 
{
    static Logger logger = Logger.getLogger(RNCGTest.class);

	protected void setUp() 
    {
	}
    
    /**********************************************************
     * Tests the RNCG Descriptor.
     *********************************************************/
    public void testGetRNCG()
    { 
        try
        {
            Mol mol = new Mol("C[OH]C(C)(C)C(CC)C[OH]");
            Descriptor descriptor = DescriptorFactory.getDescriptor("RNCG");
            if( descriptor == null )
            {
                fail("Descriptor 'RNCG' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            if( result.getDouble() != 5.488984425080324E-1 )
            {
                logger.debug("Incorrectly calculated RNCG");
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
