package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;
import joelib.molecule.JOEAtom;

/****************************************************************
 * Tests the HBondAcceptors descriptor
 ***************************************************************/
public class RgyrTest extends TestCase 
{
    static Logger logger = Logger.getLogger(RgyrTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor Rgyr
     ************************************************************/
    public void testDescriptor()
    {
    	
        try
        {
        	JOEAtom atom = null;
            Descriptor descriptor = DescriptorFactory.getDescriptor("Rgyr");
            if( descriptor == null )
            {
                fail("Descriptor 'Rgyr' could not be initialised");
            }
            String smiles = "c1cccccc1";
            Mol mol = new Mol(smiles);
            int i=-2;
            for( int j=1; j <= mol.numAtoms(); ++j )
            {
                atom = mol.getAtom(j);
                atom.setVector(i++,0.0,0.0);
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 2.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of Rgyr for: " + smiles +
                     " Expected: " + expected + " Received: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
