package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the NonCarbonProportion descriptor
 ***************************************************************/
public class NonCarbonProportionTest extends TestCase 
{
    static Logger logger = Logger.getLogger(NonCarbonProportionTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor
     ************************************************************/
    public void testDescriptor()
    {
        try
        {
            Mol mol = new Mol("c1ccccc1");
            Descriptor descriptor = DescriptorFactory.getDescriptor("NonCarbonProportion");
            if( descriptor == null )
            {
                fail("Descriptor 'NonCarbonProportion' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of NonCarbonProportion for benzene. Expected: " +
                    expected + " Recieved: " + result.getDouble());
            }
            mol = new Mol("C([NH3])C([NH3])C([NH3])C([NH3])");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 0.5;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of NonCarbonProportion for C/N chain. Expected: " +
                    expected + " Recieved: " + result.getDouble());
            }
            mol = new Mol("[H]");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of NonCarbonProportion for H Expected: " +
                    expected + " Recieved: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
