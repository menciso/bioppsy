package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the descriptors.HydrophobicCarbon class
 ***************************************************************/
public class HydrophobicCarbonTest extends TestCase 
{
    static Logger logger = Logger.getLogger(HydrophobicCarbonTest.class);
    
    Database db;

	protected void setUp() 
    {
	}
    
    /**********************************************************
     * Tests the getDouble method.
     *********************************************************/
    public void testGetDouble()
    { 
        try
        {
            Descriptor descriptor = DescriptorFactory.getDescriptor("HydrophobicCarbon");
            if( descriptor == null )
            {
                fail("Descriptor null");
            }
            Mol mol = new Mol("CCCC");
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            if( result == null )
            {
                fail("Result null");
            }
            if( result.getDouble() != 4.0)
            {
                fail("Expected: 4.0 got: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
