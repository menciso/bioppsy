package bioppsy.tests.descriptors;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.descriptors.*;




//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the EAVE2 descriptor
 * The test ESTATE values are taken from Table 2 in:
 * Lowell H Hall 
 * J. Chem. Inf. Comput. Sci. 1995, 35, 1039-1045<br>
 * This article is avaliable as 
 * <a href="../../../reference/ESTATE.pdf">ESTATE.pdf</a> in docs/reference.
 * Note that only heteroatoms have been included.
 ***************************************************************/
public class EAVE2Test extends TestCase 
{
    static Logger logger = Logger.getLogger(EAVE2Test.class);
    
    Database db;
    Mol mol;

    double[] ESTATES =
    {
        1.6277777777777778,
        2.7144198790627363,
        1.4899839380196522,
        3.839722222222222,
        0.6665740740740742,
        0.2627314814814814,
       -0.8626851851851856,
        5.25425925925926,
       -1.0064814814814815,
        8.417829743008314,
        10.262534958427816
    };

	protected void setUp() 
    {
        mol = new Mol("c1[nH]cnc1CC([NH2])C([OH])(=O)");
	}
 
    /********************************************************
     * Tests that correct E-State indices are calculated
     ********************************************************/
    public void testEState()
    {
        try
        {
            String eStateKey = "Electrotopological_state_index";
            Descriptor descriptor = DescriptorFactory.getDescriptor(eStateKey);
            if( descriptor == null )
            {
                throw new DescriptorException(
                    "Descriptor " + eStateKey + " could not be initialised");
            }
            AtomDynamicResult result = (AtomDynamicResult) descriptor.calculate(mol);

            // Iterate through all resulting atoms
            // note that atomic indices start at 1
            for( int i=1; i <= result.length(); ++i )
            {
                if( result.getDoubleValue(i) != ESTATES[i-1] )
                {
                    fail("Error calculating ESTATE expected: " +
                        ESTATES[i-1] + " received: " +
                        result.getDoubleValue(i) ); 
                }
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /**********************************************************
     * Tests the getDouble method.
     *********************************************************/
    public void testGetDouble()
    { 
        try
        {
            EAVE2 eave2 = new EAVE2();
            double result = eave2.getDoubleValue(mol);

            
            double expected = 0;
            int nhetatom = 0;
            for( int i=0; i < ESTATES.length; ++i )
            {
            	if ((!mol.getAtom(i+1).isCarbon()) && (!mol.getAtom(i+1).isHydrogen())){
            	   nhetatom +=1;
                   expected += ESTATES[i];
            	}
            }
            expected = expected / nhetatom;

            if( expected != result )
            {
                fail("Failed to calculate EAVE2. Expected: " +
                    expected + " received: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /*****************************************************
     * Tests that we can obtain the descriptor
     ****************************************************/
    public void testObtainDescriptor()
    {
        try
        {
            Descriptor descriptor = DescriptorFactory.getDescriptor("EAVE2");
            if( descriptor == null )
            {
                fail("Descriptor 'EAVE2' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
