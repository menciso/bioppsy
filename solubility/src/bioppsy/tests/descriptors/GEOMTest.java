package bioppsy.tests.descriptors;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.descriptors.*;




//JOELib
import joelib.desc.*;
import joelib.desc.result.*;
import joelib.molecule.*;

/****************************************************************
 * Tests the GEOM descriptors
 ***************************************************************/
public class GEOMTest extends TestCase 
{
    static Logger logger = Logger.getLogger(GEOMTest.class);

	protected void setUp() 
    {
	}
    
    /**********************************************************
     * Tests the getMomentOfInertia method.
     *********************************************************/
    public void testGetMomentOfInertia()
    { 
        JOEAtom atom = null;

        try
        {
            GEOM geom = new GEOM();
            Mol mol = new Mol("CCCCC");

            int i=-2;
            for( int j=1; j <= mol.numAtoms(); ++j )
            {
                atom = mol.getAtom(j);
                atom.setVector(i++,0.0,0.0);
            }

            double expected = atom.getAtomicMass();
            expected *= (4 + 1 + 1 + 4 );

            double result = geom.getMomentOfInertia(mol,2);
            if( result != expected )
            {
                fail("Error calculating moment of inertia" +
                    "RESULT: " + result + " EXPECTED: " + expected);
            }

            i=-2;
            for( int j=1; j <= mol.numAtoms(); ++j )
            {
                atom = mol.getAtom(j);
                atom.setVector(i++,1.0,0.0);
            }
            result = geom.getMomentOfInertia(mol,2);
            if( result != expected )
            {
                fail("Error calculating moment of inertia " +
                     "RESULT: " + result + " EXPECTED: " + expected +
                     " probably trouble with centre of mass");
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /*************************************************************
     * Tests that the descriptors are registered correctly.
     ************************************************************/
    public void testDescriptors()
    {
        try
        {
            Mol mol = new Mol("CC(C)(C)C(CC)C");
            Descriptor descriptor = DescriptorFactory.getDescriptor("GEOM1");
            if( descriptor == null )
            {
                fail("Descriptor 'GEOM1' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            descriptor = DescriptorFactory.getDescriptor("GEOM3");
            if( descriptor == null )
            {
                fail("Descriptor 'GEOM3' could not be initialised");
            }
            result = (DoubleResult) descriptor.calculate(mol);
            
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
