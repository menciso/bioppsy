package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the HBondAcceptors descriptor
 ***************************************************************/
public class HBondAcceptorsTest extends TestCase 
{
    static Logger logger = Logger.getLogger(HBondAcceptorsTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor
     ************************************************************/
    public void testDescriptor()
    {
        try
        {
            String smiles = "c1ccccc1";
            Mol mol = new Mol(smiles);
            Descriptor descriptor = DescriptorFactory.getDescriptor("HBondAcceptors");
            if( descriptor == null )
            {
                fail("Descriptor 'HBondAcceptors' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBondAcceptors for: " + smiles +
                     " Expected: " + expected + " Recieved: " + result.getDouble());
            }
            smiles = "C(=O)";
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 1.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBondAcceptors for: " + smiles +
                     " Expected: " + expected + " Recieved: " + result.getDouble());
            }
            smiles = "c1ccncc1";
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 1.0;
            if( result.getDouble() != expected )
              if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBondAcceptors for: " + smiles +
                     " Expected: " + expected + " Recieved: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
