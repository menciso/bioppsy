package bioppsy.tests.descriptors;


//junit
import java.util.Iterator;

import jmat.data.Matrix;
import joelib.desc.DescriptorException;
import junit.framework.TestCase;

import org.apache.log4j.Logger;

import bioppsy.Constants;
import bioppsy.Database;
import bioppsy.Mol;
import bioppsy.MolSet;
import bioppsy.descriptors.Topological;
import bioppsy.descriptors.VAA1;
import bioppsy.descriptors.VAD1;
import bioppsy.descriptors.VEA1;
import bioppsy.descriptors.VED1;
import bioppsy.descriptors.VRA1;
import bioppsy.descriptors.VRD1;
import bioppsy.descriptors.VRD1sq;

/****************************************************************
 * Tests the RDCHI2 descriptors
 ***************************************************************/
public class RandicTest extends TestCase 
{
    static Logger logger = Logger.getLogger(RandicTest.class);
    
    Database db;

	protected void setUp() 
    {
	}
    
    /**********************************************************
     * Tests the getDistanceMatrix method.
     *********************************************************/
    public void testGetDistanceMatrix()
    { 
        try
        {
            Mol mol = new Mol("CCOC");
            Matrix result = Topological.getTopologicalDistanceMatrix(mol);
            int[][] compare = {{0,1,2,3},{1,0,1,2},{2,1,0,1},{3,2,1,0}};
            for( int i=0; i < 4; ++i )
            {
                for( int j=0; j < 4; ++j )
                {
                    assertEquals(compare[i][j],(int) result.get(i, j));
                }
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /**********************************************************
     * Tests the getVAD1 method.
     *********************************************************/
    public void testGetVAD1()
    { 
        try
        {
            VAD1 vad1 = new VAD1();


            double result =0.0;
            double expected = 0.0;
            
            Mol mol = new Mol("CC(C)C(C)CC");

            result = vad1.getVAD1(mol);
            expected = 13.634622026730796;
            if( result !=  expected )
            {
                fail("Failed to get VAD1 for 2,3-dimethylpentane. \n Expected: " + expected + ". Result: " + result);
            }
            mol = new Mol("CCCCCCCC");
            result = vad1.getVAD1(mol);
            expected = 21.836353346989743;
            if( result !=  expected )
            {
                fail("Failed to get VAD1 for octane. \n Expected: " + expected + ". Result: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /**********************************************************
     * Tests the getVRD1 method.
     *********************************************************/
    public void testGetVRD1()
    { 
        try
        {
            VRD1 vrd1 = new VRD1();


            double result =0.0;
            double expected = 0.0;
            
            Mol mol = new Mol("CC(C)C(C)CC");

            result = vrd1.getVRD1(mol);
            expected = 18.193985305373324;
            if( result !=  expected )
            {
                fail("Failed to get VRD1 for 2,3-dimethylpentane. \n Expected: " + expected + ". Result: " + result);
            }
            mol = new Mol("CCCCCCCC");
            result = vrd1.getVRD1(mol);
            expected = 21.433509211491085;
            if( result !=  expected )
            {
                fail("Failed to get VRD1 for octane. \n Expected: " + expected + ". Result: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
    
    /**********************************************************
     * Tests the getVRD1sq method.
     *********************************************************/
    public void testGetVRD1sq()
    { 
        try
        {
            VRD1sq vrd1sq = new VRD1sq();


            double result =0.0;
            double expected = 0.0;
            
            Mol mol = new Mol("CC(C)C(C)CC");

            result = vrd1sq.getVRD1sq(mol);
            expected = Math.pow(18.193985305373324,2);
            if( result !=  expected )
            {
                fail("Failed to get VRD1 for 2,3-dimethylpentane. \n Expected: " + expected + ". Result: " + result);
            }
            mol = new Mol("CCCCCCCC");
            result = vrd1sq.getVRD1sq(mol);
            expected = Math.pow(21.433509211491085,2);
            if( result !=  expected )
            {
                fail("Failed to get VRD1 for octane. \n Expected: " + expected + ". Result: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
    
    /**********************************************************
     * Tests the getVED1 method.
     *********************************************************/
    public void testGetVED1()
    { 
        try
        {
            VED1 ved1 = new VED1();


            double result =0.0;
            double expected = 0.0;
            
            Mol mol = new Mol("CC(C)C(C)CC");

            result = ved1.getVED1(mol);
            expected = 2.6026709766527394;
            if( result !=  expected )
            {
                fail("Failed to get VED1 for 2,3-dimethylpentane. \n Expected: " + expected + ". Result: " + result);
            }
            mol = new Mol("CCCCCCCC");
            result = ved1.getVED1(mol);
            expected = 2.7824407013692642;
            if( result !=  expected )
            {
                fail("Failed to get VRD1 for octane. \n Expected: " + expected + ". Result: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
    
    /**********************************************************
     * Tests the getVAA1 method.
     *********************************************************/
    public void testGetVAA1()
    { 
        try
        {
            VAA1 vaa1 = new VAA1();


            double result =0.0;
            double expected = 0.0;
            
            Mol mol = new Mol("CC(C)C(C)CC");

            result = vaa1.getVAA1(mol);
            expected = 3.8314939575189366;
            if( result !=  expected )
            {
                fail("Failed to get VED1 for 2,3-dimethylpentane. \n Expected: " + expected + ". Result: " + result);
            }
            mol = new Mol("CCCCCCCC");
            result = vaa1.getVAA1(mol);
            expected = 4.758770483143634;
            if( result !=  expected )
            {
                fail("Failed to get VRD1 for octane. \n Expected: " + expected + ". Result: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /**********************************************************
     * Tests the getVEA1 method.
     *********************************************************/
    public void testGetVEA1()
    { 
        try
        {
            VEA1 vea1 = new VEA1();


            double result =0.0;
            double expected = 0.0;
            
            Mol mol = new Mol("CC(C)C(C)CC");

            result = vea1.getVEA1(mol);
            expected = 2.4598318098164325;
            if( result !=  expected )
            {
                fail("Failed to get VEA1 for 2,3-dimethylpentane. \n Expected: " + expected + ". Result: " + result);
            }
            mol = new Mol("CCCCCCCC");
            result = vea1.getVEA1(mol);
            expected = 2.6734678884477763;
            if( result !=  expected )
            {
                fail("Failed to get VEA1 for octane. \n Expected: " + expected + ". Result: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /**********************************************************
     * Tests the getVRA1 method.
     *********************************************************/
    public void testGetVRA1()
    { 
        try
        {
            VRA1 vra1 = new VRA1();


            double result =0.0;
            double expected = 0.0;
            
            Mol mol = new Mol("CC(C)C(C)CC");

            result = vra1.getVRA1(mol);
            expected = 15.711648794723304;
            if( result !=  expected )
            {
                fail("Failed to get VRA1 for 2,3-dimethylpentane. \n Expected: " + expected + ". Result: " + result);
            }
            mol = new Mol("CCCCCCCC");
            result = vra1.getVRA1(mol);
            expected = 21.482988360530875;
            if( result !=  expected )
            {
                fail("Failed to get VRA1 for octane. \n Expected: " + expected + ". Result: " + result);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
    
    /*******************************************************
     * Makes sure that we never get NaN as a result
     ******************************************************/
    public void testAlwaysANumber()
    {
    	VAD1 vad1 = new VAD1();
        Double result;
        MolSet molSet = new MolSet(
                Constants.instance().getString("TEST_SDF_FILENAME"));
        Iterator i = molSet.getIterator();
        while( i.hasNext() )
        {
            try
            {
                Mol mol = (Mol) i.next();
                result = new Double(vad1.getVAD1(mol));
                if( result.isNaN() )
                {
                    fail("Received NaN from VAD1");
                }
            }
            catch( DescriptorException de )
            {
                fail(de.getMessage());
            }
        }
    }
}
