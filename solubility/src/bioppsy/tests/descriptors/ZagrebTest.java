package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the Zagreb descriptor
 ***************************************************************/
public class ZagrebTest extends TestCase 
{
    static Logger logger = Logger.getLogger(ZagrebTest.class);
    
    Database db;

	protected void setUp() 
    {
	}

    /*****************************************************
     * Tests the getDouble() method
     ****************************************************/
    public void testGetDouble()
    {
        try
        {
            Mol mol = new Mol("CC(C)(C)C(CC)C");
            Descriptor descriptor = DescriptorFactory.getDescriptor("Zagreb");
            if( descriptor == null )
            {
                fail("Descriptor 'Zagreb' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 34.0; // 1+4^2+1+1+3^2+2^2+1+1=34
            if( result.getDouble() != expected )
            {
                fail("Failed to calculate Zagreb recieved: " + 
                    result.getDouble() +
                    " expected: " + expected);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
