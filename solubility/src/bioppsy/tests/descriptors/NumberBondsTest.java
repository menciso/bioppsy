package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the NumberBonds descriptors
 ***************************************************************/
public class NumberBondsTest extends TestCase 
{
    static Logger logger = Logger.getLogger(NumberBondsTest.class);
    
    Database db;

	protected void setUp() 
    {
	}
    
    /**********************************************************
     * Tests the NumberSingleBonds descriptor.
     *********************************************************/
    public void testNumberSingleBonds()
    { 
        try
        {
            Descriptor descriptor = DescriptorFactory.getDescriptor("NumberSingleBonds");
            if( descriptor == null )
            {
                fail("Descriptor null");
            }
            Mol mol = new Mol("CCCC");
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 3.0;
            if( result.getDouble() != expected )
            {
                fail("Expected: " + expected + " received: " + result.getDouble());
            }
            mol = new Mol("c1ccccc1");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Expected: " + expected + " received: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }

    /**********************************************************
     * Tests the NumberDoubleBonds descriptor.
     *********************************************************/
    public void testNumberDoubleBonds()
    { 
        try
        {
            Descriptor descriptor = DescriptorFactory.getDescriptor("NumberDoubleBonds");
            if( descriptor == null )
            {
                fail("Descriptor null");
            }
            Mol mol = new Mol("C=CCC");
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 1.0;
            if( result.getDouble() != expected )
            {
                fail("Expected: " + expected + " received: " + result.getDouble());
            }
            mol = new Mol("c1ccccc1");
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Expected: " + expected + " received: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
