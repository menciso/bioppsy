package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the HBondDonors descriptor
 ***************************************************************/
public class HBondDonorsTest extends TestCase 
{
    static Logger logger = Logger.getLogger(HBondDonorsTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor
     ************************************************************/
    public void testDescriptor()
    {
        try
        {
            String smiles = "CCCCCCCCCCCC";
            Mol mol = new Mol(smiles);
            Descriptor descriptor = DescriptorFactory.getDescriptor("HBondDonors");
            if( descriptor == null )
            {
                fail("Descriptor 'HBondDonors' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            double expected = 0.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBondDonors for: " + smiles +
                     " Expected: " + expected + " Recieved: " + result.getDouble());
            }
            smiles = "C([NH])C([NH2])C([OH])C";
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 3.0;
            if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBondDonors for: " + smiles +
                     " Expected: " + expected + " Recieved: " + result.getDouble());
            }
            smiles = "c1cc[nH]cc1";
            mol = new Mol(smiles);
            result = (DoubleResult) descriptor.calculate(mol);
            expected = 1.0;
            if( result.getDouble() != expected )
              if( result.getDouble() != expected )
            {
                fail("Incorrect calculation of HBondDonors for: " + smiles +
                     " Expected: " + expected + " Recieved: " + result.getDouble());
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
