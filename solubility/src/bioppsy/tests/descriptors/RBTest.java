package bioppsy.tests.descriptors;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/****************************************************************
 * Tests the RB descriptor
 ***************************************************************/
public class RBTest extends TestCase 
{
    static Logger logger = Logger.getLogger(RBTest.class);

	protected void setUp() 
    {
	}

    /*************************************************************
     * Tests the descriptor
     ************************************************************/
    public void testDescriptor()
    {
        try
        {
            Mol mol = new Mol("CC(C)(C)C(CC)C[NH3]");
            Descriptor descriptor = DescriptorFactory.getDescriptor("RB");
            if( descriptor == null )
            {
                fail("Descriptor 'RB' could not be initialised");
            }
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            if( result.getDouble() != 6.0 )
            {
                fail("Received: " + result.getDouble() + " expected: " + 6.0);
            }
        }
        catch( DescriptorException de )
        {
            fail(de.getMessage());
        }
    }
}
