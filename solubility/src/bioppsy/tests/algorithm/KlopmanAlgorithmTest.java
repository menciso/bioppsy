package bioppsy.tests.algorithm;


//junit
import junit.framework.*;
//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.*;



/****************************************************************
 * Tests the KlopmanAlgorithm class
 ***************************************************************/
public class KlopmanAlgorithmTest extends TestCase {

    private Database db = null;

    static Logger logger = Logger.getLogger(KlopmanAlgorithmTest.class);

	protected void setUp() {
	}

    protected void tearDown() 
    {
    }

    /****************************************************************
     * Tests the calculateLogSStar(double logS) method
     ***************************************************************/
    public void testCalculateLogSStar()
    {
        double logS = -1;
        double result = Klopman.calculateLogSStar(logS);
        if( logS != result )
        {
            fail("Incorrectly calculated logS*, received: " +
                result + " expected: " + logS);
        }
        if( Klopman.calculateLogSStar(2.0) <= 2.0 )
        {
            fail("logS* is not an increasing function at 2.0");
        }
        if( Klopman.calculateLogSStar(-5.0) >= -5.0 )
        {
            fail("logS* is not a decreasing function at -5.0");
        }
    }

    /****************************************************************
     * Tests the calculateLogS(double logS) method
     ***************************************************************/
    public void testCalculateLogS()
    {
        double logSStar = -1;
        double logS = Klopman.calculateLogS(logSStar);
        if( logSStar != logS )
        {
            fail("Incorrectly calculated logS, received: " +
                logS + " expected: " + logSStar);
        }
        logS = Klopman.calculateLogS(-5.0);
        logSStar = Klopman.calculateLogSStar(logS);
        if( logSStar != -5.0 )
        {
           fail("Converted -5.0 to logS then back to logSStar. Received: " +
               logSStar);
        }
    }
}
