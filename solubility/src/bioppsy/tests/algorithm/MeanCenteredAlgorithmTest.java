package bioppsy.tests.algorithm;


//junit
import junit.framework.*;
//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.*;
import bioppsy.util.Stats;


//Jama
import Jama.Matrix;
// java
import java.util.Iterator;
import java.util.Vector;

/****************************************************************
 * Tests the MeanCenteredAlgorithm class
 ***************************************************************/
public class MeanCenteredAlgorithmTest extends TestCase {

    private Database db = null;

    static Logger logger = Logger.getLogger(MeanCenteredAlgorithmTest.class);

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
        if( db != null )
        {
            db.close();
        }
    }

    /****************************************************************
     * Tests the getY method
     ***************************************************************/
    public void testGetY()
    {
        int i=0;

        MolSet molSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME") );
        MeanCentered mca = new MeanCentered(molSet,"mca");
        int size = molSet.size();
        Matrix Y = mca.getY(size);
        if( Y == null )
        {
            fail("getExpPropMatrix( ) returned null");
        }

        Vector values = new Vector();
        for( i=0; i < size; ++i )
        {
            values.add(new Double(Y.get(i,0)));
        }

        double nearlyZero = 1E-10;
        double mean = Stats.mean(values);

        if( Math.abs(mean) > nearlyZero )
        {
            fail("Mean of mean centered property values is not close to zero: " +
                mean);
        }
    }

    /****************************************************************
     * Tests the getX method
     ***************************************************************/
    public void testGetX()
    {
        int i=0;
        int j=0;

        MolSet molSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME") );
        MeanCentered mca = new MeanCentered(molSet,"mca");

        mca.addIntercept();
        String filename = Constants.instance().getString("HOU_SMARTS_FILENAME");
        filename = "/" + filename;
        java.io.InputStream inputStream = getClass().getResourceAsStream(filename); 
        if( !mca.addSmarts(inputStream) )
        {
            fail("Unable to add SMARTS to algorithm");
        }

        int size = molSet.size();
        int k = mca.getNumberParameters()-1;
        Matrix X = mca.getX(size,k);
        if( X == null )
        {
            fail("calculateOccurrence( ) returned null");
        }

        for( j=0; j <= k; ++j )
        {
            Vector values = new Vector();
            for( i=0; i < size; ++i )
            {
                values.add(new Double(X.get(i,j)));
            }

            double nearlyZero = 1E-10;
            double mean = Stats.mean(values);

            if( mca.getParamList().paramAt(j).isIntercept() )
            {
                if( mean != 1.0 )
                {
                    fail("Mean of intercept is: " + mean + " should be 1.0");
                }
            }
            else if( Math.abs(mean) > nearlyZero )
            {
                fail("Mean of mean centered values is not close to zero: " +
                    mean + " for column: " + j);
            }
        }
    }

    /****************************************************************
     * Tests the calulcateProperty method
     ***************************************************************/
    public void testCalculateProperty()
    {
        MolSet molSet = new MolSet(
            Constants.instance().getString("TEST_SDF_FILENAME") );
        MeanCentered mca = new MeanCentered(molSet,"mca");

        mca.addIntercept();
        String filename = Constants.instance().getString("HOU_SMARTS_FILENAME");
        filename = "/" + filename;
        java.io.InputStream inputStream = getClass().getResourceAsStream(filename); 
        if( !mca.addSmarts(inputStream) )
        {
            fail("Unable to add SMARTS to algorithm");
        }

        if( !mca.calculateParameters() )
        {
            fail("Unable to calculate Parameters");
        }

        MolSet testMolSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME") );

        Iterator i = testMolSet.getIterator();
        while( i.hasNext() )
        {
            Mol mol = (Mol) i.next();
            Result result = new Result();
            if( mca.calculateProperty(mol,result) == null )
            {
                fail("Failed to calculate property for Mol: " +
                    mol.getNumber());
            }
            if( Math.abs(result.getValue()) > 14.0 )
            {
                fail("Extreme property calculated: " + result.getValue());
            }
        }
    }
}
