package bioppsy.tests.algorithm;

//junit
import junit.framework.*;
//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.*;


// java
import java.util.Iterator;

/****************************************************************
 * Tests the MCUVAlgorithm class
 ***************************************************************/
public class MCUVAlgorithmTest extends TestCase {

    private Database db = null;

    static Logger logger = Logger.getLogger(MCUVAlgorithmTest.class);

	protected void setUp() 
    {
	}

    protected void tearDown() 
    {
    }

    /****************************************************************
     * Tests the calculateProperty method
     ***************************************************************/
    public void testCalculateProperty()
    {
        MolSet molSet = new MolSet(
            Constants.instance().getString("TEST_SDF_FILENAME") );
        MCUV mca = new MCUV(molSet,"mca");

        mca.addIntercept();
        String filename = Constants.instance().getString("HOU_SMARTS_FILENAME");
        filename = "/" + filename;
        java.io.InputStream inputStream = getClass().getResourceAsStream(filename); 
        if( !mca.addSmarts(inputStream) )
        {
            fail("Unable to add SMARTS to algorithm");
        }

        if( !mca.calculateParameters() )
        {
            fail("Unable to calculate Parameters");
        }

        MolSet testMolSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME") );

        Iterator i = testMolSet.getIterator();
        while( i.hasNext() )
        {
            Mol mol = (Mol) i.next();
            Result result = new Result();
            if( mca.calculateProperty(mol,result) == null )
            {
                fail("Failed to calculate property for Mol: " +
                    mol.getNumber());
            }
            if( (new Double(result.getValue())).isNaN() )
            {
                fail("NaN property calculated for mol: " + mol.getNumber());
            }
            if( Math.abs(result.getValue()) > 14.0 )
            {
                fail("Extreme property calculated: " + result.getValue() +
                    " for mol: " + mol.getNumber());
            }
        }
    }
}
