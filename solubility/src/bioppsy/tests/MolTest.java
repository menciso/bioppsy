package bioppsy.tests;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.Constants;
import bioppsy.Mol;
import bioppsy.MolSet;
import bioppsy.NotFoundException;


import joelib.molecule.JOEAtom;

import java.util.Vector;
import java.util.Iterator;

/*************************************************************
 * Tests the Mol class.
 ************************************************************/
public class MolTest extends TestCase 
{
    MolSet molSet;

    static Logger logger = Logger.getLogger(MolTest.class);

	protected void setUp() 
    {
        molSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
	}

    protected void tearDown() 
    {
    }

    /*******************************************************
     * Tests the Mol.getDesciptor() method and the 
     * Mol.getCAS() method.
     *******************************************************/
    public void testGetDescriptor()
    {
        Iterator i = molSet.getIterator();
        
        while( i.hasNext() )
        {
            Mol mol = (Mol) i.next();

            String cas = mol.getDescriptor(
                Constants.instance().getString("CAS_KEY2"));
            if( cas == null )
            {
                fail("Failed to get descriptor CAS_NUMBER from: " +
                    Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
            }
            if( !cas.equals(mol.getMolId()) )
            {
                fail("getCAS() returned: " + mol.getMolId() + " we got: " +
                    cas );
            }

            String expProp = mol.getDescriptor("Property");
            if( expProp == null )
            {
                fail("Failed to get descriptor Property from: " +
                    Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
            }
        }

        logger.debug("Successfully tested Mol.getDescriptor()");
    }

    /*******************************************************
     * Tests the Mol.getExpSol() method.
     *******************************************************/
    public void testGetExpProp()
    {
        Mol mol;

        MolSet msFileProp = new MolSet(
            Constants.instance().getString("DRUGS_MOL2_FILENAME_TEST"));

        Iterator i = molSet.getIterator();
        
        while( i.hasNext() )
        {
            mol = (Mol) i.next();

            try
            {
                double expProp = mol.getExpProp();
            }
            catch( NotFoundException nfe )
            {
                fail(nfe.getMessage());
            }
        }

        // make sure we don't get expSol values before
        // we have set the expSol file
        i = msFileProp.getIterator();
        while( i.hasNext() )
        {
            mol = (Mol) i.next();
            try
            {
                double expProp = mol.getExpProp();
                fail(expProp + "ExpProp value mistakingly retrieved for mol: " +
                    mol.getNumber());
            }
            catch(NotFoundException nfe)
            { 
               // this is what we want
            }
        }

        if( !msFileProp.setExpPropFile(
            Constants.instance().getString("DRUGS_SOL_FILENAME_TEST")) )
        {
            fail("Failed to set ExpSol file");
        }

        i = msFileProp.getIterator();
        
        while( i.hasNext() )
        {
            mol = (Mol) i.next();
            try
            {
                double expProp = mol.getExpProp();
                if( expProp == 0.0 )
                {
                    fail("Zero Property found");
                }
            }
            catch(NotFoundException nfe)
            {
                logger.debug(nfe.getMessage());
            }
        }

        logger.debug("Successfully tested Mol.getDescriptor()");
    }

    /****************************************************************
     * Tests the numHydrophobic() method
     ***************************************************************/
    public void testNumHydrophobic()
    {
        Mol mol = new Mol("C(C=CCCCC(=O)O)C1C(=O)CC(O)C1C=CC(O)CCCCC");

        int numHyd = mol.numHydrophobic();
        assertEquals(6,numHyd);
    }

    /****************************************************************
     * Tests the getCarbons() method
     ***************************************************************/
    public void testGetCarbons()
    {
        Mol mol = new Mol("C(C=CCCCC(=O)O)C1C(=O)CC(O)C1C=CC(O)CCCCC");

        Vector carbons = mol.getCarbons();
        for( int i=0; i < carbons.size(); ++i )
        {
            JOEAtom atom = (JOEAtom) carbons.elementAt(i);
            assertEquals(atom.getAtomicNum(),6);
            int hyb = atom.getHyb();
            if( hyb == 2 )
            {
                if( atom.isAromatic() || atom.isInRing() )
                {
                    fail("Got an Aromatic or Ring Carbon");
                }
            }
            else if ( hyb != 3 )
            {
                fail("Carbon not sp3 or sp2");
            }
        }
    }

    /*****************************************************************
     * Tests the getHeteroatoms() method
     ***************************************************************/
    public void testGetHeteroatoms()
    {
        Mol mol = new Mol("C(C=CCCCC(=O)O)C1C(=O)CC(O)C1C=CC(O)CCCCC");

        Vector heteros = mol.getHeteroatoms();
        for( int i=0; i < heteros.size(); ++i )
        {
            JOEAtom atom = (JOEAtom) heteros.elementAt(i);
            int num = atom.getAtomicNum();
            if( num == 1 || num == 6 )
            {
                fail("Carbon counted as a heteroatom");
            }
        }
    }
}
