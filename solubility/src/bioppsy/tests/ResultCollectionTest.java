package bioppsy.tests;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;


//java
import java.sql.SQLException;

/*************************************************************
 * Tests the ResultCollection class.
 ************************************************************/
public class ResultCollectionTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(ResultCollectionTest.class);

    MLR alg;
    MolSet molSet;
    ResultCollection rc;
    Result result;
    Mol mol;

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
            molSet = new MolSet("fname");
            alg = new MLR(molSet,"algorithm");
            rc = new ResultCollection(alg,molSet);
            mol = new Mol("CCCCCCCCCC");
            mol.setNumber(1234);
            mol.setMolSet(molSet);
            result = new Result(rc,mol,3.564);
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
    }

    public void testAll() 
    {
        rc.add(result);
        Mol mol2 = new Mol("CCCCCCCCCC");
        mol2.setNumber(2);
        mol2.setMolSet(molSet);
        Result result2 = new Result(rc,mol2,8.39);
        rc.add(result2);
        int preSize = rc.size();
       
        try
        {
            if( !rc.save(db) )
            {
               fail("Unable to save ResultCollection");
            }
            if(preSize!=2)
            {
            	fail("Wrong size of ResultCollection");
            }
            if( !result.exists(db) )
            {
               fail("result does not exist after saving ResultCollection");
            }
            if( !result2.exists(db) )
            {
               fail("result2 does not exist after saving ResultCollection");
            }
            if( !rc.delete(db) )
            {
                fail("Failed to delete ResultCollection");
            }
            if( result.exists(db) )
            {
                fail("Deleting ResultCollection did not delete contained result");
            }
            if( result2.exists(db) )
            {
                fail("Deleting ResultCollection did not delete contained result2");
            }
            assertEquals(0,rc.size());
        }
        catch(SQLException sqle)
        {
            fail(sqle.getMessage());
        }
    }
}
