package bioppsy.tests;

import junit.framework.*;
import org.apache.log4j.PropertyConfigurator;

import bioppsy.Constants;



import java.net.URL;

/***************************************************************
 * TestSuite that runs all the sample tests
 ***************************************************************/
public class AllTests {

	public static void main (String[] args) {

        String filename = Constants.instance().getString("TEST_LOG_CONFIG_FILE");
        // make filename absolute
        filename = "/" + filename;
        // get URL of resource using absolute filename
        URL url = AllTests.class.getResource(filename);
        // Configures the logger using a configuration file
        PropertyConfigurator.configure(url);

		junit.textui.TestRunner.run (suite());
	}
	public static Test suite ( ) {

		TestSuite suite= new TestSuite("Solubility JUnit Tests");
	    suite.addTest(new TestSuite(bioppsy.tests.ExampleTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.util.UtilTest.class));
        // note the DatabaseTest suite drops all the tables in the test database
        suite.addTest(new TestSuite(bioppsy.tests.DatabaseTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.MolTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.MolSetTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.io.ExpPropFileTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.ParamTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.ATParamTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.DescriptorParamTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.ParamListTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.AlgorithmTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.ResultTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.ResultCollectionTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.ResultCollectionNavigatorTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.HydrophobicCarbonTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.NumberBondsTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.MDETest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.EAVE2Test.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.GEOMTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.GRAVTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.RNCGTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.CTDHTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.RBTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.APTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.NonCarbonProportionTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.HBondAcceptorsTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.HBondDonorsTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.algorithm.KlopmanAlgorithmTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.WienerTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.ZagrebTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.descriptors.SmartsPatternTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.algorithm.MeanCenteredAlgorithmTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.util.StatsTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.algorithm.MCUVAlgorithmTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.util.SaveMatrixTest.class));
        suite.addTest(new TestSuite(bioppsy.tests.AlgorithmMatriciesTest.class));
        // add any additional test classes here
	    return suite;
	}
}
