package bioppsy.tests;

//junit
import java.io.FileNotFoundException;

import joelib.molecule.JOEMol;
import joelib.smiles.JOESmilesParser;
import junit.framework.TestCase;

import org.apache.log4j.Logger;

import bioppsy.Constants;
import bioppsy.util.IniFileReader;



import Jama.Matrix;

public class ExampleTest extends TestCase {

    private String connString = null;
    private String driverString = null;

    // Define a static logger variable so that it references the
    // Logger instance named "ExampleTest".
    static Logger logger = Logger.getLogger(ExampleTest.class);

	protected void setUp() {
        try 
        {
            // read in the database parameters from the
            // general ini file
            IniFileReader ifr = new IniFileReader(
                Constants.instance().getString("DATABASE_INI_FILENAME"));
            ifr.read();
            connString = ifr.getString("CONN_STRING");
            driverString = ifr.getString("DRIVER");
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error(fnfe.getMessage());
        }
	}
	public void testFirstTest() {

        logger.info("Testing correct libraries present");
        logger.info("JUnit and log4j functional");

        JOEMol mol=new JOEMol();
        String smiles="C(C=CCCCC(=O)O)C1C(=O)CC(O)C1C=CC(O)CCCCC";
        if (!JOESmilesParser.smiToMol(mol, smiles, "new molecule"))
        { 
            logger.error("SMILES entry \"" + smiles + "\" could not be loaded.");
            fail("JOElib not functioning correctly");
        }
        logger.info("JOElib functioning correctly");

        double[][] vals = {{1.,2.,3},{4.,5.,6.},{7.,8.,10.}};
        Matrix A = new Matrix(vals);
        logger.info("Jama functioning correctly");
/*
        // check we can connect to the database
        try 
        { 
            Class.forName(driverString).newInstance();
            logger.debug(connString);
            Connection conn = 
                DriverManager.getConnection(connString);
            conn.close();
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
        logger.info("Database functioning correctly");
*/
	}

}
