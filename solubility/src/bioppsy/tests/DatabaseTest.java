package bioppsy.tests;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.Constants;
import bioppsy.Database;
import bioppsy.FailedConstructorException;


//java
import java.sql.SQLException;

/****************************************************************
 * Tests the Database class
 ***************************************************************/
public class DatabaseTest extends TestCase 
{
    // Define a static logger variable so that it references the
    // Logger instance named "DatabaseTest".
    static Logger logger = Logger.getLogger(DatabaseTest.class);
    
    Database db = null;

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
        }
        catch( FailedConstructorException fce )
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown()
    {
        if( db != null )
        {
            db.close();
        }
    }
    
    /**********************************************************
     * Tests the Database constructor. Warning, this drops
     * all the tables in the database and all data is deleted.
     *********************************************************/
    public void testDASCreate()
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
            if( db.deleteDatabase() ) 
            {
                logger.debug("Database tables have been deleted");
            }
            if( db.createDatabase() ) 
            {
                logger.debug("Database tables have just been created");
            }
            else
            {
                logger.debug("Database tables already exist");
            }
        }
        catch( FailedConstructorException fce )
        {
            fail(fce.getMessage());
        }
    }

    /******************************************************************
     * Tests the database connection functionality
     *****************************************************************/
    public void testConnection()
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
            db.close();
            try
            {
                db.executeQuery("SELECT * FROM Algorithm");
            }
            catch( SQLException sqle )
            {
                logger.debug("Database did not try and reconnect after failed query");
            }
            try
            {
                db.execute("SELECT * FROM Algorithm");
            }
            catch( SQLException sqle )
            {
                logger.debug("Database did not try and reconnect after failed operation");
            }
        }
        catch( FailedConstructorException fce )
        {
            fail(fce.getMessage());
        }   
    }

    /********************************************************************
     * Tests inserting and searching the Result table. This test 
     * shows how performing a SELECT and then INSERT in succession
     * gets very slow as the number of rows increases.
     *******************************************************************/
    public void testResultTable()
    {
/*
        try
        {
            long startTotal = System.currentTimeMillis();
            for( int j=0; j < 100; ++j )
            {
                long start = System.currentTimeMillis();
                for( int i=0; i < 1000; ++i )
                {
                    String query =
                        "SELECT id FROM Result WHERE " +
                        " algorithm='algorithm'" +
                       " AND molSet=" + j +
                        " AND molNumber=" + i + ";";
                    db.executeQuery(query);
                    query = 
                        "INSERT INTO Result(algorithm,molSet,molNumber) " +
                        "VALUES('algorithm'," + j + "," + i + ");";
                    db.execute(query);
                }
                long end = System.currentTimeMillis();
                logger.debug(j + " TIME TAKEN: " + (end - start));
            }
            long endTotal = System.currentTimeMillis();
            logger.debug("Total time: " + (endTotal-startTotal));
        }
        catch( SQLException sqle )
        {
            fail(sqle.getMessage());
        }
*/
    }
}
