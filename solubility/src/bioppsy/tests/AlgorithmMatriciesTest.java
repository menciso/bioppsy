package bioppsy.tests;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;
import bioppsy.util.SaveMatrix;


//Jama
import Jama.Matrix;

/*************************************************************
 * Tests the AlgorithmMatrices class.
 ************************************************************/
public class AlgorithmMatriciesTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(AlgorithmMatriciesTest.class);

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
        if( db != null )
        {
            db.close();
        }
    }

    /*******************************************************
     * Tests the whole class all in one method
     ******************************************************/
    public void testEveything()
    {
        int rows = 13;
        int columns = 13;
        Matrix M = new Matrix(rows,columns);
        for( int i=0; i < rows; ++i )
        { 
            for( int j=0; j < columns; ++j )
            {
                M.set(i,j,i+j);
            }
        }
        String[] keys = {"SM1","SM2","SM3"};
        MLR alg = new MLR();
        alg.setName("algorithm");
        SaveMatrix SM1 = new SaveMatrix(alg,keys[0],M,rows,columns);
        SaveMatrix SM2 = new SaveMatrix(alg,keys[1],M,rows,columns);
        SaveMatrix SM3 = new SaveMatrix(alg,keys[2],M,rows,columns);

        AlgorithmMatrices am = new AlgorithmMatrices(alg);
        am.put(keys[0],SM1);
        am.put(keys[1],SM2);
        am.put(keys[2],SM3);

        if( !am.save(db) )
        {
            fail("Unable to save AlgorithmMatrices");
        }

        AlgorithmMatrices ret = new AlgorithmMatrices(alg);
        if( !ret.load(db) )
        {
            fail("Unable to load AlgorithmMatrices");
        }
        for( int i=0; i < keys.length; ++i )
        {
            SaveMatrix smRet = ret.get(keys[i]);
            if( smRet == null )
            {
                fail("Save Matrix null: " + keys[i]);
            }
            assertEquals(rows,smRet.getRows());
            assertEquals(columns,smRet.getColumns());
            Matrix retM = smRet.getMatrix();
            for( int k=0; k < rows; ++k )
            {
                for( int j=0; j < columns; ++j )
                {
                    if( M.get(k,j) != retM.get(k,j) )
                    {
                       fail("Loaded value different to saved");
                    }
                }
            }
        }

        if( !am.delete(db) )
        {
            fail("Unable to delete AlgorithmMatrices");
        }
    }
}
