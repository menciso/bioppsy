package bioppsy.tests;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;
import bioppsy.util.Notifier;


//java
import java.sql.SQLException;
import java.util.Vector;
import java.util.Iterator;

//Jama
import Jama.Matrix;

/*************************************************************
 * Tests the Algorithm class.
 ************************************************************/
public class AlgorithmTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(AlgorithmTest.class);

    MLR alg;
    MolSet molSet;

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
            molSet = new MolSet(
                Constants.instance().getString("TEST_SDF_FILENAME"));
            alg = new MLR(molSet,"algorithm");
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
        alg.delete(db);
    }

    /*******************************************************************
     * Tests the getUnmatchedAtoms() method
     ******************************************************************/
    public void testGetUnmatchedAtoms()
    {
        MLR unmatchedAlg = new MLR();
        ParamList umPL = unmatchedAlg.getParamList();
        ATParam atParam = new ATParam(unmatchedAlg);
        String pattern = "[OH]";
        atParam.addSmarts(pattern);
        umPL.add(atParam);
        ATParam atParam2 = new ATParam(unmatchedAlg);
        String pattern2 = "[N]";
        atParam2.addSmarts(pattern2);
        String pattern3 = "[Cl]";
        atParam2.addSmarts(pattern3);
        umPL.add(atParam2);
        String molSmarts = "CCC[NH3]CCCC[Cl]CCCC[OH]";
        int numCarbon = 11;
        Mol unmatchedMol = new Mol(molSmarts);
        Vector unmatchedAtoms = unmatchedAlg.getUnmatchedAtoms(unmatchedMol);
        assertEquals(numCarbon,unmatchedAtoms.size());
    }

    /***********************************************************
     * Tests all the database methods.
     ***********************************************************/
    public void testDatabaseMethods()
    {
        double rsq = 4.874;

        MLR ret;

        try
        {
/** Commented-out because Algorithm.initParameters not implemented
            if( !alg.initParameters(
                Constants.instance().getString("TEST_SMARTS_FILENAME")) )
            {
                fail("Failed to initialise SMARTS");
            }
**/
        	if( alg.exists(db) )
            {
                fail("Algorithm exists before being saved");
            }
            if( !alg.modified() )
            {
                fail("Algorithm not modified after creation");
            }
            if( !alg.save(db) )
            {
                fail("Unable to save Algorithm");
            }
            if( alg.modified() )
            {
                fail("Algorithm modified after saving");
            }

            ret = MLR.getAlgorithm(alg.getName(),db);
            if( ret == null )
            {
                fail("Unable to load Algorithm first");
            }
            if( ret.modified() )
            {
                fail("Loaded Algorithm set to modified");
            }
            if( !ret.getParamList().equals(alg.getParamList()) )
            {
               fail("Loaded ParamList different to saved ParamList");
            }
            if( ret.getRsq() != alg.getRsq() )
            {
               fail("Loaded rsq different to saved rsq");
            }
            if( ret.getSdDev() != alg.getSdDev() )
            {
               fail("Loaded SdDev different to saved SdDev");
            }

            alg.setRsq(rsq);
            if( !alg.modified() )
            {
                fail("Algorithm not modified after changing rsq");
            }
            if( !alg.save(db) )
            {
                fail("Not able to save Algorithm a second time");
            }

            ret = MLR.getAlgorithm(alg.getName(),db);
            if( ret == null )
            {
                fail("Unable to load Algorithm second");
            }
            if( ret.modified() )
            {
                fail("Loaded Algorithm set to modified");
            }
            if( !ret.getParamList().equals(alg.getParamList()) )
            {
               fail("Loaded ParamList different to saved ParamList");
            }
            if( ret.getRsq() != alg.getRsq() )
            {
               fail("Loaded rsq: " + ret.getRsq() +
                   " different to saved rsq: " + alg.getRsq());
            }
            if( ret.getSdDev() != alg.getSdDev() )
            {
               fail("Loaded SdDev different to saved SdDev");
            }

            if( !alg.delete(db) )
            {
                fail("Unable to delete Algorithm");
            }

            ret = MLR.getAlgorithm(alg.getName(),db);
            if( ret != null )
            {
                fail("Algorithm loaded after being deleted");
            }
        }
        catch( SQLException sqle )
        {
            fail(sqle.getMessage());
        }
    }

    /*****************************************************************
     * Tests the getNames() method.
     ****************************************************************/
    public void testGetNames()
    {
        String name1 = "name1";
        String name2 = "name2";
        MLR alg1, alg2;
        Vector names;
        String name;
        boolean have1 = true;
        boolean have2 = true;

        alg1 = new MLR(molSet,name1);
        if( !alg1.save(db) )
        {
            fail("Unable to save Algorithm: " + name1);
        }
        alg2 = new MLR(molSet,name2);
        if( !alg2.save(db) )
        {
            fail("Unable to save Algorithm: " + name2);
        }

        names = MLR.getNames(db);
        if( names == null )
        {
            fail("Failed to retrieve names");
        }
        for( int i=0; i < names.size(); ++i )
        {
            name = (String) names.elementAt(i);
            if( name.equals(name1) )
            {
                have1 = true;
            }
            if( name.equals(name2) )
            {
                have2 = true;
            }
        }

        if( !have1 )
        {
            fail("Failed to retrieve MolSet name: " + name1);
        }
        if( !have2 )
        {
            fail("Failed to retrieve MolSet name: " + name2);
        }

        alg1.delete(db);
        alg2.delete(db);
    }


    /******************************************************************
     * Tests the sum(Matrix,int) method.
     *****************************************************************/
    public void testSum()
    {
        int N = 800;
        Matrix Y = new Matrix(N,1,1.0);
        double sum = MLR.sum(Y,N);
        assertEquals(N, (int) sum);
    }

    /********************************************************************
     * Tests the calculateB, rsq, and sdDev methods
     *******************************************************************/
    public void testCalculateBAndRsqAndSdDev()
    {
        double b1 = -52.50909090910136;
        double b2 = 0.19672727272728707;
        int N = 10;

        double[][] xVals = 
        {
            {1,400},
            {1,410},
            {1,395},
            {1,415},
            {1,390},
            {1,385},
            {1,405},
            {1,420},
            {1,380},
            {1,375}
        };

        Matrix X = new Matrix(xVals);

        double[][] yVals =
        {
            {26.15},
            {28.45},
            {25.20},
            {29.30},
            {24.35},
            {23.10},
            {27.40},
            {29.60},
            {22.05},
            {21.30}
        };
        
        Matrix Y = new Matrix(yVals);

        Matrix B = MLR.calculateB(X,Y,1);

        if( B.get(0,0) != b1 )
        {
            fail("Calculated incorrect parameter found: " +
                B.get(0,0) + " expected: " + b1);
        }

        if( B.get(1,0) != b2)
        {
            fail("Calculated incorrect parameter found: " +
                B.get(1,0) + " expected: " + b2);
        }

        double rsq = MLR.rsq(Y,B,X,N, true);
        logger.debug("R2: " + rsq);
        //System.out.print("R2: " + rsq);
        double sdDev = MLR.sdDev(Y,B,X,N);
        logger.debug("SDDEV: " + sdDev);
    }


    /********************************************************
     * Tests the calculateParameters method
     ********************************************************/
    public void testCalculateParameters()
    {
        MLR alg = null;
        String alName = "alName";
        MolSet molSet = null;

        molSet = new MolSet(Constants.instance().getString("TEST_SDF_FILENAME"));
        alg = new MLR(molSet,alName);
        
        alg.addIntercept();

        String filename = Constants.instance().getString("TEST_SMARTS_FILENAME");
        filename = "/" + filename;
        java.io.InputStream inputStream = getClass().getResourceAsStream(filename);
        if( !alg.addSmarts(inputStream) )
        {
            fail("Failed to initialise SMARTS");
        }

        // test calculateParameters

        if( !alg.calculateParameters() )
        {
            fail("Did not correctly calculate parameters");
        }

        if( !alg.modified() )
        {
            fail("Algorithm is not modified after calculating parameters");
        }

        logger.debug("Successfully tested calculateParameters()");
    }


    /***********************************************************
     * Tests the calculateOccurrence(), 
     * calculateOccurrenceRow(), and
     * calculateB() methods.
     *********************************************************/
    public void testgetX()
    {
        MLR alg = null;
        String alName = "alName2";
        Mol mol;
        MolSet molSet = null;
        int N,k;
        Matrix X;

        molSet = new MolSet(Constants.instance().getString("TEST_SDF_FILENAME"));
        alg = new MLR(molSet,alName);

        alg.addIntercept();
        String filename = Constants.instance().getString("TEST_SMARTS_FILENAME");
        filename = "/" + filename;
        java.io.InputStream inputStream = getClass().getResourceAsStream(filename);
        
        if( !alg.addSmarts(inputStream, "bioppsy.ATParam") )
        {
            fail("Failed to initialise SMARTS");
        }

        N = alg.getMolSet().size();
        k = alg.getNumberParameters() - 1;

        // Test calculateOccurrenceRow(Matrix,Mol,int,int)
        Iterator i = molSet.getIterator();
        mol = (Mol) i.next();
        X = new Matrix(1,k+1);

        if( !alg.calculateOccurrenceRow(X,mol,0,k) )
        {
            fail("Failed to calculate occurrence of a single row");
        }
/*
        logger.debug(mol.getTitle());
        for( int r=0; r < (k+1); ++r )
        {
            logger.debug("Row: " + r + " Value: " + X.get(0,r) );
        }
*/
 
        /***********************************************************
         * Check the values are correct. These values have been
         * worked out by hand to compare to the calculated
         * results. This is for the molecule CC(N)=O.
         * The corresponding SMARTS patterns are shown.
         **********************************************************/
        if( X.get(0,0) != 1.0 ||  // the intercept, should always be 1.0
            X.get(0,4) != 1.0 ||  // [CX4;H3][CX3,c,F,Cl,Br,I]=[#8,#7]
            X.get(0,26) != 1.0 || // [C;H0]=O
            X.get(0,45) != 1.0 || // [#8]=C
            X.get(0,50) != 1.0 )  // [N;H2] and [N;H2][C]
        {
            fail("Incorrect calculation of atomic typing occurrences");
        }

        X = null;

        // test calculateOccurrence(int,int)

        if( (X = alg.calculateOccurrence(N,k)) == null )
        {
            fail("Did not correctly calculate occurrence");
        }

        // make sure it doesn't work if we try for too many Mol
        if( alg.calculateOccurrence(N+1,k) != null )
        {
            fail("Allowed calculateOccurrence() with too many Mol");
        }
        // make sure it doesn't work if we try for too little Mol
        if( alg.calculateOccurrence(N-1,k) != null )
        {
            fail("Allowed calculateOccurrence() with too many Mol");
        }

        logger.debug("Successfully tested calculateOccurrence()");
    }

    /*************************************************************
     * Tests the calculateSolubilities(MolSet,Notifier,Database) method
     *******************************************************************/
    public void testCalculateProperties()
    {
        String filename = Constants.instance().getString("TEST_SMARTS_FILENAME");
        filename = "/" + filename;
        java.io.InputStream inputStream = getClass().getResourceAsStream(filename);
        if( !alg.addSmarts(inputStream,"bioppsy.ATParam") )
        {
            fail("Failed to initialise SMARTS");
        }
        if( !alg.calculateParameters() )
        {
            fail("Did not correctly calculate parameters");
        }
        MolSet testMolSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));

        if( !alg.calculateProperties(testMolSet,new Notifier(),db) )
        {
            fail("Failed to calculate all properties");
        }

        logger.debug("Successfully tested calculateProperties");
    }
}
