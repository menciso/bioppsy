package bioppsy.tests;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.Constants;
import bioppsy.Database;
import bioppsy.FailedConstructorException;
import bioppsy.Mol;
import bioppsy.MolSet;
import bioppsy.algorithm.MLR;


//java
import java.util.Iterator;
import java.sql.SQLException;
import java.util.Vector;

/*************************************************************
 * Tests the MolSet class.
 ************************************************************/
public class MolSetTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(MolSetTest.class);

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
    }

    /**********************************************************************
     * Tests iterations through the MolSet.
     **********************************************************************/
    public void testMolSet()
    {
        Mol mol;

        MolSet molSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));

        Iterator msi = molSet.getIterator();

        int counter = 0;

        while( msi.hasNext() )
        {
            ++counter;
            mol = (Mol) msi.next();
            assertEquals(new Integer(mol.getNumber()).intValue(),
                counter);
        }
    }

    /********************************************************************
     * Tests the haveExpSol() function.
     *******************************************************************/
    public void testHaveExpProp()
    {
        MolSet molSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));

        if( !molSet.haveExpProp() )
        {
            fail("Returned false to haveExpProp when expected true");
        }

        molSet = new MolSet(
            Constants.instance().getString("DRUGS_MOL2_FILENAME_TEST"));

        if( molSet.haveExpProp() )
        {
            fail("Returned true to haveExpProp when expected false");
        }
    }

    /********************************************************************
     * Tests all the database functions.
     ******************************************************************/
    public void testDatabase()
    {
        MolSet molSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
        MolSet ret;

        double var = 45.76;

        try
        {
            molSet.setVariance(var);
            if( !molSet.save(db) )
            {
                fail("Unable to save MolSet");
            }
            var = var + 1.0;
            molSet.setVariance(var);
            if( !molSet.save(db) )
            {
                fail("Unable to update MolSet");
            } 
            if( molSet.getId() < 0 )
            {
                fail("MolSet does not have an id after being created");
            }
            if( molSet.modified() )
            {
               fail("MolSet modified after being saved");
            }
            if( !molSet.exists(db) )
            {
               fail("MolSet does not exist after being saved");
            }
            ret = MolSet.get(
                Constants.instance().getString("HOU_TEST1_SDF_FILENAME"),
                db);
            if( ret == null )
            {
                fail("Unable to load MolSet");
            }
            assertEquals(molSet.getFile().getAbsolutePath(),
                ret.getFile().getAbsolutePath() );
            if( ret.getExpPropFile() != null )
            {
                fail("First retrieved MolSet has non-null expPropFile");
            }
            if( ret.getVariance() != var )
            {
                fail("Retrieved variance: " + molSet.getVariance() +
                    " expected: " + var);
            }
            int id = ret.getId();
            ret = MolSet.get(
                ret.getId(),
                db);
            if( ret == null )
            {
                fail("Unable to load MolSet from id");
            }
            assertEquals(molSet.getFile().getAbsolutePath(),
                ret.getFile().getAbsolutePath() );
            if( ret.getVariance() != var )
            {
                fail("Retrieved variance: " + molSet.getVariance() +
                    " expected: " + var);
            }
            if( ret.getExpPropFile() != null )
            {
                fail("Retrieved MolSet by id has non-null expPropFile");
            }
            if( (ret.getId() != id) || (ret.getId() == -1) )
            {
                fail("Retrieved MolSet by id does not have a correct id");
            }
            if( !molSet.setExpPropFile(Constants.instance().getString("TEST_INI_FILENAME")) )
            {
               fail("Unable to MolSet.setExpPropFile()");
            }
            if( !molSet.modified() )
            {
                fail("MolSet not modified after setting expPropFile");
            }
            molSet.setVariance(var);
            if( !molSet.save(db) )
            {
               fail("Unable to update MolSet");
            }
            if( molSet.getId() < 0 )
            {
                fail("MolSet does not have an id after being saved");
            }
            ret = MolSet.get(
                Constants.instance().getString("HOU_TEST1_SDF_FILENAME"),
                db);
            if( ret == null )
            {
                fail("Unable to load MolSet the second time");
            }
            assertEquals(molSet.getExpPropFile().getAbsolutePath(),
                ret.getExpPropFile().getAbsolutePath() );
            if( ret.getVariance() != var )
            {
                fail("Retrieved variance: " + molSet.getVariance() +
                    " expected: " + var);
            }
            if( !molSet.delete(db) ) 
            {
                fail("Unable to delete MolSet");
            }
            if( !molSet.modified() )
            {
                fail("MolSet not modified after being deleted");
            }

            if( !molSet.save(db) )
            {
                fail("Failed to create MolSet with an expPropFile");
            }
            if( !molSet.delete(db) )
            {
                fail("Failed to delete MolSet for the second time");
            }
        }
        catch( SQLException sqle )
        {
            fail(sqle.getMessage());
        }

        logger.debug("Successfully tested MolSet database functions");
    }

    /******************************************************************
     * Tests the MolSubSetIterator functionality
     *****************************************************************/
    public void testMolSubSetIterator()
    {
        MolSet molSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
        Iterator mssi = molSet.getSubSetIterator();
        while( mssi.hasNext() )
        {
            Vector subset = (Vector) mssi.next();
            assertEquals(molSet.size(),subset.size());
        }
    }

    /*******************************************************************
     * Tests the expPropSdDev() method
     ******************************************************************/
    public void testExpSolSdDev()
    {
        String dataFile = "TEST_SD_DEV_SET";
        MolSet molSet = new MolSet(
            Constants.instance().getString(dataFile));
        double result = molSet.expPropSdDev();
        if( (new Double(result)).isNaN() )
        {
            fail("Failed to calculate sdDev on: " + dataFile);
        }
        logger.debug("Standard Deviation: " + result);
        if( result != 3.0276503540974917 )
        {
            fail("Incorrect sdDev calculation");
        }
    }

    /*******************************************************************
     * Tests the usedByAlgorithm() method
     ******************************************************************/
    public void testUsedByAlgorithm()
    {
        MolSet molSet = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));

        String algNames = molSet.usedByAlgorithm(db);
        
        if( algNames != null )
        {
            fail("usedByAlgorithm() did not return null for a MolSet that " +
                "does not exist yet");
        }

        if( !molSet.save(db) )
        {
            fail("Unable to save MolSet: " + molSet.getFile().getAbsolutePath());
        }

        algNames = molSet.usedByAlgorithm(db);
        
        if( algNames != null )
        {
            fail("usedByAlgorithm() did not return null for a MolSet that " +
                "is not used by an Algorithm");
        }

        MLR alg = new MLR(molSet,"testUsedByAlgorithm");
        if( !alg.save(db) )
        {
            fail("Unable to save Algorithm: " + alg.getName());
        }

        algNames = molSet.usedByAlgorithm(db);
        if( (algNames == null) || (!algNames.equals(alg.getName())) )
        {
            fail("Incorrect result for usedByAlgorithm()");
        }

        if( molSet.delete(db) )
        {
            fail("Able to delete molSet before deleting algorithm using it");
        }
        if( !alg.delete(db) )
        {
            fail("Unable to delete Algorithm: " + alg.getName());
        }
        if( !molSet.delete(db) )
        {
            fail("Unable to delete molSet after deleting Algorithm");
        }
    }
}
