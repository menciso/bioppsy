package bioppsy.tests.io;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.Constants;
import bioppsy.NotFoundException;
import bioppsy.io.ExpPropFile;



//java
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/*************************************************************
 * Tests the ExpPropFile class.
 ************************************************************/
public class ExpPropFileTest extends TestCase 
{
    static Logger logger = Logger.getLogger(ExpPropFileTest.class);

    ExpPropFile file;

	protected void setUp() 
    {
        file = new ExpPropFile(Constants.instance().getString("DRUGS_SOL_FILENAME_TEST"));
	}

    protected void tearDown() 
    {
    }

    /***************************************************************
     * Tests the getExpSol() method.
     ***************************************************************/
    public void testGetExpProp()
    {
        BufferedReader in = null;
        String line;
        String fileCAS;
        double expProp;
        String[] splitLine;

        try 
        {
            in = new BufferedReader(new InputStreamReader(file.getInputStream()));
            while( (line = in.readLine()) != null )
            {
                // split on whitespace
                splitLine = line.split("[ \t\n\f\r]+",0);
                if( splitLine.length <= 1 )
                {
                    // do nothing, skip the line
                }
                else
                {
                    fileCAS = splitLine[0];
                    try
                    {
                        expProp = file.getExpProp(fileCAS);
                    }
                    catch( Exception e )
                    {
                        fail(e.getMessage());
                    }
                }
            }
        }
        catch( IOException ioe )
        {
            if( in != null )
            {
                try
                { 
                    in.close(); 
                }
                catch (IOException ioe2) 
                { }
            }
            fail(ioe.getMessage());
        }

        // now try and get a cas that doesn't exist
        try
        {
            expProp = file.getExpProp("THISDOESNOTEXIST");
            fail("Retrieved a non-existent expProp");
        }
        catch(NotFoundException nfe)
        {
            // do nothing, this is what we want.
        }

        logger.debug("ExpPropFile.getExpProp() tested successfully");
    }
}
