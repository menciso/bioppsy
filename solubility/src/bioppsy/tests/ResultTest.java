package bioppsy.tests;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;


//java
import java.sql.SQLException;

/*************************************************************
 * Tests the Result class.
 ************************************************************/
public class ResultTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(ResultTest.class);

    MLR alg;
    MolSet molSet;
    ResultCollection rc;
    Result result;
    Mol mol;

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
            molSet = new MolSet("filename");
            alg = new MLR(molSet,"algorithm");
            rc = new ResultCollection(alg,molSet);
            mol = new Mol("CCCCCCCCCC");
            mol.setNumber(1234);
            mol.setMolSet(molSet);
            result = new Result(rc,mol,3.564);
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
    }

    /*******************************************************
     * Tests the database methods of the Result class
     *****************************************************/
    public void testDatabaseMethods()
    {
        try
        {
            if( result.exists(db) )
            {
                fail("Result exists before saving");
            }
            if( !result.modified() )
            {
                fail("Result not modified after creation");
            }
            if( !result.save(db) )
            {
                fail("Result failed to save");
            }
            if( result.modified() )
            {
                fail("Result modified after saving");
            }

            result.setValue(4.4444);
            if( !result.modified() )
            {
                fail("Result not modified after setting value");
            }
            // we must delete the Result before updating
            // because Results are not updated
            if( !result.delete(db) )
            {
               fail("Failed to delete result");
            }
            if( !result.save(db) )
            {
                fail("Failed to update result");
            }

            if( !result.delete(db) )
            {
               fail("Failed to delete result");
            }
            if( !result.modified() )
            {
                fail("Result not modified after deleting");
            }

            mol.setExpProp(55.55);
            mol.setMolId("55-55-55");
            result.setModified();
            if( !result.save(db) )
            {
                fail("Unable to save Result with expSol and CAS");
            }

            /* removed, Results can no longer be updated
            result.setValue(4.4444);
            mol.setExpSol(44.44);
            if( !result.save(db) )
            {
                fail("Failed to update result with expSol");
            }
            */

            if( !result.delete(db) )
            {
                fail("Unable to delete Result with expSol and CAS");
            }
        }
        catch( SQLException sqle )
        {
            fail(sqle.getMessage());
        }
    }

    /*******************************************************************
     * Tests the getId(Database) method. 
     *******************************************************************/
    public void testGetId()
    {
        int id;

        try
        {
            id = result.getId(db);
            assertEquals(-1,id);
            if( !result.save(db) ) 
            {
                fail("Unable to save Result");
            }

            id = result.getId(db);          
            if( id < 0 )
            {
                logger.debug("Unable to retrieve Result's id from database");
            }
            if( !result.delete(db) )
            {
                fail("Unable to delete Result");
            }

            result.setId(45);
            id = result.getId(db);
            if( id != 45 )
            {
                fail("Unable to retrieve Result's id");
            }
        }
        catch( SQLException sqle )
        {
            fail(sqle.getMessage());
        }
    }
}
