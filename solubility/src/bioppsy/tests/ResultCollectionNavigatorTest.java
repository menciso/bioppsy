package bioppsy.tests;


//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;
import bioppsy.ResultCollection;
import bioppsy.util.*;


import java.io.File;

/*************************************************************
 * Tests the ResultCollectionNavigator class.
 ************************************************************/
public class ResultCollectionNavigatorTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(ResultCollectionNavigatorTest.class);

    MLR alg;
    MolSet molSet, molSetTest;
    ResultCollection rc;
    ResultCollectionNavigator rcn;
    Result result;
    Mol mol;

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
    }

    /**********************************************************
     * Tests everything.
     *********************************************************/
    public void testAll() 
    {
        int i,left;
        molSet = new MolSet(
            Constants.instance().getString("TEST_SDF_FILENAME"));
        molSetTest = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
        alg = new MLR(molSet,"MLR");
//        alg.initParameters(Constants.instance().getString("TEST_SMARTS_FILENAME"));
        alg.calculateParameters();

        alg.calculateProperties(molSetTest,new Notifier(),db);
        rcn = new ResultCollectionNavigator(alg,molSetTest,"molNumber",db);
        rc = rcn.first();
        if( rc == null )
        {
            fail("Unable to obtain first ResultCollection for molSetTest");
        }
        int count = 0;
        for( i=0; i < rc.size(); ++i )
        {
            ++count;
            result = (Result) rc.elementAt(i);
            assertEquals(count,result.getMol().getNumber());
            //System.out.print("count" + count + result.getMol().getMolId()+"\n");
        }
        assertEquals(
            Constants.instance().getInt("HOU_TEST1_NUM_MOLS"),
            count );

        rc = rcn.last();
        if( rc == null )
        {
            fail("Unable to obtain last ResultCollection for molSetTest");
        }
        for( i=0; i < rc.size(); ++i )
        {
            result = (Result) rc.elementAt(i);
            assertEquals(i+1,result.getMol().getNumber());
        }
        assertEquals(
            Constants.instance().getInt("HOU_TEST1_NUM_MOLS"),
            count );

        if( !alg.calculateProperties(molSet,new Notifier(),db) )
        {
            fail("Unable to calculate solubilities for molSet");
        }
        rcn = new ResultCollectionNavigator(alg,molSet,"molNumber",db);
        rc = rcn.first();
        if( rc == null )
        {
            fail("Unable to obtain first ResultCollection for molSet");
        }
        count = 0;
        for( i=0; i < rc.size(); ++i )
        {
            ++count;
            result = (Result) rc.elementAt(i);
            assertEquals(count,result.getMol().getNumber());
        }
        // make sure we only get the first section of results
        assertEquals(
            Math.min(Constants.instance().getInt("RESULT_COLLECTION_NAVIGATOR_SIZE"),
                Constants.instance().getInt("TEST_NUMBER_MOL")),
            count );
        rc = rcn.next();
        count = Constants.instance().getInt("RESULT_COLLECTION_NAVIGATOR_SIZE");
        if( Constants.instance().getInt("RESULT_COLLECTION_NAVIGATOR_SIZE") < 
            Constants.instance().getInt("TEST_NUMBER_MOL") )
        {
            if( rc == null )
            {
                fail("Unable to obtain next ResultCollection for molSet");
            }
            for( i=0; i < rc.size(); ++i )
            {
                ++count;
                result = (Result) rc.elementAt(i);
                assertEquals(count,result.getMol().getNumber());
            }
            // make sure we only get the next section of results
            left = Constants.instance().getInt("TEST_NUMBER_MOL") - 
                   Constants.instance().getInt("RESULT_COLLECTION_NAVIGATOR_SIZE");
            assertEquals(left,rc.size());
        }
        else
        {
           if( rc != null )
           {
               fail("Next returned a ResultCollection for molSet when there were none");
           }
        }

        rc = rcn.last();
        if( rc == null )
        {
            fail("Unable to obtain last ResultCollection for molSet");
        }
        count = Constants.instance().getInt("TEST_NUMBER_MOL") - 
                Constants.instance().getInt("RESULT_COLLECTION_NAVIGATOR_SIZE");
        for( i=0; i < rc.size(); ++i )
        {
            ++count;
            result = (Result) rc.elementAt(i);
            assertEquals(count,result.getMol().getNumber());
        }
        left =
            Math.min(
                Constants.instance().getInt("TEST_NUMBER_MOL"),
                Constants.instance().getInt("RESULT_COLLECTION_NAVIGATOR_SIZE"));
        assertEquals(left,rc.size());

        rc = rcn.previous();
        if( rc == null )
        {
            fail("Unable to obtain previous ResultCollection for molSet");
        }
        count = 0;
        for( i=0; i < rc.size(); ++i )
        {
           ++count;
           result = (Result) rc.elementAt(i);
           assertEquals(count,result.getMol().getNumber());
        }
        left =
                Constants.instance().getInt("TEST_NUMBER_MOL") - 
                Constants.instance().getInt("RESULT_COLLECTION_NAVIGATOR_SIZE");
        assertEquals(left,rc.size());

        rc = new ResultCollection(alg,molSetTest);
        rc.delete(db);
        rc = new ResultCollection(alg,molSet);
        rc.delete(db);
    }
    
    /****************************************************
     * Test "save ResultCollection"
     ****************************************************/
    
    public void testSave() 
    {
        molSet = new MolSet(
                Constants.instance().getString("TEST_SDF_FILENAME"));
        molSetTest = new MolSet(
                Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
        alg = new MLR(molSet,"MLR");
        rc = new ResultCollection(alg,molSet);
        Mol mol1= new Mol(molSetTest, 1);
        Mol mol2= new Mol(molSetTest, 2);
        Mol mol3= new Mol(molSetTest, 3);
        double r1d = 3.0; 
        double r2d = 4.0; 
        double r3d = 5.0;
        double m1d = 1.0; 
        double m2d = 2.0; 
        double m3d = 12.0;
        mol1.setExpProp(m1d); 
        mol2.setExpProp(m2d); 
        mol3.setExpProp(m3d);

        Result r1 = new Result(rc,mol1,r1d);
        Result r2 = new Result(rc,mol2,r2d);
        Result r3 = new Result(rc,mol3,r3d);
        
        rc.add(r1); 
        rc.add(r2); 
        rc.add(r3);
        
        if( !rc.save(db) )
        {
            fail("Unable to save ResultCollection");
        }
        rc.delete(db);
//        molSet.delete(db);
        molSetTest.delete(db);
     }
    

    /****************************************************
     * Tests the getRsq() function. For this test we
     * use the following values of Y and Yhat:<br><br>
     *
     * Y    = (1,2,12)<br>
     * Yhat = (3,4,5)<br>
     * Ymean = 5<br><br>
     *
     * rsq = SSreg/SStotal<br>
     * SSreg = 2^2 + 1^2 + 0^2 = 5<br>
     * SStotal = 4^2 + 3^2 + 7^2 = 74<br>
     * rsq = 5/74 = 0.0676 (3sf)
     * 
     ***************************************************/
    public void testGetRsq() 
    {
        molSet = new MolSet(
                Constants.instance().getString("TEST_SDF_FILENAME"));
        molSetTest = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
        alg = new MLR(molSet,"MLR");

        rc = new ResultCollection(alg,molSet);
                
        double r1d = 3.0;
        double r2d = 4.0;
        double r3d = 5.0;
        double m1d = 1.0;
        double m2d = 2.0;
        double m3d = 12.0;

        Mol mol1 = new Mol(molSetTest,1);
        Mol mol2 = new Mol(molSetTest,2);
        Mol mol3 = new Mol(molSetTest,3);
        mol1.setExpProp(m1d);
        mol2.setExpProp(m2d);
        mol3.setExpProp(m3d);
        Result r1 = new Result(rc,mol1,r1d);
        Result r2 = new Result(rc,mol2,r2d);
        Result r3 = new Result(rc,mol3,r3d);
        rc.add(r1);
        rc.add(r2);
        rc.add(r3);
        rcn = new ResultCollectionNavigator(alg,molSet,"molNumber",db);
        
        //System.out.print("mol set " + rcn.getMolSet());
        
        if( !rc.save(db) )
        {
            fail("Unable to save ResultCollection");
        }

        double rsq;
        double expected = 0.06756756756756757;

        rsq = rcn.getRsq();
        if( expected != rsq)
        {
            fail("Did not correctly calculate rsq. \n Expected: " + expected + "\n Calculated: " + rsq);
        }

        rc.delete(db);
        molSetTest.delete(db);
     }

    /****************************************************
     * Tests the getSdDev() function. For this test we
     * use the following values of Y and Yhat:<br><br>
     *
     * Y    = (1,2,12)<br>
     * Yhat = (3,4,5)<br>
     * => sumSqDev = 2^2 + 2^2 + 7^2 = 57<br>
     * sdSquared = 57/2<br>
     * sdDev = sqrt(57/2) = 4.359 (3dp)
     * 
     ***************************************************/
    public void testGetSdDev()
    {
        molSet = new MolSet(
            Constants.instance().getString("TEST_SDF_FILENAME"));
//    	molSet = new MolSet("file");
        molSetTest = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
        alg = new MLR(molSet,"algorithm");

        rcn = new ResultCollectionNavigator(alg,molSetTest,"molNumber",db);
        rc = new ResultCollection(alg,molSetTest);
                
        double r1d = 3.0;
        double r2d = 4.0;
        double r3d = 5.0;
        double m1d = 1.0;
        double m2d = 2.0;
        double m3d = 12.0;

        Mol mol1 = new Mol(molSetTest,1);
        Mol mol2 = new Mol(molSetTest,2);
        Mol mol3 = new Mol(molSetTest,3);
        mol1.setExpProp(m1d);
        mol2.setExpProp(m2d);
        mol3.setExpProp(m3d);
        Result r1 = new Result(rc,mol1,r1d);
        Result r2 = new Result(rc,mol2,r2d);
        Result r3 = new Result(rc,mol3,r3d);
        rc.add(r1);
        rc.add(r2);
        rc.add(r3);

        if( !rc.save(db) )
        {
            fail("Unable to save ResultCollection");
        }

        double sdDev;
        double expected = Math.sqrt(57.0/2.0);

        sdDev = rcn.getSdDev();
        if( expected != sdDev)
        {
            fail("Did not correcty calculate sdDev: Expected: " + expected + ". Result: " + sdDev);
        }

        rc.delete(db);
     }

    /****************************************************
     * Tests the getMUE() function. For this test we
     * use the following values of Y and Yhat:<br><br>
     *
     * Y    = (1,2,12)<br>
     * Yhat = (3,4,5)<br>
     * => MUE = 2 + 2 + 7 / 3 = 3.666667
     * 
     ***************************************************/
    public void testGetMUE() 
    {
        molSet = new MolSet(
            Constants.instance().getString("TEST_SDF_FILENAME"));
        molSetTest = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
        alg = new MLR(molSet,"algorithm");

        rcn = new ResultCollectionNavigator(alg,molSetTest,"molNumber",db);
        rc = new ResultCollection(alg,molSetTest);
                
        double r1d = 3.0;
        double r2d = 4.0;
        double r3d = 5.0;
        double m1d = 1.0;
        double m2d = 2.0;
        double m3d = 12.0;

        Mol mol1 = new Mol(molSetTest,1);
        Mol mol2 = new Mol(molSetTest,2);
        Mol mol3 = new Mol(molSetTest,3);
        mol1.setExpProp(m1d);
        mol2.setExpProp(m2d);
        mol3.setExpProp(m3d);
        Result r1 = new Result(rc,mol1,r1d);
        Result r2 = new Result(rc,mol2,r2d);
        Result r3 = new Result(rc,mol3,r3d);
        rc.add(r1);
        rc.add(r2);
        rc.add(r3);

        if( !rc.save(db) )
        {
            fail("Unable to save ResultCollection");
        }

        double mue;
        double expected = 3.6666666666666665;

        mue = rcn.getMUE();
        if( expected != mue)
        {
            fail("Did not correcty calculate MUE");
        }

        rc.delete(db);
    }


    /***********************************************************
     * The ResultCollectionNavigator must behave properly when the
     * Algorithm is null, this test verifies that.
     ***********************************************************/
    public void testNullBehaviour()
    {
        try
        {
           molSetTest = new MolSet(
               Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
           rcn = new ResultCollectionNavigator(null,molSetTest,"molNumber",db);
           rc = rcn.first();
           rc = rcn.next();
           rc = rcn.last();
           rc = rcn.previous();
        }
        catch( Exception e )
        {
            fail(e.getMessage());
        }
    }

    /**********************************************************
     * Tests the printResultsToFile() method
     *********************************************************/
    public void testPrintResultsToFile() 
    {
        molSet = new MolSet(
            Constants.instance().getString("TEST_SDF_FILENAME"));
        molSetTest = new MolSet(
            Constants.instance().getString("HOU_TEST1_SDF_FILENAME"));
        alg = new MLR(molSet,"algorithm");
//        alg.initParameters(Constants.instance().getString("TEST_SMARTS_FILENAME"));
        alg.calculateParameters();

        alg.calculateProperties(molSetTest,new Notifier(),db);
        rcn = new ResultCollectionNavigator(alg,molSetTest,"molNumber",db);

        String testPrintFilename = Constants.instance().getString("TEST_OUTPUT_FILE");

        if( !rcn.printResultsToFile(new File(testPrintFilename)) )
        {
            fail("Failed to print results to file: " + testPrintFilename);
        }

        File file = new File(testPrintFilename);
        file.delete();
    }
}
