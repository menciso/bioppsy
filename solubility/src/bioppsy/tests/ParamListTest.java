package bioppsy.tests;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;


//java
import java.util.Iterator;

/*************************************************************
 * Tests the ParamList class.
 ************************************************************/
public class ParamListTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(ParamListTest.class);

    MLR alg;
    MolSet molSet;

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
            molSet = new MolSet("filename");
            alg = new MLR(molSet,"algorithm");
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
    }

    /****************************************************
     * Tests the ParamList.add(Param) method.
     ***************************************************/
    public void testAdd()
    {
        int i;
        ParamList paramList = new ParamList(alg);

        // add 10 params
        for(i=0; i < 10; ++i)
        {
            DescriptorParam param = new DescriptorParam(alg,3,i);
            paramList.add(param);
        }

        // add 10 ATParams
        for(i=0; i < 20; ++i)
        {
            ATParam atParam = new ATParam(alg,3,(double)i+10);
            paramList.add(atParam);
        }

        // check that they are right
        for( i=0; i < 10; ++i )
        {
            Param param = (Param) paramList.elementAt(i);
            assertEquals(i,param.getId());
        }

        for( i=10; i < 20; ++i )
        {
            ATParam atParam = (ATParam) paramList.elementAt(i);
            assertEquals(i,atParam.getId());
        }

        i = 0;
        Iterator it = paramList.getParamIterator();
        while( it.hasNext() )
        {
           Param param = (Param) it.next();
           assertEquals(i++,param.getId());
        }

        assertEquals(i,10);

        i = 30;
        Iterator ri = paramList.getReverseATParamIterator();
        while( ri.hasNext() )
        {
            ATParam atParam = (ATParam) ri.next();
            assertEquals(--i,atParam.getId());
        }

        assertEquals(i,10);

        // now check it works when we remove a couple of elements
        // and reIndex

        paramList.removeElementAt(5);
        paramList.removeElementAt(15);
        paramList.reIndex();

        i = 0;
        it = paramList.getParamIterator();
        while( it.hasNext() )
        {
           Param param = (Param) it.next();
           assertEquals(i++,param.getId());
        }

        assertEquals(i,9);

        i = 28;
        ri = paramList.getReverseATParamIterator();
        while( ri.hasNext() )
        {
            ATParam atParam = (ATParam) ri.next();
            assertEquals(--i,atParam.getId());
        }

        assertEquals(i,9);
    }

    /***********************************************************
     * Tests the save(), getParamList(), and delete() methods.
     ***********************************************************/
    public void testSaveDelete()
    {
        int i;
        ParamList paramList = new ParamList(alg);

        // add 10 params
        for(i=0; i < 10; ++i)
        {
            Param param = new Param(alg,3,i);
            paramList.add(param);
        }

        paramList.remove(5);
        if( !paramList.modified() )
        {
            fail("ParamList not modified after removing a Param");
        }
        paramList.reIndex();

        if( !paramList.save(db) )
        {
            fail("Unable to save ParamList");
        }

        if( paramList.modified() )
        {
            fail("ParamList modified after being saved");
        }

        ParamList ret = ParamList.getParamList(alg,db);
        if( ret == null )
        {
            fail("Unable to load ParamList");
        }
        if( paramList.modified() )
        {
            fail("ParamList modified after being loaded");
        }

        if( !ret.equals(paramList) )
        {
            fail("Loaded ParamList different to saved ParamList");
        }

        paramList.add( new Param(alg,3,34.872) );
        if( !paramList.modified() )
        {
            fail("ParamList not modified after adding a Param");
        }
        if( !paramList.save(db) )
        {
            fail("Unable to save ParamList a second time");
        }

        if( !paramList.delete(db) )
        {
            fail("Unable to delete ParmList");
        }
        if( !paramList.modified() )
        {
            fail("ParamList not modified after deleting");
        }
    }
}
