package bioppsy.tests.util;


import junit.framework.*;
import org.apache.log4j.Logger;

import bioppsy.util.*;



//java
import java.util.Vector;

/*
    StatsTest.java

    Tests the property.util.Stats class
*/
public class StatsTest extends TestCase 
{
    static Logger logger = Logger.getLogger(StatsTest.class);

	protected void setUp() 
    {
	}

    protected void tearDown()
    {
    }

	public void testVariance()
    {
        Vector values = new Vector();
        for( int i=1; i <= 9; ++i )
        {
            values.add(new Double(i));
        }
        double variance = Stats.variance(values);
        double expected = 7.5;
        if( variance != expected )
        { 
            fail("Error calculating variance, expected: " + expected +
                " recieved: " + variance);
        }
    }

}
