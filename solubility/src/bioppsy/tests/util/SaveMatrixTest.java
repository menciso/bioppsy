package bioppsy.tests.util;


import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;
import bioppsy.util.*;


//Jama
import Jama.Matrix;

//java
import java.util.Vector;

/*
    SaveMatrixTest.java

    Tests the property.util.SaveMatrix class
*/
public class SaveMatrixTest extends TestCase 
{
    private Database db = null;

    static Logger logger = Logger.getLogger(SaveMatrixTest.class);

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown()
    {
        if( db != null )
        {
            db.close();
        }
    }

    /******************************************************
     * Tests the save() and delete() functions
     *****************************************************/
	public void testDatabaseFunctions()
    {
        double[][] values = {{1,2,3},{1,2,3},{1,2,3}};
        Matrix M = new Matrix(values);
        int rows = 3;
        int columns = 3;
        String key = "SM";
        MLR alg = new MLR();
        alg.setName("algorithm");
        SaveMatrix SM = new SaveMatrix(alg,key,M,rows,columns);
        
        if( !SM.save(db) )
        {
            fail("Unable to save Matrix");
        }
        if( !SM.delete(db) )
        {
            fail("Unable to delete Matrix");
        }
        if( !SM.save(db) )
        {
            fail("Unable to save Matrix a second time");
        }
        if( !SM.delete(db) )
        {
            fail("Unable to delete Matrix a second time");
        }
    }

    /**************************************************************
     * Tests the SaveMatrix(Algorithm,String,Vector) constructor
     **************************************************************/
    public void testRowsConstructor()
    {
        SaveMatrix SM;

        MLR alg = new MLR();
        alg.setName("algorithm");
        Vector rowsVector = new Vector();
        int rows = 13;
        int columns = 13;
        Vector row;

        for( int i=0; i < rows; ++i )
        {
            row = new Vector();
            for( int j=0; j < columns; ++j )
            {
                row.add(new Double(i+j));                          
            }
            rowsVector.add(row);
        }

        try
        {
            SM = new SaveMatrix(alg,"key",rowsVector);
        }
        catch( FailedConstructorException fce )
        {
            fail(fce.getMessage());
        }

        // make sure it complains with odd numbers
        // of columns
        Vector rowVector = new Vector();
        for( int i=0; i < rows; ++i )
        {
            row = new Vector();
            for( int j=0; j < i+2; ++j )
            {
                row.add(new Double(i+j));                          
            }
            rowsVector.add(row);
        }

        try
        {
            SM = new SaveMatrix(alg,"key",rowsVector);
            fail("Failed to complain about odd column lengths");
        }
        catch( FailedConstructorException fce )
        {
            logger.debug(fce.getMessage());
        }
    }

    /********************************************************
     * Tests the rowMatrix() method
     *******************************************************/
    public void testRowMatrix()
    {
        Vector values = new Vector();
        int i,j;
        for( i=0; i < 10; ++i )
        {
            Vector row = new Vector();
            for( j=0; j < 10; ++j )
            {
                row.add(new Double(i+j));
            }
            values.add(row);
        }

        SaveMatrix sm = SaveMatrix.rowMatrix(
            new MLR(), "key", values );
        Matrix M = sm.getMatrix();

        for( i=0; i < 10; ++i )
        {
           Vector row = (Vector) values.elementAt(i);

           for( j=0; j < 10; ++j )
           {
               double value = ((Double) row.elementAt(j)).doubleValue();
               if( value != M.get(i,j) )
               {
                  fail("Values differ");
               }
           }
        }
    }

    /********************************************************
     * Tests the columnMatrix() method
     *******************************************************/
    public void testColumnMatrix()
    {
        Vector values = new Vector();
        int i,j;
        for( i=0; i < 10; ++i )
        {
            Vector column = new Vector();
            for( j=0; j < 10; ++j )
            {
                column.add(new Double(i+j));
            }
            values.add(column);
        }

        SaveMatrix sm = SaveMatrix.columnMatrix(
            new MLR(), "key", values );
        Matrix M = sm.getMatrix();

        for( i=0; i < 10; ++i )
        {
           Vector column = (Vector) values.elementAt(i);

           for( j=0; j < 10; ++j )
           {
               double value = ((Double) column.elementAt(j)).doubleValue();
               if( value != M.get(j,i) )
               {
                  fail("Values differ expected: " + value + " got " + M.get(j,i));
               }
           }
        }
    }

    /********************************************************
     * Tests the diagonalMatrix() method
     *******************************************************/
    public void testDiagonalMatrix()
    {
        Vector diagonals = new Vector();
        int i;
        for( i=0; i < 10; ++i )
        {
            diagonals.add(new Double(i));
        }
        Matrix M = SaveMatrix.diagonalMatrix(
            new MLR(), "key", diagonals ).getMatrix();
        for( i=0; i < 10; ++i )
        {
            if( M.get(i,i) != i )
            {
                fail("Diagonals differ");
            }
            for( int j=0; j < 10; ++j )
            {
                if( (j != i) && (M.get(i,j) != 0.0) )
                {
                    fail("Non-zero off diagonal");
                }
            }
        }
    }

    /***********************************************************
     * Tests the getRowVector() method
     ***********************************************************/
    public void testGetRowVector()
    {
        double[][] columns = {{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}};
        Matrix V = new Matrix(columns);
        Vector v = SaveMatrix.getRowVector(V);
        if( v != null )
        {
            fail("Row vector returned for column vector");
        }
        double[][] rows = {{1,2,3,4,5,6,7,8,9,10}};
        V = new Matrix(rows);
        v = SaveMatrix.getRowVector(V);
        if( v == null )
        {
            fail("Row vector not returned for correct row vector");
        }
    }
}
