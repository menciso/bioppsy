package bioppsy.tests.util;


import junit.framework.*;
import org.apache.log4j.Logger;

import bioppsy.util.*;



/*
    UtilTest.java

    Tests all the classes in the property.util package.
*/
public class UtilTest extends TestCase 
{
    // Define a static logger variable so that it references the
    // Logger instance named "UtilTest".
    static Logger logger = Logger.getLogger(UtilTest.class);

	protected void setUp() 
    {
	}
	public void testIniFileReader()
    {
        try
        {
            String name = "ini/testini.ini";
            IniFileReader ifr = new IniFileReader(name);
            if( !ifr.read() )
            {
                fail("Could not read ini file: " + name);
            }
            assertEquals("SECOND_VALUE", ifr.getString("SECOND_ELEMENT"));
            assertEquals("FIRST_VALUE", ifr.getString("FIRST_ELEMENT"));
            
            //check that lines are commented out correctly
            assertNull( ifr.getString("BAD") );

            String[] stringArray = ifr.getStringArray("STRING_ARRAY");
            if( !stringArray[0].equals("ONE") ||
                !stringArray[1].equals("TWO") ||
                !stringArray[2].equals("THREE") )
            {
                fail("Failed to read String Array");
            }
            assertEquals(ifr.getInt("INT"),1);
            if( ifr.getDouble("DOUBLE") != 2.3 )
            {
                fail("Failed to read double");
            }
                  
        }
        catch( Exception e ) 
        {
            fail(e.getMessage());
        }
        logger.info("IniFileReader is functioning correctly");
    }

}
