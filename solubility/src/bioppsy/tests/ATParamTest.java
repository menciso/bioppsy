package bioppsy.tests;

//JOElib
import joelib.smarts.JOESmartsPattern;

//junit
import junit.framework.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Constants;
import bioppsy.Database;
import bioppsy.FailedConstructorException;
import bioppsy.Mol;
import bioppsy.MolSet;
import bioppsy.Param;
import bioppsy.algorithm.MLR;


//java
import java.util.Vector;

/*************************************************************
 * Tests the ATParam class.
 ************************************************************/
public class ATParamTest extends TestCase 
{
    private Database db = null;
    static Logger logger = Logger.getLogger(ATParamTest.class);

    MLR alg;
    MolSet molSet;
    ATParam atParam;

	protected void setUp() 
    {
        try
        {
            db = new Database(Constants.instance().getString("TEST_INI_FILENAME"));
            molSet = new MolSet("filename");
            alg = new MLR(molSet,"algorithm");
            atParam = new ATParam(alg,0,4.0);

        }
        catch(FailedConstructorException fce)
        {
            logger.error(fce.getMessage());
        }
	}

    protected void tearDown() 
    {
    }

    /************************************************************
     * Tests the save(), get() and delete() methods of ATParam
     ***********************************************************/
    public void testSaveDelete()
    {
        ATParam ret;

        String smarts1 = "C";
        String smarts2 = "[C][n]~[#4]";

        atParam.addSmarts(smarts1);
        atParam.addSmarts(smarts2);

        if( !atParam.save(db) )
        {
            fail("Unable to save ATParam");
        }

        if( atParam.modified() )
        {
            fail("ATParam modified after being saved");
        }

        ret = (ATParam) Param.getParam(atParam.getId(),atParam.getAlgorithm(),db);
        if( ret == null )
        {
            fail("Unable to load ATParam");
        }
        if( ret.modified() )
        {
            fail("ATParam modified after being loaded");
        }

        Vector smartsVec = ret.getSmartsVec();
        JOESmartsPattern retSmarts1 = (JOESmartsPattern) smartsVec.elementAt(0);
        JOESmartsPattern retSmarts2 = (JOESmartsPattern) smartsVec.elementAt(1);
        
        if( retSmarts1.getSMARTS().equals(smarts1) )
        {
            assertEquals(retSmarts2.getSMARTS(),smarts2);
                
        }
        else
        {
            assertEquals(retSmarts1.getSMARTS(),smarts2);
            assertEquals(retSmarts2.getSMARTS(),smarts1);
        }

        if( !atParam.delete(db) )
        {
            fail("Unable to delete ATParam");
        }
    }

    /***********************************************************
     * Tests the matchList() function
     *******************************************************/
    public void testMatchList()
    {
        Vector result;
        Mol mol = null;
        ATParam atParam = new ATParam(null);
        String smarts2 = "[N]";
        String smarts3 = "[O]";

        // create a Mol to match
        mol = new Mol("CCNCCNO");

        if( !atParam.addSmarts(smarts2) )
        {
            fail("ATParam failed to accept smarts: " + smarts2);
        }

        if( !atParam.addSmarts(smarts3) )
        {
            fail("ATParam failed to accept smarts: " + smarts3);
        }

        result = atParam.matchList(mol);
        assertEquals(result.size(),3);
        assertEquals(((Integer) result.elementAt(0)).intValue(),3);
        assertEquals(((Integer) result.elementAt(1)).intValue(),6);
        assertEquals(((Integer) result.elementAt(2)).intValue(),7);
    }
}
