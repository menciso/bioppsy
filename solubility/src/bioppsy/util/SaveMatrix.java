package bioppsy.util;


//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;


//Jama
import Jama.Matrix;

//java
import java.sql.SQLException;
import java.util.Vector;

/**************************************************************
 * A SaveMatrix object contains a Jama.Matrix object which
 * is passed to the constructor. This class contains functions
 * which allow the Matrix to be saved.
 **************************************************************/
public class SaveMatrix
{
    static Logger logger = Logger.getLogger(SaveMatrix.class);

    private Matrix M;
    private int rows;
    private int columns;
    private MLR alg;
    private String key;

    public SaveMatrix(MLR alg, String key, Matrix M, int rows, int columns)
    {
        this.alg = alg;
        this.key = key;
        this.M = M;
        this.rows = rows;
        this.columns = columns;
    }

    /*******************************************************************
     * Constructs a new SaveMatrix from rowsVectors. rowsVectors must be a Vector
     * of Vector of Double objects. Each Vector in rowsVectors represents a
     * row of the Matrix. The number of columns in rowsVectors must be consistent.
     ******************************************************************/
    public SaveMatrix(MLR alg, String key, Vector rowsVectors)
        throws FailedConstructorException
    {
        this.alg = alg;
        this.key = key;
        rows = rowsVectors.size();

        if( rows > 0 ) 
        {
            columns = ((Vector) rowsVectors.elementAt(0)).size();
        }
        
        M = new Matrix(rows,columns);

        for( int i=0; i < rows; ++i )
        {
            Vector row = (Vector) rowsVectors.elementAt(i);
            if( row.size() != columns )
            {
                throw new FailedConstructorException(
                    "Inconsistent row length creating SaveMatrix." +
                    " Length: " + row.size() + " Required: " + columns);
            }
            for( int j=0; j < columns; ++j )
            {
                Double value = (Double) row.elementAt(j);
                M.set(i,j,value.doubleValue());
            }
        }
    }

    /*******************************************************************
     * Constructs a new SaveMatrix from rowsVectors. rowsVectors must be a Vector
     * of Vector of Double objects. Each Vector in rowsVectors represents a
     * row of the Matrix. The number of columns in rowsVectors must be consistent
     * otherwise null is returned. Returns null if there are zero columns or rows.
     ******************************************************************/
    public static SaveMatrix rowMatrix(MLR alg, String key, Vector rowsVectors)
    {
        Matrix newMatrix;
        int numColumns, numRows;

        numRows = rowsVectors.size();
        if( numRows <= 0 )
        { 
            return null;
        }
        numColumns = ((Vector) rowsVectors.elementAt(0)).size();
        if( numColumns <= 0 )
        {
            return null;
        }
        
        newMatrix = new Matrix(numRows,numColumns);
        for( int i=0; i < numRows; ++i )
        {
            Vector row = (Vector) rowsVectors.elementAt(i);
            for( int j=0; j < row.size(); ++j )
            {
                double value = ((Double) row.elementAt(j)).doubleValue();
                newMatrix.set(i,j,value);
            }
        }

        return new SaveMatrix(alg,key,newMatrix,numRows,numColumns);
    }

    /*******************************************************************
     * Constructs a new SaveMatrix from columnsVectors. columnsVectors must be a Vector
     * of Vector of Double objects. Each Vector in columnsVectors represents a
     * column of the Matrix. The number of rows in columnsVectors must be consistent
     * otherwise null is returned. Returns null if there are zero columns or rows.
     ******************************************************************/
    public static SaveMatrix columnMatrix(MLR alg, String key, Vector columnsVectors)
    {
        Matrix newMatrix;
        int numColumns, numRows;

        numColumns = columnsVectors.size();
        if( numColumns <= 0 )
        { 
            return null;
        }
        numRows = ((Vector) columnsVectors.elementAt(0)).size();
        if( numRows <= 0 )
        {
            return null;
        }
        
        newMatrix = new Matrix(numRows,numColumns);
        for( int i=0; i < numColumns; ++i )
        {
            Vector column = (Vector) columnsVectors.elementAt(i);
            for( int j=0; j < column.size(); ++j )
            {
                double value = ((Double) column.elementAt(j)).doubleValue();
                newMatrix.set(j,i,value);
            }
        }

        return new SaveMatrix(alg,key,newMatrix,numRows,numColumns);
    }

    /*************************************************************************
     * Constructs a new diagonal k by k SaveMatrix from diagonals. diagonals
     * must be a Vector of k Double objects. The resulting SaveMatrix has
     * the elements in diagonals as its diagonals and 0.0 eveywhere else
     ************************************************************************/
    public static SaveMatrix diagonalMatrix(MLR alg, String key, Vector diagonals)
    {
        Matrix newMatrix;
        int k = diagonals.size();
        newMatrix = new Matrix(k,k);

        for( int i=0; i < k; ++i )
        {
            double value = ((Double) diagonals.elementAt(i)).doubleValue();
            newMatrix.set(i,i,value);
        }

        return new SaveMatrix(alg,key,newMatrix,k,k);
    }

    public Matrix getMatrix()
    {
        return M;
    }

    public int getRows()
    {
        return rows;
    }

    public int getColumns()
    {
        return columns;
    }

    /******************************************************************
     * Saves this Matrix in the database. Returns true on success and
     * false on database error. If this matrix already exists in the 
     * database false will be returned. Please delete any existing 
     * matricies first.
     *****************************************************************/
    public boolean save(Database db)
    {
        int row = 0;
        int column = 0;
        String save;

        try
        {
            for( row=0; row < rows; ++row )
            {
                for( column=0; column < columns; ++column )
                {
                    save =
                        "INSERT INTO Matrix(algorithm,key,row,column,value)" +
                        " VALUES(" +
                        "'" + alg.getName() + "'" +
                        ",'" + key + "'" +
                        "," + row +
                        "," + column +
                        "," + M.get(row,column) +
                        ");";
                    //logger.debug(save);
                    db.execute(save);
                }
            }
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        } 

        return true;
    }

    /*********************************************************
     * Deletes this Matrix from the database. Returns true on
     * success, false on failure. If the Matrix does not exist
     * true is returned.
     *********************************************************/
    public boolean delete(Database db)
    {
        String delete =
            "DELETE FROM Matrix WHERE" +
            " algorithm='" + alg.getName() + "'" +
            " AND key='" + key + "';";

        try
        {
            //logger.debug(delete);
            db.execute(delete);
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }

        return true;
    }

    /************************************************************
     * Returns a Vector of Double corresponding to the given row
     * vector v. If v is not a row vector, null is returned.
     ************************************************************/
    public static Vector getRowVector(Matrix v)
    {
        Vector rowVector = new Vector();
        double[][] arrays = v.getArray();
        if( arrays.length != 1 )
        {
            logger.error("Row vector has " + arrays.length + " rows");
            return null;
        }
        double[] row = arrays[0];
        for( int i=0; i < row.length; ++i )
        {
            rowVector.add(new Double(row[i]));
        }

        return rowVector;
    }
}
