package bioppsy.util;

import org.apache.log4j.Logger;

/**************************************************
 * The notifier class is use to communicate between
 * threads. 
 **************************************************/
public class Notifier
{
    private boolean stop = false;
    private boolean die = false;

    /**********************************************
     * Call this method to stop the thead listening
     * to this object.
     **********************************************/
    public synchronized void stop()
    {
        System.out.println("Stopping thread");
        stop = true;
    }

    /*********************************************
     * Call this method to start the thread listening
     * to this object.
     ************************************************/
    public synchronized void start()
    {
        System.out.println("Starting thread");
        stop = false;
        notifyAll();
    }

    /*************************************************
     * Tells the listening thread to end.
     *************************************************/
    public synchronized void kill()
    {
        System.out.println("Killing thread");
        die = true;
        notifyAll();
    }

    /************************************************
     * If we have been asked to stop, wait until
     * we have been asked to start again.
     *************************************************/
    public synchronized void waitForStart()
    {
        if(stop)
        {
            System.out.println("Waiting for start call");
            try
            {
                wait();
            }
            catch( InterruptedException ie )
            {
                Logger.getLogger(getClass()).info("Thread Interrupted: " + ie.getMessage());
            }
        }
    }

    /****************************************************
     * Tells us whether we should finish or not.
     ****************************************************/
    public synchronized boolean die()
    {
        return die;
    }

    /****************************************************
     * Informs the controlling thread of the current 
     * status of the calculation. numDone is the number
     * of molecules that have been successfully calculated
     * and numError if the number of molecules that could
     * not be calculated.
     ***************************************************/
    public synchronized void notify(int numDone, int numError)
    {
    }
}
