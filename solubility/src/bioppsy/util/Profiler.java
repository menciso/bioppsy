package bioppsy.util;

//log4j
import org.apache.log4j.Logger;

/**********************************************
 * A tool for profiling code. Sums the actual user
 * time taken in milliseconds over many iterations
 * of a section of code. Pass the number of
 * markers which you wish to use to the constructor.
 * Then at the beginning of the loop to be profiled,
 * call the resetMark() method to start the timer.
 * Then at intervals in the loop, call the
 * addToCount(int) method to add the time taken
 * since resetMark() to the total counted time
 * for that indexed position. Then call the results()
 * method to obtain the results after the loop.
 ***********************************************/
public class Profiler
{
    private long baseTime;
    private long[] count;

    static Logger logger = Logger.getLogger(Profiler.class);

    /***************************************************
     * numCount is the number of indices to profile,
     * subsequent calls to addToCount must not pass
     * an index of more than numCount - 1.
     ***************************************************/
    public Profiler(int numCount)
    {
        count = new long[numCount];
        for( int i=0; i < numCount; ++i )
        {
            count[i] = 0;
        }
    }

    /***************************************************
     * Resets the timer to start from the current time
     ***************************************************/
    public void resetMark()
    {
        baseTime = System.currentTimeMillis();
    }

    /***************************************************
     * Add the time passed since resetMark() was called
     * to the total time for index. index must be less than
     * numCount passed to Profiler().
     *****************************************************/
    public void addToCount(int index)
    {
        long currTime = System.currentTimeMillis();
        long timePassed = currTime - baseTime;
        count[index] += timePassed;
    }

    /*******************************************************
     * Returns an array of long 'array' where array[index]
     * is the sum of times in milliseconds between all
     * calls to resetMark() and addToCount(index) for the
     * item at 'index'.
     ******************************************************/
    public long[] results()
    {
        return count;
    }

    /********************************************************
     * Displays the results to stdout
     ******************************************************/
    public void printResults()
    {
        for( int i=0; i < count.length; ++i )
        {
            logger.info("Result " + i + ": " + count[i]);
        }
    }
}
