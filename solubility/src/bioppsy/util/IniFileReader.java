package bioppsy.util;

import java.io.*;
import java.util.Hashtable;
import org.apache.log4j.Logger;

/**************************************************************
 * This class allows the user to read in an ini file and get
 * the values from the file. The ini file provided must contain
 * lines consisting of name=value pairs. The corresponding 
 * values can be obtained by using the get() function by
 * supplying the name.
 * <br><br>
 * Example:
 * <br><br>
 * IniFileReader ifr = new IniFileReader("myfile.ini");<br>
 * ifr.read(); // actually do the reading<br>
 * String value = ifr.getString("myname");
 * int intValue = ifr.getInt("myint");
 * String[] arrayValue = ifr.getStringArray("myarray");
 * <br><br>
 * The file 'myfile.ini' should look like this:
 * <br><br>
 * examplename1=examplevalue1<br>
 * examplename2=examplevalue2
 * <br><br>
 * #linecommented out the last one was blank<br>
 * myname=yaya
 * <br><br>
 * Hence the string 'value' in the above code will contain
 * 'yaya'
 * <br><br>
 * Empty lines and lines commented out with a # as the
 * first character are permitted in the ini file.
 * <br><br>
 * If retrieving an array of Strings the Strings should be
 * separated by commas with no spaces such as:<br>
 * key1=firststring,secondstring,thirdstring
 * <br><br>
 * Note that all error messages are written to a log4j logger
 * with the same name as this class.
 **************************************************************/
public class IniFileReader
{
    // Define a static logger variable so that it references the
    // Logger instance named "UtilTest".
    private static Logger logger = Logger.getLogger(IniFileReader.class);


    String filename;
    InputStream inputStream;
    Hashtable table;

    /*********************************************************
     * Creates a new IniFileReader for the specified file.
     * <br><br>
     * The filename must be given as an absolute filename
     * starting with the src directoy as the base. All
     * ini files are currently in the src/ini directoy.
     * For example, if you wanted to specify the file 
     * 'src/ini/testini.ini', the filename that you would
     * provide to this constructor would be 'ini/testini.ini'.
     * You cannot specify files outside the src directoy tree.
     * <br><br>
     * If the file given does not exist a FileNotFound
     * exception is thrown.
     ********************************************************/
    public IniFileReader( String filename )
        throws FileNotFoundException
    {
        // make the filename absolute
        filename = "/" + filename;
        
        inputStream = getClass().getResourceAsStream(filename);
        if( inputStream == null )
        {
            throw new FileNotFoundException("File not found: " + filename);
        }

        table = new Hashtable();
        this.filename = filename;
    }

    /************************************************************
     * Actually reads the ini file into an internal buffer.
     * This method must be called before any calls to get().
     * Returns true if successfull, false otherwise.
     *************************************************************/
    public boolean read()
    {
        BufferedReader in = null;
        String line;

        try
        {
            in = new BufferedReader(new InputStreamReader(inputStream));

            while( (line = in.readLine()) != null )
            {
                // skip empty lines and 
                // lines commented out with #
                if( (line.length() == 0) || line.startsWith("#") )
                {
                    continue;
                }

                // seperate elements by the '=' sign
                String[] elements = line.split("=");
                if( elements.length == 2 )
                {
                    table.put(elements[0].trim(),elements[1].trim());
                }
                else
                {
                    logger.debug("Invalid line in ini file '" +
                        filename + "' : " + line );
                }
            }
            in.close();
            return true;
        }
        catch( IOException ioe )
        {
            if( in != null )
            {
                try
                { 
                    in.close(); 
                }
                catch (IOException ioe2) 
                { }
            }
            logger.error(ioe.getMessage());
            return false;
        }
    }

    /*****************************************************
     * Returns the value for the given key for elements
     * stored as key=value pairs in the ini file.
     * Returns null if the key is not found.
     * Note read() must be called before this method.
     ***************************************************/
    public String getString(String key)
    {
        return (String) table.get(key);
    }

    /*****************************************************
     * Returns the value for the given key for elements
     * stored as key=value pairs in the ini file.
     * Returns null if the key is not found.
     * Note read() must be called before this method.
     ***************************************************/
    public int getInt(String key)
    {
        return new Integer((String) table.get(key)).intValue();
    }

    /*****************************************************
     * Returns the value for the given key for elements
     * stored as key=value pairs in the ini file.
     * Returns null if the key is not found.
     * Note read() must be called before this method.
     ***************************************************/
    public double getDouble(String key)
    {
        return new Double((String) table.get(key)).doubleValue();
    }

    /*****************************************************
     * Returns the value for the given key for elements
     * stored as key=value pairs in the ini file.
     * Returns null if the key is not found.
     * Note read() must be called before this method.
     ***************************************************/
    public String[] getStringArray(String key)
    {
        return ((String) table.get(key)).split(",");
    }
}
