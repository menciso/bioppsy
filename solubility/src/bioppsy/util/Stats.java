package bioppsy.util;

// java
import java.util.Vector;

/***********************************************
 * Utitily class containing static methods for
 * statistical procedures such as variance.
 ***********************************************/
public class Stats
{

    /**************************************************
     * Returns the variance of the given Vector
     * of Double objects.<br>
     * v = ( n*sum(x^2) - sum(x)^2 ) / n(n-1)
     **************************************************/
    public static double variance(Vector values)
    {
        double value;
        double sumX = 0.0;
        double sumX2 = 0.0;
        double n = values.size();

        for( int i=0; i < values.size(); ++i )
        {
            value = ((Double) values.elementAt(i)).doubleValue();
            sumX += value;
            sumX2 += Math.pow(value,2);
        }
 
        return ( (n*sumX2) - Math.pow(sumX,2) ) / (n*(n-1.0));
    }

    /****************************************************
     * Returns the standard deviation of the given Vector
     * of Double objects.<br>
     * s = sqrt( ( n*sum(x^2) + sum(x)^2 ) / n(n-1) )
     ***************************************************/
    public static double standardDeviation(Vector values)
    {
        return Math.pow(variance(values),0.5);
    }

    /**************************************************
     * Returns the arithmetic mean of the given Vector
     * of Double objects.<br>
     **************************************************/
    public static double mean(Vector values)
    {
        double value;
        double mean = 0.0;
        double n = values.size();

        for( int i=0; i < values.size(); ++i )
        {
            mean += ((Double) values.elementAt(i)).doubleValue();
        }
 
        mean = mean / n;

        return mean;
    }

}
