package bioppsy.algorithm;


//java
import org.apache.log4j.Logger;

import bioppsy.Mol;
import bioppsy.MolSet;
import bioppsy.Param;
import bioppsy.Result;

import Jama.Matrix;

/************************************************************
 * The MeanCenteredAlgorithm centers both the Y and X
 * matrices by subtracting the mean value (from the training
 * set) before performing regression. When properties are
 * calculated, mean values are added to the parameter values
 * and subtracted from the result. i.e. Each element in each
 * column of both the X and Y matrices has the mean value
 * of its column subtracted from it.
 ***********************************************************/
public class MeanCentered extends MLR
{
    static Logger logger = Logger.getLogger(MeanCentered.class);

    /**
     * This is the MeanCenteredAlgorithm method
     */
    public MeanCentered()
    {
        super();
    }

    /*******************************************************
     * Creates a new MeanCenteredAlgorithm with the given MolSet and
     * name. The MolSet is used for training the MeanCenteredAlgorithm's
     * parameters. An intercept Param is added as the first
     * parameter.
     * @param molSet Set of molecules
     * @param name Name of the algorithm (model)
     *******************************************************/
    public MeanCentered(MolSet molSet, String name)
    {
        super(molSet,name);
    }

    /***********************************************************
     * <p>
     * Returns a Nx1 matrix of N experimentally calculated
     * property values. These values are obtained by iterating
     * through this Algorithm's MolSet. null is returned on
     * error. N is the number of molecules in the MolSet.
     * </p>
     * <p>
     * The mean  value for the training MolSet is subtracted
     * from each value returned in the Matrix.
     * </p>
     * @param N number of molecules
     * @return Matrix
     **********************************************************/
    public Matrix getY(int N)
    {
    	int j=0;
        Matrix Y= getExpPropMatrix(N); // Method inherited from Algorithm
        double mean = sum(Y, N)/N;
        for (j=0; j< N; j++)
        {
        	Y.set(j,0, (Y.get(j,0) - mean));
        }
        return Y;
    }

    /**************************************************************
     * <p>
     * Returns the occurrence matrix X.
     * The occurrence matrix is mean centered before returning. For parameter
     * i, the mean occurrence is calculated over all the molecules
     * in the training set. This mean value is then subtracted from
     * the occurrence of that parameter for each molecule in the occurrence
     * matrix. 
     * </p>
     * @param N number of molecules
     * @param k number of parameters
     * @return Matrix
     *************************************************************/
    public Matrix getX(int N, int k)
    {
        int i = 0;
        int j = 0;
        Matrix X=calculateOccurrence(N, k);
        double[] means; // mean value of all parameters
        // make sure we don't have less than N Mol

        // calculate the means
        means = new double[k+1];
        for( i=0; i < means.length; ++i )
        {
            means[i] = sumRow(X, N, i)/N;
            // save the mean so that it can be used
            // to calculate the property later
            Param param = paramList.paramAt(i);
            param.setMean(means[i]);
        }

        for( i=0; i < means.length; ++i )
        {
            // if the parameter is the intercept we
            // do not want to mean center it
            if( paramList.paramAt(i).isIntercept() )
            {
                continue;
            }

            for( j=0; j < N; ++j )
            {
                double meanCentered = X.get(j,i) - means[i];
                X.set(j,i,meanCentered);
            }
        }

        return X;
    }

    /**************************************************************
     * <p>
     * Calculates the property of the given Mol and returns
     * it in a Result object.
     * null is returned on error.
     * </p>
     * <p>
     * To calculate the property of a molecule the 
     * calculateOccurrenceRow() function is first used to determine
     * the occurrence/value of each parameter for the given molecule.
     * Each of these occurrences is multiplied by the coefficient of
     * the corresponding parameter in this algorithm's paramList and
     * the results are summed to obtain the result.
     * </p>
     * <p>
     * Because the algorithm has been mean centered, the mean of
     * each parameter is subtracted from the occurrence for the 
     * molecule before multiplying it by the regression coefficient.
     * The result is then added to the mean property for the 
     * training set since this has also been mean centered.
     * </p>
     * @param mol Molecule
     * @param result Result
     * @return float
     *************************************************************/
    public Matrix calculateProperty(Mol mol, Result result)
    {
        Matrix X;
        int k;
        double prop = 0.0;
        double mcen=0.0;
        Param param;

        k = getNumberParameters();
        X = new Matrix(1,k);

        if( !calculateOccurrenceRow(X,mol,0,k) )
        {
            logger.error("Failed to calculate occurrence for Mol: " +
                mol.getNumber() );
            return null;
        }

        for( int i=0; i < paramList.size(); ++i )
        {
            param = paramList.paramAt(i);
            mcen = X.get(0,i);
            if( !paramList.paramAt(i).isIntercept() )
            {
            	mcen = X.get(0,i) - param.getMean();
            }
            prop += mcen*param.getValue();
        }

        prop += mean;

        result.setMol(mol);
        result.setValue(prop);

        return X;
    }
}

