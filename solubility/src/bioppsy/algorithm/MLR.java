package bioppsy.algorithm;


//JOElib
import joelib.desc.*;
import joelib.molecule.JOEAtom;
//java
import java.util.Vector;
import java.io.*;
import java.util.Iterator;
//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Constants;
import bioppsy.Database;
import bioppsy.DescriptorParam;
import bioppsy.GroupParam;
import bioppsy.Mol;
import bioppsy.MolSet;
import bioppsy.NotFoundException;
import bioppsy.Param;
import bioppsy.ParamList;
import bioppsy.Result;
import bioppsy.ResultCollection;
import bioppsy.util.Notifier;
//Jama
import Jama.Matrix;
//sql
import java.sql.SQLException;
import java.sql.ResultSet;

/************************************************************
 * <p>
 * The Algorithm class is an abstract class providing basic
 * algorithm functionality such as loading and saving, etc.
 * An algorithm is uniquely identified by its name. 
 * An algorithm has a MolSet which is used as a training set 
 * to determine the algorithm's parameters. The type of an 
 * algorithm is determined by the full name of its class 
 * (i.e. by using getClass().getName()).
 * </p>
 * <p>
 * To use an Algorithm call the following:
 * <br><br>
 * Algorithm alg = new Algorithm(molSet,"name");<br>
 * alg.initParameters("trainingsetfilename");<br>
 * alg.calculateParameters();
 * <br><br>
 * Then the algorithm is ready to be used by calling:<br>
 * alg.calculateProperties(MolSet,Notifier);<br>
 * </p>
 * 
 ***********************************************************/
public class MLR
{
    private boolean modified = true;

    protected MolSet molSet = null; // used to calculate parameters
    private String name; // unique name
    // r squared value and standard deviation for the 
    // regression calculation of parameters
    protected double rsq = 0.0;
    protected double sdDev = 0.0;
    // mean Prop value of the training set
    protected double mean = 0.0;
    // variance Prop value of the training set
    protected double variance = 0.0;

    private int id = -1;
    


    protected ParamList paramList;

    static Logger logger = Logger.getLogger(MLR.class);

    
    /*******************************************************
     * A list of all parameters for this algorithm. The
     * last parameter is the intercept if there is one.
     ******************************************************/
    
    public MLR()
    {
        paramList = new ParamList(this);
    }

    /*******************************************************
     * Creates a new multivariate algorithm with the given MolSet and
     * name. The MolSet is used for training the Algorithm's
     * parameters. An intercept Param is added as the first
     * parameter.
     *******************************************************/
    public MLR(MolSet molSet, String name)
    {
        this.molSet = molSet;
        this.name = name;
        paramList = new ParamList(this);
    }

    /**********************************************************
     * Adds the intercept parameter to this Algorithm's 
     * paramList. This parameter is added first so it
     * has an id of 0 by convention.
     **********************************************************/
    public void addIntercept()
    {
        try
        {
            Descriptor descriptor = DescriptorFactory.getDescriptor("Intercept");
            DescriptorParam dp = new DescriptorParam(this);
            dp.setDescriptor(descriptor);
            getParamList().add(dp);
        }
        catch( DescriptorException de )
        {
            logger.warn(de.getMessage());
        }
    }

    /**********************************************************
     * Returns the molecule dataset currently in use
     * @return MolSet
     *********************************************************/
    
    public MolSet getMolSet()
    {
        return molSet;
    }

    /***********************************************************
     * Returns the name of the dataset
     * @return String
     ***********************************************************/
    public String getName()
    {
        return name;
    }

    /***********************************************************
     * Returns the correlation coefficient (R<sup>2</sup>) of the model
     * @return double
     ***********************************************************/
    public double getRsq()
    {
        return rsq;
    }

    /***********************************************************
     * Returns the standard deviation (s.d.) of the model
     * @return double
     ***********************************************************/
    public double getSdDev()
    {
        return sdDev;
    }

    /***********************************************************
     * Returns a list of the parameters used by the algorithm
     * @return ParamList
     ***********************************************************/
    public ParamList getParamList()
    {
        return paramList;
    }

    /***********************************************************
     * Sets the molecule set used by the algorithm
     ***********************************************************/
    public void setMolSet(MolSet molSet)
    {
        this.molSet = molSet;
    }

    /***********************************************************
     * Sets name of the model
     ***********************************************************/
    public void setName(String name)
    {
        this.name = name;
    }
    
    /***********************************************************
     * Sets the correlation coefficient of the obtained algorithm
     ***********************************************************/
    public void setRsq(double rsq)
    {
        this.rsq = rsq;
        setModified();
    }

    /***********************************************************
     * Sets the standard deviation of the obtained algorithm
     ***********************************************************/
    public void setSdDev(double sdDev)
    {
        this.sdDev = sdDev;
        setModified();
    }

    /***********************************************************
     * Sets the mean of the obtained algorithm
     ***********************************************************/
    public void setMean(double mean)
    {
        this.mean = mean;
        setModified();
    }
    
    /***********************************************************
     * Sets the variance of the obtained algorithm
     ***********************************************************/
    public void setVariance(double variance)
    {
        this.variance = variance;
        setModified();
    }

    /***********************************************************
     * Gets the id of the obtained algorithm
     ***********************************************************/
    public int getId()
    {
        return id;
    }

    /***********************************************
     * Loads this Algorithm's ParamList from the database.
     * Returns true if successful, false otherwise.
     * @param db	Molecule Database
     * @return boolean
     ***********************************************/
    public boolean loadParamList(Database db)
    {
        paramList = ParamList.getParamList(this,db);
        if( paramList == null )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // DatabaseInterface implementation

    /*******************************************************
     * Saves this object in the database. Returns true on
     * success, false on failure. Creates the object in the
     * database if it does not exist. This Algorithm's 
     * ParamList and MolSet are also saved.
     * @param db	Database
     * @return boolean
     ******************************************************/
    public boolean save(Database db)
    {
        if( !modified() )
        {
            return true;
        }

        if( molSet != null )
        {
            if( !molSet.save(db) )
            {
                return false;
            }
        }

        try
        {
            if( !exists(db) )
            {
                if( !create(db) )
                {
                    return false;
                }
            }    
            else 
            { 
                if( !doSave(db) )
                {
                    return false;
                }
            }        
        }
        catch(SQLException sqle)
        {
            logger.error(sqle.getMessage());
            return false;
        }

        // save the ParamList after because otherwise
        // this Algorithm may not exist in the database
        // (would be a problem if we had referential
        // integrity constraints in the database)
        if( !paramList.save(db) )
        {
            return false;
        }

        setNotModified();

        return true;
    }

    private boolean doSave(Database db)
        throws SQLException
    {
        String save =
            "UPDATE Algorithm SET ";

        if( molSet != null )
        {
            save +=
                " molSet=" + molSet.getId() + ", ";
        }

        save +=
            "rsq=" + rsq + 
            ", sdDev=" + sdDev +
            ", mean=" + mean +
            ", type='" + getClass().getName() + "'" +
            ", variance='" + variance + "'" +
            " WHERE name='" + getName() + "';";

        logger.debug(save);

        db.execute(save);

        return true;
    }

    private boolean create(Database db)
        throws SQLException
    {
        String create = 
            "INSERT INTO Algorithm(name,type,molSet,rsq,sdDev,mean,variance)" +
            " VALUES(" +
            "'" + getName() + "'" +
            ",'" + getClass().getName() + "'" +
            "," + molSet.getId() +
            "," + rsq + 
            "," + sdDev +
            "," + mean + 
            "," + variance +
            ");";

        logger.debug(create);

        db.execute(create); 

        return true;
    }

    /*****************************************************
     * Deletes this object from the database based on its
     * primary key. Returns true on success and false if
     * there has been an error. If the object is not found
     * true is still returned.
     * @param db Molecule Database
     * @return boolean
     *****************************************************/
    public boolean delete(Database db)
    {
        String delete =
            "DELETE FROM Algorithm WHERE name=" +
            "'" + getName() + "';";

        try
        {
            db.execute(delete);
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }

        // delete the corresponding paramList
        if( !paramList.delete(db) )
        {
            logger.error("Unable to delete ParamList");
            return false;
        }

        setModified();

        return true;
    }

    /******************************************************
     * <p>
     * Returns an Algorithm from the database with the given
     * name. If there is an error or if no Algorithm is found
     * null is returned. The Algorithm's ParamList is loaded 
     * as well.
     * </p>
     * @param name 	Name of the model
     * @param db 	Molecule Database
     * @return Algorithm (null if there is an error)
     *******************************************************/
    public static MLR getAlgorithm(String name, Database db)
    {
        ResultSet rs;
        String type;
        int molSetId;
        double retRsq;
        double retSdDev;
        double retMean;
        double retVariance;
        MolSet molSet = null;
        MLR alg;
//        ParamList retParamList;

        String get =
            "SELECT * FROM Algorithm WHERE name=" +
            "'" + name + "';";
 
        logger.debug(get);

        try
        {
            rs = db.executeQuery(get);
            while( rs.next() )
            {
                molSetId = rs.getInt("molSet");
                if( !rs.wasNull() )
                {
                    molSet = MolSet.get(molSetId,db);
                    if( molSet == null )
                    {
                        logger.error("Algorithm.getAlgorithm() -> " +
                            "Unable to retrieve MolSet: " + molSetId);
                    }
                }
                retRsq = rs.getDouble("rsq");
                retSdDev = rs.getDouble("sdDev");
                retMean = rs.getDouble("mean");
                retVariance = rs.getDouble("variance");
                type = rs.getString("type");

                alg = (MLR) Class.forName(type).newInstance();

                alg.setMolSet(molSet);
                alg.setName(name);
                alg.setRsq(retRsq);
                alg.setSdDev(retSdDev);
                alg.setMean(retMean);
                alg.setVariance(retVariance);
                
                if( !alg.loadParamList(db) )
                {
                    logger.error("Unable to load Algorithm's ParamList: " +
                        alg.getName() );
                    return null;
                }

                if( !alg.load(db) )
                {
                    logger.error("Unable to load Algorithm exta data: " + 
                        alg.getName());
                }

                alg.setNotModified();
                return alg;
            }
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
        }
        catch( ClassNotFoundException cnfe )
        {
            logger.error("Class not found: " + cnfe.getMessage());
        }
        catch( InstantiationException ie )
        {
            logger.error("Unable to instantiate: " + ie.getMessage());
        }
        catch( IllegalAccessException iae )
        {
            logger.error("Illegal access: " + iae.getMessage());
        }

        return null;
    }

    /******************************************************
     *  <p>
     * Renames an algorithm
     * </p>
     * @param oldname Old name of the algorithm
     * @param newname New name of the algorithm
     * @param db	  Database used by the algorithm
     * @return boolean
     *******************************************************/
    public static boolean renameAlgorithm(String oldname,String newname, Database db)
    {
 //       ResultSet rs;
     
        String rename1 =
            "Update Algorithm set name=" +
            "'" + newname + "' WHERE name='"+oldname+"';";
 
        String rename2 =
            "Update Result set algorithm=" +
            "'" + newname + "' WHERE algorithm='"+oldname+"';";

        String rename3 =
            "Update Parameter set algorithm=" +
            "'" + newname + "' WHERE algorithm='"+oldname+"';";

        String rename4 =
            "Update ParameterExtra set algorithm=" +
            "'" + newname + "' WHERE algorithm='"+oldname+"';";

        String rename5 =
            "Update Matrix set algorithm=" +
            "'" + newname + "' WHERE algorithm='"+oldname+"';";

        
        logger.debug(rename1);
        logger.debug(rename2);
        logger.debug(rename3);
        logger.debug(rename4);
        logger.debug(rename5);

        try
        {
            db.execute(rename1);
            db.execute(rename2);
            db.execute(rename3);
            db.execute(rename4);
            db.execute(rename5);
            return true;
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
        }

        return false;
    }  
    
    
    /*************************************************************
     * This function loads any extra data an Algorithm may need.
     * This is specifically to allow subclasses to load extra
     * data for their particular purposes. Currently not used.
     * @param db Molecule Database
     ************************************************************/
    public boolean load(Database db)
    {
        return true;
    }

    /**************************************************************
     * Returns all the names of the Algorithms in the database as
     * a Vector of Strings. Returns null on failure.
     * @param db Molecule Database
     * @return vector (null on failure)
     *************************************************************/
    public static Vector getNames( Database db )
    {
        ResultSet rs;
        String name;
        Vector names = new Vector();

        String getNames = 
            "SELECT name FROM Algorithm;";

        // retrieve the names from the database
        try
        {
            rs = db.executeQuery(getNames);
            while (rs.next())
            {
                name = rs.getString("name");
                names.add(name.trim());
            }
            return names;
        }
        catch( SQLException sqle )
        {
            logger.error("Failed to retrieve Algorithm names");
            logger.error(sqle.getMessage());
            return null;
        }
    }

    /******************************************************
     * Returns true if this object has already been saved in
     * the database, false otherwise.
     * @param db Molecule Database
     * @return boolean
     ******************************************************/
    public boolean exists(Database db)
        throws SQLException
    {
        ResultSet rs;

        String exists = 
            "SELECT name FROM Algorithm WHERE" +
            " name='" + getName() + "';";

        rs = db.executeQuery(exists);
        while( rs.next() )
        {
            return true;
        }

        return false;
    }

    /*****************************************************
     * Returns true if this Object has been modified since
     * it has last been saved, false otherwise. If this
     * Object does not have an id (or other primary key) 
     * true is returned.
     ****************************************************/
    public boolean modified()
    {
        return modified;
    }

    /*****************************************************
     * Registers that this object has been modified.
     *****************************************************/
    public void setModified()
    {
        modified = true;
    }

    /*****************************************************
     * Registers that this object has not been modified.
     *****************************************************/
    public void setNotModified()
    {
        modified = false;
    }

    /*****************************************************
     * Returns true if this object is equal to the given
     * object, false otherwise.
     * @param other Algorithm
     * @return boolean
     ****************************************************/
    public boolean equals(MLR other)
    {
        if( !getClass().getName().equals(
            other.getClass().getName()) )
        {
            return false;
        }
        if( getName().equals(other.getName()) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /********************************************************
     * Calculates the parameters for this Algorithm using
     * this Algorithm's MolSet. Returns true if successful,
     * false otherwise.
     *
     * <p>
     * Multivariate Regression equation:<br><br>
     *
     * Y = XB + e<br><br>
     * 
     * k = numAtom + 2 (since there are 2 correction factors)<br>
     *     - indicates total number of parameters.<br><br>
     *
     * N - number of molecules 
     *
     * Y - Nx1 matrix of N Prop values where S are the experimentally<br>
     *     calculated property values (same units as provided in data).
     * <br><br>
     * X - Nx(k+1) matrix storing the occurrence of correction factors.<br>
     *     The first column of X is filled with ones since the first<br>
     *     parameter indicates the intercept.<br>
     *     The ij'th member of X is the occurrence of the j'th parameter<br>
     *     for the i'th data point.<br><br>
     *
     * B - (k+1)x1 matrix storing the regression parameters. The first<br>
     *     row is the intercept. This is what we want to calculate.<br><br>
     *
     * e - (k+1)x1 matrix indicating the regression error. The average<br>
     *     error is assumed to be constant so this term is ignored.<br><br>
     *
     * From the regression equation, assuming e=0, we can calculate B as:
     * <br><br>
     *
     * B = (X'X)-1 * X' * Y
     * @return boolean
     *******************************************************/
    public boolean calculateParameters()
    {
        if( molSet == null )
        {
            logger.error("Attempted to calculate parameters for Algorithm with no MolSet");
            return false;
        }

        int N; // number of molecules
        int k; // number of parameters
        Matrix Y,X,B;

        N = molSet.size();

        if( N <=0 )
        { 
            logger.error("Algorithm has an empty MolSet");
            return false;
        }
        Y = getY(N);
        
        if( Y == null )
        {
            logger.error("Unable to initialise experimental property matrix");
            return false;
        }

        // k is the number of parameters 
        // minus 1 for the intercept
        k = getNumberParameters() - 1;
        if( k < 0 )
        {
            return false;
        }
        X = getX(N,k);
        if( X == null )
        {
            logger.error("X matrix is null");
            return false;
        }

        // calculate B
        B = calculateB(X,Y,k);

        // calculate r squared for this regression
        // Equation will differ whether there's an intercept or not
        boolean hasInt=false;
        for( int i=0; i < paramList.size(); ++i )
        {
            hasInt = paramList.paramAt(i).isIntercept();
            if(hasInt)
            	break;
        }
        
        rsq = rsq(Y,B,X,N,hasInt);
        // calculate standard deviation as well
        sdDev = sdDev(Y,B,X,N);

        if( !setParameters(B) )
        {
            return false;
        }

        setModified();

        return true;
    }

    /*****************************************************************
     * Returns a (k+1)x1 Matrix B = (X'X)-1*X'*Y (regression parameters 
     * matrix). X must be a Nx(k+1) Matrix and Y must be a Nx1 Matrix.
     * @param X Occurrence of correction factors (descriptors) matrix
     * @param Y Experimental properties matrix
     * @return B
     *****************************************************************/
    public static Matrix calculateB(Matrix X, Matrix Y, int k)
    {
        Matrix I = Matrix.identity(k+1,k+1);
        Matrix Xt = X.transpose();     // (k+1)xN
//    	for (int i=0; i<X.getRowDimension();i++)
//    	{
//    		for(int j=0; j<X.getColumnDimension();j++)
//        	{
//        	    System.out.print(i + " " + j + " " + X.get(i, j) + "   ");
//        	}
//        	System.out.print("\n");
//        }
        Matrix XtX = Xt.times(X);      // (k+1)x(k+1)
//        for(int i=0; i<=k;i++)
//        {
//        	for (int j=0; j<=k;j++)
//        	 System.out.print(i + " " + j + " " + XtX.get(i, j) + " " + I.get(i, j) + "\n");
//        }
        Matrix XtXinv = XtX.solve(I);  // (k+1)x(k+1)
        Matrix XtY = Xt.times(Y);      // (k+1)x1
        return XtXinv.times(XtY);      // (k+1)x1
    }

    /*****************************************************************
     * Returns the correlation coefficient (R<sup>2</sup>) value of the
     * regression.<br><br>
     *
     * The r squared value is calculated using the following equation:<br><br>
     *
     * rsq = <br>
     * (B'X')Y - (sumY)^2/N <br>
     * ------------------<br>
     * Y'Y - (sumY)^2/N <br>
     * 
     * Note that this equation only gives a result between zero and one if
     * the intercept is not set to zero (ie. we are applying standard linear regression).
     * Check  http://global.oup.com/booksites/content/0199268010/samplesec3
     * for details.
     * @param Y Nx1 matrix of experimental property values
     * @param B (k+1)x1 matrix of calculated regression coefficients
     * @param X Nx(k+1) matrix, each row is the calculated occurrences
     * of each molecule in the training set, each column the occurrence
     * for a particular parameter
     * @param N - number of molecules in the training set
     * @return float
     *****************************************************************/
    public static double rsq(Matrix Y, Matrix B, Matrix X, int N, boolean hasInt)
    {
        // k+1 = K
        // Y - Nx1
        // B - Kx1
        // X - NxK
    	double inv= 1.0/N;
    	Matrix I = Matrix.identity(N,1);
    	//Matrix It = I.transpose();
        Matrix Bt = B.transpose(); // 1xK
        Matrix Yt = Y.transpose(); // 1xN
        Matrix Xt = X.transpose(); // KxN
        double sumY = sum(Y,N);
        double sq = sumY*sumY/N;

        Matrix BtXtY = Bt.times(Xt).times(Y); // 1x1
        Matrix YtY = Yt.times(Y); // 1x1
        Matrix XB=X.times(B);
//        Matrix YtIItY = Yt.times(I).times(It).times(Y);
        double mean=sum(Y,N)*inv;
        double result=0;
        //System.out.print(hasInt + " Here \n");
        if(hasInt)
        {
            //double sse = YtY.get(0,0) -BtXtY.get(0,0);
            //double sse=sumSQ(Y.minus(XB),N);
            //double ssr= BtXtY.get(0,0)-inv*Math.pow(sum(Y,N),2);
            double ssr=sumSQ(XB.minus(I.times(mean)),2);
            //double ssto=sumSQ(Y, N);//-inv*Math.pow(sum(Y,N),2);
            double ssto=sumSQ(Y.minus(I.times(mean)),2);
            //System.out.print(mean + "mean "+ ssr + "ssr " + sse + "sse " + ssto +"ssto\n");
            //System.out.print((BtXtY.get(0,0) - sq) / (YtY.get(0,0) - sq) + " r2 "+ ssr/ssto + "r2 " + (1.0-sse/ssto) +" r2\n");
            
            result = (BtXtY.get(0,0) - sq) / (YtY.get(0,0) - sq);
            if(result > 1.0 || result < 0.0)
            	result = ssr/ssto;
            	

        }
        else
        {
            double xy=BtXtY.get(0,0);
            double y2=YtY.get(0,0);
            result=xy/y2;
            //System.out.print(xy + " xy "+ y2 + " y2 " + xy/y2 + " r2 \n");
        }

        return result;

    }

    /*****************************************************************
     * Returns the standard deviation value of the regression.<br><br>
     *
     * The standard deviation value is calculated using the following
     * equation:<br><br>
     *
     * sdDevSq = sqrt of: <br>
     * Y'Y - B'X'Y<br>
     * -----------<br>
     * N - 1
     * 
     * @param Y  Nx1 matrix of experimental property values
     * @param B  (k+1)x1 matrix of calculated regression coefficients
     * @param X  Nx(k+1) matrix, each row is the calculated occurrences
     * of each molecule in the training set, each column the occurrence
     * for a particular parameter
     * @param N number of molecules in the training set
     * @return float
     *****************************************************************/
    public static double sdDev(Matrix Y, Matrix B, Matrix X, int N)
    {
        // k+1 = K
        // Y - Nx1
        // B - Kx1
        // X - NxK

        Matrix Bt = B.transpose(); // 1xK
        Matrix Yt = Y.transpose(); // 1xN
        Matrix Xt = X.transpose(); // KxN

        Matrix BtXtY = Bt.times(Xt).times(Y); // 1x1
        Matrix YtY = Yt.times(Y); // 1x1

        return Math.sqrt((YtY.get(0,0) - BtXtY.get(0,0)) / (N-1));
    }

    /*******************************************************************
     * Returns all the values in the given Matrix Y of the given size N
     * summed together. The given Matrix must be a column vector with
     * N rows.
     * @param Y Nx1 matrix of experimental property values
     * @param N number of molecules in the training set
     * @return float
     ******************************************************************/
    public static double sum(Matrix Y, int N)
    {
        double sum = 0.0;

        for( int i=0; i < N; ++i )
        {
            sum += Y.get(i,0);
        }

        return sum;
    }
    
    /*******************************************************************
     * Returns all the values in the given Matrix Y of the given size N
     * squared and summed together. The given Matrix must be a column vector with
     * N rows.
     * @param Y Nx1 matrix of experimental property values
     * @param N number of molecules in the training set
     * @return float
     ******************************************************************/
    public static double sumSQ(Matrix Y, int N)
    {
        double sumSQ = 0.0;

        for( int i=0; i < N; ++i )
        {
            sumSQ += Math.pow(Y.get(i,0),2);
        }

        return sumSQ   	;
    }
    
    /*******************************************************************
     * Returns all the values in the given Matrix Y of the given size N x k
     * summed together.
     * @param Y Nx1 matrix of experimental property values
     * @param N number of molecules in the training set
     * @param k number of parameters
     * @return float
     ******************************************************************/
    
    public static double sum(Matrix Y, int N, int k)
    {
        double sum = 0.0;

        for( int i=0; i < N; ++i )
        {
        	for (int j=0; j<k;j++)
        	{
        		sum += Y.get(i,j);
        	}
        }

        return sum;
    }
    
    /*******************************************************************
     * Returns all the values in row i of the given Matrix Y of 
     * size N x k, k>=i summed together.
     * @param Y Nx1 matrix of experimental property values
     * @param N number of molecules in the training set
     * @param i row index
     * @return float
     ******************************************************************/
    
    public static double sumRow(Matrix Y, int N, int i)
    {
        double sumRow = 0.0;

        for( int j=0; j < N; ++j )
        {
            sumRow += Y.get(j,i);
        }
        return sumRow;
    }
    
    /*******************************************************************
     * Returns the variance of all the values in row i of the given Matrix Y of 
     * size N x k, k>=i
     * @param Y Nx1 matrix of experimental property values
     * @param N number of molecules in the training set
     * @param i row index
     * @return float
     ******************************************************************/
    
    public static double varianceRow(Matrix Y, int N, int i)
    {
        double sumsq = 0.0;
        double sum = sumRow(Y, N, i);
        for( int j=0; j < N; ++j )
        {
            sumsq += Math.pow(Y.get(j,i),2);
        }
        double varianceRow = ((N*sumsq) - Math.pow(sum,2) ) / (N*(N-1.0));
        return varianceRow;
    }
    

    /******************************************************************
     * Sets the parameters and the intercept of this Algorithm from
     * a (k+1)x1 Matrix B. This Algorithm must have k parameters where (k+1)
     * is the number of rows in the given Matrix B. Returns true
     * on success, false on failure. Remember the first parameter
     * is always the intercept.
     * @param B regression coefficient matrix
     * @return boolean
     ******************************************************************/
    public boolean setParameters(Matrix B)
    {
        int i;
        Param param;

        for( i=0; i < paramList.size(); ++i )
        {
            param = paramList.paramAt(i);
            param.setValue(B.get(i,0));
        }

        return true;
    }

    /***********************************************************
     * Returns Matrix Y (dependent variables)
     * Uses the getExpPropMatrix method
     * @param N number of molecules
     * @return matrix
     */
    public Matrix getY(int N)
    {
    	Matrix Y = this.getExpPropMatrix(N);
    	return Y;
    }
    
    
    /***********************************************************
     * Returns a Nx1 matrix of N experimentally calculated
     * property values. These values are obtained by iterating
     * through this Algorithm's MolSet. null is returned on
     * error.
     * @param N number of molecules
     * @return matrix (null on failure)
     **********************************************************/
    public Matrix getExpPropMatrix(int N)
    {
        int j = 0; // keep track of where we are
        Matrix Y = new Matrix(N,1);
        Mol mol;

        Iterator i = molSet.getIterator();
        if( i == null )
        {
            return null;
        }
        while( i.hasNext() )
        {
            // don't want to overrun Y
            if( j >= N )
            {
                logger.error("getExpPropMatrix() called with N larger " +
                    "than internal MolSet");
                return null;
            }

            mol = (Mol) i.next();
            // notice that the matrix index begins at 0
            try
            {
                Y.set(j,0,mol.getExpProp());
            }
            catch( NotFoundException nfe )
            {
                logger.error(nfe);
                return null;
            }
            ++j;         
        }

        return Y;
    }
    
    /**************************************************************
     * Returns matrix X (independent variables)
     * Uses the calculateOcurrence method
     * @param N Number of molecules
     * @param k number of parameters
     */
    public Matrix getX(int N, int k)
    {
    	Matrix X = this.calculateOccurrence(N, k);
    	return X;
    }

    /**************************************************************
     * <p>
     * The occurrence matrix X is returned if successful, null otherwise.
     * <p>
     * The occurrence matrix X consists of N rows where N is the number of
     * molecules in the training data set. The j'th member of each row
     * indicates the frequency the j'th parameter occurred (or the calculated
     * value of the j'th parameter) for the molecule. The first column
     * is filled with 1's since the first parameter is the intercept.
     * There are k+1 columns where k is the number of parameters.
     * </p>
     * <p>
     * N and k are passed to this method just to avoid obtaining
     * them more than once.
     * </p>
     * 
     * @param N number of molecules in this Algorithm's MolSet<br>
     * @param k number of parameters<br>
     * @return Matrix
     * 
     *************************************************************/
    public Matrix calculateOccurrence(int N, int k)
    {
        int i = 0;
        Iterator msi;
        Mol mol; 
        Matrix X;

        // reset the occurrence of all the parameters
        paramList.resetFrequencies();
 
        msi = molSet.getIterator();
        if( msi == null )
        {
            logger.error("Unable to obtain iterator for MolSet: " +
                molSet.getFile().getName() );
            return null;
        }

        X = new Matrix(N,k+1); // initialise with all 0s

        // iterate through all Mol in the MolSet
        while( msi.hasNext() )
        {
            // make sure we don't have more than N
            // Mol
            if( i >= N )
            {
                logger.error("Out of bounds in MolSet. " +
                    "expected size: " + N);
                return null;
            }
            mol = (Mol) msi.next();
            calculateOccurrenceRow( X, mol, i, k);
            ++i;
        }
        // make sure we don't have less than N Mol
        if( N > i )
        {
            logger.error("Not enough Mol in MolSet. " +
                "expected size: " + N + " have: " + i);
            return null;
        }

        return X;
    }

    /**************************************************************
     * Returns the number of parameters that this algorithm has.
     * Returns a non-negative value on success, -1 if there has
     * been an error. This number includes the intercept. This value
     * is equal to (k+1).
     * @return integer
     *************************************************************/
    public int getNumberParameters()
    {
        if( paramList == null )
        {
            logger.error("Smarts patterns not initialised");
            return -1;
        }
        else
        {
            return paramList.size();
        }
    }

    /*************************************************************
     * <p>
     * Calculates the properties (Prop) of all the Mol in the
     * given MolSet and stores these results in this Algorithm's
     * ResultCollection.
     * The function calculateProperty(Mol) is used for
     * each of the Mol in the given MolSet.
     * True is returned on success, false on failure.
     * </p>
     * <p>
     * After NUM_RESULTS_BEFORE_SAVE Results have been calculated
     * the internal ResultCollection is saved. The ResultCollection
     * is also saved before the method returns.
     * </p>
     * <p>
     * The Notifier object is used for::
     * <br><br>
     * 1. After the property of every Mol in the given MolSet is
     * calculated, the waitForStart() method of the Notifier is
     * called. Thus if you want to pause the calculation call
     * the stop method of the Notifier. Also the die() method
     * is called so if you want the calculation to stop call
     * kill() method of the Notifier.
     * </p>
     * @param molSet Set of molecules
     * @param not Notifier
     * @param db Molecule database
     * @return boolean
     ************************************************************/
    public boolean calculateProperties(MolSet molSet, Notifier not, Database db)
    {
        Mol mol;
        Result result;
        boolean returnValue = true;
        Iterator msi = molSet.getIterator();
        ResultCollection rc = new ResultCollection(this,molSet);
        if( !rc.delete(db) )  // delete any previous results
        {
            logger.error("Unable to delete old results to calculate new ones");
            return false;
        } 
        int numDone = 0;
        int numError = 0;
  
        while( msi.hasNext() )
        {
            mol = (Mol) msi.next();
            result = new Result();
            Matrix matrix = null;
            try
            {
                matrix = calculateProperty( mol, result );
            }
            catch( NullPointerException npe )
            {
                logger.error(npe.getMessage());
                matrix = null;
            }
            if( matrix == null )
            {
                logger.error("Failed to calculate property for Mol: " +
                    mol.getNumber() );
                returnValue = false;
                ++numError;
            }
            else
            {
                ++numDone;
                result.setResultCollection(rc);
                rc.add(result);
                // if the size of the ResultSet is over a certain amount,
                // notify the Notifier to save and display it.
                if( rc.size() >= 
                    Constants.instance().getInt("NUM_RESULTS_BEFORE_SAVE") )
                {
                    if( !rc.save(db) )
                    {
                        logger.error("Failed to save ResultCollection");
                    }
                    not.notify(numDone,numError);
                }
                Thread.currentThread();
				// don't hog the processor
                Thread.yield();
            }

            // check to see if another thread wants us to stop
            // this method blocks if we are told to wait
            not.waitForStart();
            // check to see if we should die
            if( not.die() )
            {
                return returnValue;
            }
        }

        // now that we have finished, make sure to save 
        // any remaining Results
        if( !rc.save(db) )
        {
            logger.error("Failed to save final ResultCollection");
        }
        not.notify(numDone,numError);

        return returnValue;
    }

    /**************************************************************
     * <p>
     * Calculates the property (Prop) of the given Mol and returns
     * it in a Result object.
     * null is returned on error.
     * </p>
     * <p>
     * To calculate the property of a molecule the 
     * calculateOccurrenceRow() function is first used to determine
     * the occurrence/value of each parameter for the given molecule.
     * Each of these occurrences is multiplied by the coefficient of
     * the corresponding parameter in this algorithm's paramList and
     * the results are summed to obtain Prop.
     * </p>
     *************************************************************/
    public Matrix calculateProperty(Mol mol, Result result)
    {
        Matrix X;
        int k;
        double prop = 0.0;

        k = getNumberParameters();
        X = new Matrix(1,k);

        if( !calculateOccurrenceRow(X,mol,0,k) )
        {
            logger.error("Failed to calculate occurrence for Mol: " +
                mol.getNumber() );
            return null;
        }

        for( int i=0; i < paramList.size(); ++i )
        {
            prop += X.get(0,i)*paramList.paramAt(i).getValue();
        }
        
        result.setMol(mol);
        result.setValue(prop);

        return X;
    }

    /*******************************************************
     * <p>
     * Initialises the smarts patterns that are used as
     * atomic typing parameters for this Algorithm. True is 
     * returned if the patterns have been read in successfully, 
     * false otherwise. 
     * If any invalid SMARTS pattern is 
     * encountered false is returned. If the file is empty
     * true is returned without doing anything.
     * </p>
     * <p>
     * <b>File Format:</b><br><br>
     *
     * Each line in the file should start with a number,
     * indicating the parameter number of the SMARTS pattern.
     * Several SMARTS patterns can have the same parameter
     * number. This indicates that a match for any one
     * of the parameters with the same parameter number
     * will be counted as a match for that parameter.
     * <br><br>
     *
     * After the number there should be one or more spaces
     * and then a single SMARTS pattern until the end of
     * the line. For example:<br><br>
     *
     * 1 [CX4;H4]<br>
     * 1 [CX4;H3]<br>
     * 2 [CX4;H3][#6]<br><br>
     *
     * The SMARTS pattern cannot contain any spaces at all.
     * Each line must be in order and no number can be
     * missing.
     *
     * </p>
     * <p>
     * The SMARTS patters in the given file are taken as
     * being in increasing precedence. An atom in a 
     * molecule can match at most one of the given patterns.
     * This match is taken as the last pattern in the
     * list that the given atom matches.
     * </p>
     * @param filename Name of the file
     * @return boolean
     ******************************************************/
    public boolean addSmarts(String filename)
    {
        return addSmarts(filename,"bioppsy.ATParam");
    }
    
    /*******************************************************
     * <p>
     * Initialises the smarts patterns that are used as
     * atomic typing parameters for this Algorithm. True is 
     * returned if the patterns have been read in successfully, 
     * false otherwise. 
     * If any invalid SMARTS pattern is 
     * encountered false is returned. If the file is empty
     * true is returned without doing anything.
     * </p>
     * <p>
     * <b>File Format:</b><br><br>
     *
     * Each line in the file should start with a number,
     * indicating the parameter number of the SMARTS pattern.
     * Several SMARTS patterns can have the same parameter
     * number. This indicates that a match for any one
     * of the parameters with the same parameter number
     * will be counted as a match for that parameter.
     * <br><br>
     *
     * After the number there should be one or more spaces
     * and then a single SMARTS pattern until the end of
     * the line. For example:<br><br>
     *
     * 1 [CX4;H4]<br>
     * 1 [CX4;H3]<br>
     * 2 [CX4;H3][#6]<br><br>
     *
     * The SMARTS pattern cannot contain any spaces at all.
     * Each line must be in order and no number can be
     * missing.
     *
     * </p>
     * <p>
     * The SMARTS patters in the given file are taken as
     * being in increasing precedence. An atom in a 
     * molecule can match at most one of the given patterns.
     * This match is taken as the last pattern in the
     * list that the given atom matches.
     * </p>
     * @param inputStream SMARTS pattern
     * @return boolean
     ******************************************************/
    public boolean addSmarts(InputStream inputStream)
    {
        return addSmarts(inputStream,"bioppsy.ATParam");
    }
    
    /*******************************************************
     * <p>
     * Initialises the smarts patterns that are used as
     * the given type of parameters for this Algorithm. True is 
     * returned if the patterns have been read in successfully, 
     * false otherwise. 
     * If any invalid SMARTS pattern is 
     * encountered false is returned. If the file is empty
     * true is returned without doing anything.
     * </p>
     * <p>
     * The given type must be the class name of a class that
     * extends the bioppsy.ATParam class. Currently this
     * is only property.GroupParam or bioppsy.ATParam.
     * </p>
     * <p>
     * <b>File Format:</b><br><br>
     *
     * Each line in the file should start with a number,
     * indicating the parameter number of the SMARTS pattern.
     * Several SMARTS patterns can have the same parameter
     * number. This indicates that a match for any one
     * of the parameters with the same parameter number
     * will be counted as a match for that parameter.
     * <br><br>
     *
     * After the number there should be one or more spaces
     * and then a single SMARTS pattern until the end of
     * the line. For example:<br><br>
     *
     * 1 [CX4;H4]<br>
     * 1 [CX4;H3]<br>
     * 2 [CX4;H3][#6]<br><br>
     *
     * The SMARTS pattern cannot contain any spaces at all.
     * Each line must be in order and no number can be
     * missing.
     *
     * </p>
     * <p>
     * The SMARTS patters in the given file are taken as
     * being in increasing precedence. An atom in a 
     * molecule can match at most one of the given patterns.
     * This match is taken as the last pattern in the
     * list that the given atom matches.
     * </p>
     * @param filename Name of the file with the SMARTS patterns
     * @param type class name
     * @return boolean
     ******************************************************/
    public boolean addSmarts(String filename, String type)
    {
        try
        {
            InputStream inputStream = new FileInputStream(filename);
            {
                return addSmarts(inputStream,type);
            }
        }
        catch( FileNotFoundException fnfe )
        {
            logger.error(fnfe.getMessage());
            return false;
        }
    }

    /*******************************************************
     * <p>
     * Initialises the smarts patterns that are used as
     * the given type of parameters for this Algorithm. True is 
     * returned if the patterns have been read in successfully, 
     * false otherwise. 
     * If any invalid SMARTS pattern is 
     * encountered false is returned. If the file is empty
     * true is returned without doing anything.
     * </p>
     * <p>
     * The given type must be the class name of a class that
     * extends the bioppsy.ATParam class. Currently this
     * is only property.GroupParam or bioppsy.ATParam.
     * </p>
     * <p>
     * <b>File Format:</b><br><br>
     *
     * Each line in the file should start with a number,
     * indicating the parameter number of the SMARTS pattern.
     * Several SMARTS patterns can have the same parameter
     * number. This indicates that a match for any one
     * of the parameters with the same parameter number
     * will be counted as a match for that parameter.
     * <br><br>
     *
     * After the number there should be one or more spaces
     * and then a single SMARTS pattern until the end of
     * the line. For example:<br><br>
     *
     * 1 [CX4;H4]<br>
     * 1 [CX4;H3]<br>
     * 2 [CX4;H3][#6]<br><br>
     *
     * The SMARTS pattern cannot contain any spaces at all.
     * Each line must be in order and no number can be
     * missing.
     *
     * </p>
     * <p>
     * The SMARTS patters in the given file are taken as
     * being in increasing precedence. An atom in a 
     * molecule can match at most one of the given patterns.
     * This match is taken as the last pattern in the
     * list that the given atom matches.
     * </p>
     * @param inputStream SMARTS patterns
     * @param type Class name
     * @return boolean
     ******************************************************/
    public boolean addSmarts(InputStream inputStream, String type)
    {
        BufferedReader in = null;
        String line;
        int i = 0;
        int lastParam = 0;
        int param = 0;
        String pattern;
        ATParam atParam;

        try
        {
            in = new BufferedReader(new InputStreamReader(inputStream));

            lastParam = param = i = 0;

            while( (line = in.readLine()) != null )
            {
                ++i;

                // split the line on whitespace
                String[] splitLine = line.trim().split(" ");

                // if smarts string contains any whitespace
                // warn and exit
                if( splitLine.length != 2 )
                {
                    logger.error("Invalid line in SMARTS file " +
                        " line number: " + i);
                    return false;
                }

                // the first element is the parameter number
                lastParam = param; // save the last param
                param = new Integer(splitLine[0].trim()).intValue();
                
                // the second element is the pattern
                pattern = splitLine[1].trim();
                
                // if the current parameter number is equal
                // to the last parameter number add the new
                // smarts to the last Param in the ParamList
                if( param == lastParam )
                {
                    atParam = (ATParam) paramList.paramAt(
                        paramList.size() - 1 );
                    if( !atParam.addSmarts(pattern) )
                    {
                        return false;
                    }
                }
                // otherwise add a new Param to the ParamList
                else
                {
                    atParam  = (ATParam) Class.forName(type).newInstance();
                    atParam.setAlgorithm(this);
                    if( !atParam.addSmarts(pattern) )
                    {
                        return false;
                    }
                    else
                    {
                        if( !getParamList().add(atParam) )
                        {
                            return false;
                        }
                    }
                }
            }
            in.close();
            return true;
        }
        catch( IOException ioe )
        {
            if( in != null )
            {
                try
                { 
                    in.close(); 
                }
                catch (IOException ioe2) 
                { }
            }
            logger.error(ioe.getMessage());
            return false;
        }
        catch( ClassNotFoundException cnfe )
        {
            logger.error("Class not found: " + cnfe.getMessage());
            return false;
        }
        catch( InstantiationException ie )
        {
            logger.error("Instantiation exception: " + ie.getMessage());
            return false;
        }
        catch( IllegalAccessException iae )
        {
            logger.error("Illegal Access: " + iae.getMessage());
            return false;
        }
    }


    /********************************************************************
     * <p>
     * Calculates the occurrence of all the k parameters for the given
     * Mol and stores them in the given row number of the given Matrix.
     * Returns true on success and false on failure. Note the Matrix X
     * must be initialised to all zeros before calling this function.
     * Only this method needs to be overridden in a subclass to implement
     * a different type of Algorithm. When a particular particular parameter
     * occurs in the given Mol, the occurrence of that parameter is incremented
     * by the number of times the parameter occurred in the Mol. This
     * uses the ATParam.incrementOccurrence(int) method.
     * </p>
     * <p>
     * Atom Typing Parameters are ordered in increasing
     * precedence. This means that for an atom in the molecule, only
     * the match with the latest parameter counts. So what we start from
     * the end and work forward. For each parameter we obtain a list
     * of the atom indices that match that parameter in the molecule.
     * Then we add those indices to a Hashmap of atoms that have 
     * already been matched. As we go over each parameter, if the atoms
     * returned are already in our Hashmap, we remove them from that
     * parameters match list. At the end the occurrence of each parameter
     * is obtained as the size of that parameter's match list. 
     * </p>
     * @param X Occurrence matrix (descriptors)
     * @param mol Molecule
     * @param row Position in the matrix
     * @param k Number of parameters
     * @return boolean
     ********************************************************************/
    public boolean calculateOccurrenceRow( Matrix X, Mol mol, int row, int k)
    {
        int j = 0;
        int count = 0;

        Iterator it;

        ATParam atParam;
        // Atoms in the Mol that we have already 
        // encountered. Vector of Integer
        Vector haveAtoms = new Vector();
        // Vector of Integer representing all atoms in
        // the Mol that a parameter matches.
        Vector paramAtoms;
        // must create a clone of paramAtoms
        // so we can modify it as we iterate 
        // through it
        Vector paramAtomsClone;

        it = paramList.getParamIterator();
        while( it.hasNext() )
        {
            DescriptorParam dp = 
                (DescriptorParam) it.next();
            try
            {
                double value = dp.calculateOccurrence(mol);
                dp.incrementFrequency((new Double(value)).intValue());
                X.set(row,dp.getId(),value);
            }
            catch( DescriptorException de )
            {
                logger.error(de.getMessage());
                return false;
            }
        }

        it = paramList.getGroupParamIterator();
        while( it.hasNext() )
        {
            GroupParam gp = 
                (GroupParam) it.next();
            int value = gp.match(mol);
            gp.incrementFrequency(value);
            X.set(row,gp.getId(),(double) value);

        }

        it = paramList.getReverseATParamIterator();
        while( it.hasNext() )
        {
            atParam = (ATParam) it.next();
            // get the list of all matching atom
            // indices
            paramAtoms = atParam.matchList(mol);
            if( paramAtoms == null )
            {
                return false;
            }

            // create a clone of paramAtoms so
            // we can modify it while iterating
            paramAtomsClone = (Vector) paramAtoms.clone();

            // remove all atoms from the paramAtoms
            // if we have already seen them, add
            // them to haveAtoms otherwise
            for( j=0; j < paramAtomsClone.size(); ++j )
            {
                Integer atom = (Integer) paramAtomsClone.elementAt(j);

                if( haveAtoms.contains(atom) )
                {
                    paramAtoms.remove(atom);
                }
                else
                {
                    haveAtoms.add(atom);
                }
            }

            count = paramAtoms.size();
            X.set(row,atParam.getId(), (double) count);
            // increment the occurrence of this Param
            atParam.incrementFrequency(count);
        }
        return true;
    }

    /**********************************************************
     * Returns a Vector of Integer containing the indices of
     * the atoms in the given Mol that do not match any of this
     * Algorithm's ATParams. Only returns indices of heavy
     * atoms.
     * @param mol Molecule
     * @return vector
     **********************************************************/
    public Vector getUnmatchedAtoms(Mol mol)
    {
        int j = 0;

        Iterator it;

        ATParam atParam;
        // Atoms in the Mol that have not yet
        // been matched to an ATParam
        Vector haveAtoms = new Vector();
        // Vector of Integer representing all atoms in
        // the Mol that a parameter matches.
        Vector paramAtoms;

        // first we populate haveAtoms will the indices
        // of all the atoms in the Mol
        it = mol.atomIterator();
        while( it.hasNext() )
        {
            JOEAtom atom = (JOEAtom) it.next();
            if( !atom.isHydrogen() )
            {
                haveAtoms.add(new Integer(atom.getIdx()));
            }
        }

        it = paramList.getReverseATParamIterator();
        while( it.hasNext() )
        {
            atParam = (ATParam) it.next();

            // get the list of all matching atom
            // indices
            paramAtoms = atParam.matchList(mol);

            if( paramAtoms == null )
            {
                continue;
            }

            // remove any atoms from the haveAtoms
            // if they match the ATParam
            for( j=0; j < paramAtoms.size(); ++j )
            {
                Integer atom = (Integer) paramAtoms.elementAt(j);

                if( haveAtoms.contains(atom) )
                {
                    haveAtoms.remove(atom);
                }
            }
        }

        return haveAtoms;
    }
}
