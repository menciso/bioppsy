package bioppsy.algorithm;


//java
import org.apache.log4j.Logger;

import bioppsy.Mol;
import bioppsy.MolSet;
import bioppsy.Param;
import bioppsy.Result;

import Jama.Matrix;

/************************************************************
 * <p>
 * The MCUVAlgorithm (Mean Centered Unit Variance) mean centers
 * and divides by the variance for both the X (independent predictor
 * variables) and Y (dependent property values) matrices.
 * </p>
 * <p>
 * Before performing regression, each value in both matrices is taken.
 * The mean and variance of the values in the same column of
 * that value is calculated. The value is mean-centered by
 * subtracting the mean from the value and scaled by dividing by
 * the sd.
 * </p>
 * <p>
 * When predicting a property for a molecule, the value of each parameter
 * for the molecule is added to the mean and multiplied by the sd 
 * for that parameter. The regression coefficients are then multiplied
 * by the values and added to get the scaled property. The predicted property
 * value is then obtained by adding the mean value of the property for the training set
 * and multiplying by the sd.
 * </p>
 ***********************************************************/
public class MCUV extends MeanCentered
{
    static Logger logger = Logger.getLogger(MCUV.class);

    public MCUV()
    {
        super();
    }

    /*******************************************************
     * Creates a new MCUVAlgorithm with the given MolSet and
     * name. The MolSet is used for training the MCUVAlgorithm's
     * parameters. An intercept Param is added as the first
     * parameter.
     *******************************************************/
    public MCUV(MolSet molSet, String name)
    {
        super(molSet,name);
    }

    /***********************************************************
     * <p>
     * Returns a Nx1 matrix of N experimentally calculated
     * property values. These values are obtained by iterating
     * through this Algorithm's MolSet. null is returned on
     * error. N is the number of molecules in the MolSet.
     * </p>
     * <p>
     * The mean-centered unit variance for each member in the MolSet 
     * is obtained by first subtracting the mean property (for the training 
     * set) from each value and then dividing by the standard deviation.
     * </p>
     **********************************************************/
    public Matrix getY(int N)
    {
    	int j=0;
        Matrix Y= getExpPropMatrix(N); // Method inherited from Algorithm
        double mean = sum(Y, N)/N;
        setMean(mean);
        double variance = varianceRow(Y, N, 0);
        setVariance(variance);

        // subtract the mean from all the property values
        // and divide by the sd
        for (j=0; j< N; j++)
        {
        	Y.set(j,0, (Y.get(j,0) - mean)/Math.pow(variance,0.5));
        }

        return Y;
    }

    /**************************************************************
     * <p>
     * Calculates the property of the given Mol and returns
     * it in a Result object.
     * null is returned on error.
     * </p>
     * <p>
     * To calculate the property of a molecule the 
     * calculateOccurrenceRow() function is first used to determine
     * the occurrence/value of each parameter for the given molecule.
     * Each of these occurrences is multiplied by the coefficient of
     * the corresponding parameter in this algorithm's paramList and
     * the results are summed to obtain the value.
     * </p>
     * <p>
     * Because the algorithm has been mean centered, the mean of
     * each parameter is subtracted from the occurrence for the 
     * molecule before multiplying it by the regression coefficient.
     * The occurrence is also divided by the standard deviation.
     * The value is multiplied by the standard deviation since it
     * has been scaled previously.
     * The result is then added to the mean value for the 
     * training set since this has also been mean centered.
     * </p>
     *************************************************************/
    public Matrix calculateProperty(Mol mol, Result result)
    {        
        Matrix X;
        int k;
        double prop=0.0;
        double mcuv=0.0;
        Param param;

        k = getNumberParameters();
        X = new Matrix(1,k);

        if( !calculateOccurrenceRow(X,mol,0,k) )
        {
            logger.error("Failed to calculate occurrence for Mol: " +
                mol.getNumber() );
            return null;
        }
        prop=0.0;
        for( int i=0; i < paramList.size(); ++i )
        {
            param = paramList.paramAt(i);
            mcuv = X.get(0,i);
            if( !paramList.paramAt(i).isIntercept() )
            {
            	mcuv = X.get(0,i) - param.getMean();
            	mcuv /=Math.pow(param.getVariance(),0.5);
            }
            prop += mcuv*param.getValue();
        }
        prop *= Math.pow(variance,0.5);
        prop += mean;

        result.setMol(mol);
        result.setValue(prop);

        return X;        
        
        
        
        
        
    }

    /**************************************************************
     * <p>
     * The occurrence matrix X is returned if successful, null otherwise.
     * <p>
     * The occurrence matrix X consists of N rows where N is the number of
     * molecules in the training data set. The j'th member of each row
     * indicates the frequency the j'th parameter occurred (or the calculated
     * value of the j'th parameter) for the molecule. The first column
     * is filled with 1's since the first parameter is the intercept.
     * There are k+1 columns where k is the number of parameters.
     * </p>
     * <p>
     * <h3>INPUT</h3>
     * int N - number of molecules in this Algorithm's MolSet<br>
     * int k - number of parameters<br>
     * Database db - the database to retrieve molecules from<br>
     * </p>
     * <p>
     * N and k are passed to this method just to avoid obtaining
     * them more than once.
     * </p>
     * <p>
     * The occurrence matrix is mean centered before returning. For parameter
     * i, the mean occurrence is calculated over all the molecules
     * in the training set. This mean value is then subtracted from
     * the occurrence of that parameter for each molecule in the occurrence
     * matrix. The occurrence is then divided by the variance.
     * </p>
     *************************************************************/
    public Matrix getX(int N, int k)
    {
        int i = 0;
        int j = 0;
        Matrix X=calculateOccurrence(N, k);
        double[] means,variances; // mean value of all parameters
        // make sure we don't have less than N Mol

        // calculate the means and sd
        means = new double[k+1];
        variances = new double[k+1];
        for( i=0; i < means.length; ++i )
        {
            means[i] = sumRow(X, N, i)/N;
            variances[i]=varianceRow(X, N, i);
            // save the mean so that it can be used
            // to calculate the property later
            Param param = paramList.paramAt(i);
            param.setMean(means[i]);
            param.setVariance(variances[i]);
        }

        // now subtract the means from the occurrences
        // and divide by the sd's
        for( i=0; i < means.length; ++i )
        {
            // if the parameter is the intercept we
            // do not want to mean centre it
            if( paramList.paramAt(i).isIntercept() )
            {
                continue;
            }

            for( j=0; j < N; ++j )
            {
                double mcuv = X.get(j,i) - means[i];
                mcuv = mcuv / Math.pow(variances[i],0.5);
                X.set(j,i,mcuv);
            }
        }

        return X;
    }
}

