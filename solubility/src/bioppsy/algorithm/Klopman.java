package bioppsy.algorithm;


import java.util.Iterator;
//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//Jama
import Jama.Matrix;

/************************************************************
 * The Klopman algorithm uses stereographic projection to 
 * keep extreme calculated property values within reasonable
 * boundaries. Note also that this method has been especially 
 * implemented for the calculation of solubility, so the 
 * experimental property Prop is referred as logS along 
 * this section.<br><br>
 *  
 * The Algorithm is performed on the value of logS* which
 * is a function of the experimental property logS:
 * <br><br>
 *
 * logS* = m + SIGN[logS-m]x[tan(alpha)x(logSmax-logSmin)/pi]<br><br>
 *
 * logSmax - upper limit of possible property values<br>
 * logSmin - lower limit of possible property values<br>
 * m = (logSmin+logSmax)/2<br>
 * SIGN - takes the sign of the term in the square bracket<br>
 * alpha = pi x Abs[logS-m] / (logSmax-logSmin)
 * 
 * <br><br>
 * 
 * Before the Algorithm contributions are calculated we convert the
 * experimental logS values to logS* by overriding the
 * Algorithm.getExpSolMatrix() method.<br>
 * Consequently the predicted logS values obtained through
 * calculateSolubility() are actually logS* values. These
 * have been converted back to logS values by overriding the
 * calculateSolubility() method and using the formula:<br><br>
 *
 * logS = m + SIGN[logS*-m] x [(alpha / pi) x (logSmax-logSmin)]<br>
 * alpha = arctan{ABS[logS*-m]/[(logSmax-logSmin)/pi]}  
 *
 * <br><br>
 *
 * This algorithm was taken from the publication:<br>
 * <a href="http://dx.doi.org/10.1021/ci000152d" target="_blank">
 * Klopman G, Zhu H, J Chem Inf Comput Sci 2001;41:439-445
 * </a><br>
 * <a href="http://pubs.acs.org/doi/pdf/10.1021/ci010048b" target="_blank">
 * ERRATA J Chem Inf Comput Sci 2001;41(4):1096-1097
 * </a>
 ***********************************************************/
public class Klopman extends MLR
{
    static Logger logger = Logger.getLogger(Klopman.class);

    /**
     * This is the Klopman Algorithm method
     */
    public Klopman()
    {
        super();
    }

    /*******************************************************
     * Creates a new KlopmanAlgorithm with the given MolSet and
     * name. The MolSet is used for training the KlopmanAlgorithm's
     * parameters. An intercept Param is added as the first
     * parameter.
     * @param molSet Molecule Set
     * @param name Name of the algorithm
     *******************************************************/
    public Klopman(MolSet molSet, String name)
    {
        super(molSet,name);
    }

    /**************************************************************
     * <p>
     * Calculates the property (logS) of the given Mol and returns
     * it in a Result object. The Result contains the given 
     * ResultCollection.
     * null is returned on error.
     * </p>
     * <p>
     * To calculate the property of a molecule the 
     * calculateOccurrenceRow() function is first used to determine
     * the occurrence/value of each parameter for the given molecule.
     * Each of these occurrences is multiplied by the coefficient of
     * the corresponding parameter in this algorithm's paramList and
     * the results are summed to obtain logS.
     * </p>
     * @param mol Molecule
     * @param rc Result collection
     * @return Result
     *************************************************************/
    public Result calculateSolubility(Mol mol, ResultCollection rc)
    {
        Matrix X;
        int k;
        double logSStar = 0.0;

        k = getNumberParameters();
        X = new Matrix(1,k);

        if( !calculateOccurrenceRow(X,mol,0,k) )
        {
            logger.error("Failed to calculate occurrence for Mol: " +
                mol.getNumber() );
            return null;
        }

        for( int i=0; i < paramList.size(); ++i )
        {
            logSStar += X.get(0,i)*paramList.paramAt(i).getValue();
        }

        return new Result(rc,mol,calculateLogS(logSStar));
    }

    /**************************************************************
     * <p>
     * Calculates the property (logS) of the given Mol and returns
     * it in a Result object.
     * null is returned on error.
     * </p>
     * <p>
     * To calculate the property of a molecule the 
     * calculateOccurrenceRow() function is first used to determine
     * the occurrence/value of each parameter for the given molecule.
     * Each of these occurrences is multiplied by the coefficient of
     * the corresponding parameter in this algorithm's paramList and
     * the results are summed to obtain logS.
     * </p>
     * @param mol Molecule
     * @param result Result
     * @return Matrix
     *************************************************************/
    public Matrix calculateProperty(Mol mol, Result result)
    {
        Matrix X;
        int k;
        double logSStar = 0.0;

        k = getNumberParameters();
        X = new Matrix(1,k);

        if( !calculateOccurrenceRow(X,mol,0,k) )
        {
            logger.error("Failed to calculate occurrence for Mol: " +
                mol.getNumber() );
            return null;
        }

        for( int i=0; i < paramList.size(); ++i )
        {
            logSStar += X.get(0,i)*paramList.paramAt(i).getValue();
        }

        result.setMol(mol);
        result.setValue(calculateLogS(logSStar));

        return X;
    }

    /***********************************************************
     * Returns a Nx1 matrix of N experimentally calculated
     * property values. These values are obtained by iterating
     * through this Algorithm's MolSet. null is returned on
     * error.
     * @param N number of experimental property values
     * @return Matrix
     **********************************************************/
    public Matrix getExpPropMatrix(int N)
    {
        int j = 0; // keep track of where we are
        Matrix Y = new Matrix(N,1);
        Mol mol;

        Iterator i = molSet.getIterator();
        if( i == null )
        {
            return null;
        }
        while( i.hasNext() )
        {
            // don't want to overrun Y
            if( j >= N )
            {
                logger.error("getExpSolMatrix() called with N larger " +
                    "than internal MolSet");
                return null;
            }

            mol = (Mol) i.next();
            // notice that the matrix index begins at 0
            try
            {
                Y.set(j,0,calculateLogSStar(mol.getExpProp()));
            }
            catch( NotFoundException nfe )
            {
                logger.error(nfe);
                return null;
            }
            ++j;         
        }

        return Y;
    }

    /*******************************************************************
     * Returns the logS* value which is calculated from logS:
     * <br><br>
     *
     * logS* = m + SIGN[logS-m]x[tan(alpha)x(logSmax-logSmin)/pi]<br><br>
     *
     * logSmax - upper limit of possible property values<br>
     * logSmin - lower limit of possible property values<br>
     * m = (logSmin+logSmax)/2<br>
     * SIGN - takes the sign of the term in the square bracket<br>
     * alpha = pi x Abs[logS-m] / (logSmax-logSmin)
     * @param logS solubility
     * @return float
     ********************************************************************/
    public static double calculateLogSStar(double logS)
    {
        double logSmin = Constants.instance().getDouble("LOGS_MIN");
        double logSmax = Constants.instance().getDouble("LOGS_MAX");

        double m = (logSmin+logSmax)/2;
        double x = logS - m;
        double sign;
        if( x < 0 )
        {
            sign = -1.0;
        }
        else
        {
            sign = 1.0;
        }

        double maxNmin = logSmax - logSmin;
        double alpha = ( Math.PI * Math.abs(x) ) / maxNmin;
 
        return  m + ( (sign * Math.tan(alpha) * maxNmin) / Math.PI );
    }

    /*******************************************************************
     * Returns the logS value which is calculated from logS*:
     * <br><br>
     *
     * logS = m + SIGN[logS*-m] x [(alpha / pi) x (logSmax-logSmin)]<br>
     * alpha = arctan{ABS[logS*-m]/[(logSmax-logSmin)/pi]}
     * @param logSStar logS*
     * @return float
     ********************************************************************/
    public static double calculateLogS(double logSStar)
    {
        double logSmin = Constants.instance().getDouble("LOGS_MIN");
        double logSmax = Constants.instance().getDouble("LOGS_MAX");

        double m = (logSmin+logSmax)/2;

        double x = logSStar - m;
        double sign;
        if( x < 0 )
        {
            sign = -1.0;
        }
        else
        {
            sign = 1.0;
        }

        double maxNmin = logSmax - logSmin;
        double alpha = Math.atan(Math.abs(x) / (maxNmin/Math.PI));
 
        return  m + ( sign * (alpha / Math.PI) * maxNmin);
    }
}
