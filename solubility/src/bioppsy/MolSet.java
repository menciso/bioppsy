package bioppsy;


//log4j
import org.apache.log4j.Logger;

import bioppsy.io.*;
import bioppsy.util.Stats;


//JOElib
import joelib.io.SimpleReader;
import joelib.io.IOType;
import joelib.io.IOTypeHolder;

//java
import java.io.InputStream;
import java.io.IOException;
import java.util.Vector;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.sql.SQLException;
import java.sql.ResultSet;

/*********************************************************
 * <p>
 * A MolSet is a collection of Mol objects. A MolSet can
 * be uniquely identified by the absolute filename of the
 * file with the molecule data is sourced from.
 * A MolSet has a database id which uniquely identifies 
 * the MolSet in the database.
 * A MolSet also has an optional expPropFile 
 * from which the solubility data is sourced from.
 * </p>
 * <p>
 * To source experimental property data from a file you
 * must call setExpPropFile(String filename). Then any
 * subsequent call to one of this MolSet's Mol object's 
 * getExpProp() methods will search that file for it's 
 * solubility data.
 * </p>
 *********************************************************/
public class MolSet
{
    private boolean modified = true;

    private PropFile file = null; 
    private ExpPropFile expPropFile = null;
    private int id = -1;

    // variance of experimental solubility values of this
    // molSet
    private double variance = 0.0;

    static Logger logger = Logger.getLogger(MolSet.class);

    /********************************************************
     * Creates a MolSet with the molecules sourced from the 
     * given filename, with the filename representing the
     * absolute resource location of the file.
     *******************************************************/
    public MolSet( String filename)
    {
        file = new PropFile(filename);
    }

    public PropFile getFile()
    {
        return file;
    }

    /********************************************************
     * May return null if no expPropFile has been set.
     *******************************************************/
    public ExpPropFile getExpPropFile()
    {
        return expPropFile;
    }

    public int getId()
    {
        return id;
    }

    /**********************************************************
     * Sets the file of this MolSet to source the molecule data
     *  from the given filename. Returns true
     * if the given file exists and is readable, false otherwise.
     **********************************************************/
    public boolean setFile(String filename)
    {
        file = new PropFile(filename);
        setModified(true);
        return true;
    }

    /**********************************************************
     * Sets the expPropFile of this MolSet to source the experimental
     * solubility data from the given filename. Returns true
     * if the given file exists and is readable, false otherwise.
     **********************************************************/
    public boolean setExpPropFile(String filename)
    {
        expPropFile = new ExpPropFile(filename);
        setModified(true);
        return true;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public double getVariance()
    {
        return variance;
    }

    public void setVariance(double variance)
    {
        this.variance = variance;
        setModified(true);
    }

    /******************************************************
     * Saves this MolSet in the Database. Returns true on
     * success and false on failure. If the MolSet already
     * exists in the database it is updated, otherwise
     * a new row is created. After save() is called this
     * MolSet will always have a database id.
     *****************************************************/
    public boolean save(Database db)
    {
        if( !modified() )
        {
            return true;
        }

        try
        {
            if( exists(db) )
            {
                doSave(db);  
            }
            else
            {
                create(db);
                // make sure create worked
                // and load the id
                if( !exists(db) )
                {
                    logger.error("MolSet does not exist after being created");
                    return false;
                }
            }
        }
        catch(SQLException sqle)
        {
            logger.error(sqle.getMessage());
            return false;
        }

        setModified(false);

        return true;
    }

    public void doSave(Database db)
        throws SQLException
    {
        String save = 
            "UPDATE MolSet SET";

        save += " variance=" + variance;

        if( expPropFile != null )
        {
           save += ", expSolFilename='" + expPropFile.getAbsolutePath() + "'";
        }

        save += " WHERE id=" + getId() + ";";

        //logger.debug(save);

        db.execute(save);
    }

    public void create(Database db) 
        throws SQLException
    {
        String create = 
            "INSERT INTO MolSet(filename";

        if( expPropFile != null )
        {
           create += ",expPropFilename";
        }

        create += ",variance) VALUES(" +
            "'" + file.getAbsolutePath() + "'";

        if( expPropFile != null )
        {
            create += ",'" + expPropFile.getAbsolutePath() + "'";
        }

        create += "," + variance;

        create += ");";
 
        //logger.debug(create);

        db.execute(create);
    }

    /******************************************************
     * Returns a Vector of Strings which are the filenames
     * of all the MolSets in the database. Returns an
     * empty Vector if there are none. Returns null on
     * error.
     *******************************************************/
    public static Vector getFilenames(Database db)
    {
        Vector names = new Vector();

        String find =
            "SELECT filename FROM MolSet;";

        try
        {
            ResultSet rs = db.executeQuery(find);
            while(rs.next())
            {
                String name = rs.getString("filename");
                names.add(name);
            }
        }
        catch( SQLException sqle)
        {
            logger.error(sqle.getMessage());
            return null;
        }

        return names;
    }

    /******************************************************
     * Returns a new MolSet loaded from the database with
     * the given filename. If the MolSet does not exist
     * or if there is an error null is returned.
     *****************************************************/
    public static MolSet get(String searchFile, Database db)
    {
        MolSet ret = new MolSet(searchFile);
        boolean have = false;
        int retId;
        String retExpProp;

        String load =
            "SELECT * FROM MolSet WHERE" +
            " filename='" + ret.getFile().getAbsolutePath() + "'" +
            ";";

        try
        {
            ResultSet rs = db.executeQuery(load);
            while( rs.next() )
            {
                retId = rs.getInt("id");
                ret.setId(retId);
                ret.setVariance(rs.getDouble("variance"));
                retExpProp = rs.getString("expSolFilename");
                if( !rs.wasNull() )
                {
                   ret.setExpPropFile(retExpProp);
                }
                have = true;
            } 
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return null;
        }

        if( have )
        {
            ret.setModified(false);
            return ret;
        }
        else
        {
            return null;
        }
    }


    /******************************************************
     * Returns a new MolSet loaded from the database with
     * the given database id. If the MolSet does not exist
     * or if there is an error null is returned.
     *****************************************************/
    public static MolSet get(int searchId, Database db)
    {
        MolSet ret = null;
        boolean have = false;
        String retFilename;
        String retExpProp;
        double retVar;
        int retId;

        String load =
            "SELECT * FROM MolSet WHERE" +
            " id=" + searchId +
            ";";

        try
        {
            ResultSet rs = db.executeQuery(load);
            while( rs.next() )
            {
                retFilename = rs.getString("filename");
                ret = new MolSet(retFilename);
                retId = rs.getInt("id");
                ret.setId(retId);
                retVar = rs.getDouble("variance");
                ret.setVariance(retVar);
                retExpProp = rs.getString("expSolFilename"); // Note that Sol is used instead of Prop in this particular case
                if( !rs.wasNull() )
                {
                   ret.setExpPropFile(retExpProp);
                }
                have = true;
            } 
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return null;
        }

        if( have )
        {
            ret.setModified(false);
            return ret;
        }
        else
        {
            return null;
        }
    }

    /******************************************************
     * Deletes this MolSet from the database. Returns true
     * on success and false on error. Returns true even
     * if the MolSet does not exist to be deleted.
     * All the results associated with this MolSet are
     * also deleted. The index for this MolSet is also
     * deleted if it exists.
     *****************************************************/
    public boolean delete(Database db)
    {
        String algNames = usedByAlgorithm(db);

        if( algNames != null )
        {
            logger.error("Cannot delete MolSet since it is used " +
                "by the algorithm(s): " + algNames);
            return false;
        }

        try
        {
            if( !exists(db) )
            {
                return true;
            }

            String delete =
                "DELETE FROM MolSet WHERE" +
                " filename='" + file.getAbsolutePath() + "'" +
                ";";

            String deleteResults =
                "DELETE FROM Result WHERE" +
                " molSet=" + getId() +
                ";";

            db.execute(delete);
            db.execute(deleteResults);
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }

        setModified(true);

        return true;
    }

    /****************************************************
     * Returns true if this MolSet already exists in the
     * database, false otherwise. Throws an SQLException
     * if there is an error. If the MolSet does exists
     * its id is loaded from the database.
     ***************************************************/
    public boolean exists(Database db)
        throws SQLException
    {
        String exists = 
            "SELECT id FROM MolSet WHERE" +
            " filename='" + file.getAbsolutePath() + "'" +
            ";";

        //logger.debug(exists);

        ResultSet rs = db.executeQuery(exists);
        while( rs.next() )
        {
            id = rs.getInt("id");
            return true;
        }
        return false;
    }

    public boolean modified()
    {
        return modified;
    }

    public void setModified(boolean modified)
    {
        this.modified = modified;
    }

    /*****************************************************
     * Returns true if this object is equal to the given
     * object, false otherwise. Two MolSet objects are
     * considered equal if their names are the same.
     ****************************************************/
    public boolean equals(Object other)
    {
        MolSet otherMolSet;

        if( getClass().getName().equals(other.getClass().getName()) )
        {
            otherMolSet = (MolSet) other;
        }
        else
        {
            return false;
        }

        if( getFile().getAbsolutePath().equals(
            otherMolSet.getFile().getAbsolutePath()) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**************************************************************
     * Returns a Iterator. This allows you to iterate
     * through all the Mol objects in this MolSet using the
     * Iterator.hasNext() and Iterator.next()
     * methods. null is returned if there has been an error.
     ************************************************************/
    public Iterator getIterator()
    {
        MolSetIterator msi = null;
        try
        {
            msi = new MolSetIterator(this);
        }
        catch(FileNotFoundException fnfe)
        {
            logger.error("FileNotFoundException: " + fnfe.getMessage());
            return null;
        }
        catch(IOException ioe)
        {
            logger.error("IOException: " + ioe.getMessage());
            return null;
        }
        return msi;
    }

    private class MolSetIterator implements Iterator
    {
        MolSet parent;
        InputStream input = null;
        SimpleReader reader = null;
        IOType inType,outType;
        Mol mol = null;
        Mol nextMol = null;
        int molNumber = 1;

        public MolSetIterator(MolSet parent)
            throws FileNotFoundException, IOException
        {
            this.parent = parent;

            input = file.getInputStream();

            // estimate input file type
            inType = IOTypeHolder.instance().filenameToType(
                file.getName());
            
            if (inType == null)
            {
                // unknown file type
                String error = "Unknown file type: " + file.getName();
                logger.error(error);
                throw new IOException(error);
            }

            // open simple reader
            reader = new SimpleReader(input, inType);

            // read the first Mol
            readNext();
        }

        public boolean hasNext()
        {
            if( mol == null )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Object next()
        {
            nextMol = mol;
            readNext();
            return nextMol;
        }
        public void remove() {}

        private void readNext()
        {
            try
            {
                // read in the next Mol
                mol = new Mol(inType,outType,parent,molNumber++);
                if( !reader.readNext(mol) )
                {
                    // set the Mol to null so we know that we
                    // don't have a next one
                    mol = null;
                }
            }
            catch(IOException ioe)
            {
            	logger.error("Error6");
                logger.error(ioe.getMessage());
                ioe.printStackTrace();
                mol = null;
            }
            catch(joelib.io.MoleculeIOException mioe)
            {
                logger.error(mioe);
                mol = null;
            }
        }
    }

    /****************************************************
     * Returns the number of molecules in this MolSet
     ****************************************************/
    public int size()
    {
        int size = 0;
        Iterator i = getIterator();
        while( i.hasNext() )
        {
            i.next();
            ++size;
        }

        return size;   
    }

    /*****************************************************
     * Returns true if all the Mol in this MolSet have
     * valid experimental solubility values, false otherwise
     *****************************************************/
    public boolean haveExpProp()
    {
        Mol mol;
        Iterator i = getIterator();
        if( i == null )
        {
            logger.error("Unable to obtain iterator");
            return false;
        }

        while( i.hasNext() )
        {
            mol = (Mol) i.next();
            try
            {
                mol.getExpProp();
            }
            catch( NotFoundException nfe )
            {
                return false;
            }
        }

        return true;
    }

    /**************************************************
     * Returns the Mol with the specified number from this
     * MolSet file. The file will be parsed sequentially 
     * until the Mol is found.
     ***************************************************/
    public Mol getMol(int number, Database db)
    {
        try
        {
            return getMolByNumber(number);
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return null;
        }
        catch( joelib.io.MoleculeIOException mioe )
        {
            logger.error(mioe.getMessage());
            return null;
        }
    }

    /***************************************************
     * Returns the molecule in this MolSet with the 
     * specified number. Molecules are number sequentially
     * from the beginning of the file.
     * This function parses the file sequentially until
     * the Mol with the given number is found.
     * Returns null on error, throws an IOException on
     * file error, and a MoleculeIOException on molecule
     * io error.
     ***************************************************/
    private Mol getMolByNumber(int molecule)
        throws IOException, joelib.io.MoleculeIOException
    {
        InputStream input = null;
        SimpleReader reader = null;
        IOType inType,outType;
        Mol mol = null;
        int current = 0;

        input = file.getInputStream();

        // estimate input file type
        inType = IOTypeHolder.instance().filenameToType(
            file.getName());
        // the output type is always the same
        outType = IOTypeHolder.instance().getIOType(
            Constants.instance().getString("SDF"));
        if (inType == null)
        {
            logger.error("Unknown file type: " + file.getName());
            return null;
        }

        // open simple reader
        reader = new SimpleReader(input, inType);

        mol = new Mol(inType,outType,this,molecule);

        // keep going until we get to the correct number
        while( reader.readNext(mol) )
        {
            ++current;
            if( current == molecule )
            {
                return mol;
            }
        }

        return null;
    }

    /************************************************************
     * Returns a MolSubSetIterator which iterates through the
     * MolSet in sequential order returning subsets of the MolSet.
     * Each subset returned is a Vector of Mol objects.
     ************************************************************/
    public Iterator getSubSetIterator()
    {
        return new MolSubSetIterator(this);
    }

    /*************************************************************
     * A MolSubSetIterator navigates through the MolSet passed
     * to its constructor in sequential order. Each time next()
     * is called a Vector of Mol objects is returned that contains
     * no more than MOL_SUBSET Mol.
     *************************************************************/
    public class MolSubSetIterator implements Iterator
    {
        private Iterator i;
        private Vector next;

        public MolSubSetIterator(MolSet molSet)
        {
            i = molSet.getIterator();
            next = getNext();
        }

        public Object next()
        {
            Vector ret = next;
            next = getNext();
            return ret;
        }

        public boolean hasNext()
        {
            if( next.size() > 0 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Vector getNext()
        {
            Vector next = new Vector();
            int maxSize = Constants.instance().getInt("MOL_SUBSET");

            for(int index=0;index < maxSize && i.hasNext();++index)
            {
                next.add( (Mol) i.next() );
            }

            return next;        
        }

        public void remove()
        {
            // does nothing
        }
    }

    /****************************************************************
     * Returns the standard deviation of the experimental properties
     * of the molecules in this MolSet.
     *****************************************************************/
    public double expPropSdDev()
    {
        Vector values = new Vector();
        double expProp;
        Mol mol;
        int numErrors = 0;
        int NUM_EXCEPTIONS = Constants.instance().getInt("MAX_EXCEPTIONS");

        Iterator i = getIterator();
        while( i.hasNext() )
        {
            try
            {
                mol = (Mol) i.next();    
                expProp = mol.getExpProp();
                values.add(new Double(expProp));
            }
            catch( NotFoundException nfe)
            {
                logger.error(nfe.getMessage());
                ++numErrors;
                if( numErrors > NUM_EXCEPTIONS )
                {
                    logger.error("Exceeded " + NUM_EXCEPTIONS + 
                        " molecules without an expSol in expSolSdDev()");
                    return Double.NaN; 
                }
            }
        }

        return Stats.standardDeviation(values);
    }

    /***************************************************************
     * Returns the Algorithm name(s) if this molSet is being used by
     * any Algorithm. Returns null otherwise or on error.
     ***************************************************************/
    public String usedByAlgorithm(Database db)
    {
        String algNames = null;
        String algName = null;

        try
        {
            if( !exists(db) )
            {
                logger.error("MolSet does not exist: " + 
                    file.getAbsolutePath());
                return null;
            }

            String getAlgorithm = 
                "SELECT name FROM Algorithm WHERE" +
                " molSet=" + getId() + ";";

            ResultSet rs = db.executeQuery(getAlgorithm);
            while( rs.next() )
            {
                algName = rs.getString("name");
                if( algNames == null )
                {
                    algNames = algName;
                }
                else
                {
                    algNames += ", " + algName;
                }
            }
            return algNames;
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return algNames;
        }
    }
}

