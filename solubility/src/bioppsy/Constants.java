package bioppsy;


// java
import java.io.FileNotFoundException;

//log4j
import org.apache.log4j.Logger;

import bioppsy.util.IniFileReader;



/************************************************************
 * This class contains constant values that are used throughout
 * the package. Use the instance() method to get an Constants
 * object. Then use the get methods with the name of the
 * value that you require.
 *************************************************************/
public class Constants
{
    private static Constants theObject = null;
    private IniFileReader ifr;

    static Logger logger = Logger.getLogger(Constants.class);

    private Constants() 
    {
        String filename = "ini/constants.ini";

        try
        {
            ifr = new IniFileReader(filename);
            if( !ifr.read() )
            {
                logger.error("Unable to read Constants ini file: " +
                    filename);
                System.exit(0);
            }
        }
        catch(FileNotFoundException fnfe)
        {
            logger.error(fnfe.getMessage());
            System.exit(0);
        }
    }

    public static Constants instance()
    {
        if( theObject == null )
        {
            theObject = new Constants();
        }
        return theObject;
    }

    public String getString(String key)
    {
        return ifr.getString(key);
    }

    public String[] getStringArray(String key)
    {
        return ifr.getStringArray(key);
    }

    public int getInt(String key)
    {
        return ifr.getInt(key);
    }

    public double getDouble(String key)
    {
        return ifr.getDouble(key);
    }
}
