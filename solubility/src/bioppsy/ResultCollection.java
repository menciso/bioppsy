package bioppsy;

//log4j
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Vector;

import org.apache.log4j.Logger;

import bioppsy.algorithm.MLR;





/****************************************************************
 * A ResultCollection is a wrapper for a Vector of Result objects.
 * You should add Results via the add(Result) method. 
 * A ResultCollection is
 * associated with an Algorithm and MolSet. Each Result in a
 * ResultCollection is associated with the same Algorithm as
 * the ResultCollection. Each Mol in each Result also has the
 * same MolSet as the ResultCollection they are in. 
 * A ResultCollection object is uniquely identified by its
 * (Algorithm,MolSet) pair.
 ****************************************************************/
public class ResultCollection extends Vector
{
    private MLR alg;
    private MolSet molSet;

    static Logger logger = Logger.getLogger(ResultCollection.class);

    public ResultCollection(MLR alg, MolSet molSet)
    {
        this.alg = alg;
        this.molSet = molSet;
    }

    public MLR getAlgorithm()
    {
        return alg;
    }

    public MolSet getMolSet()
    {
        return molSet;
    }

    public void setAlgorithm(MLR alg)
    {
        this.alg = alg;
    }

    public void setMolSet(MolSet molSet)
    {
        this.molSet = molSet;
    }

    /**************************************************
     * Checks to make sure the given Result has the same
     * Algorithm as this ResultCollection. Makes sure
     * that the given Result's MolSet is the same as
     * this ResultCollection's MolSet. Then the Result
     * is added to the collection. 
     ***************************************************/
    public synchronized boolean add(Result result)
    {
        super.add(result);
        return true;
    }

    /*************************************************
     * Returns an iterator over the Results in this
     * ResultCollection in a thread safe manner.
     ************************************************/
    public synchronized Iterator getIterator()
    {            
        // take a copy of the results because they
        // may be modified by another thread
        // this may be taken care of by the 
        // List.iterator() but not sure yet.
        Vector copy = (Vector) clone();
        return copy.iterator();
    }

    /****************************************************
     * Saves the current Results in this ResultCollection
     * to the Database. Once they have been saved successfully
     * this ResultCollection is emptied. Returns true on
     * success, false on failure.
     * The results are only successfully saved if they do
     * not already exist in the database, existing results
     * are not updated.
     ********************************************************/
    public synchronized boolean save(Database db)
    {
        Result result;

        // first make sure the Algorithm is saved
        if( !alg.save(db) )
        {
            return false;
        }
        // also make sure the MolSet is saved
        if( !molSet.save(db) )
        {
            return false;
        }

        int size = size();
        if( size <= 0 )
        {
            return true;
        }
        // just iterate through all the Results and save
        // each one.
        Iterator i = iterator();
        while( i.hasNext() )
        {      
            result = (Result) i.next();
            if( !result.save(db) )
            {
                logger.error("Failed to save result");
                return false;
            }
        }

        removeAllElements();

        return true;
    }

    /**********************************************************
     * Deletes all the Results in this ResultCollection from
     * the Database. Returns true on success, false on failure.
     **********************************************************/
    public boolean delete(Database db)
    {
        String delete =
            "DELETE FROM Result WHERE " +
            " algorithm='" + getAlgorithm().getName() + "'" +
            " AND molSet=" + 
                getMolSet().getId() +
            ";";

        try
        {
            db.execute(delete);
        }
        catch(SQLException sqle)
        {
            logger.error(sqle.getMessage());
            return false;
        }

        return true;
    }

    /*******************************************************
     * Reverses the order of this ResultCollection
     ******************************************************/
    public void reverse()
    {
        Vector temp = (Vector) clone();
        removeAllElements();
        for( int i = (temp.size()-1); i >= 0; --i )
        {
            add(temp.elementAt(i));
        }
    }
}
