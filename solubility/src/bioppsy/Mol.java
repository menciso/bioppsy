package bioppsy;

//log4j
import org.apache.log4j.Logger;
//JOElib
import joelib.molecule.JOEMol;
import joelib.io.IOType;
import joelib.io.IOTypeHolder;
import joelib.util.iterator.GenericDataIterator;
import joelib.data.JOEGenericData;
import joelib.data.JOEPairData;
import joelib.data.JOEDataType;
import joelib.io.SimpleWriter;
import joelib.smiles.JOESmilesParser;
import joelib.util.iterator.AtomIterator;
import joelib.molecule.JOEAtom;

import java.io.IOException;
import java.util.Vector;

/****************************************************************
 * <p>
 * The Mol class represents a molecule and is a subclass of the JOEMol class. A
 * Mol has a number, a MolSet, an experimental property expProp, and a molecule
 * identifier molId (we strongly recommend using a unique identifier such as the
 * CID or CAS numbers). A Mol can be uniquely identified by the combination
 * (number, MolSet) or its molId number.
 * </p>
 **************************************************************/
public class Mol extends JOEMol {

	private MolSet molSet = null;
	private int number;
	private double expProp = 0.0;
	private boolean haveExpProp = false;
	private String molId = null;
	private String rncg = null;
	private String comm = null;
	private String clogp = null;
	private String psa = null;
	private String dgs = null;
	private String dgsub = null;
	private String desc1 = null;
	private String desc2 = null;
	private String desc3 = null;

	static Logger logger = Logger.getLogger(Mol.class);

	/*****************************************************
	 * Constructs a Mol ready to be read in from a file using the JOELib
	 * library.
	 ****************************************************/
	public Mol(IOType inType, IOType outType, MolSet molSet, int number) {
		super(inType, outType);
		this.molSet = molSet;
		this.number = number;
	}

	/****************************************************
	 * Constructs a Mol with the given molSet and number. A Mol created with
	 * this constructor cannot be subsequently read in from a file.
	 ***************************************************/
	public Mol(MolSet molSet, int number) {
		this.molSet = molSet;
		this.number = number;
	}

	/***********************************************************
	 * This constructor initialises a Mol from a smiles string.
	 *********************************************************/
	public Mol(String smiles) {
		if (!JOESmilesParser.smiToMol(this, smiles, "new molecule")) {
			logger.error("SMILES entry " + smiles + " could not be loaded.");
		}
	}

	/***********************************************************
	 * Constructs a new Mol object as a copy of the subclass JOEMol
	 ***********************************************************/
	public Mol(JOEMol joeMol) {
		super(joeMol, true);
	}

	public MolSet getMolSet() {
		return molSet;
	}

	public int getNumber() {
		return number;
	}

	public double getExpProp() throws NotFoundException {
		NotFoundException nfe = new NotFoundException(
				"No Experimental Property is available for the Mol: "
						+ getNumber());

		if (haveExpProp == false) {
			// load exp properties from descriptors
			// if that doesn't work try
			// and get it from the molSet's
			// expPropFile, if that doesn't
			// work or if the expSolFile is
			// null throw an Exception.
			String s = getDescriptor(Constants.instance().getString("PROP_KEY"));
			if (s == null) {
				if (molSet.getExpPropFile() == null) {
					logger.debug("ExpPropFile null for MolSet: "
							+ molSet.getId());
					throw nfe;
				} else {
					String retMolId = getMolId();
					if (retMolId == null) {
						logger.debug("No Molecule Identifier for Mol: "
								+ getNumber());
						throw nfe;
					}
					expProp = molSet.getExpPropFile().getExpProp(retMolId);

				}
			} else {
				expProp = Double.parseDouble(s);
			}
		}
		haveExpProp = true;
		return expProp;
	}

	/******************************************************************
	 * Returns comments form the molecule. In the datasets provided with BioPPSy
	 * source code, these are the type of properties included in the dataset and
	 * their origin. We have used special characters to identify fields (##) and
	 * descriptions (%). Example: Property: %Caco2 permeation ##Reference:
	 * %Tingjun Hou, Wei Zhang, Ke Xia, Xiaojie Xu, ADME evaluation in drug
	 * discovery. 5. Correlation of Caco-2 permeation with simple molecular
	 * properties, Journal of Chemical Information and Computer Sciences, 2004,
	 * 44, 1585-1600
	 ******************************************************************/
	public String getComment() {
		if (comm == null) {
			// load comments from descriptors
			comm = getDescriptor(Constants.instance().getString("COMM_KEY"));
			if (comm == null) {
				comm = "No comments found for Mol";
			}
			comm = comm.replaceAll("%", "&emsp;");
			comm = comm.replaceAll("##", "<p>");
		}
		return comm;
	}

	/******************************************************************
	 * Returns the Mol's molecule identifier. Returns null if this Mol does not
	 * have a molecule identifier.
	 ******************************************************************/
	public String getMolId() {
		if (molId == null) {
			// load molId from descriptors
			molId = getDescriptor(Constants.instance().getString("MOLID_KEY"));
			if (molId == null) {
				// try the second key
				molId = getDescriptor(Constants.instance().getString(
						"MOLID_KEY2"));
				if (molId == null) {
					// if not supplied in molecule file, send the title to
					// JOELib
					molId = getTitle();
				}
			}
		}
		return molId;
	}

	/******************************************************************
	 * Returns the Mol's RNCG supplied by user. Returns null if this Mol does
	 * not have a RNCG attached.
	 ******************************************************************/
	public String getU_RNCG() {
		if (rncg == null) {
			// load rncg from descriptors
			rncg = getDescriptor(Constants.instance().getString("RNCG_KEY"));
		}
		return rncg;
	}

	/******************************************************************
	 * Returns a value for log of the Mol's partition coefficient. Returns null
	 * if this Mol does not have a ClogP attached.
	 ******************************************************************/
	public String getCLOGP() {
		if (clogp == null) {
			// load ClogP from descriptors
			clogp = getDescriptor(Constants.instance().getString("CLOGP_KEY"));
		}
		return clogp;
	}

	/******************************************************************
	 * Returns a value for polar surface area. Returns null if this Mol does not
	 * have a PSA attached.
	 ******************************************************************/
	public String getPSA() {
		if (psa == null) {
			// load ClogP from descriptors
			psa = getDescriptor(Constants.instance().getString("PSA_KEY"));
		}
		return psa;
	}

	/******************************************************************
	 * Returns the Mol's solvation energy (supplied in input file). Returns null
	 * if this Mol does not have a deltaG_solv attached.
	 ******************************************************************/
	public String getdeltaGsolv() {
		if (dgs == null) {
			// load deltaG from descriptors
			dgs = getDescriptor(Constants.instance().getString(
					"DELTAG_SOLV_KEY"));
		}
		return dgs;
	}

	/******************************************************************
	 * Returns the Mol's solvation energy (supplied in input file). Returns null
	 * if this Mol does not have a deltaG_solv attached.
	 ******************************************************************/
	public String getdeltaGsub() {
		if (dgsub == null) {
			// load deltaG from descriptors
			dgsub = getDescriptor(Constants.instance().getString(
					"DELTAG_SUB_KEY"));
		}
		return dgsub;
	}

	/******************************************************************
	 * Returns a value for a user defined descriptor (1)
	 ******************************************************************/
	public String getdescriptor1() {
		
		if (desc1 == null) {
			//System.out.print(" MolDesc1-string " + Constants.instance().getString("DESC1_KEY") + "\n");
			desc1 = getDescriptor(Constants.instance().getString("DESC1_KEY"));
		}
		return desc1;
	}

	/******************************************************************
	 * Returns a value for a user defined descriptor (2)
	 ******************************************************************/
	public String getdescriptor2() {
		//System.out.print(" MolDesc2 " + desc2 + "\n");
		if (desc2 == null) {
			//System.out.print(" MolDesc2-string " + Constants.instance().getString("DESC2_KEY") + "\n");
			desc2 = getDescriptor(Constants.instance().getString("DESC2_KEY"));
		}
		return desc2;
	}

	/******************************************************************
	 * Returns a value for a user defined descriptor (3)
	 ******************************************************************/
	public String getdescriptor3() {
		if (desc3 == null) {
			desc3 = getDescriptor(Constants.instance().getString("DESC3_KEY"));
		}
		return desc3;
	}

	/****************************************************************************
	 * Returns the descriptor stored about this Mol for the given descriptor
	 * name. For example the name "MOLID" returns the molecule identifier
	 * descriptor that has been loaded from the file. null is returned if a
	 * descriptor with the given name was not found.
	 ****************************************************************************/
	public String getDescriptor(String name) {
		GenericDataIterator gdit = genericDataIterator();
		JOEGenericData genericData;
		JOEPairData pairData;
		String key, value;
		while (gdit.hasNext()) {
			genericData = gdit.nextGenericData();
			if (genericData.getDataType() == JOEDataType.JOE_PAIR_DATA) {
				key = genericData.getAttribute();
				pairData = (JOEPairData) genericData;
				value = pairData.toString(IOTypeHolder.instance().getIOType(
						getInputType().getName()));
				if (key.trim().compareToIgnoreCase(name) == 0) {
					return value;
				}
			}
		}
		return null;
	}

	public void setMolSet(MolSet molSet) {
		this.molSet = molSet;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setExpProp(double expProp) {
		this.expProp = expProp;
		haveExpProp = true;
	}

	public void setMolId(String molId) {
		this.molId = molId;
	}

	/*****************************************************
	 * Returns true if this object is equal to the given object, false
	 * otherwise. Two Mol objects are considered equal if their numbers and
	 * their MolSets' are equal.
	 ****************************************************/
	public boolean equals(Object other) {
		Mol otherMol;

		if (getClass().getName().equals(other.getClass().getName())) {
			otherMol = (Mol) other;
		} else {
			return false;
		}

		if ((getNumber() == otherMol.getNumber())
				&& getMolSet().equals(otherMol.getMolSet())) {
			return true;
		} else {
			return false;
		}
	}

	/**************************************************************************
	 * Returns the number of hydrophobic carbon atoms present in this molecule.
	 * A hydrophobic carbon has been chosen as being a sp3 or sp2 hybridised
	 * carbon atom that does not have a heteroatom (any atom other than carbon
	 * or hydrogen) attached to itself, attached to a neighbouring carbon atom,
	 * or attached to a neighbouring carbon's neighbour. sp3 carbons that are
	 * aromatic or in a ring are excluded.
	 *************************************************************************/
	public int numHydrophobic() {
		// atoms that could be hydrophobic if they
		// are not connected 1-2,1-3,1-4 to a
		// heteroatom
		Vector targets = getCarbons();

		Vector heteros = getHeteroatoms();

		return getTargetsNotAttached14(targets, heteros);
	}

	/********************************************************
	 * Returns a Vector of all heteroatoms in this Mol, a Vector of JOEAtom
	 * objects.
	 ********************************************************/
	public Vector getHeteroatoms() {
		int atomicNum;
		AtomIterator ait;
		JOEAtom atom;
		// all heteroatoms
		Vector heteros = new Vector();

		// Search for heteroatoms
		ait = atomIterator();
		while (ait.hasNext()) {
			atom = ait.nextAtom();
			atomicNum = atom.getAtomicNum();
			if ((atomicNum != 6) && (atomicNum != 1)) {
				// add to list of heteroatoms
				heteros.add(atom);
			}
		}

		return heteros;
	}

	/***********************************************************
	 * Returns a list of all sp3 and sp2 Carbons in this Mol. Sp2 carbons that
	 * are in a ring or are aromatic are not returned. A Vector of JOEAtom
	 * objects is returned.
	 ***********************************************************/
	public Vector getCarbons() {
		AtomIterator ait;
		JOEAtom atom;
		int hyb;
		int atomicNum;
		// atoms that could be hydrophobic if they
		// are not connected 1-2,1-3,1-4 to a
		// heteroatom
		Vector targets = new Vector();

		// Search for targets
		ait = atomIterator();
		while (ait.hasNext()) {
			atom = ait.nextAtom();

			atomicNum = atom.getAtomicNum();
			if (atomicNum == 6) {
				// Carbon
				hyb = atom.getHyb();
				if (hyb == 3) {
					// sp3 Carbon
					targets.add(atom);
				} else if (hyb == 2) {
					// sp2 Carbon
					if ((!atom.isAromatic()) && (!atom.isInRing())) {
						// not Aromatic or in Ring
						targets.add(atom);
					}
				}
			}
		}

		return targets;
	}

	/******************************************************
	 * Returns the number of atoms in the given targets Vector that are not
	 * connected in the 1-2, 1-4, or 1-3 relationship to any of the atoms in the
	 * given hetero list.
	 ******************************************************/
	public int getTargetsNotAttached14(Vector targets, Vector heteros) {
		int i, j;
		JOEAtom target, hetero;
		int count = 0;
		boolean good;

		for (i = 0; i < targets.size(); ++i) {
			target = (JOEAtom) targets.elementAt(i);
			good = true;
			for (j = 0; j < heteros.size(); ++j) {
				hetero = (JOEAtom) heteros.elementAt(j);
				if (target.isConnected(hetero) || target.isOneThree(hetero)
						|| target.isOneFour(hetero)) {
					good = false;
					break;
				}
			}
			if (good) {
				++count;
			}
		}

		return count;
	}

	/*********************************************************
	 * Uses JOELib to write this Mol to the given filename. The output format is
	 * determined by the extension of the given filename. Returns true on
	 * success, false on failure.
	 *********************************************************/
	public boolean writeToFile(String filename) {
		try {
			SimpleWriter writer = new SimpleWriter(filename);
			return writer.writeNext(this);
		} catch (IOException ioe) {
			logger.error(ioe.getMessage());
			return false;
		} catch (joelib.io.MoleculeIOException mioe) {
			logger.error(mioe.getMessage());
			return false;
		}
	}
}
