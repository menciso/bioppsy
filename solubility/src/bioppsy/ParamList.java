package bioppsy;

//java
import java.util.Vector;
import java.util.Iterator;
//log4j
import org.apache.log4j.Logger;

import bioppsy.algorithm.MLR;


//sql
import java.sql.SQLException;
import java.sql.ResultSet;

/*******************************************************
 * <p>
 * A ParamList is a wrapper for a Vector of Param objects.
 * The index of the Param objects in a ParamList directly
 * corresponds to their id. When a Param object is added
 * to the end of a ParamList, its id is set to its index
 * in the ParamList. Param indices begin at 0.
 * </p>
 * <p>
 * By convention the intercept parameter is always 
 * stored at index 0 and is a standard Param object.
 * </p>
 ****************************************************/
public class ParamList extends Vector
{
	private boolean modified = true;

    MLR alg = null;

    static Logger logger = Logger.getLogger(ParamList.class);

    public ParamList(MLR alg)
    {
        super();
        this.alg = alg;
    }

    /*******************************************************
     * Adds the given Param to this ParamList. The id of
     * the given Param is set to the next id in this ParamList,
     * hence Param objects should be added in order.
     *******************************************************/
    public boolean add( Param param )
    {
        // set the Param id to the next index
        param.setId(size());

        setModified();
        alg.setModified();

        super.add(param);

        return true;
    }

    // DatabaseInterface implementation

    /*******************************************************
     * Saves this object in the database. Returns true on
     * success, false on failure. Creates the object in the
     * database if it does not exist.
     ******************************************************/
    public boolean save(Database db)
    {

        if( !modified() )
        {
            return true;
        }

        // first we have to delete all the current 
        // parameters

        deleteAll(db);

        for( int i=0; i < size(); ++ i )
        {
            if( !paramAt(i).save(db) )
            {
                return false;
            }
        }

        setNotModified();

        return true;
    }

    /***************************************************
     * Deletes all the Param associated with this
     * ParamList.
     **************************************************/
    public boolean deleteAll(Database db)
    {
        String delete =
            "DELETE FROM Parameter WHERE" +
            " algorithm='" + alg.getName() + "';";

        try
        {
            db.execute(delete);
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }

        return true;
    }

    /*****************************************************
     * Returns a ParamList loaded from the database with
     * the given Algorithm id. Returns null on error.
     ******************************************************/
    public static ParamList getParamList(MLR alg, Database db)
    {
        ParamList ret = new ParamList(alg);
        ResultSet rs;
        Param param;
        int id, frequency;
        String getParamList =
            "SELECT * from Parameter WHERE algorithm='" +
            alg.getName() + "' ORDER BY id" + ";";

        //logger.debug(getParamList);

        try
        {
            rs = db.executeQuery(getParamList);
            while( rs.next() )
            {
                id = rs.getInt("id");
                frequency = rs.getInt("frequency");
                param = Param.getParam(id,alg,db);
                param.setFrequency(frequency);
                ret.add(param);
            }
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return null;
        }

        // note that the ParamList ret may be completely
        // empty, this is ok

        return ret;
    }

    /*****************************************************
     * Deletes this object from the database based on its
     * primary key. Returns true on success and false if
     * there has been an error. If the object is not found
     * true is still returned.
     *****************************************************/
    public boolean delete(Database db)
    {
        for( int i=0; i < size(); ++i )
        {
            if( !paramAt(i).delete(db) )
            {
                return false;
            }
        }

        setModified();

        return true;
    }

    /*****************************************************
     * Returns true if this Object has been modified since
     * it has last been saved, false otherwise.
     ****************************************************/
    public boolean modified()
    {
        return modified;
    }

    /*****************************************************
     * Registers that this object has been modified.
     *****************************************************/
    public void setModified()
    {
        modified = true;
    }

    /*****************************************************
     * Registers that this object has not been modified.
     *****************************************************/
    public void setNotModified()
    {
        modified = false;
    }

    /*****************************************************
     * Returns true if this object is equal to the given
     * object, false otherwise.
     ****************************************************/
    public boolean equals(Object other)
    {
        ParamList otherParamList;

        if( getClass().getName().equals(
            other.getClass().getName() ) )
        {
            otherParamList = (ParamList) other;
        }
        else
        {
            return false;
        }

        if( size() != otherParamList.size() )
        {
            return false;
        }

        for( int i=0; i < size(); ++i )
        {
            if( !paramAt(i).equals(otherParamList.paramAt(i)) )
            {
                return false;
            }
        }

        return true;
    }

    public Param paramAt(int i)
    {
        if( i >= size() )
        {
            return null;
        }
        return (Param) elementAt(i);
    }

    /***********************************************
     * Resets all the frequencies of all the Param in
     * this ParamList to 0.
     ************************************************/
    public void resetFrequencies()
    {
        setModified();
        for( int i=0; i < size(); ++i )
        {
            paramAt(i).setFrequency(0);
        }
    }

    /*****************************************************
     * Returns an Iterator that is used to iterate in
     * ascending order through all the Param objects
     * in this ParamList that are not ATParam. The first
     * Param should be the intercept with an id of 0.
     *****************************************************/
    public Iterator getParamIterator()
    {
        return new ParamIterator();
    }

    /*****************************************************
     * An Iterator that is used to iterate in
     * ascending order through all the Param objects
     * in this ParamList that are not ATParam. The first
     * Param should be the intercept with an id of 0.
     *****************************************************/
    public class ParamIterator implements Iterator
    {
        Param nextParam = null;
        int position;

        public ParamIterator()
        {
            position = 0;
            getNextParam();
        }

        public boolean hasNext()
        {
            if( nextParam != null )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
 
        public Object next()
        {
            Param ret = nextParam;
            getNextParam();
            return ret;
        }

        /***************************************
         * Does nothing
         **************************************/
        public void remove()
        {
            // does nothing
        }

        public void getNextParam()
        {
            Param param;
            String[] types = Constants.instance().getStringArray("PARAM_TYPES");
            if( types == null )
            {
                logger.error("PARAM_TYPES missing in constants.ini");
                nextParam = null;
            }

            while( position < size() )
            {
                param = paramAt(position++);

                String type = param.getClass().getName();

                if( types[Constants.instance().getInt("DESCRIPTOR_PARAM_INDEX")].equals(
                    type) )
                {
                    nextParam = param;
                    return;
                }
            }

            nextParam = null;
        }
    }

    /*****************************************************
     * Returns an Iterator that can be used to iterate
     * through the ATParam objects in this ParamList in
     * reverse order.
     *****************************************************/
    public Iterator getReverseATParamIterator()
    {
        return new ReverseATParamIterator();
    }

    /*******************************************************
     * An Iterator that iterates through the ATParam objects
     * in this ParamList in reverse order.
     *******************************************************/
    public class ReverseATParamIterator implements Iterator
    {
        Param nextParam = null;
        int position;

        public ReverseATParamIterator()
        {
            position = size()-1;
            getNextParam();
        }

        public boolean hasNext()
        {
            if( nextParam != null )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
 
        public Object next()
        {
            Param ret = nextParam;
            getNextParam();
            return ret;
        }

        /***************************************
         * Does nothing. Required by Iterator
         **************************************/
        public void remove()
        {
            // does nothing
        }

        public void getNextParam()
        {
            Param param;

            String[] types = Constants.instance().getStringArray("PARAM_TYPES");
            if( types == null )
            {
                logger.error("PARAM_TYPES missing in constants.ini");
                nextParam = null;
            }

            while( position >= 0 )
            {
                param = paramAt(position--);

                String type = param.getClass().getName();

                if( types[Constants.instance().getInt("ATPARAM_INDEX")].equals(
                    type) )
                {
                    nextParam = param;
                    return;
                }
            }

            nextParam = null;
        }
    }

    /*****************************************************
     * Returns an Iterator that is used to iterate in
     * ascending order through all the GroupParam objects
     * in this ParamList.
     *****************************************************/
    public Iterator getGroupParamIterator()
    {
        return new GroupParamIterator();
    }

    /*****************************************************
     * An Iterator that is used to iterate in
     * ascending order through all the GroupParam objects
     * in this ParamList.
     *****************************************************/
    public class GroupParamIterator implements Iterator
    {
        Param nextParam = null;
        int position;

        public GroupParamIterator()
        {
            position = 0;
            getNextParam();
        }

        public boolean hasNext()
        {
            if( nextParam != null )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
 
        public Object next()
        {
            Param ret = nextParam;
            getNextParam();
            return ret;
        }

        /***************************************
         * Does nothing
         **************************************/
        public void remove()
        {
            // does nothing
        }

        public void getNextParam()
        {
            Param param;
            String[] types = Constants.instance().getStringArray("PARAM_TYPES");
            if( types == null )
            {
                logger.error("PARAM_TYPES missing in constants.ini");
                nextParam = null;
            }

            while( position < size() )
            {
                param = paramAt(position++);

                String type = param.getClass().getName();

                if( types[Constants.instance().getInt("GROUP_PARAM_INDEX")].equals(
                    type) )
                {
                    nextParam = param;
                    return;
                }
            }

            nextParam = null;
        }
    }

    /********************************************************
     * Ensures that ids of all Params in this ParamList are
     * the same as their index in the ParamList.
     **********************************************************/
    public void reIndex()
    {
        Param param;

        for( int i=0; i < size(); ++i )
        {
            param = paramAt(i);
            param.setId(i);
        }
    }

    /**********************************************************
     * Removes the element at the specified index from this
     * ParamList.
     ********************************************************/
    public Object remove(int index)
    {
        setModified();
        alg.setModified();
        return super.remove(index);
    }

    public void removeElementAt(int index)
    {
        setModified();
        alg.setModified();
        super.removeElementAt(index);
    }
}
