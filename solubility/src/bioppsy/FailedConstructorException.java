package bioppsy;

/***********************************************
 * A FailedConstructorException is thrown by a
 * class constructor when there has been an
 * error. This is used because constructors 
 * cannot return values.
 ***********************************************/
public class FailedConstructorException
    extends Exception
{
    public FailedConstructorException(String str)
    {
        super(str);
    }
}
