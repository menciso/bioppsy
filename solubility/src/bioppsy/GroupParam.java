package bioppsy;

// sql
import java.sql.SQLException;
import java.sql.ResultSet;

import bioppsy.algorithm.MLR;



/******************************************************
 * A GroupParam is an ATParam that is treated
 * as a Group Contribution Parameter. When matching
 * atoms in a molecule, each atom can match more
 * than one GroupParam as opposed to at most one
 * ATParam.
 ******************************************************/
public class GroupParam extends ATParam
{
    public String getType()
    {
        return "Group Contribution";
    }

    public GroupParam() 
    {
        super();
    }

    public GroupParam(MLR al)
    {
        super(al);
    }

    public GroupParam(MLR al, int id, double value)
    {
        super(al,id,value);
    }
    
    
    protected Param get(int id, MLR alg, Database db)
    throws SQLException
{
    ResultSet rs;
    GroupParam groupParam;

    String load =
        "SELECT value,frequency FROM Parameter WHERE" + 
        " algorithm='" + alg.getName() + "'" +
        " AND id=" + id + ";";


    rs = db.executeQuery(load);
    while(rs.next())
    {
         groupParam = new GroupParam(alg,id,rs.getDouble("value"));
         groupParam.setFrequency(rs.getInt("frequency"));
         if( !groupParam.loadSmarts(db) )
         {
             return null;
         }
         groupParam.setNotModified();
         return groupParam;
    }
    return null;
}
}
