package bioppsy;

//log4j
import org.apache.log4j.Logger;

import bioppsy.algorithm.MLR;


// sql
import java.sql.SQLException;
import java.sql.ResultSet;
//JOELib
import joelib.desc.*;
import joelib.desc.result.*;

/******************************************************
 * A Descriptor Param is a Param that has a Descriptor
 * object. The Descriptor object implements the
 * joelib.desc.Descriptor interface.
 ******************************************************/
public class DescriptorParam extends Param
{
    static Logger logger = Logger.getLogger(DescriptorParam.class);

    private Descriptor descriptor = null;

    protected DescriptorParam() 
    {
       super();
    } 

    public DescriptorParam(MLR al)
    {
        super(al);
    }

    public DescriptorParam(MLR al, int id, double value)
    {
        super(al,id,value);
    }

    public void setDescriptor(Descriptor descriptor)
    {
        this.descriptor = descriptor;
    }

    public Descriptor getDescriptor()
    {
        return descriptor;
    }

    public String getType()
    {
        if( descriptor == null )
        {
            logger.warn("No Descriptor Object inside DesciptorParam");
            return null;
        }
        DescriptorInfo descInfo = descriptor.getDescInfo();
        if( descInfo == null )
        {
            logger.warn("Failed to get descriptor information");
            return null;
        }
        else
        {
            return descInfo.getName();
        }
    }

    /*******************************************************
     * Returns the value of this descriptor for the given
     * Mol.
     ******************************************************/
    public double calculateOccurrence(Mol mol)
        throws DescriptorException
    {
        DoubleResult result = (DoubleResult) descriptor.calculate(mol);
        return result.getDouble();
    }

    /***************************************************
     * Saves any changes to this DescriptorParam's values in the 
     * database. Returns true on success, false on 
     * error. Param.save() is used initially and then
     * the Descriptor type is saved in the ParameterExtra
     * table.
     ***************************************************/
    public boolean save(Database db)
    {
        // note we must first save the
        // Param object otherwise we
        // will not have a database row
        // to save to
        if( !super.save(db) )
        {
            return false;
        }

        if( !saveDescriptor(db) )
        {
            logger.error("Unable to save descriptor");
            return false;
        }

        return true;
    }

    /***************************************************
     * Saves the type of the descriptor in the database
     * so it can be loaded later.
     ***************************************************/
    public boolean saveDescriptor(Database db)
    {
        if( descriptor == null )
        {
            return false;
        }

        String save = 
            "UPDATE Parameter SET" +
            " descriptor='" + descriptor.getDescInfo().getName() + "'" +
            " WHERE algorithm='" + getAlgorithm().getName() + "'" +
            " AND id=" + getId() + ";";

        try
        {
            db.execute(save);
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }

        return true;
    }

    protected Param get(int id, MLR alg, Database db)
        throws SQLException
    {
        ResultSet rs;
        DescriptorParam param;

        String load =
            "SELECT value,frequency,descriptor FROM Parameter WHERE" + 
            " algorithm='" + alg.getName() + "'" +
            " AND id=" + id + ";";


        rs = db.executeQuery(load);
        while(rs.next())
        {
             param = new DescriptorParam(alg,id,rs.getDouble("value"));
             param.setFrequency(rs.getInt("frequency"));

             String descriptorName = rs.getString("descriptor");
             if( rs.wasNull() )
             {
                 logger.error("No descriptor value for DescriptorParam: " +
                     id);
                 return null;
             }
             try
             {
                 Descriptor descriptor = 
                     DescriptorFactory.getDescriptor(descriptorName);
                 param.setDescriptor(descriptor);
             }
             catch( DescriptorException de )
             {
                 logger.error(de.getMessage());
                 return null;
             }

             param.setNotModified();
             return param;
        }
        return null;
    }

    /*********************************************************
     * Returns the value of this descriptor for the given
     * Mol. Returns NaN on error.
     ********************************************************/
    public double getDouble(Mol mol)
    {
        try
        {
            return calculateOccurrence(mol);
        }
        catch( DescriptorException de )
        {
            logger.error(de.getMessage());
            return Double.NaN;
        }
    }
}
