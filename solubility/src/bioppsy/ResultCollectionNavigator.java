package bioppsy;

//log4j
import org.apache.log4j.Logger;

import bioppsy.algorithm.MLR;

// sql
import java.sql.SQLException;
import java.sql.ResultSet;

// io
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

// util
import java.util.Iterator;

/****************************************************************
 * <p>
 * A ResultCollectionNavigator has an Algorithm and MolSet. A 
 * Navigator object is used to iterate through a series of 
 * ResultCollection objects.
 * </p>
 * <p>
 * All the Result objects for a
 * particular Algorithm and MolSet are partitioned into chunks
 * of RESULT_COLLECTION_NAVIGATOR_SIZE number of Results which
 * are represented as ResultCollection objects. These
 * ResultCollections can be retrieved using the following methods:
 * <br><br>
 * first() - retrieve the first one<br>
 * last() - retrieve the last one<br>
 * previous() - retrieve the one immediately after the last
 *     one retrieved<br>
 * next() - retrieve the one immediately after the last one
 *     retrieved<br>
 * </p>
 * <p>
 * The ResultCollection objects are sorted by the field specified
 * by the orderBy parameter passed to the constructor.
 * </p>
 ****************************************************************/
public class ResultCollectionNavigator
{
    private MLR alg;
    private MolSet molSet;
    private Database db;
    private int collectionSize = 
        Constants.instance().getInt("RESULT_COLLECTION_NAVIGATOR_SIZE");
    private String orderBy = "molNumber";

    private ResultCollection rc;

    static Logger logger = Logger.getLogger(ResultCollectionNavigator.class);

    /************************************************************************
     * Inputs:<br>
     * -------<br>
     * Algorithm alg - the Algorithm used to calculate the Results (by name)<br>
     * MolSet molSet - the file of molecules from which the Results were obtained<br>
     * String orderBy - One of; molNumber,molCAS,value,expSol. Determines
     *     which field the Results are sorted by. These possible values are available 
     *     in Constants.instance().getStringArray("RESULT_FIELDS")<br>
     **********************************************************************/
    public ResultCollectionNavigator(MLR alg, MolSet molSet,
        String orderBy, Database db)
    {
        this.alg = alg;
        this.molSet = molSet;
        this.orderBy = orderBy;
        this.db = db;
    }

    public MolSet getMolSet()
    {
        return molSet;
    }

    public MLR getAlgorithm()
    {
        return alg;
    }

    public void setAlgorithm(MLR alg)
    {
        this.alg = alg;
    }

    /**********************************************************************
     * Uses the given query to retrieve a ResultCollection from the given
     * Database. Returns the ResultCollection on success and null on failure
     * or if not Results were found for the given query.
     ***********************************************************************/
    private ResultCollection getResultCollection(String query, Database db)
    {
        boolean haveRc = false;
        ResultCollection ret = new ResultCollection(alg,molSet);
        int id;
        int molNumber;
        String molId;
        double value;
        double expProp;
        Mol mol;
        Result result;
        ResultSet rs;

        try
        {
            rs = db.executeQuery(query);

            while( rs.next() )
            {
                id = rs.getInt("id");
                molNumber = rs.getInt("molNumber");
                mol = new Mol(molSet,molNumber);

                molId = rs.getString("molCAS");
                if( !rs.wasNull() )
                {
                    // molCAS was not null
                    mol.setMolId(molId);
                }

                expProp = rs.getDouble("expSol");
                if( !rs.wasNull() )
                {
                    // expSol was not null
                    mol.setExpProp(expProp);
                }

                value = rs.getDouble("value");

                result = new Result();
                result.setResultCollection(ret);
                result.setMol(mol);
                result.setValue(value);
                result.setId(id);
                ret.add(result);
            
                haveRc = true;
            }

            if( haveRc )
            {
                return ret;
            }
            else
            {
                return null;
            }
        }
        catch(SQLException sqle)
        {
            logger.debug(sqle.getMessage());
            return null;
        }
    }

    private String getOrderBy()
    {
        return orderBy;
    }

    /******************************************************
     * Sets the column which to order the results
     * by. Must be one of the strings in:
     * Constants.instance().getStringArray("RESULT_FIELDS").
     * Returns true if the orderBy field is set to the new value
     * and false if it is not an acceptable field.
     ******************************************************/
    public boolean setOrderBy(String newOrderBy)
    {
        String[] resultFields = 
            Constants.instance().getStringArray("RESULT_FIELDS");

        for( int i=0; i < resultFields.length; ++i )
        {
            if( resultFields[i].equals(newOrderBy) )
            {
                orderBy = newOrderBy;
                return true;
            }
        }

        return false;
    }

    private String getInitialSelect()
    {
        String initialSelect = 
            "SELECT * from Result WHERE" +
            " algorithm='" + alg.getName() + "'" +
            " AND molSet=" + 
                molSet.getId();
         return initialSelect;
    }

    private String getInitialSelectWithLimit()
    {
        String initialSelect = 
            "SELECT" +
            " LIMIT " + " 0 " + collectionSize +
            " * from Result WHERE" +
            " algorithm='" + alg.getName() + "'" +
            " AND molSet=" + 
                molSet.getId();
         return initialSelect;
    }

    private String getOrderByQuery()
    {
        return " ORDER BY " + orderBy;
    }

    private String getValueForResult(Result result)
    {
        if( orderBy.equals("molNumber") )
        {
            return (new Integer(result.getMol().getNumber())).toString();
        }
        if( orderBy.equals("molCAS") )
        {
            // must put quotes around strings in sql queries
            return "'" + result.getMol().getMolId() + "'";
        }
        if( orderBy.equals("value") )
        {
            return (new Double(result.getValue())).toString();
        }

        // default
        return (new Integer(result.getMol().getNumber())).toString();
    }

    /******************************************************************
     * Returns a ResultCollection containing up to the first 
     * RESULT_COLLECTION_NAVIGATOR_SIZE number of Results. 
     * null is returned on error or if there are no Results.
     *****************************************************************/
    public ResultCollection first()
    {
        if( alg == null )
        {
            return null;
        }

        String first = getInitialSelectWithLimit();

        first += getOrderByQuery() + " ASC;";

        ResultCollection ret = getResultCollection(first,db);
        if( ret != null )
        {
            rc = ret;
        }
        return ret;
    }

    /******************************************************************
     * Returns a ResultCollection containing up to the last 
     * RESULT_COLLECTION_NAVIGATOR_SIZE number of Results.
     * null is returned on error or if there are no Results.
     *****************************************************************/
    public ResultCollection last()
    {
        if( alg == null )
        {
            return null;
        }

        String last = getInitialSelectWithLimit();

        last += getOrderByQuery() + " DESC;";

        ResultCollection ret = getResultCollection(last,db);
        if( ret != null )
        {
            // must reverse because we are retrieving
            // the results in reverse order
            ret.reverse();
            rc = ret;
        }
        return ret;
    }

    /******************************************************************
     * Returns a ResultCollection containing up to the next 
     * RESULT_COLLECTION_NAVIGATOR_SIZE number of Results starting
     * from after the last position accessed.
     * null is returned on error or if there are no Results left.
     *****************************************************************/
    public ResultCollection next()
    {
        if( alg == null )
        {
            return null;
        }

        if( rc == null )
        {
            return null;
        }

        String next = getInitialSelectWithLimit();
        
        if( rc.size() <= 0 )
        {
            return null;
        }

        Result result = (Result) rc.elementAt(rc.size()-1);

        next += " AND " + getOrderBy() + ">" + getValueForResult(result);

        next += getOrderByQuery() + " ASC;";

        //logger.debug(next);

        ResultCollection ret = getResultCollection(next,db);
        if( ret != null )
        {
            rc = ret;
        }
        return ret;
    }

    /******************************************************************
     * Returns a ResultCollection containing up to the previous 
     * RESULT_COLLECTION_NAVIGATOR_SIZE number of Results starting
     * from just before the first Result returned in the last
     * ResultCollection accesed.
     * null is returned on error or if there are no Results left.
     *****************************************************************/
    public ResultCollection previous()
    {
        if( alg == null )
        {
            return null;
        }

        if( rc == null )
        {
            return null;
        }

        String previous = getInitialSelectWithLimit();
        
        if( rc.size() <= 0 )
        {
            return null;
        }

        Result result = (Result) rc.elementAt(0);

        previous += " AND " + getOrderBy() + "<" + getValueForResult(result);

        previous += getOrderByQuery() + " DESC;";

        //logger.debug(previous);

        ResultCollection ret = getResultCollection(previous,db);
        if( ret != null )
        {
            ret.reverse();
            rc = ret;
        }
        return ret;
    }
    
    /*************************************************
     * Returns the mean of the experimental values
     */
    
    public double getMean()
    {
        double sum = 0.0;
        double mean;
        int i, size;
        ResultCollection rc;
        Result result = null;
        // keep track of the number of exceptions
        // we receive, if there are too many we
        // should abort.
        int numExceptions = 0;
        int MAX_EXCEPTIONS = Constants.instance().getInt("MAX_EXCEPTIONS");
        // calculate the mean
        size = 0;
        for( rc = first(); rc != null; rc = next() )
        {
            for( i=0; i < rc.size(); ++i )
            {
                try
                {
                    result = (Result) rc.elementAt(i);
                    sum += result.getMol().getExpProp();
                    ++size;
                }
                catch( NotFoundException nfe )
                {
                    logger.warn(nfe.getMessage());
                    ++numExceptions;
                    if( numExceptions > MAX_EXCEPTIONS )
                    {
                        // abort if there are too many
                        // exceptions being received
                        return Double.NaN;
                    }
                }
            }
        }
        mean = sum / size;
        
        return mean;
    }

    /*************************************************
     * Returns the r squared value which is: <br>
     * sum( (y'- y*)^2 ) / sum( (y - y*)^2<br>
     * where y' is the estimated property of a
     * molecule and y* is the mean.
     * If the expProp value cannot be retrieved more
     * than MAX_EXCEPTIONS times NaN is returned.
     ************************************************/
    public double getRsq()
    {
        double mean = 0.0;
        double numerator = 0.0;
        double denominator = 0.0;
        double x=0.0;
        double y=0.0;
        double r2;
        int i;
        double size=0;
        ResultCollection rc;
        Result result = null;
        Mol mol = null;
        
        

        // keep track of the number of exceptions
        // we receive, if there are too many we
        // should abort.
        int numExceptions = 0;
        int MAX_EXCEPTIONS = Constants.instance().getInt("MAX_EXCEPTIONS");
        
        double sumy2=0,sumy=0,sse=0,ssto=0;
        double yhat;
        double rsq=0.0;
        for( rc = first(); rc != null; rc = next() )
        {
        	ParamList paramList = rc.getAlgorithm().getParamList();
            boolean hasInt=false;
            for( i=0; i < paramList.size(); ++i )
            {
                hasInt = paramList.paramAt(i).isIntercept();
                if(hasInt)
                	break;
            }
            if(hasInt)
            {
              for( i=0; i < rc.size(); ++i )
              {
                try
                {
                    result = (Result) rc.elementAt(i);
                    yhat = result.getValue(); // estimated
                    y = result.getMol().getExpProp(); // experimental

                    sumy2 += Math.pow(y, 2);
                    sumy += y;
                    sse += Math.pow(y-yhat,2);
                    size +=1.0;
                }
                catch( NotFoundException nfe )
                {
                    logger.warn(nfe.getMessage());
                    ++numExceptions;
                    if( numExceptions > MAX_EXCEPTIONS )
                    {
                        // abort if there are too many
                        // exceptions being received
                        return Double.NaN;
                    }
                }
              }
              ssto = sumy2 - Math.pow(sumy,2)/size;
              rsq = 1.0-sse/ssto;
            }
            else
            {
            	double sumyhy=0.0;
                for( i=0; i < rc.size(); ++i )
                {
                  try
                  {
                      result = (Result) rc.elementAt(i);
                      yhat = result.getValue(); // estimated
                      y = result.getMol().getExpProp(); // experimental

                      sumy2 += Math.pow(y, 2);
                      sumyhy += yhat*y;
                  }
                  catch( NotFoundException nfe )
                  {
                      logger.warn(nfe.getMessage());
                      ++numExceptions;
                      if( numExceptions > MAX_EXCEPTIONS )
                      {
                          // abort if there are too many
                          // exceptions being received
                          return Double.NaN;
                      }
                  }
                }
                rsq = sumyhy/sumy2;
            }
        }

//        //System.out.print("now " + sse + " ssr " + ssto + "ssto\n");
//        //System.out.print("mean " + mean + " ssto\n");
//   
        return rsq;
    }

    /****************************************
     * Returns the mean unsigned error of the
     * estimated property values from the 
     * experimental values;
     * If the expSol value cannot be retrieved more
     * than MAX_EXCEPTIONS times NaN is returned.
     *****************************************/
    public double getMUE()
    {
        int i;
        double error = 0.0;
        Result result = null;
        ResultCollection rc;
        int size = 0;
 
        // keep track of the number of exceptions
        // we receive, if there are too many we
        // should abort.
        int numExceptions = 0;
        int MAX_EXCEPTIONS = Constants.instance().getInt("MAX_EXCEPTIONS");
       
        for( rc = first(); rc != null; rc = next() )
        {
            for( i=0; i < rc.size(); ++i )
            {
                try
                {
                    result = (Result) rc.elementAt(i);
                    error += Math.abs(result.getValue() - 
                        result.getMol().getExpProp() );
                    ++size;
                }
                catch( NotFoundException nfe )
                {
                    logger.warn(nfe.getMessage());
                    ++numExceptions;
                    if( numExceptions > MAX_EXCEPTIONS )
                    {
                        // abort if there are too many
                        // exceptions being recieved
                        return Double.NaN;
                    }
                }
            }
        }

        return error / size;
    }
  
    /*************************************************
     * Returns the standard deviation of the estimated
     * property values from the experimental property
     * values.
     * If the expSol value cannot be retrieved more
     * than MAX_EXCEPTIONS times NaN is returned.
     *************************************************/
    public double getSdDev()
    {
        int i;
        double stDev;
        double sumSq = 0.0;
        Result result = null;
        ResultCollection rc;
        int size = 0;

        // keep track of the number of exceptions
        // we receive, if there are too many we
        // should abort.
        
        
        int numExceptions = 0;
        int MAX_EXCEPTIONS = Constants.instance().getInt("MAX_EXCEPTIONS");
        for( rc = first(); rc != null; rc = next() )
        {
            // calculate the sum of squared deviations
            for( i=0; i < rc.size(); ++i )
            {
                try
                {
                    result = (Result) rc.elementAt(i);
                    sumSq += Math.pow( result.getValue() - 
                        result.getMol().getExpProp(), 2 );
                    ++size;
                }
                catch( NotFoundException nfe )
                {
                    logger.warn(nfe.getMessage());
                    ++numExceptions;
                    if( numExceptions > MAX_EXCEPTIONS )
                    {
                        // abort if there are too many
                        // exceptions being received
                        return Double.NaN;
                    }
                }
            }
        }
        --size;
        return Math.sqrt(sumSq/size );
    }

    /***************************************************** 
     * Returns a ResultCollectionNavigator that the same
     * as this one. The Algorithm, MolSet, and Database
     * references from the old object are simply copied
     * to the new one. The new object does not retain
     * the position.
     ****************************************************/
    public ResultCollectionNavigator getAnother()
    {
        return new ResultCollectionNavigator(
            alg,molSet,orderBy,db
        );
    }

    /**************************************************************
     * Outputs all the results for this Algorithm and MolSet pair
     * to the given filename. Returns true if all data has
     * been successfully written, false otherwise.
     *************************************************************/
    public boolean printResultsToFile(File file)
    {
        Iterator iterator;
        Result result;
        String line;

        if( alg == null )
        {
            logger.debug("Null algorithm in ResultCollectionNavigator");
            return false;
        }

        try
        {

            PrintWriter out = new PrintWriter(new FileWriter(file));

            line = "Property values for Algorithm: " + alg.getName() +
                " for the molecules in: " + molSet.getFile().getName();

            out.println(line);

            ResultCollection rc = first();

            while( rc != null )
            {
                iterator = rc.getIterator();
                while( iterator.hasNext() )
                {
                   result = (Result) iterator.next();
                   line = 
                       "" + result.getMol().getNumber() +
                       "," + result.getValue();
                   out.println(line);
                }

                rc = next();
            }

            out.close();
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }

        return true;
    }
}
