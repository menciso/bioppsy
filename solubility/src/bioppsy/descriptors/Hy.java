package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Mol;



//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
//java
import java.util.Vector;

/*********************************************************
 * Hy: hydrophilicity index as defined
 *  by Todeschini and Consonni (DOI:10.1080/10629369708039130):
 * Hy=[(1+N_hy)*ln(1+N_hy)+N_c(1/A+ln(1/A))+SQRT(N_hy/A)]/ln(1+A)
 * where N_hy is the number of hydrophilic groups (OH,SH,NH)
 * N_c is the number of carbons and
 * A is the total number of atoms hydrogen excluded
 * 
 * author: Marta Enciso, La Trobe University
 ****************************************************************/
public class Hy extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(Hy.class);
    public static final String DESC_KEY = "Hy";

    public static final String[] Smarts_Nhy = // Hydrophilic groups
    {
    	"[NX3;H2,H1;!$(NC=O)]", 	// Primary or secondary amine, not amide
    	"[O;H1]", 					// Hydroxyl group
    	"[S;H1]"					// tiol
    };
    
    public static final String Smarts_A = "[!#1]"; // Atoms not H
    public static final String Smarts_Nc = "[c,C]"; // C atoms

    public Hy()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Hydrophilicity index";
    }

    /**************************************************************
     * Returns Hy, the hydrophilicity index
     * @throws DescriptorException 
     **************************************************************/
    public double getDoubleValue(JOEMol jMol) throws DescriptorException
    {
    	double nC = getC(jMol);
    	double nA = getA(jMol);
    	double nHy = getNhy(jMol);
    	double hy;
    	    	
    	hy=((1+nHy)*Math.log(1+nHy)+nC/nA*Math.log(1.0/nA)+Math.sqrt(nHy)/nA)/Math.log(1+nA);
    	//System.out.print(hy + "\n");
        return hy;
    }
    
    private double getC(JOEMol jMol)
	throws DescriptorException
{
    ATParam atParam = new ATParam();
    if( !atParam.addSmarts(Smarts_Nc) )
    {
        throw new DescriptorException(
           "Invalid SMARTS pattern: " + Smarts_Nc);
    }
    Vector matches = atParam.matchList(new Mol(jMol));
    if( matches == null )
    {
        throw new DescriptorException(
           "Error matching SMARTS to calculate N_C");
    }
    else
    {
       return matches.size();
    }
}

    
    private double getA(JOEMol jMol)
    	throws DescriptorException
    {
        ATParam atParam = new ATParam();
        if( !atParam.addSmarts(Smarts_A) )
        {
            throw new DescriptorException(
               "Invalid SMARTS pattern: " + Smarts_A);
        }
        Vector matches = atParam.matchList(new Mol(jMol));
        if( matches == null )
        {
            throw new DescriptorException(
               "Error matching SMARTS to calculate N_A");
        }
        else
        {
           return matches.size();
        }
    }

	private double getNhy(JOEMol jMol)
    throws DescriptorException
    {
        ATParam atParam = new ATParam();
        for( int i=0; i < Smarts_Nhy.length; ++i )
        {
            if( !atParam.addSmarts(Smarts_Nhy[i]) )
            {
                throw new DescriptorException(
                    "Invalid SMARTS pattern: " + Smarts_Nhy[i]);
            }
        }

        Vector matches = atParam.matchList(new Mol(jMol));
        if( matches == null )
        {
            throw new DescriptorException(
                "Error matching SMARTS to calculate N_Hy");
        }
        else
        {            
            return matches.size();
        }
    }
}
