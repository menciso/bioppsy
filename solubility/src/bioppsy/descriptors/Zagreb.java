package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.molecule.JOEAtom;
import joelib.util.iterator.AtomIterator;
import joelib.desc.*;

/*******************************************************************************************
 * Zagreb: sum of squares of (heavy atom) valencies
 ******************************************************************************************/
public class Zagreb extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(Zagreb.class);
    public static final String DESC_KEY = "Zagreb";

    public Zagreb()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "sum of squares of (heavy atom) valencies";
    }

    /**************************************************************
     * returns Zagreb: sum of squares of (heavy atom) valencies
     * in a hydrogen-suppressed molecule
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        double sum = 0.0;
        JOEAtom atom;
        AtomIterator ai = jMol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            if(!atom.isHydrogen()) {
                sum += Math.pow(atom.getHvyValence(),2);
            }
        }
   
/*        System.out.println("Zagreb "+sum); */
        
        return sum;
    }
}
