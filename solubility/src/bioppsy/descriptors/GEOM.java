package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.util.iterator.AtomIterator;
import joelib.desc.*;
import java.util.Arrays;

//Jama
import Jama.Matrix;
import Jama.EigenvalueDecomposition;

/*********************************************************
 * GEOM is a Descriptor that calculates the geometric moment 
 * of a molecule. The geometric moment is the principal moment
 * of inertia.
 * See <a href="http://kwon3d.com/theory/moi/prin.html">the reference</a>
 * for a description of how principal moments of inertia are 
 * calculated.
 ****************************************************************/
public class GEOM extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(GEOM.class);
    public static final String DESC_KEY = "GEOM";

    public GEOM()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /**************************************************************
     * Returns the geometric moment for this molecule.
     * This superclass is used by GEOM1 and GEOM3
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        logger.warn("Using superclass GEOM, returning value of 0.0");
        return 0.0;
    }

    /****************************************************************
     * Returns the moment of inertia of the given molecule.
     ****************************************************************/
    
    /* The original version had the "geometric moment", which is slightly from the moment of inertia. 
     * See <a href="http://kwon3d.com/theory/moi/prin.html">the reference</a>
     * for a description of how principal moments of inertia are calculated. The code did not pass 
     * its test, so the traditional version is left here. */
    public double getMomentOfInertia(JOEMol jMol, int axis)
        throws DescriptorException
    {
        if( (axis <= 0) || (axis > 3) )
        {
            throw new DescriptorException("Cannot calculate principal moment " +
                " of inertia of number: " + axis);
        }

        // values of the Inertia Tensor
        double Ixx = 0.0, Iyy = 0.0, Izz = 0.0,
               Ixy = 0.0, Ixz = 0.0, Iyz = 0.0;       
               
        // centre-of-mass
        double Cx  = 0.0, Cy  = 0.0, Cz  = 0.0,
               CMx = 0.0, CMy = 0.0, CMz = 0.0,
               Mt  = 0.0;
        
        // the Inertia Tensor
        Matrix iTensor = new Matrix(3,3);

        // the eigenvalues
        double[] eigenvalues;

        double mass;
        JOEAtom atom;
        AtomIterator ai;
        EigenvalueDecomposition eigenDecomp;
        
        // firstly, we determine the centre-of-mass
        ai = jMol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            
        // Ignore hydrogen; as dmgeo/ADAPT
            
            if(!atom.isHydrogen())
            {
                mass = atom.getAtomicMass();
                Cx += mass * atom.x();
                Cy += mass * atom.y();
                Cz += mass * atom.z();
                Mt += mass;
            }
        }
        CMx = Cx/Mt;
        CMy = Cy/Mt;
        CMz = Cz/Mt;

        // next, we calculate the values 
        // of the geometric moment (or moment of inertia) tensor
        ai = jMol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            mass = atom.getAtomicMass();
            
             
 //         Calculation of moments of inertia below
 //
          Ixx += mass *
              ( Math.pow((atom.y()-CMy),2) + Math.pow((atom.z()-CMz),2) );
          Iyy += mass *
              ( Math.pow((atom.x()-CMx),2) + Math.pow((atom.z()-CMz),2) );
          Izz += mass *
             ( Math.pow((atom.x()-CMx),2) + Math.pow((atom.y()-CMy),2) );
         Ixy += mass * (atom.x()-CMx) * (atom.y()-CMy);
        Ixz += mass * (atom.x()-CMx) * (atom.z()-CMz);
         Iyz += mass * (atom.y()-CMy) * (atom.z()-CMz);
 //           
 //         Geometric moment tensor components: as dmgeo/ADAPT
            
/*            if(!atom.isHydrogen())
            {
                Ixx += mass * ( Math.pow((atom.x()-CMx),2));
                Iyy += mass * ( Math.pow((atom.y()-CMy),2));
                Izz += mass * ( Math.pow((atom.z()-CMz),2));
                Ixy += mass * ( atom.x()-CMx) * (atom.y()-CMy);
                Ixz += mass * ( atom.x()-CMx) * (atom.z()-CMz);
                Iyz += mass * ( atom.y()-CMy) * (atom.z()-CMz);
            }
            */
        }
        

//        Moment of inertia tensor
//
        iTensor.set(0,0,Ixx);
        iTensor.set(0,1,Ixy);
        iTensor.set(0,2,Ixz);
        iTensor.set(1,0,Ixy);
        iTensor.set(1,1,Iyy);
        iTensor.set(1,2,Iyz);
        iTensor.set(2,0,Ixz);
        iTensor.set(2,1,Iyz);
        iTensor.set(2,2,Izz);
        
//        Geometric moment tensor: as dmgeo/ADAPT
        
/*          iTensor.set(0,0,(Ixx/Mt));
          iTensor.set(0,1,(Ixy/Mt));
          iTensor.set(0,2,(Ixz/Mt));
          iTensor.set(1,0,(Ixy/Mt));
          iTensor.set(1,1,(Iyy/Mt));
          iTensor.set(1,2,(Iyz/Mt));
          iTensor.set(2,0,(Ixz/Mt));
          iTensor.set(2,1,(Iyz/Mt));
          iTensor.set(2,2,(Izz/Mt));*/
        
        // note that the iTensor is symmetric so it will
        // only have real eigenvalues
          
        eigenDecomp = new EigenvalueDecomposition(iTensor);
        eigenvalues = eigenDecomp.getRealEigenvalues();

        // the eigenvalues are the principal moments of
        // inertia or geometric moments. We sort then to get the correct
        // order Iz <= Iy <= Ix
        Arrays.sort(eigenvalues);
        return eigenvalues[axis-1];
    }

}
