package bioppsy.descriptors;

//log4j
import jmat.data.Matrix;
import jmat.data.matrixDecompositions.EigenvalueDecomposition;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * Reciprocal distance sum Randic-like index, as described in
 * Molecular Descriptors for Chemoinformatics, Volume 41 (2 Volume Set)
 * By Roberto Todeschini, Viviana Consonni

 ****************************************************************/
public class VAA1 extends Topological
{
    static Logger logger = Logger.getLogger(VAA1.class);
    public static final String DESC_KEY = "VAA1";

    public VAA1()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "VAA1 - sum of positive eigenvalues of A";
    }
    
    /**************************************************************
     * Returns the descriptor for this molecule.
     * @throws DescriptorException 
     **************************************************************/
    public double getDoubleValue(JOEMol jMol) throws DescriptorException
    {
    	double result = getVAA1(jMol);
    	return result;
    }

    /*********************************************************
     * Reciprocal distance sum Randic-like index, as described in
     * Molecular Descriptors for Chemoinformatics, Volume 41 (2 Volume Set)
     * By Roberto Todeschini, Viviana Consonni
     ****************************************************************/
    public double getVAA1(JOEMol jMol)
        throws DescriptorException
    {
        EigenvalueDecomposition eigDec = getAdjDecomposition(jMol);
        Matrix D = eigDec.getD(); // Largest eigenvalue is always the last one!!

        double result=0.0;
        int isize=D.getColumnDimension();
        int i;

        double val=0; 
        // VED1
        for (i = 0; i < isize; i++)
        {
        	val=D.get(i,i);
        	if(val >0)
        		result+=val;
        }
        
        //System.out.print(result + "\n ");
//        // Adjacent matrix methods. Build first the adjacent matrix
//        Matrix adjMatrix = new Matrix (isize, isize);
//        for (i = 0; i < isize; i++)
//        {
//        	for (j = i; j < isize ; j++)
//        	{
//        		System.out.print(" " + distanceMatrix.get(i, j));
//      	  	  if(distanceMatrix.get(i,j)==1.0)
//        	  {
//      	  		adjMatrix.set(i, j, 1.0);
//        	  }
//      	  	  else
//      	  	  {
//      	  		adjMatrix.set(i, j, 0.0);
//      	  	  }
//        	}
//        	System.out.print("\n ");
//        }
//        // VRA1
//        EigenvalueDecomposition eigDec = adjMatrix.eig();
//        Matrix V = eigDec.getV();
//        Matrix D = eigDec.getD(); // Largest eigenvalue is always the last one!!
//        for (i = 0; i < isize; i++)
//        {
//        	for (j = i; j < isize ; j++)
//        	{
//              System.out.print(" " + adjMatrix.get(i, j));
//      	  	  if(distanceMatrix.get(i,j)==1.0)
//        	  {
//      	  		result+=1.0/Math.sqrt(V.get(i,isize1)*V.get(j, isize1));
//        	  }
//        	}
//        	System.out.print(result + "\n ");
//        }
        
        
        return result;
    }
}
