package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * The Intercept descriptor, always returns 1.0.
 ****************************************************************/
public class Intercept extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(Intercept.class);
    public static final String DESC_KEY = "Intercept";

    public Intercept()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Always 1.0";
    }

    /**************************************************************
     * Returns 1.0
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        return 1.0;
    }
}
