package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Mol;



//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
//java
import java.util.Vector;

/*********************************************************
 * HBondAcceptors - Count of hydrogen bond acceptor atoms<br>
 * A hydrogen bond acceptor is an atom that matches one of the
 * following SMARTS patterns:<br>
 * [O]<br>
 * [N&H2;X3;$(*-[C])]<br>
 * [N&H1;X3;$(*-[C]);$(*-[C])]<br>
 * [N&H0;X3;$(*-[C]);$(*-[C]);(*-[C])]<br>
 * [N;X2;$(*=[*])]<br>
 * [N;X1;$(*#[*])]<br>
 * [n;X2]
 ****************************************************************/
public class HBondAcceptors extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(HBondAcceptors.class);
    public static final String DESC_KEY = "HBondAcceptors";

    public static final String[] SMARTS = 
    {
        "[O]",
        "[N&H2;X3]-C",
        "[N&H1;X3](-C)-C",
        "[N&H0;X3](-C)(-C)-C",
        "[N;X2;$(*=[*])]",
        "[N;X1;$(*#[*])]",
        "[n;X2]"
    };

    public HBondAcceptors()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Count of hydrogen bond acceptor atoms";
    }

    /**************************************************************
     * Returns the count of hydrogen bond acceptor atoms<br>
     * A hydrogen bond acceptor is an atom that matches one of the
     * following SMARTS patterns:<br>
     * [O]
     * [N&H2;X3;$(*-[C])]
     * [N&H1;X3;$(*-[C]);$(*-[C])]
     * [N&H0;X3;$(*-[C]);$(*-[C]);(*-[C])]
     * [N;X2;$(*=[*])]
     * [N;X1;$(*#[*])]
     * [n;X2]
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        ATParam atParam = new ATParam();
        for( int i=0; i < SMARTS.length; ++i )
        {
            if( !atParam.addSmarts(SMARTS[i]) )
            {
                throw new DescriptorException(
                    "Invalid SMARTS pattern: " + SMARTS[i]);
            }
        }

        Vector matches = atParam.matchList(new Mol(jMol));
        if( matches == null )
        {
            throw new DescriptorException(
                "Error matching SMARTS to calculate HBondAcceptors");
        }
        else
        {
            return matches.size();
        }
    }
}
