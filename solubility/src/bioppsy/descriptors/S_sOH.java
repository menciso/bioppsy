package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.desc.*;

/*******************************************************************************************
 * S_sOH returns the number of hydroxyl groups in the given molecule.
 * Passes a SMARTS string describing hydroxyl groups to the abstract 
 * class SmartsPattern for calculation of number of matches
 ******************************************************************************************/
public class S_sOH extends SmartsPattern
{
    static Logger logger = Logger.getLogger(S_sOH.class);
    public static final String DESC_KEY = "S_sOH";

    public S_sOH()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*************************************************************
     * Return the SMARTS pattern that this SmartsPattern descriptor
     * matches as a String.
     *************************************************************/
    public String getPattern()
    {
        return "[OH]";
    }
}
