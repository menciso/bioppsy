package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;

/*********************************************************
 * MDE14, MDE (Molecular Distance Edge) with i=1, j=4. 
 ****************************************************************/
public class MDE14 extends MDE
{
    static Logger logger = Logger.getLogger(MDE14.class);
    public static final String DESC_KEY = "MDE14";

    public MDE14()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Molecular Distance Edge between all primary and quaternary carbons";
    }

    /**************************************************************
     * Returns the Molecular Distance Edge value for this molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        
/*        System.out.println(" MDE-14 "+(getMDE(jMol,1,4))); */
        
        return getMDE(jMol,1,4);
    }
}
