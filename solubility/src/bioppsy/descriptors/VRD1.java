package bioppsy.descriptors;

//log4j
import jmat.data.Matrix;
import jmat.data.matrixDecompositions.EigenvalueDecomposition;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * Reciprocal distance sum Randic-like index, as described in
 * Molecular Descriptors for Chemoinformatics, Volume 41 (2 Volume Set)
 * By Roberto Todeschini, Viviana Consonni

 ****************************************************************/
public class VRD1 extends Topological
{
    static Logger logger = Logger.getLogger(VRD1.class);
    public static final String DESC_KEY = "VRD1";

    public VRD1()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "VRD1 - Randic formula from LOVIs based on D";
    }
    
    /**************************************************************
     * Returns the descriptor for this molecule.
     * @throws DescriptorException 
     **************************************************************/
    public double getDoubleValue(JOEMol jMol) throws DescriptorException
    {
    	double result = getVRD1(jMol);
    	return result;
    }

    /*********************************************************
     * Reciprocal distance sum Randic-like index, as described in
     * Molecular Descriptors for Chemoinformatics, Volume 41 (2 Volume Set)
     * By Roberto Todeschini, Viviana Consonni
     ****************************************************************/
    public double getVRD1(JOEMol jMol)
        throws DescriptorException
    {
    	Matrix distanceMatrix = getHeavyTopologicalDistanceMatrix(jMol);
        EigenvalueDecomposition eigDec = getDistDecomposition(jMol);
        Matrix V = eigDec.getV(); // Largest eigenvalue is always the last one!!

        double result=0.0;
        int isize=V.getColumnDimension(),isize1=isize-1;
        // VRD1
        int i,j;
        for (i = 0; i < isize; i++)
        {
        	for (j = i; j < isize ; j++)
        	{
        		if(distanceMatrix.get(i,j)==1.0)
        		{
        			result+=1.0/Math.sqrt(V.get(i,isize1)*V.get(j, isize1));
        		}
        	}
        }
        return result;
    }
}
