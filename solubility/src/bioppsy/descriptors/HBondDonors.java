package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Mol;



//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
//java
import java.util.Vector;

/*********************************************************
 * HBondDonors - Count of hydrogen bond donor atoms<br>
 * A hydrogen bond acceptor is an atom that matches one of the
 * following SMARTS patterns:<br>
 * [N&H1]<br>
 * [N&H2]<br>
 * [N&H3]<br>
 * [O&H1]<br>
 * [n&H1]
 ****************************************************************/
public class HBondDonors extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(HBondDonors.class);
    public static final String DESC_KEY = "HBondDonors";

    public static final String[] SMARTS = 
    {
        "[N&H1]",
        "[N&H2]",
        "[N&H3]",
        "[O&H1]",
        "[n&H1]",
        "[n&h1]"
    };

    public HBondDonors()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Count of hydrogen bond donor atoms";
    }

    /**************************************************************
     * Returns HBondDonors - Count of hydrogen bond donor atoms<br>
     * A hydrogen bond acceptor is an atom that matches one of the
     * following SMARTS patterns:<br>
     * [NH]<br>
     * [NH2]<br>
     * [NH3]<br>
     * [OH]<br>
     * [nH]
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        ATParam atParam = new ATParam();
        for( int i=0; i < SMARTS.length; ++i )
        {
            if( !atParam.addSmarts(SMARTS[i]) )
            {
                throw new DescriptorException(
                    "Invalid SMARTS pattern: " + SMARTS[i]);
            }
        }

        Vector matches = atParam.matchList(new Mol(jMol));
        if( matches == null )
        {
            throw new DescriptorException(
                "Error matching SMARTS to calculate HBondDonors");
        }
        else
        {
     
/*            System.out.println("HBD "+matches.size()); */
            
            return matches.size();
        }
    }
}
