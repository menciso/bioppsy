package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Mol;



//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
//java
import java.util.Vector;

/*********************************************************
 * HBondAcceptor Nitrogen - Count of hydrogen bond acceptor nitrogens<br>
* A hydrogen bond acceptor nitrogen is an atom that matches the
     * following SMARTS pattern (from Daylight website):<br>
     * [N,n;!$([*+1,*+2,*+3])]
     * 
     * @author Marta Enciso. La Trobe University. 2014

 ****************************************************************/
public class HBAN extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(HBAN.class);
    public static final String DESC_KEY = "HBAN";

    public static final String SMARTS = 
    	"[n,N;!$([nX3,#7v5,*+1,*+2,*+3])]";

    public HBAN()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Count of hydrogen bond acceptor nitrogens";
    }

    /**************************************************************
     * Returns the count of hydrogen bond acceptor nitrogens<br>
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        ATParam atParam = new ATParam();
        if( !atParam.addSmarts(SMARTS) )
        {
            throw new DescriptorException(
               "Invalid SMARTS pattern: " + SMARTS);
        }

        Vector matches = atParam.matchList(new Mol(jMol));
        if( matches == null )
        {
            throw new DescriptorException(
                "Error matching SMARTS to calculate HBAN");
        }
        else
        {
            return matches.size();
        }
    }
}
