package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.Mol;



//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * Gas phase solvation energy from Gaussian. To be added by user
 * 
 ****************************************************************/
public class User_deltaG_sub extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(User_deltaG_sub.class);
    public static final String DESC_KEY = "User_deltaG_sub";

    public User_deltaG_sub()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "User-supplied sublimation energy - provided in molecule input";
    }

    /**************************************************************
     * Returns the sublimation energy of the given molecule, provided 
     * as part of the initial molecule input.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {        
        return Double.parseDouble(((Mol)jMol).getdeltaGsub());
    }
}