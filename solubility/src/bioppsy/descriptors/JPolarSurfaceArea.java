package bioppsy.descriptors;

//log4j
import joelib.desc.Descriptor;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorFactory;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.desc.result.DoubleResult;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * JPolarSurfaceArea - Polar Surface Area descriptors provided
 * by JOELib.
 ****************************************************************/
public class JPolarSurfaceArea extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(JPolarSurfaceArea.class);
    public static final String DESC_KEY = "JPolarSurfaceArea";

    public JPolarSurfaceArea()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Polar Surface Area provided by JOELib";
    }

    /**************************************************************
     * Returns Polar Surface Area.
     **************************************************************/
    
    public double getDoubleValue(JOEMol jMol)
    throws DescriptorException
    {
    double result = getJPSA(jMol); 
    //System.out.print(result + "\n");
    return result;
    }
    /************************************************************
    * The method 
    * ***********************************************************/
    public double getJPSA(JOEMol jMol)
        throws DescriptorException
    {
        String psaKey = "PolarSurfaceArea";
        //jMol.addHydrogens();
        Descriptor descriptor = DescriptorFactory.getDescriptor(psaKey);
        if( descriptor == null )
        {
            throw new DescriptorException(
                "Descriptor " + psaKey + " could not be initialised");
        }
        DoubleResult result = (DoubleResult) descriptor.calculate(jMol);
        if( result == null )
        {
            throw new DescriptorException(
                "Descriptor " + psaKey + " could not be calculated");
        }
        return result.getDouble();
    }
}
