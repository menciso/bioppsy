package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Mol;



//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
//java
import java.util.Vector;

/*********************************************************
 * Hydrogens bound to nitrogen or oxygen - Count of hydrogen bond acceptor nitrogens<br>
 * A hydrogen bond acceptor nitrogen is an atom that matches the
 * following SMARTS pattern (from Daylight website):<br>
 * [*H](~[#7,#8])
 * Note that hydrogens are not explicitly considered "atoms" in SMILES

 * @author Marta Enciso. La Trobe University. 2014
 ****************************************************************/
public class HBD extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(HBD.class);
    public static final String DESC_KEY = "HBD";

    public static final String [] SMARTS=
    {
    	"[#7,#8;H1]",
    	"[#7,#8;H2]",
    	"[#7,#8;H3]",
    };
    public HBD()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Count of hydrogens bound to oxygen or nitrogen";
    }

    /**************************************************************
     * Returns the count of hydrogens bound to oxygen or nitrogen<br>
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
    	double result = 0.0;
    	for( int i=0; i < SMARTS.length; ++i )
    	{
    		ATParam atParam = new ATParam();
    		if( !atParam.addSmarts(SMARTS[i]) )
    		{
    			throw new DescriptorException(
    					"Invalid SMARTS pattern: " + SMARTS[i]);
    		}

    		Vector matches = atParam.matchList(new Mol(jMol));
    		if( matches == null )
    		{
    			throw new DescriptorException(
    				"Error matching SMARTS to calculate HBD");
    		}
    		else
    		{
    			result+=(i+1)*matches.size();
    		}
    	}
    	return result;
    }
}
