package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;

/*********************************************************
 * MDE34, MDE (Molecular Distance Edge) with i=3, j=4. 
 ****************************************************************/
public class MDE34 extends MDE
{
    static Logger logger = Logger.getLogger(MDE34.class);
    public static final String DESC_KEY = "MDE34";

    public MDE34()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Molecular Distance Edge between all tertiary and quaternary carbons";
    }

    /**************************************************************
     * Returns the Molecular Distance Edge value for this molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
/*        System.out.println(" MDE-34 "+(getMDE(jMol,3,4))); */
        return getMDE(jMol,3,4);
    }
}
