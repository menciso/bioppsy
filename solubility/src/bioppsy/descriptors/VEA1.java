package bioppsy.descriptors;

//log4j
import jmat.data.Matrix;
import jmat.data.matrixDecompositions.EigenvalueDecomposition;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * Reciprocal distance sum Randic-like index, as described in
 * Molecular Descriptors for Chemoinformatics, Volume 41 (2 Volume Set)
 * By Roberto Todeschini, Viviana Consonni

 ****************************************************************/
public class VEA1 extends Topological
{
    static Logger logger = Logger.getLogger(VEA1.class);
    public static final String DESC_KEY = "VEA1";

    public VEA1()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "VAA1 - sum of positive eigenvalues of A";
    }
    
    /**************************************************************
     * Returns the descriptor for this molecule.
     * @throws DescriptorException 
     **************************************************************/
    public double getDoubleValue(JOEMol jMol) throws DescriptorException
    {
    	double result = getVEA1(jMol);
    	return result;
    }

    /*********************************************************
     * Reciprocal distance sum Randic-like index, as described in
     * Molecular Descriptors for Chemoinformatics, Volume 41 (2 Volume Set)
     * By Roberto Todeschini, Viviana Consonni
     ****************************************************************/
    public double getVEA1(JOEMol jMol)
        throws DescriptorException
    {
        EigenvalueDecomposition eigDec = getAdjDecomposition(jMol);
        Matrix V = eigDec.getV(); // Largest eigenvalue is always the last one!!
        double result=0.0;
        int isize=V.getColumnDimension(),isize1=isize-1;

        // VED1
        for (int i = 0; i < isize; i++)
        	result+=V.get(i,isize1);       
        result=Math.abs(result);
        
        return result;
    }
}
