package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.util.iterator.AtomIterator;
import joelib.desc.*;

/*********************************************************
 * CTDH, the count of donatable hydrogens. Counts the
 * number of hydrogen atoms that are not connected to
 * Carbon.
 ****************************************************************/
public class CTDH extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(CTDH.class);
    public static final String DESC_KEY = "CTDH";

    public CTDH()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Count of donatable Hydrogens";
    }

    /**************************************************************
     * Returns the count of donatable hydrogens for the molecule 
     * which is the number of hydrogens in the molecule not
     * attached to a carbon atom.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        int count = 0;
        JOEAtom atom;
        
        AtomIterator ai = jMol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            if( !atom.isCarbon() && !atom.isHydrogen() && !atom.isSulfur())
            {
                count += atom.implicitHydrogenCount();
            }
        }
/*        System.out.println(" CTDH "+count); */
        return count;
    }
}
