package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * GRAV6, the  cube root of the gravitational index of the given molecule.
 * The gravitational index is a sum over all heavy atom pairs
 * in a molecule and is computed as the product of the
 * atom masses divided by the square of the distance between them.
 * <br>grav = sum((mi*mj)/rij^2) <br>
 * This descriptor is taken from:
 * Katritzky, A. R. et al.
 * J.Phys.Chem 1996, 100, 10400-10407<br>
 * This article is avaliable as 
 * <a href="../../../reference/GRAV.pdf">GRAV.pdf</a>
 ****************************************************************/
public class GRAV6 extends GRAV
{
    static Logger logger = Logger.getLogger(GRAV6.class);
    public static final String DESC_KEY = "GRAV6";

    public GRAV6()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return  "The cube root of the gravitational index of the given molecule." +
                " The gravitational index is a sum over all heavy atom pairs" +
                " in a molecule and is computed as the product of the" +
                " atom masses divided by the square of the distance between them." +
                " grav = sum((mi*mj)/rij^2)";
    }

    /**************************************************************
     * Returns the cube root of the Gravitational Index of the 
     * given molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
/*        System.out.println(" GRAV-6 "+Math.pow(getGrav(jMol),1/3.0)); */
        return Math.pow(getGrav(jMol),1/3.0);
    }
}
