package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.smarts.JOESmartsPattern;
import joelib.desc.*;
//java
import java.util.List;

/*******************************************************************************************
 * The SmartsPattern descriptor is an abstract class. A subclass of SmartsPattern
 * has a single smarts pattern stored as a string that should be specified by overriding
 * the getPattern() method. This descriptor returns a double value which is a count of
 * the number of unique atoms that match the smarts pattern in the given molecule.
 ******************************************************************************************/
public abstract class SmartsPattern extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(SmartsPattern.class);
    public static final String DESC_KEY = "SmartsPattern";

    public SmartsPattern()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Number of SMARTS matching: " + getPattern();
    }

    /**************************************************************
     * Returns the number of unique atoms in the given molecule that
     * match this descriptor's smarts pattern.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        JOESmartsPattern smarts = new JOESmartsPattern();
        if( !smarts.init(getPattern()) )
        {
            throw new DescriptorException("Invalid pattern: " +
                getPattern());
        }
        if( !smarts.match(jMol) )
        { 
            return 0.0;
        } 
        List matches = smarts.getUMapList();
        //System.out.print(jMol.getTitle() + " pattern " + matches.size() + "\n");
        return matches.size();
    }

    /*************************************************************
     * Return the SMARTS pattern that this SmartsPattern descriptor
     * matches as a String.
     *************************************************************/
    public abstract String getPattern();
}
