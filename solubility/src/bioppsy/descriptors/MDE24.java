package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;

/*********************************************************
 * MDE14, MDE (Molecular Distance Edge) with i=2, j=4. 
 ****************************************************************/
public class MDE24 extends MDE
{
    static Logger logger = Logger.getLogger(MDE24.class);
    public static final String DESC_KEY = "MDE24";

    public MDE24()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Molecular Distance Edge between all secondary and quaternary carbons";
    }

    /**************************************************************
     * Returns the Molecular Distance Edge value for this molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
/*        System.out.println(" MDE-24 "+(getMDE(jMol,2,4))); */
        return getMDE(jMol,2,4);
    }
}
