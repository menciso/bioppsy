package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.molecule.JOEAtom;
import joelib.desc.*;
import joelib.util.iterator.AtomIterator;

/*********************************************************
 * AP - Aromatic Proportion<br>
 * Proportion of heavy atoms in the molecule that are in an
 * aromatic ring.
 ****************************************************************/
public class AP extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(AP.class);
    public static final String DESC_KEY = "AP";

    public AP()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Proportion of heavy atoms in the molecule that are in an aromatic ring";
    }

    /**************************************************************
     * Returns the Aromatic Proportion<br>
     * Proportion of heavy atoms in the molecule that are in an
     * aromatic ring.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        int total = 0;
        int aromatic = 0;

        AtomIterator i = jMol.atomIterator();
        while( i.hasNext() )
        {
            JOEAtom atom = i.nextAtom();
            // only count heavy atoms
            if( atom.isHydrogen() )
            {
                continue;
            }
            else
            {
                ++total;
            }
            if( atom.isAromatic() )
            {
                ++aromatic;
            }
        }

        // avoid dividing by 0
        if( total == 0 )
        {
            // if  there are no atoms there are
            // definitely no aromatic atoms
            return 0.0;
        }

        return ( (double) aromatic ) / ( (double) total );
    }
}
