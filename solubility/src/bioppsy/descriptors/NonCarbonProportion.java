package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.molecule.JOEAtom;
import joelib.desc.*;
import joelib.util.iterator.AtomIterator;

/*********************************************************
 * NonCarbonProportion - <br>
 * Proportion of heavy atoms in the molecule that are not
 * Carbon.
 ****************************************************************/
public class NonCarbonProportion extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(NonCarbonProportion.class);
    public static final String DESC_KEY = "NonCarbonProportion";

    public NonCarbonProportion()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Proportion of heavy atoms in the molecule that are not Carbon";
    }

    /**************************************************************
     * Returns the NonCarbonProportion - <br>
     * Proportion of heavy atoms in the molecule that are not
     * Carbon.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        int total = 0;
        int nonCarbon = 0;

        AtomIterator i = jMol.atomIterator();
        while( i.hasNext() )
        {
            JOEAtom atom = i.nextAtom();
            // only count heavy atoms
            if( atom.isHydrogen() )
            {
                continue;
            }
            else
            {
                ++total;
            }
            if( !atom.isCarbon() )
            {
                ++nonCarbon;
            }
        }

        // avoid dividing by 0
        if( total == 0 )
        {
            // if there are no heavy atoms
            // there are definately no C atoms
            return 0.0;
        }

        return ( (double) nonCarbon ) / ( (double) total );
    }
}
