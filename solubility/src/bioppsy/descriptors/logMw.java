package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;

/*********************************************************
 * logMw - logarithm of Molecular Weight
 ****************************************************************/
public class logMw extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(MWT.class);
    public static final String DESC_KEY = "logMw";

    public logMw()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "logarithm of Molecular Weight";
    }

    /**************************************************************
     * Returns the logarithm of the molecular weight of the molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        double Mw, lnMw;
        
        Mw = jMol.getMolWt();
        lnMw = Math.log(Mw);
        
        return lnMw;
    }
}
