package bioppsy.descriptors;

import java.util.Date;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.math.XYZVector;
import joelib.molecule.*;
import joelib.data.JOEElementTable;
import joelib.util.iterator.AtomIterator;
import joelib.desc.*;

/*********************************************************
 * Molecular volume.
 * Returns the molecular volume for this molecule.
 * Not a user option - used by Volume, logVol.
****************************************************************/
public class MolecularVolume extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(MolecularVolume.class);
    public static final String DESC_KEY = "MolecularVolume";

    public MolecularVolume()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "van der Waals volume calculator";
    }
    
    /**************************************************************
     * This class should not be used directly, but only through
     * a child class such as Volume or logVol. They have their own
     * versions of getDoubleValue, which override this one.
     ***************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        logger.warn("Using superclass MolecularVolume, returning value of 0.0");
        return 0.0;
    }

    /****************************************************************
     * Returns the molecular volume of the given molecule.
     * The volume is determined by the number of voxels enclosed by the
     * van der Waals radii of the atoms in the molecule.
     * @throws IOException 
     ****************************************************************/
    public double getMolecularVolume(JOEMol jMol)
    {

        // maximum extents of molecule
        double [] Max = new double [3];
        double [] Min = new double [3];
               
        double radius, distsq, radsq;
        double resolution, VoxVol;
        
        long NumVox;
        
        int i,j,k,l;
        
        int [] N = new int [3];
        int [] c = new int [3];
        double [] iCoord = new double [3];
        double [] dsq = new double [3];
// Initialise variables     
        for (k=0; k<3; k++)
        {
        	Max[k]=-9999.9;
        	Min[k]=9999.9;
        }
        
        
        JOEAtom atom;
        AtomIterator ai;
        // determine the molecular extents
        ai = jMol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            radius = JOEElementTable.instance().getVdwRad(atom.getAtomicNum());
            iCoord[0]=atom.x();iCoord[1]=atom.y();iCoord[2]=atom.z();
            for (k=0;k<3;k++)
            {
            	double cPlus=iCoord[k]+radius;
            	double cMinus=iCoord[k]-radius;
            	if(Min[k]>cMinus)
            		Min[k]=cMinus;
            	if(Max[k]<cPlus)
            		Max[k]=cPlus;
            }
        }
        
        resolution = 0.10;
        for (k=0;k<3;k++)
        {
        	N[k]=(int) (1 + Math.round((Max[k]-Min[k])/resolution));
        	c[k]=0;
        	dsq[k]=0.0;
        }
        boolean [][][] countVox = new boolean [N[0]] [N[1]] [N[2]] ; // false by default
        for (i=0;i<N[0];i++)
        {
        	for (j=0;j<N[1];j++)
        	{
        		for (k=0;k<N[2];k++)
        			countVox[i][j][k]=false;
        	}
        }
        i=0;j=1;k=2;

        VoxVol = Math.pow(resolution,3);
        NumVox = 0;

        //System.out.print(jMol.getTitle()+ " --" + N[0] +  " " + N[1] + " " + N[2] + " z \n");
        ai = jMol.atomIterator();
        while (ai.hasNext())
        {
        	atom = ai.nextAtom();
            radius = JOEElementTable.instance().getVdwRad(atom.getAtomicNum());
            radsq = Math.pow(radius,2);
            iCoord[0]=atom.x()-Min[i];iCoord[1]=atom.y()-Min[j];iCoord[2]=atom.z()-Min[k];
            for(c[i]=0;c[i]<N[i];c[i]++)
        	{
            	dsq[i]=Math.pow(iCoord[i] - c[i]*resolution,2);
                for(c[j]=0;c[j]<N[j];c[j]++)
            	{
                	dsq[j]=Math.pow(iCoord[j] - c[j]*resolution,2);
                	for(c[k]=0;c[k]<N[k];c[k]++)
                	{
                		if(countVox[c[i]][c[j]][c[k]]) // Do not count a voxel twice
                			continue;
                		dsq[k]=Math.pow(iCoord[k] - c[k]*resolution,2);
                		distsq = 0;
                		for (l=0;l<3;l++)
                			distsq+=dsq[l];
                		if(distsq < radsq)
                		{
                			++NumVox;
                			countVox[c[i]][c[j]][c[k]]=true;
                			//System.out.print(c[i] + " "+ c[j] + " " + c[k]  + "  ,  " + radius + " , " + Math.sqrt(distsq) + "\n");
                		}
                	}
            	}
            	
            }
       }

        double vol;
        vol=NumVox * VoxVol;
        //System.out.print(jMol.getTitle()+ " --" + NumVox + "  y " + NumVox*VoxVol + "  y " + VoxVol + " y \n");
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
         
        System.out.println(" Date "+dateFormat.format(date)+" Volume "+vol);
        
        return vol;
    }

}
