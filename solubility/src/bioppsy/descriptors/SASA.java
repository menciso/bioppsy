package bioppsy.descriptors;

import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;
import bioppsy.descriptors.LCPO;
import org.apache.log4j.Logger;

/*********************************************************
 * Van Der Waals Surface Area descriptor
 * author: Marta Enciso, La Trobe University
 ****************************************************************/

public class SASA extends LCPO
{
  static Logger logger = Logger.getLogger(Volume.class);
  public static final String DESC_KEY = "SASA";

  public SASA()
  {
      descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
              DescriptorInfo.TYPE_NO_COORDINATES, null,
              "joelib.desc.result.DoubleResult");
  }

  /*********************************************************
   * Returns a string that describes this Descriptor
   ********************************************************/
  public String getTextDescription()
  {
      return "Solvent Accessible Surface Area of the molecule";
  }

  /**************************************************************
   * Returns the van der Waals surface for this molecule, using superclass LCPO
 * @throws DescriptorException 
   **************************************************************/
  public double getDoubleValue(JOEMol jMol) throws DescriptorException
  {
      return getSASAsurface(jMol);
  }
}
