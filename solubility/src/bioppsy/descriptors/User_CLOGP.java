package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.Mol;



//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * Water-octanol partition coefficient
 ****************************************************************/
public class User_CLOGP extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(RNCG.class);
    public static final String DESC_KEY = "User_CLOGP";

    public User_CLOGP()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "User supplied clogP - provided in molecule input/";
    }

    /**************************************************************
     * Returns the clogP of the given molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        //read the CLOGP from the Mol class
        //jMol needs to be type cast to Mol for this to work
        
        /*System.out.println(jMol.getTitle() + "User CLOGP "+((Mol)jMol).getCLOGP()); */ 
        
         return Double.parseDouble(((Mol)jMol).getCLOGP());
    }
}
