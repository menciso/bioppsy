package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.Mol;



//JOELib
import joelib.molecule.*;
import joelib.desc.*;


public class User_descriptor2 extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(User_descriptor2.class);
    public static final String DESC_KEY = "User_descriptor2";

    public User_descriptor2()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Descriptor2 - provided in molecule input";
    }

    /**************************************************************
     * Returns the value of the descriptor
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {   
    	//System.out.print(jMol.getTitle() + " Desc2 " + Double.parseDouble(((Mol)jMol).getdescriptor2()) + "\n");
        return Double.parseDouble(((Mol)jMol).getdescriptor2());
    }
}