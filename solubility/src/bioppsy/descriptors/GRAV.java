package bioppsy.descriptors;

//log4j
import jmat.data.Matrix;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * GRAV, the gravitational index of the given molecule.
 * The gravitational index is a sum over all heavy atom pairs
 * in a molecule and is computed as the product of the
 * atom masses divided by the square of the distance between them.
 * <br>grav = sum((mi*mj)/rij^2) <br>
 * This descriptor is taken from:
 * Katritzky, A. R. et al.
 * J.Phys.Chem 1996, 100, 10400-10407<br>
 * This article is available as 
 * <a href="../../../reference/GRAV.pdf">GRAV.pdf</a>
 ****************************************************************/
public class GRAV extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(GRAV.class);
    public static final String DESC_KEY = "GRAV";

    public GRAV()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /**************************************************************
     * Returns the Gravitational Index of the given molecule.
     * This superclass is used by GRAV6
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        logger.warn("Using superclass GRAV, returning value of 0.0");
        return 0.0;
    }

    /**************************************************************
     * Returns the gravitational index of the given molecule.
     * The gravitational index is a sum over all heavy atom pairs
     * in a molecule and is computed as the product of the
     * atom masses divided by the square of the distance between them.
     * <br>grav = sum((mi*mj)/rij^2) <br>
     * This descriptor is taken from:
     * Katritzky, A. R. et al.
     * J.Phys.Chem 1996, 100, 10400-10407<br>

     *****************************************************************/
    public double getGrav(JOEMol jMol)
/*************************************************************************
 *   Sum is over ALL pairs of heavy atoms, NOT just those that are bonded 
 *************************************************************************/
    {
        double grav = 0.0;
        int icount, jcount;
        int iIndex,jIndex;
        JOEAtom ai,aj;
        double mi,mj;
        int[] heavy=Distances.getHeavyAtoms(jMol);
        Matrix distances = Distances.getHeavyReciprocalSquareDistanceMatrix(jMol);
        int n=distances.getColumnDimension();
        for( icount = 0; icount < n; ++icount )
        {
        	iIndex=heavy[icount]+1;
            ai=jMol.getAtom(iIndex);
            mi=ai.getAtomicMass();
            for( jcount = icount + 1; jcount < n; ++jcount )
            {
            	jIndex=heavy[jcount]+1;
                aj=jMol.getAtom(jIndex);
                mj=aj.getAtomicMass();
                grav += (mi*mj)*distances.get(icount,jcount);
            }
        }
        return grav;
    }
}
