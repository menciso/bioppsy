package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * Number of Atoms in the molecule.
 ****************************************************************/
public class NumberOfAtoms extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(NumberOfAtoms.class);
    public static final String DESC_KEY = "NumberOfAtoms";

    public NumberOfAtoms()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Number of atoms in the molecule";
    }

    /**************************************************************
     * Returns the number of atoms in the molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        return jMol.numAtoms();
    }
}
