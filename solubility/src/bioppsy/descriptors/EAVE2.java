package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
import joelib.desc.result.*;

/*********************************************************
 * EAVE2 is the mean of the Electrotopological State indices
 * for each heteroatom in the molecule (E-State).
 * The E-State index is defined in:<br>
 * Lowell H Hall 
 * J. Chem. Inf. Comput. Sci. 1995, 35, 1039-1045<br>
 * This article is avaliable as 
 * <a href="../../../reference/ESTATE.pdf">ESTATE.pdf</a> in docs/reference.
 ****************************************************************/
public class EAVE2 extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(EAVE2.class);
    public static final String DESC_KEY = "EAVE2";

    public EAVE2()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "The mean of the Electrotopological State indices for" +
               " each atom in the molecule";
    }

    /**************************************************************
     * Return EAVE2, the mean of the Electrotopological State indices
     * for each hetero-atom in the molecule (E-State).
     * The E-State index is defined in:<br>
     * Lowell H Hall 
     * J. Chem. Inf. Comput. Sci. 1995, 35, 1039-1045<br>
     * This article is available as 
     * <a href="../../../reference/ESTATE.pdf">ESTATE.pdf</a> in docs/reference.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        double ave = 0; // average eState
        int j = 0;
        
        String eStateKey = "Electrotopological_state_index";
        Descriptor descriptor = DescriptorFactory.getDescriptor(eStateKey);
        if( descriptor == null )
        {
            throw new DescriptorException(
                "Descriptor " + eStateKey + " could not be initialised");
        }
        AtomDynamicResult result = (AtomDynamicResult) descriptor.calculate(jMol);

        // Iterate through all resulting atoms
        // note that atomic indices start at 1
        for( int i=1; i <= result.length(); ++i )
        {
            if((!jMol.getAtom(i).isCarbon()) && (!jMol.getAtom(i).isHydrogen())) {
/*                System.out.println(i+" "+jMol.getAtom(i).getAtomicNum())); */
                j++; 
                ave += result.getDoubleValue(i);
            }
        }

/*        return ave/result.length(); */
        if(j!=0) {
        //System.out.println(" EAVE-2 "+(ave/j)); 
        return ave/j;
        }
        return 0;
    }
}
