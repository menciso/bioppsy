package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * logVol returns the logarithm of the van der Waals volume of the molecule.
 ****************************************************************/
public class logVol extends MolecularVolume
{
    static Logger logger = Logger.getLogger(Volume.class);
    public static final String DESC_KEY = "logVol";

    public logVol()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "logarithm of the van der Waals volume of the molecule";
    }

    /**************************************************************
     * Returns the logarithm of the volume for this molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {    
        
        double vol, logV;
        
        vol = getMolecularVolume(jMol);
        logV = Math.log(vol);
        
        return (logV);
    }
}
