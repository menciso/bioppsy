package bioppsy.descriptors;

//log4j
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;
import joelib.util.iterator.AtomIterator;

import org.apache.log4j.Logger;

/*********************************************************
 * Volume returns the van der Waals volume of the molecule.
 ****************************************************************/
public class Rgyr extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(Rgyr.class);
    public static final String DESC_KEY = "Rgyr";

    public Rgyr()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Radius of gyration of the molecule";
    }

    /**************************************************************
     * Returns the radius if gyration of the molecule
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        return getRgyr(jMol);
    }

	private double getRgyr(JOEMol jMol) {
		double rgyr=0.0;
		double cmx=0.0, cmy=0.0,cmz=0.0;
		double tmp=0.0;
		JOEAtom atom;
		AtomIterator ai = jMol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            cmx+=atom.getX();cmy+=atom.getY();cmz+=atom.getZ();
        }
        cmx/=jMol.numAtoms();cmy/=jMol.numAtoms();cmz/=jMol.numAtoms();
		ai = jMol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            tmp=atom.getX()-cmx;rgyr+=Math.pow(tmp, 2);
            tmp=atom.getY()-cmy;rgyr+=Math.pow(tmp, 2);
            tmp=atom.getZ()-cmz;rgyr+=Math.pow(tmp, 2);
        }
        rgyr=Math.pow(rgyr/jMol.numAtoms(), 0.5);
		return rgyr;
	}
}
