package bioppsy.descriptors;

//log4j
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * Atomic charge weighted positive surface area (PPSA3)
 * using Polar Surface Area by LCPO
 * Note that charge of H attached to heavy atoms is summed to the
 * heavy atom charge (see getUnitedCharge)
 * @author Marta Enciso, La Trobe University 
 ****************************************************************/
public class PPSA3 extends LCPO
{
    static Logger logger = Logger.getLogger(PPSA3.class);
    public static final String DESC_KEY = "PPSA3";

    public PPSA3()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Atomic charge weighted positive surface area based on LCPO";
    }

    /**************************************************************
     * Returns PPSA3
     * Negatively charged atoms<>1, Positively Charged atoms <>2
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
    throws DescriptorException
    {
    double result = getAtomicWeightedSurface(jMol, 2);
    return result;
    }

}
