package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;

/*********************************************************
 * MWT - Molecular Weight
 ****************************************************************/
public class MWT extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(MWT.class);
    public static final String DESC_KEY = "MWT";

    public MWT()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Molecular Weight";
    }

    /**************************************************************
     * Returns the molecular weight of the molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
    	//System.out.print(jMol.getMolWt() + "\n");
        return jMol.getMolWt();
    }
}
