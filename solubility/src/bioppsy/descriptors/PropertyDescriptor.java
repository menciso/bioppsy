package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.data.JOEKernel;
import joelib.util.JOEProperty;
import joelib.util.JOEPropertyHelper;
import joelib.desc.*;
import joelib.desc.result.*;

//java
import java.util.Map;


/***************************************************************
 * A PropertyDescriptor is just like a SimpleDoubleDesc
 * except the method getDouble() can throw a DescriptorException
 ****************************************************************/
public abstract class PropertyDescriptor implements Descriptor
{
    static Logger logger = Logger.getLogger(PropertyDescriptor.class);
    public static final String DESC_KEY = "PropertyDescriptor";
    public DescriptorInfo descInfo;
    public static final boolean PROPERTY_DESCRIPTOR = true;

    public PropertyDescriptor()
    {
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Description not available";
    }

    /****************************************************************
     *  Returns the double value. Throws a DescriptorException on error.
     ******************************************************************/
    public abstract double getDoubleValue(JOEMol mol) throws DescriptorException;

    public DescriptorInfo getDescInfo()
    {
        return descInfo;
    }

    /**
     * Gets the descriptor description.
     *
     * @return   the descriptor description
     */
    public DescDescription getDescription()
    {
        return new DescDescription(descInfo.getDescriptionFile());
    }

    public JOEProperty[] acceptedProperties()
    {
        return null;
    }

    /**
     * Calculate descriptor for this molecule. If no DescResult or Map supplied, get a
     * DescResult, and give null for the Map.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol) throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, null);
    }

    /**
     * Calculate descriptor for this molecule. If no DescResult supplied, get a DescResult
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, Map properties)
        throws DescriptorException
    {
        DescResult result = ResultFactory.instance().getDescResult(descInfo.getName());

        return calculate(mol, result, properties);
    }

    /**
     * Calculate descriptor for this molecule.  If no Map supplied, Map value is null.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult)
        throws DescriptorException
    {
        return calculate(mol, descResult, null);
    }

    /**
     * Calculate descriptor for this molecule.
     *
     * It should be faster, if we can can use an already initialized result class,
     * because this must not be get by Java reflection. Ensure that you will clone
     * this result class before you store these results in molecules, or the next molecule will
     * overwrite this result.
     *
     * @param mol                      molecule for which this descriptor should be calculated
     * @param descResult               the descriptor result class in which the result should be stored
     * @param initData                 initialization properties
     * @return                         the descriptor calculation result for this molecule
     * @exception DescriptorException  descriptor calculation exception
     */
    public DescResult calculate(JOEMol mol, DescResult descResult,
        Map properties) throws DescriptorException
    {
        if (!(descResult instanceof DoubleResult))
        {
            logger.error(descInfo.getName() + " result should be of type " +
                DoubleResult.class.getName() + " but it's of type " +
                descResult.getClass().toString());
        }

        // check if the init type is correct
        if (!initialize(properties))
        {
            return null;
        }

        DoubleResult result = (DoubleResult) descResult;
        Double doubleValue = new Double(getDoubleValue(mol));
        if( doubleValue.isNaN() )
        {
            throw new DescriptorException("Double Value NaN");
        }
        result.setDouble(doubleValue.doubleValue());
        result.addCMLProperty(JOEKernel.instance().getKernelID());

        return result;
    }

    /**
     * Clear descriptor calculation method for a new molecule.
     */
    public void clear()
    {
    }

    /**
     * Initialize descriptor calculation method for all following molecules.
     *
     * @param initData  initialization properties
     * @return <tt>true</tt> if the initialization was successfull
     */
    public boolean initialize(Map properties)
    {
        if (!JOEPropertyHelper.checkProperties(this, properties))
        {
            logger.error("Empty property definition or missing property entry.");

            return false;
        }

        return true;
    }

    /**
     * Test the implementation of this descriptor.
     *
     * @return <tt>true</tt> if the implementation is correct
     */
    public boolean testDescriptor()
    {
        return true;
    }
}
