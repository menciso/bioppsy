package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.desc.*;
import joelib.molecule.JOEMol;
import joelib.molecule.JOEBond;
import joelib.util.iterator.BondIterator;

/**
 * Calculates the number of double bonds in a molecule
 */
public class NumberDoubleBonds extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(NumberDoubleBonds.class);
    public static final String DESC_KEY = "NumberDoubleBonds";

    public NumberDoubleBonds()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Number of double bonds";
    }

    /******************************************************
     * Returns the number of double bonds in the given molecule
     *******************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        int count = 0;
        JOEBond jBond;

        BondIterator bi = jMol.bondIterator();
        while( bi.hasNext() )
        {
            jBond = bi.nextBond();
            if( (jBond.getBondOrder()==2) && (!jBond.isAromatic()) )
            {
                ++count;
            }
        }
        
/*        System.out.println(" NDB "+count); */
        return count;
    }
}
