package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.Mol;



//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * RNCG, the Relative Negative chage of a molecule.
 * This descriptor is taken from:
 * Stanton, D. T.; Jurs, P. C.
 * Anal. Chem 1990, 62, 2323-2329
 * Avaliable as: <a href="../../../reference/RNCG.pdf">RNCG.pdf</a>
 ****************************************************************/
public class User_RNCG extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(User_RNCG.class);
    public static final String DESC_KEY = "User_RNCG";

    public User_RNCG()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "User Supplied Relative Negative Charge - provided in molecule input";
    }

    /**************************************************************
     * Returns the Relative Negative Charge of the given molecule provided 
     * as part of the initial molecule input.
     * To calculate from charges use class RNCG
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
       //read the RNCG from the Mol class
       //jMol needs to be type cast to Mol for this to work
        
/*        System.out.println(" User_RNCG "+(Double.parseDouble(((Mol)jMol).getRNCG()))); */
        
        return Double.parseDouble(((Mol)jMol).getU_RNCG());
    }
}
