package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
import joelib.desc.result.*;

/*********************************************************
 * JLogP - log of Octanol/Water partition coefficient provided
 * by the JOELib library. 
 ****************************************************************/
public class JLogP extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(JLogP.class);
    public static final String DESC_KEY = "JLogP";

    public JLogP()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "log of Octanol/Water partition coefficient provided by JOELib";
    }

    /**************************************************************
     * Returns logP.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        String logPKey = "LogP";
        Descriptor descriptor = DescriptorFactory.getDescriptor(logPKey);
        if( descriptor == null )
        {
            throw new DescriptorException(
                "Descriptor " + logPKey + " could not be initialised");
        }
        DoubleResult result = (DoubleResult) descriptor.calculate(jMol);
        if( result == null )
        {
            throw new DescriptorException(
                "Descriptor " + logPKey + " could not be calculated");
        }
        /*System.out.print("JlogP " + result.getDouble()); */
        return result.getDouble();
    }
}
