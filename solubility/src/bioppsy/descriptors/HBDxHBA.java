package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
import joelib.desc.result.*;

/*********************************************************
 * HBDxHBA - product of number of hydrogen bond donors
 * and hydrogen bond acceptors
 ****************************************************************/
public class HBDxHBA extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(HBDxHBA.class);
    public static final String DESC_KEY = "HBDxHBA";

    public HBDxHBA()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "product of number of H bond donors and acceptors";
    }

    /**************************************************************
     * Returns product of number of H bond donors and acceptors
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        String hbdKey = "HBondDonors";
        String hbaKey = "HBondAcceptors";
        double hbd, hba;

        Descriptor descriptor = DescriptorFactory.getDescriptor(hbdKey);
        if( descriptor == null )
        {
            throw new DescriptorException(
                "Descriptor " + hbdKey + " could not be initialised");
        }
        DoubleResult result = (DoubleResult) descriptor.calculate(jMol);
        if( result == null )
        {
            throw new DescriptorException(
                "Descriptor " + hbdKey + " could not be calculated");
        }
        hbd = result.getDouble();

        descriptor = DescriptorFactory.getDescriptor(hbaKey);
        if( descriptor == null )
        {
            throw new DescriptorException(
                "Descriptor " + hbaKey + " could not be initialised");
        }
        result = (DoubleResult) descriptor.calculate(jMol);
        if( result == null )
        {
            throw new DescriptorException(
                "Descriptor " + hbaKey + " could not be calculated");
        }
        hba = result.getDouble();
        
/*        System.out.println(" HDB*HDA "+hbd*hba); */
        
        return hbd*hba;
    }
}
