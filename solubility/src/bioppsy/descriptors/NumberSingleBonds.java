package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.desc.*;
import joelib.molecule.JOEMol;
import joelib.molecule.JOEBond;
import joelib.util.iterator.BondIterator;

/**
 * Calculates the number of single bonds in a molecule
 */
public class NumberSingleBonds extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(NumberSingleBonds.class);
    public static final String DESC_KEY = "NumberSingleBonds";

    public NumberSingleBonds()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Number of single bonds (Excluding bonds to hydrogen)";
    }

    /******************************************************
     * Returns the number of single bonds in the given molecule (excluding bonds to hydrogen)
     *******************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        int count = 0;
        JOEBond jBond;

        BondIterator bi = jMol.bondIterator();
        while( bi.hasNext() )
        {
            jBond = bi.nextBond();
            if( (jBond.getBondOrder()==1) && 
                    (!jBond.isAromatic()) && 
                    (!jBond.getBeginAtom().isHydrogen()) && 
                    (!jBond.getEndAtom().isHydrogen()) )
            {
                ++count;
            }
        }
        
/*        System.out.println(" NSB "+count); */
        
        return count;
    }
}
