package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * GEOM1 is a Descriptor that calculates the moment of inertia
 * of a molecule around the x-axis. Note that there is no absolute
 * frame of reference for a molecule and hence GEOM may differ
 * depending on the method used for determining the molecule's
 * 3D coordinates.
 ****************************************************************/
public class GEOM1 extends GEOM
{
    static Logger logger = Logger.getLogger(GEOM1.class);
    public static final String DESC_KEY = "GEOM1";

    public GEOM1()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "First principal moment of inertia of the molecule";
    }

    /**************************************************************
     * Returns the first geometric moment for this molecule.
     * GEOM sorts the 3 moments of inertia from smallest to largest.
     * Largest, equals 3rd value, is 'x'
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
/*        System.out.println(" GEOM-1 "+getMomentOfInertia(jMol,3)); */
        return getMomentOfInertia(jMol,3);
    }
}
