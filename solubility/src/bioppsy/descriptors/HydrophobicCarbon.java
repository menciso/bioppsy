package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.Mol;



import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;
import joelib.desc.*;

/**
 * Calculates the number of hydrophobic carbons in the molecule.
 * A hydrophobic carbon has been chosen as being an sp3 or sp2 hybridised
 * aliphatic carbon atom that does not have a heteroatom (any atom other
 * than carbon or hydrogen) attached to itself, attached to a neighbouring
 * carbon atom, or attached to a neighbouring carbon's neighbour.
 * Moreover, sp2 carbons that are in a ring are excluded.
 */
public class HydrophobicCarbon extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(HydrophobicCarbon.class);
    public static final String DESC_KEY = "HydrophobicCarbon";

    public HydrophobicCarbon()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return " A hydrophobic carbon has been chosen as being an sp3 or sp2 hybridised" +   
               " aliphatic carbon atom that does not have a heteroatom (any atom other" +
               " than carbon or hydrogen) attached to itself, attached to a neighbouring" +
               " carbon atom, or attached to a neighbouring carbon's neighbour." +
               " Moreover, sp2 carbons that are in a ring are excluded.";
    }

    /******************************************************
     * Returns the number of hydrophobic carbons in the
     * given molecule.
     *******************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        Mol mol = new Mol(jMol);

        return mol.numHydrophobic();
    }
}
///////////////////////////////////////////////////////////////////////////////
//  END OF FILE.
///////////////////////////////////////////////////////////////////////////////
