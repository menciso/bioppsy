package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.Mol;



//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * Gas phase solvation energy from Gaussian. To be added by user
 * 
 ****************************************************************/
public class User_descriptor3 extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(User_descriptor3.class);
    public static final String DESC_KEY = "User_descriptor3";

    public User_descriptor3()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Descriptor3 - provided in molecule input";
    }

    /**************************************************************
     * Returns the value of the descriptor
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {        
        return Double.parseDouble(((Mol)jMol).getdescriptor1());
    }
}