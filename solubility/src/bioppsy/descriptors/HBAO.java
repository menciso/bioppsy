package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Mol;



//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
//java
import java.util.Vector;

/*********************************************************
 * HBondAcceptors oxygens- Count of hydrogen bond acceptor atoms<br>
 * A hydrogen bond acceptor oxygen is an atom that matches the
     * following SMARTS pattern (from Daylight website):<br>
     * [O;!$([*+1,*+2,*+3])]
     * 
 * @author Marta Enciso. La Trobe University. 2014

 ****************************************************************/
public class HBAO extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(HBAO.class);
    public static final String DESC_KEY = "HBAO";

    public static final String SMARTS = 
    	"[O;!$([*+1,*+2,*+3])]";

    public HBAO()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Count of hydrogen bond acceptor oxygens";
    }

    /**************************************************************
     * Returns the count of hydrogen bond acceptor atoms<br>
     * A hydrogen bond acceptor is an atom that matches the
     * following SMARTS pattern (from Daylight website):<br>
     * [O;!$([*+1,*+2,*+3])]

     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        ATParam atParam = new ATParam();
        if( !atParam.addSmarts(SMARTS) )
        {
            throw new DescriptorException(
               "Invalid SMARTS pattern: " + SMARTS);
        }

        Vector matches = atParam.matchList(new Mol(jMol));
        if( matches == null )
        {
            throw new DescriptorException(
                "Error matching SMARTS to calculate HBAO");
        }
        else
        {
            return matches.size();
        }
    }
}
