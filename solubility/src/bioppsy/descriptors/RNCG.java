package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.util.iterator.AtomIterator;
import joelib.desc.*;

/*********************************************************
 * RNCG, the Relative Negative charge of a molecule.
 * This descriptor is taken from:
 * Stanton, D. T.; Jurs, P. C.
 * Anal. Chem 1990, 62, 2323-2329
 * Avaliable as: <a href="../../../reference/RNCG.pdf">RNCG.pdf</a>
 ****************************************************************/
public class RNCG extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(RNCG.class);
    public static final String DESC_KEY = "RNCG";

    public RNCG()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Relative Negative Charge = charge of most negative atom / sum of atom negative charges";
    }

    /**************************************************************
     * Returns the Relative Negative Charge of the given molecule.
     * rncg = charge of most negative atom / sum of atom negative charges
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        double lowestCharge = 0.0;
        double sum = 0.0;
        double charge;
        JOEAtom atom;

        AtomIterator ai = jMol.atomIterator();
        while( ai.hasNext() )
        {
            atom = ai.nextAtom();
            charge = atom.getPartialCharge();
            if( charge < 0.0 )
            {
                sum += charge;
                lowestCharge = Math.min(charge,lowestCharge);
            }
        }

        // make sure we are not dividing by 0
        if( sum == 0.0 )
        {
            // return 1.0 here, the highest possible value
            return 1.0;
        }
/*        System.out.println(" RNCG "+(lowestCharge/sum)); */
        return lowestCharge / sum;
    }
}
