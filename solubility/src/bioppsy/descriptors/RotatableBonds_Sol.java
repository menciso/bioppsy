package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
import joelib.desc.result.*;

/*********************************************************
 * RotatableBonds_Sol - number of rotatable bonds. 
 * From joelib documentation:
 * Number of rotatable bonds, where the atoms are heavy atoms with 
 * bond order one and a hybridization which is not one (no sp). 
 * Additionally the bond is a non-ring-bond.Uses KeirShape1.
 * Probably superseded by RB, which seems to do the same thing by 
 * SMARTS pattern matching. 
 ****************************************************************/
public class RotatableBonds_Sol extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(RotatableBonds_Sol.class);
    public static final String DESC_KEY = "RotatableBonds_Sol";

    public RotatableBonds_Sol()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "number of rotatable bonds (calculated by JOELib)";
    }

    /**************************************************************
     * Returns the number of rotatable bonds
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        String rotatableBondsKey = "Number_of_rotatable_bonds";
        Descriptor descriptor = DescriptorFactory.getDescriptor(rotatableBondsKey);
        if( descriptor == null )
        {
            throw new DescriptorException(
                "Descriptor " + rotatableBondsKey + " could not be initialised");
        }
        IntResult result = (IntResult) descriptor.calculate(jMol);
        if( result == null )
        {
            throw new DescriptorException(
                "Descriptor " + rotatableBondsKey + " could not be calculated");
        }
        
/*        System.out.println("Rotlbonds "+result.getInt()); */
        
        return result.getInt();
    }
}
