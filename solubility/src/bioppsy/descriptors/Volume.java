package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * Volume returns the van der Waals volume of the molecule.
 ****************************************************************/
public class Volume extends MolecularVolume
{
    static Logger logger = Logger.getLogger(Volume.class);
    public static final String DESC_KEY = "Volume";

    public Volume()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "van der Waals volume of the molecule";
    }

    /**************************************************************
     * Returns the volume for this molecule, using superclass MolecularVolume.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
    {
        return getMolecularVolume(jMol);
    }
}
