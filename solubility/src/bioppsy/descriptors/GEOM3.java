package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * GEOM3 is a Descriptor that calculates the moment of inertia
 * of a molecule around the z-axis. Note that there is no absolute
 * frame of reference for a molecule and hence GEOM may differ
 * depending on the method used for determining the molecule's
 * 3D coordinates.
 ****************************************************************/
public class GEOM3 extends GEOM
{
    static Logger logger = Logger.getLogger(GEOM3.class);
    public static final String DESC_KEY = "GEOM3";

    public GEOM3()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Third principal moment of inertia of the molecule";
    }

    /**************************************************************
     * Returns the third geometric moment for this molecule.
     * GEOM sorts the 3 moments of inertia from smallest to largest.
     * Smallest, which is 1st value, is 'z'
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
/*        System.out.println(" GEOM-3 "+getMomentOfInertia(jMol,1)); */
        return getMomentOfInertia(jMol,1);
    }
}
