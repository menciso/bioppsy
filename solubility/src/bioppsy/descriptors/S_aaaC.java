package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.desc.*;

/*******************************************************************************************
 * S_aaaC returns the number aromatic carbons in the given molecule.
 * Passes a SMARTS string describing aromatic carbons to the abstract 
 * class SmartsPattern for calculation of number of matches
 ******************************************************************************************/
public class S_aaaC extends SmartsPattern
{
    static Logger logger = Logger.getLogger(S_aaaC.class);
    public static final String DESC_KEY = "S_aaaC";

    public S_aaaC()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*************************************************************
     * Return the SMARTS pattern that this SmartsPattern descriptor
     * matches as a String.
     *************************************************************/
    public String getPattern()
    {
        return "c";
    }
}
