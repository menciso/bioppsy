package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.ATParam;
import bioppsy.Mol;



//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
//java
import java.util.Vector;

/*********************************************************
 * Rotatable Bonds<br>
 * The following SMARTS patterns are defined as rotatable:<br>
 * [!X1]-,=[$([C;X4])]-&!@[$([C;X4])]-[!X1]<br>
 * [!X1]:c-&!@[$([C;X4])]-[!X1]<br>
 * [!X1]-,=C-&!@[$([N;X4])]-[!X1]<br>
 * [!X1]-[$([C;X4])]-&!@[$([N;X3])]-[!X1]<br>
 * [!X1]-[$([C;X4])]-&!@[$([O;X2])]-[!X1]
 * Ref 21. in
 * ESOL: Estimating Aqueous Solubility Directly from Molecular Structure
 * John S. Delaney
 * J. Chem. Inf. Comput. Sci. (2004) 44(3) 1000-1005
 ****************************************************************/
public class RB extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(RB.class);
    public static final String DESC_KEY = "RB";

    public static final String[] SMARTS = 
    {
        "[!X1]-,=[$([C;X4])]-&!@[$([C;X4])]-[!X1]",
        "[!X1]:c-&!@[$([C;X4])]-[!X1]",
        "[!X1]-,=C-&!@[$([N;X4])]-[!X1]",
        "[!X1]-[$([C;X4])]-&!@[$([N;X3])]-[!X1]",
        "[!X1]-[$([C;X4])]-&!@[$([O;X2])]-[!X1]"
    };

    public RB()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Number of Rotatable Bonds (matching list of SMARTS patterns)";
    }

    /**************************************************************
     * Returns the number of Rotatable Bonds.<br>
     * The Following SMARTS patterns are defined as rotatable:<br>
     * [!X1]-,=[$([C;X4])]-&!@[$([C;X4])]-[!X1]<br>
     * [!X1]:c-&!@[$([C;X4])]-[!X1]<br>
     * [!X1]-,=C-&!@[$([N;X4])]-[!X1]<br>
     * [!X1]-[$([C;X4])]-&!@[$([N;X3])]-[!X1]<br>
     * [!X1]-[$([C;X4])]-&!@[$([O;X2])]-[!X1]
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        ATParam atParam = new ATParam();
        for( int i=0; i < SMARTS.length; ++i )
        {
            if( !atParam.addSmarts(SMARTS[i]) )
            {
                throw new DescriptorException(
                    "Invalid SMARTS pattern: " + SMARTS[i]);
            }
        }

        Vector matches = atParam.matchList(new Mol(jMol));
        if( matches == null )
        {
            throw new DescriptorException(
                "Error matching SMARTS to calculate RB");
        }
        else
        {
            return matches.size();
        }
    }
}
