package bioppsy.descriptors;

//log4j
import org.apache.log4j.Logger;

//JOELib
import joelib.molecule.JOEMol;
import joelib.desc.*;
import joelib.desc.result.*;

/*******************************************************************************************
 * Wiener: sum of no. of bonds in shortest path between all (unique) pairs of heavy atoms
 * multiple bonds treated as single bonds
 * can be calculated as half the sum of (diagonal) matrix where 
 * a(i,j) = length of shortest path between atoms i,j (NB: diagonals are zero)
 ******************************************************************************************/
public class Wiener extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(Wiener.class);
    public static final String DESC_KEY = "Wiener";

    public Wiener()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "sum of no. of bonds in shortest path between all (unique) pairs of heavy atoms";
    }

    /**************************************************************
     * returns Wiener: sum of no. of bonds in shortest path between all 
     * (unique) pairs of heavy atoms
     * multiple bonds treated as single bonds
     * can be calculated as half the sum of (diagonal) matrix where 
     * a(i,j) = length of shortest path between atoms i,j (NB: diagonals are zero)
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        // get the shortest distances between all atoms
        // in terms of number of edges on the molecular 
        // graph
        int[][] distanceMatrix = getDistanceMatrix(jMol);
        int i,j;
        int sum = 0;

        // sum up the distance matrix
        for( i = 0; i < distanceMatrix.length; ++i )
        {
            for( j = 0; j < distanceMatrix[i].length; ++j )
            {
                if((!jMol.getAtom(i+1).isHydrogen()) && (!jMol.getAtom(j+1).isHydrogen())) { 
                    sum += distanceMatrix[i][j];
                }
            }
        }

        double wiener = sum;
/*        System.out.println(" Weiner "+(wiener/2.0)); */
        
        return wiener/2.0;
    }

    /***************************************************************************
     * Returns the distance matrix which is a NxN matrix where N is the number
     * of atoms in the given molecule. The (i,j) value in the distance matrix
     * is the minimum number of edges between atom i and j in the molecular graph.
     ***************************************************************************/
    public int[][] getDistanceMatrix(JOEMol jMol)
        throws DescriptorException
    {
        Descriptor descriptor = DescriptorFactory.getDescriptor("Distance_matrix");
        if( descriptor == null )
        {
            logger.error("Descriptor 'DistanceMatrix' could not be initialised");
            return null;
        }
        IntMatrixResult result = (IntMatrixResult) descriptor.calculate(jMol);
        
        return result.value;    
    }
}
