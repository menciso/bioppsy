package bioppsy.descriptors;

//log4j
import jmat.data.Matrix;
import jmat.data.matrixDecompositions.EigenvalueDecomposition;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * Reciprocal distance sum Randic-like index, as described in
 * Molecular Descriptors for Chemoinformatics, Volume 41 (2 Volume Set)
 * By Roberto Todeschini, Viviana Consonni

 ****************************************************************/
public class VAD1 extends Topological
{
    static Logger logger = Logger.getLogger(VAD1.class);
    public static final String DESC_KEY = "VAD1";

    public VAD1()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "VAD1 - Largest eigenvalue of distance matrix";
    }
    
    /**************************************************************
     * Returns the descriptor for this molecule.
     * @throws DescriptorException 
     **************************************************************/
    public double getDoubleValue(JOEMol jMol) throws DescriptorException
    {
    	double result = getVAD1(jMol);
    	return result;
    }

    /*********************************************************
     * Reciprocal distance sum Randic-like index, as described in
     * Molecular Descriptors for Chemoinformatics, Volume 41 (2 Volume Set)
     * By Roberto Todeschini, Viviana Consonni
     ****************************************************************/
    public double getVAD1(JOEMol jMol)
        throws DescriptorException
    {

        EigenvalueDecomposition eigDec = getDistDecomposition(jMol);
        Matrix D = eigDec.getD(); // Largest eigenvalue is always the last one!!

        double result=0.0;
        int isize1=D.getColumnDimension()-1;
        //VAD1
        result=D.get(isize1, isize1);

        return result;
    }
}
