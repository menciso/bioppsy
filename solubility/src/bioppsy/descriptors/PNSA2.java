package bioppsy.descriptors;

//log4j
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * Partial negative solvent-accessible surface area multiplied by the negative charge
 * Note that charge of H attached to heavy atoms is summed to the
 * heavy atom charge (see getUnitedCharge)
 * @author Marta Enciso, La Trobe University 
 ****************************************************************/
public class PNSA2 extends LCPO
{
    static Logger logger = Logger.getLogger(PNSA2.class);
    public static final String DESC_KEY = "PNSA2";

    public PNSA2()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "Total charge weighted positive surface area based on LCPO";
    }

    /**************************************************************
     * Returns PNSA2
     * Negatively charged atoms<>1, Positively Charged atoms <>2
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
    throws DescriptorException
    {
    double result = getTotalChargeWeightedSurface(jMol,1); 
    return result;
    }

}
