package bioppsy.descriptors;


//log4j
import org.apache.log4j.Logger;

import bioppsy.Mol;



//JOELib
import joelib.molecule.*;
import joelib.desc.*;

/*********************************************************
 * Polar Surface Area from input file
 * author: Marta Enciso, La Trobe University
 ****************************************************************/
public class User_PSA extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(RNCG.class);
    public static final String DESC_KEY = "User_PSA";

    public User_PSA()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "User supplied PSA - provided in molecule input/";
    }

    /**************************************************************
     * Returns the clogP of the given molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        //read the CLOGP from the Mol class
        //jMol needs to be type cast to Mol for this to work
        
        //System.out.println(jMol.getTitle() + " PSA "+((Mol)jMol).getPSA()); 
        
         return Double.parseDouble(((Mol)jMol).getPSA());
    }
}
