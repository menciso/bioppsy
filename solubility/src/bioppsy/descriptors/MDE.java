package bioppsy.descriptors;

//log4j
import jmat.data.Matrix;
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEAtom;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * The Molecular Distance Edge value for this molecule.
 * This descriptor is calculated between atoms with heavy valence
 * (valence excluding hydrogens) i and heavy valence j.
 * Thus several MDE descriptors are possible and these
 * exist as subclasses of MDE.
 * This descriptor is taken from:<br>
 * Shushen Liu, Chenzhong Cao, and Zhiliang Li, 
 * J. Chem. Inf. Comput. Sci. 1998, 38, 387-394<br>
 * This article is available as 
 * <a href="../../../reference/MDE.pdf">MDE.pdf</a> in docs/reference.
 ****************************************************************/
public class MDE extends PropertyDescriptor
{
    static Logger logger = Logger.getLogger(MDE.class);
    public static final String DESC_KEY = "MDE";

    public MDE()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /**************************************************************
     * Returns the Molecular Distance Edge value for this molecule.
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
        throws DescriptorException
    {
        logger.warn("Using superclass MDE, returning value of 0.0");
        return 0.0;
    }

    /*********************************************************
     * Returns the Molecular Distance Edge value for this molecule.
     * This descriptor is calculated between atoms with heavy valence
     * (valence excluding hydrogens) i and heavy valence j, j >= i.
     * This descriptor is taken from:<br>
     * Shushen Liu, Chenzhong Cao, and Zhiliang Li, 
     * J. Chem. Inf. Comput. Sci. 1998, 38, 387-394<br>
     ****************************************************************/
    public double getMDE(JOEMol jMol, int i, int j)
        throws DescriptorException
    {
        if( (i<0) || (j<0) )
        {
            throw new DescriptorException("MDE calculated before MDE.init() called");
        }

        if( j < i )
        {
            throw new DescriptorException("MDE calculated for j < i");
        }

        int iCount, jCount, iIndex, jIndex;
        // the MDE d must be a double since it gets rather large
        double d = 1.0;
        int n = 0; // number of distances calculated
        JOEAtom atom;

        // get the shortest distances between all atoms
        // in terms of number of edges on the molecular 
        // graph
        Matrix distanceMatrix = Topological.getHeavyTopologicalDistanceMatrix(jMol);
        int[] heavy=Topological.getHeavyAtoms(jMol);
        // For each atom with heavy valence i, 
        // find the distance between it and all
        // atoms with heavy valence j. 
        // Then d is multiplied by this value
        // and n incremented by one

        for( iCount = 0; iCount < heavy.length; ++iCount)
        {
        	iIndex=heavy[iCount]+1;
        	atom=jMol.getAtom(iIndex);
        	
        	if( atom.getHvyValence() != i || !atom.isCarbon())
        		continue;
            if( i == j )    // don't count the same combination twice
                jCount = iCount;
            else
                jCount = 0;
            
            for( ; jCount < heavy.length; ++jCount)
            {
            	jIndex=heavy[jCount]+1;
            	atom=jMol.getAtom(jIndex);
            	
            	if( atom.getHvyValence() != j || !atom.isCarbon())
            		continue;

                if( iCount != jCount )
                {
                    d *= distanceMatrix.get(iCount,jCount);
                    ++n;
                }
            }
        }

        
        if( n == 0.0 || d == 0.0 )
        {
            return 0.0;
        }
 /*     Math.pow(d, 1.0 / (2 * n) ) is the geometric average of the graph theoretical distance */
        double result = n / (Math.pow(d, (1.0 /n )) );
        return result;
    }


}
