package bioppsy.descriptors;

//log4j
import joelib.desc.DescriptorException;
import joelib.desc.DescriptorHelper;
import joelib.desc.DescriptorInfo;
import joelib.molecule.JOEMol;

import org.apache.log4j.Logger;

/*********************************************************
 * Fractional charged partial positive surface area
 * using Polar Surface Area by LCPO
 * @author Marta Enciso, La Trobe University 
 ****************************************************************/
public class FPSA3 extends LCPO
{
    static Logger logger = Logger.getLogger(FPSA3.class);
    public static final String DESC_KEY = "FPSA3";

    public FPSA3()
    {
        descInfo = DescriptorHelper.generateDescInfo(DESC_KEY, this.getClass(),
                DescriptorInfo.TYPE_NO_COORDINATES, null,
                "joelib.desc.result.DoubleResult");
    }

    /*********************************************************
     * Returns a string that describes this Descriptor
     ********************************************************/
    public String getTextDescription()
    {
        return "FPSA3 based on LCPO";
    }

    /**************************************************************
     * Returns Fractional charged partial positive surface area
     * Negatively charged atoms<>1, Positively Charged atoms <>2
     **************************************************************/
    public double getDoubleValue(JOEMol jMol)
    throws DescriptorException
	{
	    double result=0.0;
	    result=getFractionalSurface(jMol, 2, 3);
	    //System.out.print(result + "\n");
	    return result;
	}
}
