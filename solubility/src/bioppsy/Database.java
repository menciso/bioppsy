package bioppsy;

//log4j
import org.apache.log4j.Logger;

import bioppsy.util.IniFileReader;


//sql
import java.sql.Connection;
import java.sql.DriverManager; 
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.io.FileNotFoundException;

/**********************************************************
 *  <h1>Database</h1>
 *
 *  <p>
 *  The Database Class controls all access and connections
 *  to the database. 
 *  </p>
 *
 *  <p>
 *  When a Database object is created it starts a connection to
 *  the database. This connection is held open until the
 *  Database.close() method is called. This method is called
 *  automatically by the garbage collector. 
 * 
 *  <p>
 *  If there is an exception when performing a query or 
 *  an operation an attempt is made to reconnect to the 
 *  database. The operation is retried once.
 *  </p>
 *
 *  <p>
 *  The database driver and connection string are read
 *  from the ini file passed to the constructor. 
 *  </p>
 **********************************************************/
public class Database
{
    private String INI_FILENAME;
    private String driverString = null;
    private String connString = null;
    private Connection conn  = null;
    private Statement stmt = null;

    String[] TABLES = {"MolSet","Algorithm","Result","Parameter","ParameterExtra","Matrix"};
    // This string array needs to match the table names in createDatabase

    // Define a static logger variable so that it references the
    // Logger instance named "Database".
    static Logger logger = Logger.getLogger(Database.class);

    /*****************************************************
     * Creates a Database and connects to the database. The
     * database host, name, user, and password are
     * all read in from the provided iniFilename.
     * If the ini file cannot be read correctly or
     * if a database connection cannot be obtained
     * a FailedConstructorException is thrown.
     * Database tables are created if they do not exist.
     * 
     * Jocelyn is replacing cached with CSV text-file database
     ****************************************************/
    public Database(String iniFilename)
        throws FailedConstructorException
    {
        INI_FILENAME = iniFilename;
        try 
        {
            connectToDatabase();
            createDatabase(); // in case it has not been created
        }
        catch( FileNotFoundException fnfe )
        {
            throw new FailedConstructorException(fnfe.getMessage());
        } 
        catch( ClassNotFoundException cnfe )
        {
            throw new FailedConstructorException(cnfe.getMessage());
        }
        catch( InstantiationException ie )
        {
            throw new FailedConstructorException(ie.getMessage());
        }
        catch( SQLException sqle )
        {
            throw new FailedConstructorException(sqle.getMessage());
        }
        catch( IllegalAccessException iae )
        {
            throw new FailedConstructorException(iae.getMessage());
        }
    }

    /*****************************************************************
     * Connects to the database using the driver and connection string
     * specified in INI_FILENAME. Throws an exception on error.
     ******************************************************************/
    public synchronized void connectToDatabase() 
        throws FileNotFoundException, ClassNotFoundException,
               InstantiationException, SQLException,
               IllegalAccessException
    {
        // read in the database parameters from the
        // database ini file and use them to make string variables
        IniFileReader ifr = new IniFileReader(INI_FILENAME);
        ifr.read();
        connString = ifr.getString("CONN_STRING") + System.getProperty("user.home") + ifr.getString("DATABASE_LOCATION");
        driverString = ifr.getString("DRIVER");
 
        // connect to the database
        Class.forName(driverString).newInstance();
        logger.debug(connString);
        conn = 
            DriverManager.getConnection(connString,"sa","");
    }

    /*******************************************************************
     * Executes the given query in the database. To be used with queries
     * that return results. If an SQLException is received an attempt is
     * made to connect to the database and try the query again.
     ******************************************************************/
    public synchronized ResultSet executeQuery(String query)
        throws SQLException
    {
        try
        {
            stmt = conn.createStatement();
            return stmt.executeQuery(query);
        }
        catch( SQLException sqle )
        {
            return retryExecuteQuery(query);
        }
        catch( NullPointerException npe )
        {
            return retryExecuteQuery(query);
        }
    }

   
	/*************************************************************
	 * Attempts a reconnect to the database and executes the
	 * given query again.
	 *************************************************************/
	public synchronized ResultSet retryExecuteQuery(String query)
	    throws SQLException
	{
	    try
	    {
	        close();
	        connectToDatabase();
	        stmt = conn.createStatement();
	        return stmt.executeQuery(query);
	    }
	    catch( Exception e )
	    {
	        throw new SQLException(e.getMessage());
	    }
	}

 
    /*******************************************************************
     * Executes the given query in the database, without returning a ResultSet.
     * To be used with queries that may or may not return results. If an 
     * SQLException is received an attempt is made to connect to the database 
     * and try the query again.
     ******************************************************************/
    public synchronized boolean execute(String query)
        throws SQLException
    {
        try
        {
            stmt = conn.createStatement();
            return stmt.execute(query);
        }
        catch( SQLException sqle )
        {
            return retryExecute(query);
        }
        catch( NullPointerException npe )
        {
            return retryExecute(query);
        }
    }

    /*************************************************************
     * Attempts a reconnect to the database and executes the
     * given query again.
     *************************************************************/
    public synchronized boolean retryExecute(String query)
        throws SQLException
    {
        try
        {
            close();
            connectToDatabase();
            stmt = conn.createStatement();
            return stmt.execute(query);
        }
        catch( Exception e )
        {
            throw new SQLException(e.getMessage());
        }
    }

    /*******************************************************************
     * Executes the given queries in the database all in one batch. 
     * To be used with queries that may or may not return results. 
     * Returns False if any query fails.
     * Not currently called - available as extension?
     ******************************************************************/
/*    public synchronized boolean executeBatch(String[] queries)
        throws SQLException
    {
        int i;

        try
        {
            stmt = conn.createStatement();
            for( i=0; i < queries.length; ++i )
            {
                stmt.addBatch(queries[i]);
            }
            int[] results = stmt.executeBatch();
            for( i=0; i < results.length; ++i )
            {
                if( results[i] < 0 )
                {
                    return false;
                }
            }
            return true;
        }
        catch( NullPointerException npe )
        {
            logger.error(npe.getMessage());
            return false;
        }
    }
*/
    
    /*******************************************************************
     * Closes the connection to the database.
     ******************************************************************/
    protected void finalize()
    {
        close();
    }

    /******************************************************************
     * Initializes the database by creating all the tables required.
     * Returns true if successful, false otherwise.
     *****************************************************************/
    public boolean createDatabase()
    {
    	String[] createtextTable = new String[TABLES.length];
    	String setTableStmt = null;
    	String[] headingsTable = new String[TABLES.length];
    	String headingStmt = null;

    	createtextTable[0] =    	// MolSet
            "CREATE TEXT TABLE MolSet (" +
                " id INT NOT NULL IDENTITY," +
                " filename VARCHAR(300)," +
                " expSolFilename VARCHAR(300)," +
                " variance DOUBLE" +
            ");";
    	headingsTable[0] = "id,filename,expSolFilename, variance";

    	createtextTable[1] =    	// Algorithm
            "CREATE TEXT TABLE Algorithm (" +
                " name VARCHAR(30) NOT NULL," +
                " type VARCHAR(40)," +
                " molSet INT," +
                " rsq DOUBLE," +
                " sdDev DOUBLE," +
                " mean DOUBLE," +
                " variance DOUBLE," +
                "PRIMARY KEY(name)" +
            ");";
    	headingsTable[1] = "name, type, molSet, rsq, sdDev, mean, variance";

    	createtextTable[2] =    	// Result
            "CREATE TEXT TABLE Result (" +
                " id INT NOT NULL IDENTITY," +
                " algorithm VARCHAR(30) NOT NULL," +
                " molSet INT NOT NULL," +
                " molNumber INT NOT NULL," +
                " molCas VARCHAR(30)," +
                " value DOUBLE," +
                " expSol DOUBLE," +
                " CONSTRAINT unique1 UNIQUE(algorithm,molSet,molNumber)" +
            ");";
    	headingsTable[2] = "id, algorithm, molSet, molNumber, molCas, value, expSol";

   	createtextTable[3] =    	// Parameter
            "CREATE TEXT TABLE Parameter (" +
                " algorithm VARCHAR(30) NOT NULL," +
                " id INT NOT NULL," +
                " value DOUBLE," +
                " frequency INT," +
                " type VARCHAR(40)," +
                " descriptor VARCHAR(40)," +
                " mean DOUBLE," +
                " variance DOUBLE," +
                " PRIMARY KEY(algorithm,id)" +
            ");";
	headingsTable[3] = 
		"algorithm, id, value, frequency, type, descriptor, mean, variance";

    	createtextTable[4] =    	// ParameterExtra
            "CREATE TEXT TABLE ParameterExtra (" +
                " algorithm VARCHAR(30) NOT NULL," +
                " parameter INT NOT NULL," +
                " text VARCHAR(200) NOT NULL" +
            ");";
    	headingsTable[4] = "algorithm, parameter, text";

    	createtextTable[5] =    	// Matrix
            "CREATE TEXT TABLE Matrix (" +
                " algorithm VARCHAR(30) NOT NULL," +
                " key VARCHAR(3) NOT NULL," +
                " row INT NOT NULL," +
                " column INT NOT NULL," +
                " value DOUBLE," +
                " PRIMARY KEY(algorithm,key,row,column)" +
            ");";
    	headingsTable[5] = "algorithm, key, row, column, value";

// Making the CSV text file database
        try
        {
            stmt = conn.createStatement();

            int i;
            for( i=0; i < TABLES.length; ++i )
            {
            	stmt.execute(createtextTable[i]);
            	setTableStmt = "SET TABLE " + TABLES[i] + " SOURCE " +
              	'"' + TABLES[i] + ".csv" + '"';
//            	setTableStmt = setTableStmt.substring(0,setTableStmt.length()-1) + ";ignore_first=true" + '"';
            	stmt.execute(setTableStmt);
//            	headingStmt = "SET TABLE " + TABLES[i] + " SOURCE HEADER " +
//          	'\'' + headingsTable[i] + '\'';
//            	stmt.execute(headingStmt);
            }
        }
        catch( SQLException sqle )
        {
            return false;
        }


        return true;
    }

    /*******************************************************************
     * Drops all the tables in the database. BEWARE all data in the
     * database will be deleted.
     *******************************************************************/
    public boolean deleteDatabase()
    {
        String delete = "DROP TABLE ";
        int i;
        try
        {
            stmt = conn.createStatement();
            for( i=0; i < TABLES.length; ++i )
            {
                stmt.execute(delete + TABLES[i] + " IF EXISTS;");
            }
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }

        return true;
    }

    /********************************************************************
     * Closes the connection to the database if it is still open.
     *******************************************************************/
    public synchronized void close()
    {
    	//SHUTDOWN COMPACT writes a blank to the first line if ignore_first=true
    	//I don't want this, so try SHUTDOWN
        String shutdown = "SHUTDOWN";
        if(conn != null)
        {
            try
            {
                execute(shutdown);
                conn.close();
                conn = null;
            }
            catch( SQLException sqle )
            {
                logger.debug(sqle.getMessage());
            }
        }
    }
        
}
