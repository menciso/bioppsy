package bioppsy;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Logger;

import bioppsy.algorithm.MLR;
import bioppsy.util.SaveMatrix;




/********************************************************************
 * The AlgorithmMatrices class represents associations between
 * an Algorithm and several SaveMatrix objects. Each SaveMatrix
 * is associated by a String key. Functions are available to
 * save and load these SaveMatrix objects from the database.
 *******************************************************************/
public class AlgorithmMatrices
{
    private MLR alg;
    // map from key Strings to SaveMatrix objects
    private Hashtable matrices;

    static Logger logger = Logger.getLogger(AlgorithmMatrices.class);

    
    
    /****************************************************************
     * Set the algorithm hashtable (matrices)
     * @param alg Algorithm
     ****************************************************************/
    public AlgorithmMatrices(MLR alg)
    {
        this.alg = alg;
        matrices = new Hashtable();
    }

    /****************************************************************
     * Puts a matrix
     * @param key "label" of the matrix
     * @param matrix Matrix
     * @return <tt>Object</tt> on success, <tt>null</tt> on failure
     ****************************************************************/
    public SaveMatrix put(String key, SaveMatrix matrix)
    {
        Object ret = matrices.put(key,matrix);
        if( ret != null )
        {
            return (SaveMatrix) ret;
        }
        else
        {
            return null;
        }
    }

    /****************************************************************
     * Gets a matrix
     * @param key "label" of the matrix
     * @return <tt>Object</tt> on success, <tt>null</tt> on failure
     ****************************************************************/
    public SaveMatrix get(String key)
    {
        Object ret = matrices.get(key);
        if( ret != null )
        {
            return (SaveMatrix) ret;
        }
        else
        {
            return null;
        }
    }

    /****************************************************
     * Saves all the SaveMatrix objects in this 
     * AlgorithmMatrices association. This method will
     * fail if any of the matrices have been saved
     * before, use delete first to avoid this.
     * @param db Database
     * @return (<tt>true</tt> on success; <tt>false</tt> on failure
     ***************************************************/
    public boolean save(Database db)
    {
        Enumeration keys = matrices.keys();
        String key;
        SaveMatrix SM;

        while( keys.hasMoreElements() )
        {
            key = (String) keys.nextElement();
            SM = (SaveMatrix) matrices.get(key);
            if( SM != null )
            {
                if( !SM.save(db) )
                {
                    return false;
                }
            }
        }

        return true;
    }

    /*********************************************************
     * Deletes all the SaveMatrix objects associated with
     * the given algorithm. Returns true on success, false
     * on failure. Returns true if no objects are found.
     * @param db Database
     * @return boolean
     *********************************************************/
    public boolean delete(Database db)
    {
        String delete =
            "DELETE FROM Matrix WHERE" +
            " algorithm='" + alg.getName() + "';";

        try
        {
            logger.debug(delete);
            db.execute(delete);
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }

        return true;
    }

    /*********************************************************
     * Loads all the SaveMatrix objects associated with the
     * given algorithm from the database. Returns true on
     * success, false on failure.
     * @param db Database
     * @return boolean
     *********************************************************/
    public boolean load(Database db)
    {
        SaveMatrix SM;
        String key = null;
        String oldKey = "none";
        int row;
        int oldRow = -1;
        int column;
        int oldColumn = -1;
        double value;
        // Vector of Vector of Double
        // representing the row vectors
        // of the Matrix M
        Vector rowsVector = new Vector();
        // Vector of Double
        Vector rowVector = new Vector();

        // note it is very important to order the results
        // by key,row,column
        String load =
            "SELECT * FROM Matrix WHERE" +
            " algorithm='" + alg.getName() + "'" +
            " ORDER BY key, CAST(row AS INT), CAST(column AS INT);" +
            ";";

        //logger.debug(load);

        try
        {
            ResultSet rs = db.executeQuery(load);
  
            while( rs.next() )
            {
                key = rs.getString("key");
                row = rs.getInt("row");
                column = rs.getInt("column");
                value = rs.getDouble("value");

                if( !key.equalsIgnoreCase(oldKey) )
                {
                    // we are at a new Matrix

                    // save the last row
                    rowsVector.add(rowVector);

                    // create and add the last SaveMatrix
                    SM = new SaveMatrix(alg,oldKey,rowsVector);
                    put(oldKey,SM);

                    // start a new Matrix
                    rowsVector = new Vector();
                    oldRow = -1;
                    oldColumn = -1;

                    // start a new row
                    rowVector = new Vector();
                }
                else
                {
                    if( row == oldRow )
                    {
                        // same row
                    }
                    else if( row == oldRow + 1 )
                    {
                        // save last row
                        rowsVector.add(rowVector);

                        // next row
                        rowVector = new Vector();
                        oldColumn = -1;
                    }
                    else
                    {
                        logger.error("Missing Row");
                        return false;
                    }

                    if( column != oldColumn + 1 )
                    {
                        logger.error("Missing Column");
                        return false;
                    }
                }

                rowVector.add(new Double(value));

                oldKey = key;
                oldRow = row;
                oldColumn = column;
            }

            // save the last row
            rowsVector.add(rowVector);
            // create and add the last SaveMatrix
            SM = new SaveMatrix(alg,key,rowsVector);
            put(oldKey,SM);
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }
        catch( FailedConstructorException fce )
        {
            logger.error(fce.getMessage());
            return false;
        }

        return true;
    }
}
