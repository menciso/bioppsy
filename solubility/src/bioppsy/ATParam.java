package bioppsy;

//java
import java.util.Vector;

//JOElib
import joelib.smarts.JOESmartsPattern;

// sql
import java.sql.SQLException;
import java.sql.ResultSet;

import bioppsy.algorithm.MLR;



/******************************************************
 * The ATParam class represents an AtomTypingParameter.
 * It inherits from the Param class and so can be stored
 * in a ParamList Object. Use an ATParam object by
 * adding SMARTS patterns to be matched by it with
 * the addSmarts(String) function. Then when you call
 * the match(Mol) function, the total number of matches
 * for the given molecule with all the SMARTS patterns
 * added will be returned. This is so that you can
 * associate several SMARTS patterns with a single Atom
 * Typing Parameter.
 ******************************************************/
public class ATParam extends Param
{
    // list of JOESmartsPattern Objects
    private Vector smartsVec = new Vector();

    
    public ATParam() 
    {
        super();
    }

    public ATParam(MLR al)
    {
        super(al);
    }

    public ATParam(MLR al, int id, double value)
    {
        super(al,id,value);
    }
    /****************************************************************
     * Gets the SMARTS vector
     * @return Vector
     ****************************************************************/
    public Vector getSmartsVec()
    {
        return smartsVec;
    }

    public String getType()
    {
        return "Atom Typing";
    }

    /*****************************************************
     * Adds the given SMARTS pattern to this Atom Typing
     * Parameter.
     * @param pat SMARTS pattern
     * @return boolean (<tt>true</tt> on success; 
     * <tt>false</tt> if the given pattern
     * is an invalid SMARTS pattern) 
     ******************************************************/
    public boolean addSmarts( String pat )
    {
        JOESmartsPattern smarts = new JOESmartsPattern();
        if( !smarts.init(pat) )
        {
            logger.error("Invalid SMARTS pattern: " + pat);
            return false;
        }
        else
        {
            smartsVec.add(smarts);
            setModified();
            return true;
        }
    }

    /***************************************************
     * Saves any changes to this ATParam's values in the 
     * database. Returns true on success, false on 
     * error. Param.save() is used initially and then
     * all smarts patterns are saved in the ParameterExtra
     * table.
     ***************************************************/
    public boolean save(Database db)
    {
        String smarts;
        String addSmarts;

        if( !super.save(db) )
        {
            return false;
        }

        for( int i=0; i < smartsVec.size(); ++i )
        {
            smarts = ((JOESmartsPattern) smartsVec.elementAt(i)).getSMARTS();
        
            addSmarts =
                "INSERT INTO ParameterExtra(algorithm,parameter,text) VALUES(" +
                "'" + getAlgorithm().getName() + "'," + getId() + ",'" + smarts + "');";
            //logger.debug(addSmarts);
            
            try
            {
                db.execute(addSmarts);
            }
            catch( SQLException sqle )
            {
                logger.error(sqle.getMessage());
                return false;
            }    
        }

        setNotModified();

        return true;
    }

    protected Param get(int id, MLR alg, Database db)
        throws SQLException
    {
        ResultSet rs;
        ATParam atParam;

        String load =
            "SELECT value,frequency FROM Parameter WHERE" + 
            " algorithm='" + alg.getName() + "'" +
            " AND id=" + id + " ORDER BY id;";


        rs = db.executeQuery(load);
        while(rs.next())
        {
             atParam = new ATParam(alg,id,rs.getDouble("value"));
             atParam.setFrequency(rs.getInt("frequency"));
             if( !atParam.loadSmarts(db) )
             {
                 return null;
             }
             atParam.setNotModified();
             return atParam;
        }
        return null;
    }

    /*****************************************************
     * Loads all the SMARTS patterns from the ParameterExtra
     * table of the database for this ATParam. Returns
     * true on success, false on failure.
     *****************************************************/
    public boolean loadSmarts(Database db)
    {
        ResultSet rs;
        String smarts;

        String getSmarts =
            "SELECT text FROM ParameterExtra WHERE parameter=" +
            getId() + " AND algorithm='" +
            getAlgorithm().getName() + "';";

        try
        {
            rs = db.executeQuery(getSmarts);
            while( rs.next() )
            {
                smarts = rs.getString("text");
                if( !addSmarts(smarts) )
                {
                    return false;
                }
            }
            setModified();
            return true;
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }
    }

    /*******************************************************
     * Deletes this ATParam from the database. Returns true
     * on success, false on error. Uses Param.delete()
     * to delete the row from the Parameters table and 
     * then deletes all the smarts strings stored in the
     * ParameterExtra table.
     *******************************************************/
    public boolean delete(Database db)
    {
        if( !super.delete(db) )
        {
            return false;
        }

        String deleteExtra = 
            "DELETE FROM ParameterExtra WHERE parameter=" +
            getId() + " AND algorithm='" +
            getAlgorithm().getName() + "';";
        
        //logger.debug(deleteExtra);

        try
        {
            db.execute(deleteExtra);
        }
        catch( SQLException sqle )
        {
            logger.error(sqle.getMessage());
            return false;
        }

        setModified();
 
        return true;
    }

    /*********************************************************
     * Returns a Vector of Integer which contains the indices
     * of the atoms in the given Mol that match this parameter.
     * Returns null on error. Each atom can match at most
     * once.
     *********************************************************/
    public Vector matchList(Mol mol)
    {
        int i,j,size;
        JOESmartsPattern jsp;
        // Vector of int[]
        Vector matchList;
        // Vector of Integer
        Vector matchIntegerList;
        Vector returnList;

        returnList = new Vector();

        for( i=0; i < smartsVec.size(); ++i )
        {
            jsp = (JOESmartsPattern) smartsVec.elementAt(i);
            jsp.match(mol);
            // get a list of matching atoms
            matchList  = jsp.getUMapList();
            if( matchList == null )
            {
                continue;
            }
            // find how many atoms matched
            size = matchList.size();
            if( size > 0 )
            {
                // convert Vector of int[] to Vector
                // of Integer using the first element
                // in the int[]
                matchIntegerList = new Vector();
                for(j=0; j < matchList.size(); ++j)
                {
                    matchIntegerList.add(
                        new Integer(
                            ((int []) matchList.elementAt(j))[0] ) );
                }

                // add the matchList to the end of the 
                // returnList, removing duplicates
                addNoDuplicates(matchIntegerList,returnList);
            }
        }
        
        return returnList;
    }

    /******************************************************
     * Adds the first Vector to the second Vector. Elements
     * are only added if they are not in the second Vector,
     * comparing using the equals method.
     ******************************************************/
    public void addNoDuplicates(Vector from, Vector to)
    {
        int i;
        Object element;
        for( i=0; i < from.size(); ++i )
        {
            element = from.elementAt(i);
            if( !to.contains(element) )
            {
                to.add(element);
            }
        }
    }

    /********************************************************
     * Returns the total number of matches of this ATParam's
     * SMARTS patterns for the given Mol. Each atom in
     * the given Mol can match more than one SMARTS pattern
     * in this ATParam.
     ********************************************************/
    public int match(Mol mol)
    {
        int i;
        int count = 0;
        int size;
        JOESmartsPattern jsp;
        Vector matchList;

        for( i=0; i < smartsVec.size(); ++i )
        {
            jsp = (JOESmartsPattern) smartsVec.elementAt(i);
            jsp.match(mol);
            // get a list of matching atoms
            matchList  = jsp.getUMapList();
            if( matchList == null )
            {
                continue;
            }
            // find how many atoms matched
            size = matchList.size();
            if( size > 0 )
            {
                count += size;
            }
        }

        return count;
    }

    /*********************************************************
     * Returns the number of times this ATParam occurs in
     * the given Mol.
     ********************************************************/
    public double getDouble(Mol mol)
    {
        return matchList(mol).size();
    }

    /**********************************************************
     * Returns a Vector of Strings which are all the SMARTS
     * patterns in this ATParam.
     ********************************************************/
    public Vector getSmartsPatterns()
    {
        Vector ret = new Vector();
        for( int i=0; i < smartsVec.size(); ++i )
        {
            JOESmartsPattern jsp = (JOESmartsPattern)
                smartsVec.elementAt(i);
            ret.add(jsp.getSMARTS());
        }
        return ret;
    }
}
