package bioppsy.user;


//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;



/**********************************************************************
 * An ErrorPlotPanel had a MolSet, a Param, and an Algorithm. For
 * each Mol in the MolSet the error (|calculated - experimental property|)
 * is plotted against the value of the Param for the Mol.
 * Just create an ErrorPlotPanel using the only constructor and
 * add it to a container to display it. Then call the load method
 * which starts up a new thread to load and display the chart.
 **********************************************************************/
public class ErrorPlotPanel extends PlotPanel
{
    static Logger logger = Logger.getLogger(ErrorPlotPanel.class);

    private Param param;
    private MLR alg;

    public ErrorPlotPanel(MolSet molSet, Param param, MLR alg, GUI parent)
    {
        super(molSet, parent);
        this.param = param;
        this.alg = alg;
    }

    public String getTitle()
    {
        return "Error Plot";
    }

    public String getChartTitle()
    {
        return "Parameter #" + param.getId() +
            " vs Error";
    }

    public String getXTitle()
    {
        return "Parameter Value";
    }

    public String getYTitle()
    {
        return "Error";
    }

    public void addRowToDataset(Mol mol, VectorXYDataset dataset)
    {
        double occurrence;
        Result result;
        double expProp;
        double error;

        occurrence = param.getDouble(mol);
        result = new Result();
        if( alg.calculateProperty(mol,result) == null )
        {
            return;
        }
        try
        {
            expProp = mol.getExpProp();
        }
        catch( NotFoundException nfe )
        {
            return;
        }
//      error = Math.abs(result.getValue()-expSol);
        error = result.getValue()-expProp;
        dataset.add(occurrence,error);
    }
}
