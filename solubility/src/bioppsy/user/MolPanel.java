package bioppsy.user;


import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;

import Jama.Matrix;
import bioppsy.Constants;
import bioppsy.Mol;
import bioppsy.NotFoundException;
import bioppsy.Result;
import bioppsy.ResultCollection;
import bioppsy.algorithm.MLR;

/****************************************************************
 * A MolPanel displays a single molecule in a CloseablePanel.
 * A button is available for showing the structure of a molecule
 * using a molecular graphics package. The details of the molecule 
 * such as experimental property, calculated property, 
 * molecule identifier, etc.., are displayed. The occurrences for the algorithm
 * parameters for the molecule are also shown.
 * Graphics package is UCSF Chimera. An environment variable, CHIMERA_PATH,
 * may need to be set to access it.
 ****************************************************************/
class MolPanel extends CloseablePanel
{ 
	Mol mol;
    MLR alg = null;
    String filename;
    long currTime;
    Process process = null;

    static Logger logger = Logger.getLogger(MolPanel.class);

    public MolPanel(GUI parent, Mol mol, MLR alg)
    {
        super(parent);
        this.mol = mol;
        this.alg = alg;
        displayDetails();

        /*  just playing around
        try
        {
            Descriptor descriptor = DescriptorFactory.getDescriptor("LogP");
            DoubleResult result = (DoubleResult) descriptor.calculate(mol);
            logger.debug("LogP: " + result.getDouble() );
            descriptor = DescriptorFactory.getDescriptor("TestDescriptor");
            result = (DoubleResult) descriptor.calculate(mol);
            logger.debug("Should be 1.1: " + result);
            descriptor = DescriptorFactory.getDescriptor("MolecularWeightSquared");
            if( descriptor == null )
            {
                logger.error("Descriptor null");
            }
            result = (DoubleResult) descriptor.calculate(mol);
            logger.debug("Molecular Weight Squared: " + result);
        }
        catch( DescriptorException de )
        {
            logger.error(de.getMessage());
        }
        */
    }

    public String getTitle()
    {
        return "Molecule";
    }

    public void addMenuItems(JPanel menuPanel) 
    {
        JButton view = new JButton("VIEW STRUCTURE");
        view.addActionListener(new ViewActionListener());
        menuPanel.add(view);
    }

    public class ViewActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            viewInMolGraphics(); 
        }
    }

    public boolean viewInMolGraphics() 
    /****************************************************************************
     * Calls a molecular graphics package. The package is set in 'ini/constants.ini'.
     * Currently UCSF Chimera
     ***************************************************************************/
    {
        // first delete the older files so that they don't accumulate
        deleteOldFiles();

        // Write the molecule to a short-term file for the graphics program to read
        String tempDir = 
            Constants.instance().getString("TEMP_DIR");
        String ext = 
            Constants.instance().getString("SAVE_FILE_EXTENSION");
        File tempDirPath = new File(tempDir);
        tempDirPath.mkdir(); 	// makes directory if it does not exist

        currTime = System.currentTimeMillis();
        String timeString = (new Long(currTime)).toString();

        if( filename == null )
        {
            filename = 
                tempDir + "/" + timeString + "." + ext;

            if( !mol.writeToFile(filename) )
            {
                logger.error("Unable to write Mol to file: " + filename);
            }
        }
        
        // Make the command to call the graphics program, passing the molecule file
        String WhichGraphics = Constants.instance().getString("MOL_GRAPHICS_PACK");
        String GraphicsPath = 
            Constants.instance().getString(WhichGraphics + "_FILE_PATH");
        if( GraphicsPath.startsWith ("$")) 
        {
        	GraphicsPath = System.getenv(GraphicsPath.substring(1));
        }
        // If the value for ..._FILE_PATH in constants.ini starts with a $ 
        // it indicates it refers to an environment variable. This needs to 
        // be set in the calling environment (e.g. in .bashrc)
        String GraphicsCommand = 
        	Constants.instance().getString(WhichGraphics + "_CMD");
        String command = GraphicsCommand + " " + GraphicsPath + " " + filename;

        try
        {
            process = Runtime.getRuntime().exec(command); 
            revalidate();
        }
        catch( IOException ioe )
        {
            logger.error(ioe.getMessage());
            return false;
        }

        return true;
    }

    public void displayDetails()
    {
    	JPanel detailPanel = new JPanel();
    	JLabel label;
    	detailPanel.setLayout(new GridBagLayout());
    	GridBagConstraints c = new GridBagConstraints();
    	c.fill = GridBagConstraints.HORIZONTAL;
    	c.insets = new Insets(10,10,0,0);
    	c.weightx = 0.5;
    	int y=0;
    	Border loweredbevel = BorderFactory.createLoweredBevelBorder();
    	TitledBorder title;
   	        
    	label = new JLabel(
                (new Integer(mol.getNumber())).toString());
    	c.gridy = y;
    	title = BorderFactory.createTitledBorder(loweredbevel, "Number");
    	title.setTitlePosition(TitledBorder.ABOVE_TOP);
    	label.setBorder(title);
    	detailPanel.add(label, c);


    	y=y+1;
        String molId = mol.getMolId();
        if( molId != null )
        {
            label = new JLabel(molId);
        }
        else
        {
        	label = new JLabel("Not found");
        }
    	c.gridy = y;
    	title = BorderFactory.createTitledBorder(loweredbevel, "Molecule identifier");
    	title.setTitlePosition(TitledBorder.ABOVE_TOP);
    	label.setBorder(title);
    	detailPanel.add(label, c);
    	
    	y=y+1;
        String expPropString;
        try
        {
            expPropString = (new Double(mol.getExpProp())).toString();
        }
        catch(NotFoundException nfe)
        {
           expPropString = "none";
        }
        label = new JLabel(expPropString);
    	c.gridy = y;
    	title = BorderFactory.createTitledBorder(loweredbevel, "Experimental property");
    	title.setTitlePosition(TitledBorder.ABOVE_TOP);
    	label.setBorder(title);
    	detailPanel.add(label, c);

    	// Optional information. Only show if present
    	String prop;
    	prop = mol.getCLOGP();
        if( prop != null )
        {
        	y=y+1; c.gridy = y;
            label = new JLabel(prop);
        	title = BorderFactory.createTitledBorder(loweredbevel, "ClogP");
        	title.setTitlePosition(TitledBorder.ABOVE_TOP);
        	label.setBorder(title);
        	detailPanel.add(label, c);
        }
    	prop = mol.getdeltaGsolv();
        if( prop != null )
        {
        	y=y+1; c.gridy = y;
            label = new JLabel(prop);
        	title = BorderFactory.createTitledBorder(loweredbevel, "Delta G Solvation");
        	title.setTitlePosition(TitledBorder.ABOVE_TOP);
        	label.setBorder(title);
        	detailPanel.add(label, c);
        }
    	prop = mol.getdeltaGsub();
        if( prop != null )
        {
        	y=y+1; c.gridy = y;
            label = new JLabel(prop);
        	title = BorderFactory.createTitledBorder(loweredbevel, "Delta G Sublimation");
        	title.setTitlePosition(TitledBorder.ABOVE_TOP);
        	label.setBorder(title);
        	detailPanel.add(label, c);
        }
    	prop = mol.getPSA();
        if( prop != null )
        {
        	y=y+1; c.gridy = y;
            label = new JLabel(prop);
        	title = BorderFactory.createTitledBorder(loweredbevel, "Polar Surface Area");
        	title.setTitlePosition(TitledBorder.ABOVE_TOP);
        	label.setBorder(title);
        	detailPanel.add(label, c);
        }
        
    	prop = mol.getU_RNCG();
        if( prop != null )
        {
        	y=y+1; c.gridy = y;
            label = new JLabel(prop);
        	title = BorderFactory.createTitledBorder(loweredbevel, "Relative negative surface area (RNCG)");
        	title.setTitlePosition(TitledBorder.ABOVE_TOP);
        	label.setBorder(title);
        	detailPanel.add(label, c);
        }
        
        
    	
        // Add always comments
    	y=y+1;
    	String comm = mol.getComment();
        if( comm != null )
        {
            label = new JLabel("<html><p>"+comm+"</p></html>");
        }
        else
        {
        	label = new JLabel("Not found");
        }
    	c.gridy = y;
    	title = BorderFactory.createTitledBorder(loweredbevel, "Comments");
    	title.setTitlePosition(TitledBorder.ABOVE_TOP);
    	label.setBorder(title);
    	detailPanel.add(label, c);
    	
//    	// Predicted property
//    	System.out.print( alg + " algorithm\n");
//        if( alg != null )
//        {
//        	y=y+1;c.gridy = y;
//            Result result = new Result();
//            Matrix X = alg.calculateProperty(mol,result);
//            String algName = alg.getName(); 
//        	String value = (new Double(result.getValue())).toString();
//            label = new JLabel("<html><p>Algorithm name:&emsp;" + algName +
//            		"</p><p>Calculated property:&emsp;" + value +"</p></html>");
//        	title = BorderFactory.createTitledBorder(loweredbevel, "QSPR results");
//        	title.setTitlePosition(TitledBorder.ABOVE_TOP);
//        	label.setBorder(title);
//        	detailPanel.add(label, c);            
//        }
    	
/*    	JPanel detailPanel = new JPanel();
        
      JPanel detailPanel = new JPanel();
        detailPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        detailPanel.setLayout(
            new GridLayout(4,2,20,50));
   
        
        String comm = mol.getComment();
        if( comm != null )
        {
            JLabel molCommLabel = new JLabel("Comments:");
            c.gridx = 2;c.gridy = 0;
            detailPanel.add(molCommLabel);
            JLabel moldCommValue = new JLabel(comm);
            c.gridx = 2;c.gridy = 1;
            detailPanel.add(moldCommValue);
        }

        JLabel expLabel = new JLabel("Experimental property:");
        c.gridx = 3;c.gridy = 0;
        detailPanel.add(expLabel);
        c.gridx = 3;c.gridy = 1;
        String expPropString;
        try
        {
            expPropString = (new Double(mol.getExpProp())).toString();
        }
        catch(NotFoundException nfe)
        {
           expPropString = "none";
        }

        JLabel expValue = new JLabel(expPropString);
        detailPanel.add(expValue);*/
       
        add(detailPanel);
    }

// This is from an old version of the program. Kept here because ParameterPanel 
// might be useful at some stage
//    public void displayResult()
//    {
//        Result result = new Result();
//        Matrix X = alg.calculateProperty(mol,result);
//
//        JPanel logSPanel = new JPanel();
//        logSPanel.setLayout(new GridLayout(2,2));
//        logSPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
//        JLabel logSLabel = new JLabel("Calculated property:    ");
//        logSPanel.add(logSLabel);
//        JLabel logSValue = new JLabel((new Double(result.getValue())).toString());
//        logSPanel.add(logSValue);
//
//        JLabel algLabel = new JLabel("Algorithm used for property calculation");
//        logSPanel.add(algLabel);
//        JLabel algName;
//        if( alg != null )
//        {
//            algName = new JLabel(alg.getName());
//        }
//        else
//        {
//            algName = new JLabel("not calculated");
//        }
//        logSPanel.add(algName);
//
//        add(logSPanel);
//
//        if( X != null )
//        {
//            ParameterPanel parameterPanel =
//                new ParameterPanel(X,alg,parent);
//            parameterPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
//            add(parameterPanel);
//        }
//    }

    public void close()
    {
        deleteFile();
        if( process != null )
        {
            process.destroy();
        }
        super.close();
    }

    public void deleteFile()
    {
       if( filename == null )
       {
           return;
       }
       File file = new File(filename);
       if( !file.delete() )
       {
           logger.warn("Unable to delete file: " + filename);
       }
    }

    public void deleteOldFiles()
    {
        String tmpDir = Constants.instance().getString("TEMP_DIR");
        File file = new File(tmpDir);
        File[] files = file.listFiles();
        if( files == null )
        {
            return;
        }
        for( int i=0; i < files.length; ++i )
        {
            String name = files[i].getName();
            String[] splitName = name.split(".");
            if( splitName.length == 2 )
            {
                if( splitName[1].equals(
                    Constants.instance().getString("SAVE_FILE_EXTENSION")) )
                {
                    long prefix = (new Long(splitName[0])).longValue();
                   if( prefix < currTime )
                   {
                        files[i].delete();
                    } 
                }
            }
        }
    }
}