package bioppsy.user;


// gui
import javax.swing.*; 
//log4j
import org.apache.log4j.Logger;
//jfreechart
import org.jfree.chart.*;
import org.jfree.chart.plot.*;

import bioppsy.*;


//java
import java.util.Iterator;

/**********************************************************************
 * A PlotPanel is a CloseablePanel (a JPanel that is displayed as
 * a tab in a property.user.GUI object) that plots a scatterplot
 * chart from values calculated by iterating through a MolSet.
 * To create a Plot extend this class and override the title methods
 * and the addRowToDataset method. Note that the load() method must
 * be called to load and display the chart.
 **********************************************************************/
public abstract class PlotPanel extends CloseablePanel
{
    static Logger logger = Logger.getLogger(PlotPanel.class);

    private VectorXYDataset dataset;
    private JProgressBar progressBar;
    private MolSet molSet;
    private Thread gdThread = null;

    public PlotPanel(MolSet molSet, GUI parent)
    {
        super(parent);
        this.molSet = molSet;
    }

    /*****************************************************
     * Returns the title of the tab
     *****************************************************/
    public String getTitle()
    {
        return "Plot";
    }

    /********************************************************
     * Adds items to the menuPanel. This is the panel that
     * is at the top of the tab on the left of the close 
     * button.
     *******************************************************/
    public void addMenuItems(JPanel menuPanel)
    {
        progressBar = new JProgressBar();
        progressBar.setString("Charting Values Please Wait");
        menuPanel.add(progressBar);
    }

    /**********************************************************
     * Commences the drawing of the chart.
     *********************************************************/
    public void load()
    {
        gdThread = new GetDataThread("GetData");
        gdThread.start();
    }

    /*********************************************************
     * Returns the label at the top of the chart. 
     ********************************************************/
    public abstract String getChartTitle();

    /*********************************************************
     * Returns the label of the X axis displayed on the chart.
     *********************************************************/
    public abstract String getXTitle();

    /******************************************************
     * Returns the label of the Y axis displayed on the chart.
     *****************************************************/
    public abstract String getYTitle();

    /************************************************
     * Create the chart
     ************************************************/
    public JPanel createChart()
    {
        JFreeChart chart = ChartFactory.createScatterPlot
            (
                getChartTitle(),
                getXTitle(),
                getYTitle(),
                dataset,
                PlotOrientation.VERTICAL,
                false,
                false,
                false
            );
        ChartPanel chartPanel = new ChartPanel(chart);
        return chartPanel;
    }

    /****************************************************
     * Obtaining the data from the file can take a bit of
     * time so we do it in a separate thread
     ****************************************************/
    public class GetDataThread extends Thread {

        public GetDataThread(String str) 
        {
            super(str);
        }

        public void run() 
        {
            getDataThread();
            
        }
    }

    /*****************************************************
     * The method that is run by GetDataThread
     *****************************************************/
    public void getDataThread()
    {
        progressBar.setIndeterminate(true);
        progressBar.setStringPainted(true);
        getDataset();
        add(createChart());
        progressBar.setIndeterminate(false);
        progressBar.setStringPainted(false);
        revalidate();
    }

    /*********************************************
     * Sets the internal VectorXYDataset object
     *********************************************/
    public void getDataset()
    {
        dataset =
            new VectorXYDataset();

        Iterator msi = molSet.getIterator();
        Mol mol;
        while( msi.hasNext() )
        {
            mol = (Mol) msi.next();
            addRowToDataset(mol, dataset);
        }
    }

    /*********************************************************************
     * Adds a row to the given VectorXYDataset, calculating the values
     * from the given Mol. The values should be added to the dataset
     * using the VectorXYDataset.add(double x,double y) method.
     ********************************************************************/
    public abstract void addRowToDataset(Mol mol, VectorXYDataset dataset);

    /*******************************************************************
     * Does any cleanup and closes this panel.
     ******************************************************************/
    public void close()
    {
        if( gdThread != null )
        {
            gdThread.stop();
        }
        super.close();
    }

}
