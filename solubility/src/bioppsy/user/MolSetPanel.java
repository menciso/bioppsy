package bioppsy.user;

//solubility

// gui
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;
import javax.swing.Timer;

import org.apache.log4j.Logger;

import bioppsy.Database;
import bioppsy.Mol;
import bioppsy.MolSet;
import bioppsy.ResultCollectionNavigator;
import bioppsy.algorithm.MLR;
import bioppsy.util.Notifier;

class MolSetPanel extends CloseablePanel
{
	private JFileChooser chooser;

    private int MAX_CHARS_DISPLAYED = 20;
    
    // Strings providing information to users
    private String calcPropString =
        "<html>" +
        "<p><br>The algorithm chosen above can be used to estimate " +
        "the properties of the molecules in this set. " +
        "This can be useful to rank and sort the molecules by " +
        " their calculated property in the Results tab. </p>" +
        "<p><br>Clicking on the 'start' buttton above starts the " +
        " calculation. Click on the 'pause' button once started to " +
        " suspend the calculation so it can be resumed later. " +
        " The 'stop' button will cancel the calculation to be " +
        " restarted from the beggining of the set of molecules.</p><br>" ;

    private String syntax =
        "<html><br>" +
        "Each line in the file represents a molecule and must have " +
        "the following format:<br><br>" +
        "56-47-3  deoxycorticosterone acetate -4.63_0.34<br><br>" +
        "- The first word must contain the number, this can either " +
        "be just the id number of the molecule or the CAS number<br>" +
        "- The last word must contain the experimental property, " +
        "if it is not present the line is skipped. This value " +
        "can include an optional error value after an underscore.<br>" +
        "- Any words in between the first and last word are taken as " +
        "the name of the molecule<br>" +
        "</html>";

    MolSet molSet = null;
    MLR alg = null;
    Database db;

    JProgressBar progressBar;

    JTabbedPane tabbedPane;

    JPanel contentPanel = null;
    JPanel propertyPanel = null;
    JPanel resultsPanel = null;
    JPanel detailPanel = null;
    JPanel expPropPanel = null;
    RCNPanel rcnPanel = null;
    // list of String algorithm names, the selected
    // name indicates the current selected algorithm
    JComboBox algList = null;
    JLabel successNumber = null;
    JLabel errorNumber = null;
    JLabel timerTime = null;

    JButton calcPropButton;
    JButton pauseCalcPropButton;
    JButton stopCalcPropButton;
    JCheckBox indexCheckBox;

    JLabel expPropSdDevLabel;

    JButton deleteButton;

    Notifier not;
    Thread propertyThread;

    int seconds = 0;
    int minutes = 0;
    int hours = 0;
    Timer timer;
    public final static int ONE_SECOND = 1000;

    static Logger logger = Logger.getLogger(MolSetPanel.class);

    public MolSetPanel(GUI parent, Database db)
    {
        super(parent);
        this.db = db;
        timer = new Timer(ONE_SECOND,new TimerActionListener());
    }

    public String getTitle()
    {
        return "Molecule Set";
    }

    public void addMenuItems(JPanel menuPanel)
    {
        progressBar = new JProgressBar();
        menuPanel.add(progressBar);

        // create a gap
        menuPanel.add(Box.createRigidArea(new Dimension(10,5)));

        deleteButton = new JButton("DELETE MOLSET");
        deleteButton.addActionListener(
            new DeleteActionListener() );
        deleteButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        menuPanel.add(deleteButton);
    }

    public class SetExpPropFileActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            setExpPropFile();
        }
    }

    public boolean setExpPropFile()
    {
        //JFileChooser chooser = new JFileChooser();
        File file;

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
           file = chooser.getSelectedFile();
           if( !molSet.setExpPropFile("$" + file.getAbsolutePath()) )
           {
               logger.error("File cannot be read: " + file.getName());
               return false;
           }
           displayExpPropPanel();
           return true;
        }
        else
        { 
            logger.debug("No file chosen in MolSetPanel.setExpPropFile()");
            return false;
        }
    }

    public class CalcPropActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            calculateProperties();
        }
    }

    public void calculateProperties()
    {
        // if we don't have an algorithm set
        // we load one with the name selected
        // in the algList combo box
        if( alg == null )
        {
            alg = MLR.getAlgorithm(
                (String) algList.getSelectedItem(),db);
            if( alg == null )
            {
                return;
            }
            not = new PropertyNotifier();
            notifyClear();
            propertyThread = new PropertyThread("Property");
            propertyThread.start();
        }
        else
        {
            not.start();
        }
        startCalculation();
    }

    /******************************************************
     * Returns the current Algorithm selected by the user,
     * null if none is currently selected.
     ******************************************************/
    public MLR getAlgorithm()
    {
        return MLR.getAlgorithm(
            (String) algList.getSelectedItem(),db);
    }

    public class PropertyNotifier extends Notifier
    {
        public synchronized void notify(int numDone, int numError)
        {
            doNotify(numDone,numError);
        }
    }

    public void doNotify(int numDone, int numError)
    {
        String success = (new Integer(numDone)).toString();
        successNumber.setText(success);
        String error = (new Integer(numError)).toString();
        errorNumber.setText(error);
        logger.info("Calculated: " + success + " Error: " +
            error + " Time: " + 
            hours + ":" + minutes + ":" + seconds);
        revalidate();
    }

    public void notifyClear()
    {
        successNumber.setText("0");
        errorNumber.setText("0");
        timerTime.setText("0:0:0");
        seconds = 0;
        minutes = 0;
        hours = 0;
        revalidate();
    }

    public class PropertyThread extends Thread {

        public PropertyThread(String str) 
        {
            super(str);
        }

        public void run() 
        {
            if( !not.die() )
            {
                progressBar.setString("Calculating Properties");
                alg.calculateProperties(molSet,not,db);
                progressBar.setString("Finished");
                alg = null;
                stopCalculation();
            }
        }
    }

    public class PauseCalcSolActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            not.stop();
            stopCalculation();
        }
    }

    public class StopCalcPropActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            not.kill();
            alg = null;
            stopCalculation();
        }
    }

    public void startCalculation()
    {
        calcPropButton.setEnabled(false);
        pauseCalcPropButton.setEnabled(true);
        stopCalcPropButton.setEnabled(true);
        timer.start();
        progressBar.setIndeterminate(true);
        progressBar.setStringPainted(true);
    }

    public void stopCalculation()
    {
        calcPropButton.setEnabled(true);
        pauseCalcPropButton.setEnabled(false);
        stopCalcPropButton.setEnabled(false);
        timer.stop();
        progressBar.setIndeterminate(false);
        progressBar.setStringPainted(false);
    }

    public boolean open()
    {
        //JFileChooser chooser = new JFileChooser();
        File file;

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
           file = chooser.getSelectedFile();
           String choosenFilename = "$" + file.getAbsolutePath();
           molSet = MolSet.get(choosenFilename,db);
           if( molSet == null )
           {
               molSet = new MolSet(choosenFilename);
               if( !molSet.save(db) )
               {
                   logger.error("Unable to save new MolSet: " + 
                       file.getName());
                   return false;
               }
           }
           if( !molSet.setFile(choosenFilename) )
           {
               logger.error("File cannot be read: " + file.getName());
               return false;
           }
           display();
           return true;
        }
        else
        { 
            logger.debug("No file chosen in MolSetPanel.open()");
            return false;
        }
    }

    public void display()
    {
        //Vector names;

        if( molSet == null )
        {
            return;
        }

        // We create an overall content panel so that
        // se can easily remove and refresh it

        if( contentPanel != null )
        {
            remove(contentPanel);
        }

        contentPanel = new JPanel();
        contentPanel.setLayout(
            new BoxLayout(contentPanel,BoxLayout.PAGE_AXIS));
        contentPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        displayDetailPanel();

        tabbedPane = new JTabbedPane();
        tabbedPane.setAlignmentX(Component.LEFT_ALIGNMENT);

        addPropertyPanel();
        displayResultsPanel();
        MolSetNavigatorPanel msnp = new MolSetNavigatorPanel(molSet,this);
        tabbedPane.addTab("Browse Molecules", msnp);
        displayExpPropPanel();

        contentPanel.add(tabbedPane);

        add(contentPanel);
        revalidate();
    }

    /********************************************************************
     * Adds the details about the MolSet such as filename to the tabbedPane
     *********************************************************************/
    public void displayDetailPanel()
    {
        if( detailPanel != null )
        {
            detailPanel.removeAll();
        }
        else
        {
            detailPanel = new JPanel();
            detailPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            detailPanel.setLayout(new BoxLayout(detailPanel,BoxLayout.LINE_AXIS));
            detailPanel.setBorder(BorderFactory.createLoweredBevelBorder());
            contentPanel.add(detailPanel);
        }
        
        String filename = molSet.getFile().getName();
        filename = "..." + filename.substring(filename.length()-MAX_CHARS_DISPLAYED);
        JLabel filenameLabel = new JLabel(filename);
        detailPanel.add(filenameLabel);

        detailPanel.add(Box.createHorizontalGlue());

        Vector names = MLR.getNames(db);
        if( names == null || names.size() <= 0 )
        {
            logger.debug("There are no Algorithms to load");
            names = new Vector();
            names.add("none");
        }
        JLabel algorithmIntro = new JLabel("Algorithm: ");
        detailPanel.add(algorithmIntro);
        JPanel algListPanel = new JPanel();
        algListPanel.setLayout(
            new BoxLayout(algListPanel,BoxLayout.LINE_AXIS) );
        algList = new JComboBox(names);
        algList.setMaximumSize(new Dimension(170,30));
        algList.addActionListener(new AlgListActionListener());
        algListPanel.add(algList);
        JButton resetList = new JButton("Reset List");
        resetList.addActionListener(new RefreshDetailPanelActionListener());
        algListPanel.add(resetList);
        algListPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        detailPanel.add(algListPanel);
    }

    /****************************************************
     * When the algorithm is changed in the algList this
     * ActionListener is notified. The resultPanel is
     * then notified of the change in algorithm
     ****************************************************/
    public class AlgListActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            MLR retAlg = getAlgorithm();
            if( retAlg != null )
            {
                rcnPanel.getRCN().setAlgorithm(retAlg);
                displayResultsPanel();
            }
        }
    }

    public class RefreshDetailPanelActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            displayDetailPanel();
            revalidate();
        }
    }

    /******************************************************************
     * Adds the solubility calculation buttons to the tabbedPane
     ******************************************************************/
    public void addPropertyPanel()
    {
        propertyPanel = new JPanel();
        propertyPanel.setLayout(
            new BoxLayout(propertyPanel,BoxLayout.PAGE_AXIS));
        propertyPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        propertyPanel.setBorder(
            BorderFactory.createTitledBorder("Estimate Properties"));

        JPanel controlButtonsPanel = new JPanel();
        controlButtonsPanel.setLayout(
            new BoxLayout(controlButtonsPanel,BoxLayout.LINE_AXIS));

        calcPropButton = new JButton();
        calcPropButton.addActionListener(
            new CalcPropActionListener() );
        calcPropButton.setText("START");
        controlButtonsPanel.add(calcPropButton);

        pauseCalcPropButton = new JButton();
        pauseCalcPropButton.addActionListener(
            new PauseCalcSolActionListener() );
        pauseCalcPropButton.setText("PAUSE");
        pauseCalcPropButton.setEnabled(false);
        controlButtonsPanel.add(pauseCalcPropButton);

        stopCalcPropButton = new JButton();
        stopCalcPropButton.addActionListener(
            new StopCalcPropActionListener() );
        stopCalcPropButton.setText("STOP");
        stopCalcPropButton.setEnabled(false);
        controlButtonsPanel.add(stopCalcPropButton);

        controlButtonsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        propertyPanel.add(controlButtonsPanel);

        JLabel helpLabel = new JLabel(calcPropString);
        helpLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        propertyPanel.add(helpLabel);

        JPanel dataPanel = new JPanel();
        dataPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        dataPanel.setLayout(new GridLayout(3,2));

        JLabel successIntro = new JLabel("Successful molecules: ");
        dataPanel.add(successIntro);
        successNumber = new JLabel("0");
        dataPanel.add(successNumber);
        
        JLabel errorIntro = new JLabel("Failed molecules: ");
        dataPanel.add(errorIntro);
        errorNumber = new JLabel("0");
        dataPanel.add(errorNumber);

        JLabel timerIntro = new JLabel("Calculation time: ");
        dataPanel.add(timerIntro);
        timerTime = new JLabel("0");
        dataPanel.add(timerTime);

        propertyPanel.add(dataPanel);

        tabbedPane.addTab("Estimate property", propertyPanel);
    }

    /******************************************************************
     * Adds a tab to the tabbedPane showing the results of the
     * solubility estimation
     *****************************************************************/
    public void displayResultsPanel()
    {
        // load the algorithm to view results of
        // by the name selected in algList
        MLR theAlg =    MLR.getAlgorithm(
            (String) algList.getSelectedItem(),db);
        // notice that theAlg can be null here, it is
        // ok, a ResultCollectionNavigator can handle it
        ResultCollectionNavigator rcn =
            new ResultCollectionNavigator(
                theAlg,molSet,"molNumber",db);
        rcnPanel = new RCNPanel(parent,rcn,db);
		rcnPanel.setJFileChooser(chooser);
        insertTab("Results",rcnPanel);
    }
    /******************************************************************
     * Adds the experimental property functions to the tabbedPane
     *******************************************************************/
    public void displayExpPropPanel()
    {
        expPropPanel = new JPanel();
        expPropPanel.setLayout(
            new BoxLayout(expPropPanel,BoxLayout.PAGE_AXIS));
        expPropPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        expPropPanel.add(Box.createRigidArea(new Dimension(5,5)));

        String filename = "none";
        if( molSet.getExpPropFile() != null )
        {
            filename = molSet.getExpPropFile().getName();
        }
        JLabel logSFileIntro = new JLabel("Experimental data file: ");
        expPropPanel.add(logSFileIntro);

        expPropPanel.add(Box.createRigidArea(new Dimension(5,5)));

        JLabel logSFileLabel = new JLabel(filename);
        logSFileLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        expPropPanel.add(logSFileLabel);

        expPropPanel.add(Box.createRigidArea(new Dimension(5,5)));

        JLabel logSFileSyntax = new JLabel(syntax);
        expPropPanel.add(logSFileSyntax);

        expPropPanel.add(Box.createRigidArea(new Dimension(5,5)));

        JButton expSolFileButton = new JButton("LOAD DATA FILE");
        expSolFileButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        expSolFileButton.addActionListener(
            new ExpPropFileActionListener() );
        expPropPanel.add(expSolFileButton);

        expPropPanel.add(Box.createRigidArea(new Dimension(10,30)));

        JLabel expSolSdDevIntro = new JLabel("Standard Deviation of experimental data values:");
        expPropPanel.add(expSolSdDevIntro);
        expPropSdDevLabel = new JLabel("click on button below to calculate");
        expPropSdDevLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        expPropPanel.add(expPropSdDevLabel);
        JButton expSolSdDevButton = new JButton("STANDARD DEVIATION");
        expSolSdDevButton.addActionListener(new ExpPropSdDevActionListener());
        expPropPanel.add(expSolSdDevButton);

        insertTab("Experimental Data", expPropPanel);
    }

    public class ExpPropSdDevActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            double sdDev = molSet.expPropSdDev();
            expPropSdDevLabel.setText((new Double(sdDev)).toString());
        }
    }

    public void close()
    {
        // make sure to stop any running threads
        if( propertyThread != null )
        {
            not.kill();
        }
        super.close();
    }

    public class TimerActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            ++seconds;
            if( seconds == 61 )
            {
                seconds = 0;
                ++minutes;
            }
            if( minutes == 61 )
            {
                minutes = 0;
                ++hours;
            }
            timerTime.setText(
                hours + ":" + minutes + ":" + seconds
            );
        }
    }

    public class DeleteActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            delete();
        }
    }
    
    public void delete()
    {
        int result = 
            JOptionPane.showConfirmDialog(
                this,
                "Are you sure you want to delete this MolSet?",
                "Confirm Delete",
                JOptionPane.YES_NO_OPTION);
        
        if( result != 0 )
        {
            // no
            return;
        }


        if( !molSet.delete(db) )
        {
            JOptionPane.showMessageDialog(
                this,
                "Delete Failed",
                "Error", 
                JOptionPane.ERROR_MESSAGE );
        }
        else
        {
            JOptionPane.showMessageDialog(
                this,
                "MolSet Deleted",
                "Finished", 
                JOptionPane.PLAIN_MESSAGE );
        }

        parent.remove(this);
    }

    public class ExpPropFileActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            expPropFile();
        }
    }

    public void expPropFile()
    {
        //JFileChooser chooser = new JFileChooser();
        File file;

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
           file = chooser.getSelectedFile();
           if( !molSet.setExpPropFile(file.getAbsolutePath()) )
           {
               JOptionPane.showMessageDialog(
                   this,
                   "Unable to read file",
                   "Error",
                   JOptionPane.ERROR_MESSAGE );
           }
           else
           {
               JOptionPane.showMessageDialog(
                   this,
                   "Experimental Property file set",
                   "Valid File",
                   JOptionPane.PLAIN_MESSAGE );
               if( !molSet.save(db) )
               {
                   logger.warn("Unable to save MolSet");
               }
               displayExpPropPanel();
               revalidate();
           }
        }
    }

    /********************************************************
     * Displays the details of the given mol in a new tabbed
     * panel.
     ********************************************************/
    public void displayMol(Mol mol)
    {
        MLR algorithm = getAlgorithm();
        MolPanel molPanel = new MolPanel(parent,mol,algorithm);
        parent.add(molPanel);
    }

    /**********************************************************
     * Inserts the given component with the given title as
     * a tab in the tabbedPane. If a tab with the same title
     * is already displayed, it is removed and replaced.
     * The current selected index remains selected after 
     * calling this function.
     *********************************************************/
    public void insertTab(String title, Component component)
    {
        int selectedIndex = tabbedPane.getSelectedIndex();
        int index = tabbedPane.indexOfTab(title);
        if( index >= 0 )
        {
            tabbedPane.remove(index);
            tabbedPane.insertTab(title,null,component,null,index);
        }
        else
        {
            tabbedPane.addTab(title,component);
        }

        if( selectedIndex >= 0 )
        {
            tabbedPane.setSelectedIndex(selectedIndex);
        }
    }
	
	/*********************************************************
	* Set the File chooser to be used
	*********************************************************/
	public void setJFileChooser(JFileChooser chsr)
	{
		this.chooser=chsr;
	}
}
