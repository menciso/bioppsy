package bioppsy.user;

//property

// gui
import javax.swing.*; 
import javax.swing.event.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;
import bioppsy.descriptors.PropertyDescriptor;




//java
import java.io.File;
import java.util.Vector;

//JOELib
import joelib.desc.*;

/*****************************************************
 * A tabbed pane panel that displays an Algorithm's details
 * and provides all the functionality for manipulating
 * the Algorithm.
 *******************************************************/
class AlgPanel extends CloseablePanel
{
	JFileChooser chooser;
    Database db;

    public static int TYPE_COLUMN_INDEX = 1;

    Vector descriptors;
    JTable descriptorsTable;
    JTable paramsTable;
    JTable detailTable;

    JTextField smartsFilename;

    JButton calculateParametersButton;
    JButton saveButton;
    JButton deleteButton;
    JPopupMenu extraFunctionsMenu;
    JTextField propFile = new JTextField(20);

    MLR alg;

    JPanel contentPanel = null;
    JPanel paramPanel = null;
 
    Popup smartsPopup = null;

    StartCalculateParametersActionListener startCPActionListener;
    StopCalculateParametersActionListener stopCPActionListener;
    TrainThread trainThread = null;    

    String[] paramTypes = Constants.instance().getStringArray("PARAM_TYPES");
    String atomChoiceString = 
        paramTypes[Constants.instance().getInt("ATPARAM_INDEX")];
    String groupChoiceString = 
        paramTypes[Constants.instance().getInt("GROUP_PARAM_INDEX")];
    ButtonGroup rbGroup;

    static Logger logger = Logger.getLogger(AlgPanel.class);

    public AlgPanel(GUI parent, Database db)
    {
        super(parent);
        this.db = db;
    }

    public String getTitle()
    {
        return "Algorithm";
    }

    public void addMenuItems(JPanel menuPanel)
    {
    	calculateParametersButton = new JButton("Start Train");
    	startCPActionListener = new StartCalculateParametersActionListener();
    	stopCPActionListener = new StopCalculateParametersActionListener();
    	calculateParametersButton.addActionListener(
    			startCPActionListener );
    	menuPanel.add(calculateParametersButton);

    	saveButton = new JButton("Save");
    	saveButton.addActionListener(
    			new SaveActionListener() );
    	menuPanel.add(saveButton);


    	deleteButton = new JButton("Delete");
    	deleteButton.addActionListener(
    			new DeleteActionListener() );
    	menuPanel.add(deleteButton);
    }

    /**********************************************************
     * Adds a button to the given JPanel that, when pressed,
     * shows a menu of extra functions.
     ***********************************************************/
    public void createExtraFunctionsMenu(JPanel menuPanel)
    {
        JLabel popupButton = new JLabel("More..");
        popupButton.setBorder(BorderFactory.createRaisedBevelBorder());
        menuPanel.add(popupButton);
        extraFunctionsMenu = new JPopupMenu();
        JMenuItem unmatchedPlot = new JMenuItem("Plot Unmatched Atoms vs Error");
        unmatchedPlot.addActionListener(new UnmatchedPlotActionListener());
        extraFunctionsMenu.add(unmatchedPlot);
        popupButton.addMouseListener(new ExtraFunctionsPopupListener());
    }

    /*****************************************************************
     * A MouseListener class. When the mouse is pressed, if the
     * extraFunctionsMenu popup menu is not visible it is shown.
     * If it is visible it is hidden.
     ****************************************************************/
    public class ExtraFunctionsPopupListener extends MouseAdapter
    {
        public void mousePressed(MouseEvent e) 
        {
            if( extraFunctionsMenu.isVisible() )
            {
                hidePopup(e);
            }
            else
            {
                showPopup(e);
            }
        }

        private void showPopup(MouseEvent e) 
        {
            extraFunctionsMenu.show(e.getComponent(),
                e.getX(), e.getY());
        }

        private void hidePopup(MouseEvent e)
        {
            extraFunctionsMenu.setVisible(false);
        }
    }

    public class UnmatchedPlotActionListener 
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            unmatchedPlot();
        }
    }
  
    /******************************************************
     * This function displays a chart. For each Mol in this
     * Algorithm's MolSet, the of the number of atoms in the Mol
     * that do not match any ATParams in this Algorithm
     * are is charted against the error.
     *******************************************************/
    public void unmatchedPlot()
    {
        UAtomsPlotPanel uapp = new UAtomsPlotPanel
        (
            alg.getMolSet(),
            alg,
            parent 
        );
        parent.add(uapp);
        uapp.load();
    }

    public class SaveActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            save();
        }
    }

    public void save()
    {
        if( !alg.save(db) )
        {
            JOptionPane.showMessageDialog(
                this,
                "Save Failed",
                "Error", 
                JOptionPane.ERROR_MESSAGE );
        }
        else
        {
            JOptionPane.showMessageDialog(
                this,
                "Algorithm Saved",
                "Finished", 
                JOptionPane.PLAIN_MESSAGE );
        }
    }

    public class StartCalculateParametersActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            setCalculatingParameters();
            trainThread = new TrainThread("TrainThread");
            trainThread.start();
        }
    }

    public class TrainThread extends Thread {

        public TrainThread(String str) 
        {
            super(str);
        }

        public void run() 
        {
            try
            {
                calculateParameters();
            }
            catch( Exception e )
            {
                handleException(e);
            }
            setFinishedCalculatingParameters();
        }
    }

    public void handleException(Exception e)
    {
        logger.error(e.getMessage());
        e.printStackTrace();
        JOptionPane.showMessageDialog(
            this,
            e.getMessage(),
            "Error", 
            JOptionPane.ERROR_MESSAGE );  
    }

    public boolean calculateParameters()
    {
        // Check to make sure that the MolSet has correct 
        // experimental property values
        if( !alg.getMolSet().haveExpProp() )
        {
//            int result = 
//                JOptionPane.showConfirmDialog(
//                    this,
//                    "Experimental Property values were not found " +
//                    "for all molecules." +
//                    "Do you wish to load these from a file?",
//                    "Experimental Properties not complete",
//                    JOptionPane.YES_NO_OPTION);
                	JOptionPane.showMessageDialog(
                              this,
                                "No experimental properties found",
                                "Error", 
                                JOptionPane.ERROR_MESSAGE );
                        display();
                        return false;
        
//            if( result != 0 )
//            {
//                // no
//                return false;
//            }
//
//            //JFileChooser chooser = new JFileChooser();
//            File file;
//            boolean haveFile = false;
//
//            while( !haveFile )
//            {
//                int returnVal = chooser.showOpenDialog(this);
//                if(returnVal == JFileChooser.APPROVE_OPTION) 
//                {
//                   file = chooser.getSelectedFile();
//                   propFile.setText(file.getAbsolutePath());
//                   alg.getMolSet().setExpPropFile(propFile.getText());
//                   System.out.print("expprop file cero " + alg.getMolSet().getExpPropFile() + "\n");
//                   System.out.print("expprop file path - " + alg.getMolSet().getExpPropFile().getAbsolutePath() + "\n"); 
//                   if( !alg.getMolSet().setExpPropFile(propFile.getName()) )
//                   {
//                       result = JOptionPane.showConfirmDialog(
//                           this,
//                           "The chosen file is not readable. Do you wish to choose "+
//                           "another file?",
//                           "File Error",
//                           JOptionPane.YES_NO_OPTION);
//                       if( result != 0 )
//                       {
//                           return false;
//                       }
//                   }
//                   haveFile = true;
//                }
//                else
//                {
//                    return false;
//                }
//            }
        }

        if( !alg.calculateParameters() )
        {
            logger.error("Failed to calculate Parameters");
            JOptionPane.showMessageDialog(
                    this,
                    "Failed to calculate Parameters",
                    "Error", 
                    JOptionPane.ERROR_MESSAGE );
            display();
            return false;
        }

        JOptionPane.showMessageDialog(
            this,
            "Finished Calculating Parameters",
            "Finished", 
            JOptionPane.PLAIN_MESSAGE );

        display();

        return true;
    }

    public class StopCalculateParametersActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            trainThread.stop();
            setFinishedCalculatingParameters();
        }
    }

    public void setCalculatingParameters()
    {
        calculateParametersButton.setText("Stop Train");
        calculateParametersButton.removeActionListener(
            startCPActionListener );
        calculateParametersButton.addActionListener(
            stopCPActionListener );
        revalidate();
    }

    public void setFinishedCalculatingParameters()
    {
        calculateParametersButton.setText("Start Train");
        calculateParametersButton.removeActionListener(
            stopCPActionListener );
        calculateParametersButton.addActionListener(
            startCPActionListener );
        display();
    }

    public class DeleteActionListener
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            delete();
        }
    }

    public void delete()
    {
        int result = 
            JOptionPane.showConfirmDialog(
                this,
                "Are you sure you want to delete this Algorithm?",
                "Confirm Delete",
                JOptionPane.YES_NO_OPTION);
        
        if( result != 0 )
        {
            // no
            return;
        }


        if( !alg.delete(db) )
        {
            JOptionPane.showMessageDialog(
                this,
                "Delete Failed",
                "Error", 
                JOptionPane.ERROR_MESSAGE );
        }
        else
        {
            JOptionPane.showMessageDialog(
                this,
                "Algorithm Deleted",
                "Finished", 
                JOptionPane.PLAIN_MESSAGE );
        }

        parent.remove(this);
    }

    public void create()
    { 
        AlgCreateDialog algCD = new AlgCreateDialog(parent);
		algCD.setJFileChooser(chooser);
        algCD.setObjectListener(new CreateAlgObjectListener());
        algCD.start();
    }

    public class CreateAlgObjectListener implements ObjectListener
    {
        public void objectReady(Object obj)
        {
            createObjectReady(obj);
        }
    }

    public void createObjectReady(Object obj)
    {
        if( obj == null )
        {
            return;
        }
        alg = (MLR) obj;
        display();
        parent.add(this);
    }

    public void display()
    {
        if( contentPanel != null )
        {
            remove(contentPanel);
            contentPanel = null;
        }

        contentPanel = new JPanel();
        contentPanel.setLayout(
            new BoxLayout(contentPanel, BoxLayout.PAGE_AXIS));

        Vector columnNames = new Vector();
        columnNames.add("DESCRIPTION");
        columnNames.add("VALUE");
        Vector detailMatrix = new Vector();
        Vector row = new Vector();
        row.add("Name");
        row.add(alg.getName());
        detailMatrix.add(row);
        row = new Vector();
        row.add("Training File");
        String filename = "none";
        if( alg.getMolSet() != null )
        {
            filename = alg.getMolSet().getFile().getName();
        }
        row.add(filename);
        detailMatrix.add(row);
        row = new Vector();
        row.add("Type");
        row.add(alg.getClass().getName());
        detailMatrix.add(row);
        row = new Vector();
        row.add("R Squared");
        row.add((new Double(alg.getRsq())).toString());
        detailMatrix.add(row);
        row = new Vector();
        row.add("Standard Deviation");
        row.add((new Double(alg.getSdDev())).toString());
        detailMatrix.add(row);
       //only allow the name field to be changed
        detailTable = new JTable(detailMatrix,columnNames){
			public boolean isCellEditable(int rowIndex, int vColIndex) {
                if(rowIndex==0){
                    return true;
                }
                return false;
            }
        };
        detailTable.setAlignmentX(Component.LEFT_ALIGNMENT);
        detailTable.getModel().addTableModelListener(new ModifyMainListener());
        contentPanel.add(detailTable);

        add(contentPanel);

        displayAtomTypingParameters();

        displayDescriptorsToAdd();

        displayParameters();

        revalidate();
    }
    
  

    public void displayAtomTypingParameters()
    {
        JPanel atomParamsPanel = new JPanel();
        atomParamsPanel.setBorder(
            BorderFactory.createTitledBorder("SMARTS Patterns"));
        atomParamsPanel.setLayout(
            new BoxLayout(atomParamsPanel,BoxLayout.PAGE_AXIS));

        JPanel atomParamsFilePanel = new JPanel();
        atomParamsFilePanel.setLayout(
            new BoxLayout(atomParamsFilePanel,BoxLayout.LINE_AXIS));
        atomParamsFilePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        smartsFilename = new JTextField(30);
        atomParamsFilePanel.add(smartsFilename);
        JButton browseSmartsFile = new JButton("Browse");
        browseSmartsFile.addActionListener(
            new BrowseSmartsFileActionListener());
        atomParamsFilePanel.add(browseSmartsFile);
        JButton addSmarts = new JButton("Add");
        addSmarts.addActionListener(new AddSmartsActionListener());
        atomParamsFilePanel.add(addSmarts);
        atomParamsPanel.add(atomParamsFilePanel);

        JPanel choicesPanel = new JPanel();
        choicesPanel.setLayout(new BoxLayout(choicesPanel,BoxLayout.LINE_AXIS));
        choicesPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        JRadioButton atomTypingChoice = new JRadioButton("Atomic Typing Parameters");
        atomTypingChoice.setActionCommand(atomChoiceString);
        atomTypingChoice.setSelected(true);
        choicesPanel.add(atomTypingChoice);
        JRadioButton groupContributionChoice = new JRadioButton("Group Contribution Parameters");
        groupContributionChoice.setActionCommand(groupChoiceString);
        groupContributionChoice.setSelected(true);
        groupContributionChoice.setAlignmentX(Component.LEFT_ALIGNMENT);
        choicesPanel.add(groupContributionChoice);
        rbGroup = new ButtonGroup();
        rbGroup.add(atomTypingChoice);
        rbGroup.add(groupContributionChoice);
        atomParamsPanel.add(choicesPanel);

        atomParamsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        contentPanel.add(atomParamsPanel);
    }

    public class BrowseSmartsFileActionListener 
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            browseSmartsFile();
        }
    }

    public void browseSmartsFile()
    {
        //JFileChooser chooser = new JFileChooser();
        File file;

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
           file = chooser.getSelectedFile();
           smartsFilename.setText(file.getAbsolutePath());
           revalidate();
        }
    }

    public class AddSmartsActionListener 
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            addSmarts();
        }
    }

    public void addSmarts()
    {
        boolean result = alg.addSmarts
        (
            smartsFilename.getText().trim(),
            rbGroup.getSelection().getActionCommand()
        );
        if( result )
        {
            JOptionPane.showMessageDialog
            (
                this,
                "Atomic typing parameters added",
                "SMARTS added",
                JOptionPane.PLAIN_MESSAGE
            );

            displayParameters();
        }
        else
        {
            JOptionPane.showMessageDialog
            (
                this,
                "Error adding atomic typing parameters",
                "Error",
                JOptionPane.ERROR_MESSAGE
            );
        }
    }
    

    public void displayDescriptorsToAdd()
    {
        Vector row;
        Vector columnNames;
        Vector matrix;

        columnNames = new Vector();
        columnNames.add("NAME");
        columnNames.add("DESCRIPTION");

        matrix = new Vector();

        try
        {
            descriptors = getDescriptors();
            Descriptor descriptor;
            for( int i=0; i < descriptors.size(); ++i )
            {
                descriptor = (Descriptor) descriptors.elementAt(i);
                DescriptorInfo descInfo = descriptor.getDescInfo();
                row = new Vector();
                row.add(descInfo.getName());
                PropertyDescriptor sd = (PropertyDescriptor) descriptor;
                row.add(sd.getTextDescription());
                matrix.add(row);
            }
        }
        catch( DescriptorException de )
        {
            logger.error(de.getMessage());
            return;
        }

        descriptorsTable = new JTable(matrix,columnNames){

        	public boolean isCellEditable(int rowIndex, int vColIndex) {
                return false;
            }
        };
        JScrollPane scrollPane = new JScrollPane(descriptorsTable);
        scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        JPanel descriptorsPanel = new JPanel();
        descriptorsPanel.setPreferredSize(new Dimension(100,400));
        descriptorsPanel.setBorder(
            BorderFactory.createTitledBorder("Molecular Descriptors"));
        descriptorsPanel.setLayout(
            new BoxLayout(descriptorsPanel, BoxLayout.PAGE_AXIS));
        JPanel pbPanel = new JPanel();
        pbPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        pbPanel.setLayout(new BoxLayout(pbPanel,BoxLayout.LINE_AXIS));
        descriptorsPanel.add(pbPanel);
        descriptorsPanel.add(scrollPane);
        descriptorsTable.doLayout();
        JButton addDescriptors = new JButton("Add Descriptors");
        addDescriptors.addActionListener(new AddDescriptorsActionListener());
        descriptorsPanel.add(addDescriptors);
        descriptorsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        contentPanel.add(descriptorsPanel);
    }

    public Vector getDescriptors()
        throws DescriptorException
    {
        Vector nativeDescs = DescriptorHelper.instance().getNativeDescs();
        int size = nativeDescs.size();
        String descName;
        Descriptor descriptor;
        Vector descriptors = new Vector();
        for (int i=0; i < size; i++)
        {
            descName = (String) nativeDescs.get(i);
            descriptor = DescriptorFactory.getDescriptor(descName);
            if( descriptor != null )
            {
                try
                {
//                    java.lang.reflect.Field field = 
                        descriptor.getClass().getField("PROPERTY_DESCRIPTOR");
                    descriptors.add(descriptor);
                }
                catch( NoSuchFieldException nsfe )
                { // do nothing
                }
            }    
        }
        return descriptors;
    }

    public class AddDescriptorsActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            addDescriptors();
        }
    }

    private void addDescriptors()
    {
        int[] rows = descriptorsTable.getSelectedRows();
        if( rows.length <= 0 )
        {
            return;
        }
        int count = 0;
        for( int i=0; i < rows.length; ++i )
        {
            if( rows[i] < descriptors.size() )
            {
                ++count;
                Descriptor descriptor = (Descriptor)
                    descriptors.elementAt(rows[i]);
                addDescriptor(descriptor);
            }
        }
        JOptionPane.showMessageDialog(
            parent,
            count + " Descriptors have been added",
            "Descriptors Added", 
            JOptionPane.PLAIN_MESSAGE );

        displayParameters();
    }

    private void addDescriptor(Descriptor descriptor)
    {
        DescriptorParam descParam = new DescriptorParam(alg);
        descParam.setDescriptor(descriptor);
        alg.getParamList().add(descParam);
    }

    public class RemoveParamsActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            removeParams();
        }
    }

    public void removeParams()
    {
        int[] rows = paramsTable.getSelectedRows();
        int count = 0;
        if( rows.length <= 0 )
        {
            return;
        }
        // we have to start from the end, otherwise the
        // indices in the ParamList will change
        java.util.Arrays.sort(rows);
        for( int i = (rows.length - 1); i >= 0; --i )
        {
            try
            {
                alg.getParamList().removeElementAt(rows[i]);
                ++count;
            }
            catch( ArrayIndexOutOfBoundsException ae )
            {
                logger.warn("Tried to remove a Param from ParamList that " +
                    " does not exist: " + rows[i]);
            }
        }
        JOptionPane.showMessageDialog(
            parent,
            count + " Parameters have been removed",
            "Parameters Removed", 
            JOptionPane.PLAIN_MESSAGE );

        alg.getParamList().reIndex();

        displayParameters();
    }

    public class PlotParamActionListener
       implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            plotParam();
        }
    }

    public void plotParam()
    {
        int row = paramsTable.getSelectedRow();
        if( row < 0 )
        {
            logger.debug("No parameter selected");
            return;
        }

        Param param = alg.getParamList().paramAt(row);

        ErrorPlotPanel errorPlotPanel
            = new ErrorPlotPanel
            (
                alg.getMolSet(),
                param,
                alg,
                parent 
            );

        parent.add(errorPlotPanel);
        errorPlotPanel.load();
    }

    public void displayParameters()
    {
        // remove current parameters if there
        // are any
        if( paramPanel != null )
        {
            contentPanel.remove(paramPanel);
        }

        paramPanel = new ParameterPanel(alg,parent,detailTable);
        paramPanel.setBorder(
            BorderFactory.createTitledBorder("Current Parameters"));
        paramPanel.setLayout(
            new BoxLayout(paramPanel, BoxLayout.PAGE_AXIS));
        paramPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        contentPanel.add(paramPanel);

        revalidate();
    }

    public class TableMouseListener extends MouseAdapter
    {
        public void mousePressed(MouseEvent e)
        {
            showATParamSmartsPopup(e);
        }

        public void mouseReleased(MouseEvent e)
        {
            hideATParamSmartsPopup();
        }
    }

    /*******************************************************
     * Display the ATParam smarts patterns as a popup next
     * to where the mouse cursor is.
     *******************************************************/
    public void showATParamSmartsPopup(MouseEvent e)
    {
        int row = paramsTable.rowAtPoint(e.getPoint());
        int column = paramsTable.columnAtPoint(e.getPoint());
        Point tableLocation = paramsTable.getLocationOnScreen();

        String[] paramTypes = Constants.instance().getStringArray(
            "PARAM_TYPES");
        String atParamClass = paramTypes[Constants.instance().getInt(
            "ATPARAM_INDEX")];
        String groupParamClass = paramTypes[Constants.instance().getInt(
            "GROUP_PARAM_INDEX")];

        // Padding to the right of where the mouse is
        // otherwise the cursor hides the popup
        int xPadding = 15;

        if( column == TYPE_COLUMN_INDEX )
        {
            Param param = alg.getParamList().paramAt(row);
            ATParam atParam;
            if( (param.getClass().getName().equals(atParamClass)) ||
                (param.getClass().getName().equals(groupParamClass)) )
            {
                atParam = (ATParam) param;
            }
            else
            {
                return;
            }

            Vector smarts = atParam.getSmartsPatterns();

            JPanel popupPanel = new JPanel();
            popupPanel.setLayout(new BoxLayout(
                popupPanel, BoxLayout.PAGE_AXIS));
            for( int i=0; i < smarts.size(); ++i )
            {
                JLabel label = new JLabel(
                    (String) smarts.elementAt(i));
                if( i == 0 )
                {
                    label.setAlignmentX(Component.LEFT_ALIGNMENT);
                }
                popupPanel.add(label);
            }

            PopupFactory factory = PopupFactory.getSharedInstance();
            smartsPopup = factory.getPopup
            (
                paramsTable,
                popupPanel,
                e.getPoint().x + tableLocation.x + xPadding,
                e.getPoint().y + tableLocation.y
            );
            smartsPopup.show();
        }
    }

    public void hideATParamSmartsPopup()
    {
        if( smartsPopup != null )
        {
            smartsPopup.hide();
            smartsPopup = null;
        }
    }

    public boolean open()
    {
        alg = getAlgorithm(this,db);
        if( alg == null )
        {
            return false;
        }

        display();

        return true;
    }

    public static MLR getAlgorithm(JPanel parent, Database db)
    {
        String name = null;
        Vector names = null;
        MLR ret;

        names = MLR.getNames(db);
        if( names == null || names.size() <= 0 )
        {
            JOptionPane.showMessageDialog(
                parent,
                "There are no Algorithms to load",
                "Error", 
                JOptionPane.ERROR_MESSAGE );
            return null;
        }

        name = (String)JOptionPane.showInputDialog(
            parent,
            "Choose an Algorithm to load:",
            null,
            JOptionPane.PLAIN_MESSAGE,
            null,
            names.toArray(),
            names.elementAt(0));
        if( name == null )
        {
            return null;
        }

        ret = MLR.getAlgorithm(name,db);

        if( ret == null )
        {
            JOptionPane.showMessageDialog(
                parent,
                "Algorithm was not found: " + name,
                "Error", 
                JOptionPane.ERROR_MESSAGE );
            return null;
        }

        return ret;
    }

    public void close()
    {
        if( trainThread != null )
        {
            trainThread.stop();
        }

        super.close();
    }

    /************************************************
     * Does any cleanup actions and redraws the panel
     ************************************************/
    public void revalidate()
    {
        // hide the popup in case we missed any
        // mouse releasing events
        hideATParamSmartsPopup();

        super.revalidate();
    }
	
	/*********************************************************
	* Set the File chooser to be used
	*********************************************************/
	public void setJFileChooser(JFileChooser chsr)
	{
		this.chooser=chsr;
	}

    public class ModifyMainListener implements TableModelListener
    {
        public void tableChanged(TableModelEvent e)
        {
            TableModel model = (TableModel)e.getSource();
            int row = e.getFirstRow();
            int column=e.getColumn();
            Object data=model.getValueAt(row, column);
            //only change the value column
            if(column==1){
                //row numbers correspond to the rows added in display() function
                switch(row){
                    case 0: //Name
                        alg.setName(data.toString());
                        alg.setModified();
                        break;
                    case 3: //R^2
                        alg.setRsq(Double.valueOf(data.toString()).doubleValue());
                        alg.setModified();
                        break;
                    case 4: //Std Div
                        alg.setSdDev(Double.valueOf(data.toString()).doubleValue());
                        alg.setModified();
                        break;
                }
            }
        }
    }
    


}


