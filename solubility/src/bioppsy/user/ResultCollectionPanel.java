package bioppsy.user;


// gui
import javax.swing.*; 
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.Mol;
import bioppsy.NotFoundException;
import bioppsy.Result;
import bioppsy.ResultCollection;


//java
import java.util.Vector;


class ResultCollectionPanel extends JPanel
{ 
	ResultCollection rc;
    RCNPanel parent;
    JTable table;

    static Logger logger = Logger.getLogger(ResultCollectionPanel.class);

    public ResultCollectionPanel(ResultCollection rc, RCNPanel parent)
    {
        super();
        this.parent = parent;
        // so that the table's maximum size is taken into account
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.rc = rc;
        display();
    }

    public void display()
    {
        Vector columnNames = new Vector();
        columnNames.add("NUMBER");
        columnNames.add("MOL ID");
        columnNames.add("CALC");
        columnNames.add("EXP");

        Vector matrix = new Vector();
  
        for( int i=0; i < rc.size(); ++i )
        {
            Result result = (Result) rc.elementAt(i);
            Mol mol = result.getMol();
            Vector row = new Vector();
            row.add((new Integer(mol.getNumber())).toString());
            row.add(mol.getMolId());
            row.add((new Double(result.getValue())).toString());
            try
            {
                row.add((new Double(mol.getExpProp())).toString());
            }
            catch(NotFoundException nfe)
            {
                row.add("none");
            }
            matrix.add(row);
        }

        table = new JTable(matrix,columnNames);
        table.addMouseListener(new TableMouseListener());
        JScrollPane scrollpane = new JScrollPane(table);
        add(scrollpane);
    }

    public class TableMouseListener extends MouseAdapter
    {
        public void mousePressed(MouseEvent me)
        {
            if( me.getClickCount() == 2 )
            {
                int element = table.rowAtPoint(me.getPoint());
                Result result = (Result) rc.elementAt(element);
                parent.displayResult(result);
            }
        }
    }
}
