package bioppsy.user;


// gui
import javax.swing.*; 

import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bioppsy.Constants;
import bioppsy.Database;
import bioppsy.FailedConstructorException;



public class GUI extends JFrame
{
	//a single file chooser is used to allow for remembering of locations
	JFileChooser chooser = new JFileChooser();
    JTabbedPane tabbedPanel = null;
    JPanel welcomePanel = null;
    JMenuBar menuBar;
    JMenu molSetMenu;
    JMenuItem openMolSet;
    JMenu algMenu;
    JMenuItem newAlg;
    JMenuItem openAlg;
    JMenuItem renameAlg;
    JMenuItem quit;
    JMenuItem about;
    Database db;

    static Logger logger = Logger.getLogger(GUI.class);

    public GUI(String title)
    {
        super(title);
    	BasicConfigurator.configure();
    }

    /*************************************************
     * Adds the given CloseablePanel to this GUI's 
     * JTabbedPane with the title taken from the
     * CloseablePanel.getTitle() method.
     *************************************************/
    public void add(CloseablePanel panel)
    {
        // Set up the tab panel if it is null
        if( tabbedPanel == null )
        {
            tabbedPanel = new JTabbedPane();
            if( welcomePanel != null )
            {
                getContentPane().remove(welcomePanel);
            }
            getContentPane().add(tabbedPanel);
        }

        tabbedPanel.addTab(panel.getTitle(), panel);
        tabbedPanel.setSelectedComponent(panel);
    }

    /************************************************
     * Removed the given CloseablePanel from this GUI's
     * JTabbedPane.
     *************************************************/
    public void remove(CloseablePanel panel)
    {
        tabbedPanel.remove(panel);
        
        // show the welcome screen if there are no more
        // tabbed panels
        if( tabbedPanel.getTabCount() <= 0 )
        {
            getContentPane().remove(tabbedPanel);
            tabbedPanel = null;
            showWelcomeScreen();
        }
    }

    public static void main(String[] args)
    {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        System.out.println("Home="+System.getProperty("user.home"));
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                GUI gui = new GUI("BioPPSy");
                gui.createAndShowGUI();
            }
        });
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private void createAndShowGUI() {

        // configure the logger
        String filename = Constants.instance().getString("LOG_CONFIG_FILE");
        filename = "/" + filename;
        java.net.URL url = getClass().getResource(filename);
        PropertyConfigurator.configure(url);

        JFrame.setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addWindowListener(new GUIWindowListener());

        logger.info("About to connect to database");

        if( !connectToDatabase() )
        {
            logger.error("Unable to connect to the database");
            System.exit(0);
        }
        createMenu();

        showWelcomeScreen();

        //Display the window.
        pack();
        setSize(500,650);
        position();
        setVisible(true);
    }

    public void showWelcomeScreen()
    {
        welcomePanel = new JPanel();
      //  welcomePanel.setLayout(new BoxLayout(welcomePanel,BoxLayout.PAGE_AXIS));
		welcomePanel.setLayout(new BorderLayout());
		
		ImageIcon icon = new ImageIcon(Constants.instance().getString("OPENING_IMAGE_FILENAME"));
		JLabel image = new JLabel(icon);
		welcomePanel.add(image,BorderLayout.CENTER);
        getContentPane().add(welcomePanel);
    }

    public class GUIWindowListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent e)
        {
            if( db != null )
            {
                db.close();
            }
        }
    }

    public void position()
    {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension size = getSize();
        int x = (int) ((dim.getWidth()/2)-(size.getWidth()/2));
        int y = (int) ((dim.getHeight()/2)-(size.getHeight()/2));
        setLocation(x,y);
    }

    /**************************************************
     * Connects to the database by creating a new Database
     * object, returns true on success, false on failure.
     *****************************************************/
    public boolean connectToDatabase()
    {
        try
        {
            db = new Database(
                Constants.instance().getString("DATABASE_INI_FILENAME"));
        }
        catch( FailedConstructorException fce )
        {
            logger.error(fce.getMessage());
            return false;
        }
        return true;
    }

    /*******************************************************
     * Creates the menu.
     *******************************************************/
    public void createMenu()
    {
        menuBar = new JMenuBar();

        molSetMenu = new JMenu("Molecule Set");
        openMolSet = new JMenuItem("Open");
        openMolSet.addActionListener(new OpenMolSetActionListener());
        molSetMenu.add(openMolSet);

        /*
        JMenu openChoice = new JMenu("Chose From List");
        Vector names = MolSet.getFilenames(db);
        for( int i=0; i < names.size(); ++i )
        {
            String name = (String) names.elementAt(i);
            JMenuItem item = new JMenuItem(name);
            openChoice.add(item);
        }
        molSetMenu.add(openChoice);
        */

        menuBar.add(molSetMenu);

        algMenu = new JMenu("Algorithm");
        newAlg = new JMenuItem("New");
        newAlg.addActionListener(new NewAlgActionListener());
        algMenu.add(newAlg);
        openAlg = new JMenuItem("Open");
        openAlg.addActionListener(new OpenAlgActionListener());        
        algMenu.add(openAlg);
        renameAlg = new JMenuItem("Rename");
        renameAlg.addActionListener(new RenameAlgActionListener());
        algMenu.add(renameAlg);
        menuBar.add(algMenu);
        
        JMenuItem about = new JMenuItem("About");
        about.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent event) {
            AboutDialog ad = new AboutDialog();
            ad.setVisible(true);
          }
        });
        menuBar.add(about);

        
        JMenu quitMenu=new JMenu("Quit");
        quit = new JMenuItem("Confirm");
        quit.addActionListener(new QuitActionListener());
        quitMenu.add(quit);
        menuBar.add(quitMenu);

        setJMenuBar(menuBar);
    }

    /*****************************************************
     * Listens for actions to the openMolSet JMenuItem
     ****************************************************/
    public class OpenMolSetActionListener 
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            openMolSet();
        }
    }
    
    class AboutDialog extends JDialog {
    	  public AboutDialog() {
    	    setTitle("About");
    	    GridBagConstraints c = new GridBagConstraints();
    	    setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
    	    c.fill = GridBagConstraints.HORIZONTAL;
        	c.insets = new Insets(10,10,0,0);
    	    add(Box.createRigidArea(new Dimension(0, 10)));

    	    JLabel name = new JLabel("<html><p>This program is " +
			"free software: you can redistribute it and/or " +
			"modify it under the terms of the GNU General " +
			"Public License as published by the Free Software " +
			"Foundation, either version 3 of the License, " +
			"or (at your option) any later version.<p>" +
			"This program is distributed in the hope that it will " +
			"be useful, but WITHOUT ANY WARRANTY; without even " +
			"the implied warranty of MERCHANTABILITY or FITNESS FOR " +
			"A PARTICULAR PURPOSE.  See the GNU General Public License " +
			"for more details. <p>" +
			"You should have received a copy of the GNU General " +
			"Public License along with this program.  If not, see " +
			"&lt;http://www.gnu.org/licenses/&gt;.</html>");
    	    name.setAlignmentX(0.5f);
    	    add(name);

    	    add(Box.createRigidArea(new Dimension(0, 100)));
    	    JButton close = new JButton("Close");
    	    close.addActionListener(new ActionListener() {
    	      public void actionPerformed(ActionEvent event) {
    	        dispose();
    	      }
    	    });

    	    close.setAlignmentX(0.5f);
    	    add(close);
    	    setModalityType(ModalityType.APPLICATION_MODAL);
    	    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    	    setSize(500, 350);
    	  }
    	}
    public class QuitActionListener 
    implements ActionListener
{
    public void actionPerformed(ActionEvent e)
    {
    	 if( db != null )
         {
             db.close();
         }
        System.exit(0);
    }
}
    
    public void openMolSet()
    {
        MolSetPanel msp = new MolSetPanel(this,db);
		msp.setJFileChooser(chooser);
        if( msp.open() )
        {
            add(msp);
        }
    }

    /*****************************************************
     * Listens for actions to the newAlg JMenuItem
     ****************************************************/
    public class NewAlgActionListener 
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            newAlg();
        }
    }

    public void newAlg()
    {
        AlgPanel algPanel = new AlgPanel(this,db);
		algPanel.setJFileChooser(chooser);
        algPanel.create();
    }

    /*****************************************************
     * Listens for actions to the newAlg JMenuItem
     ****************************************************/
    public class RenameAlgActionListener 
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            renameAlg();
        }
    }

    public void renameAlg()
    {
       // AlgPanel algPanel = new AlgPanel(this,db);
      //  algPanel.rename();
       // algPanel.close();
    	AlgRenameDialog algRD = new AlgRenameDialog(this,db);
	//	algRD.setJFileChooser(chooser);
     //   algRD.setObjectListener(new RenameAlgObjectListener());
        algRD.start();
    }
    

    /*****************************************************
     * Listens for actions to the openAlg JMenuItem
     ****************************************************/
    public class OpenAlgActionListener 
        implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            openAlg();
        }
    }

    public void openAlg()
    {
        AlgPanel algPanel = new AlgPanel(this,db);
		algPanel.setJFileChooser(chooser);
        if( algPanel.open() )
        {
            add(algPanel);
        }
    }
}
