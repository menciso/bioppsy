package bioppsy.user;

//property

// gui
import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//java
import java.io.File;

class RCNPanel extends JPanel
{
	JFileChooser chooser;
    ResultCollectionNavigator rcn = null;
    ResultCollectionPanel rcp = null;
    JPanel statsPanel = null;
    // users choice of which field to sort
    // the list by
    JComboBox sortFieldCombo = null;
    Database db;
    GUI parent;

    JButton first,previous,next,last;
    JButton delete,statistics;

    ActionListener statsActionListener, removeStatsActionListener;

    static Logger logger = Logger.getLogger(RCNPanel.class);

    public RCNPanel(GUI parent,ResultCollectionNavigator rcn, Database db)
    {
        this.rcn = rcn;
        this.db = db;
        this.parent = parent;
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        setAlignmentX(Component.LEFT_ALIGNMENT);
        addMenuItems();
        addSortPanel();
        //first();
    }

    public ResultCollectionNavigator getRCN()
    {
        return rcn;
    }

    public void addMenuItems()
    {
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new BoxLayout(menuPanel,
            BoxLayout.LINE_AXIS));
        menuPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        first = new JButton();
        first.addActionListener(
            new FirstActionListener() );
        first.setText("FIRST");
        menuPanel.add(first);

        previous = new JButton();
        previous.addActionListener(
            new PreviousActionListener() );
        previous.setText("PREV");
        menuPanel.add(previous);

        next = new JButton();
        next.addActionListener(
            new NextActionListener() );
        next.setText("NEXT");
        menuPanel.add(next);

        last = new JButton();
        last.addActionListener(
            new LastActionListener() );
        last.setText("LAST");
        menuPanel.add(last);

        statistics = new JButton();
        statsActionListener = new StatisticsActionListener();
        statistics.addActionListener(statsActionListener);
        statistics.setText("STATS");
        menuPanel.add(statistics);

        delete = new JButton("DELETE");
        delete.addActionListener(
            new DeleteActionListener() );
        menuPanel.add(delete);

        add(menuPanel);
    }

    public String getTitle()
    {
        return "Results";
    }

    /****************************************************
     * Adds the panel at the top that allows the user to 
     * choose which field to sort by
     ****************************************************/
    public void addSortPanel()
    {
        JPanel sortAndPrintPanel = new JPanel();
        sortAndPrintPanel.setLayout( new BoxLayout(sortAndPrintPanel,
            BoxLayout.LINE_AXIS) );
        sortAndPrintPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        JPanel sortByPanel = new JPanel();
        sortByPanel.setLayout( new BoxLayout(sortByPanel,
            BoxLayout.LINE_AXIS) );
        sortByPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        sortByPanel.setBorder(BorderFactory.createTitledBorder("Sort By:"));

        String[] choices = Constants.instance().getStringArray("RESULT_FIELDS");
        sortFieldCombo = new JComboBox(choices);
        sortFieldCombo.setMaximumSize(new Dimension(140,30));
        sortByPanel.add(sortFieldCombo);
        JButton changeSortField = new JButton("Sort");
        changeSortField.addActionListener(new SortActionListener());
        sortByPanel.add(changeSortField);

        sortAndPrintPanel.add(sortByPanel);
        
        JPanel printPanel = new JPanel();
        printPanel.setBorder(BorderFactory.createTitledBorder("Print"));
        printPanel.setLayout( new BoxLayout(printPanel,
            BoxLayout.LINE_AXIS) );
        JButton printResultsButton = new JButton("Print All To File");
        printResultsButton.addActionListener(new PrintResultsActionListener());
        printPanel.add(printResultsButton);
        sortAndPrintPanel.add(printPanel);

        add(sortAndPrintPanel);
    }

    public class SortActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            sortFields();
        }
    }

    /*******************************************************
     * Changes the sorting field to the one chosen in the
     * sortFieldCombo JComboBox and displays first() again.
     ******************************************************/
    public void sortFields()
    {
        String selectedField = (String) sortFieldCombo.getSelectedItem();

        if( !rcn.setOrderBy(selectedField) )
        {
            logger.debug("Invalid field to sort ResultCollection by: " +
                selectedField);
            return;
        }
        first();
    }

    public class PrintResultsActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            printResults();
        }
    }

    /*******************************************************
     * Prints all the results for the given RCN to a file
     * chosen by the user in a FileDialog.
     *******************************************************/
    public void printResults()
    {
        if( rcn == null )
        {
            return;
        }

        //JFileChooser chooser = new JFileChooser();
        File file;

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
            file = chooser.getSelectedFile();
            if( !rcn.printResultsToFile(file) )
            {
                JOptionPane.showMessageDialog(
                    this,
                    "Failed to Print Results",
                    "Error", 
                   JOptionPane.ERROR_MESSAGE );
            }
            else
            {
                JOptionPane.showMessageDialog(
                    this,
                    "Results printed to: " + file.getPath(),
                    "Printed", 
                   JOptionPane.PLAIN_MESSAGE );
            }
        }
    }

    public class FirstActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            first();
        }
    }

    public void first()
    {
        ResultCollection rc = rcn.first();
        if( rc != null )
        {
            if( rcp != null )
            {
                remove(rcp);
            }
            rcp = new ResultCollectionPanel(rc,this);
            add(rcp);
        }
        revalidate();
    }

    public class PreviousActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            previous();
        }
    }

    public void previous()
    {
        ResultCollection rc = rcn.previous();
        if( rc != null )
        {
            if( rcp != null )
            {
                remove(rcp);
            }
            rcp = new ResultCollectionPanel(rc,this);
            add(rcp);
        }
        revalidate();
    }

    public class NextActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            next();
        }
    }

    public void next()
    {
        ResultCollection rc = rcn.next();
        if( rc != null )
        {
            if( rcp != null )
            {
                remove(rcp);
            }
            rcp = new ResultCollectionPanel(rc,this);
            add(rcp);
        }
        revalidate();
    }

    public class LastActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            last();
        }
    }

    public void last()
    {
        ResultCollection rc = rcn.last();
        if( rc != null )
        {
            if( rcp != null )
            {
                remove(rcp);
            }
            rcp = new ResultCollectionPanel(rc,this);
            add(rcp);
        }
        revalidate();
    }

    public class DeleteActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            delete();
        }
    }

    public void delete()
    {
        int result = 
            JOptionPane.showConfirmDialog(
                this,
                "Are you sure you want to delete these Results?",
                "Confirm Delete",
                JOptionPane.YES_NO_OPTION);
        
        if( result != 0 )
        {
            // no
            return;
        }

        ResultCollection rc = rcn.last();
        if( rc != null )
        {
            if( !rc.delete(db) )
            {
                JOptionPane.showMessageDialog(
                    this,
                    "Delete Failed",
                    "Error", 
                    JOptionPane.ERROR_MESSAGE );
            }
            else
            {
                JOptionPane.showMessageDialog(
                    this,
                    "Results Deleted",
                    "Finished", 
                    JOptionPane.PLAIN_MESSAGE );
                first();
            }
        }
        else
        {
            JOptionPane.showMessageDialog(
                this,
                "No Results were found",
                "Error", 
                JOptionPane.ERROR_MESSAGE );
            first();
        }
    }

    public class StatisticsActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            statistics();
        }
    }

    public class RemoveStatsActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            removeStats();
        } 
    }

    public void removeStats() 
    {
        if( statsPanel != null )
        {
            remove(statsPanel);
        }

        statistics.removeActionListener(removeStatsActionListener);
        statistics.addActionListener(statsActionListener);

        revalidate();
    }

    public void statistics()
    {
        double rsq,mue,sddev;
        int rows = 3;
        int columns = 2;

        // we must make a copy of the rcn since
        // otherwise we will loose our place
        ResultCollectionNavigator copyRcn =
            rcn.getAnother();
        rsq = copyRcn.getRsq();
        mue = copyRcn.getMUE();
        sddev = copyRcn.getSdDev();

        if( statsPanel != null )
        {
            remove(statsPanel);
        }
        if( rcp != null )
        {
            remove(rcp);
        }

        statsPanel = new JPanel();
        statsPanel.setBorder(BorderFactory.createTitledBorder("Stats"));
        statsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        statsPanel.setLayout(new GridLayout(rows,columns));
        JLabel rsqText = new JLabel("R squared");
        statsPanel.add(rsqText);
        JLabel rsqNumber = new JLabel((new Double(rsq)).toString());
        statsPanel.add(rsqNumber);
        JLabel mueText = new JLabel("Mean Unsigned Error");
        statsPanel.add(mueText);
        JLabel mueNumber = new JLabel((new Double(mue)).toString());
        statsPanel.add(mueNumber);
        JLabel sddevText = new JLabel("Standard Deviation");
        statsPanel.add(sddevText);
        JLabel sddevNumber = new JLabel((new Double(sddev)).toString());
        statsPanel.add(sddevNumber);

        add(statsPanel);

        statistics.removeActionListener(statsActionListener);
        removeStatsActionListener = new RemoveStatsActionListener();
        statistics.addActionListener(removeStatsActionListener);

        if( rcp != null )
        {
            add(rcp);
        }
        revalidate();
    }

    public void displayResult(Result result)
    {
        MolSet molSet = rcn.getMolSet();
        Mol mol = molSet.getMol(result.getMol().getNumber(),db);
        if( mol == null )
        {
            logger.debug("Mol not found: " + result.getMol().getNumber());
        }
        else
        {
            MolPanel molPanel = new MolPanel(parent,mol,rcn.getAlgorithm());
            parent.add(molPanel);
        }
    }
	
	/*********************************************************
	* Set the File chooser to be used
	*********************************************************/
	public void setJFileChooser(JFileChooser chsr)
	{
		this.chooser=chsr;
	}
}
