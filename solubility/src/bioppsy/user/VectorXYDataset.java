package bioppsy.user;


// java 
import java.util.Vector;

// jfreechart
import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.DomainOrder;

import bioppsy.util.Pair;



/*****************************************************
 * A VectorXYDataset is an XYDataset which stores the
 * data in a Vector of Pair objects. It does not
 * properly implement the XYDataset interface since
 * it only ever had one series, but it is enough
 * for our purposes.
 *****************************************************/
public class VectorXYDataset extends AbstractXYDataset
{
    private Vector data;

    public VectorXYDataset()
    {
        super();
        data = new Vector();
    }

    public void add(double x, double y)
    {
        Pair pair = new Pair(new Double(x),new Double(y));
        data.add(pair);
    }

    public DomainOrder getDomainOrder()
    {
        return DomainOrder.NONE;
    }

    public int getItemCount(int series)
    {
        return data.size();
    }

    public double getXValue(int series, int item)
    {
        return ((Double) ((Pair) data.elementAt(item)).getFirst()).doubleValue();
    }

    public Number getX(int series, int item)
    {
        return new Double(getXValue(series,item));
    }

    public double getYValue(int series, int item)
    {
        return ((Double) ((Pair) data.elementAt(item)).getSecond()).doubleValue();
    }

    public Number getY(int series, int item)
    {
        return new Double(getYValue(series,item));
    }

    public String getSeriesName(int index)
    {
        return "unnamed";
    }

    public int getSeriesCount()
    {
        return 1;
    }
}
