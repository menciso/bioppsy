package bioppsy.user;


// gui
import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//java 
import java.util.Iterator;
import java.util.Vector;


/***********************************************************************
 * A Panel that allows the user to navigate through a MolSet and
 * view the molecules.
 **********************************************************************/
public class MolSetNavigatorPanel extends JPanel
{
	static Logger logger = Logger.getLogger(MolSetNavigatorPanel.class);

    private MolSet molSet;
    private Iterator subsetI;
    private MolSubSetPanel subsetPanel = null;
    private MolSetPanel parent;

    public MolSetNavigatorPanel(MolSet molSet, MolSetPanel parent)
    {
        super();
        this.molSet = molSet;
        this.parent = parent;
        display();
    }

    public void display()
    {
        setAlignmentX(Component.LEFT_ALIGNMENT);
        setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        setBorder(BorderFactory.createTitledBorder("Browse Molecules"));

        JPanel buttonPanel = new JPanel();
        buttonPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
        String DisplayLength = Constants.instance().getString("MOL_SUBSET");
        JButton firstButton = new JButton("FIRST " + DisplayLength);
        firstButton.addActionListener(new FirstActionListener());
        buttonPanel.add(firstButton);
        JButton nextButton = new JButton("NEXT " + DisplayLength);
        nextButton.addActionListener(new NextActionListener());
        buttonPanel.add(nextButton);
        add(buttonPanel);
    }

    public class FirstActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            first();
        }
    }

    /************************************************************
     * Displays the first subset in the MolSet
     ************************************************************/
    public void first()
    {
        subsetI = molSet.getSubSetIterator();
        displaySubSet();
    }

    public class NextActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            next();
        }
    }

    /***********************************************************
     * Displays the next subset in the MolSet
     ***********************************************************/
    public void next()
    {
        if( subsetI == null )
        {
            subsetI = molSet.getSubSetIterator();  
        }
        displaySubSet();
    }

    /***********************************************************
     * Adds a MolSubSetPanel, displaying the next subset in the MolSet
     **************************************************************/
    public void displaySubSet()
    {
        Vector subset = null;

        if( subsetPanel != null )
        {
            remove(subsetPanel);
        }

        if( subsetI.hasNext() )
        {
            subset = (Vector) subsetI.next();
            subsetPanel = new MolSubSetPanel(subset,parent);
            add(subsetPanel);
            revalidate();
        }
    }

}
