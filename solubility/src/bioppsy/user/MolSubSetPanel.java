package bioppsy.user;


// gui
import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;


//java 
import java.util.Vector;

/***********************************************************************
 * A Panel that displays a subset of a MolSet. The subset is provided
 * to the constructor as a Vector of Mol objects to display.
 **********************************************************************/
public class MolSubSetPanel extends JPanel
{
	static Logger logger = Logger.getLogger(MolSubSetPanel.class);

    private Vector subset;
    private JTable table;
    private MolSetPanel parent;

    public MolSubSetPanel(Vector subset, MolSetPanel parent)
    {
       super();
       this.parent = parent;
       this.subset = subset;
       this.setAlignmentX(Component.LEFT_ALIGNMENT);
       this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
       display();
    }

    public void display()
    {
        Vector columnNames = new Vector();
        columnNames.add("NUMBER");
        columnNames.add("Molecule identifier");

        Vector matrix = new Vector();
        Vector row;
        Mol mol;
        for( int i=0; i < subset.size(); ++i )
        {
            mol = (Mol) subset.elementAt(i);
            row = new Vector();
            row.add((new Integer(mol.getNumber())).toString());
            row.add(mol.getMolId());
            matrix.add(row);
        }

        table = new JTable(matrix,columnNames);
        table.addMouseListener(new TableMouseListener());
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        add(scrollPane);
    }

    public class TableMouseListener extends MouseAdapter
    {
        public void mousePressed(MouseEvent me)
        {
            if( me.getClickCount() == 2 )
            {
                int element = table.rowAtPoint(me.getPoint());
                Mol mol = (Mol) subset.elementAt(element);
                parent.displayMol(mol);
            }
        }
    }
}
