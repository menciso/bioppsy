package bioppsy.user;

//property

// gui
import javax.swing.*; 
import javax.swing.event.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;


//java
import java.util.Vector;
//Jama
import Jama.Matrix;

class ParameterPanel extends JPanel
{ 
	private Matrix X = null;
    private MLR alg;
    private JTable paramsTable;
    private JTable detailstable;
    private JScrollPane scrollpane = null;
    private GUI gui;
    private JPanel bottomButtons = null;
    private boolean showBottomButtons;

    public static int TYPE_COLUMN_INDEX = 1;

    Popup smartsPopup = null;

    static Logger logger = Logger.getLogger(ParameterPanel.class);

    public ParameterPanel(Matrix X, MLR alg, GUI gui)
    {
        super();
        this.X = X;
        this.alg = alg;
        this.gui = gui;
        showBottomButtons = false;
        display();
    }

    //create a panel with added table to be updated when a modification has happened
    public ParameterPanel(MLR alg, GUI gui, JTable detailstable)
    {
        super();
        this.detailstable=detailstable;
        this.alg = alg;
        this.gui = gui;
        showBottomButtons = true;
        display();
    }
    
    public ParameterPanel(MLR alg, GUI gui)
    {
        super();
        this.alg = alg;
        this.gui = gui;
        showBottomButtons = true;
        display();
    }

    public void display()
    {
        if( scrollpane != null )
        {
            remove(scrollpane);
        } 
        if( bottomButtons != null )
        {
            remove(bottomButtons);
        }
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        Vector columnNames = new Vector();
        columnNames.add("PARAMETER");
        columnNames.add("TYPE");
        if( X != null )
        {
            columnNames.add("OCCURRENCE");
        }
        columnNames.add("COEFFICIENT");
        columnNames.add("FREQUENCY");
        Vector matrix = new Vector();
        for( int i=0; i < alg.getParamList().size(); ++i )
        {
            Vector row = new Vector();
            Param param = alg.getParamList().paramAt(i);
            row.add((new Integer(i)).toString());
            row.add(param.getType());
            if( X != null )
            {
                double value = X.get(0,i);
                row.add((new Double(value)).toString());
            }
            double coefficient = param.getValue();
            row.add((new Double(coefficient)).toString());
            row.add("" + param.getFrequency());
            matrix.add(row);
        }
        
        //create a jTable that does not allow edits on any col except col 2
        paramsTable = new JTable(matrix,columnNames){
			public boolean isCellEditable(int rowIndex, int vColIndex) {
                if(vColIndex==2){
                    return true;
                }
                return false;
            }
        };
        paramsTable.addMouseListener(new TableMouseListener());
        paramsTable.getModel().addTableModelListener(new ModifyParametersListener(this.detailstable));
        scrollpane = new JScrollPane(paramsTable);
        add(scrollpane);

        if( showBottomButtons )
        {
            bottomButtons = new JPanel();
            bottomButtons.setLayout( new BoxLayout(bottomButtons,
                BoxLayout.LINE_AXIS) );
            bottomButtons.setAlignmentX(Component.LEFT_ALIGNMENT);

            JButton removeParams = new JButton("Remove Selected Parameters");
            removeParams.addActionListener(new RemoveParamsActionListener());
            bottomButtons.add(removeParams);
            JButton plotParam = new JButton("Plot Selected Parameter");
            plotParam.addActionListener(new PlotParamActionListener());
            bottomButtons.add(plotParam);
            add(bottomButtons);
        }
    }

    
    private class ModifyParametersListener implements TableModelListener
    {
        private JTable detailTable;
        public ModifyParametersListener(JTable detailTable){
            super();
            this.detailTable=detailTable;
        }
        public void tableChanged(TableModelEvent e)
        {
            TableModel model = (TableModel)e.getSource();
            
            int row = e.getFirstRow();
            int column=e.getColumn();
            //set the value to the value of the cell
            double value=0;
            //if the cell does not contain a number change it 0.0
            try{
                //obtain and cast the value from the cell
                value=Double.valueOf(model.getValueAt(row, column).toString()).doubleValue();
            }catch(NumberFormatException except){
                //set the display of the cell to 0.0
                model.setValueAt("0.0", row, column);
            }
        
            switch(column){
                case 2: //value field
                    //assign the value to the parameter
                    alg.getParamList().paramAt(row).setValue(value);
                    //reset R^2 and StdDev
                    detailTable.setValueAt("0", 3, 1);
                    detailTable.setValueAt("0", 4, 1);
                     
                    //set that the alg has been modified so it becomes updated
                    alg.setModified();
                    break;         
            }
        }
    }
    
    public class TableMouseListener extends MouseAdapter
    {
        public void mousePressed(MouseEvent e)
        {
            showATParamSmartsPopup(e);
        }

        public void mouseReleased(MouseEvent e)
        {
            hideATParamSmartsPopup();
        }
    }

    /*******************************************************
     * Display the ATParam smarts patterns as a popup next
     * to where the mouse cursor is.
     *******************************************************/
    public void showATParamSmartsPopup(MouseEvent e)
    {
        int row = paramsTable.rowAtPoint(e.getPoint());
        int column = paramsTable.columnAtPoint(e.getPoint());
        Point tableLocation = paramsTable.getLocationOnScreen();

        String[] paramTypes = Constants.instance().getStringArray(
            "PARAM_TYPES");
        String atParamClass = paramTypes[Constants.instance().getInt(
            "ATPARAM_INDEX")];
        String groupParamClass = paramTypes[Constants.instance().getInt(
            "GROUP_PARAM_INDEX")];

        // Padding to the right of where the mouse is
        // otherwise the cursor hides the popup
        int xPadding = 15;

        if( column == TYPE_COLUMN_INDEX )
        {
            Param param = alg.getParamList().paramAt(row);
            ATParam atParam;
            if( (param.getClass().getName().equals(atParamClass)) ||
                (param.getClass().getName().equals(groupParamClass)) )
            {
                atParam = (ATParam) param;
            }
            else
            {
                return;
            }

            Vector smarts = atParam.getSmartsPatterns();

            JPanel popupPanel = new JPanel();
            popupPanel.setLayout(new BoxLayout(
                popupPanel, BoxLayout.PAGE_AXIS));
            for( int i=0; i < smarts.size(); ++i )
            {
                JLabel label = new JLabel(
                    (String) smarts.elementAt(i));
                if( i == 0 )
                {
                    label.setAlignmentX(Component.LEFT_ALIGNMENT);
                }
                popupPanel.add(label);
            }

            PopupFactory factory = PopupFactory.getSharedInstance();
            smartsPopup = factory.getPopup
            (
                paramsTable,
                popupPanel,
                e.getPoint().x + tableLocation.x + xPadding,
                e.getPoint().y + tableLocation.y
            );
            smartsPopup.show();
        }
    }

    public void hideATParamSmartsPopup()
    {
        if( smartsPopup != null )
        {
            smartsPopup.hide();
            smartsPopup = null;
        }
    }

    public class RemoveParamsActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            removeParams();
        }
    }

    public void removeParams()
    {
        int[] rows = paramsTable.getSelectedRows();
        int count = 0;
        if( rows.length <= 0 )
        {
            return;
        }
        // we have to start from the end, otherwise the
        // indices in the ParamList will change
        java.util.Arrays.sort(rows);
        for( int i = (rows.length - 1); i >= 0; --i )
        {
            try
            {
                alg.getParamList().removeElementAt(rows[i]);
                ++count;
            }
            catch( ArrayIndexOutOfBoundsException ae )
            {
                logger.warn("Tried to remove a Param from ParamList that " +
                    " does not exist: " + rows[i]);
            }
        }
        JOptionPane.showMessageDialog(
            gui,
            count + " Parameters have been removed",
            "Parameters Removed", 
            JOptionPane.PLAIN_MESSAGE );

        alg.getParamList().reIndex();

        display();
    }

    public class PlotParamActionListener
       implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            plotParam();
        }
    }

    public void plotParam()
    {
        int row = paramsTable.getSelectedRow();
        if( row < 0 )
        {
            logger.debug("No parameter selected");
            return;
        }

        Param param = alg.getParamList().paramAt(row);

        ErrorPlotPanel errorPlotPanel
            = new ErrorPlotPanel
            (
                alg.getMolSet(),
                param,
                alg,
                gui 
            );

        gui.add(errorPlotPanel);
        errorPlotPanel.load();
    }

}
