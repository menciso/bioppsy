package bioppsy.user;

//log4j
import org.apache.log4j.Logger;

public interface ObjectListener
{
    static Logger logger = Logger.getLogger(ObjectListener.class);

    public void objectReady(Object obj);
}
