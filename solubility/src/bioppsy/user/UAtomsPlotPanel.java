package bioppsy.user;


//log4j
import org.apache.log4j.Logger;

import bioppsy.*;
import bioppsy.algorithm.MLR;



/**********************************************************************
 * An UAtomsPlotPanel has a MolSet, and an Algorithm. For
 * each Mol in the MolSet the error (|calculated - experimental|)
 * is plotted against the number of atoms in the Mol that do not match
 * any of the ATParam in the Algorithm.
 **********************************************************************/
public class UAtomsPlotPanel extends PlotPanel
{
    static Logger logger = Logger.getLogger(UAtomsPlotPanel.class);

    private MLR alg;

    public UAtomsPlotPanel(MolSet molSet, MLR alg, GUI parent)
    {
        super(molSet, parent);
        this.alg = alg;
    }

    public String getTitle()
    {
        return "Error Plot";
    }

    public String getChartTitle()
    {
        return "Unmatched Atoms Against Error";
    }

    public String getXTitle()
    {
        return "Number of Unmatched Atoms in a Molecule";
    }

    public String getYTitle()
    {
        return "Error";
    }

    public void addRowToDataset(Mol mol, VectorXYDataset dataset)
    {
        Result result;
        double expSol;
        double error;
        double numUnmatched;

        result = new Result();
        if( alg.calculateProperty(mol,result) == null )
        {
            return;
        }
        try
        {
            expSol = mol.getExpProp();
        }
        catch( NotFoundException nfe )
        {
            return;
        }
//      error = Math.abs(result.getValue()-expSol);
        error = result.getValue()-expSol;
        numUnmatched = alg.getUnmatchedAtoms(mol).size();
        dataset.add(numUnmatched,error);
    }
}
