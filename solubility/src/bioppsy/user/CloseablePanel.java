package bioppsy.user;

// gui
import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;

public class CloseablePanel extends JPanel
{
    static Logger logger = Logger.getLogger(CloseablePanel.class);

    protected GUI parent;

    public CloseablePanel(GUI parent)
    {
        this.parent = parent;
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        addMenuPanel();
    }

    public void addMenuPanel()
    {
        JPanel menuPanel = new JPanel();
        menuPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.LINE_AXIS));
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener( new CloseActionListener() );
        addMenuItems(menuPanel);
        menuPanel.add(Box.createHorizontalGlue());
        menuPanel.add(closeButton);
        this.add(menuPanel);
    }

    public void addMenuItems(JPanel menuPanel) {}

    public class CloseActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            close();
        }
    }

    public void close()
    {
        parent.remove(this);
    }

    public String getTitle()
    {
        return "Closeable Panel";
    }
}
