package bioppsy.user;



// gui
import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

//log4j
import org.apache.log4j.Logger;

import bioppsy.Constants;
import bioppsy.MolSet;
import bioppsy.algorithm.MLR;


//java
import java.io.File;

public class AlgCreateDialog extends JDialog
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JFileChooser chooser;
    private ObjectListener ol = null;
    private MLR alg = null;

    private JLabel nameLabel = null;
    private JTextField nameText = null;

    private JComboBox typeComboBox = null;

    private JPanel molPanel = null;
    private JLabel molFileLabel = null;
    private JTextField molFile = null;
    private JButton molFileBrowse = null;

    private JButton submit = null;;

    static Logger logger = Logger.getLogger(AlgCreateDialog.class);

    public AlgCreateDialog(JFrame parent)
    {
        super(parent, "New Algorithm");
        JFrame.setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        createAndShowGUI();
        pack();
        position();
        //setSize(500,500);
    }

    public void position()
    {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension size = getSize();
        int x = (int) ((dim.getWidth()/2)-(size.getWidth()/2));
        int y = (int) ((dim.getHeight()/2)-(size.getHeight()/2));
        setLocation(x,y);
    }

    public void setObjectListener(ObjectListener ol)
    {
        this.ol = ol;
    }

    public void start()
    {
        setVisible(true);
    }

    public void createAndShowGUI()
    {
        getContentPane().setLayout(
            new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));

        getContentPane().add(Box.createRigidArea(new Dimension(5,10)));

        nameLabel = new JLabel("Please enter a name");
        nameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        getContentPane().add(nameLabel);
        nameText = new JTextField(10);
        getContentPane().add(nameText);

        getContentPane().add(Box.createRigidArea(new Dimension(5,10)));

        JLabel typeLabel = new JLabel("Please select the type of Algorithm");
        typeLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        getContentPane().add(typeLabel);
        String[] types = Constants.instance().getStringArray("ALGORITHM_TYPES");
        typeComboBox = new JComboBox(types);
        typeComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        getContentPane().add(typeComboBox);

        getContentPane().add(Box.createRigidArea(new Dimension(5,10)));

        molFileLabel = new JLabel("Filename of training molecule set");
        molFileLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        getContentPane().add(molFileLabel);

        molPanel = new JPanel();
        molPanel.setLayout(
            new BoxLayout(molPanel, BoxLayout.LINE_AXIS));
        molPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        molFile = new JTextField(20);
        molPanel.add(molFile);
        molFileBrowse = new JButton("Browse");
        molFileBrowse.addActionListener(new MolFileBrowseActionListener());
        molPanel.add(molFileBrowse);
        getContentPane().add(molPanel);

        getContentPane().add(Box.createRigidArea(new Dimension(5,10)));

        submit = new JButton("OK");
        submit.setAlignmentX(Component.LEFT_ALIGNMENT);
        submit.addActionListener(new OkActionListener());
        getContentPane().add(submit);

        validate();
    }

    public class MolFileBrowseActionListener implements ActionListener
    {
         public void actionPerformed(ActionEvent e)
         {
              molFileBrowse();
         }
    }

    public void molFileBrowse()
    {
        //JFileChooser chooser = new JFileChooser();
        File file;

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
           file = chooser.getSelectedFile();
           molFile.setText(file.getAbsolutePath());
           validate();
        }
    }

    public class OkActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
             ok();
        }
    }

    public void ok()
    {
        if( !parseInput() )
        {
            return;
        }

        if( ol != null )
        {
            ol.objectReady(alg);
        }

        dispose();
    }

    public boolean parseInput()
    {
        String name = nameText.getText();
        String filename = molFile.getText();

        if( (name == null) ||
            (name.trim().length() == 0) )
        {
            JOptionPane.showMessageDialog(
                this,
                "Please enter a name",
                "Error", 
                JOptionPane.ERROR_MESSAGE );
            return false;
        }

        if( (filename == null) ||
            (filename.trim().length() == 0) )
        {
            JOptionPane.showMessageDialog(
                this,
                "Please enter a filename first",
                "Error", 
                JOptionPane.ERROR_MESSAGE );
            return false;
        }

        MolSet molSet = new MolSet("$" + filename);
        if( !molSet.setFile("$" + filename) )
        {
            JOptionPane.showMessageDialog(
                this,
                "File invalid: " + filename,
                "Error", 
                JOptionPane.ERROR_MESSAGE );
            return false;
        }
 
        String type = (String) typeComboBox.getSelectedItem();

        try
        {
            alg = (MLR) Class.forName("bioppsy.algorithm."+type).newInstance();
            //alg = (Algorithm) Class.forName(type).newInstance();
            alg.setMolSet(molSet);
            alg.setName(name);
        }
        catch( ClassNotFoundException cnfe )
        {
            logger.error("Class not found: " + cnfe.getMessage());
            return false;
        }
        catch( InstantiationException ie )
        {
            logger.error("Unable to instantiate: " + ie.getMessage());
            return false;
        }
        catch( IllegalAccessException ie )
        {
            logger.error("Illegal access: " + ie.getMessage());
            return false;
        }

        return true;
    }
	
	/*********************************************************
	* Set the File chooser to be used
	*********************************************************/
	public void setJFileChooser(JFileChooser chsr)
	{
		this.chooser=chsr;
	}
}
